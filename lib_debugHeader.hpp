#ifdef DEBUG
# define passing() cerr << "passing line [" << __LINE__ << "] in No." << clock() << "ms" << endl
# define debug(...) fprintf(stderr, __VA_ARGS__)
# define show(x) cerr << #x << " = " << (x) << endl
#else
# define passing() do if (0) cerr << "passing line [" << __LINE__ << "] in No." << clock() << "ms" << endl; while(0)
# define debug(...) do if (0) fprintf(stderr, __VA_ARGS__); while(0)
# define show(x) do if (0) cerr << #x << " = " << (x) << endl; while(0)
#endif