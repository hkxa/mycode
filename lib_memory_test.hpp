bool ZYLIB_MEMORY_MEASURE_TAG_1;
#define USING_MEMORY_TEST_END_TAG()                                             \
bool ZYLIB_MEMORY_MEASURE_TAG_2;                                                \
bool ZYLIB_TEST_MEMORY() {                                                      \
    size_t MemByte = &ZYLIB_MEMORY_MEASURE_TAG_2 - &ZYLIB_MEMORY_MEASURE_TAG_1; \
    double MemMiB = MemByte / 1048576.0;                                        \
    return fprintf(stderr, "memory: %.6lf MiB\n", MemMiB) == 1;                 \
}                                                                               \
bool ZYLIB_TEST_MEMORY_RUNNER_NOUSE_VARIABLE = ZYLIB_TEST_MEMORY();    