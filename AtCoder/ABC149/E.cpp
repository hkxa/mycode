/*************************************
 * @contest:      ABC149.
 * @user_name:    hkxadpall.
 * @time:         2019-12-29.
 * @language:     C++.
 * @upload_place: AtCoder.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n; 
long long m, sm;
int a[100007];
long long sum[100007];
long long ans = 0;
#define max3(a, b, c) max(max((a), (b)), (c))

int main()
{
    n = read<int>();
    a[n + 1] = -100001;
    m = read<long long>();
    // sm = sqrt(m);
    for (int i = 1; i <= n; i++) a[i] = read<int>();
    sort(a + 1, a + n + 1, greater<int>());
    for (int i = 1; i <= n; i++) sum[i] = sum[i - 1] + a[i];
    // for (int i = 1; i <= n; i++) write(a[i], i == n ? 10 : 32);
    // system("pause");
    // ans = 2 * a[1];
    for (int i = 1, j = 1, k = 0, t; m > 0; ) {
        t = max3(a[i] + a[j + 1], a[i + 1] + a[j], a[k + 1] * 2);
        if (t == a[k + 1] * 2) {
            k++; m--;
            ans += a[k] * 2;
        } else if (t == a[i] + a[j + 1]) {
            j++;
            if (m >= 2 * i) {
                m -= 2 * i;
                ans += 2 * (sum[i] + i * a[j]);
            } else {
                ans += 2 * (sum[m / 2] + (m / 2) * a[j]);
                if (m & 1) ans += a[m / 2 + 1] + a[j];
                m = 0;
            }
        } else {
            i++;
            if (m >= 2 * (j - i)) {
                m -= 2 * (j - i);
                ans += 2 * (sum[j] - sum[i] + (j - i) * a[i]);
            } else {
                ans += 2 * (sum[i + m / 2] - sum[i] + (m / 2) * a[i]);
                if (m & 1) ans += a[i + m / 2 + 1] + a[i];
                m = 0;
            }
        }
        // printf("i = %d, j = %d, k = %d, m = %d.\n", i, j, k, m);
    }
    write(ans);
    return 0;
}