/*************************************
 * @contest:      ABC149.
 * @user_name:    hkxadpall.
 * @time:         2019-12-29.
 * @language:     C++.
 * @upload_place: AtCoder.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}
// #define int long long
int n; 
long long m, sm;
int a[100007];
long long sum[100007];
long long ans = 0;
#define max3(a, b, c) max(max((a), (b)), (c))

long long testLimit(int x)
{
    long long cnt = 0;
    for (int i = 1, j = n; i <= n; i++) {
        while (a[i] + a[j] < x && j) j--;
        cnt += j;
    }
    return cnt;
}

int main()
{
    n = read<int>();
    m = read<long long>();
    for (int i = 1; i <= n; i++) a[i] = read<int>();
    sort(a + 1, a + n + 1, greater<int>());
    for (int i = 1; i <= n; i++) sum[i] = sum[i - 1] + a[i];
    int l = 0, r = a[1] * 2 + 1, limit = 0, mid;
    while (l <= r) {
        mid = (l + r) >> 1;
        // printf("l = %d, r = %d, mid = %d, limit = %d.\n", l, r, mid, limit);
        if (testLimit(mid) >= m) {
            limit = mid;
            l = mid + 1;
        } else r = mid - 1;
    }
    // printf("limit = %d.\n", limit);
    long long cnt = 0;
    for (int i = 1, j = n; i <= n; i++) {
        while (a[i] + a[j] < limit && j) j--;
        ans += sum[j] + (long long)j * a[i];
        cnt += j;
        // printf("uses %d & %d~%d.\n", i, 1, j);
    }
    write(ans - (cnt - m) * limit);
    return 0;
}

/*
E: Handshake(writer : yuma000)
Of course, you can maximize the ultimate happiness M times from the handshake that increases happiness the most to the least. But, if you implement naively it won’t finish in time. We will explain the way of finding it efficiently.
Solution 1(Solution using binary search)
First, consider how many ways of handshakes are there by which the happiness increases by more than or equal to a constant X. When a person to shake hand for his left hand is fixed, you can find how many people are there whom he can shake hand with for his right hand in O(1) time using cumulative sums, so it can be calculated in a total of O(N) time. Then, you can perform binary search for X so as to find the minimum X such that he can perform M ways of handshakes, so you can find the ultimate happiness by using cumulative sums again. The time complexity is O(NlogN).
Solution 2(Fast Fourier Transform)(Bonus)
Since Ai ≤ 100, 000, you can solve this problem by Fast Fourier Transform (hereinafter FFT). For more details about FFT, please look for many resources online. (There are an educational problem on AtCoder, but the problem statement is provided only in Japanese. For your information, the URL is as follows: )
https://atcoder.jp/contests/atc001/tasks/fft_c
Specifically, 
• Prepare two arrays, each of whose i-th element contains the number of people with power i.
• Find the convolution of those array. (By performing FFT, it can be done in a total of O(max(Ai)log(max(Ai)) time.)
so that you can find the number of ways for each increasing happinesses. Lastly count them in the decreasing order of increasing happinesses, and you can solve the problem.
*/