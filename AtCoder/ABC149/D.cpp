/*************************************
 * @contest:      ABC149.
 * @user_name:    hkxadpall.
 * @time:         2019-12-29.
 * @language:     C++.
 * @upload_place: AtCoder.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, k;
int r, s, p;
char t[100007];
int f[100007][3], ans = 0; // 0-r 1-s 2-p
#define max3(a, b, c) max(max((a), (b)), (c))

int earn(int pos, int cse)
{
    if (t[pos] == 'r' && cse == 2) return p;
    if (t[pos] == 's' && cse == 0) return r;
    if (t[pos] == 'p' && cse == 1) return s;
    return 0;
}

int main()
{
    cin >> n >> k >> r >> s >> p;
    scanf("%s", t + 1);
    for (int i = 1; i <= k; i++) {
        f[i][0] = earn(i, 0);
        f[i][1] = earn(i, 1);
        f[i][2] = earn(i, 2);
        for (int j = i + k; j <= n; j += k) {
            f[j][0] = max(f[j - k][1], f[j - k][2]) + earn(j, 0);
            f[j][1] = max(f[j - k][0], f[j - k][2]) + earn(j, 1);
            f[j][2] = max(f[j - k][0], f[j - k][1]) + earn(j, 2);
        }
    }
    // for (int i = 1; i <= n; i++) {
    //     printf("r-%-4d s-%-4d p-%-4d\n", f[i][0], f[i][1], f[i][2]);
    // }
    for (int i = n - k + 1; i <= n; i++) {
        ans += max3(f[i][0], f[i][1], f[i][2]);
    }
    write(ans);
    return 0;
}