//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      ABC172.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-27.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

struct Reader {
    char endch;
    Reader() { endch = '\0'; }
    template <typename Int>
    Reader& operator >> (Int &i) {
        i = baseFastio::read<Int>(endch);
        return *this;
    }
    template <typename Int>
    inline Int get_int() {
        return baseFastio::read<Int>();
    }
    inline char get_nxt() {
        return endch;
    }
} read;

struct Writer {
    Writer& operator << (const char *ch) {
        // char *p = ch;
        while (*ch) putchar(*(ch++));
        return *this;
    }
    Writer& operator << (const char ch) {
        putchar(ch);
        return *this;
    }
    template <typename Int>
    Writer& operator << (const Int i) {
        baseFastio::write(i);
        return *this;
    }
} write;

// #define int int64

const int P = 1e9 + 7, N = 5e5 + 7;

int n, m;
int fac[N], ifac[N];

int fpow(int a, int n, int p) {
    int ret = 1;
    while (n) {
        if (n & 1) ret = (int64)ret * a % p;
        a = (int64)a * a % p;
        n >>= 1;
    }
    return ret;
}

void pre(int g) {
    fac[0] = fac[1] = ifac[0] = 1;
    for (int i = 2; i <= g; i++) fac[i] = (int64)fac[i - 1] * i % P;
    ifac[g] = fpow(fac[g], P - 2, P);
    for (int i = g; i >= 2; i--) ifac[i - 1] = (int64)ifac[i] * i % P;
}

int C(int n, int m) {
    return (int64)fac[n] * ifac[m] % P * ifac[n - m] % P;
}

int P0(int n, int m) {
    // printf("P0(%d, %d) = %d\n", n, m, (int)((int64)fac[n] * ifac[n - m] % P));
    return (int64)fac[n] * ifac[n - m] % P;
}

int64 ac[N], wa[N];

signed main()
{
    read >> n >> m;
    pre(m);
    int base = P0(m, n);
    ac[0] = 1;
    for (int i = 1; i <= n; i++) {
        wa[i] = (wa[i - 1] * fpow(m - i - 1, i - 1, P) + ac[i - 1] * (m - i)) % P;
        ac[i] = (P0(m, i) - wa[i] + P) % P;
        // printf("wa[%d] = %lld, ac[%d] = %lld\n", i, wa[i], i, ac[i]);
    }
    write << ac[n] * base % P << '\n';
    return 0;
}