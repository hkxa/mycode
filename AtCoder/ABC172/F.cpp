//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      ABC172.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-27.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

struct Reader {
    char endch;
    Reader() { endch = '\0'; }
    template <typename Int>
    Reader& operator >> (Int &i) {
        i = baseFastio::read<Int>(endch);
        return *this;
    }
    template <typename Int>
    inline Int get_int() {
        return baseFastio::read<Int>();
    }
    inline char get_nxt() {
        return endch;
    }
} read;

struct Writer {
    Writer& operator << (const char *ch) {
        // char *p = ch;
        while (*ch) putchar(*(ch++));
        return *this;
    }
    Writer& operator << (const char ch) {
        putchar(ch);
        return *this;
    }
    template <typename Int>
    Writer& operator << (const Int i) {
        baseFastio::write(i);
        return *this;
    }
} write;

// #define int int64

int n;
int64 a[307];
int64 Xor;
int m;

signed main()
{
    read >> n;
    for (int i = 1; i <= n; i++) read >> a[i];
    for (int i = 3; i <= n; i++) Xor ^= a[i];
    if (!(Xor ^ a[1] ^ a[2])) {
        write << "0\n";
        // printf("Case $ 0\n");
        return 0;
    }
    if (a[1] + a[2] < Xor) {
        write << "-1\n";
        // printf("Case $ 1\n");
        return 0;
    }
    int64 rest = a[1] + a[2] - Xor;
    if (rest & 1) {
        write << "-1\n";
        // printf("Case $ 2\n");
        return 0;
    }
    rest >>= 1;
    if ((Xor | rest) != (Xor + rest)) {
        write << "-1\n";
        // printf("Case $ 3\n");
        return 0;
    }
    int64 pile = Xor | rest;
    if (pile < a[2]) {
        write << "-1\n";
        // printf("Case $ 4\n");
        return 0;
    }
    // printf("pile = %lld\n", pile);
    for (int i = 50; i >= 0; i--) {
        if ((pile & (1LL << i)) && !(rest & (1LL << i)) && ((pile ^ (1LL << i)) >= a[2])) {
            pile ^= (1LL << i);
        }
    }
    if (a[1] - (pile - a[2]) == 0) {
        write << "-1\n";
        // printf("Case $ 5\n");
        return 0;
    }
    write << pile - a[2] << '\n';
    return 0;
}