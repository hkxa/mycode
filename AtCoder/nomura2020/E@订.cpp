//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
       return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
       return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define int int64

char t[200007];
int n;
int c[2], sum[2];
int64 ans;

int64 SolveIt() {
    c[0] = c[1] = 0;
    ans = 0;
    sum[0] = sum[1] = 0;
    for (int i = 1; i <= n; i++) {
        if (t[i] & 1) {
            ans += (sum[0] + (sum[1] & 1)) >> 1;
            c[sum[1] & 1]++;
            sum[1]++;
            ans += (sum[1] + 1) >> 1;
        } else {
            ans += c[0] = c[1] = max(c[0], c[1]);
            sum[0]++;
        }
    }
    return ans;
}

signed main() {
    scanf("%s", t + 1);
    n = strlen(t + 1);
    write(SolveIt(), 10);
    return 0;
}

// Create File Date : 2020-05-30
/*
$ $ $ $ $
0111101101
0 1 1 1 0
 1 1 0 1 1

1               1
11              1
111             2
1111            2
11111           3
111111          3
1111111         4
11111101        3
111101101       4
0111101101      3

1       1
11      1
111     2
1101    1

0111101101
1111111         
01111111        3
011110111       4
0111101101      3
*/