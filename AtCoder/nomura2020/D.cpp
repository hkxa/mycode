//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
       return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
       return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// #define int int64

const int P = 1000000007;

inline int64 kpow(int64 a, int n) {
    int64 r = 1;
    while (n) {
        if (n & 1) r = r * a % P;
        a = a * a % P;
        n >>= 1;
    }
    return r;
}
#define inv(x) kpow(x, P - 2)
#define div(x, y) ((x) * inv(y) % P)
#define mul(x, y) ((int64)(x) * (y) % P)
inline int64 add(int64 x, int64 y) { return x + y >= P ? x + y - P : x + y; }
inline int64 minus(int64 x, int64 y) { return x + y < 0 ? x + y + P : x + y; }

const int N = 5007;
int n, k, independent;
int p[N];
int fa[N];
int family = 0;
int64 ans, n1k;

int find(int u) {
    return u == fa[u] ? u : fa[u] = find(fa[u]);
}

inline void connect(int u, int v) {
    fa[find(u)] = find(v);
}

signed main() {
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        fa[i] = i;
        p[i] = read<int>();
        if (~p[i]) connect(i, p[i]);
        else k++;
    }
    n1k = kpow(n - 1, k);
    for (int i = 1; i <= n; i++) {
        siz[find(i)]++;
        if (i == fa[i]) {
            family++;
            if (!~p[i]) independent++;
        }
    }
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= n; j++) {
            if (~p[i] && ~p[j]) ans += n1k;
        }
    }
    return 0;
}

// Create File Date : 2020-05-30