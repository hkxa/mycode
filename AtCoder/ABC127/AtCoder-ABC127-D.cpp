/*************************************
 * problem:      ABC127 problem D.
 * user name:    hkxadpall.
 * time:         2019-05-25.
 * language:     C++.
 * upload place: AtCoder.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

int n, m;
struct op {
    long long b, c;
    bool operator < (const op &o) const
    {
        return c > o.c;
    }
} a[200007];

int main()
{
    n = read<int>();
    m = read<int>();
    for (int i = 1; i <= n; i++) {
        a[i].b = 1;
        a[i].c = read<long long>();
    }
    for (int i = 1; i <= m; i++) {
        a[n + i].b = read<long long>();
        a[n + i].c = read<long long>();
    }
    sort(a + 1, a + n + m + 1);
    long long ans(0);
    int wt = n, pos = 0;
    while (wt) {
        pos++;
        if (a[pos].b >= wt) {
            ans += a[pos].c * wt;
            write(ans);
            return 0;
        }
        wt -= a[pos].b;
        ans += a[pos].c * a[pos].b;
    }
    write(ans);
    return 0;
}