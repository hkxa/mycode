/*************************************
 * problem:      ABC127 problem A.
 * user name:    hkxadpall.
 * time:         2019-05-25.
 * language:     C++.
 * upload place: AtCoder.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

int n, m;

int main()
{
    n = read<int>();
    m = read<int>();
    if (n < 6) puts("0");
    else if (n <= 12) write(m / 2);
    else write(m);
    return 0;
}