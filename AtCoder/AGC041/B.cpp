/*************************************
 * @contest:      AGC041.
 * @user_name:    hkxadpall.
 * @time:         2019-12-28.
 * @language:     C++.
 * @upload_place: AtCoder.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

long long n, v, p; long long m;
struct Prob {
    long long s;
    long long id;
    bool operator < (const Prob &b) const { return s > b.s; }
} a[100006];

int main()
{
    n = read<long long>();
    m = read<long long>();
    v = read<long long>();
    p = read<long long>();
    for (long long i = 1; i <= n; i++) {
        a[i].id = i;
        a[i].s = read<long long>();
    }
    sort(a + 1, a + n + 1);
    if (v <= p) {
        for (long long ans = p; ans <= n; ans++) {
            if (a[ans + 1].s + m < a[p].s || ans == n) {
                write(ans, 10);
                break;
            }
        }
    } else {
        long long could = (n - v) * m;
        long long sum = 0;
        long long ans = p;
        long long minn = a[p].s - m;
        while (ans <= n) {
            sum += a[ans].s;
            if (a[ans + 1].s < minn) break;
            if (sum - a[ans + 1].s * (ans - p + 1) > could) break;
            ans++;
        }
        write(min(ans, n), 10);
    }
    return 0;
}