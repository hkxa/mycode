/*************************************
 * @contest:      AGC041.
 * @user_name:    hkxadpall.
 * @time:         2019-12-28.
 * @language:     C++.
 * @upload_place: AtCoder.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;
char output[1007][1007];
// bool col[1007];
int now = 0;

int main()
{
    n = read<int>();
    if (n % 3 == 0) {
        for (register int i = 0; i < (n / 3); i++) {
            output[i * 3][i * 3] = output[i * 3][i * 3 + 1] = 'a' + i % 13 * 2;
            output[i * 3 + 1][i * 3 + 2] = output[i * 3 + 2][i * 3 + 2] = 'a' + i % 13 * 2 + 1;
        }
        for (register int i = 0; i < n; i++) {
            for (register int j = 0; j < n; j++) {
                if (output[i][j]) putchar(output[i][j]);
                else putchar('.');
            }
            putchar('\n');
        }
    } else if (n % 2 == 0 && n > 4) {
        for (int i = 0; i < n; i ++) {
            output[i][i * 2 % n] = output[i][i * 2 % n + 1] = (now++) % 5 + 'a';
        }
        for (int i = 0, pos = n / 2 - 1; i < n / 4; i++) {
            output[i * 2][pos * 2] = output[i * 2 + 1][pos * 2] = (now++) % 5 + 'h';
            output[i * 2][pos * 2 + 1] = output[i * 2 + 1][pos * 2 + 1] = (now++) % 5 + 'h';
            pos = (pos - 2 + (n / 2)) % (n / 2);
        }
        for (int i = n / 2 - 1, pos = 0; i >= n / 4; i--) {
            output[i * 2][pos * 2] = output[i * 2 + 1][pos * 2] = (now++) % 5 + 'h';
            output[i * 2][pos * 2 + 1] = output[i * 2 + 1][pos * 2 + 1] = (now++) % 5 + 'h';
            pos = (pos - 2 + (n / 2)) % (n / 2);
        }
        for (register int i = 0; i < n; i++) {
            for (register int j = 0; j < n; j++) {
                if (output[i][j]) putchar(output[i][j]);
                else putchar('.');
            }
            putchar('\n');
        }
    } else puts("-1");
    return 0;
}