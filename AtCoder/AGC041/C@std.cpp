#include <iostream>
#include <iomanip>
#include <string>
#include <stack>
#include <vector>
#include <math.h>
#include <stdio.h>
#include <algorithm>
#include <utility>
#include <functional>
#include <iterator>
#include <map>
#include <set>
#include <queue>
#include <list>
#include <regex>
using namespace std;
using pii  = pair<int,int>;
using ll=long long;
using ld=long double;
#define pb push_back
#define mp make_pair
#define sc second
#define fr first
#define stpr setprecision
#define cYES cout<<"YES"<<endl
#define cNO cout<<"NO"<<endl
#define cYes cout<<"Yes"<<endl
#define cNo cout<<"No"<<endl
#define rep(i,n) for(ll i=0;i<(n);++i)
#define Rep(i,a,b) for(ll i=(a);i<(b);++i)
#define rrep(i,n) for(int i=n-1;i>=0;i--)
#define rRep(i,a,b) for(int i=a;i>=b;i--)
#define crep(i) for(char i='a';i<='z';++i)
#define psortsecond(A,N) sort(A,A+N,[](const pii &a, const pii &b){return a.second<b.second;});
#define ALL(x) (x).begin(),(x).end()
#define debug(v) cout<<#v<<":";for(auto x:v){cout<<x<<' ';}cout<<endl;
#define endl '\n'
int ctoi(const char c){
  if('0' <= c && c <= '9') return (c-'0');
  return -1;
}
ll gcd(ll a,ll b){return (b == 0 ? a : gcd(b, a%b));}
ll lcm(ll a,ll b){return a*b/gcd(a,b);}
constexpr ll MOD=1000000007;
constexpr ll INF=1000000011;
constexpr ll MOD2=998244353;
constexpr ll LINF = 1001002003004005006ll;
constexpr ld EPS=10e-8;
template<class T>bool chmax(T &a, const T &b) { if (a<b) { a=b; return 1; } return 0; }
template<class T>bool chmin(T &a, const T &b) { if (b<a) { a=b; return 1; } return 0; }
template<typename T> istream& operator>>(istream& is,vector<T>& v){for(auto&& x:v)is >> x;return is;}
template<typename T,typename U> istream& operator>>(istream& is, pair<T,U>& p){ is >> p.first; is >> p.second; return is;}
template<typename T,typename U> ostream& operator>>(ostream& os, const pair<T,U>& p){ os << p.first << ' ' << p.second; return os;}
template<class T> ostream& operator<<(ostream& os, vector<T>& v){
  for(auto i=begin(v); i != end(v); ++i){
    if(i !=begin(v)) os << ' ';
    os << *i;
  }
  return os;
}
 
void fanc6(ll N){
  rep(i,N){
    rep(j,N){
      if(i==0){
        if((j==0||j==1)||j==N-1){
          cout << 'a';
        }
        else if((j==N-2||j==N-3)){
          cout << 'b';
        }
        else{
          cout << '.';
        }
      }
      else if(i==N-1){
        if((j==0||j==N-2)||j==N-1){
          cout << 'a';
        }
        else if((j==1||j==2)){
          cout << 'b';
        }
        else{
          cout << '.';
        }
      }
      else{
        if(j==0){
          if(i==1 || i==2){
            cout << 'b';
          }
          else if(i==N-2){
            cout << 'a';
          }
          else{
            cout << '.';
          }
        }
 
        else if(j==N-1){
          if(i==1){
            cout<<'a';
          }
          else if(i==N-2 || i==N-3){
            cout<<'b';
          }
          else{
            cout<<'.';
          }
        }
 
        else{
          if((j==i+1 || j==i+2)&&i!=N-3){
            if(i%2==1){
              cout<<'c';
            }
            else{
              cout<<'d';
            }
          }
          else if((i!=N-2 && j==i-1)||j==i-2){
            if(j%2==1){
              cout<<'c';
            }
            else{
              cout<<'d';
            }
          }
          else{
            cout <<'.';
          }
        }
      }
    }
    cout << endl;
  }
}
 
void fanc5(){
  cout << "aabbc" << endl << "dee.c" << endl << "d..ea" << endl << "a..ea" <<  endl << "abbcc";
}
 
void fanc4(){
  cout << "aacd" << endl << "bbcd" << endl << "cdaa" << endl << "cdbb" <<  endl;
}
 
void fanc3(){
  cout << "aa." << endl << "..a" << endl << "..a" << endl;
}
 
int main(){
  ll N;
  cin >> N;
  if (N==2) cout << -1 << endl;
  else if(N==3) fanc3();
  else if(N==4) fanc4();
  else if(N==5) fanc5();
  else fanc6(N);
}