/*************************************
 * @contest:      AtCoder ABC156.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-02-22.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define P 1000000007

int n, a, b;

int64 fexp(int64 a, int np)
{
    int64 res = 1;
    while (np) {
        if (np & 1) res = res * a % P;
        a = a * a % P;
        np >>= 1;
    }
    return res;
}
#define inv(x) fexp(x, P - 2)
int64 fac(int n)
{
    register int64 res = 1;
    for (register int i = 2; i <= n; i++) res = res * i % P;
    return res;
}
int64 C(int n, int m)
{
    int64 res = 1;
    for (int i = m; i >= m - n + 1; i--) res = res * i % P;
    return res * inv(fac(n)) % P;
}

int main()
{
    n = read<int>();
    a = read<int>();
    b = read<int>();
    int64 ans = fexp(2, n) - 1;
    ans = (ans - C(a, n) + P) % P;
    ans = (ans - C(b, n) + P) % P;
    write(ans, 10);
    return 0;
}