/*************************************
 * @problem:      ABC327 F.
 * @author:       brealid.
 * @time:         2023-11-04.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

// #define USE_FREAD  // 使用 fread  读入，去注释符号
// #define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 2e5;

int n, mx, my;

namespace segmentTree {
    struct treeNode {
        unsigned int c:18;
        unsigned int l:27;
        unsigned int r:27;
    } tr[N * 600];
    int root[(N + 5) << 2], cnt;
    void y_rangeadd(int u, int l, int r, int yl, int yr) {
        if (l >= yl && r <= yr) {
            ++tr[u].c;
            return;
        }
        int mid = (l + r) >> 1;
        if (mid >= yl) {
            if (!tr[u].l) tr[u].l = ++cnt;
            y_rangeadd(tr[u].l, l, mid, yl, yr);
        }
        if (mid < yr) {
            if (!tr[u].r) tr[u].r = ++cnt;
            y_rangeadd(tr[u].r, mid + 1, r, yl, yr);
        }
    }
    void x_rangeadd(int u, int l, int r, int xl, int xr, int yl, int yr) {
        if (l >= xl && r <= xr) {
            if (!root[u]) root[u] = ++cnt;
            y_rangeadd(root[u], 1, N, yl, yr);
            return;
        }
        int mid = (l + r) >> 1, ls = u << 1, rs = ls | 1;
        if (mid >= xl) x_rangeadd(ls, l, mid, xl, xr, yl, yr);
        if (mid < xr) x_rangeadd(rs, mid + 1, r, xl, xr, yl, yr);
    }
    int y_getmax(int u) {
        if (!u) return 0;
        return tr[u].c + max(y_getmax(tr[u].l), y_getmax(tr[u].r));
    }
    int x_getmax(int u, int l, int r) {
        if (l == r) return y_getmax(root[u]);
        int mid = (l + r) >> 1, ls = u << 1, rs = ls | 1;
        return y_getmax(root[u]) + max(x_getmax(ls, l, mid), x_getmax(rs, mid + 1, r));
    }
}

signed main() {
    kin >> n >> mx >> my;
    for (int i = 1, x, y; i <= n; ++i) {
        kin >> x >> y;
        segmentTree::x_rangeadd(1, 1, N, max(1, x - mx + 1), x, max(1, y - my + 1), y);
    }
    kout << segmentTree::x_getmax(1, 1, N) << '\n';
    return 0;
}