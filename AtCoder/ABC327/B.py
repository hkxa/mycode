B = int(input())
A = 1
while A**A < B:
    A += 1
print(A if A ** A == B else -1)