#include <bits/stdc++.h>
typedef long long int64;
using namespace std;
const int64 N = 400010;
inline int max(int x, int y) { return x > y ? x : y; }
inline int min(int x, int y) { return x < y ? x : y; }
inline void swap(int &x, int &y) { x ^= y ^= x ^= y; }
int64 T, n, w, h, C[N];
struct Segment
{
    int64 l, r, h;
    int64 val;
    bool operator<(const Segment &a) const
    {
        return (h != a.h) ? h < a.h : val > a.val;
    }
} Seg[N << 2];
struct SegmentTree
{
    int64 l, r;
    int64 mx, add;
#define l(x) tree[x].l
#define r(x) tree[x].r
#define mx(x) tree[x].mx
#define add(x) tree[x].add
} tree[N << 2];
void init()
{
    memset(Seg, 0, sizeof(Seg));
    memset(tree, 0, sizeof(tree));
}
void Pushup(int64 x)
{
    mx(x) = max(mx(x << 1), mx(x << 1 | 1));
}
void Build(int64 x, int64 l, int64 r)
{
    l(x) = l, r(x) = r, mx(x) = add(x) = 0;
    if (l == r)
        return;
    int64 mid = (l + r) >> 1;
    Build(x << 1, l, mid);
    Build(x << 1 | 1, mid + 1, r);
}
void Pushdown(int64 x)
{
    mx(x << 1) += add(x);
    mx(x << 1 | 1) += add(x);
    add(x << 1) += add(x);
    add(x << 1 | 1) += add(x);
    add(x) = 0;
}
void Change(int64 x, int64 L, int64 R, int64 d)
{
    int64 l = l(x), r = r(x);
    if (L <= l && r <= R)
    {
        mx(x) += d;
        add(x) += d;
        return;
    }
    Pushdown(x);
    int64 mid = (l + r) >> 1;
    if (L <= mid)
        Change(x << 1, L, R, d);
    if (R > mid)
        Change(x << 1 | 1, L, R, d);
    Pushup(x);
}
int main()
{
    T = 1;
    while (T--)
    {
        init();
        scanf("%lld%lld%lld", &n, &w, &h);
        for (int64 i = 1; i <= n; i++)
        {
            int64 x, y, l;
            scanf("%lld%lld", &x, &y);
            C[(i << 1) - 1] = y;
            C[i << 1] = y + h - 1;
            Seg[(i << 1) - 1] = (Segment){y, y + h - 1, x, 1};
            Seg[i << 1] = (Segment){y, y + h - 1, x + w - 1, -1};
        }
        n <<= 1;
        sort(C + 1, C + n + 1);
        sort(Seg + 1, Seg + n + 1);
        int64 cnt = unique(C + 1, C + n + 1) - C - 1;
        for (int64 i = 1; i <= n; i++)
        {
            int64 pos1 = lower_bound(C + 1, C + cnt + 1, Seg[i].l) - C;
            int64 pos2 = lower_bound(C + 1, C + cnt + 1, Seg[i].r) - C;
            Seg[i].l = pos1;
            Seg[i].r = pos2;
        }
        Build(1, 1, cnt);
        int64 ans = 0;
        for (int64 i = 1; i <= n; i++)
        {
            Change(1, Seg[i].l, Seg[i].r, Seg[i].val);
            ans = max(ans, mx(1));
        }
        printf("%lld\n", ans);
    }
    return 0;
}