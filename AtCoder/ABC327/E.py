n = int(input())
p = [0] + list(map(int, input().split()))

f = [[-1e6 for j in range(n + 1)] for i in range(n + 1)]
f[0][0] = 0

for i in range(1, n + 1):
    f[i][0] = 0
    for j in range(i):
        f[i][j + 1] = max(f[i - 1][j + 1], f[i - 1][j] * 0.9 + p[i])

ans = -1e6
cur_fm = 0
for k in range(1, n + 1):
    cur_fm = cur_fm * 0.9 + 1
    cur = f[n][k] / cur_fm - 1200 / (k ** .5)
    ans = max(ans, cur)
print(f'{ans:.12f}')