n, m = map(int, input().split())
a = list(map(int, input().split()))
b = list(map(int, input().split()))

fa = [i for i in range(2 * n + 1)]

def find(u):
    if u == fa[u]:
        return u
    update_list = []
    while u != fa[u]:
        update_list.append(u)
        u = fa[u]
    for upd in update_list:
        fa[upd] = u
    return u

def connect(s, t):
    fa[find(s)] = find(t)

for i in range(m):
    s, t = a[i], b[i]
    connect(s, t + n), connect(s + n, t)

res = 'Yes'
for i in range(1, n + 1):
    if find(i) == find(i + n):
        res = 'No'
print(res)