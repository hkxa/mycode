A = []
for i in range(9):
    A.append(list(map(int, input().split())))

check = lambda lis: len(set(lis)) == 9
ok = 'Yes'

for i in range(9):
    if not check(A[i]):
        ok = 'No'
for i in range(9):
    if not check(x[i] for x in A):
        ok = 'No'
for i in range(3):
    for j in range(3):
        if not check(val for x in A[i*3:i*3+3] for val in x[j*3:j*3+3]):
            ok = 'No'

print(ok)