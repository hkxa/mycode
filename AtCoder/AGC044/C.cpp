//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, _3n;
char t[200007], len;

struct M {
    int p[540000];
    M operator* (const M &T) {
        M ret;
        for (int i = 0; i < _3n; i++) 
            ret.p[i] = T.p[p[i]];
        return ret;
    }
} base, S, R;

int TmpTurnArr[3] = {0, 2, 1};
int turn(int p) {
    // printf("turn(%d) = ", p);
    int digit[13], cnt = 0, res = 0;
    while (p) {
        digit[cnt++] = p % 3;
        p /= 3;
    }
    while (~(--cnt)) {
        res = res * 3 + TmpTurnArr[digit[cnt]];
    }
    // printf("%d\n", res);
    return res;
}

inline M fpow(M x, int n) {
    M ret = base;
    while (n) {
        if (n & 1) ret = ret * x;
        x = x * x;
        n >>= 1;
    }
    return ret;
}

M genR(int k) {
    M ret;
    for (int i = 0; i < _3n; i++) {
        ret.p[i] = (i + k) % _3n;
    }
    return ret;
}

int chk(int p) {
    int cS = 0;
    if (t[p] != 'S') return false;
    while (t[p] == 'S') {
        cS++;
        p++;
    }
    if (!(cS & 1)) return false;
    if (t[p] == 'R') return p + 1;
    return false;
}

int main() {
    n = read<int>();
    _3n = pow(3, n) + 0.5;
    scanf("%s", t);
    len = strlen(t);
    for (int i = 0; i < _3n; i++) {
        S.p[i] = turn(i);
        R.p[i] = i + 1;
        base.p[i] = i;
    }
    R.p[_3n - 1] = 0;
    M ans = base;
    int cS = 0, cR = 0, cSR = 0;
    for (int i = 0, nRet; i < len; i++) {
        if (chk(i)) {
            if (cR) ans = ans * genR(cR);
            if (cS) ans = ans * S;
            // printf("Until %d:\n", i - 1);
            // for (int j = 0; j < _3n; j++) {
            //     write(ans.p[j], j == _3n - 1 ? 10 : 32);
            // }
            while (nRet = chk(i)) {
                // printf("SR jump from %d to %d\n", i, nRet);
                cSR++;
                i = nRet;
            }
            ans = ans * fpow(S * R, cSR);
            cR = cS = cSR = 0;
        }
        if (t[i] == 'S') {
            if (cR) {
                ans = ans * genR(cR);
                cS = 1;
                cR = 0;
            } else cS ^= 1;
        } else if (t[i] == 'R') {
            if (cS) {
                ans = ans * S;
                cS = 0;
            } 
            cR++;
        }
        // printf("Until %d:\n", i);
        // for (int j = 0; j < _3n; j++) {
        //     write(ans.p[j], j == _3n - 1 ? 10 : 32);
        // }
    }
    
    if (cR) ans = ans * genR(cR);
    if (cS) ans = ans * S;
    // M H = base * S * S * R * R * R * S;
    for (int i = 0; i < _3n; i++) {
        write(ans.p[i], i == _3n - 1 ? 10 : 32);
    }
    return 0;
}