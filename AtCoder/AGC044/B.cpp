//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;
int P;

#define min4(a, b, c, d) min(min(a, b), min(c, d))
bool status[507][507];
int ans[507][507], tot;

void Upd(int x, int y, int v) {
    if (x < 1 || x > n || y < 1 || y > n) return;
    if (v < ans[x][y]) {
        ans[x][y] = v;
        v += status[x][y];
        Upd(x - 1, y, v);
        Upd(x + 1, y, v);
        Upd(x, y - 1, v);
        Upd(x, y + 1, v);
    }
}

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) 
        for (int j = 1; j <= n; j++) {
            status[i][j] = 1;
            ans[i][j] = min4(i - 1, j - 1, n - i, n - j);
        }
    for (int i = 1, t, x, y; i <= n * n; i++) {
        P = read<int>();
        x = (P + n - 1) / n; 
        y = (P - 1) % n + 1;
        tot += ans[x][y];
        // if (ans[x][y]) printf("(%d, %d) cost %d\n", x, y, ans[x][y]);
        t = min4(ans[x - 1][y] + status[x - 1][y], ans[x + 1][y] + status[x + 1][y], 
                 ans[x][y - 1] + status[x][y - 1], ans[x][y + 1] + status[x][y + 1]);
        // printf("t = %d\n", t);
        status[x][y] = 0;
        Upd(x - 1, y, t);
        Upd(x + 1, y, t);
        Upd(x, y - 1, t);
        Upd(x, y + 1, t);
        // printf("After del (%d, %d) :\n", x, y);
        // for (int j = 1; j <= n; j++) {
        //     for (int k = 1; k <= n; k++) {
        //         write(ans[j][k], k == n ? 10 : 32);
        //     }
        // }
    }
    write(tot, 10);
    return 0;
}