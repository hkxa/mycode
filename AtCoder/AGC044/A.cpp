//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;
typedef __int128             int128;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int t;
int128 n, a, b, c, d;

map<int128, int128> g;

int128 get(int128 goal) {
    // printf("get(%lld)\n", goal);
    if (!goal) return 0;
    if (g.find(goal) != g.end()) return g[goal];
    int128 ans = d * goal, x = goal / 2;
    ans = min(ans, get(x) + a + goal % 2 * d);
    if (goal % 2) ans = min(ans, get(x + 1) + a + (2 - goal % 2) * d);
    x = goal / 3;
    ans = min(ans, get(x) + b + goal % 3 * d);
    if (goal % 3) ans = min(ans, get(x + 1) + b + (3 - goal % 3) * d);
    x = goal / 5;
    ans = min(ans, get(x) + c + goal % 5 * d);
    if (goal % 5) ans = min(ans, get(x + 1) + c + (5 - goal % 5) * d);
    // if (goal == 4) printf("%d + %d + %d\n", (int)get(x + 1), (int)c, (int)(5 - goal % 5) * (int)d);
    // printf("g[%lld] = ", goal); write(ans, 10);
    // if (ans < 0) { printf("when goal = %lld, ans begin equal to ", goal); write(ans, 10); }
    return g[goal] = ans;
}

int main()
{
    t = read<int>();
    while (t--) {
        n = read<int128>();
        a = read<int128>();
        b = read<int128>();
        c = read<int128>();
        d = read<int128>();
        g.clear();
        g[1] = d;
        g[2] = g[1] + min(d, a);
        g[3] = d + min(g[2], b);
        write(get(n), 10);
        // for (int i = 1; i <= n; i++) {
        //     printf("num %d cost ", i);
        //     write(g[i], 32);
        //     printf("coins\n");
        // }
    }
    return 0;
}