/*************************************
 * @contest:      dwacon6th-prelims.
 * @user_name:    hkxadpall.
 * @time:         2020-01-11.
 * @language:     C++.
 * @upload_place: AtCoder.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define P 1000000007

int n;
long long x[100007];
long long fac[100007];
long long ans = 0;

bool clear[100007];

// [1] x [2] y [3]
// 1- x+y
// 2- x+2y
// 2x+3y
// 2 / 2

// [1] x [2] y [3] z [4]
// 1- 
//     2- x+y+z
//     3- x+y+2z
// 2-
//     1- x+2y+z
//     3- x+2y+2z
// 3-
//     1- x+y+2z
//     2- x+2y+3z
// 6x+9y+11z
// 6 / 2 / 3

// [1] x [2] y [3] z [4] a [5]
// 24x+36y+44z+50a
// 24 / 2 / 3 / 4

// [1] x [2] y [3] z [4] a [5] b [6]
// 120x+180y+220z+250a+274b
// 60 40 30 24
// 120 / 2 / 3 / 4 / 5

void dfs(int u)
{
    if (u >= n) return;
    int las = 0;
    for (int i = n; i >= 1; i--) {
        if (!clear[i]) {
            if (las) {
                clear[i] = 1;
                ans = (ans + (x[las] - x[i]) * fac[n - u - 1]) % P;
                dfs(u + 1);
                clear[i] = 0;
            }
            las = i;
        }
    }
}

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) x[i] = read<int>();
    fac[0] = 1;
    for (int i = 1; i <= n; i++) {
        fac[i] = (fac[i - 1] * i) % P;
    }
    dfs(1);
    write(ans);
    return 0;
}