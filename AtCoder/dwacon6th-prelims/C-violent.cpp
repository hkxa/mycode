/*************************************
 * @contest:      dwacon6th-prelims.
 * @user_name:    hkxadpall.
 * @time:         2020-01-11.
 * @language:     C++.
 * @upload_place: AtCoder.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define P 1000000007

int n, k;
int a[20 + 3];
int c[1007];
long long ans, tans;

void dfs(int u)
{
    if (u > k) {
        tans = 1;
        for (int i = 1; i <= n; i++) tans = tans * c[i] % P;
        ans = (ans + tans) % P;
        return;
    }
    int t[1007];
    for (int i = 1; i <= n - a[u]; i++) t[i] = 0;
    for (int i = n - a[u] + 1; i <= n; i++) t[i] = 1;
    do {
        for (int i = 1; i <= n; i++) c[i] += t[i];
        dfs(u + 1);
        for (int i = 1; i <= n; i++) c[i] -= t[i];
    } while (next_permutation(t + 1, t + n + 1));
}

int main()
{
    n = read<int>();
    k = read<int>();
    for (int i = 1; i <= k; i++) a[i] = read<int>();
    dfs(1);
    write(ans);
    return 0;
}

/*
0       0
2       1   1  
12      4   2
48      12  4
160     32  8
480     80  16
1344    192 32
*/

/*
12          0
156         6       26
1224        72      17      9
7560        540     14      3
40500       3240    12.5    1.5
197316      17010   11.6    0.9
*/