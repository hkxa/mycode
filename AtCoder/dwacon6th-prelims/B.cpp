/*************************************
 * @contest:      dwacon6th-prelims.
 * @user_name:    hkxadpall.
 * @time:         2020-01-11.
 * @language:     C++.
 * @upload_place: AtCoder.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define P 1000000007

int n;
long long x[100007];
long long pre[100007], nxt[100007];
long long t, ans;
long long Except(int i) 
{ 
    if (!i) return nxt[1];
    else return pre[i - 1] * nxt[i + 1] % P; 
}

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) x[i] = read<int>();
    pre[0] = 1;
    nxt[n] = 1;
    for (int i = 1; i <= n - 1; i++) pre[i] = (pre[i - 1] * i) % P;
    for (int i = n - 1; i >= 1; i--) nxt[i] = (nxt[i + 1] * i) % P;
    for (int i = 1; i < n; i++) {
        t = (t + Except(i)) % P;
        // printf("t = %lld.\n", t);
        ans = (ans + (x[i + 1] - x[i]) * t) % P;
    }
    write(ans);
    return 0;
}