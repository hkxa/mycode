/*************************************
 * @contest:      dwacon6th-prelims.
 * @user_name:    hkxadpall.
 * @time:         2020-01-11.
 * @language:     C++.
 * @upload_place: AtCoder.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;
int a[100007];
int same[100007];
bool vis[100007]; int p = 1;
int num[100007];

bool tryFill(int u)
{
    if (clock() > 2.0 * CLOCKS_PER_SEC) {
        puts("-1");
        exit(0);
    }
    // printf("TF(%d)\n", u);
    if (u > n) return true;
    // for (int i = 1; i < u - 1; i++) write(num[i], i == u - 2 ? 10 : 32);
    // printf("J");
    while (p <= n && vis[p]) p++;
    int nowp = p;
    // printf("YJ");
    while (nowp <= n) {
        // printf("nowp = %d.\n", nowp);
        if (u > 1 && a[num[u - 1]] == nowp) {
            nowp++;
            while (nowp <= n && vis[nowp]) nowp++;
            continue;
        }
        num[u] = nowp;
        vis[nowp] = true;
        // lasp = p;
        // printf("tuy");
        if (tryFill(u + 1)) return true;
        // printf("ut");
        vis[nowp] = false;
        // p = lasp + 1;
        while (nowp <= n && vis[nowp]) nowp++;
    }
    return false;
}

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        a[i] = read<int>();
    }
    tryFill(1);
    for (int i = 1; i <= n; i++) {
        write(num[i], 32);
    }
    return 0;
}