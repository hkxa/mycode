/*************************************
 * @problem:      CPU���.
 * @user_name:    brealid/hkxadpall/zhaoyi20/j(o)moo/littleTortoise.
 * @time:         2020-10-06.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace File_IO {
    void init_IO(string file_name) {
        freopen((file_name + ".in" ).c_str(), "r", stdin);
        freopen((file_name + ".out").c_str(), "w", stdout);
    }
}

// #define int int64

const int N = 1e5 + 7;

int n, m;
int initial_value[N];
struct Tag {
    int addv, coverv;
    bool covered;
    Tag() : addv(0), coverv(INT_MIN), covered(0) {}
    void clear() {
        addv = covered = 0;
        coverv = INT_MIN;
    }
    void cover(int v) {
        covered = true;
        coverv = v;
    }
    void add(int v) {
        if (covered) coverv += v;
        else addv += v;
    }
    Tag &operator += (const Tag &b) {
        if (covered) {
            if (b.covered) coverv = b.coverv;
            else coverv += b.addv;
        } else {
            addv += b.addv;
            if (b.covered) {
                covered = true;
                coverv = b.coverv;
            }
        }
        return *this;
    }
};

Tag operator + (Tag a, Tag b) {
    return a += b;
}

int getNowVal(int a, Tag b) {
    return b.covered ? b.coverv : a + b.addv;
}

int getHistoryMax(int a, Tag b) {
    return max(a + b.addv, b.coverv);
}

Tag getMaxest(const Tag &pre, const Tag &dif) {
    Tag ret = pre;
    if (ret.covered) {
        ret.coverv = getHistoryMax(ret.coverv, dif);
    } else {
        ret.addv += dif.addv;
        if (dif.covered) {
            ret.covered = true;
            ret.coverv = dif.coverv;
        }
    }
    return ret;
}

void pushTagToHistory(const Tag &a, Tag &b) {
    b.addv = max(a.addv, b.addv);
    if (a.covered) {
        b.covered = true;
        b.coverv = max(a.coverv, b.coverv);
    }
}

struct segtree_node {
    Tag now, history;
    int mx_now, mx_bef;
} tr[N << 2];

void pushdown(int id) {
    segtree_node &u = tr[id], &ls = tr[id << 1], &rs = tr[id << 1 | 1];
    pushTagToHistory(getMaxest(ls.now, u.history), ls.history);
    pushTagToHistory(getMaxest(rs.now, u.history), rs.history);
    ls.mx_bef = max(ls.mx_bef, getHistoryMax(ls.mx_now, u.history));
    rs.mx_bef = max(rs.mx_bef, getHistoryMax(rs.mx_now, u.history));
    ls.now += u.now;
    rs.now += u.now;
    ls.mx_now = getNowVal(ls.mx_now, u.now);
    rs.mx_now = getNowVal(rs.mx_now, u.now);
    u.now.clear();
    u.history.clear();
}

void pushup(int id) {
    segtree_node &u = tr[id], &ls = tr[id << 1], &rs = tr[id << 1 | 1];
    u.mx_now = max(ls.mx_now, rs.mx_now);
    u.mx_bef = max(ls.mx_bef, rs.mx_bef);
}

void build(int u, int l, int r) {
    if (l == r) {
        tr[u].mx_bef = tr[u].mx_now = initial_value[l];
        return;
    }
    int mid = (l + r) >> 1;
    build(u << 1, l, mid);
    build(u << 1 | 1, mid + 1, r);
    pushup(u);
}

void range_cover(int u, int l, int r, int ml, int mr, int cover_value) {
    if (l >= ml && r <= mr) {
        tr[u].now.cover(cover_value);
        tr[u].mx_now = cover_value;
        tr[u].mx_bef = max(tr[u].mx_bef, tr[u].mx_now);
        pushTagToHistory(tr[u].now, tr[u].history);
        return;
    }
    pushdown(u);
    int mid = (l + r) >> 1;
    if (mid >= ml) range_cover(u << 1, l, mid, ml, mr, cover_value);
    if (mid < mr) range_cover(u << 1 | 1, mid + 1, r, ml, mr, cover_value);
    pushup(u);
}

void range_add(int u, int l, int r, int ml, int mr, int add_value) {
    if (l >= ml && r <= mr) {
        tr[u].now.add(add_value);
        tr[u].mx_now += add_value;
        tr[u].mx_bef = max(tr[u].mx_bef, tr[u].mx_now);
        pushTagToHistory(tr[u].now, tr[u].history);
        return;
    }
    pushdown(u);
    int mid = (l + r) >> 1;
    if (mid >= ml) range_add(u << 1, l, mid, ml, mr, add_value);
    if (mid < mr) range_add(u << 1 | 1, mid + 1, r, ml, mr, add_value);
    pushup(u);
}

int query_max_now(int u, int l, int r, int ml, int mr) {
    if (l > mr || r < ml) return INT_MIN;
    if (l >= ml && r <= mr) return tr[u].mx_now;
    pushdown(u);
    int mid = (l + r) >> 1;
    return max(query_max_now(u << 1, l, mid, ml, mr), query_max_now(u << 1 | 1, mid + 1, r, ml, mr));
}

int query_max_bef(int u, int l, int r, int ml, int mr) {
    if (l > mr || r < ml) return INT_MIN;
    if (l >= ml && r <= mr) return tr[u].mx_bef;
    pushdown(u);
    int mid = (l + r) >> 1;
    return max(query_max_bef(u << 1, l, mid, ml, mr), query_max_bef(u << 1 | 1, mid + 1, r, ml, mr));
}

signed main() {
    scanf("%d", &n);
    for (int i = 1; i <= n; ++i) scanf("%d", initial_value + i);
    build(1, 1, n);
    scanf("%d", &m);
    char opt;
    for (int i = 1, l, r, x; i <= m; ++i) {
        scanf("%c", &opt);
        while (!isupper(opt)) scanf("%c", &opt);
        if (opt == 'P' || opt == 'C') scanf("%d%d%d", &l, &r, &x);
        else scanf("%d%d", &l, &r);
        if (opt == 'Q') printf("%d\n", query_max_now(1, 1, n, l, r));
        else if (opt == 'A') printf("%d\n", query_max_bef(1, 1, n, l, r));
        else if (opt == 'P') range_add(1, 1, n, l, r, x);
        else if (opt == 'C') range_cover(1, 1, n, l, r, x);
    }
    return 0;
}