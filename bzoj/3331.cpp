//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      [BeiJing2013]压力.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-11.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

namespace against_cpp11 {
    const int N = 100007;
    int n, m, q;
    vector<int> original_graph[N], G[N * 2];
    int dfn[N], low[N], dft;
    bool instack[N];
    int sta[N], top;
    int cnt[N * 2];
    int square_node_cnt;
    int fa[N * 2][20], dep[N * 2];
    int ans[N];
    void tarjan(int u, int fa) {
        low[u] = dfn[u] = ++dft;
        sta[++top] = u;
        instack[u] = true;
        for (size_t i = 0; i < original_graph[u].size(); i++) {
            int &v = original_graph[u][i];
            // if (v == fa) continue;
            if (!dfn[v]) {
                tarjan(v, u);
                if (dfn[u] <= low[v]) {
                    ++square_node_cnt;
                    do {
                        instack[sta[top]] = false;
                        G[square_node_cnt].push_back(sta[top]);
                        G[sta[top]].push_back(square_node_cnt);
                    } while (sta[top--] != v);
                    G[square_node_cnt].push_back(u);
                    G[u].push_back(square_node_cnt);
                }
                low[u] = min(low[u], low[v]);
            } else if (instack[v] && 1) {
                low[u] = min(low[u], dfn[v]);
            }
        }
    }
    void init_LCA(int u = 1, int f = 0) {
        fa[u][0] = f;
        dep[u] = dep[f] + 1;
        for (int i = 0; i < 19; i++)
            fa[u][i + 1] = fa[fa[u][i]][i];
        for (size_t i = 0; i < G[u].size(); i++) {
            int &v = G[u][i];
            if (v != f) init_LCA(v, u);
        }
    }
    int query_LCA(int u, int v) {
        if (dep[u] < dep[v]) swap(u, v);
        for (int k = 19; dep[u] > dep[v]; k--)
            if (dep[u] - (1 << k) >= dep[v]) u = fa[u][k];
        if (u == v) return u;
        for (int k = 19; fa[u][0] != fa[v][0]; k--)
            if (fa[u][k] != fa[v][k]) {
                u = fa[u][k];
                v = fa[v][k];
            }
        return fa[u][0];
    }
    void solve(int u) {
        for (size_t i = 0; i < G[u].size(); i++) {
            int &v = G[u][i];
            if (v != fa[u][0]) {
                solve(v);
                cnt[u] += cnt[v];
            }
        }
        if (u <= n) ans[u] = cnt[u];
    }
    signed main() {
        read >> n >> m >> q;
        for (int i = 1, u, v; i <= m; i++) {
            read >> u >> v;
            original_graph[u].push_back(v);
            original_graph[v].push_back(u);
        }
        square_node_cnt = n;
        tarjan(1, 0);
        init_LCA();
        for (int i = 1, u, v, g; i <= q; i++) {
            read >> u >> v;
            g = query_LCA(u, v);
            // printf("LCA of (%d, %d) is %d\n", u, v, g);
            cnt[u]++;
            cnt[v]++;
            cnt[g]--;
            cnt[fa[g][0]]--;
        }
        solve(1);
        for (int i = 1; i <= n; i++)
            write << ans[i] << '\n';
        return 0;
    }
}

signed main() { return against_cpp11::main(); }