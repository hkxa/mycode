#include "explore.h"
#include <bits/stdc++.h>
typedef unsigned long long uint64;
using std::make_pair;
using std::vector;
using std::pair;
using std::map;
using std::set;

inline int min(int a, int b) { return a < b ? a : b; }
inline int max(int a, int b) { return a > b ? a : b; }
inline uint64 rand_ull() {
    static uint64 x = (19260817 * time(0)) ^ 20170933;
    x ^= x << 13;
    x ^= x >> 17;
    x ^= x << 5;
    return x ^ ((uint64)rand() * rand() + 1);
}
vector<uint64> val, res;
vector<int> S, CHK;
set<pair<int, int> > e;
map<uint64, int> occured_value;

void check(int u);

void tell_edge(int u, int v) {
    // if (e.count(make_pair(u, v)) || e.count(make_pair(v, u))) return;
    e.insert(make_pair(u, v));
    // printf("%2u. I report <%d, %d>\n", e.size(), u, v);
    res[u - 1] ^= val[v - 1];
    res[v - 1] ^= val[u - 1];
    check(u);
    check(v);
    Report(u, v);
}

void check(int u) {
    // printf("map contains : ");
    // for (map<uint64, int>::iterator it = occured_value.begin(); it != occured_value.end(); it++)
    //     printf("(%llu, %d) ", it->first, it->second);
    // printf("\n");
    // printf("check %d which res is %llu, val is %llu (related : %d)\n", u, res[u - 1], val[u - 1], occured_value.count(res[u - 1]) ? occured_value[res[u - 1]] : -1);
    if (occured_value.count(res[u - 1])) tell_edge(u, occured_value[res[u - 1]]);
}

// void get_rid_of_checked_edge() {
//     for (size_t i = 0; i < e.size(); i++) {
//         res[e[i].first - 1] ^= val[e[i].second - 1];
//         res[e[i].second - 1] ^= val[e[i].first - 1];
//     }
// }

void Solve(int n, int m) {
    srand(time(0));
    val.resize(n);
    for (int i = 1; i <= n; i++) {
        val[i - 1] = rand_ull();
        // printf("val[%d] = %llu\n", i, val[i - 1]);
        occured_value[val[i - 1]] = i;
    }
    int block_size = m / 50;
    for (int l = 1, r; l <= m; l = r + 1) {
        r = min(l + block_size, m);
        // printf("Edge Range %d ~ %d\n", l, r);
        S.clear();
        for (int i = l; i <= r; i++) S.push_back(i);
        res = Query(val, S);
        // get_rid_of_checked_edge();
        bool updated = 1;
        for (int i = 1; i <= n; i++) check(i);
        while (updated) {
            updated = 0;
            CHK.clear();
            for (int i = 1; i <= n; i++) if (res[i - 1]) CHK.push_back(i);
            int cnt = CHK.size();
            if (!cnt) break;
            while (!updated) {
                int u = CHK[rand_ull() % cnt + 1], v = CHK[rand_ull() % cnt + 1];
                if (u == v || !res[u - 1] || !res[v - 1]) continue;
                if (occured_value.count(res[u - 1] ^ val[v - 1]) || occured_value.count(val[u - 1] ^ res[v - 1])) {
                    tell_edge(u, v);
                    updated = true;
                }
            }
        }
    }
}
