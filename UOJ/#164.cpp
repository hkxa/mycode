/*************************************
 * @problem:      V.
 * @user_name:    brealid/hkxadpall/zhaoyi20/j(o)moo/littleTortoise.
 * @time:         2020-10-05.
 * @language:     C++.
 * @fastio_ver:   20200913.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0,    // input
        flush_stdout = 1 << 1,  // output
        flush_stderr = 1 << 2,  // output
    };
    enum number_type_flags {
        output_double_stable = 1 << 0,  // output
        output_double_faster = 1 << 1   // output
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f & ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore character set = {' ', '\r', '\n', '\t'}
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (double &lf) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            lf = endch & 15;
            while (isdigit(endch = getchar())) lf = lf * 10 + (endch & 15);
            if (endch == '.') {
                double digit = 0.1;
                while (isdigit(endch = getchar())) {
                    lf += (endch & 15) * digit;
                    digit *= 0.1;
                }
            }
            if (flag) lf = -lf;
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            Int d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return d;
        }
    } read;

    struct Writer {
        Writer& operator << (io_flags f) {
            if (f & flush_stdout) fflush(stdout);
            if (f & flush_stderr) fflush(stderr);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
        inline void operator () (double val, int eps_digit = 6, char endch = '\0', number_type_flags flg = output_double_faster) {
            if (flg & output_double_stable) {
                static char output_format[10];
                sprintf(output_format, "%%.%dlf", eps_digit);
                printf(output_format, val);
            } else if (flg & output_double_faster) {
                if (val < 0) {
                    putchar('-');
                    val = -val;
                }
                double eps_number = 0.5;
                for (int i = 1; i <= eps_digit; i++) eps_number /= 10;
                val += eps_number;
                (*this) << (int64)val;
                val -= (int64)val;
                if (eps_digit) putchar('.');
                while (eps_digit--) {
                    val *= 10;
                    putchar((int)val | '0');
                    val -= (int)val;
                }
            } else {
               (*this) << '1';
                if (eps_digit) {
                    (*this) << ".E";
                    for (int i = 2; i <= eps_digit; i++) (*this) << '0';
                }
            }
            if (endch) putchar(endch);
        }
    } write;
}
using namespace Fastio;

namespace File_IO {
    void init_IO(const char *file_name) {
        char buff[107];
        sprintf(buff, "%s.in", file_name);
        freopen(buff, "r", stdin);
        sprintf(buff, "%s.out", file_name);
        freopen(buff, "w", stdout);
    }
}

// #define int int64

const int N = 5e5 + 7;
const int64 inf = 1e18 + 7;

int n, m;
int orig[N];

struct mark {
    int64 a, b;
    mark() : a(0), b(-inf) {}
    mark(int64 A, int64 B) : a(A), b(B) {}
    void clear() {
        a = 0; b = -inf;
    }
    friend int64 operator + (int x, mark y) {
        return max(x + y.a, y.b);
    }
    mark update_maxer(mark y) {
        if (y.a > a) a = y.a;
        if (y.b > b) b = y.b;
        return *this;
    }
    mark operator + (mark y) const {
        mark ret;
        if (a == -inf || y.a == -inf) ret.a = -inf;
        else ret.a = a + y.a;
        if (b == -inf && y.b == -inf) ret.b = -inf;
        else ret.b = max(b + y.a, y.b);
        return ret;
    }
    mark& operator += (mark b) {
        return *this = *this + b;
    }
};

mark tr[N << 2], history[N << 2];

#define pushdown()                                  \
do {                                                \
    history[ls].update_maxer(tr[ls] + history[u]);  \
    history[rs].update_maxer(tr[rs] + history[u]);  \
    history[u].clear();                             \
    tr[ls] += tr[u];                                \
    tr[rs] += tr[u];                                \
    tr[u].clear();                                  \
} while (0)

void modify(int u, int l, int r, int ml, int mr, mark dif) {
    if (l >= ml && r <= mr) {
        tr[u] += dif;
        history[u].update_maxer(tr[u]);
        return;
    }
    int mid = (l + r) >> 1, ls = u << 1, rs = ls | 1;
    pushdown();
    if (ml <= mid) modify(ls, l, mid, ml, mr, dif);
    if (mr > mid) modify(rs, mid + 1, r, ml, mr, dif);
}

int access(int u, int l, int r, int pos) {
    if (l == r) return u;
    int mid = (l + r) >> 1, ls = u << 1, rs = ls | 1;
    pushdown();
    if (pos <= mid) return access(ls, l, mid, pos);
    else return access(rs, mid + 1, r, pos);
}

signed main() {
    // File_IO::init_IO("V");
    read >> n >> m;
    for (int i = 1; i <= n; ++i) read >> orig[i];
    for (int i = 1, opt, a, b, c; i <= m; ++i) {
        read >> opt >> a;
        if (opt <= 3) read >> b >> c;
        if (opt == 1) modify(1, 1, n, a, b, mark(c, 0));
        else if (opt == 2) modify(1, 1, n, a, b, mark(-c, 0));
        else if (opt == 3) modify(1, 1, n, a, b, mark(-inf, c));
        else if (opt == 4) write << (orig[a] + tr[access(1, 1, n, a)]) << '\n';
        else write << (orig[a] + history[access(1, 1, n, a)]) << '\n';
        // if (opt == 4 || opt == 5) 
        //     printf("mark : tr = {%lld, %lld} history = {%lld, %lld}\n", 
        //             tr[access(1, 1, n, a)].a, tr[access(1, 1, n, a)].b, 
        //             history[access(1, 1, n, a)].a, history[access(1, 1, n, a)].b);
    }
    return 0;
}