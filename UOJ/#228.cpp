//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      基础数据结构练习题.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-12.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 2e5 + 7;

int n, m;
int a[N];
int64 mx[N << 2], mn[N << 2];
int64 sum[N << 2];
int64 mul[N << 2], add[N << 2];

inline void pushdown(int u, int l, int r) {
    int rl = ((l + r) >> 1) - l + 1, rr = r - ((l + r) >> 1);
    mx[u << 1] = mx[u << 1] * mul[u] + add[u];
    mn[u << 1] = mn[u << 1] * mul[u] + add[u];
    sum[u << 1] = sum[u << 1] * mul[u] + add[u] * rl;
    mx[u << 1 | 1] = mx[u << 1 | 1] * mul[u] + add[u];
    mn[u << 1 | 1] = mn[u << 1 | 1] * mul[u] + add[u];
    sum[u << 1 | 1] = sum[u << 1 | 1] * mul[u] + add[u] * rr;
    if (mul[u]) {
        add[u << 1] += add[u];
        add[u << 1 | 1] += add[u];
    } else {
        mul[u << 1] = mul[u << 1 | 1] = 0;
        add[u << 1] = add[u << 1 | 1] = add[u];
    }
    mul[u] = 1;
    add[u] = 0;
}

inline void pushup(int u) {
    sum[u] = sum[u << 1] + sum[u << 1 | 1];
    mx[u] = max(mx[u << 1], mx[u << 1 | 1]);
    mn[u] = min(mn[u << 1], mn[u << 1 | 1]);
}

void build_tree(int u, int l, int r) {
    mul[u] = 1;
    add[u] = 0;
    if (l == r) return void(sum[u] = mx[u] = mn[u] = a[l]);
    int mid = (l + r) >> 1;
    build_tree(u << 1, l, mid);
    build_tree(u << 1 | 1, mid + 1, r);
    pushup(u);
}

void range_add(int u, int l, int r, int ml, int mr, int diff) {
    // printf("range_add(%d, %d, %d, %d, %d) info {%d, %d, %d, {%d, %d}}\n", u, l, r, ml, mr, sum[u], mx[u], mn[u], mul[u], add[u]);
    if (l > mr || r < ml) return;
    pushdown(u, l, r);
    if (l >= ml && r <= mr) {
        add[u] += diff;
        sum[u] += (int64)diff * (r - l + 1);
        mx[u] += diff;
        mn[u] += diff;
        return;
    }
    int mid = (l + r) >> 1;
    range_add(u << 1, l, mid, ml, mr, diff);
    range_add(u << 1 | 1, mid + 1, r, ml, mr, diff);
    pushup(u);
}

int64 int_sqrt(int64 a) {
    return sqrt(a);
}

void range_sqrt(int u, int l, int r, int ml, int mr) {
    // printf("range_sqrt(%d, %d, %d, %d, %d) info {%d, %d, %d, {%d, %d}}\n", u, l, r, ml, mr, sum[u], mx[u], mn[u], mul[u], add[u]);
    if (l > mr || r < ml) return;
    pushdown(u, l, r);
    if (l >= ml && r <= mr && int_sqrt(mx[u]) == int_sqrt(mn[u])) {
        mul[u] = 0;
        add[u] = mx[u] = mn[u] = int_sqrt(mx[u]);
        sum[u] = mx[u] * (r - l + 1);
        return;
    }
    if (l >= ml && r <= mr && mx[u] - mn[u] == 1) {
        int64 dif = int_sqrt(mx[u]) - mx[u];
        add[u] = dif;
        mx[u] += dif;
        mn[u] += dif;
        sum[u] += dif * (r - l + 1);
        return;
    }
    int mid = (l + r) >> 1;
    range_sqrt(u << 1, l, mid, ml, mr);
    range_sqrt(u << 1 | 1, mid + 1, r, ml, mr);
    pushup(u);
}

int64 range_query(int u, int l, int r, int ml, int mr) {
    // printf("range_query(%d, %d, %d, %d, %d) info {%d, %d, %d, {%d, %d}}\n", u, l, r, ml, mr, sum[u], mx[u], mn[u], mul[u], add[u]);
    if (l > mr || r < ml) return 0;
    if (l >= ml && r <= mr) return sum[u];
    pushdown(u, l, r);
    int mid = (l + r) >> 1;
    return range_query(u << 1, l, mid, ml, mr) + range_query(u << 1 | 1, mid + 1, r, ml, mr);
}

signed main() {
    read >> n >> m;
    for (int i = 1; i <= n; i++) read >> a[i];
    build_tree(1, 1, n);
    for (int i = 1, opt, l, r, v; i <= m; i++) {
        read >> opt >> l >> r;
        if (opt == 1) {
            read >> v;
            range_add(1, 1, n, l, r, v);
        } else if (opt == 2) {
            range_sqrt(1, 1, n, l, r);
        } else {
            write << range_query(1, 1, n, l, r) << '\n';
        }
    }
    return 0;
}