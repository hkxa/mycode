# 【1008 - 2021年第8周练习题】A. 字母统计
# ord(x) 表示获取字符 c 的 ASCII 值(c 类型应为 str)

def IsUpperLetter(x):                               # 是否为大写字母
    return ord(x) >= ord('A') and ord(x) <= ord('Z')

def IsLowerLetter(x):                               # 是否为小写字母
    return ord(x) >= ord('a') and ord(x) <= ord('z')

def IsLetter(x):                                    # 是否为字母
    return IsUpperLetter(x) or IsLowerLetter(x)

Count = {}                                          # 类似单词统计，新建 dict
for character in input():
    if IsLetter(character):
        if character in Count:                      # 这 4 行跟之前的题目很像
            Count[character] += 1
        else:
            Count[character] = 1

for character in sorted(Count.keys()):
    print('%c:%d'%(character, Count[character]))    # %c 表示单个字符，这里自然用 %c 和 %s 都是可以的