y=int(input())
if y <= 0 or y >= 10000:
    print("输入有误")
elif (y % 4 == 0) and (y % 100 != 0 or y % 400 == 0):
    print("%d年是闰年"%y)
else:
    print("%d年不是闰年"%y)