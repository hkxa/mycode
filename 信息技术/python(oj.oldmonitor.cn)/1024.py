# 【1012 - 2021年第14周练习题】A. 斐波那契倒序输出
n = int(input())
fib = [1, 1]
for i in range(2, n):
    fib.append(fib[-1] + fib[-2])
for num in reversed(fib):
    print(num, end = ' ')

