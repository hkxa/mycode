# 【1010 - 2021年第9周练习题】A. 「英语」动词过去式
YuanYin = "aeiou"

word = input()
if len(word) >= 3 and word[-3] not in YuanYin and word[-2] in YuanYin and word[-1] not in YuanYin:
    print(word + word[-1] + "ed") # 辅元辅
elif len(word) >= 2 and word[-2] not in YuanYin and word[-1] == 'y':
    print(word[:-1] + "ied") # 改 y 为 i 加 ed
elif word[-1] == 'e':
    print(word + 'd') # 直接加 d
else:
    print(word + "ed") # 直接加 ed