# 【1008 - 2021年第8周练习题】B. 进制转换：十进制转换成R进制

def DanGeShuZi_ZhuanHuanCheng_ZiFu(x):
    if x < 10:
        return str(x)
    else:
        return chr(x - 10 + ord('A'))       # 利用 ASCII 码进行操作

def ZhuanHuan(x, R):
    if x < R:                               # 一位数，直接转换
        return DanGeShuZi_ZhuanHuanCheng_ZiFu(x)
    return ZhuanHuan(x // R, R) + DanGeShuZi_ZhuanHuanCheng_ZiFu(x % R)  # 递归转换

n, R = map(int, input().split())
print(ZhuanHuan(n, R))
