a_list = map(int, input().split())  # 读入并转换为数字存储(此时 a_list 为 map 类型)
a_list = list(a_list)               # 转换为 list 类型以便操作
a_list.sort()                       # 排序
del a_list[0], a_list[-1]           # 删去最小的和最大的（此时在列表的第一位和最后一位）
print(a_list)                       # 输出