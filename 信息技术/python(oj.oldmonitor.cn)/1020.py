# 【1011 - 2021年第11周练习题】A. 寻找质因子

n = int(input())
for i in range(2, n + 1):
    if n % i == 0:
        print(i, end = ' ')
        while n % i == 0: # 防止筛到非质数的因子，于是去掉这个质因子
            n //= i