# 【1010 - 2021年第9周练习题】B. 求s=a+aa+aa...a的值

def Make_Repeat_Number(a, n): # 返回的是数字 a 重复 n 次的结果
    result = 0
    for i in range(n):
        result += a * 10**i
    return result

a = int(input())
result = 0
for i in range(1, a + 1):
    result += Make_Repeat_Number(a, i) # 累加: a 重复 1~a 次得到的数字的和
print(result)