# 【1011 - 2021年第11周练习题】B. 温度转换

a, b = map(int, input().split())
if a > b:
    a, b = b, a
for i in range(a, b + 1):
    degree = (i - 32) / 1.8
    print('%d->%.2lf'%(i, degree))