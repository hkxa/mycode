x = input()                     # 此时 x 是 str 类型，可以像 list 一样访问
ans = 0                         # ans 存储答案
n = len(x)                      # 输入的二进制数的位数
for t in range(len(x)):         # 从最高位到最低位枚举
    if x[t] == '1':             # (最低位为第 0 位)如果第 n - t - 1 位为 1
        ans += 2 ** (n - t - 1) # 通过简单数学计算可知正确性
print(ans)                      # 输出