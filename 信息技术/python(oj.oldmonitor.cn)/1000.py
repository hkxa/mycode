n = int(input())
x = 2
PrefixCharacter = '='
print(n, end = '')
while x * x <= n:
    while n % x == 0:
        print('%s%d'%(PrefixCharacter, x), end = '')
        PrefixCharacter = '*'
        n /= x
    x += 1
if n != 1:
    print('%s%d'%(PrefixCharacter, n), end = '')