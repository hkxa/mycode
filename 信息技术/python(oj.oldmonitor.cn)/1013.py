def Test_If_Have_7(x):
    turned_to_str = str(x)
    return '7' in turned_to_str # 是否存在 7

n = int(input())
count = 0
for x in range(1, n + 1):
    if x % 3 == 0 and Test_If_Have_7(x):
        print(x, end = ' ')
        count += 1
if count == 0:
    print('None', end = '') # 无解
print() # 换行
print(count)