def is_sqr(s):
    return int(s ** .5) ** 2 == s
 
for i in range(0, int(input()) + 1):
    if is_sqr(i + 100) and is_sqr(i + 268):
        print(i)