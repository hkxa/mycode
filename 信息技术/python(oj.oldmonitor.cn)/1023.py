# 【1012 - 2021年第12周练习题】B. 素数在素数表中的位置

def isPrime(n):
    if n <= 1:
        return False
    for i in range(2, int(n ** .5) + 1):
        if n % i == 0:
            return False
    return True

n = int(input())
if isPrime(n):
    cnt = 0
    for i in range(n + 1, 10000):
        if isPrime(i):
            cnt += 1  # 统计有多少个大于 n 的质数
    print(cnt + 1)    # 题目本质：大于 n 的质数的个数 + 1
else:
    print('None')