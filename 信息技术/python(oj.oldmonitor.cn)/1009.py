def GetAnswer(Number):
    if Number == 0:
        return ''
    elif Number % 2 == 0:
        return GetAnswer(Number // 2) + '0'
    else:
        return GetAnswer(Number // 2) + '1'

n = int(input())
print(GetAnswer(n))