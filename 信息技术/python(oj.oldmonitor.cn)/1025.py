# 【1012 - 2021年第14周练习题】B. 龟爷爷过生日
def rn(y):
    return (y % 100 != 0 and y % 4 == 0) or (y % 400 == 0)

y, m, d = map(int, input().split())
n = int(input())
add = int((rn(y) and m <= 2) or (rn(y + n) and m > 2))
for yd in range(n - 1):
    add += int(rn(y + yd + 1))
print(add + 1 + n * 365)
