def get_1(n):
    n = n[::-1]
    return sum(map(int, n[1::2])) * 3

def get_2(n):
    n = n[::-1]
    return sum(map(int, n[2::2]))

def get_3(n):
    return (get_1(n) + get_2(n)) % 10

def get_4(n):
    return (10 - get_3(n)) % 10

n = input()
if len(n) != 13:
    print("Error")
elif get_4(n) == int(n[-1]):
    print("Yes")
else:
    print("No")

