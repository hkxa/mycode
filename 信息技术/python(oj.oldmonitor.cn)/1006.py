a, b, c = sorted(map(float, input().split()))
if c <= 0 or a + b <= c:
    print('不能构成三角形')
else:
    p = (a + b + c) * .5
    S = (p * (p - a) * (p - b) * (p - c)) ** .5
    print('%.3f'%S)
