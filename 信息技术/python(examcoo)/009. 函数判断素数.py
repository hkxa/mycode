# Powered by 2011150, all rights reserved
def isprime(x: int):                        # 定义函数
    if x == 1:                              # 特判 1
        return False
    for i in range(2, int(x ** 0.5) + 1):   # 从 2 到 √n 枚举
        if x % i == 0:                      # 有 2~n-1 的因子
            return False
    return True                             # 否则返回'真'

n = int(input("请输入一个整数:"))
count = 0
for i in range(1, n + 1):                   # 范围内枚举
    if isprime(i):
        print(i, end = ' ')                 # 满足条件就输出
        count = count + 1                   # 答案计数
print("\n%d以内共有%d个素数"%(n, count))    # 输出结果