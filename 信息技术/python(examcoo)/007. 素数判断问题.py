def is_prime(x: int) -> bool:                # 定义函数
    if x == 1:                              # 特判 1
        return False
    for i in range(2, int(x ** 0.5) + 1):   # 从 2 到 √n 枚举
        if x % i == 0:                      # 有 2~n-1 的因子
            return False
    return True                             # 否则返回'真'

n = int(input('输入一个整数: '))
if is_prime(n):
    print(n, "是素数")
else:
    print(n, "不是素数")
