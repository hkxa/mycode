FangAnShu = 0
for XiaoJi in range(0, 100 + 1, 3):     # 显然，小鸡的只数只可能是 3 的倍数，且在 0~100 之间
    for GongJi in range(0, 100 + 1):    # 由于 python 是左开右闭的，所以应该写 100 + 1 即：[0, 100 + 1)
        MuJi = 100 - XiaoJi - GongJi    # 条件：母鸡买了（大于零）钱数为 100
        if MuJi >= 0 and XiaoJi // 3 + GongJi * 3 + MuJi * 5 == 100:
            FangAnShu = FangAnShu + 1   # 方案数加一
            print("方案", FangAnShu, ": 公鸡", GongJi, "只, 母鸡", MuJi, "只, 小鸡", XiaoJi, "只") # 输出