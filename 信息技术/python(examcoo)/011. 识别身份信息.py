weekdays = ['日', '一', '二', '三', '四', '五', '六']

def get_date(x: str) -> (int, int, int):
    year = int(x[:4])
    month = int(x[4:6])
    day = int(x[6:8])
    if month >= 11:
        year -= 1
        month += 12
    return year, month, day

# 1 for male, 2 for female
def get_sex(x: str) -> bool:
    return '男' if int(x) % 2 == 1 else '女'

def get_wk(y, m, d) -> str:
    c = y // 100
    y %= 100
    wk = (c // 4 - 2 * c + y + y // 4 + (13 * m + 13) // 5 + d - 1) % 7
    return weekdays[wk]

def get_date_info(x: str) -> str:
    y, m, d = get_date(x)
    wk = get_wk(y, m, d)
    return '%d 年 %d 月 %d 日, 星期%s'%(y, m, d, wk)

Id = input('请输入身份证号: ')
if len(Id) != 18:
    print('身份证号不合法!')
else:
    # date_info = get_date_info(Id[6:14])
    # sex = get_sex(Id[16])
    # print('您的生日是: %s\n您的性别是: %s\n'%(date_info, sex))
    y, m, d = get_date(Id[6:14])
    wk = get_wk(y, m, d)
    sex = get_sex(Id[16])
    print('您的出生日期为：%d年%d月%d日，性别为%s，星期%s\n'%(y, m, d, sex, wk))