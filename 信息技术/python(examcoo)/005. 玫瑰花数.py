print('Rose number(s):', end = ' ')
cnt = 0                     # 统计个数
for i in range(1000, 10000):
    number = i
    a = number % 10
    number //= 10
    b = number % 10
    number //= 10
    c = number % 10
    number //= 10
    d = number
    if i == a**4 + b**4 + c**4 + d**4:
        print(i, end = ' ') # 输出玫瑰花数
        cnt += 1            # 统计个数
print()                     # 换行
print('Total Count:', cnt)  # 输出个数
