_year__ChineseEra_HeavenlyStem = '甲乙丙丁戊己庚辛壬癸'
_year__ChineseEra_TerrestrialBranch = '子丑寅卯辰巳午未申酉戌亥'
_year__ChineseEra_YearAnimal = '鼠牛虎兔龙蛇马羊猴鸡狗猪'

class year:
    def __init__(self, y):
        self.y = y
    
    def get_ChineseEra(self):
        a = (self.y + 6) % 10
        b = (self.y + 8) % 12
        return __ChineseEra_HeavenlyStem[a] + __ChineseEra_TerrestrialBranch[b]
    
    def get_YearAnimal(self):
        a = (self.y + 8) % 12
        return __ChineseEra_YearAnimal[a]

starting_year = int(input('请输入起始年份: '))
print('年份    天干地支    生肖')
for YearId in range(starting_year, starting_year + 12):
    now = year(YearId)
    ChineseEra = now.get_ChineseEra()
    YearAnimal = now.get_YearAnimal()
    print('%d年    %s       %s'%(YearId, ChineseEra, YearAnimal))