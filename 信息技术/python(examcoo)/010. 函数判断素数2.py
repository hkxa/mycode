def is_prime(x: int) -> bool:                # 定义函数
    if x == 1:                              # 特判 1
        return False
    for i in range(2, int(x ** 0.5) + 1):   # 从 2 到 √n 枚举
        if x % i == 0:                      # 有 2~n-1 的因子
            return False
    return True                             # 否则返回'真'

def is_huiwen(n: int) -> bool:
    x = str(n)
    i, j = 0, len(x) - 1
    while i < j:
        if x[i] != x[j]:
            return False
        i += 1
        j -= 1
    return True

l, r = [int(x) for x in input("请输入两个整数，表示范围: ").split()]
count = 0
for i in range(l, r + 1):                   # 范围内枚举
    if is_prime(i) and is_huiwen(i):
        print(i, end = ' ')                 # 满足条件就输出
        count = count + 1                   # 答案计数
print("\n%d ~ %d 以内共有 %d 个回文素数"%(l, r, count))    # 输出结果