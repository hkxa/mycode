a = int(input("Input a:"))
b = int(input("Input b:"))
c = int(input("Input c:"))
delta = b**2 - 4 * a * c
if (a == 0):
    print("输入非一元二次方程")
else:
    equation = str(a) + "x^2+" + str(b) + "x+" + str(c) + "=0"
    if delta < 0:
        print("方程", equation, "无实数解")
    elif delta == 0:
        print("方程", equation, "有两个相等实数根 x1 = x2 =", -b / (2 * a))
    else:
        x1 = -(b - delta**0.5) / (2 * a)
        x2 = -(b + delta**0.5) / (2 * a)
        print("方程", equation, "有两个不等实数根 x1 = %.2lf, x2 = %.2lf"%(x1, x2))