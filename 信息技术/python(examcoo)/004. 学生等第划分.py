# By 2011150 赵奕
while True:
    a = input("请输入学生分数: ")
    if a == "quit": 
        break
    a = int(a)
    if a > 100 or a < 0:
        print("分数无效")
    elif a >= 90:
        print("A等")
    elif a >= 80:
        print("B等")
    elif a >= 70:
        print("C等")
    elif a >= 60:
        print("D等")
    else:
        print("E等")