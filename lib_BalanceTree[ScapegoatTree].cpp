// N need to be defined
template<typename T>
class BalanceTree {
  private:  
    struct ScapegoatNode {
        T v;
        int tot, siz, l, r;
    } t[N * 2];
    int root;
    int temp[N * 2];
    int TempCnt, NodeCnt;

    bool Balance(int x) {
        return t[t[x].l].siz < t[x].siz * 0.75 && 
               t[t[x].r].siz < t[x].siz * 0.75;
    }
    void Flatten(int x) {
        if (!x) return;
        Flatten(t[x].l);
        if (t[x].tot) temp[++TempCnt] = x;
        Flatten(t[x].r);
    }
    void Do_ReBuild(int &x, int l, int r) {
        if (l > r) return void(x = 0);
        if (l == r) {
            x = temp[l];
            t[x].l = 0;
            t[x].r = 0;
            t[x].siz = t[x].tot;
            return;
        }
        int mid = (l + r) >> 1;
        x = temp[mid];
        Do_ReBuild(t[x].l, l, mid - 1);
        Do_ReBuild(t[x].r, mid + 1, r);
        t[x].siz = t[t[x].l].siz + t[t[x].r].siz + t[x].tot;
    }
    void ReBuild(int &x) {
        TempCnt = 0;
        Flatten(x);
        Do_ReBuild(x, 1, TempCnt);
    }
    void Check(int &a) {
        if (!a) return;
        if (!Balance(a)) ReBuild(a);
        else if (t[t[a].l].siz > t[t[a].r].siz) Check(t[a].l);
        else Check(t[a].r);
    }
    void Insert(int &a, T v) {
        if (!a) {
            a = ++NodeCnt;
            t[a].v = v;
            t[a].tot = 1;
        } else if (v == t[a].v) {
            t[a].tot++;
        } else if (v < t[a].v) {
            Insert(t[a].l, v);
        } else {
            Insert(t[a].r, v);
        }
        t[a].siz++;
    }
    bool Delete(int a, T v) {
        if (!a) return false;
        bool succeed = false;
        if (v == t[a].v) {
            if (t[a].tot) {
                --t[a].tot;
                succeed = true;
            }
        } else if (v < t[a].v) {
            if (Delete(t[a].l, v)) succeed = true;
        } else {
            if (Delete(t[a].r, v)) succeed = true;
        }
        if (succeed) --t[a].siz;
        return succeed;
    }
    T GetVal(int rank) {
        int a = root;
        while (true) {
            if (t[t[a].l].siz < rank && t[t[a].l].siz + t[a].tot >= rank) return t[a].v;
            else if (t[t[a].l].siz >= rank) a = t[a].l;
            else rank -= t[t[a].l].siz + t[a].tot, a = t[a].r;
        }
    }
    int GetRank_lower(T val) {
        int a = root, rank = 1;
        while (a) {
            if (t[a].v >= val) a = t[a].l;     
            else rank += t[t[a].l].siz + t[a].tot, a = t[a].r;     
        }
        return rank;
    }
    int GetRank_upper(T val) {
        int a = root, rank = 1;
        while (a) {
            if (t[a].v > val) a = t[a].l;     
            else rank += t[t[a].l].siz + t[a].tot, a = t[a].r;     
        }
        return rank;
    }
  public:
    void insert(T val) {
        Insert(root, val);
        Check(root);
    }
    bool remove(T val) {
        if (Delete(root, val)) {
            Check(root);
            return true;
        } else return false;
    }
    void lower_bound(T val) {
        return GetVal(GetRank_lower(val));
    }
    void upper_bound(T val) {
        return GetVal(GetRank_upper(val));
    }
    int count_less(T val) {
        return GetRank_lower(val) - 1;
    }
    int count_greater(T val) {
        return t[root].siz - GetRank_upper(val) + 1;
    }
};