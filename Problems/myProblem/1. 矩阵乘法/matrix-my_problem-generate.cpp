#include <bits/stdc++.h>
#include "eval.hpp"
using namespace std;

long long dataNum[21] = {
    0, 
    1, 31, 89, 276, 673, 1e3, // 30 pts (1e3)
    7889, 33482, 353249, 5e5, // 50 pts (5e5)
    823459, 2238476, 6843246, 1e7, // 70 pts (1e7)
    6e8, 5e10, 4e12, 3e14, 2e16, 1e18 // 100 pts (1e18)
};

int main()
{
    // 30 pts
    printf("gererate data.\n\n");
    printf("loading..."); 
    for (int i = 0; i <= 40000000; i++);
    for (int i = 1; i <= 20; i++) {
        eval td;
        td.open("matrix-my_problem-data\\recursion", i);
        td << dataNum[i];
        td.generate("std-recursion.exe");
        printf("\rgenerating data #%d...", i);
        for (int i = 0; i <= 40000000; i++);
    }
    return 0;
}