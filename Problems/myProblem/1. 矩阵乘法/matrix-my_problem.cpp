/*************************************
 * problem:      Strange Pro.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-05-11.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

#define MATRIX_SIZE 3
/**
 * class matrix "矩阵"类
 * 所有类型为该类的变量均为 MATRIX_SIZE * MATRIX_SIZE 的矩阵
 * 提供：构造函数，构造单位矩阵函数，重定义乘法函数，设置函数，获取函数
 */
struct matrix {
    long long a[MATRIX_SIZE + 2][MATRIX_SIZE + 2];

    /**
     * matrix::matrix() 构造函数
     * 初始化，构造 "0" 矩阵
     */
    matrix() 
    {
        memset(a, 0, sizeof(a));
    }

    /**
     * matrix::setBaseMatrix() 初始化，构造单位矩阵
     * example (MATRIX_SIZE = 3):
     * {{1, 0, 0},
     * {0, 1, 0},
     * {0, 0, 1}}
     */
    void setBaseMatrix() 
    {
        memset(a, 0, sizeof(a));
        for (int i = 1; i <= MATRIX_SIZE; i++) {
            a[i][i] = 1LL;
        }
    }

    /**
     * matrix::operator*() 重定义乘法
     * 矩阵乘法
     */
    matrix operator * (matrix ter) 
    {
        matrix res;
        for (int i = 1; i <= MATRIX_SIZE; i++) {
            for (int k = 1; k <= MATRIX_SIZE; k++) {
                for (int j = 1; j <= MATRIX_SIZE; j++) {
                    res.a[i][j] += a[i][k] * ter.a[k][j];
                }
            }
        }
        for (int i = 1; i <= MATRIX_SIZE; i++) {
            for (int j = 1; j <= MATRIX_SIZE; j++) {
                res.a[i][j] %= 19260817LL;
            }
        }
        return res;
    }

    /**
     * matrix::set() 设置函数
     * 使用传入的数组初始化矩阵
     */
    void set(long long setMatrix[MATRIX_SIZE + 2][MATRIX_SIZE + 2])
    {
        for (int i = 1; i <= MATRIX_SIZE; i++) {
            for (int j = 1; j <= MATRIX_SIZE; j++) {
                a[i][j] = setMatrix[i][j];
            }
        }
    }

    /**
     * matrix::get() 获取函数
     * 获取矩阵的 (i, j) 位置的值
     */
    long long get(long long i, long long j)
    {
        return a[i][j];
    }
} a;

long long recursion_way1[5][5] = {
    {0, 0, 0, 0, 0}, 
    {0, 1, 0, 5, 0}, 
    {0, 1, 0, 0, 0}, 
    {0, 0, 0, 5, 0}, 
    {0, 0, 0, 0, 0}, 
};


long long recursion_way2[5][5] = {
    {0, 0, 0, 0, 0}, 
    {0, 5, 0, 1, 0}, 
    {0, 1, 0, 0, 0}, 
    {0, 0, 0, 1, 0}, 
    {0, 0, 0, 0, 0}, 
};

matrix fpow(matrix a, long long p)
{
    matrix res;
    res.setBaseMatrix();
    while (p) {
        if (p & 1) res = res * a;
        a = a * a;
        p >>= 1;
    }
    return res;
}

int main()
{
    long long n = read<long long>() - 1;
    a.set(recursion_way1);
    // a.set(recursion_way2);
    matrix ans = fpow(a, n);
    // for (int i = 1; i <= MATRIX_SIZE; i++) {
    //     for (int j = 1; j <= MATRIX_SIZE; j++) {
    //         write(ans.get(i, j), j == MATRIX_SIZE ? 10 : 32);
    //     }
    // }
    write(ans.get(1, 1) + ans.get(1, 3));
    return 0;
}