#include <bits/stdc++.h>
using namespace std;

const int N = 1e5 + 7;
int n, m, fa[N];
int cnt[N]; // 每个结点的孩子个数 

int main() {
	cin >> n >> m;
	int x, y;
	for (int i = 1; i <= m; ++i) {
		cin >> x >> y;
		fa[y] = x;
	}
	// 操作 1: 寻找树根
	for (int i = 1; i <= n; ++i)
		if (!fa[i]) cout << i << endl;
	// 操作 2: 寻找孩子最多的结点
	for (int i = 1; i <= n; ++i)
		if (fa[i]) ++cnt[fa[i]];
	int answer = 1;
	for (int i = 2; i <= n; ++i)
		if (cnt[i] > cnt[answer]) answer = i;
	cout << answer << endl;
	// 操作 3: 输出该节点的所有孩子
	for (int i = 1; i <= n; ++i)
		if (fa[i] == answer) cout << i << ' ';
	cout << endl; 
	return 0;
}