import random, os

def make_in(n, fin):
    with open(fin, 'w') as f:
        edges = []
        root = random.randint(1, n)
        for i in range(1, n + 1):
            if i != root:
                fa = random.randint(1, n)
                while fa == i:
                    fa = random.randint(1, n)
                edges.append((fa, i))
        random.shuffle(edges)
        print(n, n - 1, file = f)
        for e in edges:
            print(*e, file = f)

def make_out(fin, fout, std):
    os.system(f'.\{std} <{fin} >{fout}')

os.system('g++ std.cpp -o std.exe')
std = 'std.exe'
for i in range(1, 4):
    fin = f'data{i}.in'
    fout = f'data{i}.out'
    make_in(100, fin)
    make_out(fin, fout, std)
for i in range(4, 11):
    fin = f'data{i}.in'
    fout = f'data{i}.out'
    make_in(100000, fin)
    make_out(fin, fout, std)