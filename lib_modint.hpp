template <int ModNum>
struct mint {
    int num;
    mint() : num(0) {}
    mint(int val) : num((val % ModNum + ModNum) % ModNum) {}
    mint(int64 val) : num((val % ModNum + ModNum) % ModNum) {}
    operator int() const { return num; }
    operator bool() const { return num; }
    mint pow(int K) const {
        mint ret = 1, now = *this;
        while (K) {
            if (K & 1) ret *= now;
            now *= now;
            K >>= 1;
        }
        return ret;
    }
    mint inv() const { return this->pow(ModNum - 2); }
    mint& operator ++ () { if (++num >= ModNum) num -= ModNum; return *this; }
    mint operator ++ (int) { mint ret = *this; ++*this; return ret; }
    mint& operator -- () { if (--num < 0) num += ModNum; return *this; }
    mint operator -- (int) { mint ret = *this; --*this; return ret; }
    mint operator - () const { return mint(-num); }
    mint operator << (const int &b) const { return mint((int64)num << b); }
    mint operator >> (const int &b) const { return mint(num >> b); }
    mint operator + (const mint &b) const { return mint(num + b.num); }
    mint operator - (const mint &b) const { return mint(num - b.num); }
    mint operator * (const mint &b) const { return mint((int64)num * b.num); }
    mint operator / (const mint &b) const { return *this * b.inv(); }
    mint operator % (const mint &b) const { return mint(num % b.num); }
    mint operator + (const int &b) const { return *this + mint(b); }
    mint operator - (const int &b) const { return *this - mint(b); }
    mint operator * (const int &b) const { return *this * mint(b); }
    mint operator / (const int &b) const { return *this / mint(b); }
    mint operator % (const int &b) const { return *this % mint(b); }
    mint& operator += (const mint &b) { return *this = *this + b; }
    mint& operator -= (const mint &b) { return *this = *this - b; }
    mint& operator *= (const mint &b) { return *this = *this * b; }
    mint& operator /= (const mint &b) { return *this = *this / b; }
    mint& operator %= (const mint &b) { return *this = *this % b; }
    friend mint operator + (const int &a, const mint &b) { return mint(a) + b; }
    friend mint operator - (const int &a, const mint &b) { return mint(a) - b; }
    friend mint operator * (const int &a, const mint &b) { return mint(a) * b; }
    friend mint operator / (const int &a, const mint &b) { return mint(a) / b; }
    friend mint operator % (const int &a, const mint &b) { return mint(a) % b; }
    bool operator < (const mint &b) { return num < b.num; }
    bool operator > (const mint &b) { return num > b.num; }
    bool operator <= (const mint &b) { return num <= b.num; }
    bool operator >= (const mint &b) { return num >= b.num; }
    bool operator == (const mint &b) { return num == b.num; }
    bool operator != (const mint &b) { return num != b.num; }
    bool operator < (const int &b) { return num < b; }
    bool operator > (const int &b) { return num > b; }
    bool operator <= (const int &b) { return num <= b; }
    bool operator >= (const int &b) { return num >= b; }
    bool operator == (const int &b) { return num == b; }
    bool operator != (const int &b) { return num != b; }
    friend bool operator < (const int &a, const mint &b) { return a < b.num; }
    friend bool operator > (const int &a, const mint &b) { return a > b.num; }
    friend bool operator <= (const int &a, const mint &b) { return a <= b.num; }
    friend bool operator >= (const int &a, const mint &b) { return a >= b.num; }
    friend bool operator == (const int &a, const mint &b) { return a == b.num; }
    friend bool operator != (const int &a, const mint &b) { return a != b.num; }
};