/*************************************
 * @problem:      wqs_example maximum k period sum.
 * @user_name:    brealid/hkxadpall/zhaoyi20/j(o)moo/littleTortoise.
 * @time:         2020--.
 * @language:     C++.
 * @fastio_ver:   20200827.
*************************************/ 
// k 段子段和最大

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        // simple io flags
        ignore_int = 1 << 0,    // input
        char_enter = 1 << 1,    // output
        flush_stdout = 1 << 2,  // output
        flush_stderr = 1 << 3,  // output
        // combination io flags
        endline = char_enter | flush_stdout // output
    };
    enum number_type_flags {
        // simple io flags
        output_double_stable = 1 << 0,  // output
        output_double_faster = 1 << 1   // output
        // combination io flags
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f & ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore character set : ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (double &lf) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            lf = endch & 15;
            while (isdigit(endch = getchar())) lf = lf * 10 + (endch & 15);
            if (endch == '.') {
                double digit = 0.1;
                while (isdigit(endch = getchar())) {
                    lf += (endch & 15) * digit;
                    digit *= 0.1;
                }
            }
            if (flag) lf = -lf;
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            Int d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return d;
        }
    } read;

    struct Writer {
        Writer& operator << (io_flags f) {
            if (f & char_enter) putchar(10);
            if (f & flush_stdout) fflush(stdout);
            if (f & flush_stderr) fflush(stderr);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
        inline void operator () (double val, int eps_digit = 6, char endch = '\0', number_type_flags flg = output_double_faster) {
            if (flg & output_double_stable) {
                static char output_format[10];
                sprintf(output_format, "%%.%dlf", eps_digit);
                printf(output_format, val);
            } else if (flg & output_double_faster) {
                if (val < 0) {
                    putchar('-');
                    val = -val;
                }
                (*this) << (int64)val;
                val -= (int64)val;
                putchar('.');
                double eps_number = 0.5;
                for (int i = 1; i <= eps_digit; i++) eps_number /= 10;
                val += eps_number;
                while (eps_digit--) {
                    val *= 10;
                    putchar((int)val | '0');
                    val -= (int)val;
                }
            } else {
                (*this) << "1.#ERROR_NOT_IDENTIFIED_FLAG";
            }
            if (endch) putchar(endch);
        }
    } write;
}
using namespace Fastio;
namespace File_IO {
    void init_IO() {
        freopen("wqs_example maximum k period sum.in", "r", stdin);
        freopen("wqs_example maximum k period sum.out", "w", stdout);
    }
}

// #define int int64

const int N = 1e6 + 7;

pair<int64, int> operator + (pair<int64, int> a, pair<int64, int> b) {
    return make_pair(a.first + b.first, a.second + b.second);
}

pair<int64, int> operator + (pair<int64, int> a, int b) {
    return make_pair(a.first + b, a.second);
}

pair<int64, int> dp(int n, int64 q, int *a) {
    static pair<int64, int> f[N][2];
    f[0][0] = make_pair(0ll, 0);
    f[0][1] = make_pair((int64)INT_MIN, 0);
    pair<int64, int> dif = make_pair(q, 1);
    for (int i = 1; i <= n; i++) {
        f[i][0] = max(f[i - 1][0], f[i - 1][1]);
        f[i][1] = max(f[i - 1][0] + dif, f[i - 1][1]) + a[i];
    }
    return max(f[n][0], f[n][1]);
}

int64 wqs_solve(int n, int k, int *a) {
    int64 l = -1e9, r = 1e9, mid, ans;
    while (l <= r) {
        mid = (l + r) >> 1;
        if (dp(n, mid, a).second >= k) r = mid - 1, ans = mid;
        else l = mid + 1;
    }
    // printf("when q = %d\n", ans);
    return dp(n, ans, a).first - k * ans;
}

signed main() {
    // File_IO::init_IO();
    int n, *a;
    read >> n;
    a = new int[n + 1];
    for (int i = 1; i <= n; i++) read >> a[i];
    write << wqs_solve(n, 3, a) << '\n';
    return 0;
}