/*************************************
 * @problem:      blockCount.
 * @time:         2020-02-14.
 * @language:     C++.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define P 998244353
#define pow2(x) ((x) * (x) % P)

int n, m;
char mapIn[107];
bool block[107][107];
int f[107][107], g[107][107];
int ans;

int main()
{
    n = read<int>();
    m = read<int>();
    // 保证 (n+m+1)|2
    for (int i = 1; i <= n; i++) {
        scanf("%s", mapIn + 1);
        for (int j = 1; j <= m; j++)
            if (mapIn[j] == '+') block[i][j] = true;
    }
    // for (int i = 1; i <= n; i++) 
    //     for (int j = 1; j <= m; j++)
    //         printf("%c%c", " +"[block[i][j]], " \n"[j == m]);
    f[n][0] = g[1][m + 1] = 1;
    block[n][0] = block[1][m + 1] = true;
    for (int i = n; i >= 1; i--)
        for (int j = 1; i - j >= (n - m) / 2 && j <= m; j++) 
            if (block[i][j]) {
                if (block[i + 1][j]) f[i][j] = (f[i][j] + f[i + 1][j]) % P;
                if (block[i][j - 1]) f[i][j] = (f[i][j] + f[i][j - 1]) % P;
            }
    for (int i = 1; i <= n; i++)
        for (int j = n; i - j <= (n - m) / 2 && j >= 1; j--) 
            if (block[i][j]) {
                if (block[i - 1][j]) g[i][j] = (g[i][j] + g[i - 1][j]) % P;
                if (block[i][j + 1]) g[i][j] = (g[i][j] + g[i][j + 1]) % P;
            }
    // for (int i = 1; i <= n; i++) 
    //     for (int j = 1; j <= m; j++)
    //         if (i - j == (n - m) / 2) printf("[%-2d %2d]%c", f[i][j], g[i][j]," \n"[j == m]);
    //         else 
    //             if (max(f[i][j], g[i][j]))
    //                 printf("%-2d%c", max(f[i][j], g[i][j])," \n"[j == m]);
    //             else 
    //                 printf("  %c"," \n"[j == m]);
    for (int i = n, j = (n + m) / 2; i >= 1 && j >= 1; i--, j--) {
        ans = (ans + pow2((int64)f[i][min(j, m)] * g[i][min(j, m)] % P)) % P;
    }
    write(ans, 10);
    /*
6 6
````++
``++++
++++++
++++++
++++``
++++``
    */
    return 0;
}