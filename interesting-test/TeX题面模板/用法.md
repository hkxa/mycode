## 定义题目名称

```
\newcommand\problemacn{国王的难题}
\newcommand\problema{king}

\newcommand\problembcn{别怕，DravDe很善良}
\newcommand\problemb{dravde}

\newcommand\problemccn{复杂易记的密码}
\newcommand\problemc{tricky}

\newcommand\problemdcn{方块填数}
\newcommand\problemd{square}

\newcommand\problemecn{方块填数}
\newcommand\probleme{square}
```

## 题目表格

### 三题

```
\begin{ProblemTableWindows}{\problemacn}{\problema}{\problembcn}{\problemb}{\problemccn}{\problemc}
    \ProblemInfo{每个测试点时限}{1s}{2s}{2s}
    \ProblemInfo{内存限制}{256MB}{256MB}{256MB}
\end{ProblemTableWindows}
\ProgramSuffix{\problema}{\problemb}{\problemc}
~\\
\notice{注意：最终测试时，所有编译命令均不打开任何优化开关。}
```

### 四题

就是把三题的`ProblemTableWindows`改成`ProblemTableWindowsFour`

```
\begin{ProblemTableWindowsFour}{\problemacn}{\problema}{\problembcn}{\problemb}{\problemccn}{\problemc}{\problemdcn}{\problemd}
    \ProblemInfoFour{每个测试点时限}{1s}{2s}{2s}{2s}
    \ProblemInfoFour{内存限制}{256MB}{256MB}{256MB}{256MB}
\end{ProblemTableWindowsFour}
\ProgramSuffixFour{\problema}{\problemb}{\problemc}{\problemd}
~\\
\notice{注意：最终测试时，所有编译命令均不打开任何优化开关。}
```


### 五题
五题的参数有10个，而LaTeX默认最多只能是9个，因此与上面不一样。[具体见帖子](https://tex.stackexchange.com/questions/439474/how-to-define-a-newenvironment-more-than-9-parameters/439635)
```
\begin{ProblemTableWindowsFive}{\problemacn, \problema, \problembcn, \problemb, \problemccn, \problemc, \problemdcn, \problemd, \problemecn, \probleme}
    \ProblemInfoFive{每个测试点时限}{1s}{1s}{1s}{2s}{2s}
    \ProblemInfoFive{内存限制}{256MB}{256MB}{256MB}{256MB}{256MB}
\end{ProblemTableWindowsFive}
\ProgramSuffixFive{\problema}{\problemb}{\problemc}{\problemd}{\probleme}
~\\
\notice{注意：最终测试时，所有编译命令均不打开任何优化开关。}
```

## 题目描述模板

TeXstudio里的快捷键是`Shift+F2`

```
%%%%%%%%%%%%% x
\begin{ProblemDescription}{\problemacn}{\problema} %题目中文名\problemacn, 英文名\problema
\begin{SubDescription}{题目描述}

\end{SubDescription}
\begin{SubDescription}{输入格式}

\end{SubDescription}
\begin{SubDescription}{输出格式}
一行，答案。
\end{SubDescription}
\begin{InputOutputExample}
\exmp{%%Input
4
aabb
}{%%Output
2
}%
\exmp{%%Input
6
aabcaa
}{%%Output
1
}%
\end{InputOutputExample}
\begin{SubDescription}{样例解释}

\end{SubDescription}
\begin{SubDescription}{数据规模与约定}
$50\%$的数据$n \leq 10^3$

$100\%$的数据$n \leq 10^6$
\end{SubDescription}
\end{ProblemDescription}
```