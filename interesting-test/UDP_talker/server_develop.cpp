// UDP server

#pragma comment(lib, "ws2_32.lib")
#include <WinSock2.h>
#include <bits/stdc++.h>
using std::vector;
using std::string;
using std::stringstream;

#define DEFAULT_RECV_PORT 5051 // UDP(server) RECV
// #define DEFAULT_SEND_PORT 5052 // UDP(server) SEND
#define BUFFER_LENGTH 1024

int iPort = DEFAULT_RECV_PORT;
WSADATA wsaData;
SOCKET sSocket;
int iLen;
int iSend;
int iRecv;
char send_buf[1000], recv_buf[BUFFER_LENGTH];
struct sockaddr_in ser, cli;

struct USER_INFO {
    string ip, name;
    u_short port;
    bool in_room;
};

vector<USER_INFO> clic;
USER_INFO generate_user(const char *IP_address, const char *user_name, u_short port) {
    string ip = IP_address, name = user_name;
    return (USER_INFO){ip, name, port, 1};
}

int main() {

    printf("HQ server: Loading winsock...\n");
    if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
    {
        printf("Error: failed to load Winsock.\n");
        return 0;
    }

    printf("HQ server: Loading socket...\n");
    sSocket = socket(AF_INET, SOCK_DGRAM, 0);
    if (sSocket == INVALID_SOCKET) {
        printf("Error: failed to load socket().\nnRet = %d\n", WSAGetLastError());
        return 0;
    }

    ser.sin_family = AF_INET;
    ser.sin_port = htons(iPort);
    ser.sin_addr.s_addr = htonl(INADDR_ANY);

    printf("HQ server: Loading bind...\n");
    if (bind(sSocket, (LPSOCKADDR)&ser, sizeof(ser)) == SOCKET_ERROR) {
        printf("Error: failed to load bind().\nnRet = %d\n", WSAGetLastError());
        return 0;
    }

    printf("+-------------------------+\n");
    printf("| HQ server               |\n");
    printf("|      Powered by brealid |\n");
    printf("+-------------------------+\n");

    iLen = sizeof(cli);

    memset(recv_buf, 0, sizeof(recv_buf));

    while (1) {
        // 接收数据包 确定cli  表示 cli存有客户端发来的 ip 和 port
        Sleep(1000);
        printf("iLen : %d ", iLen);
        iRecv = recvfrom(sSocket, recv_buf, BUFFER_LENGTH, 0, (SOCKADDR *)&cli, &iLen);
        printf("=> %d\n", iLen);
        printf("nnRet = %d, WSAGetLastError() = %d\n", iRecv, WSAGetLastError());
        if (iRecv == SOCKET_ERROR) {
            printf("Error: recvfrom() failed.\n");
        } else if (iRecv == 0) {
            break;
        } else { // 接收到了正确数据
            printf("Recv: %s\n", recv_buf);
            string client_addr = inet_ntoa(cli.sin_addr);
            int client_port = ntohs(cli.sin_port);
            printf("Client: %s:%d\n", client_addr.c_str(), client_port);
            // newuser / newsays

            string recv_message_type;
            for (int i = 0; i < 7; ++i) recv_message_type.push_back(recv_buf[i]);

            if (recv_message_type == "newuser") {
                printf("New user found: %s\n", recv_buf + 8);
                clic.push_back(generate_user(client_addr.c_str(), recv_buf + 8, client_port));
                printf("----------------------\n");
            } else if (recv_message_type == "newsays") {
                int p = 0;
                while (p < clic.size()) {
                    if (clic[p].in_room && clic[p].ip == client_addr.c_str() && clic[p].port == client_port) {
                        printf("New say found: %s (by %s, %s:%d)\n", recv_buf + 7, clic[p].name.c_str(), client_addr.c_str(), client_port);
                        break;
                    }
                    ++p;
                }
                if (p >= clic.size()) {
                    printf("New say found: %s (%s, %s:%d)\n", recv_buf + 7, "N/A", client_addr.c_str(), client_port);
                    printf("Error: user(ip = %s:%d) not found.\n", client_addr.c_str(), client_port);
                    printf("Try to fix : add it to client user now.\n");
                    stringstream gen_name;
                    gen_name << "anonymous(" << client_addr << ":" << client_port << ")";
                    clic.push_back(generate_user(client_addr.c_str(), gen_name.str().c_str(), client_port));
                }
                strcpy(send_buf, clic[p].name.c_str());
                strcat(send_buf, " : ");
                strcat(send_buf, recv_buf + 7);
                for (int i = 0; i < clic.size(); ++i) {
                    if (!clic[i].in_room || i == p)
                        continue;
                    cli.sin_addr.s_addr = inet_addr(clic[i].ip.c_str());
                    cli.sin_port = htons(clic[i].port);
                    iSend = sendto(sSocket, send_buf, sizeof(send_buf), 0, (SOCKADDR *)&cli, sizeof(cli));
                    printf("  Send to user%02d(%s, %s:%d): ", i + 1, clic[i].name.c_str(), clic[i].ip.c_str(), clic[i].port);
                    if (iSend == SOCKET_ERROR || !iSend || WSAGetLastError()) {
                        printf("failed. set user status to Exited.\n");
                        clic[i].in_room = false;
                        break;
                    } else {
                        printf("sendto() succeeded! (nRet = %d, WSAGetLastError() = %d)\n", iSend, WSAGetLastError());
                    }
                }
                printf("----------------------\n");
            } else printf("Error: recv message type not correct! (which is %s)\n", recv_message_type.c_str());
        }
    }
    closesocket(sSocket);
    WSACleanup();
    return 0;
}