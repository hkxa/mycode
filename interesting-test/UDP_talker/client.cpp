
#pragma comment(lib, "ws2_32.lib")
#include <WinSock2.h>
#include <bits/stdc++.h>

#define DEFAULT_PORT 5051     //FOR SEND
#define DEFAULT_PORTRECV 5052 //FOR RECV
#define BUFFER_LENGTH 1024

struct P {
    SOCKET *sClient;
    struct sockaddr_in *ser;
};

void clear_line() {
    printf("\r");
    for (int i = 1; i < 80; ++i) putchar(' ');
    printf("\r");
}

DWORD WINAPI ThreadProc_Recv(LPVOID lpParam)
{
    //printf("threadproc1 is running! ");

    int iRecv;
    char recv_buf[1030];
    int iLen;

    SOCKET sClient;
    struct sockaddr_in ser;
    sClient = *((struct P *)lpParam)->sClient;
    ser = *((struct P *)lpParam)->ser;

    iLen = sizeof(ser);
    while (1)
    {
        iRecv = recvfrom(sClient, recv_buf, sizeof(recv_buf), 0, (struct sockaddr *)&ser, &iLen);
        if (iRecv == SOCKET_ERROR)
        {
            printf("recvfrom()Failed.:%d\n", WSAGetLastError());
            return 0;
        }
        else if (iRecv == 0)
            return 0;
        else {
            clear_line();
            printf("[Rcv] %s\n", recv_buf);
            printf("Your info(click enter to send):");
        }
    }

    return 0;
}

int main(int argc, char *argv[])
{
    WSADATA wsaData;
    SOCKET sClient;
    int iPort = DEFAULT_PORT;

    int iLen;

    int iSend;
    int iRecv;

    char send_buf[1024] = "newuser ";

    char recv_buf[BUFFER_LENGTH];

    struct sockaddr_in ser;

    if (argc < 2)
    {
        printf("Usage: client [server IP address]\n");
        return 0;
    }

    memset(recv_buf, 0, sizeof(recv_buf));
    if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
    {
        printf("Failed to load winsock.\n");
        return 0;
    }

    //建立服务器信息 发送到目标机器
    ser.sin_family = AF_INET;
    ser.sin_port = htons(iPort);
    ser.sin_addr.s_addr = inet_addr(argv[1]);

    sClient = socket(AF_INET, SOCK_DGRAM, 0);
    if (sClient == INVALID_SOCKET){
        printf("socket() failed.\nnRet = %d\n", WSAGetLastError());
        return 0;
    }

    iLen = sizeof(ser);

    struct sockaddr_in cser;

    cser.sin_family = AF_INET;
    cser.sin_port = htons(DEFAULT_PORTRECV);
    cser.sin_addr.s_addr = htonl(INADDR_ANY);

    for (int port = DEFAULT_PORTRECV; port <= DEFAULT_PORTRECV + 11; ++port) {
        cser.sin_port = htons(port);
        if (bind(sClient, (LPSOCKADDR)&cser, sizeof(cser)) == SOCKET_ERROR) {
            printf("Fail to use port %d : bind().nRet = %d\n", port, WSAGetLastError());
            if (port == DEFAULT_PORTRECV + 11) {
                printf("Error: We tried 12 ports, and none of them is ok.\n");
                return 0;
            }
        } else {
            printf("succeed to use port %d\n", port);
            break;
        }
    }

    /*
    cser.sin_family=AF_INET;
    cser.sin_port=htons(iPort);
    cser.sin_addr.s_addr=htonl(INADDR_ANY);

    if(bind(sSocket,(LPSOCKADDR)&ser,sizeof(ser))==SOCKET_ERROR)
    {printf("bind()Faild:%d\n",WSAGetLastError());return 0;}
    */
    char name[100];
    printf("Input your user name:");
    scanf("%s", &name);
    if (strcmp(name, "exit") == 0)
        return 0;
    getchar();
    strcat(send_buf, name);
    iSend = sendto(sClient, send_buf, sizeof(send_buf), 0, (struct sockaddr *)&ser, iLen);

    //CreateThread(
    //	NULL,              // default security attributes
    //	0,                 // use default stack size
    //	(LPTHREAD_START_ROUTINE )ThreadProc_Recv,        // thread function
    //	argv[1],             // argument to thread function
    //	0,                 // use default creation flags
    //	(LPDWORD)&ThreadProc_Recv);           // returns the thread identifier

    struct P *point;
    point = (struct P *)malloc(sizeof(struct P));
    point->sClient = &sClient;
    point->ser = &ser;

    CreateThread(
        NULL,              // default security attributes
        0,                 // use default stack size
        ThreadProc_Recv,   // thread function
        (LPVOID)point,     // argument to thread function
        0,                 // use default creation flags
        NULL);             // returns the thread identifier

    while (1) {
        std::string tmp_info;
        char says[1024];
        printf("Your info(click enter to send):");
        std::getline(std::cin, tmp_info);
        strcpy(says, tmp_info.c_str());

        strcpy(send_buf, "newsays ");
        strcat(send_buf, says);

        iSend = sendto(sClient, send_buf, sizeof(send_buf), 0, (struct sockaddr *)&ser, iLen);
        if (iSend == SOCKET_ERROR)
        {
            printf("sendto() failed.\nnRet = %d\n", WSAGetLastError());
            return 0;
        }
        else if (iSend == 0)
            return 0;
        else
        {
            ; //printf("sendto() mubiao succeeded .\n");
        }
        if (strcmp(says, "exit") == 0)
        {
            printf("exited!\n");
            break;
        }

        /*
        iRecv=recvfrom(sClient,recv_buf,sizeof(recv_buf),0,(struct sockaddr*)&ser,&iLen);
        if(iRecv==SOCKET_ERROR)
        {printf("recvfrom()Failed.:%d\n",WSAGetLastError());return 0;}
        else if(iRecv==0) return 0;
        else
        {
        printf("recvfrom():%s\n",recv_buf);
        printf("----------------------\n");
        }
        */
    }
    closesocket(sClient);
    WSACleanup();
}
