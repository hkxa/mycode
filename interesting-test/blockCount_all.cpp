/*************************************
 * @problem:      blockCount.
 * @time:         2020-02-14.
 * @language:     C++.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
typedef long long int64;

inline int64 pow2(int64 v) {
    return v * v % P;
}

const int N = 1007, P = 998244353;
int n, m;
char charMapInput[N];
bool block[N][N];
int64 f[N][N], g[N][N];
int64 ans;

void odd_solve() {
    for (int i = n; i >= 1; i--)
        for (int j = 1; i - j >= (n - m + 1) / 2 && j <= m; j++) 
            if (block[i][j]) {
                if (block[i + 1][j]) f[i][j] = (f[i][j] + f[i + 1][j]) % P;
                if (block[i][j - 1]) f[i][j] = (f[i][j] + f[i][j - 1]) % P;
            }
    for (int i = 1; i <= n; i++)
        for (int j = n; i - j < (n - m + 1) / 2 && j >= 1; j--) 
            if (block[i][j]) {
                if (block[i - 1][j]) g[i][j] = (g[i][j] + g[i - 1][j]) % P;
                if (block[i][j + 1]) g[i][j] = (g[i][j] + g[i][j + 1]) % P;
            }
    for (int i = n, j = (n + m - 1) / 2; i >= 1 && j >= 1; i--, j--) {
        ans = (ans + pow2(f[i][min(j, m)] * g[i][min(j, m) + 1] % P)) % P;
        ans = (ans + pow2(f[i][min(j, m)] * g[i - 1][min(j, m)] % P)) % P;
    }
    printf("%d\n", ans);
}

void even_solve() {
    for (int i = n; i >= 1; i--)
        for (int j = 1; i - j >= (n - m) / 2 && j <= m; j++) 
            if (block[i][j]) {
                if (block[i + 1][j]) f[i][j] = (f[i][j] + f[i + 1][j]) % P;
                if (block[i][j - 1]) f[i][j] = (f[i][j] + f[i][j - 1]) % P;
            }
    for (int i = 1; i <= n; i++)
        for (int j = n; i - j <= (n - m) / 2 && j >= 1; j--) 
            if (block[i][j]) {
                if (block[i - 1][j]) g[i][j] = (g[i][j] + g[i - 1][j]) % P;
                if (block[i][j + 1]) g[i][j] = (g[i][j] + g[i][j + 1]) % P;
            }
    for (int i = n, j = (n + m) / 2; i >= 1 && j >= 1; i--, j--)
        ans = (ans + pow2(f[i][min(j, m)] * g[i][min(j, m)] % P)) % P;
    printf("%d\n", ans);
}

int main() {
    scanf("%d%d", &n, &m);
    for (int i = 1; i <= n; i++) {
        scanf("%s", charMapInput + 1);
        for (int j = 1; j <= m; j++)
            if (charMapInput[j] == '+') block[i][j] = true;
    }
    f[n][0] = g[1][m + 1] = 1;
    block[n][0] = block[1][m + 1] = true;
    if ((n + m) & 1) odd_solve();
    else even_solve();
    return 0;
}