g++ -std=c++11 -O2 -o KT_fc.exe source_file\file_comp_gbk.cpp -Wall -Wextra -DKT_DEBUG
g++ -std=c++11 -O2 -o KT_inherit.exe source_file\inherit.cpp -Wall -Wextra -DKT_DEBUG
g++ -std=c++11 -O2 -o KT_monitor.exe source_file\monitor_run.cpp -lpsapi -Wall -Wextra -DKT_DEBUG
g++ -std=c++11 -O2 -o KTOJ.exe source_file\main.cpp -Wall -Wextra -DKT_DEBUG