// Problem: In Out Handle
#include <bits/stdc++.h>
#include <windows.h>
#include <psapi.h>

double timeLim, memLim;
std::string exe;

STARTUPINFO si;
PROCESS_INFORMATION pi;
PROCESS_MEMORY_COUNTERS pmc;
FILETIME ct, et, kt, ut;
SYSTEMTIME _kt, _ut;

void onProcessFinished() {
    WaitForSingleObject(pi.hProcess, INFINITE);
    CloseHandle(pi.hThread);
    CloseHandle(pi.hProcess);
}

void run() {
    memset(&si, 0, sizeof(si));
    si.cb = sizeof(si);
    si.dwFlags = STARTF_USESTDHANDLES;
    if (!CreateProcess(NULL, (LPTSTR)exe.c_str(), 0, 0, false, CREATE_NO_WINDOW, 0, 0, &si, &pi))
        printf("无法运行程序: %u\n", (unsigned int)GetLastError()), exit(1);

    int beginTime = clock();
    for (;;) {
        if (WaitForSingleObject(pi.hProcess, 0) == WAIT_OBJECT_0) break;

        GetProcessMemoryInfo(pi.hProcess, &pmc, sizeof(pmc));
        GetProcessTimes(pi.hProcess, &ct, &et, &kt, &ut);
        FileTimeToSystemTime(&kt, &_kt);
        FileTimeToSystemTime(&ut, &_ut);
        double userTime = (_ut.wHour * 3600 + _ut.wMinute * 60 + _ut.wSecond) * 1000 + _ut.wMilliseconds;
        double kernelTime = (_kt.wHour * 3600 + _kt.wMinute * 60 + _kt.wSecond) * 1000 + _kt.wMilliseconds;
        double blockTime = (double)(clock() - beginTime) / CLOCKS_PER_SEC * 1000 - userTime - kernelTime;
        if (pmc.PeakPagefileUsage > memLim * (1 << 10)) {
            TerminateProcess(pi.hProcess, 0);
            onProcessFinished();
            printf("%.2lf %.2lf\n", userTime, pmc.PeakPagefileUsage / 1024.0);
            printf("超过内存限制\n");
            exit(4);
        }
        if (userTime > timeLim * 1.2 || kernelTime > timeLim || blockTime > 1500) {
            TerminateProcess(pi.hProcess, 0);
            onProcessFinished();
            printf("%.2lf %.2lf\n", userTime, pmc.PeakPagefileUsage / 1024.0);
            if (userTime > timeLim * 1.2) printf("超过时间限制\n"), exit(3);
            else if (kernelTime > timeLim) printf("系统 CPU 时间过长\n"), exit(3);
            else printf("进程被阻塞\n"), exit(2);
        }
        Sleep(13);
    }

    DWORD exitCode;
    GetExitCodeProcess(pi.hProcess, &exitCode);
    if (exitCode && exitCode != STILL_ACTIVE) {
        onProcessFinished();
        printf("运行时错误: %u\n", (unsigned int)exitCode);
        exit(2);
    }

    GetProcessMemoryInfo(pi.hProcess, &pmc, sizeof(pmc));
    double usedMemory = pmc.PeakPagefileUsage / 1024.0;

    GetProcessTimes(pi.hProcess, &ct, &et, &kt, &ut);
    FileTimeToSystemTime(&ut, &_ut);
    int usedTime = (_ut.wHour * 3600 + _ut.wMinute * 60 + _ut.wSecond) * 1000 + _ut.wMilliseconds;

    onProcessFinished();
    printf("%d %.2lf\n", usedTime, usedMemory);

    if (usedMemory > memLim) printf("超过内存限制\n"), exit(4);
    if (usedTime > timeLim) printf("超过时间限制\n"), exit(3);
    printf("成功运行\n");

    exit(0);
}

int main(int argc, char* argv[]) {
    if (argc != 6) {
        fprintf(stderr, "Usage:\nmonitior.exe EXE_FILE <--disable_in_file | IN_FILE> <--disable_out_file | OUT_FILE> TIME_LIM MEM_LIM\n");
        return 0;
    }
    exe = argv[1];
    // if (strcmp(argv[2], "--disable_in_file")) exe += std::string(" <") + argv[2];
    if (strcmp(argv[2], "--disable_in_file")) exe = std::string("type ") + argv[2] + " | " + exe;
    if (strcmp(argv[3], "--disable_out_file")) exe += std::string("C:\\ProgramData\\KTOJ\\KT_inherit ") + argv[3] + "\"" + exe + "\"";
    sscanf(argv[4], "%lf", &timeLim);
    sscanf(argv[5], "%lf", &memLim);
    run();
    return 0;
}
// return:    0:N; 1:E; 2:R; 3:T; 4:M;
