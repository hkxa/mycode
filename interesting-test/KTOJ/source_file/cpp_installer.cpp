#include "header.h"
#define TotalStep "6"

const unsigned char KTOJ[] = {exe_KTOJ}, KT_fc[] = {exe_KT_fc}, KT_monitor[] = {exe_KT_monitor}, KT_inherit[] = {exe_KT_inherit};

void WriteExe(char *address, const unsigned char *s, size_t len) {
    FILE *f = fopen(address, "w");
    fwrite(s, 1, len, f);
    fclose(f);
}

int main() {
    printf("KTOJ (1.0.0) Installer");
    printf("(1 / %s) Build Folder...\n", TotalStep);
    if (KT_sys::read_file("C:\\ProgramData\\KTOJ\\ktoj.kt-db").second == -1) {
        KT_sys::call("mkdir C:\\ProgramData\\KTOJ\\");
        KT_sys::call("mkdir C:\\ProgramData\\KTOJ\\Problem");
        KT_sys::call("mkdir C:\\ProgramData\\KTOJ\\Submission");
        KT_sys::write_file(KT_dbfile::prob_list(), "problem_count={0}");
        KT_sys::write_file(KT_dbfile::submission_total(), "submission_count={0}\n");
    }
    printf("(2 / %s) Update Version...\n", TotalStep);
    KT_sys::write_file("C:\\ProgramData\\KTOJ\\ktoj.kt-db", "version={1}{0}{0}");
    printf("(3 / %s) Unzip KTOJ.exe...\n", TotalStep);
    WriteExe("C:\\ProgramData\\KTOJ\\KTOJ.exe", KTOJ, sizeof(KTOJ));
    printf("(4 / %s) Unzip KT_fc.exe...\n", TotalStep);
    WriteExe("C:\\ProgramData\\KTOJ\\KT_fc.exe", KT_fc, sizeof(KT_fc));
    printf("(5 / %s) Unzip KT_monitor.exe...\n", TotalStep);
    WriteExe("C:\\ProgramData\\KTOJ\\KT_monitor.exe", KT_monitor, sizeof(KT_monitor));
    printf("(6 / %s) Unzip KT_inherit.exe...\n", TotalStep);
    WriteExe("C:\\ProgramData\\KTOJ\\KT_inherit.exe", KT_inherit, sizeof(KT_inherit));
    return 0;
}