import os

cmd_compile_program = [
    'g++ -std=c++11 -O2 -o KT_fc.exe source_file\\file_comp_gbk.cpp -Wall -Wextra',
    'g++ -std=c++11 -O2 -o KT_inherit.exe source_file\\inherit.cpp -Wall -Wextra',
    'g++ -std=c++11 -O2 -o KT_monitor.exe source_file\\monitor_run.cpp -lpsapi -Wall -Wextra',
    'g++ -std=c++11 -O2 -o KTOJ.exe source_file\\main.cpp -Wall -Wextra'
]

def readFile_ToIntArray(ff):
    with open(ff, 'rb') as f:
        content = f.read()
    result = ''
    for i in range(len(content)):
        if i:
            result += ','
        result += str(content[i])
    return result


for index, cmd in enumerate(cmd_compile_program):
    os.system(cmd)
    print("Compiling KTOJ... (%d / %d)"%(index + 1, 4), end = '\r')
print()
print("Getting cpp_installer...")
with open("source_file\\cpp_installer.cpp", 'r') as f:
    cpp_installer = f.read()
cpp_installer = cpp_installer.replace("exe_KTOJ", readFile_ToIntArray('KTOJ.exe'))
cpp_installer = cpp_installer.replace("exe_KT_fc", readFile_ToIntArray('KT_fc.exe'))
cpp_installer = cpp_installer.replace("exe_KT_monitor", readFile_ToIntArray('KT_monitor.exe'))
cpp_installer = cpp_installer.replace("exe_KT_inherit", readFile_ToIntArray('KT_inherit.exe'))
print("Compiling cpp_installer...")
with open('source_file\\Temp_Installer.cpp', 'w') as f:
    print(cpp_installer, file = f)
os.system('g++ source_file\\Temp_Installer.cpp -o KTOJ_Installer_v1-0-0.exe -Wl,--stack=536870912 -std=c++11')
os.system('rm source_file\\Temp_Installer.cpp')