#ifndef SOURCE_console_color_hpp
#define SOURCE_console_color_hpp
#include "header.h"
#include "database_input.hpp"
    
/*
0 = 黑色       8 = 灰色
1 = 蓝色       9 = 淡蓝色
2 = 绿色       A = 淡绿色
3 = 浅绿色     B = 淡浅绿色
4 = 红色       C = 淡红色
5 = 紫色       D = 淡紫色
6 = 黄色       E = 淡黄色
7 = 白色       F = 亮白色
*/

namespace KT {
    struct colored_text {
        int back, text;
        string info;
    };
}

namespace KT_console {
    int now_text = 0, now_back = 7;
    void set(int back = 0, int text = 7) {
        now_text = text;
        now_back = back;
        SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), text | (back << 4));
    }
    int char2int(char ch) {
        if (isdigit(ch)) return ch - '0';
        else if (islower(ch)) return ch - 'a' + 10;
        else if (isupper(ch)) return ch - 'A' + 10;
        else return -1;
    }
    int str2int(string color) {
        if (color.size() == 1 && char2int(color[0]) != -1) return char2int(color[0]);
        else if (if_same(color, "black")) return 0;
        else if (if_same(color, "blue") || if_same(color, "dark blue")) return 1;
        else if (if_same(color, "green") || if_same(color, "dark green")) return 2;
        else if (if_same(color, "shallow green")) return 3;
        else if (if_same(color, "red")) return 4;
        else if (if_same(color, "purple")) return 5;
        else if (if_same(color, "yellow")) return 6;
        else if (if_same(color, "white")) return 7;
        else if (if_same(color, "gray") || if_same(color, "grey")) return 8;
        else if (if_same(color, "light blue")) return 9;
        else if (if_same(color, "light green")) return 10;
        else if (if_same(color, "shallow light green") || if_same(color, "light shallow green")) return 11;
        else if (if_same(color, "light red")) return 12;
        else if (if_same(color, "light purple") || if_same(color, "pink")) return 13;
        else if (if_same(color, "light yellow")) return 14;
        else if (if_same(color, "light white") || if_same(color, "bone")) return 15;
        else return 0;
    }
    char int2char(int v) {
        if (v < 10) return v + '0';
        else return v - 10 + 'a';
    }
    void set(char b, char t) {
        set(char2int(b), char2int(t));
    }
    void set(string s) {
        set(char2int(s[0]), char2int(s[1]));
    }
    void set_text(string color) {
        set(now_back, str2int(color));
    }
    void set_back(string color) {
        set(str2int(color), now_text);
    }
    void set(string back, string text) {
        set(str2int(back), str2int(text));
    }
    void show(const KT::colored_text &txt) {
        set(txt.back, txt.text);
        printf("%s", txt.info.c_str());
        set();
    }
    void show(vector<KT::colored_text> vtxt) {
        for (const KT::colored_text &txt: vtxt) show(txt);
        if (vtxt.empty() || vtxt.back().info.back() != '\n') putchar(10);
    }
    void test() {
        string s = "0123456789abcdef";
        for (int i = 0; i < 16; ++i) {
            for (int j = 0; j < 16; ++j) {
                string info = s.substr(i, 1) + s.substr(j, 1);
                set(info);
                printf("%s", info.c_str());
                set();
                printf(" ");
            }
            printf("\n");
        }
    }
}

namespace KT {
    colored_text make_colored_text(string text, string col_text = "7", string col_back = "0") {
        KT::colored_text ct;
        ct.back = KT_console::str2int(col_back);
        ct.text = KT_console::str2int(col_text);
        ct.info = text;
        return ct;
    }
    vector<colored_text> read_from(database &db) {
        string s1, s2, s3;
        vector<colored_text> res;
        do {
            db >> s1 >> s2 >> s3;
            if (s1 == "<EOF>" || s2 == "<EOF>" || s3 == "<EOF>") break;
            res.push_back(make_colored_text(s3, s2, s1));
        } while (true);
        return res;
    }
    vector<colored_text> read_from(string file) {
        database db;
        KT_Log("KT::read_from: try to open file " + file + "\n");
        db.open(file);
        KT_Log("KT::read_from: open succeeded\n");
        return read_from(db);
    }
}

namespace KT_console {
    void print(string text, string col_text = "7", string col_back = "0") {
        show(KT::make_colored_text(text, col_text, col_back));
    }
}
#endif // SOURCE_console_color_hpp