#ifndef SOURCE_problem_hpp
#define SOURCE_problem_hpp
#include "header.h"
#include "database_input.hpp"
#include "console_color.hpp"
#include "encode_decode.hpp"

namespace KT {
    int get_problem_total() {
        database prob_list;
        prob_list.open(KT_dbfile::prob_list());
        return prob_list.get_int();
    }
    string get_prob_name(int prob_id) {
        database db;
        db.open(KT_dbfile::prob_list());
        int n, id;
        string name;
        db >> n;
        for (int i = 1; i <= n; ++i) {
            db >> id >> name;
            if (id == prob_id) return name;
        }
        return "<NotFound>";
    }
    struct problem_judge {
        string judge, in, out;
    };
    struct problem_statement {
        string description, input_format, output_format, data_limit;
    };
    struct problem_std {
        string lang, file;
    };
    struct problem_datacase {
        string case_type, case_name, in, out;
        int points, time_limit, memory_limit;
    };
    struct problem_subtask {
        string subtask_name, calc_way;
        vector<string> cases;
    };
    class problem {
    public:
        int problem_id;
        string name;
        problem_judge judge;
        problem_statement statement;
        vector<problem_std> std;
        vector<problem_datacase> data;
        vector<problem_subtask> subtask;
        problem_datacase* find_data(string case_name) {
            for (size_t i = 0; i < data.size(); ++i)
                if (data[i].case_name == case_name) return &data[i];
            return NULL;
        }
        void init(int id, string problem_config_file) {
            problem_id = id;
            database db;
            db.open(problem_config_file);
            string io_way;
            db >> name >> judge.judge >> io_way;
            if (io_way == "stdio") judge.in = judge.out = "stdio";
            else db >> judge.in >> judge.out;
            db >> statement.description >> statement.input_format >> 
                  statement.output_format >> statement.data_limit;
            std.resize(db.get_int());
            for (unsigned i = 0; i < std.size(); ++i)
                db >> std[i].lang >> std[i].file;
            data.resize(db.get_int());
            for (unsigned i = 0; i < data.size(); ++i)
                db >> data[i].case_type >> data[i].case_name >> data[i].points
                   >> data[i].time_limit >> data[i].memory_limit
                   >> data[i].in>> data[i].out;
            subtask.resize(db.get_int());
            for (unsigned i = 0; i < subtask.size(); ++i) {
                db >> subtask[i].subtask_name >> subtask[i].calc_way;
                subtask[i].cases.resize(db.get_int());
                for (unsigned j = 0; j < subtask[i].cases.size(); ++j)
                    db >> subtask[i].cases[j];
            }
        }
        vector<colored_text> get_statement() {
            vector<colored_text> res, t;
            res.push_back(make_colored_text(name, "f", "pink"));
            res.push_back(make_colored_text("\n"));
            res.push_back(make_colored_text("Description", "0", "B"));
            res.push_back(make_colored_text("\n"));
            t = read_from(KT_dbfile::prob_statement(problem_id, statement.description));
            res.insert(res.end(), t.begin(), t.end());
            res.push_back(make_colored_text("\n"));
            res.push_back(make_colored_text("Input Format", "0", "B"));
            res.push_back(make_colored_text("\n"));
            t = read_from(KT_dbfile::prob_statement(problem_id, statement.input_format));
            res.insert(res.end(), t.begin(), t.end());
            res.push_back(make_colored_text("\n"));
            res.push_back(make_colored_text("Output Format", "0", "B"));
            res.push_back(make_colored_text("\n"));
            t = read_from(KT_dbfile::prob_statement(problem_id, statement.output_format));
            res.insert(res.end(), t.begin(), t.end());
            res.push_back(make_colored_text("\n"));
            res.push_back(make_colored_text("Sample Test", "0", "B"));
            res.push_back(make_colored_text("\n"));
            for (unsigned i = 0; i < data.size(); ++i) {
                if (data[i].case_type != "sample") continue;
                res.push_back(make_colored_text("Sample Input #" + to_string(i + 1) + "\n", "bone"));
                string inf = KT_sys::read_file(KT_dbfile::prob_data(problem_id, data[i].in)).first;
                if (inf.size() <= 200) res.push_back(make_colored_text(inf));
                else {
                    string place = KT_dbfile::temp_dir() + "\\" + KT_random::gen_str(8) + ".in";
                    KT_sys::write_file(place, inf);
                    res.push_back(make_colored_text("See \"" + place + "\"\n"));
                }
                res.push_back(make_colored_text("\n"));
                res.push_back(make_colored_text("Sample Output #" + to_string(i + 1) + "\n", "bone"));
                string ouf = KT_sys::read_file(KT_dbfile::prob_data(problem_id, data[i].out)).first;
                if (ouf.size() <= 200) res.push_back(make_colored_text(ouf));
                else {
                    string place = KT_dbfile::temp_dir() + "\\" + KT_random::gen_str(8) + ".out";
                    KT_sys::write_file(place, ouf);
                    res.push_back(make_colored_text("See \"" + place + "\"\n"));
                }
                res.push_back(make_colored_text("\n"));
            }
            res.push_back(make_colored_text("\n"));
            res.push_back(make_colored_text("Data Limit", "0", "B"));
            res.push_back(make_colored_text("\n"));
            t = read_from(KT_dbfile::prob_statement(problem_id, statement.data_limit));
            res.insert(res.end(), t.begin(), t.end());
            return res;
        }
    };
    #define encode(xxx) KT_codec::codec.encode(xxx, 3)
    #define read_and_encode(ff) encode(KT_sys::read_file(ff).first)
    string load_out(int pid) {
        string result;
        problem p;
        p.init(pid, KT_dbfile::prob_config(pid));
        // config;
        KT_console::print("Collecting config...\n");
        result += "{" + encode(KT_sys::read_file(KT_dbfile::prob_config(pid)).first) + "}";
        // statement;
        KT_console::print("Collecting statement...\n");
        result += "{" + encode(p.statement.description) + "}";
        result += "{" + read_and_encode(KT_dbfile::prob_statement(pid, p.statement.description)) + "}";
        result += "{" + encode(p.statement.input_format) + "}";
        result += "{" + read_and_encode(KT_dbfile::prob_statement(pid, p.statement.input_format)) + "}";
        result += "{" + encode(p.statement.output_format) + "}";
        result += "{" + read_and_encode(KT_dbfile::prob_statement(pid, p.statement.output_format)) + "}";
        result += "{" + encode(p.statement.data_limit) + "}";
        result += "{" + read_and_encode(KT_dbfile::prob_statement(pid, p.statement.data_limit)) + "}";
        // std;
        KT_console::print("Collecting std...\n");
        for (size_t i = 0; i < p.std.size(); ++i)
            result += "{" + read_and_encode(KT_dbfile::prob_std(pid, p.std[i].file)) + "}";
        // data;
        KT_console::print("Collecting data 0...");
        for (size_t i = 0; i < p.data.size(); ++i) {
            KT_console::print("\rCollecting data " + to_string(i + 1) + " / " + to_string(p.data.size()) + "...");
            result += "{" + read_and_encode(KT_dbfile::prob_data(pid, p.data[i].in)) + "}";
            result += "{" + read_and_encode(KT_dbfile::prob_data(pid, p.data[i].out)) + "}";
        }
        return result;
    }
    #undef encode
    #undef read_and_encode

    #define decode(xxx) KT_codec::codec.decode(xxx, 3)
    void load_in(int pid, string ktp_package_file_name, bool update_ProblemList = true) {
        database db;
        db.open(ktp_package_file_name);
        problem p;
        // init;
        KT_sys::call("mkdir " + KT_dbfile::prob_folder(pid) + "statement");
        KT_sys::call("mkdir " + KT_dbfile::prob_folder(pid) + "std");
        KT_sys::call("mkdir " + KT_dbfile::prob_folder(pid) + "data");
        // config;
        KT_console::print("Getting config...\n");
        KT_sys::write_file(KT_dbfile::prob_config(pid), decode(db.get_str()));
        p.init(pid, KT_dbfile::prob_config(pid));
        // statement;
        KT_console::print("Getting statement...\n");
        string name, content;
        db >> name >> content;
        KT_sys::write_file(KT_dbfile::prob_statement(pid, decode(name)), decode(content));
        db >> name >> content;
        KT_sys::write_file(KT_dbfile::prob_statement(pid, decode(name)), decode(content));
        db >> name >> content;
        KT_sys::write_file(KT_dbfile::prob_statement(pid, decode(name)), decode(content));
        db >> name >> content;
        KT_sys::write_file(KT_dbfile::prob_statement(pid, decode(name)), decode(content));
        // std;
        KT_console::print("Getting std...\n");
        for (size_t i = 0; i < p.std.size(); ++i)
            KT_sys::write_file(KT_dbfile::prob_std(pid, p.std[i].file), decode(db.get_str()));
        // data;
        KT_console::print("Getting data 0...");
        for (size_t i = 0; i < p.data.size(); ++i) {
            KT_console::print("\rGetting data " + to_string(i + 1) + " / " + to_string(p.data.size()) + "...");
            KT_sys::write_file(KT_dbfile::prob_data(pid, p.data[i].in), decode(db.get_str()));
            KT_sys::write_file(KT_dbfile::prob_data(pid, p.data[i].out), decode(db.get_str()));
        }
        KT_console::print("\n");
        // update_ProblemList
        if (update_ProblemList) {
            KT_console::print("Updating ProblemList...\n");
            string plist = KT_sys::read_file(KT_dbfile::prob_list()).first;
            plist = "problem_count={" + to_string(pid - 999) + "}" + plist.substr(plist.find('}') + 1);
            string info = "{" + to_string(pid) + "}{" + p.name + "}\n";
            KT_sys::write_file(KT_dbfile::prob_list(), plist + info);
        }
    }
    #undef decode
}

#endif