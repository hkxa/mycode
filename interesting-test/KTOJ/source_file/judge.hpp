#ifndef SOURCE_judge_hpp
#define SOURCE_judge_hpp
#include "header.h"
#include "console_color.hpp"

namespace KT {
    struct judge_result {
        string result;
        int time, memory;
        judge_result(string res, int tim, int mem) : result(res), time(tim), memory(mem) {}
    };
    string get_extension_name(string file_name) {
        int p = file_name.length() - 1;
        while (p >= 0 && file_name[p] != '.') --p;
        if (file_name[p] == '.') ++p;
        else return "";
        return file_name.substr(p, file_name.length() - p);
    }
    string get_no_extension_name(string file_name) {
        int p = file_name.length() - 1;
        while (p >= 0 && file_name[p] != '.') --p;
        if (file_name[p] != '.') return "";
        return file_name.substr(0, p);
    }
    int compile(string SourceFile, string ExeFile) {
        string Lang = get_extension_name(SourceFile), 
               NoExt = get_no_extension_name(SourceFile), compile_command;
        if (Lang == "cpp") compile_command = string("g++ \"") + SourceFile + "\" -o \"" + ExeFile + "\" -Wall -Wextra -Wl,--stack=536870912 -DONLINE_JUDGE -DKTOJ";
        else if (Lang == "c") compile_command = string("g++ \"") + SourceFile + "\" -o \"" + ExeFile + "\" -Wall -Wextra -Wl,--stack=536870912 -DONLINE_JUDGE -DKTOJ";
        else return 1;
        return KT_sys::call(compile_command);
    }
    int compare_file_ignoreNotprintChar(string file1, string file2) {
        // call fulltext_gbk
        // not written ok now
        string command = "C:\\ProgramData\\KTOJ\\KT_fc.exe NO_INPUT \"" + file1 + "\" \"" + file2 + "\"";
        int nRet = KT_sys::call(command);
        (void)nRet; // For Warning-Disable
        KT_Log("call nRet: " + to_string(nRet) + "\n");
        return KT_sys::closest_info[0] != '1';
    }
    pair<int, double> divide_string_into_int_and_double(string s) {
        stringstream ss(s);
        pair<int, double> ret;
        ss >> ret.first >> ret.second;
        return ret;
    }
    judge_result judge(string ExeFile, string ProgInFile, string ProgOutFile, 
                       string InFile, string OutFile, int TimeLimit, int MemoryLimit) {
        // if (ProgInFile == "stdio" || ProgOutFile == "stdio") return judge_result("JudgementFailed", 0, 0);
        if (KT_sys::call("copy " + InFile + " " + ProgInFile))
            return judge_result("JudgementFailed", 0, 0);
        string str_TimeLimit = to_string(TimeLimit);
        string str_MemoryLimit = to_string(MemoryLimit);
        string RunCmd = "C:\\ProgramData\\KTOJ\\KT_monitor " + ExeFile;
        if (ProgInFile == "stdio") {
            ProgInFile = "%temp%\\" + KT_random::gen_str(10) + ".in";
            RunCmd += " " + ProgInFile;
        } else RunCmd += " --disable_in_file";
        if (ProgOutFile == "stdio") {
            ProgOutFile = "%temp%\\" + KT_random::gen_str(10) + ".out";
            RunCmd += " " + ProgOutFile;
        } else RunCmd += " --disable_out_file";
        RunCmd += " " + str_TimeLimit + " " + str_MemoryLimit;
        // fprintf(stderr, "hi: %s", RunCmd.c_str());
        KT_Log(RunCmd);
        int nRet = KT_sys::call(RunCmd);
        switch (nRet) {
            // 0:N; 1:E; 2:R; 3:T; 4:M;
            case 0 : {
                pair<int, int> TimMem = divide_string_into_int_and_double(KT_sys::closest_info);
                int nRet = compare_file_ignoreNotprintChar(OutFile, ProgOutFile);
                if (nRet) return judge_result("WA", TimMem.first, TimMem.second);
                else return judge_result("AC", TimMem.first, TimMem.second);
                break;
            }
            case 1:
                return judge_result("UKE", 0, 0); 
                break;
            case 2:
                return judge_result("RE", 0, 0); 
                break;
            case 3:
                return judge_result("TLE", TimeLimit, 0); 
                break;
            case 4:
                return judge_result("MLE", 0, MemoryLimit); 
                break;
        }
        return judge_result("UKE", 0, 0); 
    }
}
#endif