#ifndef SOURCE_database_input_hpp
#define SOURCE_database_input_hpp
#include "header.h"

namespace KT {
    class database {
        FILE *file;
    public:
        database() : file(NULL) {}
        ~database() {
            if (file) fclose(file);
        }
        bool open(string file_name) {
            file = fopen(file_name.c_str(), "r");
            return (bool)file;
        }
        bool close() {
            if (file == NULL) return false;
            fclose(file);
            return true;
        }
        bool is_open() {
            return (bool)file;
        }
        string get_str() {
            string ret;
            char now;
            while (fscanf(file, "%c", &now) == 1 && now != '{');
            if (now != '{') return "<EOF>";
            while (fscanf(file, "%c", &now) == 1 && now != '}') ret.push_back(now);
            return ret;
        }
        int get_int() {
            stringstream ss(get_str());
            int ret;
            ss >> ret;
            return ret;
        }
        database& operator >> (string &ss) {
            ss = get_str();
            return *this;
        }
        database& operator >> (int &ii) {
            ii = get_int();
            return *this;
        }
    };
}

#endif