#ifndef SOURCE_submission_hpp
#define SOURCE_submission_hpp
#include "header.h"
#include "database_input.hpp"
#include "problem.hpp"
#include "judge.hpp"
#include "console_color.hpp"

namespace KT {
    struct submission_datacase {
        string case_name, result;
        int points, time, memory;
    };
    struct submission_subtask {
        string subtask_name, result;
        int points;
        string calc_way;
        vector<string> cases;
    };
    int get_submission_total() {
        database sub_list;
        sub_list.open(KT_dbfile::submission_total());
        return sub_list.get_int();
    }
    void update_submission_total(int submission_count) {
        ofstream fout(KT_dbfile::submission_total());
        fout << "submission_count={" << submission_count << "}\n";
        fout.close();
    }
    string status_color(string status) {
        if (status == "AC" || status == "Accepted") return "green";
        else if (status == "CE" || status == "CompileError") return "yellow";
        else if (status == "WA" || status == "Unaccepted") return "red";
        else if (status == "TLE" || status == "MLE") return "blue";
        else if (status == "RE") return "purple";
        else if (status == "UKE" || status == "SettingsError" || status == "JudgementFailed") return "gray";
        else return "white";
    }
    class submission {
    public:
        int submission_id;
        int user, problem;
        string lang;
        vector<submission_datacase> data;
        vector<submission_subtask> subtask;
        submission() : submission_id(-1) {}
        submission_datacase* find_data(string case_name) {
            for (size_t i = 0; i < data.size(); ++i)
                if (data[i].case_name == case_name) return &data[i];
            return NULL;
        }
        void init(int id, string submission_file) {
            submission_id = id;
            database db;
            db.open(submission_file);
            db >> user >> problem >> lang;
            data.resize(db.get_int());
            for (unsigned i = 0; i < data.size(); ++i)
                db >> data[i].case_name >> data[i].result >> data[i].points
                   >> data[i].time >> data[i].memory;
            subtask.resize(db.get_int());
            for (unsigned i = 0; i < subtask.size(); ++i) {
                db >> subtask[i].subtask_name >> subtask[i].result >> subtask[i].points >> subtask[i].calc_way;
                subtask[i].cases.resize(db.get_int());
                for (unsigned j = 0; j < subtask[i].cases.size(); ++j)
                    db >> subtask[i].cases[j];
            }
        }
        vector<colored_text> show() {
            vector<colored_text> res;
            if (!submission_id) {
                res.push_back(make_colored_text("Submission Error!\n", "red"));
                return res;
            }
            res.push_back(make_colored_text("R" + to_string(submission_id) + " - P" + to_string(problem) + " " + get_prob_name(problem) + "\n"));
            res.push_back(make_colored_text("language: " + lang + "\n"));
            res.push_back(make_colored_text("submitter: U" + to_string(user) + "\n"));
            res.push_back(make_colored_text("result: "));
            if (data[0].result == "CE") {
                res.push_back(make_colored_text("Compile Error 0 pts\n", "yellow"));
                return res;
            }
            string prob_result = result();
            res.push_back(make_colored_text(prob_result + " " + to_string(total_points()) + " pts\n", status_color(prob_result)));
            res.push_back(make_colored_text("Time(sum): " + to_string(total_time()) + " ms\nMemory(max): " + to_string(max_memory()) + " KiB\n"));
            for (unsigned i = 0; i < data.size(); ++i) {
                res.push_back(make_colored_text("data " + data[i].case_name + ": "));
                res.push_back(make_colored_text(data[i].result + " " + to_string(data[i].points) + " pts", status_color(data[i].result)));
                res.push_back(make_colored_text(" (Time: " + to_string(data[i].time) + " ms Memory: " + to_string(data[i].memory) + " KiB)\n"));
            }
            for (unsigned i = 0; i < subtask.size(); ++i) {
                res.push_back(make_colored_text("Subtask " + subtask[i].subtask_name + ": "));
                res.push_back(make_colored_text(subtask[i].result + " " + to_string(subtask[i].points) + " pts\n", status_color(subtask[i].result)));
            }
            return res;
        }
        void judge(KT::problem prob, int UsedId, string ProgramFile, int sid = get_submission_total() + 1) {
            submission_id = sid;
            printf("Judging: R%d (P%d %s)\n", submission_id, prob.problem_id, prob.name.c_str());
            update_submission_total(submission_id);
            problem = prob.problem_id;
            lang = get_extension_name(ProgramFile);
            user = UsedId;
            // **************** compile ****************
            string ExeFile = "%temp%\\" + KT_random::gen_str(16) + ".exe";
            printf("Compiling... ");
            int compile_nRet = compile(ProgramFile, ExeFile);
            if (compile_nRet) {
                KT_console::print("Compile Error!\n", "yellow");
                data.resize(prob.data.size());
                for (unsigned i = 0; i < data.size(); ++i) {
                    data[i].case_name = prob.data[i].case_name;
                    data[i].result = "CE";
                    data[i].points = 0;
                    data[i].time = 0;
                    data[i].memory = 0;
                }
                goto KT_submission_calc_subtask;
            }
            KT_console::print("Compiled ok\n", "green");
            // **************** judge data ****************
            data.resize(prob.data.size());
            KT_console::print("[" + string(".") * data.size() + "]\r[", "gray");
            for (unsigned i = 0; i < data.size(); ++i) {
                data[i].case_name = prob.data[i].case_name;
                KT::judge_result res = KT::judge(ExeFile, prob.judge.in, prob.judge.out, 
                                                 KT_dbfile::prob_data(prob.problem_id, prob.data[i].in),
                                                 KT_dbfile::prob_data(prob.problem_id, prob.data[i].out),
                                                 prob.data[i].time_limit, prob.data[i].memory_limit);
                KT_Log(res.result + " | " + to_string(res.time) + " | " + to_string(res.memory) + "\n");
                data[i].result = res.result;
                data[i].points = (res.result == "AC" ? prob.data[i].points : 0);
                data[i].time = res.time;
                data[i].memory = res.memory;
                KT_console::print(res.result.substr(0, 1), status_color(res.result));
            }
            KT_console::print("\n");
            if (prob.judge.in != "stdio") KT_sys::call("del \"" + prob.judge.in + "\"");
            if (prob.judge.out != "stdio") KT_sys::call("del \"" + prob.judge.out + "\"");
            KT_sys::call("del \"" + ExeFile + "\"");
            // **************** calc subtask ****************
            KT_submission_calc_subtask:
            subtask.resize(prob.subtask.size());
            for (unsigned i = 0; i < subtask.size(); ++i) {
                subtask[i].subtask_name = prob.subtask[i].subtask_name;
                subtask[i].calc_way = prob.subtask[i].calc_way;
                subtask[i].cases.resize(prob.subtask[i].cases.size());
                if (subtask[i].calc_way == "sum") {
                    subtask[i].points = 0;
                    subtask[i].result = "AC";
                    for (unsigned j = 0; j < subtask[i].cases.size(); ++j) {
                        subtask[i].cases[j] = prob.subtask[i].cases[j];
                        submission_datacase *p = find_data(prob.subtask[i].cases[j]);
                        if (p == NULL) {
                            subtask[i].result = "SettingsError";
                            continue;
                        }
                        subtask[i].points += p->points;
                        if (subtask[i].result == "AC" && p->result != "AC")
                            subtask[i].result = p->result;
                    }
                } else if (subtask[i].calc_way == "max") {
                    subtask[i].points = -1;
                    subtask[i].result = "UKE";
                    for (unsigned j = 0; j < subtask[i].cases.size(); ++j) {
                        subtask[i].cases[j] = prob.subtask[i].cases[j];
                        submission_datacase *p = find_data(prob.subtask[i].cases[j]);
                        if (p == NULL) {
                            subtask[i].result = "SettingsError";
                            continue;
                        }
                        if (subtask[i].points < p->points || (subtask[i].points == p->points && subtask[i].result != "AC" && p->result == "AC")) {
                            subtask[i].points = p->points;
                            subtask[i].result = p->result;
                        }
                    }
                } else if (subtask[i].calc_way == "min") {
                    subtask[i].points = 1e9;
                    subtask[i].result = "AC";
                    for (unsigned j = 0; j < subtask[i].cases.size(); ++j) {
                        subtask[i].cases[j] = prob.subtask[i].cases[j];
                        submission_datacase *p = find_data(prob.subtask[i].cases[j]);
                        if (p == NULL) {
                            subtask[i].result = "SettingsError";
                            continue;
                        }
                        if (subtask[i].points > p->points || (subtask[i].points == p->points && subtask[i].result == "AC" && p->result != "AC")) {
                            subtask[i].points = p->points;
                            subtask[i].result = p->result;
                        }
                    }
                }
            }
            KT_console::show(show());
        }
        void write_to_file(string submission_file) {
            if (submission_id == -1) return;
            ofstream fout(submission_file);
            fout << "user={" << user << "}\n";
            fout << "problem={" << problem << "}\n";
            fout << "lang={" << lang << "}\n";
            fout << "\n";
            fout << "data={" << data.size() << "}\n";
            for (unsigned i = 0; i < data.size(); ++i)
                fout << "{" << data[i].case_name << "}"
                     << "{" << data[i].result << "}"
                     << "{" << data[i].points << "}"
                     << "{" << data[i].time << "}"
                     << "{" << data[i].memory << "}\n";
            fout << "\n";
            fout << "subtask={" << subtask.size() << "}\n";
            for (unsigned i = 0; i < subtask.size(); ++i) {
                fout << "{" << subtask[i].subtask_name << "}"
                     << "{" << subtask[i].result << "}"
                     << "{" << subtask[i].points << "}"
                     << "{" << subtask[i].calc_way << "}"
                     << "{" << subtask[i].cases.size() << "}";
                for (unsigned j = 0; j < subtask[i].cases.size(); ++j)
                    fout << "{" << subtask[i].cases[j] << "}";
                fout << "\n";
            }
            fout.close();
        }
        string source_name() {
            return "sub" + to_string(submission_id) + "." + lang;
        }
        string result() {
            string prob_result = "Accepted";
            for (unsigned i = 0; i < data.size(); ++i)
                if (data[i].result == "JudgementFailed") prob_result = "JudgementFailed";
                else if (data[i].result != "AC" && prob_result != "JudgementFailed") prob_result = "Unaccepted";
            return prob_result;
        }
        int total_points() {
            int ret = 0;
            for (unsigned i = 0; i < subtask.size(); ++i)
                ret += subtask[i].points;
            return ret;
        }
        int total_time() {
            int ret = 0;
            for (unsigned i = 0; i < data.size(); ++i)
                ret += data[i].time;
            return ret;
        }
        int max_memory() {
            int ret = 0;
            for (unsigned i = 0; i < data.size(); ++i)
                ret = max(ret, data[i].memory);
            return ret;
        }
    };
}

#endif