#include "header.h"
#include "problem.hpp"
#include "submission.hpp"
#include "console_color.hpp"

// Declare start
vector<string> divide_command(string);
template <typename CT, typename T> bool check_all_in_range(const CT&, T, T);
bool check_file_exist(string);
int execute(vector<string>);
int execute(string);
string submit(int, string);
// Declare end

vector<string> divide_command(string s) {
    vector<string> res;
    unsigned i, lst;
    bool is_comma = false;
    for (i = lst = 0; i <= s.size(); ++i)
        if (i == s.size() || (!is_comma && s[i] == ' ') || (is_comma && s[i] == '\"')) {
            if (i == lst) {
                lst = i + 1;
                continue;
            }
            res.push_back(s.substr(lst, i - lst));
            lst = i + 1;
            if (is_comma) is_comma = false;
        } else if (s[i] == '\"') {
            is_comma = true;
            lst = i + 1;
        }
    return res;
}

template <typename CT, typename T>
bool check_all_in_range(const CT &ss, T low, T upp) {
    for (const T &x : ss)
        if (x < low || x > upp) return false;
    return true;
}

bool check_file_exist(string ff) {
    FILE *f = fopen(ff.c_str(), "r");
    if (!f) return false;
    fclose(f);
    return true;
}

bool check_file_writable(string ff) {
    FILE *f = fopen(ff.c_str(), "w");
    if (!f) return false;
    fclose(f);
    return true;
}

// Unchecked Operation (pid, file must be checked)
string submit(int pid, string file) {
    string prob_name = KT::get_prob_name(pid);
    string src_file = KT_dbfile::sub_src(KT::get_submission_total() + 1, KT::get_extension_name(file));
    KT_sys::call("copy \"" + file + "\" " + src_file);
    // KT_console::print("Loading Problem Config...\n");
    KT::problem p;
    p.init(pid, KT_dbfile::prob_config(pid));
    KT::submission sub;
    sub.judge(p, 1, src_file);
    sub.write_to_file(KT_dbfile::sub_id(sub.submission_id));
    return sub.result();
}

int execute(vector<string> v) {
    if (v.empty()) return 0;
    if (if_same(v[0], "exit")) {
        KT_console::print("exit\n");
        if (v.size() > 1) exit(str_to_int(v[1]));
        exit(0);
    }
    if (if_same(v[0], "submit")) {
        if (v.size() != 3) {
            KT_console::print("Usage: submit <problem_id> <program_file>\n");
            return 1;
        }
        if (v[1][0] == 'p' || v[1][0] == 'P' || v[1][0] == '#') v[1] = v[1].substr(1);
        if (!check_all_in_range(v[1], '0', '9')) {
            KT_console::print("Usage: submit <problem_id> <program_file>\n");
            return 1;
        }
        if (!check_file_exist(v[2])) {
            KT_console::print("Source File not found!\n");
            return 1;
        }
        int prob_id = str_to_int(v[1]);
        if (KT::get_prob_name(prob_id) == "<NotFound>") {
            KT_console::print("Problem not found!\n");
            return 1;
        }
        submit(prob_id, v[2]);
        return 0;
    }
    if (if_same(v[0], "show")) {
        if (v.size() == 1) {
            KT_console::print("Usage: show problem list\n"
                              "  <or> show problem <problem id>\n"
                              "  <or> show KT-TF <KT-TF file>\n"
                              "  <or> show submission <submission id>\n");
            return 1;
        }
        if (if_same(v[1], "p") || if_same(v[1], "prob") || if_same(v[1], "problem")) {
            if (v.size() != 3) {
                KT_console::print("Usage: show problem list\n  <or> show problem <problem id>\n");
                return 1;
            }
            if (v[2][0] == 'p' || v[2][0] == 'P' || v[2][0] == '#') v[2] = v[2].substr(1);
            if (!if_same(v[2], "list") && !check_all_in_range(v[2], '0', '9')) {
                KT_console::print("Usage: show problem list\n  <or> show problem <problem id>\n");
                return 1;
            }
            if (if_same(v[2], "list")) {
                KT::database plist;
                plist.open(KT_dbfile::prob_list());
                int cnt = plist.get_int();
                KT_console::print("KTOJ", "light blue");
                KT_console::print(" - Problem List\n");
                KT_console::print("Total Problem(s): " + to_string(cnt) + "\n", "green");
                for (int i = 1, id; i <= cnt; ++i) {
                    string name;
                    plist >> id >> name;
                    KT_console::print("P" + to_string(id) + " " + name + "\n");
                }
            } else {
                int prob_id = str_to_int(v[2]);
                string prob_name = KT::get_prob_name(prob_id);
                if (prob_name == "<NotFound>") {
                    KT_console::print("Problem not found!\n");
                    return 1;
                }
                KT::problem p;
                p.init(prob_id, KT_dbfile::prob_config(prob_id));
                KT_console::show(p.get_statement());
            }
            return 0;
        }
        if (if_same(v[1], "KT-TF") || if_same(v[1], "TF")) {
            if (v.size() != 3) {
                KT_console::print("Usage: show KT-TF <KT-TF file>\n");
                return 1;
            }
            if (!check_file_exist(v[2])) {
                KT_console::print("KT-TF file not found!\n");
                return 1;
            }
            KT_console::show(KT::read_from(v[2]));
            return 0;
        }
        if (if_same(v[1], "s") || if_same(v[1], "sub") || if_same(v[1], "submission")) {
            if (v.size() != 3) {
                KT_console::print("Usage: show submission <submission id>\n");
                return 1;
            }
            if (v[2][0] == 's' || v[2][0] == 'S' || v[2][0] == 'r' || v[2][0] == 'R' || v[2][0] == '#') v[2] = v[2].substr(1);
            if (!check_all_in_range(v[2], '0', '9')) {
                KT_console::print("Usage: show submission <submission id>\n");
                return 1;
            }
            int id = str_to_int(v[2]);
            if (id < 0 || id > KT::get_submission_total()) {
                KT_console::print("Submission isn't exist!\n");
                return 1;
            }
            KT::submission sub;
            sub.init(id, KT_dbfile::sub_id(id));
            KT_console::show(sub.show());
            return 0;
        }
        KT_console::print("Usage: show problem list\n"
                          "  <or> show problem <problem id>\n"
                          "  <or> show KT-TF <KT-TF file>\n"
                          "  <or> show submission <submission id>\n");
        return 1;
    }
    if (if_same(v[0], "check-data") || if_same(v[0], "chk-data")) {
        if (v.size() != 2) {
            KT_console::print("Usage: check-data <problem id>\n");
            return 1;
        }
        if (v[1][0] == 'p' || v[1][0] == 'P' || v[1][0] == '#') v[1] = v[1].substr(1);
        if (!check_all_in_range(v[1], '0', '9')) {
            KT_console::print("Usage: check-data <problem id>\n");
            return 1;
        }
        int prob_id = str_to_int(v[1]);
        if (KT::get_prob_name(prob_id) == "<NotFound>") {
            KT_console::print("Problem not found!\n");
            return 1;
        }
        KT::problem p;
        p.init(prob_id, KT_dbfile::prob_config(prob_id));
        if (p.std.empty()) {
            KT_console::print("The problem didn't have std!\n");
            return 0;
        }
        int AC = 0, UnAC = 0, JF = 0;
        for (unsigned i = 0; i < p.std.size(); ++i) {
            KT_console::print("Checking data with std[" + to_string(i + 1) + "]: " + p.std[i].file + "\n", "purple");
            string ret = submit(prob_id, KT_dbfile::prob_std(prob_id, p.std[i].file));
            if (ret == "Accepted") ++AC;
            else if (ret == "Unaccepted") ++UnAC;
            else ++JF;
        }
        string color = UnAC ? "red" : "green";
        KT_console::print("Checked " + to_string(p.std.size()) + " std(s), " + to_string(AC + UnAC) + " finished\n", color);
        KT_console::print(to_string(AC) + " of " + to_string(AC + UnAC) + " finished stds outputed the same answer as the data without throwing errors.\n", color);
        return 0;
    }
    if (if_same(v[0], "load")) {
        if (v.size() > 1 && if_same(v[1], "in")) {
            if (v.size() != 3) {
                KT_console::print("Usage: load in <file>\n");
                return 1;
            }
            if (!check_file_exist(v[2])) {
                KT_console::print("File \"%s\" isn't exists!\n", v[2].c_str());
                return 1;
            }
            int prob_id = KT::get_problem_total() + 1000;
            KT::load_in(prob_id, v[2]);
            printf("Load in successfully!\nP%d %s\n", prob_id, KT::get_prob_name(prob_id).c_str());
            return 0;
        }
        if (v.size() > 1 && if_same(v[1], "out")) {
            if (v.size() != 4) {
                KT_console::print("Usage: load out <problem id> <file>\n");
                return 1;
            }
            if (v[2][0] == 'p' || v[2][0] == 'P' || v[2][0] == '#') v[2] = v[2].substr(1);
            if (!check_all_in_range(v[2], '0', '9')) {
                KT_console::print("Usage: load out <problem id> <file>\n");
                return 1;
            }
            int prob_id = str_to_int(v[2]);
            if (KT::get_prob_name(prob_id) == "<NotFound>") {
                KT_console::print("Problem not found!\n");
                return 1;
            }
            if (!check_file_writable(v[3])) {
                KT_console::print("File \"%s\" isn't writable!\n", v[3].c_str());
                return 1;
            }
            printf("P%d %s\n", prob_id, KT::get_prob_name(prob_id).c_str());
            string ProblemContent = KT::load_out(prob_id);
            KT_console::print("\nWriting to file...");
            KT_sys::write_file(v[3], ProblemContent);
            KT_console::print("\nLoad out successfully!\n");
            return 0;
        }
        KT_console::print("Usage: load in <input ktp file>\n"
                          "  <or> load out <problem id> <output ktp file>");
        return 1;
    }
    if (if_same(v[0], "erase")) {
        if (v.size() == 1) {
            KT_console::print("Usage: erase p <problem id>\n");
            return 1;
        }
        if (if_same(v[1], "p") || if_same(v[1], "prob") || if_same(v[1], "problem")) {
            if (v.size() != 3) {
                KT_console::print("Usage: erase p <problem id>\n");
                return 1;
            }
            if (v[2][0] == 'p' || v[2][0] == 'P' || v[2][0] == '#') v[2] = v[2].substr(1);
            if (!check_all_in_range(v[2], '0', '9')) {
                KT_console::print("Usage: erase p <problem id>\n");
                return 1;
            }
            int prob_id = str_to_int(v[2]);
            if (KT::get_prob_name(prob_id) == "<NotFound>") {
                KT_console::print("Problem not found!\n");
                return 1;
            }
            KT_console::print("Sorry, the function isn't completed.\n", "light red");
            return 0;
        }
        KT_console::print("Usage: erase p <problem id>\n");
        return 1;
    }
    if (if_same(v[0], "QAQ")) {
        KT_console::print("Do not ");
        KT_console::print("QAQ", "pink");
        KT_console::print(" to a OJ!\n");
        return 0;
    }
    if (if_same(v[0], "QWQ")) {
        KT_console::print("Do not ");
        KT_console::print("QWQ", "pink");
        KT_console::print(" to a OJ!\n");
        return 0;
    }
    if (if_same(v[0], "help")) {
        KT_console::print("KTOJ", "light blue");
        KT_console::print(" - Help Page\n"
                          "命令解释        | 命令英文\n"
                          "----------------|------------------------------------------\n"
                          "退出 KTOJ       | exit [<exit code>]\n"
                          "提交题目        | submit <problem id> <program file>\n"
                          "展示题目列表    | show p list\n"
                          "展示题目        | show p <problem id>\n"
                          "题目数据检验    | check-data <problem id>\n"
                          "显示评测记录    | show s <submission id>\n"
                          "显示 KT-TF 文件 | show kt-tf <KT-TF file>\n"
                          "导入题目        | load in <input ktp file>\n"
                          "导出题目        | load out <problem id> <output ktp file>\n"
                          "删除题目        | erase p <problem id> "
                        );
        KT_console::print("(尚未完善)\n", "light red");
        KT_console::print("卖萌 1          | qaq\n"
                          "卖萌 2          | qwq\n"
                          "\n"
                          "大部分情况下, KTOJ 不对大小写敏感\n"
                          "在所有命令中, p 一般可以用 prob 或 problem 代替, s 一般可以用 sub 或 submission 代替\n"
                          "所有的 <problem id> 可以是一串数字，也可以带有前缀 'P' 或 '#'\n"
                          "所有的 <submission id> 可以是一串数字，也可以带有前缀 'S', 'R' 或 '#'\n"
                          "\n"
                        );
        KT_console::print("Powered by brealid(luogu - U63720)\n"
                          "All rights reserved.\n", 
                          "gray");
        return 0;
    }
    return 3;
}

int execute(string cmd) {
    vector<string> v_cmd = divide_command(cmd);
    int nRet = execute(v_cmd);
    if (nRet == 3)
        printf("Unrecognized option \"%s\"\n", cmd.c_str());
    return nRet;
}

int main(int argc, char **argv) {
    if (argc != 1) {
        string cmd;
        for (int i = 1; i < argc; ++i) {
            string s = argv[i];
            if (s.find(' ') != s.npos) s = "\"" + s + "\"";
            cmd += s + " ";
        }
        cmd.pop_back();
        return execute(cmd);
    }
    while (true) {
        KT_console::print("> ");
        string cmd;
        getline(cin, cmd);
        execute(cmd);
    }
    return 0;
}