## All supporting operations

```plain
KTOJ - Help Page
命令解释        | 命令英文
----------------|------------------------------------------
退出 KTOJ       | exit [<exit code>]
提交题目        | submit <problem id> <program file>
展示题目列表    | show p list
展示题目        | show p <problem id>
题目数据检验    | check-data <problem id>
显示评测记录    | show s <submission id>
显示 KT-TF 文件 | show kt-tf <KT-TF file>
导入题目        | load in <input ktp file>
导出题目        | load out <problem id> <output ktp file>
卖萌 1          | qaq
卖萌 2          | qwq

大部分情况下, KTOJ 不对大小写敏感
在所有命令中, p 一般可以用 prob 或 problem 代替, s 一般可以用 sub 或 submission 代替
所有的 <problem id> 可以是一串数字，也可以带有前缀 'P' 或 '#'
所有的 <submission id> 可以是一串数字，也可以带有前缀 'S', 'R' 或 '#'

Powered by brealid(luogu - U63720)
All rights reserved.
```