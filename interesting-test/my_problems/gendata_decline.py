import random
import genlib

def gen_input(file_name: str, n: int, k: int, tree_type: str):
    TIMES = 3 + 2 * random.random()
    if n == 500000 and k == 1:
        TIMES = 10
    with open(file_name, "w") as f:
        y = random.randint(1, n)
        x = random.randint(2, int(n ** 0.5))
        print(n, y, k, x, file = f)
        tree = genlib.gen_tree(n, tree_type)
        for i in range(n - 1):
            print(tree[i][0], tree[i][1], file = f)
        for i in range(k):
            per = random.random() ** TIMES
            print(random.randint(1, n), int(per * (n - 2) + 2), file = f)

genlib.start("decline", "ex_decline")
genlib.gen_data(gen_input, 1, 10, 2, "random")
genlib.gen_data(gen_input, 2, 10, 1, "chain")
genlib.gen_data(gen_input, 3, 5000, 150, "random")
genlib.gen_data(gen_input, 4, 5000, 200, "chain")
genlib.gen_data(gen_input, 5, 5000, 100, "chrysanthemum")
genlib.end()
genlib.start("decline")
genlib.gen_data(gen_input, 1, 10, 1, "random")
genlib.gen_data(gen_input, 2, 10, 1, "random")
genlib.gen_data(gen_input, 3, 10, 10, "random")
genlib.gen_data(gen_input, 4, 10, 10, "random")
genlib.gen_data(gen_input, 5, 10, 10, "random")
genlib.gen_data(gen_input, 6, 10, 10, "random")
genlib.gen_data(gen_input, 7, 500, 1, "random")
genlib.gen_data(gen_input, 8, 500, 1, "random")
genlib.gen_data(gen_input, 9, 3000, 2, "random")
genlib.gen_data(gen_input, 10, 3000, 2, "random")
genlib.gen_data(gen_input, 11, 3000, 60, "chain")
genlib.gen_data(gen_input, 12, 3000, 180, "chain")
genlib.gen_data(gen_input, 13, 500000, 1, "random")
genlib.gen_data(gen_input, 14, 500000, 1, "random")
genlib.gen_data(gen_input, 15, 500000, 50000, "chain")
genlib.gen_data(gen_input, 16, 500000, 200000, "chain")
genlib.gen_data(gen_input, 17, 500000, 80000, "random")
genlib.gen_data(gen_input, 18, 500000, 100000, "chrysanthemum")
genlib.gen_data(gen_input, 19, 500000, 100000, "random")
genlib.gen_data(gen_input, 20, 500000, 500000, "random")
genlib.end()