import random, genlib

def do_not_genIn(f_name: str):
    pass

def genIn_random(f_name: str, nm_range: tuple, pct_range: tuple = (0, 1)):
    with open(f_name, 'w') as f:
        for i in range(5):
            n = random.randint(*nm_range)
            m = random.randint(*nm_range)
            pct = random.uniform(*pct_range)
            print(n, m, file = f)
            for i in range(n):
                for j in range(m):
                    print(int(genlib.test(pct)), end = '', file = f)
                print(file = f)

genlib.start('bintable')
for i in range(1, 9):
    genlib.gen_data(do_not_genIn, i)
for i in range(9, 17):
    genlib.gen_data(genIn_random, i, (8, 10))
for i in range(17, 20):
    genlib.gen_data(do_not_genIn, i)
for i in range(20, 33):
    genlib.gen_data(genIn_random, i, (i * 3 - 6, i * 3 + 4))
for i in range(33, 45):
    genlib.gen_data(genIn_random, i, (i * 8 - 120, i * 8 + 148))
for i in range(45, 51):
    genlib.gen_data(genIn_random, i, (i * 10 - 20, i * 10))
genlib.end()