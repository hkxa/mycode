#include "testlib.h"

const int N = 2000 + 7;
int n, x[N], y[N], order[N];
bool occured[N];

double calc_error(double x, double y) {
    return abs(x - y) / std::max(double(1), y);
}

double dist(int i, int j) {
    long long x_diff = x[i] - x[j], y_diff = y[i] - y[j];
    return sqrt(x_diff * x_diff + y_diff * y_diff);
}

int main(int argc, char* argv[]) {
    registerTestlibCmd(argc, argv);
    double userDist = ouf.readDouble(), expectedDist = ans.readDouble();
    if (calc_error(userDist, expectedDist) > 1e-6)
        quitf(_wa, "The answer is wrong: expected = %.9lf, found = %.9lf", expectedDist, userDist);
    
    n = inf.readInt();
    for (int i = 0; i <= n; ++i) {
        x[i] = inf.readInt();
        y[i] = inf.readInt();
    }
    for (int i = 1; i <= n; ++i) order[i] = ouf.readInt();
    for (int i = 1; i <= n; ++i) {
        if (order[i] < 1 || order[i] > n)
            quitp(0.6, "Q1 is right; answer in Q2 are not in range[1, n].");
        if (occured[order[i]])
            quitp(0.6, "Q1 is right; answer in Q2 is duplicated.");
        occured[order[i]] = true;
    }
    double totalDist = dist(order[0], order[n]);
    for (int i = 1; i <= n; ++i)
        totalDist += dist(order[i - 1], order[i]);
    if (calc_error(totalDist, userDist) > 1e-6)
        quitp(0.6, "Q1 is right; answer in Q2 calced is %.9lf, expected %.9lf", totalDist, userDist);
    quitf(_ok, "The answer is correct.");
}