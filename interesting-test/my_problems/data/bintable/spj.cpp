#include "testlib.h"

const int N = 1000 + 7;
int n, m, k;
std::pair<int, int> p[3], &p1 = p[0], &p2 = p[1], &p3 = p[2];
std::string table[N];
const char SUM = '0' + '1';
std::string res;
// 0≤k≤nm nm<k≤3nm 3nm<k fail_to_do

int score = 2;
char grade;
void evaluate(char g) {
    if (g == 'I') grade = g;
    if (grade == 'I') return;
    if (g == 'F') grade = g;
    if (grade == 'F') return;
    if (g == 'E') grade = g;
    if (grade == 'E') return;
    if (g == 'P') grade = g;
    if (grade == 'P') return;
}

int main(int argc, char* argv[]) {
    registerTestlibCmd(argc, argv);
    for (int i = 1; i <= 5; ++i) {
        n = inf.readInt(), m = inf.readInt();
        for (int i = 1; i <= n; ++i) table[i] = " " + inf.readToken();
        k = ouf.readInt();
        grade = 'A';
        if (k < 0) evaluate('I');
        if (k > n * m && k <= 3 * n * m) evaluate('P');
        if (k > 3 * n * m) evaluate('E');
        for (int i = 1; i <= k; ++i) {
            p1.first = ouf.readInt(); p1.second = ouf.readInt();
            p2.first = ouf.readInt(); p2.second = ouf.readInt();
            p3.first = ouf.readInt(); p3.second = ouf.readInt();
            if (p1.first < 1 || p1.first > n || p1.second < 1 || p1.second > m) {
                evaluate('I');
                continue;
            }
            if (p2.first < 1 || p2.first > n || p2.second < 1 || p2.second > m) {
                evaluate('I');
                continue;
            }
            if (p3.first < 1 || p3.first > n || p3.second < 1 || p3.second > m) {
                evaluate('I');
                continue;
            }
            sort(p, p + 3);
            if (p1.first == p2.first) {
                if (p1.first + 1 != p3.first) {
                    evaluate('I');
                    continue;
                }
                if (p1.second + 1 != p2.second) {
                    evaluate('I');
                    continue;
                }
                if (p1.second != p3.second && p2.second != p3.second) {
                    evaluate('I');
                    continue;
                }
            } else if (p2.first == p3.first) {
                if (p1.first + 1 != p2.first) {
                    evaluate('I');
                    continue;
                }
                if (p2.second + 1 != p3.second) {
                    evaluate('I');
                    continue;
                }
                if (p1.second != p3.second && p1.second != p2.second) {
                    evaluate('I');
                    continue;
                }
            } else evaluate('I');
            table[p1.first][p1.second] = SUM - table[p1.first][p1.second];
            table[p2.first][p2.second] = SUM - table[p2.first][p2.second];
            table[p3.first][p3.second] = SUM - table[p3.first][p3.second];
        }
        for (int i = 1; i <= n; ++i)
            for (int j = 1; j <= m; ++j)
                if (table[i][j] != '0') evaluate('F');
        res += grade;
        if (grade == 'A') continue;
        if (grade == 'P') score = std::min(score, 1);
        else score = 0;
    }
    if (score == 2) quitf(_ok, "AC: Result are [%s]\n", res.c_str());
    if (score == 1) quitp(0.5, "PC: Result are [%s]\n", res.c_str());
    if (score == 0) quitf(_wa, "WA: Result are [%s]\n", res.c_str());
}