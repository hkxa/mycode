#include <bits/stdc++.h>

long long x, y;

int main() {
#ifndef ONLINE_JUDGE
    freopen("jfrog.in", "r", stdin);
    freopen("jfrog.out", "w", stdout);
#endif
    std::cin >> x >> y;
    std::cout << y * y - x * x << std::endl;
    return 0;
}