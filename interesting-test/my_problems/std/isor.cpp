/*************************************
 * @problem:      保序回归，约束图为链，贪心.
 * @user_name:    brealid.
 * @time:         2020-10-30.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

namespace isotonic_regression {
#define IGNORE_VALUE_UNUSED(x) ((void)(x))
    namespace L0 {
        // L = 0: 人口普查
        pair<vector<double>, double> solve(size_t n, vector<int> w, vector<int> y)  {
            IGNORE_VALUE_UNUSED(y);
            long long ans = 0;
            for (size_t i = 0; i < n; ++i) ans += w[i];
            return make_pair(vector<double>(n, 0), (double)ans);
        }
    }
    namespace L1 {
        struct sequence {
            priority_queue<pair<int, int>, vector<pair<int, int> >, less<pair<int, int> > > q1;
            priority_queue<pair<int, int>, vector<pair<int, int> >, greater<pair<int, int> > > q2;
            long long q1_sum, q2_sum;
            sequence() : q1_sum(0), q2_sum(0) {}
            void maintain() {
                while (q1_sum < q2_sum) {
                    q1_sum += q2.top().second;
                    q2_sum -= q2.top().second;
                    q1.push(q2.top());
                    q2.pop();
                }
                while (q1_sum - q1.top().second >= q2_sum + q1.top().second) {
                    q1_sum -= q1.top().second;
                    q2_sum += q1.top().second;
                    q2.push(q1.top());
                    q1.pop();
                }
            }
            void insert(int w, int y) {
                if (q1.empty() || y <= q1.top().first) {
                    q1.push(make_pair(y, w));
                    q1_sum += w;
                } else {
                    q2.push(make_pair(y, w));
                    q2_sum += w;
                }
                maintain();
            }
            // arg 'seq' whould be cleared
            void merge_from(sequence &seq) {
                seq.q1_sum = seq.q2_sum = 0;
                while (!seq.q1.empty()) {
                    if (q1.empty() || seq.q1.top().first <= q1.top().first) {
                        q1.push(seq.q1.top());
                        q1_sum += seq.q1.top().second;
                    } else {
                        q2.push(seq.q1.top());
                        q2_sum += seq.q1.top().second;
                    }
                    seq.q1.pop();
                }
                while (!seq.q2.empty()) {
                    if (q1.empty() || seq.q2.top().first <= q1.top().first) {
                        q1.push(seq.q2.top());
                        q1_sum += seq.q2.top().second;
                    } else {
                        q2.push(seq.q2.top());
                        q2_sum += seq.q2.top().second;
                    }
                    seq.q2.pop();
                }
                maintain();
            }
            int qMiddle_down() const {
                return q1.top().first;
            }
            int qMiddle_up() const {
                if (q1_sum == q2_sum) return q2.top().first;
                else return q1.top().first;
            }
        };
        struct element {
            int s, bound_down;
            sequence seq;
            element() {}
            element(int S, int W, int Y) : s(S) {
                seq.insert(W, Y);
                bound_down = seq.qMiddle_down();
            }
            void UpdMax_bound_down(int new_bound) {
                bound_down = max(bound_down, new_bound);
            }
            bool operator < (const element &b) const {
                return seq.qMiddle_up() < b.bound_down;
            }
        };
        // L = 1: 单调栈(贪心) + 中位数
        pair<vector<double>, double> solve(size_t n, vector<int> w, vector<int> y) {
            element *sta = new element[n + 1];
            size_t top = 0;
            for (size_t i = 0; i < n; ++i) {
                sta[top++] = element(i, w[i], y[i]);
                if (top >= 2) sta[top - 1].UpdMax_bound_down(sta[top - 2].bound_down);
                while (top >= 2 && sta[top - 1] < sta[top - 2]) {
                    element &e1 = sta[top - 1], &e2 = sta[top - 2];
                    e2.seq.merge_from(e1.seq);
                    e2.bound_down = e2.seq.qMiddle_down();
                    if (top >= 3) e2.UpdMax_bound_down(sta[top - 3].bound_down);
                    --top;
                }
            }
            sta[top].s = n;
            vector<double> ret(n);
            long long ans = 0;
            for (size_t i = 0; i < top; ++i) {
                double now_y = sta[i].bound_down;
                for (int p = sta[i].s; p < sta[i + 1].s; ++p) {
                    ret[p] = now_y;
                    ans += abs(now_y - y[p]) * w[p];
                }
            }
            return make_pair(ret, (double)ans);
        }
    }
    namespace L2 {
        struct element {
            int s;
            long long w, y; // w means actual_w*y
            element() {}
            element(int S, long long W, long long Y) : s(S), w(W), y(Y) {}
            bool operator < (const element &b) const {
                return (double)y * b.w < (double)b.y * w;
            }
        };
        // L = 2: 单调栈(贪心) + 加权平均数
        pair<vector<double>, double> solve(size_t n, vector<int> w, vector<int> y) {
            element *sta = new element[n + 1];
            size_t top = 0;
            for (size_t i = 0; i < n; ++i) {
                sta[top++] = element(i, w[i], (long long)y[i] * w[i]);
                while (top >= 2 && sta[top - 1] < sta[top - 2]) {
                    element &e1 = sta[top - 1], &e2 = sta[top - 2];
                    e2 = element(e2.s, e1.w + e2.w, e1.y + e2.y);
                    --top;
                }
            }
            sta[top].s = n;
            vector<double> ret(n);
            double ans = 0;
            for (size_t i = 0; i < top; ++i) {
                double now_y = (double)sta[i].y / sta[i].w;
                for (int p = sta[i].s; p < sta[i + 1].s; ++p) {
                    ret[p] = now_y;
                    ans += (now_y - y[p]) * (now_y - y[p]) * w[p];
                }
            }
            return make_pair(ret, ans);
        }
    }
};

signed main() {
    freopen("isor.in", "r", stdin);
    freopen("isor.out", "w", stdout);
    int n, p;
    scanf("%d%d", &n, &p);
    vector<int> y(n), w(n);
    for (int i = 0; i < n; ++i) scanf("%d", &w[i]);
    for (int i = 0; i < n; ++i) scanf("%d", &y[i]);
    pair<vector<double>, double> ans;
    switch (p) {
        case 0 : ans = isotonic_regression::L0::solve(n, w, y); break;
        case 1 : ans = isotonic_regression::L1::solve(n, w, y); break;
        case 2 : ans = isotonic_regression::L2::solve(n, w, y); break;
    }
    printf("%.9lf\n", ans.second);
    for (int i = 0; i < n; ++i)
        printf("%.9lf ", ans.first[i]);
    printf("\n");
    return 0;
}