#include <bits/stdc++.h>
using namespace std;
typedef long long int64;
 
const int N = 2e5 + 7, P = 1e9 + 7;
 
int n;
bool notp[N];
int prime[N], pcnt;
 
pair<double, int64> f[N], g[N];
 
void getPrime() {
    for (int i = 2; i <= n; i++) {
        if (!notp[i]) prime[++pcnt] = i;
        for (int j = i + i; j <= n; j += i)
            notp[j] = true;
    }
}
 
signed main() {
    freopen("lcm.in", "r", stdin);
    freopen("lcm.out", "w", stdout);
    cin >> n;
    getPrime();
    f[0] = make_pair(0, 1);
    for (int i = 1; i <= min(pcnt, 240); ++i) {
        for (int T = prime[i], gain = 2; T <= n; ++gain, T = (T + 1) * prime[i])
            for (int j = n; j >= T; j--)
                g[j] = max(g[j], make_pair(f[j - T].first + log(gain), f[j - T].second * gain % P));
        for (int j = n; j >= prime[i]; j--)
            f[j] = g[j];
    }
    pair<double, int64> ans;
    for (int i = 0; i <= n; i++) ans = max(ans, f[i]);
    cout << ans.second << '\n';
    return 0;
}