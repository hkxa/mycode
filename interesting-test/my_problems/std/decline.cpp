#include <bits/stdc++.h>
using namespace std;

const int N = 5e5 + 7;

int n, y, k, x;
bool ishave[N];
int value[N];
vector<int> G[N];
int f[N], ans = 0;

void dfs(int p, int fa) {
    f[p] = ishave[p] ? value[p] : 0x3f3f3f3f;
    for (size_t i = 0; i < G[p].size(); ++i) {
        int v = G[p][i];
        if (v == fa) continue;
        dfs(v, p);
        f[p] = min(f[p], f[v] - 1);
    }
    if (f[p] == 1 && p != y) { // 再不中继，信号就没了
        ++ans;
        f[p] = x;
    }
}

int main() {
    freopen("decline.in", "r", stdin);
    freopen("decline.out", "w", stdout);
    scanf("%d%d%d%d", &n, &y, &k, &x);
    for (int i = 1, u, v; i < n; ++i) {
        scanf("%d%d", &u, &v);
        G[u].push_back(v);
        G[v].push_back(u);
    }
    for (int i = 1, p, d; i <= k; ++i) {
        scanf("%d%d", &p, &d);
        if (ishave[p]) {
            value[p] = min(value[p], d);
        } else {
            ishave[p] = 1;
            value[p] = d;
        }
    }
    dfs(y, 0);
    printf("%d", ans);
    return 0;
}