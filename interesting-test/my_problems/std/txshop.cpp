#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#define getchar() (i_P1 == i_P2 && (i_P2 = (i_P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), i_P1 == i_P2) ? EOF : *i_P1++)
char BUF[1 << 21], *i_P1 = BUF, *i_P2 = BUF;
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 2e5 + 7;

int n, q, a[N];

namespace segt {
    struct segtree_node_info {
        int fir_min, sec_min;
        int cnt_min;
        int64 sum;
    } tr[N << 2];

    void pushdown(int u) {
        int ls = u << 1, rs = ls | 1, val = tr[u].fir_min;
        if (val > tr[ls].fir_min) {
            tr[ls].sum += (int64)(val - tr[ls].fir_min) * tr[ls].cnt_min;
            tr[ls].fir_min = val;
        }
        if (val > tr[rs].fir_min) {
            tr[rs].sum += (int64)(val - tr[rs].fir_min) * tr[rs].cnt_min;
            tr[rs].fir_min = val;
        }
    }

    void pushup(int u) {
        int ls = u << 1, rs = ls | 1;
        tr[u].sum = tr[ls].sum + tr[rs].sum;
        if (tr[ls].fir_min == tr[rs].fir_min) {
            tr[u].fir_min = tr[ls].fir_min;
            tr[u].cnt_min = tr[ls].cnt_min + tr[rs].cnt_min;
            tr[u].sec_min = min(tr[ls].sec_min, tr[rs].sec_min);
        } else {
            tr[u].fir_min = tr[rs].fir_min;
            tr[u].cnt_min = tr[rs].cnt_min;
            tr[u].sec_min = min(tr[rs].sec_min, tr[ls].fir_min);
        }
    }

    void build(int u, int l, int r) {
        if (l == r) {
            tr[u].sum = tr[u].fir_min = a[l];
            tr[u].sec_min = 1e9 + 7;
            tr[u].cnt_min = 1;
            return;
        }
        int mid = (l + r) >> 1;
        build(u << 1, l, mid);
        build(u << 1 | 1, mid + 1, r);
        pushup(u);
    }

    void replace_max(int u, int l, int r, int mr, int val) {
        if (r <= mr) {
            if (val < tr[u].fir_min) return;
            if (val < tr[u].sec_min) {
                tr[u].sum += (int64)(val - tr[u].fir_min) * tr[u].cnt_min;
                tr[u].fir_min = val;
                return;
            }
        }
        int mid = (l + r) >> 1;
        pushdown(u); 
        replace_max(u << 1, l, mid, mr, val);
        if (mr > mid) replace_max(u << 1 | 1, mid + 1, r, mr, val);
        pushup(u);
    }

    int find_next_lesseq(int u, int l, int r, int ml, int val) {
        if (l == r) return l;
        int mid = (l + r) >> 1, ls = u << 1, rs = ls | 1;
        pushdown(u); 
        if (ml <= mid && val >= tr[ls].fir_min) return find_next_lesseq(ls, l, mid, ml, val);
        else return find_next_lesseq(rs, mid + 1, r, ml, val);
    }

    int find_next_lesseq(int pl, int value) {
        if (value < tr[1].fir_min) return -1;
        return find_next_lesseq(1, 1, n, pl, value);
    }

    int pos, *mon;
    void try_to_expand_right(int u, int l, int r, int ml) {
        if (l >= ml) {
            if (*mon < tr[u].fir_min) return;
            if (*mon >= tr[u].sum) {
                *mon -= tr[u].sum;
                pos = r;
                return;
            }
        }
        int mid = (l + r) >> 1;
        pushdown(u);
        if (ml <= mid) try_to_expand_right(u << 1, l, mid, ml);
        if (pos >= mid) try_to_expand_right(u << 1 | 1, mid + 1, r, ml);
    }
    int try_to_expand_right(int *money, int pos_begin) {
        pos = pos_begin;
        mon = money;
        try_to_expand_right(1, 1, n, pos_begin);
        return pos;
    }
}

signed main() {
#ifndef ONLINE_JUDGE
    freopen("txshop.in", "r", stdin);
    freopen("txshop.out", "w", stdout);
#endif
    kin >> n >> q;
    for (int i = 1; i <= n; ++i) kin >> a[i];
    segt::build(1, 1, n);
    for (int i = 1, opt, x, y; i <= q; ++i) {
        kin >> opt >> x >> y;
        if (opt == 1) segt::replace_max(1, 1, n, x, y);
        else {
            int ans = 0;
            while (x <= n) {
                x = segt::find_next_lesseq(x, y);
                if (!~x) break;
                int t = segt::try_to_expand_right(&y, x);
                ans += t - x + 1;
                x = t + 1;
            }
            kout << ans << '\n';
        }
    }
    return 0;
}