#include <bits/stdc++.h>

const int N = 2000 + 7;
int n;
struct mailbox { 
    int id, x, y;
    bool operator < (const mailbox &b) const { return y < b.y; }
} a[N];
struct dp_box {
    double val;
    int from;
    dp_box() {}
    dp_box(double V, int F) : val(V), from(F) {}
} f[N][N];

double dist(int i, int j) {
    long long x_diff = a[i].x - a[j].x, y_diff = a[i].y - a[j].y;
    return sqrt(x_diff * x_diff + y_diff * y_diff);
}

int path[2][N], cnt[2];

void make_path(int i, int j, bool state) {
    path[state][++cnt[state]] = a[i].id;
    if (j == i - 1) state = !state;
    if (i == 1) return;
    make_path(i - 1, f[i][j].from, state);
}

int main() {
#ifndef ONLINE_JUDGE
    freopen("btour.in", "r", stdin);
    freopen("btour.out", "w", stdout);
#endif
    std::cin >> n;
    for (int i = 0; i <= n; ++i) {
        a[i].id = i;
        std::cin >> a[i].x >> a[i].y;
    }
    std::sort(a + 1, a + n + 1);
    f[1][0].val = dist(1, 0);
    for (int i = 1; i < n; ++i) {
        double dist_near = dist(i, i + 1);
        f[i + 1][i].val = 1e18;
        for (int j = 0; j < i; ++j) {
            f[i + 1][j] = dp_box(f[i][j].val + dist_near, j);
            double dist_cur = f[i][j].val + dist(j, i + 1);
            if (f[i + 1][i].val > dist_cur) {
                f[i + 1][i].val = dist_cur;
                f[i + 1][i].from = j;
            }
        }
    }
    dp_box ans(1e18, 0);
    for (int i = 0; i < n; ++i) {
        double dist_cur = f[n][i].val + dist(n, i);
        if (ans.val > dist_cur) {
            ans.val = dist_cur;
            ans.from = i;
        }
    }
    make_path(n, ans.from, 0);
    std::cout << std::fixed << std::setprecision(9) << ans.val << std::endl;
    for (int i = cnt[0]; i >= 1; --i) std::cout << path[0][i] << ' ';
    for (int i = 1; i <= cnt[1]; ++i) std::cout << path[1][i] << ' ';
    std::cout << std::endl;
    return 0;
}