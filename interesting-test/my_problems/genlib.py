import random
import os

problemName = "Not-Set"
dataPrefix = "Not-Set"
std = "Not-Compiled"

# p 的概率返回 True, (1 - p) 的概率返回 False
def test(p: float):
    return random.random() <= p

def random_str(len: int, seq: str="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz") -> str:
    name = str()
    for i in range(len):
        name += random.choice(seq)
    return name

def compile(prog: str) -> str:
    exe_name = random_str(8) + ".exe"
    os.system("g++ " + prog + " -o " + exe_name + " -Wall -Wl,--stack=536870912 -DGEN_DATA")
    return exe_name

def run(prog: str, inf: str, ouf: str):
    os.system("copy " + inf + " " + problemName + ".in")
    os.system(prog)
    os.system("copy " + problemName + ".out " + ouf)

def gen_data(input_generator, data_id: int, *info):
    print("generating data No.%d"%data_id)
    inf_name = "data\\" + problemName + "\\" + dataPrefix + str(data_id) + ".in" 
    ouf_name = "data\\" + problemName + "\\" + dataPrefix + str(data_id) + ".out" 
    input_generator(inf_name, *info)
    run(std, inf_name, ouf_name)

def start(pName: str, dPrev: str = "same as pName"):
    global problemName, dataPrefix, std
    problemName = pName
    if dPrev == "same as pName":
        dataPrefix = problemName
    else:
        dataPrefix = dPrev
    std = compile("std\\" + problemName + ".cpp")

def end():
    os.system("rm " + problemName + ".in " + problemName + ".out")
    os.system("rm " + std)

class UnionFindSet:
    def __init__(self, n):
        self.fa = [i for i in range(n + 1)]
    
    def find(self, u):
        if u == self.fa[u]:
            return u
        self.fa[u] = self.find(self.fa[u])
        return self.fa[u]
    
    def is_family(self, u, v):
        return self.find(u) == self.find(v)
    
    def merge(self, u, v):
        self.fa[self.find(u)] = self.find(v)

def gen_tree_random(n: int):
    s = set()
    ufs = UnionFindSet(n)
    while len(s) < n - 1:
        u = random.randint(1, n)
        v = random.randint(1, n)
        if u == v or (u, v) in s or (v, u) in s or ufs.is_family(u, v):
            continue
        s.add((u, v))
        ufs.merge(u, v)
    return list(s)

def gen_tree_chain(n: int):
    x = [i for i in range(1, n + 1)]
    random.shuffle(x)
    s = []
    for i in range(n - 1):
        if random.randint(0, 1):
            s.append((x[i], x[i + 1]))
        else:
            s.append((x[i + 1], x[i]))
    random.shuffle(s)
    return s

def gen_tree_chrysanthemum(n: int):
    root = random.randint(1, n)
    s = []
    for i in range(1, n + 1):
        if i == root:
            continue
        if random.randint(0, 1):
            s.append((root, i))
        else:
            s.append((i, root))
    random.shuffle(s)
    return s

def gen_tree_complete_binary_tree(n: int):
    return None

def gen_tree(n: int, tree_type: str = "random"):
    tree_type = tree_type.lower()
    if tree_type == "random" or tree_type == "suiji":
        return gen_tree_random(n)
    elif tree_type == "chain" or tree_type == "lian":
        return gen_tree_chain(n)
    elif tree_type == "chrysanthemum" or tree_type == "juhua":
        return gen_tree_chrysanthemum(n)
    elif tree_type == "complete-binary-tree" or tree_type == "wanquanerchashu":
        return gen_tree_complete_binary_tree(n)
    else:
        return None