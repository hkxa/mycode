import random, genlib

def genIn(inf: str, lim: int, sep: int):
    with open(inf, 'w') as f:
        x, y = random.randint(0, lim), random.randint(0, lim)
        while abs(x - y) > sep:
            x, y = random.randint(0, lim), random.randint(0, lim)
        print(min(x, y), max(x, y), file = f)

def force_genIn(inf: str, x: int, y: int):
    with open(inf, 'w') as f:
        print(x, y, file = f)

genlib.start('jfrog')
for i in range(1, 6):
    genlib.gen_data(genIn, i, 1000, 1000)
genlib.gen_data(force_genIn, 6, 11, 1000)
for i in range(7, 12):
    genlib.gen_data(genIn, i, 1000000, 1000)
genlib.gen_data(force_genIn, 12, 999000, 1000000)
for i in range(13, 20):
    genlib.gen_data(genIn, i, 1000000000, 1000000000)
genlib.gen_data(force_genIn, 20, 27, 1000000000)
genlib.end()