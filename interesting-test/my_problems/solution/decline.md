## decline solution
### 算法 1: 30 pts
$n \le 10$

dfs，乱搞等等

### 算法 2: 60 pts
$n \le 3000$

DP，``f[i][j]`` 表示结点 $i$ 剩余 $j$ 的信号强度所需加入的最小中转站数。

$\Theta(n^2)$

如果没写好，容易写成 $\Theta(n^3)$，拿 $n\le500$ 这档的分（为 40 pts）

### 算法 3: 30 pts
$k = 1$

直接求信号发射处到根节点的距离（设为 $d$），则答案为 $\lfloor\frac{d-1}{x-1}\rfloor$

结合算法 2 可以获得 70 pts

### 算法 4: 20 pts
树退化为链

接近正解，从叶子到树根扫两遍（可能根节点有两条子链）即可。

两条子链可以分开处理。

$\Theta(n)$

结合算法 2,3 可以获得 80 pts

### 算法 5: 100 pts
树形 DP 

容易发现，两个大小为 $x,y$ 的信号只需要管理其中较小的一个。因为较大的信号威胁更迟，而且中继时两者会被中继为一样强度的信号。

所以对每个节点，可以用 $f_i$ 记录到达这个节点的信号强度的最小值。

转移：$f_u=\begin{cases}\min(\min\limits_{v\in son[u]}f_v-1, val[u])&u\ have\ signal\\\min\limits_{v\in son[u]}f_v-1&otherwise\end{cases}$

转移之后，如果 $f_u=1$ 且 $u\not=y$，那么必须使用一次中继，然后 $f_u$ 变为 $x$

$\Theta(n)$