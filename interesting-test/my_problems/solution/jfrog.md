## jfrog

设 $f_i$ 表示期望下从 $i-1$ 跳到 $i$ 所需要的步数，那么答案为 $\sum\limits_{l+1}^{r}f_i$

现在考虑 $f_i$ 的计算。

首先有 $f_1 = 1$。

$$f_i=\underbrace{\frac{1}{2}(1+f_{i-1}+f_i)}_\text{如果向左了一步}+\underbrace{\frac{1}{2}\times1}_\text{如果向右了一步}$$

移项，解得

$$f_i=f_{i-1}+2$$

求得通项

$$f_i=2i-1$$

求得和式

$$\sum_{l+1}^{r}f_i=r^2-l^2$$

时间复杂度 $\Theta(1)$