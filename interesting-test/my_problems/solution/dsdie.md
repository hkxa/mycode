## dsdie solution
为什么要对 $|x|$ 进行限制？  
只是想给忘记开 ``long long`` 的人一点分。（快夸良心出题人）

### 算法 1: 30 pts
暴力模拟

算法复杂度 $\Theta(n^2)$

### 算法 2: 15 pts 
没有 1 操作

开棵线段树，简单维护即可  
甚至可以使用前缀和与 ST 表，避免线段树（虽然节省不了多少码量

算法复杂度 $\Theta(n\log n)$

结合算法 1 可以获得 40 pts

### 算法 3: 15 pts 
没有 4 操作

Segment Tree Beats 入门题

2016国集论文“区间最值操作与历史最值问题”@吉如一 <https://gitee.com/hkxa/OI-documentary/blob/master/国家集训队论文1999-2019/国家集训队2016论文集.pdf>

算法复杂度 $\Theta(n\log^2n)$

结合算法 1, 2 可以获得 50 pts

### 算法 4: 15 pts 
没有 2, 3 操作

两棵普通线段树维护 $a[i]$ 与 $Z[i] - t * a[i]$，$t$ 表示时间。这样可以快速修改与查询，思想自行理会，很妙。

算法复杂度 $\Theta(n\log n)$

结合算法 1, 2, 3 可以获得 60 pts

### 算法 5: 60 pts
（可能的，没尝试过的）分块做法

算法复杂度 $\Theta(n\sqrt{n})$

结合算法 1, 2, 3 可以获得 75 pts

### 算法 6: 100 pts

2016国集论文“区间最值操作与历史最值问题”@吉如一 <https://gitee.com/hkxa/OI-documentary/blob/master/国家集训队论文1999-2019/国家集训队2016论文集.pdf>

**这里假设你已经理解了算法 3, 4**

考虑 2 棵 ``Segment Tree Beats`` 线段树维护 $X[i]$，$Y[i]$，2 棵普通线段树维护 $a[i]$ 与 $Z[i] - t * a[i]$

这里码量为：2 棵 ``Segment Tree Beats``，1 棵普通线段树

这样已经完美地解决了问题，我们考虑如何**减小码量**。

首先，对 ``Segment Tree Beats`` 进行封装，$Y[i]$ 取相反数按 $X[i]$ 的方式维护，节省一棵 ``Segment Tree Beats`` 的码量。

然后，放弃常数性能，将 $a[i]$ 与 $Z[i] - t * a[i]$ 大材小用地用封装的 ``Segment Tree Beats`` 进行维护。

最后，我们只剩下了 1 棵 ``Segment Tree Beats`` 的码量。

算法复杂度 $\Theta(n\log^2n)$