## C. 最大公倍数 (lcm) (2s, 512MiB)

### 题目背景
B 和 H 意识到 OI 逐渐式微（bushi），于是想转向数竞。现在摆在他们面前的，是数竞中一项基础的内容——数论。

你需要帮助他们解决一个关于 $\operatorname{lcm}$ 的问题

注：$\operatorname{lcm}(a,b)$ 为 $a,b$ 的最小公倍数。

### 题目描述
给出一个数 $n$，将它拆成一个长度由你决定（假设长度为 $k$）的序列 $a_i$，满足 $n=\sum\limits_{i=1}^{k}a_i$。

一个集合的 $\operatorname{lcm}$ 定义为集合中所有数的 $\operatorname{lcm}$（空集的 $\operatorname{lcm}$ 为 $1$）

一个序列的权值定义为：这个序列的所有子集中不同 $\operatorname{lcm}$ 的个数。

现在要求最大化你拆出来序列的权值，答案对 $10^9+7$ 取模

### 输入格式
一行一个正整数 $n$

### 输出格式
一行一个非负整数，表示最大权值对 $10^9+7$ 取模后的值

### 样例
#### 样例输入 #1
```plain
5
```
#### 样例输出 #1
```
4
```
#### 样例解释 #1

$5=2+3$，子集有 $4$ 个 $\varnothing,\{2\},\{3\},\{2,3\}$，其 $\operatorname{lcm}$ 分别为 $1,2,3,6$

### 提示说明

对于 $10\%$ 的数据，$n\le10$  
对于 $30\%$ 的数据，$n\le500$  
对于 $60\%$ 的数据，$n\le5000$  
对于 $100\%$ 的数据，$n\le1.5\times 10^5$，提交文件不得大于 100k

我们以文件形式提供了 $4$ 组**额外的**样例数据。