## 队列最值数据结构 (dsa)

### 题目背景

B: 午后的时光有些难熬，但是又不想研究太高深的算法。  
H: 要不我们看看简单的数据结构，比如 $\operatorname{stack}$, 能不能研究出一下儿什么新的花样？  
B: 我觉得可以啊！比如，如果在 $\operatorname{push}$, $\operatorname{pop}$ 之外，要求支持查询栈中的最大值，应该怎么做呢？  
H: 这不是傻逼题嘛！李煜东的蓝书上面有这道题啊！就是多维护一个值 $mn_i$, 表示栈中第 $1$ 个元素到第 $i$ 个元素的最大值就可以啦！每次 $\operatorname{push}$, $\operatorname{pop}$, $\operatorname{query}$ 的时候能用 $\Theta(1)$ 维护 $mn_i$。  
B: 那如果放到 $\operatorname{queue}$ 上呢？ $\operatorname{queue}$ 是 $\texttt{FIFO}$ 的，不能简单套用这个方法。  
H: 那就把这个问题留给强大的读者吧！

### 题目描述

您需要写一种数据结构（$\operatorname{queue}$），要求能支持 $\operatorname{push}$, $\operatorname{pop}$ 和询问队列中的最大元素。

### 输入格式

第一行一个整数 $q$，表示操作个数。  

第二到第 $q+1$ 行每行的格式是下列格式之一：
- ``1 x`` 表示往 $\operatorname{queue}$ 中 $\operatorname{push}$ 一个数 $x$；  
- ``2`` 表示 $\operatorname{pop}$ 掉 $\operatorname{queue}$ 中队首的元素，保证队列非空；  
- ``3`` 表示询问当前 $\operatorname{queue}$ 中最大的元素，保证队列非空。  

### 输出格式

对于每个询问操作（即操作 $3$），输出一个数，表示当前 $\operatorname{queue}$ 中最大的元素。

### 样例

#### 输入样例 #1
```plain
13
1 5
1 3
3
2
1 6
1 4
1 2
3
2
2
3
1 7
3
```
#### 输出样例 #1
```plain
5
6
4
7
```

### 数据范围
对于 $50\%$ 的数据，$n \le 5 \times 10^3$  
对于 $80\%$ 的数据，$n \le 2 \times 10^5$  
对于 $100\%$ 的数据，$n \le 5 \times 10^6, 1 \le x \le 10^9$