# CSP-S, NOIp 模拟赛 (KT Round #2)

```cpp
题目不保证按难度排序！

青蛙跳跳[jfrog]
巡游[btour]
二维表翻转[bintable]
贪心购物[txshop]
```

统一时空限制：``1s, 512MiB``

## 青蛙跳跳 (jfrog)

### 题目描述

在一条长长的数轴上，住着一只爱跳的小青蛙。

小青蛙喜欢跳来跳去。如果小青蛙在位置 $i$，那么 $1$ 步之后，小青蛙会等概率地跳到 $i-1$ 处或 $i+1$ 处。特别的，如果小青蛙在位置 $0$，那么 $1$ 步后一定会跳到位置 $1$。

小青蛙开始时在位置 $x$，它想知道期望多少步后，它会跳到位置 $y$。保证 $x \le y$。

### 输入格式
一行，用空格隔开的两个整数，$x,y$。  

### 输出格式
第一行一个整数，表示期望步数，向下取整。  

### 样例数据
#### 样例输入 1
```plain
0 1
```
#### 样例输出 1
```plain
1
```

#### 样例输入 2
```plain
3 3
```
#### 样例输出 2
```plain
0
```

### 说明
对于 $30\%$ 的数据，$0\le x\le y\le 10^3$  
对于 $60\%$ 的数据，$0\le x\le y\le 10^6,|x-y|\le10^3$  
对于 $100\%$ 的数据，$0\le x\le y\le 10^9$  


## 巡游 (btour)

### 题目描述

在一个平面上，有 $n$ 个邮筒，分别位于 $(x_1,y_1),(x_2,y_2),\dots,(x_n,y_n)$。此外，还有一个出发站 $(x_s,y_s)$。

作为一名邮递员，你可以从出发站 $(x_s,y_s)$ 开始，**严格向右行进**，直至最右端的邮筒。之后，你必须**严格向左行进**，直至出发站 $(x_s,y_s)$，这样你就完成了一天的任务。

当然，你需要经过**所有**的邮筒以收集信。你当然可以经过一个邮筒多次，不过这显然没有必要。

这里**严格向右行进**的定义是：在行进过程中，$y_p$ 单调不减，而 $x_p$ 可以随意变化。这里 $(x_p,y_p)$ 表示邮递员的坐标。

相信你已经明白了需要做什么：你需要求出最短距离与相对应的路径。

保证 $y_1,y_2,\dots,y_n$ 不重复，保证 $\forall i=1,2,\dots,n,y_s<y_i$

### 输入格式
第一行用空格隔开的三个整数，$n,x_s,y_s$。  
第二行至第 $n+1$ 行，每行用空格隔开的两个整数。第 $i+1$ 行的两个整数为 $x_i,y_i$

### 输出格式
第一行一个实数，表示最短距离。  
第二行 $n$ 个整数，表示经过的所有邮筒。

注：如果在你的方案中，一个邮筒被经过了多次，那么只需输出一次。

### 样例数据
#### 样例输入 1
```plain
4 0 0
1 5
3 4
5 3
6 8
```
#### 样例输出 1
```plain
23.996991281
2 1 4 3
```
#### 样例解释 1
![btour1.png](https://i.loli.net/2021/07/21/HzTaNtiE3OkxpmR.png)  

答案的准确值是 $5+\sqrt{5}+2\sqrt{34}+\sqrt{26}$
#### 样例输入 2
```plain
6 1 1
2 8
3 6
4 3
5 9
6 7
7 2
```
#### 样例输出 2
```plain
25.584024595
3 2 1 4 5 6
```

### 说明
对于所有数据，$|x_s|,|y_s|,|x_i|,|y_i|\le10^6(i=1,2,\dots,n)$  

对于 $40\%$ 的数据，$n\le20$  
对于 $70\%$ 的数据，$n\le300$  
对于 $100\%$ 的数据，$n\le2000$

得分：第一行与标准答案误差不超过 ${10}^{-6}$，得测试点 $60\%$ 的分数；  
在此前提下，第二行合法且计算出的答案确为第一行输出的答案（误差不超过 ${10}^{-6}$），得测试点剩下 $40\%$ 的分数；  
**就算选手无法算出第 $2$ 行，也需要输出 $n$ 个 $0$，否则即使第一行正确也无法拿到 $60\%$ 的分**

误差定义为：相对误差与绝对误差的较小值

## 二维表翻转 (bintable)

### 题目描述
给定一个 $n\times m$ 的 $01$ 矩阵，每次操作可以将某个 $2\times2$ 的矩阵内的 $3$ 个数取反，请在 $n\times m$ 步内将矩阵变为全 $0$。

你不需要最小化操作数。

可以证明，符合条件的操作序列一定存在。

### 输入格式

由于可能的情况实在太多，我们迫不得已采用了多组数据输入。**每一个测试点的输入包括 $5$ 组数据**

对于每组数据：  
第一行两个空格隔开的整数 $n,m$，表示二维表的高与宽。  
第二至 $n+1$ 行，每行 $m$ 个紧挨着的字符（为 $0/1$）  

### 输出格式
对于每组数据：  
第一行一个整数 $k$ 表示操作总数。  
第二至 $k+1$ 行每行 $6$ 个整数 $x_1,y_1,x_2,y_2,x_3,y_3$，描述了三个翻转操作的位置 $(x_1,y_1),(x_2,y_2),(x_3,y_3)$。

### 样例
#### 输入样例 #1
```plain
2 2
10
11
3 3
011
101
110
4 4
1111
0110
0110
1111
5 5
01011
11001
00010
11011
10000
2 3
011
101
```

#### 输出样例 #1
```plain
1
1 1 2 1 2 2
2 
2 1 3 1 3 2
1 2 1 3 2 3
4
1 1 1 2 2 2 
1 3 1 4 2 3
3 2 4 1 4 2
3 3 4 3 4 4
4
1 2 2 1 2 2 
1 4 1 5 2 5 
4 1 4 2 5 1
4 4 4 5 3 4
2
1 3 2 2 2 3
1 2 2 1 2 2
```

### 说明
对于 $32\%$ 的数据，$2\le n,m\le10$  
对于 $64\%$ 的数据，$2\le n,m\le100$  
对于 $100\%$ 的数据，$2\le n,m\le500$  


如果五组数据中有任意一组输出不合法，或者无法完成要求，则不得分。

如果五组数据的输出都有 $0\le k\le nm$，得该测试点 $100\%$ 的分数。  
否则，如果五组数据的输出都有 $nm< k\le 3nm$，得该测试点 $50\%$ 的分数。

换而言之，一个测试点内表现最差的那个数据决定了你的真实得分。

---

spj 对每组数据都会有一个字母评级：
- `A`: 完美输出，$0\le k\le nm$
- `P`: 不完美输出，$nm< k\le 3nm$
- `E`: $k$ 大于 $3nm$
- `F`: 操作结束后，矩阵不全为 $0$
- `I`: 输出不合法

如果你的输出同时满足多个上面的条件（比如不合法，同时 $k>3nm$），那么评级输出的优先级为 `I > F > E > P > A`

## 贪心购物 (txshop)

### 题目描述

一条街上有 $n$ 个快餐店，第 $i$ 个快餐店提供价格为 $a_i$ 的快餐。

由于一些奇怪的原因，$a_i$ 单调不增。

有 $q$ 个询问，每个有两种可能的操作：
1. 快餐店又双叒叕涨价了！对于所有 $1\le i\le x$，$a_i$ 变为 $\max(a_i,y)$
2. 现在，你带着 $y$ 钱来到了街头 ($i=1$)。你决定从 $i=x$ 到 $i=n$ 访问这条街，并在所有能买得起快餐的店吃一顿。显然在店 $i$ 吃完后（当前钱 $\ge a_i$），你的钱会减少 $a_i$。你需要输出你能吃几顿。

### 输入格式
第一行，用空格隔开的两个整数，$n,q$。  
第二行 $n$ 个用空格隔开的整数表示 $a_i(1\le a_i\le 10^9)$  
接下来 $q$ 行，每行有三个整数 $t,x,y$，意义如题目所描述。其中 $t$ 是操作种类。（$1\le t\le 2,1\le x\le n,1\le y\le 10^9$）


### 输出格式
对于每一个 $t=2$ 的操作，输出一行，为吃的顿数。

### 样例数据
#### 样例输入 1
```plain
10 6
10 10 10 6 6 5 5 5 3 1
2 3 50
2 4 10
1 3 10
2 2 36
1 4 7
2 2 17
```
#### 样例输出 1
```plain
8
3
6
2
```

### 说明

对于 $40\%$ 的数据，$1 \leq n, q \leq 3 \cdot 10^3$  
对于另外 $20\%$ 的数据，$t=2$  
对于 $70\%$ 的数据，$1 \leq n, q \leq 5 \cdot 10^4$  
对于 $100\%$ 的数据，$1 \leq n, q \leq 2 \cdot 10^5$  