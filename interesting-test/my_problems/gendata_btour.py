import random, genlib

def genIn(inf: str, n: int, lim: int):
    with open(inf, 'w') as f:
        xs = random.randint(-lim, lim)
        ys = random.randint(-lim, lim // 100)
        print(n, xs, ys, file = f)
        occured_y = { ys }
        for i in range(n):
            x = random.randint(-lim, lim)
            y = random.randint(ys + 1, lim)
            while y in occured_y:
                y = random.randint(ys + 1, lim)
            occured_y.add(y)
            print(x, y, file = f)


genlib.start('btour')
for i in range(1, 9):
    genlib.gen_data(genIn, i, 20, 20)
for i in range(9, 15):
    genlib.gen_data(genIn, i, 300, 2000)
for i in range(15, 21):
    genlib.gen_data(genIn, i, 2000, 1000000)
genlib.end()