import random, genlib

def genIn(inf: str, id: int, nm_lim: int, x_lim: int, disabled_operation: list, nm_deline_range = (0.9, 1)):
    enabled_operation = []
    for i in range(1, 5):
        if i not in disabled_operation:
            enabled_operation.append(i)
    with open(inf, 'w') as f:
        n = int(nm_lim * random.uniform(*nm_deline_range))
        m = int(nm_lim * random.uniform(*nm_deline_range))
        print(n, m, id, file = f)
        for i in range(n):
            print(random.randint(-x_lim, x_lim), end = ' ', file = f)
        print(file = f)
        for i in range(m):
            op = random.choice(enabled_operation)
            l, r = random.randint(1, n), random.randint(1, n)
            if l > r:
                l, r = r, l
            if op == 1:
                print(op, l, r, random.randint(-x_lim, x_lim), file = f)
            else:
                print(op, l, r, file = f)

genlib.start("dsdie", "ex_dsdie")
genlib.gen_data(genIn, 1, 1, 10, 30, [1], (1, 1))
genlib.gen_data(genIn, 2, 2, 10, 30, [4], (1, 1))
genlib.gen_data(genIn, 3, 3, 10, 30, [2, 3], (1, 1))
genlib.gen_data(genIn, 4, 4, 1000, 3000, [], (1, 1))
genlib.end()
genlib.start("dsdie")
genlib.gen_data(genIn, 1, 1, 4000, 100, [1])
genlib.gen_data(genIn, 2, 2, 4000, 100, [4])
genlib.gen_data(genIn, 3, 3, 4000, 10000, [2, 3])
genlib.gen_data(genIn, 4, 4, 4000, 1000000, [])
genlib.gen_data(genIn, 5, 5, 4000, 1000000, [], (1, 1))
genlib.gen_data(genIn, 6, 6, 4000, 1000000, [], (1, 1))
genlib.gen_data(genIn, 7, 7, 50000, 10000, [1])
genlib.gen_data(genIn, 8, 8, 50000, 10000, [4])
genlib.gen_data(genIn, 9, 9, 50000, 10000, [2, 3])
genlib.gen_data(genIn, 10, 10, 50000, 10000, [3])
genlib.gen_data(genIn, 11, 11, 50000, 1000000, [])
genlib.gen_data(genIn, 12, 12, 50000, 1000000, [], (1, 1))
genlib.gen_data(genIn, 13, 13, 300000, 10000, [1])
genlib.gen_data(genIn, 14, 14, 300000, 10000, [4])
genlib.gen_data(genIn, 15, 15, 300000, 10000, [2, 3])
genlib.gen_data(genIn, 16, 16, 300000, 10000, [3])
genlib.gen_data(genIn, 17, 17, 300000, 10000, [3], (1, 1))
genlib.gen_data(genIn, 18, 18, 300000, 1000000, [])
genlib.gen_data(genIn, 19, 19, 300000, 1000000, [], (1, 1))
genlib.gen_data(genIn, 20, 20, 300000, 1000000, [], (1, 1))
genlib.end()