import random
import os

problemName = "dsa"

def gen_input(file_name: str, q: int):
    file = open(file_name, "w")
    file.write("%d\n"%q)
    top = 0
    seq = random.choice([
        [1, 1, 1, 1, 1, 1, 1, 3, 3, 3],
        [1, 1, 1, 1, 1, 1, 2, 3, 3, 3],
        [1, 1, 1, 1, 1, 2, 2, 3, 3, 3],
        [1, 1, 1, 1, 2, 2, 2, 3, 3, 3],
        [1, 1, 1, 2, 2, 2, 2, 3, 3, 3],
        [1, 1, 2, 2, 2, 2, 2, 3, 3, 3],
        [1, 2, 2, 2, 2, 2, 2, 3, 3, 3],
    ])
    for i in range(1, q + 1):
        if top == 0:
            typ = 1
        else:
            typ = random.choice(seq)
        if typ == 1:
            file.write("1 %d\n"%random.randint(1, 1000000000))
            top += 1
        else:
            file.write("%d\n"%typ)
        if typ == 2:
            top -= 1
    file.close()

def random_str(len: int, seq: str="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz") -> str:
    name = str()
    for i in range(len):
        name += random.choice(seq)
    return name

def compile(prog: str) -> str:
    exe_name = random_str(8) + ".exe"
    os.system("g++ " + prog + " -o " + exe_name)
    return exe_name

def run(prog: str, inf: str, ouf: str):
    os.system("copy " + problemName + ".in " + inf)
    os.system(prog)
    os.system("copy " + ouf + " " + problemName + ".out")

def gen_data(data_id: str, std: str, q: int):
    print("generating data No.%d"%data_id)
    inf_name = "data\\" + problemName + "\\" + problemName + str(data_id) + ".in" 
    ouf_name = "data\\" + problemName + "\\" + problemName + str(data_id) + ".out" 
    gen_input(inf_name, q)
    run(std, inf_name, ouf_name)

std = compile("std\\" + problemName + ".cpp")
gen_data(1, std, 5000)
gen_data(2, std, 5000)
gen_data(3, std, 5000)
gen_data(4, std, 5000)
gen_data(5, std, 5000)
gen_data(6, std, 5000)
gen_data(7, std, 5000)
gen_data(8, std, 5000)
gen_data(9, std, 5000)
gen_data(10, std, 5000)
gen_data(11, std, 200000)
gen_data(12, std, 200000)
gen_data(13, std, 200000)
gen_data(14, std, 200000)
gen_data(15, std, 200000)
gen_data(16, std, 200000)
gen_data(17, std, 5000000)
gen_data(18, std, 5000000)
gen_data(19, std, 5000000)
gen_data(20, std, 5000000)
os.system("rm " + problemName + ".in " + problemName + ".out")
os.system("rm " + std)