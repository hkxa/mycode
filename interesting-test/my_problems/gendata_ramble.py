import random
import os

problemName = "ramble"

# p 的概率返回 True, (1 - p) 的概率返回 False
def test(p: float):
    return random.random() <= p

def gen_input(file_name: str, n: int, m: int, is_full: bool):
    TIMES = 1.618 + 2 * random.random()
    with open(file_name, "w") as f:
        print(n, m, file = f, flush = True)
        for i in range(n - 1, -1, -1):
            for j in range(m):
                no_block = (abs(j * (n - 1) - i * (m - 1)) - 1) / ((n - 1) * (m - 1))
                if is_full or test(1 - max(no_block, 0) ** TIMES):
                    f.write('+')
                else:
                    f.write('.')
            f.write('\n')

def random_str(len: int, seq: str="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz") -> str:
    name = str()
    for i in range(len):
        name += random.choice(seq)
    return name

def compile(prog: str) -> str:
    exe_name = random_str(8) + ".exe"
    os.system("g++ " + prog + " -o " + exe_name)
    return exe_name

def run(prog: str, inf: str, ouf: str):
    os.system("copy " + problemName + ".in " + inf)
    os.system(prog)
    os.system("copy " + ouf + " " + problemName + ".out")

def gen_data(data_id: str, std: str, n: int, m: int, is_full: bool = False):
    print("generating data No.%d"%data_id)
    inf_name = "data\\" + problemName + "\\" + problemName + str(data_id) + ".in" 
    ouf_name = "data\\" + problemName + "\\" + problemName + str(data_id) + ".out" 
    gen_input(inf_name, n, m, is_full)
    run(std, inf_name, ouf_name)

def remove_files(std: str):
    os.system("rm " + problemName + ".in " + problemName + ".out")
    os.system("rm " + std)

std = compile("std\\" + problemName + ".cpp")
gen_data(1, std, 2, 2)
gen_data(2, std, 2, 3)
gen_data(3, std, 3, 3)
gen_data(4, std, 3, 2)
gen_data(5, std, 3, 3)
gen_data(6, std, 2, 3)
gen_data(7, std, 10, 20)
gen_data(8, std, 20, 7)
gen_data(9, std, 20, 20)
gen_data(10, std, 19, 20)
gen_data(11, std, 2800, 1278, True)
gen_data(12, std, 3900, 299, True)
gen_data(13, std, 4900, 3900, True)
gen_data(14, std, 2839, 4900, True)
gen_data(15, std, 2400, 5000, True)
gen_data(16, std, 5000, 3699, True)
gen_data(17, std, 5000, 800)
gen_data(18, std, 1899, 5000)
gen_data(19, std, 5000, 5000)
gen_data(20, std, 5000, 4999)
remove_files(std)