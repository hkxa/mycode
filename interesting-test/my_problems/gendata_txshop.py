import random, genlib

def genIn_random(fname: str, t_range: list, nq: int, uniformRange: tuple):
    with open(fname, 'w') as f:
        n = int(random.uniform(*uniformRange) * nq)
        q = int(random.uniform(*uniformRange) * nq)
        print(n, q, file = f)
        a = []
        for i in range(n):
            a.append(random.randint(1, 1000000000))
        a = sorted(a, reverse = True)
        for i in range(n):
            print(a[i], end = ' ', file = f)
        print(file = f)
        for i in range(q):
            t = random.choice(t_range)
            x = random.randint(1, n)
            y = random.randint(1, 1000000000)
            print(t, x, y, file = f)

genlib.start('txshop')
genlib.gen_data(genIn_random, 1, [1, 2], 3000, (0.5, 1))
genlib.gen_data(genIn_random, 2, [1, 2], 3000, (0.7, 1))
genlib.gen_data(genIn_random, 3, [1, 2], 3000, (0.9, 1))
genlib.gen_data(genIn_random, 4, [1, 2], 3000, (1, 1))
genlib.gen_data(genIn_random, 5, [2], 50000, (0.65, 1))
genlib.gen_data(genIn_random, 6, [2], 50000, (1, 1))
genlib.gen_data(genIn_random, 7, [1, 2], 50000, (1, 1))
genlib.gen_data(genIn_random, 8, [1, 2], 200000, (0.7, 1))
genlib.gen_data(genIn_random, 9, [1, 2], 200000, (0.9, 1))
genlib.gen_data(genIn_random, 10, [1, 2], 200000, (1, 1))
genlib.end()