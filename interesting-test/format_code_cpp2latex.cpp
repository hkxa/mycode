/*
int: #569CD6
if: #C586C0
"str": #CE9178
std:: : #4EC9B0
12345: #B5CEA8
func(): #DCDCAA
ERROR: #F44747
变量名: #9CDCFE
*/

/*
$\texttt{{\color{#C586C0}\#include}{\color{#CE9178}{ <bits/stdc++.h>}}}$  
$\texttt{{\color{#C586C0}using }{\color{#569CD6}namespace }{\color{#4EC9B0}std};}$  
$\texttt{}$  
$\texttt{{\color{#569CD6}int} {\color{#DCDCAA}main}() \{}$  
$\texttt{\verb|    |{\color{#569CD6}int} x, y;}$  
$\texttt{\verb|    |cin >> x >> y;}$  
$\texttt{\verb|    |cout << x + y << endl;}$  
$\texttt{\verb|    |{\color{#C586C0}return }{\color{#B5CEA8}0};}$  
$\texttt{\}}$  
*/