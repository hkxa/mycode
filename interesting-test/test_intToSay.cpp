#include <bits/stdc++.h>
using namespace std;

string ans[10007];

void prepare() {
    ans[0] = "[0]";
    ans[1] = "[1]";
    ans[2] = "[2]";
    ans[3] = "[3]";
    ans[4] = "[4]";
    ans[5] = "[5]";
    ans[6] = "[6]";
    ans[7] = "[7]";
    ans[8] = "[8]";
    ans[9] = "[9]";
    ans[10] = "[Shi]";
    for (int i = 11; i <= 19; i++)
        ans[i] = ans[10] + ans[i - 10];
    for (int i = 20; i <= 99; i++)
        if (i % 10 == 0) ans[i] = ans[i / 10] + ans[10];
        else ans[i] = ans[i / 10] + ans[10] + ans[i % 10];
    for (int i = 100; i <= 999; i++)
        if (i % 100 == 0) ans[i] = ans[i / 100] + "[Bai]";
        else if ((i / 10) % 10 == 0) ans[i] = ans[i / 100] + "[Bai][0]" + ans[i % 10];
        else ans[i] = ans[i / 100] + "[Bai]" + ans[i % 100];
    for (int i = 1000; i <= 9999; i++)
        if (i % 1000 == 0) ans[i] = ans[i / 1000] + "[Qian]";
        else if ((i / 100) % 10 == 0) ans[i] = ans[i / 1000] + "[Qian][0]" + ans[i % 100];
        else ans[i] = ans[i / 1000] + "[Qian]" + ans[i % 1000];
}

string solve(long long number) {
    if (number >= 100000000) {
        if ((number / 10000000) % 10 == 0) return ans[number / 100000000] + "[Yi][0]" + solve(number % 100000000);
        else return ans[number / 100000000] + "[Yi]" + solve(number % 100000000);
    } else if (number >= 10000) {
        if ((number / 1000) % 10 == 0) return ans[number / 10000] + "[Wan][0]" + ans[number % 10000];
        else return ans[number / 10000] + "[Wan]" + ans[number % 10000];
    } else return ans[number];
}

int main() {
    prepare();
    long long n;
    scanf("%lld", &n);
    cout << solve(n) << endl;
    return 0;
}