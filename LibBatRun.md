### Test One Problem
**One Line**
```bat
set prob=ProblemName& cd %prob%& echo Problem: %prob% & run %prob% -f -q -m & type %prob%.out & cd ..
```
**Bat File**
```bat
echo off
cls

set prob=ProblemName

echo =-=-=-=-=-=-=-=-=-=-=-=-= Problem: %prob%
cd %prob%
g++ %prob%.cpp -o %prob%.exe -std=c++11 -Wall -Wextra -Wpointer-arith -Wcast-qual -Wl,--stack=536870912
monitor %prob%.exe 50 1048576
echo [stdout (file)]
type %prob%.out
del %prob%.exe
cd ..
echo on
```

### Test All
```bat
echo off
cls

set ProblemA=1111
set ProblemB=2222
set ProblemC=3333
set ProblemD=4444

echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= Test All File
if %ProblemA% LEQ 1111 ( echo T1. [not found] ) else ( echo T1. %ProblemA% )
if %ProblemB% LEQ 2222 ( echo T2. [not found] ) else ( echo T2. %ProblemB% )
if %ProblemC% LEQ 3333 ( echo T3. [not found] ) else ( echo T3. %ProblemC% )
if %ProblemD% LEQ 4444 ( echo T4. [not found] ) else ( echo T4. %ProblemD% )


if %ProblemA% LEQ 1111 ( echo =-=-=-=-=-=-=-=-=-=-=-=-= T1. [not found] ) else (
    echo =-=-=-=-=-=-=-=-=-=-=-=-= T1. %ProblemA%
    cd %ProblemA%
    g++ %ProblemA%.cpp -o %ProblemA%.exe -std=c++11 -Wall -Wextra -Wpointer-arith -Wcast-qual -Wl,--stack=536870912
    monitor %ProblemA%.exe 50 1048576
    echo [Output]
    type %ProblemA%.out
    del %ProblemA%.exe
    cd ..
)

if %ProblemB% LEQ 2222 ( echo =-=-=-=-=-=-=-=-=-=-=-=-= T2. [not found] ) else (
    echo =-=-=-=-=-=-=-=-=-=-=-=-= T2. %ProblemB%
    cd %ProblemB%
    g++ %ProblemB%.cpp -o %ProblemB%.exe -std=c++11 -Wall -Wextra -Wpointer-arith -Wcast-qual -Wl,--stack=536870912
    monitor %ProblemB%.exe 50 1048576
    echo [Output]
    type %ProblemB%.out
    del %ProblemB%.exe
    cd ..
)

if %ProblemC% LEQ 3333 ( echo =-=-=-=-=-=-=-=-=-=-=-=-= T3. [not found] ) else (
    echo =-=-=-=-=-=-=-=-=-=-=-=-= T3. %ProblemC%
    cd %ProblemC%
    g++ %ProblemC%.cpp -o %ProblemC%.exe -std=c++11 -Wall -Wextra -Wpointer-arith -Wcast-qual -Wl,--stack=536870912
    monitor %ProblemC%.exe 50 1048576
    echo [Output]
    type %ProblemC%.out
    del %ProblemC%.exe
    cd ..
)

if %ProblemD% LEQ 4444 ( echo =-=-=-=-=-=-=-=-=-=-=-=-= T4. [not found] ) else (
    echo =-=-=-=-=-=-=-=-=-=-=-=-= T4. %ProblemD%
    cd %ProblemD%
    g++ %ProblemD%.cpp -o %ProblemD%.exe -std=c++11 -Wall -Wextra -Wpointer-arith -Wcast-qual -Wl,--stack=536870912
    monitor %ProblemD%.exe 50 1048576
    echo [Output]
    type %ProblemD%.out
    del %ProblemD%.exe
    cd ..
)

pause
exit 0
```