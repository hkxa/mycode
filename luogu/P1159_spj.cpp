#include "testlib.h"
using namespace std;

int n;
map<string, int> rank;
string s, diff;

int main(int argc, char* argv[]) 
{
    registerTestlibCmd(argc, argv);
    n = inf.readInt();
    inf.readEoln();
    for (int i = 1; i <= n; i++) {
        s = ouf.readString();
        rank[s] = i;
    }
    for (int i = 1; i <= n; i++) {
        s = inf.readString();
        diff = inf.readString();
        // printf("%s %s\n", s.c_str(), diff.c_str());
        switch (diff[0]) {
            case 'U' :
                // UP
                if (i >= rank[s]) quitf(_wa, "The answer is wrong: song %s's now rank is %d (UP), your original rank is %d.", s.c_str(), i, rank[s]);
                break;
            case 'D' :
                // DOWN
                if (i <= rank[s]) quitf(_wa, "The answer is wrong: song %s's now rank is %d (DOWN), your original rank is %d.", s.c_str(), i, rank[s]);
                break;
            case 'S' :
                // SAME
                if (i != rank[s]) quitf(_wa, "The answer is wrong: song %s's now rank is %d (SAME), your original rank is %d.", s.c_str(), i, rank[s]);
                break;
        }
    }
    quitf(_ok, "The answer is acceptable.");
    return 0;

}