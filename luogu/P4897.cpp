/*************************************
 * @problem:      【模板】最小割树（Gomory-Hu Tree）.
 * @author:       brealid.
 * @time:         2021-03-08.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

// 你不应当使用命名空间内【前面有下划线的任何变量, 函数】
namespace GeomoryHu_Tree {
    const int __Net_Node = 500, __Net_Edge = 1500;
    typedef int __value_type;
    const __value_type __inf = (__value_type)0x3f3f3f3f3f3f3f3f; // 当 __value_type 为 int 的时候，自动类型强转为 0x3f3f3f3f
    struct __edge {
        int to, nxt_edge;
        __value_type flow;
    } __e[__Net_Edge * 2 + 5];
    int __depth[__Net_Node + 5], __head[__Net_Node + 5], __cur[__Net_Node + 5], __ecnt = 1;
    int __node_total, __st, __ed;
    // 清零，此函数适用于多组数据
    void __clear() {
        memset(__head, 0, sizeof(int) * (__node_total + 1));
        __ecnt = 1;
        __st = __ed = 0;
    }
    // 添加边（无向边）
    inline void __add_edge(const int &from, const int &to, const __value_type &flow = (__value_type)1) {
        // Add "positive going __edge"
        __e[++__ecnt].to = to;
        __e[__ecnt].flow = flow;
        __e[__ecnt].nxt_edge = __head[from];
        __head[from] = __ecnt;
        // Add "reversed going __edge"
        __e[++__ecnt].to = from;
        __e[__ecnt].flow = flow;
        __e[__ecnt].nxt_edge = __head[to];
        __head[to] = __ecnt;
    }
    // Dinic 算法 bfs 函数
    inline bool __dinic_bfs() {
        memset(__depth, 0x3f, sizeof(int) * (__node_total + 1));
        memcpy(__cur, __head, sizeof(int) * (__node_total + 1));
        std::queue<int> q;
        q.push(__st);
        __depth[__st] = 0;
        while (!q.empty()) {
            int u = q.front(); q.pop();
            for (int i = __head[u]; i; i = __e[i].nxt_edge)
                if (__depth[__e[i].to] > __depth[u] + 1 && __e[i].flow) {
                    __depth[__e[i].to] = __depth[u] + 1;
                    if (__e[i].to == __ed) return __ed; // 后续 bfs 到的节点, __depth 一定大于 __ed, 没有丝毫用处
                    q.push(__e[i].to);
                }
        }
        return false;
    }
    // 将所有流还原(即: 还原残量网络至原图)
    inline void __restore_flow() {
        for (int i = 2; i <= __ecnt; i += 2)
            __e[i].flow = __e[i | 1].flow = (__e[i].flow + __e[i | 1].flow) >> 1;
    }
    // Dinic 算法 dfs 函数
    __value_type __dinic_dfs(int u, __value_type now) {
        if (u == __ed) return now;
        __value_type max_flow = 0, nRet;
        for (int &i = __cur[u]; i && now; i = __e[i].nxt_edge)
            if (__depth[__e[i].to] == __depth[u] + 1 && (nRet = __dinic_dfs(__e[i].to, std::min(now, __e[i].flow)))) {
                now -= nRet;
                max_flow += nRet;
                __e[i].flow -= nRet;
                __e[i ^ 1].flow += nRet;
            }
        return max_flow;
    }
    // Dinic 算法总工作函数，需要提供节点数（偏大影响时间），起始点（默认 1），结束点（默认 node_count）
    __value_type __dinic_work(int node_count, int start_node = 1, int finish_node = -1) {
        __node_total = node_count;
        __st = start_node;
        __ed = ~finish_node ? finish_node : node_count;
        __value_type max_flow = 0;
        while (__dinic_bfs())
            max_flow += __dinic_dfs(__st, __inf);
        return max_flow;
    }
    int __n, __a[__Net_Node + 5], __t[__Net_Node + 5];
    void (*__user_add_edge)(int, int, __value_type);
    void __work_base(int l, int r) {
        if (l >= r) return;
        __restore_flow();
        __user_add_edge(__a[l], __a[l + 1], __dinic_work(__n, __a[l], __a[l + 1]));
        int p1 = l - 1, p2 = r + 1;
        for (int i = l; i <= r; ++i)
            if (__depth[__a[i]] != 0x3f3f3f3f) __t[++p1] = __a[i];
            else __t[--p2] = __a[i];
        memcpy(__a + l, __t + l, (r - l + 1) * sizeof(int));
        __work_base(l, p1);
        __work_base(p2, r);
    }
    // 清零（多组数据时候使用）
    void clear() {
        __clear();
    }
    // 加入一条无向边
    void add_edge(int u, int v, __value_type w) {
        __add_edge(u, v, w);
    }
    // 工作函数（n: 节点个数; function_AddEdge: 加边函数）
    void work(int n, void (*function_AddEdge)(int, int, __value_type)) {
        __n = n;
        __user_add_edge = function_AddEdge;
        for (int i = 1; i <= n; ++i) __a[i] = i;
        __work_base(1, n);
    }
}

const int N = 500 + 7;

int n, m, q;
vector<pair<int, int> > G[N];
int fa[N][10], minist[N][10], dep[N];

void dfs(int u, int nFa) {
    fa[u][0] = nFa;
    dep[u] = dep[nFa] + 1;
    for (int i = 0; fa[u][i]; ++i) {
        fa[u][i + 1] = fa[fa[u][i]][i];
        minist[u][i + 1] = min(minist[u][i], minist[fa[u][i]][i]);
    }
    for (uint32 i = 0; i < G[u].size(); ++i) {
        int v = G[u][i].first;
        if (v == nFa) continue;
        minist[v][0] = G[u][i].second;
        dfs(v, u);
    }
}

inline int getMinist(int u, int v) {
    int nRet = 0x3f3f3f3f;
    if (dep[u] < dep[v]) swap(u, v);
    for (int k = 8; dep[u] > dep[v]; --k) {
        if (dep[u] - dep[v] < (1 << k)) continue;
        nRet = min(nRet, minist[u][k]);
        u = fa[u][k];
    }
    if (u == v) return nRet;
    for (int k = 8; fa[u][0] != fa[v][0]; --k) {
        if (fa[u][k] == fa[v][k]) continue;
        nRet = min(nRet, min(minist[u][k], minist[v][k]));
        u = fa[u][k], v = fa[v][k];
    }
    return min(nRet, min(minist[u][0], minist[v][0]));
}

void add_edge(int u, int v, int w) {
    // printf("E(%d, %d, %d)\n", u, v, w);
    G[u].push_back(make_pair(v, w));
    G[v].push_back(make_pair(u, w));
}

signed main() {
    kin >> n >> m;
    for (int i = 1, u, v, w; i <= m; ++i) {
        kin >> u >> v >> w;
        GeomoryHu_Tree::add_edge(u + 1, v + 1, w);
    }
    GeomoryHu_Tree::work(n + 1, add_edge);
    dfs(1, 0);
    kin >> q;
    for (int i = 1, u, v; i <= q; ++i) {
        kin >> u >> v;
        kout << getMinist(u + 1, v + 1) << '\n';
    }
    return 0;
}