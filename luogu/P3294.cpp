//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      P3294 [SCOI2016]背单词.
 * @user_name:    brealid.
 * @time:         2020-06-04.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 988244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

int n;
char s[510000 + 7]; int len;
int ch[510000 + 7][26], cnt = 1;
int tag[510000 + 7], tot[510000 + 7];
// int branch[510000 + 7];
// int64 extra[510000 + 7], ans[510000 + 7];
vector<int> G[510000 + 7]; 
int fa[510000 + 7];

int find(int u) { return fa[u] == u ? u : fa[u] = find(fa[u]); }

void insert() {
    // printf("insert(%s)\n", s);
    len = strlen(s);
    int u = 1;
    for (int i = len - 1; i >= 0; i--) {
        if (!ch[u][s[i] - 'a']) ch[u][s[i] - 'a'] = ++cnt;
        // printf("inserting char %c in node %d\n", s[i], u);
        u = ch[u][s[i] - 'a'];
        // printf("go branch %d\n", u);
    }
    tag[u]++;
}

// bool cmp1(int i, int j) { 
//     if (!i) return 0;
//     if (!j) return 1;
//     return (int64)tot[i] * branch[j] < (int64)tot[j] * branch[i]; 
// }
// bool cmp2(int i, int j) { return tot[i] ^ tot[j] ? tot[i] < tot[j] : branch[i] < branch[j]; }
bool cmp3(int i, int j) { 
    // if (!i) return 0;
    // if (!j) return 1;
    return tot[i] < tot[j]; 
}
// bool cmp4(int i, int j) { 
//     if (!i) return 0;
//     if (!j) return 1;
//     return (int64)tot[i] * extra[j] < (int64)tot[j] * extra[i]; 
// }

void solve(int u) {
    // if (tag[u]) {
    //     // ans[u] = 1;
    //     tot[u] = 1;
    //     // branch[u] = 1;
    // } 
    // tot[u] = 1;
    // printf("son[%d] = { ", u);
    // for (int i = 0; i < 26; i++) {
    //     if (!ch[u][i]) continue;
    //     printf("%d ", ch[u][i]);
    // }
    // printf("}\n");
    for (int i = 0; i < 26; i++) {
        if (!ch[u][i]) continue;
        // printf("node %d, found child %d (%c)\n", u, ch[u][i], i | 'a');
        // if (!tag[u]) branch[u] += branch[ch[u][i]];
        if (!tag[ch[u][i]]) fa[ch[u][i]] = find(u);
        else G[find(u)].push_back(ch[u][i]);
        solve(ch[u][i]);
        // tot[u] += tot[ch[u][i]];
    }
    // if (!tag[u]) {
    //     sort(ch[u], ch[u] + 26, cmp1);
    //     for (int i = 0, t = 0; i < 26; i++) {
    //         if (!ch[u][i]) continue;
    //         extra[u] += extra[ch[u][i]] + (int64)(branch[ch[u][i]] + 2 * t - 1) * branch[ch[u][i]] / 2;
    //         t += tot[ch[u][i]];
    //         // printf("son node %d : ansChild = %lld, ans = %lld, t = %d\n", ch[u][i], ans[ch[u][i]], ans[u], t);
    //     }
    //     // ans[u] += extra[u];
    // }
    // sort(ch[u], ch[u] + 26, cmp3);
    // for (int i = 0, t = 0; i < 26; i++) {
    //     if (!ch[u][i]) continue;
    //     // ans[u] += ans[ch[u][i]] + t * extra[ch[u][i]];
    //     ans[u] += ans[ch[u][i]] + t * branch[ch[u][i]];
    //     // ans[u] += ans[ch[u][i]] + t * branch[ch[u][i]] + extra[ch[u][i]];
    //     t += tot[ch[u][i]];
    //     // printf("son node %d : ansChild = %lld, ans = %lld, t = %d\n", ch[u][i], ans[ch[u][i]], ans[u], t);
    // }
    // // printf("extra[%d] = %lld, ans[%d] = %lld, branch[%d] = %d, tot[%d] = %d\n", u, extra[u], u, ans[u], u, branch[u], u, tot[u]);
}

void preDo(int u) {
    tot[u] = 1;
    for (size_t i = 0; i < G[u].size(); i++) {
        preDo(G[u][i]);
        tot[u] += tot[G[u][i]];
    }
}

int dft = 0; int64 Ans = 0;
void getAns(int u) {
    int dfn = dft++;
    sort(G[u].begin(), G[u].end(), cmp3);
    for (size_t i = 0; i < G[u].size(); i++) {
        // printf("u = %d : son node %d\n", u, G[u][i]);
        Ans += dft - dfn;
        getAns(G[u][i]);
    }
}

signed main() {
    // memset(fa, -1, sizeof(fa));
    n = read<int>();
    for (int i = 1; i <= 510000; i++) fa[i] = i;
    for (int i = 1; i <= n; i++) {
        scanf("%s", s);
        insert();
    }
    solve(1);
    // write(ans[1], 10);
    preDo(1);
    getAns(1);
    write(Ans, 10);
    return 0;
}

// Create File Date : 2020-06-04

/*
6
a
ba
aa
ca
nba
cba
[O] 10
6
a
cba
nba
fga
hga
yhga
[O] 12
5
a
zta
yta
zra
yra
[O] 11
6
a
zta
yta
zra
yra
ra
[O] 10
*/