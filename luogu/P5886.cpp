/*************************************
 * @problem:      P5886 Hello, 2020!.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-01-03.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, m, p;
int cnt[1000007];
vector<int> vec;

int main()
{
    n = read<int>();
    m = read<int>();
    p = read<int>();
    for (int i = 1, k; i <= n; i++) {
        k = read<int>();
        while (k--) cnt[read<int>()]++;
    }
    for (int i = 1; i <= m; i++) {
        if (cnt[i] == p) vec.push_back(i);
    }
    write(vec.size(), 10);
    for (unsigned i = 0; i < vec.size(); i++) {
        write(vec[i], 32);
    }
    return 0;
}