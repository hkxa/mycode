/*************************************
 * @problem:      P3384 【模板】轻重链剖分.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-04-09. (4 days)
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}
#define N 100007
#define UpdSum(a, b) ((a) = ((a) + (b)) % P)

#ifdef DEBUG
# define passing() cerr << "passing line [" << __LINE__ << "]." << endl
# define debug(...) printf(__VA_ARGS__)
# define show(x) cerr << #x << " = " << (x) << endl
#else
# define passing() do if (0) cerr << "passing line [" << __LINE__ << "]." << endl; while(0)
# define debug(...) do if (0) printf(__VA_ARGS__); while(0)
# define show(x) do if (0) cerr << #x << " = " << (x) << endl; while(0)
#endif

int n, m, r, P;
int a[N], data[N];
vector<int> G[N];
int dfn[N], dep[N], siz[N], fa[N], dfn_now = 0;
int wson[N], s[N];
int tag[N * 4], sum[N * 4], range[N * 4];

void pushdown(int u) {
    if (tag[u]) {
        UpdSum(tag[u << 1], tag[u]);
        // debug("UpdSum[%d] with %d.\n", u << 1, tag[u] * range[u << 1]);
        UpdSum(sum[u << 1], tag[u] * range[u << 1] % P);
        UpdSum(tag[u << 1 | 1], tag[u]);
        // debug("UpdSum[%d] with %d.\n", u << 1 | 1, tag[u] * range[u << 1 | 1]);
        UpdSum(sum[u << 1 | 1], tag[u] * range[u << 1 | 1] % P);
        tag[u] = 0;
    }
}

void pushup(int u) {
    sum[u] = sum[u << 1] + sum[u << 1 | 1];
}

void build(int u, int l, int r) {
    if (l > r) return;
    range[u] = r - l + 1;
    if (l == r) {
        sum[u] = data[l];
        return;
    }
    int mid = (l + r) >> 1;
    build(u << 1, l, mid);
    build(u << 1 | 1, mid + 1, r);
    pushup(u);
}

void update(int u, int l, int r, int ml, int mr, int dif) {
    // if (ml > mr) {
    //     update(u, l, r, mr, ml, dif);
    //     return;
    // }
    // if (u == 1) debug("update range[%d, %d] dif %d\n", ml, mr, dif);
    if (l > mr || r < ml) return;
    if (l >= ml && r <= mr) {
        sum[u] += dif * range[u];
        tag[u] += dif;
        return;
    }
    int mid = (l + r) >> 1;
    pushdown(u);
    update(u << 1, l, mid, ml, mr, dif);
    update(u << 1 | 1, mid + 1, r, ml, mr, dif);
    pushup(u);
}

int query(int u, int l, int r, int ml, int mr) {
    // if (ml > mr) return query(u, l, r, mr, ml);
    // if (u == 1) debug("query range[%d, %d]\n", ml, mr);
    if (l > mr || r < ml) return 0;
    if (l >= ml && r <= mr) return sum[u];
    int mid = (l + r) >> 1;
    pushdown(u);
    return (query(u << 1, l, mid, ml, mr) + query(u << 1 | 1, mid + 1, r, ml, mr)) % P;
}

void dfs1(int u, int fat)
{
    siz[u] = 1;
    fa[u] = fat;
    dep[u] = dep[fat] + 1;
    for (unsigned i = 0; i < G[u].size(); i++) {
        if (G[u][i] != fat) {
            dfs1(G[u][i], u);
            siz[u] += siz[G[u][i]];
            if (siz[G[u][i]] > siz[wson[u]])
                wson[u] = G[u][i];
        }
    }
}

void dfs2(int u, int start)
{
    // debug("Vis node %d\n", u);
    // if (u == 0) {
    //     debug("Warning! Unexpected error : u defines to 0.\n");
    //     debug("Exit dfs2.\n");
    //     return;
    // }
    s[u] = start;
    dfn[u] = ++dfn_now;
    data[dfn[u]] = a[u];
    if (wson[u]) {
        dfs2(wson[u], start);
        for (unsigned i = 0; i < G[u].size(); i++) {
            if (G[u][i] != fa[u] && G[u][i] != wson[u]) {
                dfs2(G[u][i], G[u][i]);
            }
        }
    }
}

int queryline(int x, int y)
{
    int res = 0;
    while (s[x] != s[y]) {
        // debug("x(%d), y(%d)\n", x, y);
        if (dep[s[x]] < dep[s[y]]) swap(x, y);
        UpdSum(res, query(1, 1, n, dfn[s[x]], dfn[x]));
        // x = s[x];
        // if (s[x] != s[y]) x = fa[x];
        x = fa[s[x]];
    }
    if (dep[x] < dep[y]) swap(x, y);
    UpdSum(res, query(1, 1, n, dfn[y], dfn[x]));
    return res;
}

void updateline(int x, int y, int diff)
{
    diff %= P;
    while (s[x] != s[y]) {
        if (dep[s[x]] < dep[s[y]]) swap(x, y);
        update(1, 1, n, dfn[s[x]], dfn[x], diff);
        // x = s[x];
        // if (s[x] != s[y]) x = fa[x];
        x = fa[s[x]];
    }
    if (dep[x] < dep[y]) swap(x, y);
    update(1, 1, n, dfn[y], dfn[x], diff);
}

int main()
{
    n = read<int>();
    m = read<int>();
    r = read<int>();
    P = read<int>();
    for (int i = 1; i <= n; i++) a[i] = read<int>();
    for (int i = 1, u, v; i < n; i++) {
        u = read<int>();
        v = read<int>();
        G[u].push_back(v);
        G[v].push_back(u);
    }
    dfs1(r, 0);
    // passing();
    dfs2(r, r);
    // passing();
    build(1, 1, n);
    // for (int i = 1; i <= n; i++) {
    //     debug("node %d : dfn = %d, range[%d, %d]\n", i, dfn[i], dfn[i], dfn[i] + siz[i] - 1);
    // }
    for (int i = 1, op, x, y; i <= m; i++) {
        op = read<int>();
        x = read<int>();
        if (op == 1) {
            y = read<int>();
            updateline(x, y, read<int>());
        } else if (op == 2) {
            y = read<int>();
            debug("[Program Output] ");
            write(queryline(x, y), 10);
        } else if (op == 3) {
            update(1, 1, n, dfn[x], dfn[x] + siz[x] - 1, read<int>());
        } else {
            debug("[Program Output] ");
            write(query(1, 1, n, dfn[x], dfn[x] + siz[x] - 1), 10);
        }
        // debug("[debug] ");
        // for (int i = 1; i <= n; i++) debug("%d%c", query(1, 1, n, dfn[i], dfn[i]), " \n"[i == n]);
    }
    return 0;
}