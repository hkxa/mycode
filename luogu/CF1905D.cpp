/*************************************
 * @problem:      CF1905D.
 * @author:       CharmingLakesideJewel.
 * @time:         2023-12-10.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

// #define USE_FREAD  // 使用 fread  读入，去注释符号
// #define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 1e6 + 7;

int T, n, a[N], tmp[N];

namespace segt {
    struct node {
        int64 setTag, addTag, sum;
    } t[N << 2];
    void push_up(int p) {
        t[p].sum = t[p << 1].sum + t[p << 1 | 1].sum;
    }
    void push_down(int p, int l, int r) {
        if (t[p].setTag) {
            int mid = (l + r) >> 1, ls = p << 1, rs = p << 1 | 1;
            t[ls].setTag = t[rs].setTag = t[p].setTag;
            t[ls].addTag = t[rs].addTag = t[p].addTag = 0;
            t[ls].sum = (mid - l + 1) * t[p].setTag;
            t[rs].sum = (r - mid) * t[p].setTag;
            t[p].setTag = 0;
        } else if (t[p].addTag) {
            int mid = (l + r) >> 1, ls = p << 1, rs = p << 1 | 1;
            if (t[ls].setTag) t[ls].setTag += t[p].addTag;
            else t[ls].addTag += t[p].addTag;
            if (t[rs].setTag) t[rs].setTag += t[p].addTag;
            else t[rs].addTag += t[p].addTag;
            t[ls].sum += (mid - l + 1) * t[p].addTag;
            t[rs].sum += (r - mid) * t[p].addTag;
            t[p].addTag = 0;
        }
    }
    void build(int p, int l, int r, int *a) {
        t[p].setTag = t[p].addTag = 0;
        if (l == r) {
            t[p].sum = a[l];
            return;
        }
        int mid = (l + r) >> 1, ls = p << 1, rs = p << 1 | 1;
        build(ls, l, mid, a);
        build(rs, mid + 1, r, a);
        push_up(p);
    }
    void all_add() {
        if (t[1].setTag) t[1].setTag += 1;
        else t[1].addTag += 1;
        t[1].sum += n;
    }
    void range_set(int p, int l, int r, int L, int R, int val) {
        if (L <= l && r <= R) {
            t[p].setTag = val;
            t[p].addTag = 0;
            t[p].sum = (r - l + 1) * val;
            return;
        }
        push_down(p, l, r);
        int mid = (l + r) >> 1, ls = p << 1, rs = p << 1 | 1;
        if (L <= mid) range_set(ls, l, mid, L, R, val);
        if (R > mid) range_set(rs, mid + 1, r, L, R, val);
        push_up(p);
    }
}

signed main() {
    kin >> T;
    while (T--) {
        kin >> n;
        for (int i = 0; i < n; ++i) {
            kin >> a[i];
            tmp[a[i]] = i;
        }
        for (int i = 1; i < n; ++i) tmp[i] = max(tmp[i], tmp[i - 1]);
        for (int i = 0; i < n; ++i) tmp[i] = n - tmp[i];

        segt::build(1, 0, n - 1, tmp);
        int64 ans = segt::t[1].sum;
        for (int i = 1; i < n; ++i) {
            segt::all_add();
            segt::range_set(1, 0, n - 1, a[i - 1], n - 1, 1);
            ans = max(ans, segt::t[1].sum);
        }
        kout << ans << '\n';
    }
    return 0;
}