/*************************************
 * @problem:      Julia the snail.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-26.
 * @language:     C++.
 * @fastio_ver:   20200820.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore space, \t, \r, \n
            ch = getchar();
            while (ch == ' ' || ch == '\t' || ch == '\r' || ch == '\n') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            Int flag = 1;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = -1, endch = getchar();
            i = endch & 15;
            while (isdigit(endch = getchar())) i = (i << 3) + (i << 1) + (endch & 15);
            if (flag == -1) i = -i;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            Int flag = 1;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = -1, endch = getchar();
            Int i = endch & 15;
            while (isdigit(endch = getchar())) i = (i << 3) + (i << 1) + (endch & 15);
            if (flag == -1) i = -i;
            return i;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64
// #define int128 int64
// #define int128 __int128

const int N = 1e5 + 7, MAX_BLOCK = 500 + 7;

vector<int> to[N];
int prv[N], f[MAX_BLOCK][N], ans[MAX_BLOCK][N], n, m, q, sz_b = 317, cnt_b, p[N];
pair<int, int> stk[N];
pair<int, int> *tp;

#define relax(e, v) ( if (stk[e].first > v) stk[e].second = v; )

void work() {
    for (int i = 0; i < q; i++) {
        int lm, pl;
        int l = read.get<int>() - 1, r = read.get<int>();
        if (r - l > sz_b) {
            int b_r = r / sz_b;
            lm = ans[b_r][l];
            pl = b_r * sz_b;
        }
        else lm = pl = l;
        for (int j = pl; j < r; ++j)
            if (prv[j] <= lm && prv[j] >= l)
                lm = max(lm, j);
        write << lm + 1 << '\n';
    }
}

void get_input() {
    memset(prv, 0x3f, sizeof(prv));
    read >> n >> m;
    sz_b = sqrt(n);
    for (int i = 0; i < m; i++) { 
        int u = read.get<int>() - 1, v = read.get<int>() - 1;
        prv[v] = u;
        to[u].push_back(v);
    }
    // for (int i = 0; i < n; i++) printf("prv[%d] = %d\n", i, prv[i]);
    read >> q;
}

void pre_calc() {
    for (int i = 0; i < n; i++) sort(to[i].begin(), to[i].end());
    // cnt_b = ceil((double)n / sz_b);
    cnt_b = n / sz_b;
    for (int i = 1; i <= cnt_b; i++) 
        for (int j = 0; j < i * sz_b; j++) {
            while (p[j] < to[j].size() && to[j][p[j]] < i * sz_b) ++p[j];
            if (p[j]) f[i][j] = to[j][p[j] - 1];
            else f[i][j] = j;
        }
    for (int i = 1; i <= cnt_b; i++)  {
        tp = stk; // stack pointer reset
        for (int j = i * sz_b - 1; j >= 0; j--) {
            ans[i][j] = f[i][j];
            if (tp == stk) ans[i][j] = j;
            else
                while (tp != stk && f[i][j] >= (tp - 1)->first)
                    ans[i][j] = max(ans[i][j], (--tp)->second);
            tp->first = j;
            (tp++)->second = ans[i][j];
            // printf("ans[%d][%d] = %d (stack [%d, %d])\n", i, j, ans[i][j], j, ans[i][j]);
        }
    }
}

int main() {
    get_input();
    pre_calc();
    work();
    return 0;
}