/*************************************
 * @problem:      [PKUWC2018]Minimax.
 * @author:       brealid.
 * @time:         2021-05-09.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".get_result").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 3e5 + 7, P = 998244353, inv10000 = 796898467;

int n, fa[N];
struct son {
    int cnt, s1, s2;
    son() : cnt(0) {}
    void append(int s) {
        if (++cnt == 1) s1 = s;
        else s2 = s;
    }
} g[N];
int val[N], lsh_val[N], val_cnt, s[N];
int rt[N], ls[N << 5], rs[N << 5], sum[N << 5], tag_mul[N << 5];
int ans = 0, tot = 0;

void pushup(int rt) { sum[rt] = (sum[ls[rt]] + sum[rs[rt]]) % P; }

void push_tag(int rt, int v) {
    if (!rt) return;
    sum[rt] = (int64)sum[rt] * v % P;
    tag_mul[rt] = (int64)tag_mul[rt] * v % P;
}

void pushdown(int rt) {
    if (tag_mul[rt] == 1) return;
    if (ls[rt]) push_tag(ls[rt], tag_mul[rt]);
    if (rs[rt]) push_tag(rs[rt], tag_mul[rt]);
    tag_mul[rt] = 1;
}

int newnode() {
    tag_mul[++tot] = 1;
    ls[tot] = rs[tot] = sum[tot] = 0;
    return tot;
}

void update(int &p, int l, int r, int x, int v) {
    if (!p) p = newnode();
    if (l == r) {
        sum[p] = v;
        return;
    }
    pushdown(p);
    int mid = (l + r) >> 1;
    if (x <= mid) update(ls[p], l, mid, x, v);
    else update(rs[p], mid + 1, r, x, v);
    pushup(p);
}

int merge(int x, int y, int l, int r, int xmul, int ymul, int v) {
    if (!x && !y) return 0;
    if (!x) {
        push_tag(y, ymul);
        return y;
    }
    if (!y) {
        push_tag(x, xmul);
        return x;
    }
    pushdown(x), pushdown(y);
    int mid = (l + r) >> 1;
    int lsx = sum[ls[x]], lsy = sum[ls[y]], rsx = sum[rs[x]], rsy = sum[rs[y]];
    ls[x] = merge(ls[x], ls[y], l, mid, (xmul + (int64)rsy * (1 - v + P)) % P,
                  (ymul + (int64)rsx * (1 - v + P)) % P, v);
    rs[x] = merge(rs[x], rs[y], mid + 1, r, (xmul + (int64)lsy * v) % P,
                  (ymul + (int64)lsx * v) % P, v);
    pushup(x);
    return x;
}

void get_result(int x, int l, int r) {
    if (!x) return;
    if (l == r) {
        s[l] = sum[x];
        return;
    }
    int mid = (l + r) >> 1;
    pushdown(x);
    get_result(ls[x], l, mid);
    get_result(rs[x], mid + 1, r);
}

void dfs(int u) {
    if (!g[u].cnt) update(rt[u], 1, val_cnt, val[u], 1);
    else if (g[u].cnt == 1) {
        dfs(g[u].s1);
        rt[u] = rt[g[u].s1];
    } else {
        dfs(g[u].s1), dfs(g[u].s2);
        rt[u] = merge(rt[g[u].s1], rt[g[u].s2], 1, val_cnt, 0, 0, val[u]);
    }
}

signed main() {
    kin >> n;
    for (int i = 1; i <= n; ++i) {
        kin >> fa[i];
        g[fa[i]].append(i);
    }
    for (int i = 1; i <= n; ++i) {
        kin >> val[i];
        if (g[i].cnt) val[i] = (int64)val[i] * inv10000 % P;
        else lsh_val[++val_cnt] = val[i];
    }
    sort(lsh_val + 1, lsh_val + val_cnt + 1);
    for (int i = 1; i <= n; ++i)
        if (!g[i].cnt) val[i] = lower_bound(lsh_val + 1, lsh_val + val_cnt + 1, val[i]) - lsh_val;
    dfs(1);
    get_result(rt[1], 1, val_cnt);
    for (int i = 1; i <= val_cnt; ++i)
        ans = (ans + (int64)i * lsh_val[i] % P * s[i] % P * s[i]) % P;
    // for (int i = 1; i <= val_cnt; ++i)
    //     printf("value %d: p=%d\n", lsh_val[i], s[i]);
    kout << ans << '\n';
    return 0;
}