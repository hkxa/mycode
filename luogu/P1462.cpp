/*************************************
 * problem:      P1462 通往奥格瑞玛的道路.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-07-27.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, m, s, e, b;
int cost[10007];
int dis[10007];
int inq[10007];
struct Edge {
    int to, v;
    Edge() {}
    Edge(int To, int Value) : to(To), v(Value) {}
};
vector<Edge> G[10007];
typedef vector<Edge>::iterator VEI;

bool spfa(int limit)
{
    memset(dis, 0x3f, sizeof(dis));
    // memset(inq, 0, sizeof(inq));
    queue<int> q;
    q.push(s);
    dis[s] = 0;
    inq[s] = true;
    while (!q.empty()) {
        int fr = q.front(); q.pop();
        inq[fr] = false;
        for (VEI it = G[fr].begin(); it != G[fr].end(); it++) {
            if (cost[it->to] <= limit && dis[fr] + it->v < dis[it->to]) {
                dis[it->to] = dis[fr] + it->v;
                if (!inq[it->to]) {
                    q.push(it->to);
                    inq[it->to] = true;
                }
            }
        }
    }
    // for (int i = 1; i <= n; i++) {
    //     write(dis[i], 32);
    // }
    // puts("");
    return dis[e] <= b;
}

int main()
{
    n = read<int>();
    m = read<int>();
    s = 1;
    e = n;
    b = read<int>();
    int l, r, mid, ans;
    for (int i = 1; i <= n; i++) {
        cost[i] = read<int>();
        r = max(r, cost[i]);
    }
    l = max(cost[1], cost[n]);
    int u, v, hpDiff;
    for (int i = 1; i <= m; i++) {
        u = read<int>();
        v = read<int>();
        hpDiff = read<int>();
        G[u].push_back(Edge(v, hpDiff));
        G[v].push_back(Edge(u, hpDiff));
    }
    if (!spfa(r)) {
        puts("AFK");
        return 0;
    }
    while (l <= r) {
        int mid = (l + r) >> 1;
        if (spfa(mid)) {
            ans = mid;
            r = mid - 1;
        } else {
            l = mid + 1;
        }
    }
    write(ans);
    return 0;
}