/*************************************
 * @problem:      [八省联考2018]林克卡特树.
 * @user_name:    brealid/hkxadpall/zhaoyi20/j(o)moo/littleTortoise.
 * @time:         2020-08-28.
 * @language:     C++.
 * @fastio_ver:   20200827.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        // simple io flags
        ignore_int = 1 << 0,    // input
        char_enter = 1 << 1,    // output
        flush_stdout = 1 << 2,  // output
        flush_stderr = 1 << 3,  // output
        // combination io flags
        endline = char_enter | flush_stdout // output
    };
    enum number_type_flags {
        // simple io flags
        output_double_stable = 1 << 0,  // output
        output_double_faster = 1 << 1   // output
        // combination io flags
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f & ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore character set : ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (double &lf) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            lf = endch & 15;
            while (isdigit(endch = getchar())) lf = lf * 10 + (endch & 15);
            if (endch == '.') {
                double digit = 0.1;
                while (isdigit(endch = getchar())) {
                    lf += (endch & 15) * digit;
                    digit *= 0.1;
                }
            }
            if (flag) lf = -lf;
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            Int d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return d;
        }
    } read;

    struct Writer {
        Writer& operator << (io_flags f) {
            if (f & char_enter) putchar(10);
            if (f & flush_stdout) fflush(stdout);
            if (f & flush_stderr) fflush(stderr);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
        inline void operator () (double val, int eps_digit = 6, char endch = '\0', number_type_flags flg = output_double_faster) {
            if (flg & output_double_stable) {
                static char output_format[10];
                sprintf(output_format, "%%.%dlf", eps_digit);
                printf(output_format, val);
            } else if (flg & output_double_faster) {
                if (val < 0) {
                    putchar('-');
                    val = -val;
                }
                (*this) << (int64)val;
                val -= (int64)val;
                putchar('.');
                double eps_number = 0.5;
                for (int i = 1; i <= eps_digit; i++) eps_number /= 10;
                val += eps_number;
                while (eps_digit--) {
                    val *= 10;
                    putchar((int)val | '0');
                    val -= (int)val;
                }
            } else {
                (*this) << "1.#ERROR_NOT_IDENTIFIED_FLAG";
            }
            if (endch) putchar(endch);
        }
    } write;
}
using namespace Fastio;
namespace File_IO {
    void init_IO() {
        freopen("[八省联考2018]林克卡特树.in", "r", stdin);
        freopen("[八省联考2018]林克卡特树.out", "w", stdout);
    }
}

// #define int int64

const int N = 3e5 + 7, K = 3e5 + 7, STATUS = 3;
const int INF_MIN = 0xcfcfcfcf, INF_MAX = 0x3f3f3f3f;
const int SUBTASK_K = 100 + 7;

int n, k;
// int64 f[N][SUBTASK_K][STATUS], g[SUBTASK_K][STATUS];
pair<int64, int> f[N][STATUS], g[STATUS];
vector<pair<int, int> > G[N];

template <typename value_type>
inline void upd_max(value_type &pos, value_type val) { if (pos < val) pos = val; }

pair<int64, int> operator + (pair<int64, int> a, pair<int64, int> b) {
    return make_pair(a.first + b.first, a.second + b.second);
}

pair<int64, int> operator + (pair<int64, int> a, int64 b) {
    return make_pair(a.first + b, a.second);
}

void dp(int u, int fa, int64 DIF_Q) {
    f[u][0] = f[u][1] = make_pair(0ll, 0);
    f[u][2] = make_pair(DIF_Q, 1);
    for (size_t edge_id = 0; edge_id < G[u].size(); edge_id++) {
        int v = G[u][edge_id].first, w = G[u][edge_id].second;
        if (v == fa) continue;
        dp(v, u, DIF_Q);
        g[0] = f[u][0];
        g[1] = f[u][1];
        g[2] = f[u][2];
        upd_max(g[0], f[u][0] + f[v][0]);
        upd_max(g[1], f[u][1] + f[v][0]); 
        upd_max(g[1], f[u][0] + f[v][1] + w); 
        upd_max(g[2], f[u][2] + f[v][0]);  
        upd_max(g[2], f[u][1] + f[v][1] + make_pair(w + DIF_Q, 1)); 
        f[u][0] = g[0];
        f[u][1] = g[1];
        f[u][2] = g[2];
    }
    upd_max(f[u][0], f[u][2]);
    upd_max(f[u][0], f[u][1] + make_pair(DIF_Q, 1));
}

signed main() {
    // File_IO::init_IO();
    read >> n >> k; ++k;
    for (int i = 1, u, v, w; i < n; i++) {
        read >> u >> v >> w;
        G[u].push_back(make_pair(v, w));
        G[v].push_back(make_pair(u, w));
    }
    int64 l = -3e11, r = 3e11, mid, ans;
    while (l <= r) {
        mid = (l + r) >> 1;
        dp(1, 0, mid);
        if (f[1][0].second < k) l = mid + 1;
        else r = mid - 1, ans = mid;
    }
    dp(1, 0, ans);
    write << f[1][0].first - k * ans << '\n';
    return 0;
}