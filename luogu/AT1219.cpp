/*************************************
 * @problem:      歴史の研究.
 * @author:       brealid.
 * @time:         2021-03-26.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 1e5 + 7, B = 360;

int n, q, a[N], val[N];
int64 query_answer[N];
int block_size, block_count, belong[N], bl[B], br[B];
int stk[N], top = 0;
int cnt[N];

struct Query {
    int id, l, r;
    bool operator < (const Query &b) const {
        return (belong[l] ^ belong[b.l]) ? (belong[l] < belong[b.l]) : (r < b.r);
    }
} qry[N];

signed main() {
    kin >> n >> q;
    block_size = sqrt(n);
    block_count = (n - 1) / block_size + 1;
    for (int i = 1; i <= n; ++i) {
        kin >> a[i];
        val[i] = a[i];
        belong[i] = (i - 1) / block_size + 1;
        if (!bl[belong[i]]) {
            bl[belong[i]] = i;
            br[belong[i] - 1] = i - 1;
        }
    }
    br[block_count] = n;
    sort(val + 1, val + n + 1);
    for (int i = 1; i <= n; ++i) a[i] = lower_bound(val + 1, val + n + 1, a[i]) - val;
    for (int i = 1; i <= q; ++i) {
        qry[i].id = i;
        kin >> qry[i].l >> qry[i].r;
    }
    sort(qry + 1, qry + q + 1);
    int64 ans = 0;
    for (int i = 1, now = 0, pos = 0; i <= q; ++i) {
        if (now != belong[qry[i].l]) {
            while (top) cnt[stk[top--]] = 0;
            pos = br[belong[qry[i].l]];
            now = belong[qry[i].l];
            ans = 0;
        }
        if (belong[qry[i].r] == now) {
            int64 now_ans = 0;
            for (int j = qry[i].l; j <= qry[i].r; ++j)
                now_ans = max(now_ans, (int64)++cnt[a[j]] * val[a[j]]);
            for (int j = qry[i].l; j <= qry[i].r; ++j) --cnt[a[j]];
            query_answer[qry[i].id] = now_ans;
            continue;
        }
        while (pos < qry[i].r) {
            if (++cnt[a[++pos]] == 1) stk[++top] = a[pos];
            ans = max(ans, (int64)cnt[a[pos]] * val[a[pos]]);
        }
        int64 now_ans = ans;
        for (int j = qry[i].l; j <= br[belong[qry[i].l]]; ++j)
            now_ans = max(now_ans, (int64)++cnt[a[j]] * val[a[j]]);
        for (int j = qry[i].l; j <= br[belong[qry[i].l]]; ++j) --cnt[a[j]];
        query_answer[qry[i].id] = now_ans;
    }
    for (int i = 1; i <= q; ++i) kout << query_answer[i] << '\n';
    return 0;
}