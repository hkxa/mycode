/*************************************
 * @problem:      P5468 [NOI2019]回家路线.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-05-17.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}
// #define int int64
const int N = 1000000 + 7;

struct bus {
    int x, y, p, q;
} a[N];
struct node {
    int64 x, y;
    int id;
    node(int64 X = 0, int64 Y = 0, int ID = 0) : x(X), y(Y), id(ID) {}
};

int n, m;
int64 A, B, C;
int maxT;
vector<int> G[40007];
deque<node> q[100007];
queue<int64> res[40007];
int64 ans = 1e18;
int64 dp[N];
inline int64 f(int64 x) { return A * x * x + B * x + C; }
inline double slope(node a, node b) { return (double)(a.y - b.y) / (a.x - b.x); }

inline void ins(int id) {
    int pos = a[id].y;
    node now(a[id].q, dp[id] + A * a[id].q * a[id].q - B * a[id].q, id);
    while (q[pos].size() >= 2) {
        if (slope(*(--q[pos].end()), *(--(--q[pos].end()))) < slope(*(--(--q[pos].end())), now)) break;
        q[pos].pop_back();
    }
    q[pos].push_back(now);
}

inline void del(double slop, int pos) {
    while (q[pos].size() >= 2) {
        if (slope(*q[pos].begin(), *(++q[pos].begin())) > slop) return;
        q[pos].pop_front();
    }
}

signed main()
{
    // printf("REach!");
    n = read<int>();
    m = read<int>();
    A = read<int>();
    B = read<int>();
    C = read<int>();
    // printf("REach!");
    for (int i = 1; i <= m; i++) {
        a[i].x = read<int>();
        a[i].y = read<int>();
        a[i].p = read<int>();
        a[i].q = read<int>();
        G[a[i].p].push_back(i);
        maxT = max(maxT, a[i].q);
    }
    // printf("REach!");
    q[1].push_back(node(0, 0, 0));
    // printf("REach!");
    for (int t = 0; t <= maxT; t++) {
        while (!res[t].empty()) {
            ins(res[t].front());
            res[t].pop();
        }
        for (uint32 k = 0; k < G[t].size(); k++) {
            const int &id = G[t][k], &pos = a[id].x;
            if (q[pos].empty()) continue;
            del(2.0 * A * a[id].p, pos);
            int j = q[pos].front().id;
            dp[id] = dp[j] + f(a[id].p - a[j].q);
            res[a[id].q].push(id);
            if (a[id].y == n)
                ans = min(ans, dp[id] + a[id].q);
        }
    }
    // printf("REach!");
    write(ans, 10);
    return 0;
}