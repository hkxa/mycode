/*************************************
 * problem:      P2392 kkksc03考前临时抱佛脚.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-06-13.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

int s;
int a[21];
int ans = 0;
bool f[601];

int getCase()
{
    int sum(0), maxx(0), Rwall;
    for (int i = 1; i <= s; i++) {
        a[i] = read<int>();
        sum += a[i];
    }
    Rwall = sum >> 1;
    memset(f, 0, sizeof(f));
    f[0] = 1;
    for (int i = 1; i <= s; i++) {
        for (int j = Rwall; j >= a[i]; j--) {
            if (f[j - a[i]]) {
                f[j] = 1;
                maxx = max(maxx, j);
            }
        }
    }
    return sum - maxx;
}

int main()
{
    int s1, s2, s3, s4;
    s1 = read<int>();
    s2 = read<int>();
    s3 = read<int>();
    s4 = read<int>();
    s = s1; ans += getCase();
    s = s2; ans += getCase();
    s = s3; ans += getCase();
    s = s4; ans += getCase();
    write(ans);
    return 0;
}