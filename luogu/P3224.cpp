/*************************************
 * @problem:      [HNOI2012]永无乡.
 * @author:       brealid.
 * @time:         2021-02-04.
*************************************/
#include <bits/stdc++.h>
#include <ext/pb_ds/tree_policy.hpp>
#include <ext/pb_ds/assoc_container.hpp>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 1e5 + 7;

struct UnionFindSet {
    int fa[N + 5];
    UnionFindSet() {
        memset(fa, -1, sizeof(fa));
    }
    int find(int u) {
        return fa[u] < 0 ? u : fa[u] = find(fa[u]);
    }
    inline int merge(int u, int v) {
        u = find(u), v = find(v);
        if (u == v) return false;
        if (fa[u] > fa[v]) swap(u, v);
        fa[u] += fa[v];
        return fa[v] = u;
    }
} ufs;

int n, m, q, p[N];
struct compare_p {
    bool operator () (const int &a, const int &b) const {
        return p[a] < p[b];
    }
};
__gnu_pbds::tree<int, __gnu_pbds::null_type, compare_p, __gnu_pbds::rb_tree_tag, __gnu_pbds::tree_order_statistics_node_update> keys[N];

void link(int u, int v) {
    u = ufs.find(u), v = ufs.find(v);
    int nRet = ufs.merge(u, v);
    if (!nRet) return;
    if (v == nRet) swap(u, v);
    while (!keys[v].empty()) {
        keys[u].insert(*keys[v].begin());
        keys[v].erase(keys[v].begin());
    }
}

int query(int u, int x) {
    if ((int)keys[u = ufs.find(u)].size() < x) return -1;
    else return *keys[u].find_by_order(x - 1);
}

signed main() {
    kin >> n >> m;
    for (int i = 1; i <= n; ++i) {
        p[i] = kin.get<int>();
        keys[i].insert(i);
    }
    for (int i = 1; i <= m; ++i) link(kin.get<int>(), kin.get<int>());
    kin >> q;
    char op;
    for (int i = 1, x, y; i <= q; ++i) {
        kin >> op >> x >> y;
        if (op == 'B') link(x, y);
        else kout << query(x, y) << '\n';
    }
    return 0;
}