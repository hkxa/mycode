// run P5699 <P5699_addition_file\match1.in >P5699_addition_file\match1.usr
// P5699_addition_file\checker P5699_addition_file\match1.in P5699_addition_file\match1.usr EMPTY.txt
// run P5699 -f -q <P5699_addition_file\match10.in >P5699_addition_file\match10.usr & P5699_addition_file\checker P5699_addition_file\match10.in P5699_addition_file\match10.usr EMPTY.txt
// copy P5699_addition_file\match10.usr P5699_addition_file\match10_backUp.usr  
#pragma GCC optimize("-O3")
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    register Int flag = 1;
    register char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    register Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    register Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    register Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;
char Path[1007], Path2[1007];

int main()
{
    
    // for (int i = 10; i <= 10; i++) {
    //     printf("write %d...\n", i);
    //     sprintf(Path, "P5699_addition_file\\match%d.usr", i);
    //     sprintf(Path2, "P5699_addition_file\\match%d.in", i);
    //     FILE *FI = fopen(Path2, "r"), *FO = fopen(Path, "w");
    //     int n;
    //     fscanf(FI, "%d", &n);
    //     for (int j = 1; j <= n; j++) fprintf(FO, "%d\n", j);
    //     fclose(FI);
    //     fclose(FO);
    // }
    int i = 9; // To fill in
    while (1) {
        sprintf(Path, "run P-tz -f -q <P5699_addition_file\\match%d.in", i, i);
        system(Path);
        sprintf(Path2, "P5699_addition_file\\checker P5699_addition_file\\match%d.in P5699_addition_file\\match%d.usr EMPTY.txt", i, i);
        printf("Case %d : Updated - ", i);
        system(Path2);
        // i++;
        // if (i > 10) i = 6;
    }
    return 0;
}