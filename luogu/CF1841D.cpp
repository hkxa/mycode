#include <bits/stdc++.h>
using namespace std;


const int N = 2e3 + 7;

int T, n;
struct segment {
    int l, r;
    bool operator < (const segment &b) const {
        return l == b.l ? r < b.r : l < b.l;
    }
} a[N];
int f[N], segr[N];

int main() {
    scanf("%d", &T);
    while (T--) {
        scanf("%d", &n);
        for (int i = 1; i <= n; ++i) scanf("%d%d", &a[i].l, &a[i].r);
        sort(a + 1, a + n + 1);
        int ans = 0;
        for (int i = 1; i < n; ++i) {
            f[i] = 0;
            if (a[i + 1].l > a[i].r) continue;      // 单独的区间, 不能作为“左侧区间”
            for (int j = 1; j < i; ++j)
                if (segr[j] < a[i].l) f[i] = max(f[i], f[j]);
            ans = max(ans, ++f[i]);
            int leftmost_r = 1e9;
            for (int j = i + 1; j <= n; ++j)
                if (a[j].l > a[i].r) break;         // 不能作为“右侧区间”
                else leftmost_r = min(leftmost_r, a[j].r);
            segr[i] = max(a[i].r, leftmost_r);
        }
        printf("%d\n", n - ans * 2);
    }
}