/*************************************
 * @problem:      [HEOI2016/TJOI2016]排序.
 * @author:       brealid.
 * @time:         2021-02-19.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 1e5 + 7;

int n, m, qpos, a[N];
struct operation {
    int op, l, r;
} qry[N];
struct node {
    int v, l, r;
    node(int V, int L, int R) : v(V), l(L), r(R) {}
    node(int CreateL) : l(CreateL) {}
    bool operator < (const node &b) const {
        return l < b.l;
    }
};

set<node>::iterator split(set<node> &s, int pos) {
    if (pos > n) return s.end();
    // assert(s.upper_bound(node(pos)) != s.begin());
    set<node>::iterator it = --s.upper_bound(node(pos));
    node t = *it;
    s.erase(it);
    // printf("t = ([%d, %d], %d)\n", t.l, t.r, t.v);
    if (pos != t.l) s.insert(node(t.v, t.l, pos - 1));
    return s.insert(node(t.v, pos, t.r)).first;
}

void debug(set<node> s) {
    for (set<node>::iterator it = s.begin(); it != s.end(); ++it)
        fprintf(stderr, "([%d, %d], %d)%c", it->l, it->r, it->v, " \n"[it == --s.end()]);
    // assert(s.begin()->l == 1);
    // assert((--s.end())->r == n);
    // for (set<node>::iterator it = s.begin(), pre; it != s.end(); pre = it++) {
    //     assert(it->l <= it->r);
    //     if (it != s.begin()) assert(pre->r + 1 == it->l);
    // }
}

// check if answer is less than cut
int work(int cut) {
    // printf("work(%d)\n", cut);
    set<node> s;
    for (int i = 1, lst = 1; i <= n; ++i)
        if (i == n || (a[i] <= cut) != (a[i + 1] <= cut)) {
            s.insert(node(a[i] <= cut, lst, i));
            lst = i + 1;
        }
    // debug(s);
    for (int i = 1, cnt0, cnt1; i <= m; ++i) {
        set<node>::iterator it_r = split(s, qry[i].r + 1);
        set<node>::iterator it_l = split(s, qry[i].l);
        // debug(s);
        cnt0 = cnt1 = 0;
        for (set<node>::iterator it = it_l; it != it_r; ++it)
            if (it->v) cnt1 += it->r - it->l + 1;
            else cnt0 += it->r - it->l + 1;
        s.erase(it_l, it_r);
        // debug(s);
        if (qry[i].op) {
            if (cnt0) s.insert(node(0, qry[i].l, qry[i].l + cnt0 - 1));
            if (cnt1) s.insert(node(1, qry[i].r - cnt1 + 1, qry[i].r));
        } else  {
            if (cnt1) s.insert(node(1, qry[i].l, qry[i].l + cnt1 - 1));
            if (cnt0) s.insert(node(0, qry[i].r - cnt0 + 1, qry[i].r));
        }
        // debug(s);
    }
    return (--s.upper_bound(node(qpos)))->v;
}

signed main() {
    kin >> n >> m;
    for (int i = 1; i <= n; ++i) kin >> a[i];
    for (int i = 1; i <= m; ++i) kin >> qry[i].op >> qry[i].l >> qry[i].r;
    kin >> qpos;
    int l = 1, r = n, mid, ans = -1;
    while (l <= r) {
        mid = (l + r) >> 1;
        if (work(mid)) r = mid - 1, ans = mid;
        else l = mid + 1;
    }
    kout << ans << '\n';
    return 0;
}