//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      【模板】左偏树（可并堆）.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-24.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

struct Reader {
    char endch;
    Reader() { endch = '\0'; }
    template <typename Int>
    Reader& operator >> (Int &i) {
        i = baseFastio::read<Int>(endch);
        return *this;
    }
    template <typename Int>
    inline Int get_int() {
        return baseFastio::read<Int>();
    }
    inline char get_nxt() {
        return endch;
    }
} read;

struct Writer {
    Writer& operator << (const char *ch) {
        // char *p = ch;
        while (*ch) putchar(*(ch++));
        return *this;
    }
    Writer& operator << (const char ch) {
        putchar(ch);
        return *this;
    }
    template <typename Int>
    Writer& operator << (const Int i) {
        baseFastio::write(i);
        return *this;
    }
} write;

// #define int int64

const int N = 1e5 + 7;

int n, m;

struct node {
    int id;
    int val, l, r;
    int dist;
    int root;
    inline void swapLR();
    inline void keep_leftist();
    inline void init(int u, int val);
    inline int findroot();
} t[N];

inline void node::swapLR() {
    swap(l, r);
}

inline void node::keep_leftist() {
    if (t[l].dist < t[r].dist) swapLR();
}

inline void node::init(int u, int v) {
    l = r = 0;
    val = v;
    dist = 0;
    id = root = u;
}

inline int node::findroot() {
    return root == id ? id : root = t[root].findroot();
}

inline int merge(int u, int v) {
    if (!u || !v || u == v) return u | v;
    if (t[u].val > t[v].val || (t[u].val == t[v].val && u > v)) swap(u, v);
    t[u].r = merge(t[u].r, v);
    t[u].keep_leftist();
    t[t[u].l].root = t[t[u].r].root = t[u].root = u;
    t[u].dist = t[t[u].l].dist + 1;
    return u;
}

inline void pop(int u) {
    t[u].val = -1; // delete
    if (!t[u].l || !t[u].r) {
        t[u].root = t[t[u].l | t[u].r].root = t[u].l | t[u].r;
    } else t[u].root = merge(t[u].l, t[u].r);
}

void output_heap(int u) {
    write << '{' << u << ", " << t[u].val << "} ";
    if (t[u].l) {
        write << "[ ";
        output_heap(t[u].l);
        write << "] ";
    }
    if (t[u].r) {
        write << "[ ";
        output_heap(t[u].r);
        write << "] ";
    }
}

signed main()
{
    read >> n >> m;
    for (int i = 1; i <= n; i++) t[i].init(i, read.get_int<int>());
    for (int i = 1, x, y; i <= m; i++) {
        if (read.get_int<int>() == 1) {
            read >> x >> y;
            if (t[x].val == -1 || t[y].val == -1) continue;
            merge(t[x].findroot(), t[y].findroot());
        } else {
            read >> x;
            if (t[x].val == -1) write << "-1\n";
            else {
                write << t[t[x].findroot()].val << '\n';
                pop(t[x].findroot());
            }
        }
        // for (int i = 1; i <= n; i++) {
        //     if (t[i].root == t[i].id) {
        //         printf("Heap %d : ", t[i].id);
        //         output_heap(i);
        //         printf("\n");
        //     }
        // }
    }
    return 0;
}