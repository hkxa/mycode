/*************************************
 * problem:      P2264 情书.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-03-17.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 
/*************************************
 * record ID:    R17338653.
 * time:         27 ms
 * memory:       820 KB
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

int n, ans = 0;
set <string> dict;
set <string> visd;
string temp; 
bool end;

bool get(string &a)
{
    a.clear();
    char ch;
    while (scanf("%c", &ch) != EOF) {
        if (isalpha(ch)) {
            a += ch;
            break;
        }
    }
    while (scanf("%c", &ch) != EOF) {
        if (!isalpha(ch)) {
            if (ch == ',' || ch == '.') a += ch;
            break;
        }
        a += ch;
    }
    // printf("get %s\n", a.c_str());
    if (a.size() == 0) return 0;
    else return 1;
}

int main()
{
	n = read<int>();
	for (int i = 1; i <= n; i++) {
		cin >> temp;
        transform(temp.begin(), temp.end(), temp.begin(), ::tolower);
		dict.insert(temp);
        // printf("dict : add word \"%s\".\n", temp.c_str());
	}
    while (get(temp)) {
        transform(temp.begin(), temp.end(), temp.begin(), ::tolower);
        if (temp[temp.length() - 1] == ',') temp = temp.substr(0, temp.length() - 1);
        if (temp[temp.length() - 1] == '.') {
            temp = temp.substr(0, temp.length() - 1);
            end = true;
        }
        if (dict.find(temp) != dict.end()) {
            /*  && visd.find(temp) == visd.end() */
            // printf("accept \"%s\"\n", temp.c_str());
            visd.insert(temp);
            // ans++;
        }
        if (end) {
            ans += visd.size();
            visd.clear();
            end = false;
        }
    }
    write(ans);
    return 0;
} 
