/*************************************
 * problem:      P5021 赛道修建.
 * user ID:      85848.
 * user name:    hkxadpall.
 * time:         2019-08-31.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define ForInVec(vectorType, vectorName, iteratorName) for (vector<vectorType>::iterator iteratorName = vectorName.begin(); iteratorName != vectorName.end(); iteratorName++)
#define ForInVI(vectorName, iteratorName) ForInVec(int, vectorName, iteratorName)
#define ForInVE(vectorName, iteratorName) ForInVec(Edge, vectorName, iteratorName)
#define MemWithNum(array, num) memset(array, num, sizeof(array))
#define Clear(array) MemWithNum(array, 0)
#define MemBint(array) MemWithNum(array, 0x3f)
#define MemInf(array) MemWithNum(array, 0x7f)
#define MemEof(array) MemWithNum(array, -1)
#define ensuref(condition) do { if (!(condition)) exit(0); } while(0)

#ifdef DEBUG
# define passing() cerr << "passing line [" << __LINE__ << "]." << endl
# define debug(...) printf(__VA_ARGS__)
# define show(x) cerr << #x << " = " << x << endl
#else
# define passing() do if (0) cerr << "passing line [" << __LINE__ << "]." << endl;  while(0)
# define debug(...) do if (0) printf(__VA_ARGS__); while(0)
# define show(x) do if (0) cerr << #x << " = " << x << endl; while(0)
#endif

int n, m;
struct Edge {
    int to, len;
    Edge() {}
    Edge(int Bi, int Li) : to(Bi), len(Li) {}
};
vector<Edge> G[50007];

// return pair(id, len)
pair<int, int> dfs_getD(int u, int fa)
{
    int resId = u, len = 0;
    pair<int, int> tmp;
    ForInVE(G[u], it) {
        if (it->to == fa) continue;
        tmp = dfs_getD(it->to, u);
        tmp.second += it->len;
        if (tmp.second > len) {
            resId = tmp.first;
            len = tmp.second;
        }
    }
    return make_pair(resId, len);
}

void solveA()
{
    pair<int, int> ans;
    ans = dfs_getD(1, -1);
    ans = dfs_getD(ans.first, -1);
    printf("%d", ans.second);
}

void solveB()
{
    /*
    9 3
    1 2 9
    1 3 7
    1 4 5
    1 5 3
    1 6 1
    1 7 11
    1 8 10
    1 9 6
    11 10 9 7 6 5 3 1
    */
    int len[50007], minn = 10000 * 2 + 7;
    for (int i = 1; i < n; i++) {
        len[i] = G[i].rbegin()->len;
    }
    sort(len + 1, len + n);
    for (int i = 1; i <= m; i++) {
        minn = min(minn, len[n - i] + (n - (m << 1) + i - 1 >= 1 ? len[n - (m << 1) + i - 1] : 0));
        debug("minn using %d + %d = %d\n", len[n - i], (n - (m << 1) + i - 1 >= 1 ? len[n - (m << 1) + i - 1] : 0), len[n - i] + (n - (m << 1) + i - 1 >= 1 ? len[n - (m << 1) + i - 1] : 0));
    }
    printf("%d", minn);
}

bool subtaskC_check(int *len, int least)
{
    debug("subtaskC_check : least = %d\n", least);
    int sum = 0, cnt = 0;
    for (int i = 1; i < n; i++) {
        show(i);
        debug("sum : %d -> %d", sum, sum + len[i]);
        sum += len[i];
        if (sum >= least) {
            sum = 0;
            debug(" -> 0\ncnt : %d -> %d\n", cnt, cnt + 1); 
            cnt++;
        } else debug("\n");
    }
    return cnt >= m;
}

void solveC()
{
    /*
    9 3
    1 2 6
    2 3 3
    3 4 5
    4 5 10
    5 6 4
    6 7 9
    7 8 7
    8 9 4
       6     3     5    10     4     9     7     4
    1-----2-----3-----4-----5-----6-----7-----8-----9
    */
    int len[50007], sum = 0;
    for (int i = 1; i < n; i++) {
        if (i <= G[i][0].to)
            sum += (len[i] = G[i][0].len);
        else
            sum += (len[i] = G[i][1].len);
    }
    int l = 1, r = sum / m, mid, ans;
    while (l <= r) {
        mid = (l + r) >> 1;
        if (subtaskC_check(len, mid)) {
            ans = mid;
            l = mid + 1;
        } else {
            r = mid - 1;
        }
    }
    printf("%d", ans);
}

int main()
{
    scanf("%d%d", &n, &m);
    int a, b, l;
    int caseB = 1, caseC = 1;
    for (int i = 1; i < n; i++) {
        scanf("%d%d%d", &a, &b, &l);
        if (a != 1) caseB = 0;
        if (b != a + 1) caseC = 0;
        G[a].push_back(Edge(b, l));
        G[b].push_back(Edge(a, l));
    }
    if (m == 1) solveA();
    ensuref(m != 1);
    if (caseB) solveB();
    ensuref(!caseB);
    if (caseC) solveC();
    ensuref(!caseC);
    return 0;
}