//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      id_name.
 * @user_id:      63720.
 * @user_name:    brealid.
 * @time:         2020-mm-dd.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int N = 100007;
const double alpha = 0.75;

struct ScapegoatNode {
    int v, tot, siz, l, r;
} t[N];
int root;
int temp[N];
int TempCnt, NodeCnt;

bool Balance(int x) {
    return t[t[x].l].siz < t[x].siz * alpha && 
           t[t[x].r].siz < t[x].siz * alpha;
}

void Flatten(int x) {
    // printf("Flatten(%d)\n", x);
    // printf("lc = %d; rc = %d\n", t[x].l, t[x].r);
    if (!x) return;
    Flatten(t[x].l);
    if (t[x].tot) temp[++TempCnt] = x;
    Flatten(t[x].r);
}

void Build(int &x, int l, int r) {
    if (l > r) return void(x = 0);
    if (l == r) {
        x = temp[l];
        // printf("Build(v = %d, l = %d, r = %d)\n", t[x].v, l, r);
        t[x].l = 0;
        t[x].r = 0;
        t[x].siz = t[x].tot;
        return;
    }
    int mid = (l + r) >> 1;
    x = temp[mid];
    // printf("Build(v = %d, l = %d, r = %d)\n", t[x].v, l, r);
    Build(t[x].l, l, mid - 1);
    Build(t[x].r, mid + 1, r);
    t[x].siz = t[t[x].l].siz + t[t[x].r].siz + t[x].tot;
}

void ReBuild(int &x) {
    // printf("Rebuild\n");
    TempCnt = 0;
    Flatten(x);
    // printf("Flatten => node {%d", t[temp[1]].v);
    // for (int i = 2; i <= TempCnt; i++) printf(", %d", t[temp[i]].v);
    // printf("}\n");
    Build(x, 1, TempCnt);
}

void Check(int &a) {
    if (!a) return;
    // printf("Checking node which v = %d\n", t[a].v);
    if (!Balance(a)) ReBuild(a);
    else if (t[t[a].l].siz > t[t[a].r].siz) Check(t[a].l);
    else Check(t[a].r);
}

void Insert(int &a, int v) {
    if (!a) {
        a = ++NodeCnt;
        t[a].v = v;
        t[a].tot = 1;
    } else if (v == t[a].v) {
        t[a].tot++;
    } else if (v < t[a].v) {
        Insert(t[a].l, v);
    } else {
        Insert(t[a].r, v);
    }
    t[a].siz++;
}

void Delete(int a, int v) {
    assert(a);
    if (v == t[a].v) {
        t[a].tot--;
    } else if (v < t[a].v) {
        Delete(t[a].l, v);
    } else {
        Delete(t[a].r, v);
    }
    t[a].siz--;
}

int GetVal(int rank) {
    int a = root;
    while (true) {
        // if (a) printf("rank = %d, t[t[a].l].siz = %d, t[a].tot = %d, t[a].val = %d\n", rank, t[t[a].l].siz, t[a].tot, t[a].v);
        if (t[t[a].l].siz < rank && t[t[a].l].siz + t[a].tot >= rank) return t[a].v;
        else if (t[t[a].l].siz >= rank) a = t[a].l;
        else rank -= t[t[a].l].siz + t[a].tot, a = t[a].r;
    }
}

int GetRank(int val) {
    int a = root, rank = 1;
    while (a) {
        if (t[a].v >= val) a = t[a].l;     
        else rank += t[t[a].l].siz + t[a].tot, a = t[a].r;     
    }
    return rank;
}

int n;
int opt, x;

void PreorderTraversal(int u, int dep = 1) {
    if (!u) return;
    for (int i = 1; i <= dep; i++) printf("  ");
    printf("- node V = %d, Tot = %d, Siz = %d, l = %d, r = %d\n", t[u].v, t[u].tot, t[u].siz, t[t[u].l].v, t[t[u].r].v);
    PreorderTraversal(t[u].l, dep + 1);
    PreorderTraversal(t[u].r, dep + 1);
}

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        opt = read<int>();
        x = read<int>();
        // printf("Receive Option %d (argument = %d)\n", opt, x);
        switch (opt) {
            case 1: Insert(root, x); Check(root); break;
            case 2: Delete(root, x); Check(root); break;
            case 3: write(GetRank(x), 10); break;
            case 4: write(GetVal(x), 10); break;
            case 5: write(GetVal(GetRank(x) - 1), 10); break;
            case 6: write(GetVal(GetRank(x + 1)), 10); break;
            default: 
                printf("The tree status : (node opt values, not ids)\n");
                printf("- root = %d\n", t[root].v);
                printf("- Preorder traversal\n");
                PreorderTraversal(root);
                break;
        }
        // if (opt == 5 && x == 130285) {
        //     printf("The tree status : (node opt values, not ids)\n");
        //     printf("- root = %d\n", t[root].v);
        //     printf("- Preorder traversal\n");
        //     PreorderTraversal(root);
        // }
    }
    return 0;
}