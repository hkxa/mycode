/*************************************
 * @problem:      P3370 【模板】字符串哈希.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2019-11-13.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int PowNum[5] = {11, 13, 26, 909, 2019};
int Primes[5] = {19260817, 20050321, 20170933, 201709, 2017};
struct Hash_table {
    struct Hash_result {
        int a[5];

        Hash_result() {
            memset(a, 0, sizeof(a));
        }

        bool operator < (const Hash_result &other) const {
            for (int i = 0; i < 5; i++) {
                if (a[i] < other.a[i]) return 1;
                if (a[i] > other.a[i]) return 0;
            }
            return 0;
        }

        bool operator == (const Hash_result &other) const {
            for (int i = 0; i < 5; i++) {
                if (a[i] != other.a[i]) return 0;
            }
            return 1;
        }

        bool operator != (const Hash_result &other) const {
            for (int i = 0; i < 5; i++) {
                if (a[i] != other.a[i]) return 1;
            }
            return 0;
        }
    };
    Hash_result operator () (char a[]) {
        Hash_result res;
        for (int i = 0, len = strlen(a); i < len; i++) {
            for (int k = 0; k < 5; k++) {
                res.a[k] = (res.a[k] * PowNum[k] + a[i]) % Primes[k];
            }
        }
        return res;
    }
} Hash;

char s[1500 + 7];
Hash_table::Hash_result a[10000 + 7];
int n;

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        scanf("%s", s);
        a[i] = Hash(s);
    }
    sort(a + 1, a + n + 1);
    int ans = 1;
    for (int i = 1; i < n; i++) {
        if (a[i] != a[i + 1]) ans++;
    }
    write(ans);
    return 0;
}