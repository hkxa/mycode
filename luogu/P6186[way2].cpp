/*************************************
 * @problem:      bubble.
 * @user_id:      ZJ-00625.
 * @time:         2020-03-07.
 * @language:     C++.
 * @upload_place: CCF - NOI Online.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}
 
#define lowbit(x) ((x) & (-(x)))
#define int int64

int n, m, a[200007], b[200007], d[200007];
int64 c[200007],ans;

inline void upd(int x, int num)
{
    // printf("upd %lld %c%lld\n", x, (num > 0 ? '+' : '\0'), num);
    while (x <= n) {
        c[x] += num;
        x += lowbit(x);
    }
}
inline int query(int x, int base = 0)
{
    // printf("query %lld\n", x);
    while (x) {
        base += c[x];
        x -= lowbit(x);
    }
    return base;
}

/**
 * cmd code @1 : run P6186[way2] <"D:\赵奕\下载\P6186_1.in"
 * cmd code @2 : run P6186[way2] <"D:\赵奕\下载\P6186_1.in" >t.out -q -f & fc "D:\赵奕\下载\P6186_1.out" t.out & DEL t.out 
 * to run this program
 */

signed main()
{
    int opt, x, tmp = 0;
    n = read<int>();
    m = read<int>();
    for (int i = 1; i <= n; i++) {
        a[i] = read<int>();
        b[i] = i - 1 - query(a[i]);
        ans += b[i], ++d[b[i]];
        upd(a[i], 1);
    }
    memset(c, 0, sizeof(c));
    upd(1, ans);
    for (int i = 0; i < n; i++) {
        tmp += d[i];
        upd(i + 2, tmp - n);
    }
    for (int i = 1; i <= m; i++) {
        opt = read<int>();
        x = min(read<int>(), n - 1);
        if (opt == 1) {
            swap(a[x], a[x + 1]);
            swap(b[x], b[x + 1]);
            // 此时 a[x], a[x + 1] 已经交换，所以下面的 '<' 换为 '>'
            if (a[x] > a[x + 1]) {
                upd(1, 1);
                upd(b[x + 1] + 2, -1);
                b[x + 1]++;
            }
            else {
                upd(1, -1);
                b[x]--;
                upd(b[x] + 2,1);
            }
        }
        else write(query(x + 1), 10);
    }
    return 0;
}