/*************************************
 * @problem:      CF1313D Happy New Year.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-02-26.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define ChkMax(a, b) { if ((a) < (b)) (a) = (b); }
// template <typename Tp>
// inline void ChkMax(Tp &a, Tp b) 
// { if (b > a) a = b; }

struct Magic {
    int l, r;
    bool operator < (const Magic b) const { return l != b.l ? l < b.l : r < b.r;}
} p[400005];

int disc[400005], discCnt = 0, n, m, K, t, used[400005], id[9][400005], ed[9][400005], mst[400005], mask[400005], f[2][1 << 9];
int siz[400005], adt[1 << 9];

void init() {
    sort(disc + 1, disc + 1 + discCnt);
    t = unique(disc + 1, disc + 1 + discCnt) - disc - 1;
    for (int i = 1; i <= n; i++) {
        p[i].l = lower_bound(disc + 1, disc + 1 + t, p[i].l) - disc;
        siz[p[i].l] = disc[p[i].l] - disc[p[i].l-1];
        p[i].r = lower_bound(disc + 1, disc + 1 + t, p[i].r) - disc; 
        siz[p[i].r] = disc[p[i].r] - disc[p[i].r-1];
    }
    for (int i = 1; i <= t; i++) siz[i] = disc[i] - disc[i - 1];
}
signed main()
{
    n = read<int>();
    m = read<int>();
    K = read<int>();
    for (int i = 1; i <= n; i++) {
        p[i].l = read<int>();
        p[i].r = read<int>();
        disc[++discCnt] = p[i].l; 
        disc[++discCnt] = p[i].r;
        disc[++discCnt] = max(p[i].l - 1, 1);
    }
    init();
    sort(p + 1, p + 1 + n);
    for (int i = 0; i < K; i++) {
        int last = 0, cnt = 0;
        for (int j = 1; j <= n; j++) {
            if(!used[j] && p[j].l > last) {
                last = p[j].r; used[j] = 1; cnt++;
                for (int k = p[j].l; k <= p[j].r; k++)
                    id[i][k] = cnt;
                ed[i][p[j].r] = 1;
            }
        }
    }
    for (int j = 1; j <= t; j++) 
        for (int i = 0; i < K; i++) 
            mst[j] |= (1 << i) * (id[i][j] != 0), mask[j] |= (1 << i) * (ed[i][j] == 1);
    for (int i = 0; i < (1 << K); i++) adt[i] = (__builtin_popcount(i) & 1);
    for (int i = 1; i <= t; i++) {
        memset(f[i & 1], 0, sizeof(f[i & 1]));
        for (int j = 0; j < (1 << K); j++) {
            if((j | mst[i]) != mst[i]) continue;
            int last = (j & mst[i - 1]) & (mask[i - 1] ^ mst[i - 1]);
            for (int k = mask[i - 1]; k; k = (k - 1) & mask[i - 1])
                ChkMax(f[i & 1][j], f[(i & 1) ^ 1][last | k] + adt[j] * siz[i]);
            ChkMax(f[i & 1][j], f[(i & 1) ^ 1][last] + adt[j] * siz[i]) ; 
        }
    } 
    int res = 0;
    for (int j = 0; j < (1 << K); j++) ChkMax(res, f[t & 1][j]);
    write(res, 10);
    return 0;
}