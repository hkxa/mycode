/*************************************
 * @problem:      P4719 【模板】"动态 DP"&动态树分治.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-04-16.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#ifdef DEBUG
# define passing() cerr << "passing line [" << __LINE__ << "]." << endl
# define debug(...) fprintf(stderr, __VA_ARGS__)
# define show(x) cerr << #x << " = " << (x) << endl
#else
# define passing() do if (0) cerr << "passing line [" << __LINE__ << "]." << endl; while(0)
# define debug(...) do if (0) printf(__VA_ARGS__); while(0)
# define show(x) do { if (0) cerr << #x << " = " << (x) << endl; } while(0)
#endif

const int N = 1e5 + 3, Int_Min = -1073741823;
// const int N = 1e5 + 3, Int_Min = INT_MIN;

int n, m, v[N];
vector<int> G[N];
int fa[N], wson[N], siz[N], dfn[N], id[N], dfn_cnt = 0;
int beg[N], end[N];
int f[2][N];

struct Matrix {
    int a[2][2];
    int *operator [] (int pos) { return a[pos]; }
    Matrix(int ori = Int_Min) { a[0][0] = a[0][1] = a[1][0] = a[1][1] = ori; }
    Matrix operator * (Matrix other) {
        Matrix res(Int_Min);
        for (register int i = 0; i < 2; i++) {
            for (register int j = 0; j < 2; j++) {
                for (register int k = 0; k < 2; k++) {
                    res[i][j] = max(res[i][j], a[i][k] + other[k][j]);
                }
            }
        }
        debug("%d ", a[0][0]);
        debug("%d\n", a[0][1]);
        debug("%d ", a[1][0]);
        debug("%d\n", a[1][1]);

        debug("mul\n");
        
        debug("%d ", other[0][0]);
        debug("%d\n", other[0][1]);
        debug("%d ", other[1][0]);
        debug("%d\n", other[1][1]);

        debug("equals to\n");

        debug("%d ", res[0][0]);
        debug("%d\n", res[0][1]);
        debug("%d ", res[1][0]);
        debug("%d\n\n", res[1][1]);
        return res;
    }
    // Matrix operator - (Matrix other) {
    //     Matrix res;
    //     for (register int i = 0; i < 2; i++) {
    //         for (register int j = 0; j < 2; j++) {
    //             res[i][j] = a[i][j] - other[i][j];
    //         }
    //     }
    //     return res;
    // }
} t[N * 4], val[N];

Matrix UnitMatrix() {
    Matrix res;
    res[0][0] = res[1][1] = 0;
    return res;
}

void pushup(int u) { t[u] = t[u << 1] * t[u << 1 | 1]; }

void build(int u, int l, int r)
{
    if (l == r) {
        debug("line_tree node %d : question_tree node %d\n", u, id[l]);
        debug("%d ", val[id[l]][0][0]);
        debug("%d\n", val[id[l]][0][1]);
        debug("%d ", val[id[l]][1][0]);
        debug("%d\n\n", val[id[l]][1][1]);
        t[u] = val[id[l]];
        return;
    }
    int mid = (l + r) >> 1;
    build(u << 1, l, mid);
    build(u << 1 | 1, mid + 1, r);
    pushup(u);
}

int rangeL, rangeR;
#define SetRange(L, R) { rangeL = (L); rangeR = (R); }
Matrix query(int u, int l, int r)
{
    if (l > rangeR || r < rangeL) return UnitMatrix();
    if (l >= rangeL && r <= rangeR) return t[u];
    int mid = (l + r) >> 1;
    return query(u << 1, l, mid) * query(u << 1 | 1, mid + 1, r);
}

int updPos;
#define SetPos(position) { updPos = (position); }
void update(int u, int l, int r)
{
    // printf("update(%d, %d, %d)\n", u, l, r);
    // if (l > updPos || r < updPos) return;
    if (l == updPos && r == updPos) {
        debug("[linetree update]\n");
        debug("original matrix : \n");
        debug("%d ", t[u][0][0]);
        debug("%d\n", t[u][0][1]);
        debug("%d ", t[u][1][0]);
        debug("%d\n", t[u][1][1]);
        debug("update to : \n");
        debug("%d ", val[id[updPos]][0][0]);
        debug("%d\n", val[id[updPos]][0][1]);
        debug("%d ", val[id[updPos]][1][0]);
        debug("%d\n\n", val[id[updPos]][1][1]);
        t[u] = val[id[updPos]];
        return;
    }
    int mid = (l + r) >> 1;
    if (mid >= updPos) update(u << 1, l, mid);
    else update(u << 1 | 1, mid + 1, r);
    pushup(u);
}

void dfs1(int u, int ff)
{
    fa[u] = ff;
    siz[u] = 1;
    for (unsigned i = 0; i < G[u].size(); i++) {
        if (G[u][i] != ff) {
            dfs1(G[u][i], u);
            siz[u] += siz[G[u][i]];
            if (siz[G[u][i]] > siz[wson[u]]) 
                wson[u] = G[u][i];
        }
    }
}

int dfs2(int u, int head)
{
    id[dfn[u] = ++dfn_cnt] = u;
    beg[u] = head;
    if (!wson[u]) {
        f[0][u] = val[u][0][0] = val[u][0][1] = 0;
        f[1][u] = val[u][1][0] = v[u];
        // val[u][1][1] = Int_Min;
        return end[u] = u;
    }
    int g0 = 0, g1 = v[u];
    end[u] = dfs2(wson[u], head);
    for (unsigned i = 0; i < G[u].size(); i++) {
        if (G[u][i] != fa[u] && G[u][i] != wson[u]) {
            dfs2(G[u][i], G[u][i]);
            g0 += max(f[0][G[u][i]], f[1][G[u][i]]);
            g1 += f[0][G[u][i]];
        }
    }
    f[0][u] = g0 + max(f[0][wson[u]], f[1][wson[u]]);
    f[1][u] = g1 + f[0][wson[u]];
    val[u][0][0] = val[u][0][1] = g0;
    val[u][1][0] = g1;
    // val[u][1][1] = Int_Min;
    debug("node %d : F0 = %d, F1 = %d;\n", u, f[0][u], f[1][u]);
    return end[u];
}

void updatePath(int u, int w)
{
    val[u][1][0] += w - v[u];
    v[u] = w;
    Matrix bef, aft;
    while (u) {
        show(u); show(beg[u]); show(end[u]);
        SetRange(dfn[beg[u]], dfn[end[u]]);
        SetPos(dfn[u]);
        bef = query(1, 1, n);
        update(1, 1, n);
        aft = query(1, 1, n);

        u = fa[beg[u]];
        debug("[Path(line) update]\n");
        debug("original matrix : \n");
        debug("%d ", val[u][0][0]);
        debug("%d\n", val[u][0][1]);
        debug("%d ", val[u][1][0]);
        debug("%d\n\n", val[u][1][1]);
        val[u][0][0] += max(aft[0][0], aft[1][0]) - max(bef[0][0], bef[1][0]);
        val[u][0][1] = val[u][0][0];
        val[u][1][0] += aft[0][0] - bef[0][0];
        debug("update to : \n");
        debug("%d ", val[u][0][0]);
        debug("%d\n", val[u][0][1]);
        debug("%d ", val[u][1][0]);
        debug("%d\n\n", val[u][1][1]);
    }
}

int main()
{
    show(Int_Min + Int_Min);
    n = read<int>();
    m = read<int>();
    for (int i = 1; i <= n; i++) {
        v[i] = read<int>();
    }
    for (int i = 1, u, v; i < n; i++) {
        u = read<int>();
        v = read<int>();
        G[u].push_back(v);
        G[v].push_back(u);
    }
    dfs1(1, 0);
    dfs2(1, 1);
    build(1, 1, n);
    Matrix mRet;
    for (int i = 1, u, w; i <= m; i++) {
        u = read<int>();
        w = read<int>();
        updatePath(u, w);
        SetRange(dfn[beg[1]], dfn[end[1]]);
        mRet = query(1, 1, n);
        show(max(mRet[0][0], mRet[1][0]));
        write(max(mRet[0][0], mRet[1][0]), 10);
    }
    return 0;
}

/*
10 1
-11 80 -99 -76 56 38 92 -51 -34 47 
2 1
3 1
4 3
5 2
6 2
7 1
8 2
9 4
10 7
9 -44


10 1
-11 80 -99 -76 56 38 92 -51 -34 47 
2 1
3 1
4 3
5 2
6 2
7 1
8 2
9 4
10 7
5 67

10 2
-11 80 -99 -76 56 38 92 -51 -34 47 
2 1
3 1
4 3
5 2
6 2
7 1
8 2
9 4
10 7
9 -44
2 -17

3 3
-1 -2 -3
1 2
1 3
1 -1
1 0
1 1

10 3
-11 80 -99 -76 56 38 92 -51 -34 47 
2 1
3 1
4 3
5 2
6 2
7 1
8 2
9 4
10 7
2 98
7 -58
8 48

// Todo ? 并 ? 
// 独立集 ?
*/