/*************************************
 * @problem:      【模板】杜教筛（Sum）.
 * @user_name:    brealid.
 * @time:         2020-11-12.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;

typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

const int N = 1e6;
bool IsntPrime[N + 7];
int primes[N + 7], pcnt;
int mu[N + 7];
int64 phi[N + 7];

void init() {
    phi[1] = mu[1] = 1;
    for (int i = 2; i <= N; ++i) {
        if (!IsntPrime[i]) primes[++pcnt] = i, mu[i] = -1, phi[i] = i - 1;
        for (int p = 1; p <= pcnt && i * primes[p] <= N; ++p) {
            IsntPrime[i * primes[p]] = true;
            if (i % primes[p] == 0) {
                phi[i * primes[p]] = phi[i] * primes[p];
                break;
            }
            phi[i * primes[p]] = phi[i] * (primes[p] - 1);
            mu[i * primes[p]] = -mu[i];
        }
        mu[i] += mu[i - 1];
        phi[i] += phi[i - 1];
    }
}

uint64 MemSumPhi[10007];
uint64 SumPhi(int n, int dep = 1) {
    if (n <= N) return phi[n];
    uint64 &ret = MemSumPhi[dep];
    if (ret) return ret;
    ret = (uint64)n * (n + 1ull) >> 1;
    for (int l = 2, r; l <= n && l >= 2; l = r + 1) {
        r = n / (n / l);
        ret -= (uint64)(r - l + 1) * SumPhi(n / l, dep * l);
    }
    return ret;
}

int MemSumMu[10007];
int SumMu(int n, int dep = 1) {
    if (n <= N) return mu[n];
    int &ret = MemSumMu[dep];
    if (ret) return ret;
    ret = 1;
    for (int l = 2, r; l <= n && l >= 2; l = r + 1) {
        r = n / (n / l);
        ret -= (r - l + 1) * SumMu(n / l, dep * l);
    }
    return ret;
}

int T, n;

signed main() {
    init();
    read >> T;
    while (T--) {
        read >> n;
        int DepMax = n / N + 1;
        memset(MemSumMu + 1, 0, DepMax * sizeof(int));
        memset(MemSumPhi + 1, 0, DepMax * sizeof(uint64));
        write << SumPhi(n) << ' ' << SumMu(n) << '\n';
    }
    return 0;
}