/*************************************
 * @problem:      Codeforces Round #687 (Div. 1, based on Technocup 2021 Elimination Round 2).
 * @author:       hkxadpall.
 * @time:         2020-11-29.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 5e3 + 7;

int n;
struct event {
    int64 x, t;
    bool operator < (const event &b) const {
        return t < b.t;
    }
} a[N];
bool ok[N][N];
int64 mintime[N];

inline void upd_min(int64 &val, const int64 &nv) { if (val > nv) val = nv; }
inline int64 dis(const int &p, const int &q) { return abs(a[p].x - a[q].x); }

signed main() {
    kin >> n;
    for (int i = 1; i <= n; ++i) kin >> a[i].t >> a[i].x;
    memset(mintime, 0x3f, sizeof(mintime));
    sort(a + 1, a + n + 1);
    mintime[0] = 0;
    ok[1][1] = 1;
    for (int i = 0; i < n; ++i) {
        if (mintime[i] <= a[i].t) {
            upd_min(mintime[i + 1], max(a[i].t, mintime[i] + dis(i, i + 1)));
            for (int j = i + 2; j <= n; ++j)
                if (max(a[i].t, mintime[i] + dis(i, j)) + dis(j, i + 1) <= a[i + 1].t) ok[i + 1][j] = true;
        }
        if (ok[i][i + 1] && i + 2 <= n) {
            upd_min(mintime[i + 2], max(a[i + 1].t, a[i].t + dis(i, i + 2)));
            for (int j = i + 3; j <= n; ++j)
                if (max(a[i + 1].t, a[i].t + dis(i, j)) + dis(j, i + 2) <= a[i + 2].t) ok[i + 2][j] = true;
        }
        if (a[i].t + dis(i, i + 1) <= a[i + 1].t)
            for (int j = i + 2; j <= n; ++j)
                if (ok[i][j]) ok[i + 1][j] = true;
    }
    kout << ((ok[n - 1][n] || mintime[n] <= a[n].t) ? "YES" : "NO") << '\n';
    return 0;
}