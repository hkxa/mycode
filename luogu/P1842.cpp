/*************************************
 * problem:      P1842 奶牛玩杂技.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-05-19.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

struct cow {
    long long w, s;

    bool operator < (const cow &other) const
    {
        return w + s < other.w + other.s;
    }

    void input()
    {
        w = read<long long>();
        s = read<long long>();
    }
} c[50007];

int n;

long long wsum = 0, minn = -2147483648LL;

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        c[i].input();
    }
    sort(c + 1, c + n + 1);
    for (int i = 1; i <= n; i++) {
        if (wsum - c[i].s > minn) minn = wsum - c[i].s;
        wsum += c[i].w;
    }
    write(minn);
    return 0;
}