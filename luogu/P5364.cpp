/*************************************
 * @problem:      [SNOI2017]礼物.
 * @author:       brealid.
 * @time:         2020-12-01.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#define USE_FREAD  // 使用 fread  读入，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) { // 由于这道题的特殊性, 本函数读入 unsigned
            endch = getchar();
            while ((!isdigit(endch)) && endch != EOF) endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };
}
Fastio::Reader kin;

const int M = 13, P = 1e9 + 7;

inline void upd_sum(int &a, const int &b) { if ((a += b) >= P) a -= P; }

int64 n;
int k;

struct mat {
    int v[M][M];
    void clear() {
        memset(v, 0, sizeof(v));
    }
    mat operator * (const mat &b) const {
        mat ret;
        ret.clear();
        for (int i = 0; i < M; ++i)
            for (int j = 0; j < M; ++j)
                for (int l = 0; l < M; ++l)
                    upd_sum(ret.v[i][l], (int64)v[i][j] * b.v[j][l] % P);
        return ret;
    }
};

signed main() {
    kin >> n >> k;
    mat a;
    a.clear();
    for (int i = 0; i <= k; ++i) {
        a.v[i][0] = 1;
        for (int j = 1; j <= i; ++j)
            a.v[i][j] = a.v[i - 1][j - 1] + a.v[i - 1][j];
    }
    for (int i = 0; i <= k; ++i) a.v[k + 2][i] = a.v[k + 1][i] = a.v[k][i];
    a.v[k + 1][k + 2] = 1;
    a.v[k + 2][k + 2] = 2;
    // for (int i = 0; i <= k + 2; ++i)
    //     for (int j = 0; j <= k + 2; ++j)
    //         cout << a.v[i][j] << " \n"[j == k + 2];
    mat res;
    res.clear();
    for (int i = 0; i <= k + 2; ++i) res.v[i][i] = 1;
    while (n) {
        if (n & 1) res = res * a;
        a = a * a;
        n >>= 1;
    }
    cout << res.v[k + 1][0] << '\n';
    return 0;
}