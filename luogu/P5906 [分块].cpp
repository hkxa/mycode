/*************************************
 * @problem:      【模板】回滚莫队&不删除莫队.
 * @author:       brealid.
 * @time:         2021-03-22.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 2e5 + 7, B = 5264 + 7;

int n, m;
int block, bcnt, belong[N], bl[B], br[B];
int a[N];
vector<int> where[N];
int block_ans[B][B];
int first[N];

inline int LeftMost(int col, int l) {
    vector<int>::iterator it = lower_bound(where[col].begin(), where[col].end(), l);
    // assert(it != where[col].end());
    return *it;
}

inline int RightMost(int col, int r) {
    vector<int>::iterator it = upper_bound(where[col].begin(), where[col].end(), r);
    // assert(it != where[col].begin());
    return *--it;
}

void discrete() {
    int Occured[N];
    for (int i = 1; i <= n; ++i) Occured[i] = a[i];
    sort(Occured + 1, Occured + n + 1);
    for (int i = 1; i <= n; ++i) {
        a[i] = lower_bound(Occured + 1, Occured + n + 1, a[i]) - Occured;
        where[a[i]].push_back(i);
    }
}

signed main() {
    kin >> n;
    block = pow(n, 0.301);
    bcnt = (n + block - 1) / block;
    for (int b = 1; b <= bcnt; ++b) {
        bl[b] = (b - 1) * block + 1;
        br[b] = min(b * block, n);
        for (int i = bl[b]; i <= br[b]; ++i) belong[i] = b;
    }
    for (int i = 1; i <= n; ++i) kin >> a[i];
    discrete();
    for (int b1 = 1; b1 <= bcnt; ++b1) {
        int now = 0;
        for (int b2 = b1; b2 <= bcnt; ++b2) {
            for (int i = bl[b2]; i <= br[b2]; ++i)
                if (first[a[i]]) now = max(now, i - first[a[i]]);
                else first[a[i]] = i;
            block_ans[b1][b2] = now;
        }
        // for (int i = bl[b1]; i <= n; ++i) first[a[i]] = 0;
        memset(first + 1, 0, sizeof(int) * n);
    }
    kin >> m;
    for (int i = 1, l, r, b1, b2; i <= m; ++i) {
        kin >> l >> r;
        b1 = belong[l], b2 = belong[r];
        // printf("QUERY [%d, %d] (block %d ~ %d)\n", l, r, b1, b2);
        if (b1 == b2) {
            int ans(0);
            for (int i = l; i <= r; ++i)
                ans = max(ans, RightMost(a[i], r) - i);
            kout << ans << '\n';
        } else {
            int ans(block_ans[b1 + 1][b2 - 1]);
            for (int i = l; i <= br[b1]; ++i)
                ans = max(ans, RightMost(a[i], r) - i);
            for (int i = bl[b2]; i <= r; ++i)
                ans = max(ans, i - LeftMost(a[i], l));
            kout << ans << '\n';
        }
    }
    return 0;
}