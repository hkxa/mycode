/*************************************
 * problem:      P3017.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-07-27.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int r, c, a, b;
int n[507][507] = {0};
int sum[507][507] = {0};

#define squareSum (sum[i][j] - sum[si - 1][j] - sum[i][sj - 1] + sum[si - 1][sj - 1])

bool check(int cnt)
{
    for (int i = 1, row = 0, si = 1; i <= r; i++) {
        for (int j = 1, line = 0, sj = 1; j <= c; j++) {
            if (squareSum >= cnt) {
                line++;
                sj = j + 1;
            }
            if (line == b) {
                row++;
                line = 0;
                si = i + 1;
                break;
            }
        }
        if (row == a) return true;
    }
    return false;
}

int main()
{
    r = read<int>();
    c = read<int>();
    a = read<int>();
    b = read<int>();
    for (int i = 1; i <= r; i++) {
        for (int j = 1; j <= c; j++) {
            n[i][j] = read<int>();
            sum[i][j] = sum[i][j - 1] + sum[i - 1][j] - sum[i - 1][j - 1] + n[i][j];
        }
    }
    int L = 0, R = sum[r][c] / a / b, mid, ans;
    while (L <= R) {
        mid = (L + R) >> 1;
        if (check(mid)) {
            L = mid + 1;
            ans = mid;
        } else {
            R = mid - 1;
        }
    }
    write(ans, 10);
    return 0;
}