/*************************************
 * problem:      id_name.
 * user ID:      85848.
 * user name:    hkxadpall.
 * time:         2019-mm-dd.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define ForInVec(vectorType, vectorName, iteratorName) for (vector<vectorType>::iterator iteratorName = vectorName.begin(); iteratorName != vectorName.end(); iteratorName++)
#define ForInVI(vectorName, iteratorName) ForInVec(int, vectorName, iteratorName)
#define ForInVE(vectorName, iteratorName) ForInVec(Edge, vectorName, iteratorName)
#define MemWithNum(array, num) memset(array, num, sizeof(array))
#define Clear(array) MemWithNum(array, 0)
#define MemBint(array) MemWithNum(array, 0x3f)
#define MemInf(array) MemWithNum(array, 0x7f)
#define MemEof(array) MemWithNum(array, -1)
#define ensuref(condition) do { if (!(condition)) exit(0); } while(0)

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &ch)       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    ch = c;
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

struct FastIOer {
#   define createReadlnInt(type)            \
    FastIOer& operator >> (type &x)         \
    {                                       \
        x = read<type>();                   \
        return *this;                       \
    }
    createReadlnInt(short);
    createReadlnInt(unsigned short);
    createReadlnInt(int);
    createReadlnInt(unsigned);
    createReadlnInt(long long);
    createReadlnInt(unsigned long long);
#   undef createReadlnInt

#   define createReadlnReal(type)           \
    FastIOer& operator >> (type &x)         \
    {                                       \
        char c;                             \
        x = read<long long>(c);             \
        if (c != '.') return *this;         \
        type r = (x >= 0 ? 0.1 : -0.1);     \
        while (isdigit(c = getchar())) {    \
            x += r * (c & 15);              \
            r *= 0.1;                       \
        }                                   \
        return *this;                       \
    }
    createReadlnReal(double);
    createReadlnReal(float);
    createReadlnReal(long double);

#   define createWritelnInt(type)           \
    FastIOer& operator << (type x)          \
    {                                       \
        write<type>(x);                     \
        return *this;                       \
    }
    createWritelnInt(short);
    createWritelnInt(unsigned short);
    createWritelnInt(int);
    createWritelnInt(unsigned);
    createWritelnInt(long long);
    createWritelnInt(unsigned long long);
#   undef createWritelnInt
    
    FastIOer& operator >> (char &x)
    {
        x = getchar();
        return *this;
    }
    
    FastIOer& operator << (char x)
    {
        putchar(x);
        return *this;
    }
    
    FastIOer& operator << (const char *x)
    {
        int __pos = 0;
        while (x[__pos]) {
            putchar(x[__pos++]);
        }
        return *this;
    }
} fast;

int n, m, v, e;
int c[2007], d[2007];
long long dis[307][307];
double p[2007];
#define r(x) (1 - p[x])
double f[2007][2007][2];

int main()
{
    MemBint(dis);
    fast >> n >> m >> v >> e;
    for (int i = 0; i <= v; i++) 
        dis[i][i] = 0;
    for (int i = 0; i <= n; i++) 
        for (int j = 0; j <= m; j++) 
            f[i][j][0] = f[i][j][1] = 1e10;
    for (int i = 1; i <= n; i++) fast >> c[i];
    for (int i = 1; i <= n; i++) fast >> d[i];
    for (int i = 1; i <= n; i++) fast >> p[i];
    int a, b, w;
    for (int i = 1; i <= e; i++) {
        fast >> a >> b >> w;
        if (w < dis[a][b]) dis[a][b] = dis[b][a] = w;
    }
    for (int k = 1; k <= v; k++) {
        for (int i = 1; i <= v; i++) if (i != k) {
            for (int j = 1; j <= v; j++) if (j != k && i != j) {
                if (dis[i][j] > dis[i][k] + dis[k][j]) {
                    dis[i][j] = dis[i][k] + dis[k][j];
                }
            }
        }
    }
#   define S1 c[i - 1]
#   define S2 d[i - 1]
#   define E1 c[i]
#   define E2 d[i]
    f[1][0][0] = f[1][1][1] = 0;
    for (int i = 2; i <= n; i++) {
        f[i][0][0] = f[i - 1][0][0] + dis[c[i - 1]][c[i]];
        // printf("course No.%d with no change : %.2lf.\n", i, f[i][0][0]);
        for (int j = 1; j <= min(i, m); j++) {
            f[i][j][0] = min(f[i][j][0], 
                         min(f[i - 1][j][0] + dis[S1][E1], 
                             f[i - 1][j][1] + dis[S1][E1] * r(i - 1)
                                            + dis[S2][E1] * p[i - 1]));
            f[i][j][1] = min(f[i][j][1], 
                         min(f[i - 1][j - 1][0] + dis[S1][E1] * r(i) + dis[S1][E2] * p[i], 
                             f[i - 1][j - 1][1] + dis[S1][E1] * r(i - 1) * r(i)
                                                + dis[S1][E2] * r(i - 1) * p[i]
                                                + dis[S2][E1] * p[i - 1] * r(i)
                                                + dis[S2][E2] * p[i - 1] * p[i])); 
            // printf("course No.%d with %d's change : %.2lf, %.2lf.\n", i, j, f[i][j][0], f[i][j][1]);
        }
    }
    double ans = 1e10;
    for (int i = 0; i <= m; i++) ans = min(ans, min(f[n][i][0], f[n][i][1]));
    printf("%.2lf", ans);
    return 0;
}