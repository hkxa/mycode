/*************************************
 * problem:      P1972 [SDOI2009]HH的项链.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-03-09.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 
/*************************************
 * record ID:    R17051130.
 * status:       Unac 70 pts.
 * time:         4277 ms
 * memory:       14280 KB
 * ststus - AC:  7 blocks, 70 pts.
 * ststus - PC:  0 blocks, 0 pts.
 * ststus - WA:  0 blocks, 0 pts.
 * ststus - RE:  0 blocks, 0 pts. 
 * ststus - TLE: 3 blocks, 30 pts. 
 * ststus - MLE: 0 blocks, 0 pts. 
 * ststus - OLE: 0 blocks, 0 pts. 
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

int n, a[500007], m, block;
int cnt[1000007] = {0}, ans;
int res[500007];

struct ques {
	int id;
	int l, r;
	bool operator < (const ques &other) const {
		return (id / block) ^ (other.id / block) ? 
				/* 不是一个块 */ id < other.id : 
				/*   是一个块 */ r < other.r;
	}
	void input(int thisId)
	{
		id = thisId;
		l = read<int>();
		r = read<int>();
	}
} q[500007];


namespace mos {
	void add(int pos)
	{
		cnt[a[pos]]++;
		if (cnt[a[pos]] == 1) {
			ans++;
		}
	}
	void del(int pos)
	{
		cnt[a[pos]]--;
		if (cnt[a[pos]] == 0) {
			ans--;
		}
	}
}

int main()
{
	n = read<int>();
	for (int i = 1; i <= n; i++) {
		a[i] = read<int>(); 
	} 
	m = read<int>();
	for (int i = 1; i <= m; i++) {
		q[i].input(i); 
	} 
	block = n / sqrt(m * 0.618);
	sort(q + 1, q + m + 1);
	int xl = 1, xr = 1;
	ans = cnt[a[1]] = 1;
	for (int i = 1; i <= m; i++) {
		int &l = q[i].l, &r = q[i].r;
		while (xl < l) mos::del(xl++);
		while (xl > l) mos::add(--xl);
		while (xr < r) mos::add(++xr);
		while (xr > r) mos::del(xr--);
		res[q[i].id] = ans;
	}
	for (int i = 1; i <= m; i++) {
		write(res[i]);
		putchar(10);
	}
	return 0;
} 
