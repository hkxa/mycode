/*************************************
 * @contest:      Codeforces Round #684 (Div. 1).
 * @author:       hkxadpall.
 * @time:         2020-11-17.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#if false /* 需要使用 fread 优化，改此参数为 true */
#define getchar() (i_P1 == i_P2 && (i_P2 = (i_P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), i_P1 == i_P2) ? EOF : *i_P1++)
char BUF[1 << 21], *i_P1 = BUF, *i_P2 = BUF;
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

char ss[107];
int T, n, m, a[107][107];
int x[30007][6], y;

signed main() {
    for (kin >> T; T--;) {
        kin >> n >> m;
        for (int i = 1; i <= n; ++i) {
            kin >> (ss + 1);
            for (int j = 1; j <= m; ++j)
                a[i][j] = (ss[j] & 1);
        }
        for (int j = 1; j <= n; ++j) a[j][m + 1] = 0;
        for (int j = 1; j <= m + 1; ++j) a[n + 1][j] = 0;
        y = 0;
        for (int i = 1; i <= n; i += 2)
            for (int j = 1; j <= m; j += 2) {
                if (i == n && j == m) {
                    if (a[i][j]) {
                        x[++y][0] = i;
                        x[y][1] = j;
                        x[y][2] = i - 1;
                        x[y][3] = j;
                        x[y][4] = i;
                        x[y][5] = j - 1;
                        x[++y][0] = i;
                        x[y][1] = j;
                        x[y][2] = i - 1;
                        x[y][3] = j - 1;
                        x[y][4] = i;
                        x[y][5] = j - 1;
                        x[++y][0] = i;
                        x[y][1] = j;
                        x[y][2] = i - 1;
                        x[y][3] = j - 1;
                        x[y][4] = i - 1;
                        x[y][5] = j;
                    }
                } else if (i == n) {
                    if (a[i][j] ^ a[i][j + 1]) {
                        x[++y][0] = i;
                        x[y][1] = j;
                        x[y][2] = i - 1;
                        x[y][3] = j;
                        x[y][4] = i;
                        x[y][5] = j + 1;
                        x[++y][0] = i;
                        x[y][1] = j;
                        x[y][2] = i - 1;
                        x[y][3] = j + 1;
                        x[y][4] = i;
                        x[y][5] = j + 1;
                    }
                    if (a[i][j]) {
                        x[++y][0] = i;
                        x[y][1] = j;
                        x[y][2] = i - 1;
                        x[y][3] = j;
                        x[y][4] = i - 1;
                        x[y][5] = j + 1;
                    }
                    if (a[i][j + 1]) {
                        x[++y][0] = i;
                        x[y][1] = j + 1;
                        x[y][2] = i - 1;
                        x[y][3] = j;
                        x[y][4] = i - 1;
                        x[y][5] = j + 1;
                    }
                } else if (j == m) {
                    if (a[i][j] ^ a[i + 1][j]) {
                        x[++y][0] = i;
                        x[y][1] = j;
                        x[y][2] = i + 1;
                        x[y][3] = j;
                        x[y][4] = i;
                        x[y][5] = j - 1;
                        x[++y][0] = i;
                        x[y][1] = j;
                        x[y][2] = i + 1;
                        x[y][3] = j;
                        x[y][4] = i + 1;
                        x[y][5] = j - 1;
                    }
                    if (a[i][j]) {
                        x[++y][0] = i;
                        x[y][1] = j;
                        x[y][2] = i;
                        x[y][3] = j - 1;
                        x[y][4] = i + 1;
                        x[y][5] = j - 1;
                    }
                    if (a[i + 1][j]) {
                        x[++y][0] = i + 1;
                        x[y][1] = j;
                        x[y][2] = i;
                        x[y][3] = j - 1;
                        x[y][4] = i + 1;
                        x[y][5] = j - 1;
                    }
                } else {
                    if (a[i][j] ^ a[i + 1][j] ^ a[i][j + 1]) {
                        x[++y][0] = i;
                        x[y][1] = j;
                        x[y][2] = i + 1;
                        x[y][3] = j;
                        x[y][4] = i;
                        x[y][5] = j + 1;
                    }
                    if (a[i + 1][j + 1] ^ a[i + 1][j] ^ a[i][j + 1]) {
                        x[++y][0] = i + 1;
                        x[y][1] = j + 1;
                        x[y][2] = i + 1;
                        x[y][3] = j;
                        x[y][4] = i;
                        x[y][5] = j + 1;
                    }
                    if (a[i + 1][j + 1] ^ a[i + 1][j] ^ a[i][j]) {
                        x[++y][0] = i + 1;
                        x[y][1] = j + 1;
                        x[y][2] = i + 1;
                        x[y][3] = j;
                        x[y][4] = i;
                        x[y][5] = j;
                    }
                    if (a[i + 1][j + 1] ^ a[i][j + 1] ^ a[i][j]) {
                        x[++y][0] = i + 1;
                        x[y][1] = j + 1;
                        x[y][2] = i;
                        x[y][3] = j + 1;
                        x[y][4] = i;
                        x[y][5] = j;
                    }
                }
            }
        kout << y << '\n';
        for (int i = 1; i <= y; ++i)
            kout << x[i][0] << ' ' << x[i][1] << ' ' << x[i][2] << ' '
                 << x[i][3] << ' ' << x[i][4] << ' ' << x[i][5] << '\n';
    }
    return 0;
}