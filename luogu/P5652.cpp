/*************************************
 * @problem:      Monthly Contest III.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2019-11-13.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

unsigned n, m, q, type;
bool couldVis[1000007];
int win[1000007];
// int nearbyWin[1000007];
typedef int FuncGetInt(void);
FuncGetInt *geti;

int A, B, C, P;
inline int rnd() { return A = (A * B + C) % P; }
inline int randm() { return rnd() % n + 1; }

unsigned ans = 0;

int dfn[1000007], low[1000007], dfs_cnt = 0;
vector<int> G[1000007];

int Pre[1000007], nearby[1000007];

void dfs(int u)
{
    dfn[u] = ++dfs_cnt;
    for (unsigned i = 0; i < G[u].size(); i++) {
        dfs(G[u][i]);
    }
    low[u] = dfs_cnt;
}

int main()
{
    n = read<unsigned>();
    m = read<unsigned>();
    q = read<unsigned>();
    type = read<unsigned>();
    for (unsigned i = 1; i <= n; i++) couldVis[i] = read<int>() & 1;
    if (type == 0) geti = read<int>;
    else {
        A = read<int>();
        B = read<int>();
        C = read<int>();
        P = read<int>();
        geti = randm;
    }
    for (unsigned i = 1; i <= n; i++) Pre[i] = couldVis[i] ? i : Pre[i - 1];
    for (unsigned i = 1; i <= m + 1; i++) nearby[i] = 0;
    for (unsigned i = m + 2; i <= n; i++) nearby[i] = Pre[i - m - 1];
    for (unsigned i = 1; i <= n; i++) G[nearby[i]].push_back(i);
    dfs(0);
    for (unsigned i = 1, l, r; i <= q; i++) {
        l = geti();
        r = geti();
        if (l > r) swap(l, r);
        r = Pre[r];
        // 判断 r 是否在 l 的子树里
        if (dfn[r] < dfn[l] || dfn[r] > low[l]) ans += i * i;
    }
    write(ans);
    return 0;
}