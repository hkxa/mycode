/*************************************
 * @problem:      Monthly Contest III.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2019-11-13.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

struct node {
    int rest; int64 S;
    bool operator < (const node &b) const { return S > b.S; }
};

int n, k, x;
int64 ans = 0;
int64 a[1000007], w[1000007];
priority_queue<node> q;

int main()
{
    n = read<int>();
    k = read<int>();
    for (int i = 1; i <= n; i++) a[i] = read<int>();
    for (int i = 1; i <= n; i++) w[i] = read<int>();
    for (int i = n - 1; i >= 1; i--) w[i] += w[i + 1]; // 这一步 : $w_i 变成了 \sum_{j = i}^{n}w_i$
    for (int i = 1; i <= n; i++) {
        if (w[i] > 0) x += k, ans += k * w[i], q.push((node){k * 2, w[i]});
        else x -= k, ans -= k * w[i];
        while (x > a[i]) {
            node t = q.top(); q.pop();
            if (x - t.rest < a[i]) {
                t.rest -= x - a[i];
                ans -= (x - a[i]) * t.S;
                x = a[i];
                q.push(t);
            } else {
                x -= t.rest;
                ans -= t.rest * t.S;
            }
        }
    }
    write(ans);
    return 0;
}