/*************************************
 * @problem:      GCD SUM.
 * @user_name:    brealid.
 * @time:         2020-11-12.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

const int N = 1e5 + 7;
bool IsntPrime[N];
int primes[N], pcnt;
int phi[N];
int64 presum_phi[N];

void init(int n) {
    for (int i = 2; i <= n; ++i) {
        if (!IsntPrime[i]) primes[++pcnt] = i, phi[i] = i - 1;
        presum_phi[i] = presum_phi[i - 1] + phi[i];
        for (int p = 1; p <= pcnt && i * primes[p] <= n; ++p) {
            IsntPrime[i * primes[p]] = true;
            if (i % primes[p] == 0) {
                phi[i * primes[p]] = phi[i] * primes[p];
                break;
            }
            phi[i * primes[p]] = phi[i] * (primes[p] - 1);
        }
    }
}

int n;

#define getsum(l, r) ((int64)(l + r) * (r - l + 1) >> 1)

signed main() {
    init(1e5);
    read >> n;
    int64 ans = 0;
    for (int l = 1, r; l <= n; l = r + 1) {
        r = n / (n / l);
        ans += getsum(l, r) * (2 * presum_phi[n / l] + 1);
    }
    write << ans << '\n';
    return 0;
}