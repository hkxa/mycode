//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      【模板】可持久化数组（可持久化线段树/平衡树）.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-18.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 998244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? Module(w - P) : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

const int N = 1e6 + 7, SIZ = 3e7 + 7;

int n, m;
int a[N];
int root[N], val[SIZ], l[SIZ], r[SIZ], cnt;

void build_tree(int u, int ml = 1, int mr = n) {
    if (ml == mr) {
        val[u] = a[ml];
        return;
    }
    int mid = (ml + mr) >> 1;
    build_tree(l[u] = ++cnt, ml, mid);
    build_tree(r[u] = ++cnt, mid + 1, mr);
}

void modify_tree(int base, int u, int loc, int newval, int ml = 1, int mr = n) {
    if (ml == mr) {
        val[u] = newval;
        return;
    }
    int mid = (ml + mr) >> 1;
    if (loc <= mid) {
        r[u] = r[base];
        modify_tree(l[base], l[u] = ++cnt, loc, newval, ml, mid);
    } else {
        l[u] = l[base];
        modify_tree(r[base], r[u] = ++cnt, loc, newval, mid + 1, mr);
    }
}

inline void modify(int i, int v, int loc, int newval) {
    root[i] = ++cnt;
    modify_tree(root[v], cnt, loc, newval);
}

int query_tree(int u, int loc, int ml = 1, int mr = n) {
    if (ml == mr) return val[u];
    int mid = (ml + mr) >> 1;
    if (loc <= mid) return query_tree(l[u], loc, ml, mid);
    else return query_tree(r[u], loc, mid + 1, mr);
}

inline int query(int i, int v, int loc) {
    root[i] = ++cnt;
    l[cnt] = l[root[v]];
    r[cnt] = r[root[v]];
    return query_tree(cnt, loc);
}

signed main() {
    n = read<int>();
    m = read<int>();
    for (int i = 1; i <= n; i++) a[i] = read<int>();
    build_tree(root[0] = ++cnt);
    for (int i = 1, v, opt, loc, value; i <= m; i++) {
        v = read<int>();
        opt = read<int>();
        loc = read<int>();
        if (opt == 1) {
            value = read<int>();
            modify(i, v, loc, value);
        } else {
            write(query(i, v, loc), 10);
        }
    }
    return 0;
}

// Create File Date : 2020-06-18