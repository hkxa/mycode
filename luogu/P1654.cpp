/*************************************
 * problem:      P1654.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-**-**.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

/*
x1[i]=(x1[i-1]+1)*p[i];
x2[i]=(x2[i-1]+2*x1[i-1]+1)*p[i];
ans[i]=ans[i-1]+(3*x2[i-1]+3*x1[i-1]+1)*p[i];
*/
int n;
double p[100007] = {0};
double x1[100007] = {0}, x2[100007] = {0}, ans[100007] = {0};

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        scanf("%lf", p + i);
    }
    for (int i = 1; i <= n; i++) {
        x1[i] = (x1[i - 1] + 1) * p[i];
        x2[i] = (x2[i - 1] + 2 * x1[i - 1] + 1) * p[i];
        ans[i] = ans[i - 1] + (3 * x2[i - 1] + 3 * x1[i - 1] + 1) * p[i];
    }
    printf("%.1lf", ans[n]);
    return 0;
}