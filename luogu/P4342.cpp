/*************************************
 * @problem:      P4342 [IOI1998]Polygon.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-01-20.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

typedef int Func(int, int);

int op_plus(int a, int b) { return a + b; }
int op_multi(int a, int b) { return a * b; }

int n;
Func *oper[107];
int f[107][107], g[107][107];
int maxans = -32768;
vector<int> ans;

#define debugf(i, j) printf("debug - f[%d][%d] = %d\n", i, j, f[i][j]);
#define max4(a, b, c, d) max(max(a, b), max(c, d))
#define min4(a, b, c, d) min(min(a, b), min(c, d))

int main()
{
    memset(f, 0xcf, sizeof(f));
    memset(g, 0x3f, sizeof(g));
    n = read<int>();
    char buf[3];
    for (int i = 1; i <= n; i++) {
        scanf("%s", buf);
        if (buf[0] == 't') oper[i] = oper[i + n] = op_plus;
        else oper[i] = oper[i + n] = op_multi;
        f[i][i] = f[i + n][i + n] = g[i][i] = g[i + n][i + n] = read<int>();
    }
    for (int l = 2; l <= n; l++) {
        for (int i = 1, j = l; j < n * 2; i++, j++) {
            for (int k = i; k < j; k++) {
                f[i][j] = max(f[i][j], max4(oper[k + 1](f[i][k], f[k + 1][j]),
                                            oper[k + 1](f[i][k], g[k + 1][j]),
                                            oper[k + 1](g[i][k], f[k + 1][j]),
                                            oper[k + 1](g[i][k], g[k + 1][j])));
                g[i][j] = min(g[i][j], min4(oper[k + 1](f[i][k], f[k + 1][j]),
                                            oper[k + 1](f[i][k], g[k + 1][j]),
                                            oper[k + 1](g[i][k], f[k + 1][j]),
                                            oper[k + 1](g[i][k], g[k + 1][j])));
            }
        }
    }
    for (int i = 1; i <= n; i++) {
        if (f[i][i + n - 1] > maxans) {
            maxans = f[i][i + n - 1];
            ans.clear();
            ans.push_back(i);
        } else if (f[i][i + n - 1] == maxans) ans.push_back(i);
    }
    write(maxans, 10);
    for (unsigned i = 0; i < ans.size(); i++) write(ans[i], 32);
    return 0;
}