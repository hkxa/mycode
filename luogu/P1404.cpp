/*************************************
 * @problem:      P1404 平均数.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-02-01.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, L;
int a[100007]; // sum of a[1..i]
double f[100007]; // 以 i 结尾的最大子段平均数

bool check(double x)
{
    for (int i = 1; i < L; i++) 
        f[i] = max(f[i - 1] + a[i] - a[i - 1] - x, 0.0);
    for (int i = L; i <= n; i++) {
        f[i] = max(f[i - L] + a[i] - a[i - L] - L * x, 0.0);
        if (f[i] > 0) return true;
    }
    return false;
}

int main()
{
    n = read<int>();
    L = read<int>();
    for (int i = 1; i <= n; i++)
        a[i] = a[i - 1] + read<int>();
    double l = 0, r = a[n] / L + 0.1, mid;
    while (r - l > 1e-5) {
        mid = (l + r) / 2;
        if (check(mid)) l = mid;
        else r = mid;
    }
    write((int)(r * 1000), 10);
    return 0;
}