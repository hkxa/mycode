/*************************************
 * problem:      P1247 ȡ�����Ϸ.
 * user ID:      63720.
 * user name:    �����Ű�.
 * time:         2019-03-14.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 
/*************************************
 * record ID:    R17215599.
 * time:         103 ms
 * memory:       2580 KB
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

int n, a[500007];
int todo = 0;

bool nim()
{
	for (int i = 1; i <= n; i++) {
		todo ^= a[i]; 
	}
	return todo != 0;
}

void win()
{
//	printf("debug : todo = %d\n", todo);
	for (int i = 1; i <= n; i++) {
		if (a[i] >= (a[i] ^ todo)) {
			write(a[i] - (a[i] ^ todo), 32);
			write(i, 10);
			a[i] ^= todo;
			break;
		}
	}
	for (int i = 1; i <= n; i++) {
		write(a[i], 32);
	}
}

int main()
{
	n = read<int>();
	for (int i = 1; i <= n; i++) {
		a[i] = read<int>(); 
	}
	if (nim()) win();
	else puts("lose");
	return 0;
}
