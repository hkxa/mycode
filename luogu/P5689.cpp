/*************************************
 * @problem:      [CSP-SJX2019]多叉堆.
 * @user_name:    brealid.
 * @time:         2020-11-04.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

const int N = 3e5 + 7, P = 1e9 + 7;

int64 fpow(int64 a, int n) {
    int64 ret = 1;
    while (n) {
        if (n & 1) ret = ret * a % P;
        a = a * a % P;
        n >>= 1;
    }
    return ret;
}

struct CombinationNumber {
    int64 fac[N], ifac[N];
    CombinationNumber() {
        fac[0] = ifac[0] = 1;
        for (int i = 1; i < N; ++i) fac[i] = fac[i - 1] * i % P;
        ifac[N - 1] = fpow(fac[N - 1], P - 2);
        for (int i = N - 1; i > 1; --i) ifac[i - 1] = ifac[i] * i % P;
    }
    int64 operator() (int n, int m) {
        return fac[n] * ifac[m] % P * ifac[n - m] % P;
    }
} comb;

struct MultiBranch_Tree {
    int fa[N];
    int64 f[N];
    MultiBranch_Tree() {
        fill(fa, fa + N, -1);
        fill(f, f + N, 1);
    }
    int find(int u) {
        return fa[u] < 0 ? u : fa[u] = find(fa[u]);
    }
    void merge(int u, int v) {
        u = find(u);
        v = find(v);
        // set fa[u] to v
        fa[v] += fa[u];
        // printf("f[v] = %lld * %lld * %lld (C(%d, %d))\n", f[v], f[u], comb(-fa[v], -fa[u]), -fa[v], -fa[u]);
        f[v] = f[v] * f[u] % P * comb(-fa[v] - 1, -fa[u]) % P;
        fa[u] = v;
    }
    const int64& query(int u) {
        return f[find(u)];
    }
} tree;

signed main() {
    int n, q, ans = 0, x, y;
    read >> n >> q;
    while (q--) {
        if (read.get<int>() == 1) {
            x = (read.get<int>() + ans) % n;
            y = (read.get<int>() + ans) % n;
            tree.merge(x, y);
        } else {
            x = (read.get<int>() + ans) % n;
            write << (ans = tree.query(x)) << '\n';
        }
        // for (int i = 1; i <= n; ++i)
        //     write << tree.query(i) << " \n"[i == n];
    }
    return 0;
}