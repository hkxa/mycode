/*************************************
 * problem:      P1879 [USACO06NOV]������Corn Fields.
 * user ID:      63720.
 * user name:    �����Ű�.
 * time:         2019-03-06.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 
/*************************************
 * record ID:    R16950605.
 * time:         29 ms
 * memory:       692 KB
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

int m, n;
int situation[12 + 5] = {0};
int possibility[1007] = {0}, top = 0;
int f[12 + 5][1007] = {0};

#define getBit(num, place) ((bool)((num) & (1 << ((place) - 1))))
#define modNum 100000000

void prepare_possible_situation()
{
	bool could;
	for (int i = 0; i <= (1 << n) - 1; i++) {
		could = true;
		for (int j = 1; j < n; j++) {
			if (getBit(i, j) && getBit(i, j + 1)) {
				could = false;
				break;
			}
		}
		if (could) {
			possibility[++top] = i;
//			printf("The status \"%d\" is ok.\n", i);
		}
	}
} 

bool check_near(int a, int b)
{
//	printf("check_near() : checking possibility[%d] and possibility[%d]...\n", a, b);
	for (int i = 1; i <= n; i++) {
		if (getBit(possibility[a], i) && getBit(possibility[b], i)) {
//			printf("check_near() : %d with %d in bit %d [wrong].\n", possibility[a], possibility[b], i);
			return false;
		}
	}
	return true;
} 

bool check_thisline(int lineid, int possid)
{
	for (int i = 1; i <= n; i++) {
		if (getBit(situation[lineid], i) == false && getBit(possibility[possid], i)) {
			return false;
		}
	}
	return true;
} 

/*
�����������
��������#1�� 
2 3
1 1 1
0 1 0
�������#1�� 
9
*/

int main()
{
	m = read<int>();
	n = read<int>();
	prepare_possible_situation();
	int tmp;
	for (int i = 1; i <= m; i++) {
		for (int j = 1; j <= n; j++) {
			tmp = read<int>();
			situation[i] = (situation[i] << 1) + tmp;
		}
//		printf("situation[%d] : %d\n", i, situation[i]); 
	} 
	for (int i = 1; i <= top; i++) {
		if (check_thisline(1, i)) {
			f[1][i] = 1;
		}
//		printf("%d, ", f[1][i]);
	}
//	printf("\b\b \n");
	for (int i = 2; i <= m; i++) {
		for (int j = 1; j <= top; j++) {
            if (!check_thisline(i - 1, j)) {
//            	printf("Door A : pass line %d in situation %d.\n", i - 1, j);
            	continue;
			}
            for (int k = 1; k <= top; k++) {
            	if (!check_thisline(i, k)) {
//	            	printf("Door B : pass line %d in situation %d.\n", i, k);
	            	continue;
				}
                if (!check_near(j, k)) {
//	            	printf("Door C : pass line %d(now = %d) and line %d(now = %d).\n", i - 1, j, i, k);
	            	continue;
				}
//                printf("f[%d][%d] can base on f[%d][%d].\n", i, k, i - 1, j);
                f[i][k] = (f[i][k] + f[i - 1][j]) % modNum;
            }
        }
    }
    int ans = 0;
    /*
    n = 3:
    000
    001
    010
    100
    101
    */
    for (int i = 1; i <= top; i++) {
//    	write(f[m][i]), putchar(' ');
    	ans = (ans + f[m][i]) % modNum;
	}
//	putchar('\xa');
//	printf("ans = ");
    write(ans);
	return 0;
}
