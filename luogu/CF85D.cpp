/*************************************
 * @problem:      Sum of Medians.
 * @user_name:    brealid.
 * @time:         2020-11-14.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

char get_op() {
    static char buffer[4];
    scanf("%s", buffer);
    return buffer[0];
}

const int N = 1e5 + 7;

struct segtree_node {
    int ls, rs;
    int siz;
    int64 val[5];
    void clear() {
        ls = rs = siz = 0;
        memset(val, 0, sizeof(val));
    }
} tr[N << 5];

void pushup(int u) {
    tr[u].siz = tr[tr[u].ls].siz + tr[tr[u].rs].siz;
    int shift = 5 - tr[tr[u].ls].siz % 5;
    for (int i = 0; i < 5; ++i) 
        tr[u].val[i] = tr[tr[u].ls].val[i] + tr[tr[u].rs].val[(i + shift) % 5];
}

class MemoryPool {
private:
    int pool_stack[N << 3], stack_top, unused_node;
public:
    MemoryPool() : stack_top(0), unused_node(0) {}
    void recycle(int node) {
        pool_stack[++stack_top] = node;
        tr[node].clear();
    }
    int get() {
        return stack_top ? pool_stack[stack_top--] : ++unused_node;
    }
} pool;

class segment_tree {
private:
    int root;
    void BASE_insert_or_modify(int &u, int l, int r, int pos) {
        if (!u) u = pool.get();
        if (l == r) {
            tr[u].siz = 1;
            tr[u].val[1] = pos;
            return;
        }
        int mid = (l + r) >> 1;
        if (pos <= mid) BASE_insert_or_modify(tr[u].ls, l, mid, pos);
        else BASE_insert_or_modify(tr[u].rs, mid + 1, r, pos);
        pushup(u);
    }
    void BASE_erase(int &u, int l, int r, int pos) {
        if (l == r) {
            pool.recycle(u);
            u = 0;
            return;
        }
        int mid = (l + r) >> 1;
        if (pos <= mid) BASE_erase(tr[u].ls, l, mid, pos);
        else BASE_erase(tr[u].rs, mid + 1, r, pos);
        if (!tr[u].ls && !tr[u].rs) {
            pool.recycle(u);
            u = 0;
        } else pushup(u);
    }
public:
    segment_tree() : root(0) {}
    void insert(int pos) { BASE_insert_or_modify(root, 1, 1e9, pos); }
    void modify(int pos) { BASE_insert_or_modify(root, 1, 1e9, pos); }
    void erase(int pos) { BASE_erase(root, 1, 1e9, pos); }
    int64 query_sum() { return tr[root].val[3]; }
} segt;

int n;

signed main() {
    read >> n;
    while (n--) {
        switch (get_op()) {
            case 'a': segt.insert(read.get<int>()); break;
            case 'd': segt.erase(read.get<int>()); break;
            case 's': write << segt.query_sum() << '\n'; break;
        }
    }
    return 0;
}