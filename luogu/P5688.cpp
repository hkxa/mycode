/*************************************
 * @problem:      [CSP-SJX2019]散步.
 * @user_name:    brealid.
 * @time:         2020-11-04.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 1
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

const int N = 2e5 + 7;
int n, m, L;
int a[N], l[N];
int ans[N];

struct exit_channel {
    map<int, int> channel;
    void init() {
        for (int i = 1; i <= m; ++i)
            channel[a[i]] = i;
    }
    int prev(int place) {
        map<int, int>::iterator it = channel.upper_bound(place);
        if (it == channel.begin()) it = channel.end();
        return (--it)->second;
    }
    int next(int place) {
        map<int, int>::iterator it = channel.lower_bound(place);
        if (it == channel.end()) it = channel.begin();
        return it->second;
    }
    void report(int p, int answer) {
        ans[p] = answer;
        if (!--l[answer]) channel.erase(a[answer]);
    }
    bool empty() {
        return channel.empty();
    }
} channel;

struct person {
    int id, channel_id, direction;
    int64 tim;
    person(int ID, int C_ID, int DIR, int64 TIM) : 
           id(ID), channel_id(C_ID), direction(DIR), tim(TIM) {}
    bool operator < (const person &b) const {
        return tim == b.tim ? id > b.id : tim > b.tim;
    }
};

priority_queue<person> q;

signed main() {
    read >> n >> m >> L;
    for (int i = 2; i <= m; ++i) read >> a[i];
    for (int i = 1; i <= m; ++i) read >> l[i];
    channel.init();
    for (int i = 1, s, b; i <= n; ++i) {
        read >> s >> b;
        if (s) { // 顺时针
            int cid = channel.prev(b);
            int tim = (b - a[cid] + L) % L;
            q.push(person(i, cid, 1, tim)); 
        } else { // 逆时针
            int cid = channel.next(b);
            int tim = (a[cid] - b + L) % L;
            q.push(person(i, cid, 0, tim)); 
        }
    }
    while (!q.empty() && !channel.empty()) {
        person p = q.top(); q.pop();
        // printf("pop: person %d (pos %d, time_used %lld, dir %d)\n", p.id, a[p.channel_id], p.tim, p.direction);
        if (!l[p.channel_id]) {
            if (p.direction) { // 顺时针
                int cid = channel.prev(a[p.channel_id]);
                int tim = (a[p.channel_id] - a[cid] + L) % L;
                q.push(person(p.id, cid, 1, p.tim + tim));  
            } else { // 逆时针
                int cid = channel.next(a[p.channel_id]);
                int tim = (a[cid] - a[p.channel_id] + L) % L;
                q.push(person(p.id, cid, 0, p.tim + tim));
            }
        } else {
            channel.report(p.id, p.channel_id);
        }
    }
    int64 all_ans = 0;
    for (int i = 1; i <= n; ++i)
        all_ans ^= (int64)i * ans[i];
    write << all_ans << '\n';
    return 0;
}