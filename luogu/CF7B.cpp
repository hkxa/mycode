/*************************************
 * problem:      CF7B Memory Manager.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-05-13.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

#define int long long

struct LR {
    int l, r, idx;
    int length;
    LR(int L = 0, int R = 0, int IDX = 0) : l(L), r(R), idx(IDX), length(r - l + 1) {}
    bool operator < (const LR &other) const
    {
        return l > other.l;
    }
};

struct Memory_Manager {
    int from[107], size;
    int l[107], r[107], top;

    Memory_Manager() : top(0)
    {
        memset(from, 0, sizeof(from));
    }

    void alloc(int n)
    {
        bool isSuccess = 0;
        for (int i = 1; i <= size - n + 1; i++) {
            bool couldUse = 1;
            for (int j = 0; j < n; j++) {
                if (from[i + j]) {
                    couldUse = 0;
                    i = i + j;
                    break;
                }
            }
            if (couldUse) {
                top++;
                l[top] = i;
                r[top] = i + n - 1;
                for (int j = l[top]; j <= r[top]; j++) {
                    from[j] = top;
                }
                isSuccess = 1;
                break;
            }
        }
        if (isSuccess) {
            write(top, '\n');
        } else {
            puts("NULL");
        }
        #ifdef debugTAG
        for (int i = 1; i <= size; i++) {
            write(from[i], 32);
        }
        putchar(10);
        #endif
    }

    void erase(int x)
    {
        if (x <= 0 || x > top || l[x] == -1) {
            puts("ILLEGAL_ERASE_ARGUMENT");
            return;
        }
        for (int j = l[x]; j <= r[x]; j++) {
            from[j] = 0;
        }
        l[x] = -1;
        #ifdef debugTAG
        for (int i = 1; i <= size; i++) {
            write(from[i], 32);
        }
        putchar(10);
        #endif
    }

    void defragment()
    {
        priority_queue<LR> pq;
        for (int i = 1; i <= top; i++) {
            if (l[i] != -1) {
                pq.push(LR(l[i], r[i], i));
            }
        }
        memset(from, 0, sizeof(from));
        int Rwall = 0;
        while (!pq.empty()) {
            LR f = pq.top();
            pq.pop();
            l[f.idx] = Rwall + 1;
            r[f.idx] = Rwall + f.length;
            Rwall += f.length;
            for (int i = l[f.idx]; i <= r[f.idx]; i++) {
                from[i] = f.idx;
            }
        }
        #ifdef debugTAG
        for (int i = 1; i <= size; i++) {
            write(from[i], 32);
        }
        putchar(10);
        #endif
    }
} BerlOS;

int t, m;
char buf[20], n;

signed main()
{
    t = read<int>();
    m = read<int>();
    BerlOS.size = m;
    while (t--) {
        scanf("%s", buf);
        switch(buf[0]) {
            case 'a' :
                BerlOS.alloc(read<int>());
                break;
            case 'e' :
                BerlOS.erase(read<int>());
                break;
            case 'd' :
                BerlOS.defragment();
                break;
        }
    }
    return 0;
}