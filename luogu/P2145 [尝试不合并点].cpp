/*************************************
 * problem:      P2145 [JSOI2007]祖码.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-17.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define puts_return(x) { puts(x); return 0; }
#define write_return(x) { write(x); return 0; }

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int a[501];
int n, f[501][501];

int countR(int start, int limit)
{
    int cnt(1), p(start + 1);
    while (p <= limit && a[start] == a[p]) cnt++, p++;
    return cnt;
}

int countL(int end, int limit)
{
    int cnt(1), p(end - 1);
    while (p >= limit && a[end] == a[p]) cnt++, p--;
    return cnt;
}

int main()
{
    int n = read<int>();
    for (int i = 1; i <= n; i++) {
        a[i] = read<int>();
    }
    memset(f, 0x3f, sizeof(f));
    for (int i = 1; i <= n; i++) {
        f[i][i] = 2;
    }
    for (int i = 1; i < n; i++) {
        if (a[i] == a[i + 1]) f[i][i + 1] = 1;
        else f[i][i + 1] = 2;
    }
    for (int l = 3; l <= n; l++) {
        for (int i = 1, j = l; j <= n; i++, j++) {
            for (int k = i; k < j; k++) {
                if (a[k] != a[k + 1]) f[i][j] = min(f[i][j], f[i][k] + f[k + 1][j]);
                else f[i][j] = min(f[i][j], f[i][k] + f[k + 1][j] + 3);
            }
            if (a[i] == a[j]) {
                int ml = countR(i, j), mr = countL(j, i), extraNeed = (ml + mr == 2);
                if (ml >= l) {
                    f[i][j] = 1;
                    continue;
                }
                f[i][j] = min(f[i][j], f[i + ml][j - mr] + extraNeed);
                if (ml == 1 || mr == 1) for (int k = i + ml + 1; k <= j - mr - 1; k++) {
                    if (a[i] == a[k] && (countL(k, i) + countR(k, j) == 2)) {
                        f[i][j] = min(f[i][j], f[i + ml][k - 1] + f[k + 1][j - mr]);
                        // for (int m = k + 2; m <= j - 2; m++) {
                        //     if (a[i].id == a[m].id && a[i].cnt == 1 && a[j].cnt == 1 && a[m].cnt == 1)
                        //         f[i][j] = min(f[i][j], f[i + 1][k - 1] + f[k + 1][m - 1] + f[m + 1][j - 1]);
                        // }
                    }
                }
            }
        }
    }
    write(f[1][n], 10);
    return 0;
}

/*
12
1 1 2 2 3 3 2 2 2 4 4 2
#output# 3

7 
5 4 4 5 7 7 5
#output# 2

5
1 2 1 3 1
#output# 4

7
1 2 1 3 1 4 1
#output# 6

12 
1 1 2 2 3 3 2 2 2 4 4 2
#output# 3

17 
0 1 0 0 1 1 0 1 0 1 1 0 0 1 1 0 0
#output# 2
*/