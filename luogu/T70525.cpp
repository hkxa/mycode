/*************************************
 * problem:      T70525 [LnOI2019SP]快速多项式变换(FPT).
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-03-10.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

long long m, fm;
long long a[107];

int main()
{
	m = read<long long>();
	fm = read<long long>();
	int p = 1;
	a[p] = fm;
	while (a[p] >= m) {
		a[p + 1] = a[p] / m;
		a[p] %= m;
		p++;
	}
	int n = p;
	write(n), putchar(10);
	for (int i = 1; i <= n; i++) {
		write(a[i]), putchar(32);
	} 
	return 0;
} 
