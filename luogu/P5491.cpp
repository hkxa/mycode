/*************************************
 * @problem:      【模板】二次剩余.
 * @author:       brealid.
 * @time:         2021-01-08.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

// #define USE_FREAD  // 使用 fread  读入，去注释符号
// #define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

namespace QuadraticResidue {
    typedef long long qr_int64;
    struct qr_complex {
        qr_int64 real, imag;
        qr_complex(qr_int64 R = 0, qr_int64 I = 0) : real(R), imag(I) {}
    };
    qr_complex qrc_mul(qr_complex a, qr_complex b, qr_int64 sqrI, qr_int64 p) {
        return qr_complex((a.real * b.real + a.imag * b.imag % p * sqrI) % p, 
                          (a.real * b.imag + a.imag * b.real) % p);
    }
    /// @brief Quick Power a^n in Mod-Meaning(mod p)
    qr_int64 qpow(qr_int64 a, qr_int64 n, qr_int64 p) {
        qr_int64 ret(1);
        while (n) {
            if (n & 1) ret = ret * a % p;
            a = a * a % p;
            n >>= 1;
        }
        return ret;
    }
    /// @brief Quick Power a^n in Mod-Meaning-Complex
    qr_complex qpow(qr_complex a, qr_int64 n, qr_int64 sqrI, qr_int64 p) {
        qr_complex ret(1);
        while (n) {
            if (n & 1) ret = qrc_mul(ret, a, sqrI, p);
            a = qrc_mul(a, a, sqrI, p);
            n >>= 1;
        }
        return ret;
    }
    /**
     * @brief Quadratic Residue, or Square Root in Mod-Meaning
     * @param a for equation x^2=a (mod p)
     * @param p mod num(need to be odd-prime)
     * @return -1 if no solution, 0 if one solution, and x if x^2 = a && (p-x)^2 = a (mod p) && x < p-x
     */
    qr_int64 sqrt(qr_int64 n, qr_int64 p) {
        if (n == 0) return 0;
        if (qpow(n, (p - 1) >> 1, p) == p - 1) return -1;
        qr_int64 a = rand();
        while (qpow((a * a + p - n) % p, (p - 1) >> 1, p) == 1) a = rand();
        qr_int64 res = qpow(qr_complex(a, 1), (p + 1) >> 1, (a * a + p - n) % p, p).real;
        return std::min(res, p - res);
    }
}

signed main() {
    int T = kin.get<int>(), a, p;
    while (T--) {
        kin >> a >> p;
        int res = QuadraticResidue::sqrt(a, p);
        if (!res) kout << "0\n";
        else if (res == -1) kout << "Hola!\n";
        else kout << res << ' ' << p - res << '\n';
    }
}