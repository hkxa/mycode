/*************************************
 * @problem:      [ICPC-Beijing 2006]狼抓兔子.
 * @author:       brealid.
 * @time:         2021-02-03.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 2e6 + 7;

int n, m, NodeCount;
int S, T;

int id(int x, int y, int part) {
    if (x < 1 || y >= m) return S;
    if (x >= n || y < 1) return T;
    if (part) return (x - 1) * (m - 1) + y;
    else return (x - 1) * (m - 1) + y + NodeCount;
}

struct edge {
    int to, nxt_edge;
    int fee;
} e[N * 3 + 5];
int head[N + 5], ecnt = 1;
int dis[N + 5];
bool inq[N + 5];

inline void add_edge(const int &from, const int &to, const int &fee) {
    e[++ecnt].to = to;
    e[ecnt].fee = fee;
    e[ecnt].nxt_edge = head[from];
    head[from] = ecnt;
    // undirected
    e[++ecnt].to = from;
    e[ecnt].fee = fee;
    e[ecnt].nxt_edge = head[to];
    head[to] = ecnt;
}

inline int spfa() {
    memset(dis, 0x3f, sizeof(dis));
    memset(inq, 0, sizeof(inq));
    dis[S] = 0;
    std::queue<int> q;
    q.push(S);
    inq[S] = true;
    while (!q.empty()) {
        int u = q.front(); q.pop();
        inq[u] = false;
        for (int i = head[u]; i; i = e[i].nxt_edge)
            if (dis[e[i].to] > dis[u] + e[i].fee) {
                dis[e[i].to] = dis[u] + e[i].fee;
                if (!inq[e[i].to]) {
                    q.push(e[i].to);
                    inq[e[i].to] = true;
                }
            }
    }
    return dis[T];
}

bool LD = true, RU = false;

signed main() {
    kin >> n >> m;
    NodeCount = (n - 1) * (m - 1);
    S = NodeCount * 2 + 1;
    T = S + 1;
    for (int i = 1; i <= n; ++i)
        for (int j = 1; j < m; ++j)
            add_edge(id(i - 1, j, LD), id(i, j, RU), kin.get<int>());
    for (int i = 1; i < n; ++i)
        for (int j = 1; j <= m; ++j)
            add_edge(id(i, j, LD), id(i, j - 1, RU), kin.get<int>());
    for (int i = 1; i < n; ++i)
        for (int j = 1; j < m; ++j)
            add_edge(id(i, j, RU), id(i, j, LD), kin.get<int>());
    kout << spfa() << '\n';
    return 0;
}