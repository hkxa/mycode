//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      P3350 [ZJOI2016]旅行者.
 * @user_name:    brealid.
 * @time:         2020-06-09.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#pragma GCC optimize(2)
#pragma GCC optimize(3)
#pragma GCC optimize("inline")
#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 988244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64
const int N = 100007;
int n, m, Q;
int cost[N][4];
int dis[N];
const int _dX[4] = {0, 1, 0, -1}, _dY[4] = {1, 0, -1, 0};

struct point {
    int x, y;
    point() {}
    point(int _X, int _Y) : x(_X), y(_Y) {}
    inline operator int() const { return x * m + y; }
    inline point move(int dir) const { return point(x + _dX[dir], y + _dY[dir]); }
};

struct Question {
    point u, v;
    int id;
} q[N];
int ans[N];

struct DijstraNode {
    point u; int dist;
    DijstraNode() {}
    DijstraNode(point U, int Dist) : u(U), dist(Dist) {}
    inline bool isLatest() const { return dist == dis[u]; }
    inline bool operator < (const DijstraNode &b) const { return dist > b.dist; }
};

struct interval {
    int l, r;
    struct iterator {
        int x;
        iterator(); 
        iterator(int x);
        iterator operator++();
        int operator*();
        bool operator != (iterator ot);
    };
    interval() : l(0), r(-1) {}
    interval(int L, int R) : l(L), r(R) {}
    inline bool exist() { return l <= r; }
    inline bool include(int p) { return p >= l && p <= r; }
    inline int len() { return r - l + 1; }
    inline int mid() { return (l + r) >> 1; }
    inline iterator begin() { return iterator(l); }
    inline iterator end() { return iterator(r + 1); }
    inline interval left() { return interval(l, mid() - 1); }
    inline interval right() { return interval(mid() + 1, r); }
};

interval::iterator::iterator() {}
interval::iterator::iterator(int X) : x(X) {}
interval::iterator interval::iterator::operator ++ () { x++; return *this; }
int interval::iterator::operator * () { return x; }
bool interval::iterator::operator != (const interval::iterator ot) { return x != ot.x; }

struct square {
    struct iterator {
        point x;
        bool direction;
        iterator(); 
        iterator(point X, bool dir);
        iterator operator++();
        point operator*();
        bool operator != (iterator ot);
    };
    interval x, y;
    square() : x(), y() {}
    square(interval _X, interval _Y) : x(_X), y(_Y) {}
    square(int xl, int xr, int yl, int yr) : x(xl, xr), y(yl, yr) {}
    inline bool exist() { return x.exist() && y.exist(); }
    inline bool include(point a) { return x.include(a.x) && y.include(a.y); }
    inline bool direction() { return x.len() < y.len(); }
    inline iterator mid_begin() { 
        return direction() ? iterator(point(x.l, y.mid()), 1) : iterator(point(x.mid(), y.l), 0); 
    }
    inline iterator mid_end() { 
        return direction() ? iterator(point(x.r + 1, y.mid()), 1) : iterator(point(x.mid(), y.r + 1), 0); 
    }
    inline square left() {
        return direction() ? square(x, y.left()) : square(x.left(), y);
    } 
    inline square right() {
        return direction() ? square(x, y.right()) : square(x.right(), y);
    }
};

square::iterator::iterator() {}
square::iterator::iterator(point X, bool dir) : x(X), direction(dir) {}
square::iterator square::iterator::operator ++ () { direction ? x.x++ : x.y++; return *this; }
point square::iterator::operator * () { return x; }
bool square::iterator::operator != (const square::iterator ot) {
    return direction != ot.direction || x != ot.x;
}

void ResetArray(int *arr, square range, const int val) {
    point it;
    for (it.x = range.x.l; it.x <= range.x.r; it.x++)
        for (it.y = range.y.l; it.y <= range.y.r; it.y++)
            arr[it] = val;
}

void Dij(point s, square range) {
    static priority_queue<DijstraNode> q;
    ResetArray(dis, range, 0x3fffffff);
    dis[s] = 0;
    q.push(DijstraNode(s, 0));
    point u, v;
    while (!q.empty()) {
        while (!q.empty() && !q.top().isLatest()) q.pop();
        if (q.empty()) break;
        u = q.top().u; 
        q.pop();
        for (int i = 0; i < 4; i++) {
            v = u.move(i);
            if (!range.include(v)) continue;
            if (dis[v] > dis[u] + cost[u][i]) {
                dis[v] = dis[u] + cost[u][i];
                q.push(DijstraNode(v, dis[v]));
            }
        }
    }
}

void solve(interval qRange, square range) {
    static queue<Question> lef, rig;
    if (!qRange.exist() || !range.exist()) return;
    for (square::iterator it1 = range.mid_begin(); it1 != range.mid_end(); ++it1) {
        Dij(*it1, range);
        for (interval::iterator it2 = qRange.begin(); it2 != qRange.end(); ++it2) {
            if (ans[q[*it2].id] > dis[q[*it2].u] + dis[q[*it2].v]) {
                ans[q[*it2].id] = dis[q[*it2].u] + dis[q[*it2].v];
            }
        }
    }
    for (interval::iterator it = qRange.begin(); it != qRange.end(); ++it) {
        if (range.left().include(q[*it].u) && range.left().include(q[*it].v)) {
            lef.push(q[*it]);
        } else if (range.right().include(q[*it].u) && range.right().include(q[*it].v)) {
            rig.push(q[*it]);
        }
    }
    int u1 = qRange.l, u2 = qRange.l, u3;
    while (!lef.empty()) {
        q[u2++] = lef.front();
        lef.pop();
    }
    u3 = u2;
    while (!rig.empty()) {
        q[u3++] = rig.front();
        rig.pop();
    }
    solve(interval(u1, u2 - 1), range.left());
    solve(interval(u2, u3 - 1), range.right());
}

int main() {
    n = read<int>();
    m = read<int>();
    for (int i = 1; i <= n; i++) 
        for (int j = 1; j < m; j++)
            cost[point(i, j)][0] = cost[point(i, j + 1)][2] = read<int>();
    for (int i = 1; i < n; i++) 
        for (int j = 1; j <= m; j++)
            cost[point(i, j)][1] = cost[point(i + 1, j)][3] = read<int>();
    Q = read<int>();
    for (int i = 1; i <= Q; i++) {
        // if (!(i & 255)) printf("reading Q %d/%d...\n", i, Q);
        q[i].u.x = read<int>();
        q[i].u.y = read<int>();
        q[i].v.x = read<int>();
        q[i].v.y = read<int>();
        q[i].id = i;
        ans[i] = 0x3fffffff;
    }
    solve(interval(1, Q), square(1, n, 1, m));
    for (int i = 1; i <= Q; i++)
        write(ans[i], 10);
    return 0;
}

// Create File Date : 2020-06-09