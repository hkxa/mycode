/*************************************
 * @problem:      Two Paths.
 * @user_name:    brealid/hkxadpall/zhaoyi20/j(o)moo/littleTortoise.
 * @time:         2020-09-15.
 * @language:     C++.
 * @fastio_ver:   20200827.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        // simple io flags
        ignore_int = 1 << 0,    // input
        char_enter = 1 << 1,    // output
        flush_stdout = 1 << 2,  // output
        flush_stderr = 1 << 3,  // output
        // combination io flags
        endline = char_enter | flush_stdout // output
    };
    enum number_type_flags {
        // simple io flags
        output_double_stable = 1 << 0,  // output
        output_double_faster = 1 << 1   // output
        // combination io flags
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f & ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore character set : ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (double &lf) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            lf = endch & 15;
            while (isdigit(endch = getchar())) lf = lf * 10 + (endch & 15);
            if (endch == '.') {
                double digit = 0.1;
                while (isdigit(endch = getchar())) {
                    lf += (endch & 15) * digit;
                    digit *= 0.1;
                }
            }
            if (flag) lf = -lf;
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            Int d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return d;
        }
    } read;

    struct Writer {
        Writer& operator << (io_flags f) {
            if (f & char_enter) putchar(10);
            if (f & flush_stdout) fflush(stdout);
            if (f & flush_stderr) fflush(stderr);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
        inline void operator () (double val, int eps_digit = 6, char endch = '\0', number_type_flags flg = output_double_faster) {
            if (flg & output_double_stable) {
                static char output_format[10];
                sprintf(output_format, "%%.%dlf", eps_digit);
                printf(output_format, val);
            } else if (flg & output_double_faster) {
                if (val < 0) {
                    putchar('-');
                    val = -val;
                }
                (*this) << (int64)val;
                val -= (int64)val;
                putchar('.');
                double eps_number = 0.5;
                for (int i = 1; i <= eps_digit; i++) eps_number /= 10;
                val += eps_number;
                while (eps_digit--) {
                    val *= 10;
                    putchar((int)val | '0');
                    val -= (int)val;
                }
            } else {
                (*this) << "1.#ERROR_NOT_IDENTIFIED_FLAG";
            }
            if (endch) putchar(endch);
        }
    } write;
}
using namespace Fastio;
namespace File_IO {
    void init_IO() {
        freopen("Two Paths.in", "r", stdin);
        freopen("Two Paths.out", "w", stdout);
    }
}

// #define int int64

const int N = 200 + 7;

int n;
vector<int> G[N];
bool mark[N];

bool get_nodes(int fr, int to, vector<int> &save_place, int FA = 0) {
    if (fr == to) {
        save_place.push_back(fr);
        return true;
    }
    for (size_t i = 0; i < G[fr].size(); i++) {
        int v = G[fr][i];
        if (v == FA) continue;
        if (get_nodes(v, to, save_place, fr)) {
            save_place.push_back(fr);
            return true;
        }
    }
    return false;
}

pair<int, int> dfs(int u, int FA = 0) {
    pair<int, int> res(0, 0);
    int mx2 = 0;
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i];
        if (v != FA && !mark[v]) {
            pair<int, int> r = dfs(v, u);
            ++r.second;
            res.first = max(res.first, r.first);
            if (r.second > res.second) {
                mx2 = res.second;
                res.second = r.second;
            } else if (r.second > mx2) {
                mx2 = r.second;
            }
        }
    }
    res.first = max(res.first, res.second + mx2);
    return res;
}

signed main() {
    // File_IO::init_IO();
    read >> n;
    for (int i = 1, u, v; i < n; i++) {
        read >> u >> v;
        G[u].push_back(v);
        G[v].push_back(u);
    } 
    int64 ans = 0;
    for (int i = 1; i <= n; i++) 
        for (int j = 1; j <= n; j++) {
            if (i == j) continue;
            vector<int> Nodes;
            get_nodes(i, j, Nodes);
            memset(mark, 0, sizeof(mark));
            for (size_t i = 0; i < Nodes.size(); i++)
                mark[Nodes[i]] = true;
            int mk = 0;
            for (size_t i = 0; i < Nodes.size(); i++) {
                int u = Nodes[i];
                for (size_t j = 0; j < G[u].size(); j++) {
                    int v = G[u][j];
                    if (!mark[v]) mk = max(mk, dfs(v).first);
                }
            }
            ans = max(ans, (int64)(Nodes.size() - 1) * mk);
        }
    write << ans << '\n';
    return 0;
}