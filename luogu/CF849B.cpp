//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Tell Your World.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-06.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

namespace against_cpp11 {
    const int N = 1000 + 7;
    const double eps = 1e-6;
    int n, y[N];
    // int gcd(int m, int n) {
    //     return m ? gcd(n % m, m) : n;
    // }
    // int64 lineK(int dx, int dy) {
    //     if (!dy) return 0;
    //     int G = gcd(dx, abs(dy));
    //     dx /= G; dy /= G;
    //     if (dy < 0) return -((int64)dx * P - dy);
    //     else return ((int64)dx * P + dy);
    // }
    signed main() {
        read >> n;
        for (int i = 1; i <= n; i++) read >> y[i];
        for (int i = n; i >= 1; i--) y[i] -= y[1];
        bool fail;
        for (int i = 2; i <= n; i++) {
            int cnt_ok = 0;
            double Line_K = (double)(y[i] - y[1]) / (i - 1);
            int x0 = 0, y0 = 0;
            fail = false;
            for (int j = 2; j <= n && !fail; j++) {
                if (abs(Line_K - (double)(y[j] - y[1]) / (j - 1)) <= eps) 
                    cnt_ok++;
                else {
                    if (!x0) {
                        x0 = j;
                        y0 = y[j];
                    } else {
                        if (abs(Line_K - (double)(y[j] - y0) / (j - x0)) > eps) 
                            fail = true;
                    }
                }
            }
            if (cnt_ok < n - 1 && !fail) {
                puts("Yes");
                return 0;
            }
        }
        fail = (y[3] - y[2] == y[2] - y[1]);
        for (int i = 4; i <= n && !fail; i++)
            if (y[i] - y[i - 1] != y[i - 1] - y[i - 2]) 
                fail = true;
        if (!fail) {
            puts("Yes");
            return 0;
        }
        puts("No");
        return 0;
    }
}

signed main() { return against_cpp11::main(); }