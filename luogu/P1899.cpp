/*************************************
 * @problem:      P1899 魔法物品.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-02-01.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, p0;
int start, allp1, allp2, duce = 0x3f3f3f3f;
int p1[1007], p2[1007], m = 0;
int f[10000005];

int main()
{
    n = read<int>();
    p0 = read<int>();
    for (int i = 1, t, continueFor; i <= n; i++) {
        continueFor = 0;
        char c = getchar();
        while (!isdigit(c)) c = getchar();
        t = c & 15;
        while (isdigit(c = getchar())) 
            t = (t << 3) + (t << 1) + (c & 15);
        allp1 += t;
        while (!isdigit(c)) {
            if (c == '\n') {
                start += t;
                allp2 += t;
                continueFor = true;
                break;
            }
            c = getchar();
        }
        if (continueFor) continue;
        p1[++m] = t;
        t = c & 15;
        while (isdigit(c = getchar())) 
            t = (t << 3) + (t << 1) + (c & 15);
        p2[m] = t;
        if (p2[m] - p1[m] < p0) {
            allp2 += p1[m];
            start += p1[m--];
        } else allp2 += t - p0;
        // printf("%d : %d %d\n", i, p1[m], p2[m]);
    }
    // printf("%d %d %d\n", start, allp1, allp2);
    if (start < p0) {
        memset(f, 0x3f, sizeof(f));
        f[0] = 0;
        for (int i = 1, lose; i <= m; i++) {
            lose = p2[i] - p1[i] - p0;
            for (int v = allp1 - start; v >= p1[i]; v--) {
                f[v] = min(f[v], f[v - p1[i]] + lose);
            }
        }
        for (int v = p0 - start; v <= allp1 - start; v++) {
            // printf("f[%d] = %d.\n", v, f[v]);
            duce = min(duce, f[v]);
        }
        write(max(allp2 - duce, allp1), 10);
    } else write(allp2, 10);
    return 0;
}