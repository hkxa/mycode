/*************************************
 * problem:      P4346 [CERC2015]ASCII Addition.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-05-01.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

struct ASCII_digit {
    char data[1 + 7 + 1][1 + 5 + 1];

    char* operator [] (int id)
    {
        return data[id];
    }

    bool operator == (ASCII_digit with) const
    {
        for (int i = 1; i <= 7; i++) {
            for (int j = 1; j <= 5; j++) {
                if (data[i][j] != with[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }
};

ASCII_digit cstd[11] = {
    { /* 0 */
        "      ", 
        " xxxxx", 
        " x...x", 
        " x...x", 
        " x...x", 
        " x...x", 
        " x...x", 
        " xxxxx", 
    }, 
    { /* 1 */
        "      ", 
        " ....x", 
        " ....x", 
        " ....x", 
        " ....x", 
        " ....x", 
        " ....x", 
        " ....x", 
    }, 
    { /* 2 */
        "      ", 
        " xxxxx", 
        " ....x", 
        " ....x", 
        " xxxxx", 
        " x....", 
        " x....", 
        " xxxxx", 
    }, 
    { /* 3 */
        "      ", 
        " xxxxx", 
        " ....x", 
        " ....x", 
        " xxxxx", 
        " ....x", 
        " ....x", 
        " xxxxx", 
    }, 
    { /* 4 */
        "      ", 
        " x...x", 
        " x...x", 
        " x...x", 
        " xxxxx", 
        " ....x", 
        " ....x", 
        " ....x", 
    }, 
    { /* 5 */
        "      ", 
        " xxxxx", 
        " x....", 
        " x....", 
        " xxxxx", 
        " ....x", 
        " ....x", 
        " xxxxx", 
    }, 
    { /* 6 */
        "      ", 
        " xxxxx", 
        " x....", 
        " x....", 
        " xxxxx", 
        " x...x", 
        " x...x", 
        " xxxxx", 
    }, 
    { /* 7 */
        "      ", 
        " xxxxx", 
        " ....x", 
        " ....x", 
        " ....x", 
        " ....x", 
        " ....x", 
        " ....x", 
    }, 
    { /* 8 */
        "      ", 
        " xxxxx", 
        " x...x", 
        " x...x", 
        " xxxxx", 
        " x...x", 
        " x...x", 
        " xxxxx", 
    }, 
    { /* 9 */
        "      ", 
        " xxxxx", 
        " x...x", 
        " x...x", 
        " xxxxx", 
        " ....x", 
        " ....x", 
        " xxxxx", 
    }, 
    { /* + */
        "      ", 
        " .....", 
        " ..x..", 
        " ..x..", 
        " xxxxx", 
        " ..x..", 
        " ..x..", 
        " .....", 
    }
};

int trans(ASCII_digit ad)
{
    for (int i = 0; i < 11; i++) {
        if (ad == cstd[i]) return i;
    }
    return EOF;
}

int n;
int a[2] = {0};
char ch[8][151] = {0};

ASCII_digit spilt(int id)
{
    ASCII_digit ret;
    for (int i = 1; i <= 7; i++) {
        for (int j = 1; j <= 5; j++) {
            ret[i][j] = ch[i][id * 6 + j];
        }
    }
    return ret;
}

void output_ASCIImatrix(int num)
{
    int a[11], len = 0;
    while (num) {
        a[++len] = num % 10;
        num /= 10;
    }
    for (int i = 1; i <= 7; i++) {
        for (int j = len; j >= 1; j--) {
            printf("%s%c", cstd[a[j]][i] + 1, j != 1 ? '.' : '\n');
        }
        // printf("\n");
    }
}

int main()
{
    for (int i = 1; i <= 7; i++) {
        scanf("%s", ch[i] + 1);
    }
    n = (strlen(ch[1] + 1) + 1) / 6;
    int p = 0;
    for (int i = 0; i < n; i++) {
        int t = trans(spilt(i));
        // printf("nRet = %d.\n", t);
        if (t == 10) p++;
        else a[p] = (((a[p] << 2) + a[p]) << 1) + t;
    }
    int ans = a[0] + a[1];
    // printf("debug : %d + %d = %d.\n", a[0], a[1], ans);
    output_ASCIImatrix(ans);
    return 0;
}