/*************************************
 * @problem:      CF1842C.
 * @author:       brealid.
 * @time:         2023-06-24.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

// #define USE_FREAD  // 使用 fread  读入，去注释符号
// #define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

#define int long long

const int N = 2e5 + 7;
int n, k, A;
struct point {
    int x, c;
};
vector<point> p[N];

namespace tr {
    int t[N << 2], lazy_add[N << 2];
    void build(int u, int l, int r) {
        t[u] = 2e9;
        if (l == r) return;
        int mid = (l + r) >> 1;
        build(u << 1, l, mid);
        build(u << 1 | 1, mid + 1, r);
    }

    void range_add(int u, int l, int r, int ql, int qr, int val) {
        if (ql <= l && r <= qr) {
            t[u] += val;
            lazy_add[u] += val;
            return;
        }
        int mid = (l + r) >> 1;
        if (ql <= mid) range_add(u << 1, l, mid, ql, qr, val);
        if (qr > mid) range_add(u << 1 | 1, mid + 1, r, ql, qr, val);
        t[u] = min(t[u << 1], t[u << 1 | 1]) + lazy_add[u];
    }

    void update_point(int u, int l, int r, int p, int val) {
        if (l == r) {
            t[u] = val;
            return;
        }
        int mid = (l + r) >> 1;
        if (p <= mid) update_point(u << 1, l, mid, p, val);
        else update_point(u << 1 | 1, mid + 1, r, p, val);
        t[u] = min(t[u << 1], t[u << 1 | 1]) + lazy_add[u];
    }

    int query_point(int u, int l, int r, int p) {
        if (l == r) return t[u];
        int mid = (l + r) >> 1;
        if (p <= mid) return lazy_add[u] + query_point(u << 1, l, mid, p);
        else return lazy_add[u] + query_point(u << 1 | 1, mid + 1, r, p);
    }
}

signed main() {
    kin >> n >> k >> A;
    point p_cur;
    for (int i = 1, y; i <= n; ++i) {
        kin >> p_cur.x >> y >> p_cur.c;
        p[y].push_back(p_cur);
    }
    tr::build(1, 0, k);
    tr::update_point(1, 0, k, k, k * A);
    int cur_ans = 0;
    for (int y = k - 1; y >= 0; --y) {
        // int this_ctotal = 0;
        // if (y == 1) kout << tr::query_point(1, 0, k, 3) << '\n';
        tr::update_point(1, 0, k, y, cur_ans + y * A);
        for (auto &p_cur : p[y]) //{
            if (y <= k - p_cur.x - 1)
                tr::range_add(1, 0, k, y, k - p_cur.x - 1, p_cur.c);
            // kout << "range_add: " << y << " " << k - p_cur.x - 1 << " " << p_cur.c << '\n';
        //     this_ctotal += p_cur.c;
        // }
        cur_ans = tr::t[1] - y * A;
        // if (y == 1) kout << tr::query_point(1, 0, k, 3) << '\n';
        // kout << "y: " << y << " | ans: " << cur_ans << '\n';
    }
    kout << cur_ans << '\n';
    return 0;
}