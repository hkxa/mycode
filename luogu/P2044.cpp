/*************************************
 * problem:      P2044 [NOI2012]随机数生成器.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-06-16.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

#define passing() cerr << "passing line [" << __LINE__ << "]." << endl
#define debug(x) cerr << #x << " = " << x << endl;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

unsigned long long m, a, c, seed, n, g;

unsigned long long mul(unsigned long long a, unsigned long long b)
{
    unsigned long long res = 0;
    while (b) {
        if (b & 1) {
            res = res + a;
            if (res >= m) res -= m;
        }
        a <<= 1;
        if (a >= m) a -= m;
        b >>= 1;
    }
    return res;
}

unsigned long long ksm(unsigned long long b, unsigned long long p)
{
    long long res = 1;
    while (p) {
        if (p & 1) res = mul(res, b);
        b = mul(b, b);
        p >>= 1;
    }
    return res;
} 

unsigned long long S(unsigned long long n)
{
    // debug(n);
    if (n == 1) return 1;
    if (n & 1) return (ksm(a, n - 1) + S(n - 1)) % m;
    else return mul(S(n >> 1), (ksm(a, n >> 1) + 1) % m);
}

struct Answer_sta {
    unsigned long long part1, part2;

    void solve1()
    {
        // passing();
        part1 = mul(ksm(a, n), seed);
        // passing();
    }

    void solve2()
    {
        // passing();
        part2 = mul(c, S(n));
        // passing();
    }

    void sta_output()
    {
        /* 
        printf("debug : \n"
               "part 1 : %llu\n"
               "    ksm(a = %llu, n = %llu) = %llu\n"
               "    seed = %llu\n"
               "part 2 : %llu\n"
               "    c = %llu\n"
               "    S(n = %llu) = %llu\n", 
               part1, a, n, ksm(a, n), seed, part2, c, n, S(n));
        */
        // passing();
        write((part1 + part2) % m % g, 10);
    }
} ans;

int main()
{
    m = read<unsigned long long>();
    // m = read<unsigned long long>() + 10000003;
    a = read<unsigned long long>() % m;
    c = read<unsigned long long>() % m;
    seed = read<unsigned long long>() % m;
    n = read<unsigned long long>();
    // n = 5;
    g = read<unsigned long long>();
    // g = read<unsigned long long>() + 10000003;
    // printf("%llu, %llu, %llu, %llu, %llu\n", S(1), S(2), S(3), S(4), S(5));
    ans.solve1();
    ans.solve2();
    ans.sta_output();
    /*
    I 333 384 4998 4773 733 2883
    O 3
    */
    return 0;
}