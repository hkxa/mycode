/*************************************
 * @problem:      汽车加油行驶问题.
 * @author:       brealid.
 * @time:         2021-07-21.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

namespace mcmf {
    typedef long long value_type;
    const int Net_Node = 110000/*最大节点数*/, Net_Edge = 1320000/*最大边数(无需开两倍)*/;
    const value_type inf = (value_type)0x3f3f3f3f3f3f3f3f; // 当 value_type 为 int 的时候，自动类型强转为 0x3f3f3f3f
    struct edge {
        int to, nxt_edge;
        value_type flow, fee;
    } e[Net_Edge * 2 + 5];
    int head[Net_Node + 5], ecnt = 1;
    int node_total, st, ed;
    value_type flow[Net_Node + 5], dis[Net_Node + 5];
    int from[Net_Node + 5];
    bool inq[Net_Node + 5];
    // 清零，此函数适用于多组数据
    inline void clear() {
        memset(head, 0, sizeof(head));
        ecnt = 1;
        st = ed = 0;
    }
    // 添加边（正向边和反向边均会自动添加）
    inline void add_edge(const int &from, const int &to, const value_type &flow, const value_type &fee) {
        // Add "positive going edge"
        e[++ecnt].to = to;
        e[ecnt].flow = flow;
        e[ecnt].fee = fee;
        e[ecnt].nxt_edge = head[from];
        head[from] = ecnt;
        // Add "reversed going edge"
        e[++ecnt].to = from;
        e[ecnt].flow = 0;
        e[ecnt].fee = -fee; // 反边费用相反
        e[ecnt].nxt_edge = head[to];
        head[to] = ecnt;
    }
    // 将所有流还原(即: 还原残量网络至原图)
    inline void restore_flow() {
        for (int i = 2; i <= ecnt; i += 2) {
            e[i].flow += e[i | 1].flow;
            e[i | 1].flow = 0;
        }
    }
    // SPFA 算法
    inline bool spfa() {
        memset(dis, 0x3f, sizeof(value_type) * (node_total + 1));
        memset(flow, 0, sizeof(value_type) * (node_total + 1));
        memset(inq, 0, sizeof(bool) * (node_total + 1));
        from[st] = -1;
        dis[st] = 0;
        flow[st] = inf;
        std::queue<int> q;
        q.push(st);
        inq[st] = true;
        while (!q.empty()) {
            int u = q.front(); q.pop();
            inq[u] = false;
            for (int i = head[u]; i; i = e[i].nxt_edge)
                if (dis[e[i].to] > dis[u] + e[i].fee && e[i].flow) {
                    dis[e[i].to] = dis[u] + e[i].fee;
                    flow[e[i].to] = std::min(flow[u], e[i].flow);
                    from[e[i].to] = i;
                    if (!inq[e[i].to]) {
                        q.push(e[i].to);
                        inq[e[i].to] = true;
                    }
                }
        }
        return dis[ed] != inf;
    }
    // 记录 MCMF(Min Cost Max Flow) 的结果
    struct MCMF_result {
        value_type max_flow, min_cost;
        MCMF_result(value_type MF, value_type MC) : max_flow(MF), min_cost(MC) {}
    };
    // MCMF(Edmond_Karp + SPFA) 总工作函数，需要提供节点数（偏大影响时间），起始点（默认 1），结束点（默认 node_count）
    MCMF_result work(int node_count, int start_node = 1, int finish_node = -1) {
        node_total = node_count;
        st = start_node;
        ed = ~finish_node ? finish_node : node_count;
        value_type max_flow = 0, min_cost = 0;
        while (spfa()) {
            value_type now_flow = flow[ed];
            max_flow += now_flow;
            for (int i = from[ed]; ~i; i = from[e[i ^ 1].to]) {
                e[i].flow -= now_flow;
                e[i ^ 1].flow += now_flow;
                min_cost += e[i].fee * now_flow;
            }
        }
        return MCMF_result(max_flow, min_cost);
    }
}

int n, k, a, b, c, oil[103][103];
#define id(i, j, fl) ((fl) * (n * n) + ((i) - 1) * n + (j))

signed main() {
    kin >> n >> k >> a >> b >> c;
    for (int i = 1; i <= n; ++i)
        for (int j = 1; j <= n; ++j)
            kin >> oil[i][j];
    for (int now = 1; now <= k; ++now)
        for (int i = 1; i <= n; ++i)
            for (int j = 1; j <= n; ++j) {
                if (i != n) {
                    if (oil[i + 1][j]) {
                        mcmf::add_edge(id(i, j, now), id(i + 1, j, k), 1, a);
                    } else {
                        mcmf::add_edge(id(i, j, now), id(i + 1, j, k), 1, a + c);
                        mcmf::add_edge(id(i, j, now), id(i + 1, j, now - 1), k, 0);
                    }
                }
                if (i != 1) {
                    if (oil[i - 1][j]) {
                        mcmf::add_edge(id(i, j, now), id(i - 1, j, k), 1, a + b);
                    } else {
                        mcmf::add_edge(id(i, j, now), id(i - 1, j, k), 1, a + b + c);
                        mcmf::add_edge(id(i, j, now), id(i - 1, j, now - 1), k, b);
                    }
                }
                if (j != n) {
                    if (oil[i][j + 1]) {
                        mcmf::add_edge(id(i, j, now), id(i, j + 1, k), 1, a);
                    } else {
                        mcmf::add_edge(id(i, j, now), id(i, j + 1, k), 1, a + c);
                        mcmf::add_edge(id(i, j, now), id(i, j + 1, now - 1), k, 0);
                    }
                }
                if (j != 1) {
                    if (oil[i][j - 1]) {
                        mcmf::add_edge(id(i, j, now), id(i, j - 1, k), 1, a + b);
                    } else {
                        mcmf::add_edge(id(i, j, now), id(i, j - 1, k), 1, a + b + c);
                        mcmf::add_edge(id(i, j, now), id(i, j - 1, now - 1), k, b);
                    }
                }
            }
    int st = id(n, n, k) + 1, ed = st + 1;
    mcmf::add_edge(st, id(1, 1, k), 1, 0);
    for (int now = 0; now <= k; ++now)
        mcmf::add_edge(id(n, n, now), ed, 1, 0);
    kout << mcmf::work(ed, st, ed).min_cost << '\n';
    return 0;
}