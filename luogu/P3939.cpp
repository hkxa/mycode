//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      数颜色.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-20.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 998244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? Module(w - P) : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

const int N = 3e5 + 7;

int n, m;
int a[N];
vector<int> occur[N];

inline int count(int l, int r, int c) {
    return lower_bound(occur[c].begin(), occur[c].end(), r + 1) - 
           lower_bound(occur[c].begin(), occur[c].end(), l);
}   

void debug() {
    for (int i = 1; i <= 6; i += 5) {
        printf("occur[%d] : { ", i);
        if (occur[i].empty()) {
            printf("NULL }\n");
            continue;
        } 
        printf("%d", occur[i][0]);
        for (size_t j = 1; j < occur[i].size(); j++) {
            assert(occur[i][j - 1] < occur[i][j]);
            printf(", %d", occur[i][j]);
        }
        printf(" }\n");
    }
}

signed main() {
    n = read<int>();
    m = read<int>();
    for (int i = 1; i <= n; i++) {
        a[i] = read<int>();
        if (occur[a[i]].empty()) occur[a[i]].push_back(0);
        occur[a[i]].push_back(i);
    }
    for (int i = 1; i <= n; i++) {
        if (occur[a[i]].back() != n + 1) occur[a[i]].push_back(n + 1);
    }
    // debug();
    for (int i = 1, l, r; i <= m; i++) {
        if (read<int>() == 1) {
            l = read<int>();
            r = read<int>();
            write(count(l, r, read<int>()), 10);
        } else {
            l = read<int>();
            if (a[l] == a[l + 1]) continue;
            *lower_bound(occur[a[l]].begin(), occur[a[l]].end(), l) = l + 1;
            *lower_bound(occur[a[l + 1]].begin(), occur[a[l + 1]].end(), l + 1) = l;
            swap(a[l], a[l + 1]);
            // debug();
        }
    }
    return 0;
}

// Create File Date : 2020-06-20