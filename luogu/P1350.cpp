/*************************************
 * @problem:      P1350 车的放置.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-02-04.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
       return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
       return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define P 100003
int a, b, c, d, k;
int len[2007];
int f[2007][2007];

int main()
{
    a = read<int>();
    b = read<int>();
    c = read<int>();
    d = read<int>();
    k = read<int>();
    for (int i = 1; i <= c; i++) 
        len[i] = d;
    for (int i = c + 1; i <= a + c; i++) 
        len[i] = b + d;
    f[0][0] = 1;
    for (int i = 1; i <= a + c; i++) 
        for (int j = 0; j <= i; j++)
            f[i][j] = (f[i - 1][j] + f[i - 1][j - 1] * (len[i] - j + 1)) % P;
    write(f[a + c][k], 10);
    return 0;
}