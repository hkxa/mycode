//Checker for NOI2008 Match
//Attention: This checker is **NOT** the checker used for final judging
//           Just to check whether your output is valid
//Please put testlib.h in the same directory with the checker
//before you compile it.
//usage: ./checker <input_file> <output_file> <answer_file>
//Though this checker do not need an answer file
//You still need to create an empty answer file to fill in this argument
#include "..\..\testlib\testlib.h"
using namespace std;
int n,x;
int prev[2000],next[2000],list[2000],anslist[2000],a[2000],limit[2000],res[2000];
double p[2000][2000],pos[2000][100],ansvalue,d;
double best;
int sum,v;
void calc(int l,int r,int steps)
{
 if(steps==1)pos[list[l]][1]=1;
 else
 {
  int mid=(l+r)/2;
  calc(l,mid,steps-1);
  calc(mid+1,r,steps-1);
  for(int i=l;i<=mid;i++)
   for(int j=mid+1;j<=r;j++)
   {
    pos[list[i]][steps]+=pos[list[i]][steps-1]*pos[list[j]][steps-1]*p[list[i]][list[j]];
    pos[list[j]][steps]+=pos[list[i]][steps-1]*pos[list[j]][steps-1]*p[list[j]][list[i]];
   }
  for(int i=l;i<=r;i++)
   pos[list[i]][steps-1]-=pos[list[i]][steps];
 }
}
int main(int argc,char* argv[])
{
 registerTestlibCmd(argc,argv);
 n=inf.readInt();
 for(int t=1;t<n;t*=2)
  x++;
 for(int i=1;i<=n;i++)
  for(int j=1;j<=n;j++)
  {
   double t=inf.readReal();
   p[i][j]=t;
  }
 for(int i=1;i<=n;i++)
  for(int j=1;j<=n;j++)
   if(i!=j&&(p[i][j]+p[j][i]<0.99||p[i][j]+p[j][i]>1.01))
    quitf(_fail,"Input format error\n");
 for(int i=1;i<=x+1;i++)
  a[i]=inf.readInt();
 for(int i=1;i<=n;i++)
  list[i]=ouf.readInt(1,n);
 int xy[2000];
 memset(xy,0,sizeof(xy));
 for(int i=1;i<=n;i++)
  if(xy[list[i]]>0)
   quitf(_wa,"Format error:Not a permutation\n");
  else
   xy[list[i]]=i;
 if(list[1]!=1)
  quitf(_wa,"Format error:The first number isn't 1\n");
 memset(pos,0,sizeof(pos));
 calc(1,n,x+1);
 double cur=0;
 for(int i=1;i<=x+1;i++)
  cur+=pos[1][i]*a[i];
 quitf(_ok,"OK. Your answer is %.8lf\n",cur);
 return 0;
}