//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Traffic Real Time Query System.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-28.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

struct Reader {
    char endch;
    Reader() { endch = '\0'; }
    template <typename Int>
    Reader& operator >> (Int &i) {
        i = baseFastio::read<Int>(endch);
        return *this;
    }
    template <typename Int>
    inline Int get_int() {
        return baseFastio::read<Int>();
    }
    inline char get_nxt() {
        return endch;
    }
} read;

struct Writer {
    Writer& operator << (const char *ch) {
        // char *p = ch;
        while (*ch) putchar(*(ch++));
        return *this;
    }
    Writer& operator << (const char ch) {
        putchar(ch);
        return *this;
    }
    template <typename Int>
    Writer& operator << (const Int i) {
        baseFastio::write(i);
        return *this;
    }
} write;

// #define int int64

const int N = 10000 + 7, M = 100000 + 7; // 正确但 RE-80
// const int N = 100000 + 7, M = 200000 + 7; // 在 XOJ 上只有这样才能过

int n, m;
int u[M], v[M];
vector<pair<int, int> > G[N]; // G : original (from input) 
vector<int> T[N << 1]; // note : T is new G
vector<int> vdcc[N]; int belong[M], cnt; // note : belong[e] e is edge_id
int dfn[N], low[N], dft;
int s[N], top;

void tarjan(int u, int root) {
    s[++top] = u;
    low[u] = dfn[u] = ++dft;
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i].first;
        if (!dfn[v]) {
            tarjan(v, root);
            low[u] = min(low[u], low[v]);
            if (low[v] >= dfn[u]) {
                cnt++;
                do {
                    vdcc[cnt].push_back(s[top]);
                    // printf("v-DCC[%d] : has %d\n", cnt, u, s[top]);
                } while (s[top--] != v);
                vdcc[cnt].push_back(u);
            }
        } else {
            low[u] = min(low[u], dfn[v]);
        }
    }
}

int fa[N << 1][18], dep[N << 1];
bool vis[N << 1];

void dfs(int u, int f) {
    fa[u][0] = f;
    vis[u] = 1;
    dep[u] = dep[f] + 1;
    for (int k = 0; k < 17; k++)
        fa[u][k + 1] = fa[fa[u][k]][k];
    for (size_t i = 0; i < T[u].size(); i++) {
        if (!vis[T[u][i]]) dfs(T[u][i], u);
    }
}

void jump(int &u, int k) {
    for (int i = 0; i < 18; i++)
        if (k & (1 << i)) u = fa[u][i];
}

int lca(int u, int v) {
    if (dep[u] > dep[v]) jump(u, dep[u] - dep[v]);
    else jump(v, dep[v] - dep[u]);
    if (u == v) return u;
    for (int k = 17; fa[u][0] != fa[v][0]; k--)
        if (fa[u][k] != fa[v][k]) {
            u = fa[u][k];
            v = fa[v][k];
        }
    return fa[u][0];
}

int cookie[N];

void ColorEdge(int u, int KeyToCookie) {
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i].first, Eid = G[u][i].second;
        if (cookie[v] == KeyToCookie && !belong[Eid]) {
            belong[Eid] = KeyToCookie;
            ColorEdge(v, KeyToCookie);
        }
    }
}

signed main() {
    while (true) {
        read >> n >> m;
        if (!n && !m) break;
        for (int i = 1; i <= n; i++) {
            G[i].clear();
            T[i].clear();
            T[i + n].clear();
            dfn[i] = 0;
            vis[i] = vis[i + n] = 0;
            cookie[i] = 0;
        }
        while (cnt) vdcc[cnt--].clear();
        dft = top = 0;
        for (int i = 1; i <= m; i++) {
            belong[i] = 0;
            read >> u[i] >> v[i];
            G[u[i]].push_back(make_pair(v[i], i));
            G[v[i]].push_back(make_pair(u[i], i));
        }
        for (int i = 1; i <= n; i++)
            if (!dfn[i]) tarjan(i, i);
        for (int i = 1; i <= cnt; i++)
            for (size_t j = 0; j < vdcc[i].size(); j++) {
                T[vdcc[i][j]].push_back(i + n);
                T[i + n].push_back(vdcc[i][j]);
            }
        for (int i = 1; i <= n + cnt; i++)
            if (!vis[i]) dfs(i, 0);
        int Q = read.get_int<int>(), s, t, ans;
        while (Q--) {
            read >> s >> t;
            ans = 0;
            ans = max(ans, dep[u[s]] + dep[u[t]] - 2 * dep[lca(u[s], u[t])]);
            ans = max(ans, dep[u[s]] + dep[v[t]] - 2 * dep[lca(u[s], v[t])]);
            ans = max(ans, dep[v[s]] + dep[u[t]] - 2 * dep[lca(v[s], u[t])]);
            ans = max(ans, dep[v[s]] + dep[v[t]] - 2 * dep[lca(v[s], v[t])]);
            // printf("s : E<%d, %d>\n", u[s], v[s]);
            // printf("t : E<%d, %d>\n", u[t], v[t]);
            // printf("ans = %d\n", ans);
            write << (ans / 2 - 1) << '\n';
        }
    }
    return 0;
}