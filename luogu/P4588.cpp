/*************************************
 * problem:      P4588 [TJOI2018]数学计算. 
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-11-07.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int T;
int n, P;
int op, m; 

struct data {
    int64 val;
} t[100007 << 4] = {0};  

inline void pushup(int p)
{
    t[p].val = t[p << 1].val * t[p << 1 | 1].val % P;
}

inline void build(int p, int l, int r)
{
    if (l > r) return;
    if (l == r) {
        t[p].val = 1;
        return;
    }
    int mid = (l + r) >> 1;
    build(p << 1, l, mid);
    build(p << 1 | 1, mid + 1, r);
    pushup(p);
}

inline void change(int p, int l, int r, int pos, int64 k)
{
    if (l > pos || r < pos) return;
    if (l == pos && r == pos) {
        t[p].val = k;
        return;
    }
    int mid = (l + r) >> 1;
    change(p << 1, l, mid, pos, k);
    change(p << 1 | 1, mid + 1, r, pos, k);
    pushup(p);
}

int main()
{
    T = read<int>();
    while (T--) {
        n = read<int>();
        P = read<int>();
        build(1, 1, n);
        for (int i = 1; i <= n; i++) {
            op = read<int>();
            m = read<int>();
            if (op == 1) change(1, 1, n, i, m);
            else change(1, 1, n, m, 1);
            write(t[1].val, 10);
        }
    }
    return 0;
}