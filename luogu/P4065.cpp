/*************************************
 * @problem:      [JXOI2017]颜色.
 * @author:       brealid.
 * @time:         2020-11-24.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#define getchar() (i_P1 == i_P2 && (i_P2 = (i_P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), i_P1 == i_P2) ? EOF : *i_P1++)
char BUF[1 << 21], *i_P1 = BUF, *i_P2 = BUF;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

uint64 rand_u64() {
    return ((uint64)rand() << 47) ^ ((uint64)rand() << 30) ^ ((uint64)rand() << 13) ^ ((uint64)rand() >> 4);
}

const int N = 3e5 + 7;

int T, n;
uint64 val[N];
queue<int> q[N];
map<uint64, int> occured;

signed main() {
    for (kin >> T; T--; ) {
        kin >> n;
        for (int i = 1; i <= n; ++i)
            q[kin.get<int>()].push(i);
        for (int i = 1; i <= n; ++i) {
            uint64 now = 0;
            while (!q[i].empty()) {
                int x = q[i].front();
                q[i].pop();
                if (q[i].empty()) val[x] = now;
                else now ^= (val[x] = rand_u64());
            }
        }
        occured.clear();
        occured[0] = 1;
        uint64 now = 0, ans = 0;
        for (int i = 1; i <= n; ++i) {
            now ^= val[i];
            ans += occured[now]++;
        }
        kout << ans << '\n';
    }
    return 0;
}