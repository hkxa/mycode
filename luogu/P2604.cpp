//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      [模板]网络最大流.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-13.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }

// #define int int64

const int N = 1007, M = 1007;

int n, m, s, t;
namespace Q1 {
    struct Edge {
        int u, v, w;
        int cost;
        int next;
        Edge() {}
        Edge(int _u, int _v, int _w, int _cost, int _next) : u(_u), v(_v), w(_w), cost(_cost), next(_next) {}
    };

    Edge edges[21007]; 
    int head[1007], top = 0;
    int could[1007];
    int road[1007];

    #define addEg(u, v, w, cost) edges[top] = Edge(u, v, w, cost, head[u]); head[u] = top++;

    int dis[1007];
    bool vis[1007] = {0};
    bool SPFA() {
        std::queue<int> q;
        memset(dis, 0x3f, sizeof(dis));
        memset(vis, 0, sizeof(vis));
        dis[s] = 0;
        vis[s] = 1;
        q.push(s);
        while (!q.empty()) {
            int now = q.front();
            q.pop();
            vis[now] = 0;
            for (int nNow = head[now]; ~nNow; nNow = edges[nNow].next) {
                Edge &e = edges[nNow];
                if (e.w > 0 && dis[e.v] > dis[now] + e.cost) {
                    dis[e.v] = dis[now] + e.cost;
                    road[e.v] = nNow;
                    if (!vis[e.v]) {
                        q.push(e.v);
                        vis[e.v] = 1;
                    }
                }
            }
        }
        if (dis[t] != 0x3f3f3f3f) return true; // success
        else return false; // unsuccess
    }

    int Edmonds_Karp()
    {
        int maxflow = 0, mincost = 0;
        std::queue <int> q;
        // printf("s = %d, t = %d\n", s, t);
        while (SPFA()) {
            int minn = 0x3f3f3f3f;
            for (int pNow = t; pNow != s; pNow = edges[road[pNow]].u) {
                // printf("E <%d, %d, %d, %d>\n", edges[road[pNow]].u, edges[road[pNow]].v, 
                //                                edges[road[pNow]].w, edges[road[pNow]].cost);
                if (edges[road[pNow]].w < minn) minn = edges[road[pNow]].w;
            }
            // printf("%d %d [diss = %d, dist = %d] minn = %d\n", maxflow, mincost, dis[s], dis[t], minn);
            for (int pNow = t; pNow != s; pNow = edges[road[pNow]].u) {
                edges[road[pNow]].w -= minn;
                edges[road[pNow] ^ 1].w += minn;
            }
            if (!minn) break;
            maxflow += minn;
            mincost += dis[t] * minn;
        }
        return maxflow;
    }

    inline void ADDE(int u, int v, int w, int r) {
        addEg(u, v, w, r);
        addEg(v, u, 0, -r);
    }
};

namespace Q2 {
    struct Edge {
        int u, v, w;
        int cost;
        int next;
        Edge() {}
        Edge(int _u, int _v, int _w, int _cost, int _next) : u(_u), v(_v), w(_w), cost(_cost), next(_next) {}
    };

    Edge edges[21007]; 
    int head[1007], top = 0;
    int could[1007];
    int road[1007];

    #define add(u, v, w, cost) edges[top] = Edge(u, v, w, cost, head[u]); head[u] = top++;

    int dis[1007];
    bool vis[1007] = {0};
    bool SPFA() {
        std::queue<int> q;
        memset(dis, 0x3f, sizeof(dis));
        memset(vis, 0, sizeof(vis));
        dis[s] = 0;
        vis[s] = 1;
        q.push(s);
        while (!q.empty()) {
            int now = q.front();
            q.pop();
            vis[now] = 0;
            for (int nNow = head[now]; ~nNow; nNow = edges[nNow].next) {
                Edge &e = edges[nNow];
                if (e.w > 0 && dis[e.v] > dis[now] + e.cost) {
                    dis[e.v] = dis[now] + e.cost;
                    road[e.v] = nNow;
                    if (!vis[e.v]) {
                        q.push(e.v);
                        vis[e.v] = 1;
                    }
                }
            }
        }
        if (dis[t] != 0x3f3f3f3f) return true; // success
        else return false; // unsuccess
    }

    int Edmonds_Karp()
    {
        int maxflow = 0, mincost = 0;
        std::queue <int> q;
        // printf("s = %d, t = %d\n", s, t);
        while (SPFA()) {
            int minn = 0x3f3f3f3f;
            for (int pNow = t; pNow != s; pNow = edges[road[pNow]].u) {
                // printf("E <%d, %d, %d, %d>\n", edges[road[pNow]].u, edges[road[pNow]].v, 
                //                                edges[road[pNow]].w, edges[road[pNow]].cost);
                if (edges[road[pNow]].w < minn) minn = edges[road[pNow]].w;
            }
            // printf("%d %d [diss = %d, dist = %d] minn = %d\n", maxflow, mincost, dis[s], dis[t], minn);
            for (int pNow = t; pNow != s; pNow = edges[road[pNow]].u) {
                edges[road[pNow]].w -= minn;
                edges[road[pNow] ^ 1].w += minn;
            }
            if (!minn) break;
            maxflow += minn;
            mincost += dis[t] * minn;
        }
        return mincost;
    }

    inline void ADDE(int u, int v, int w, int r) {
        add(u, v, w, r);
        add(v, u, 0, -r);
    }
}

int k;

signed main() {
    n = read<int>();
    m = read<int>();
    s = 1;
    t = n;
    k = read<int>();
    memarr(n, -1, Q1::head);
    memarr(n, -1, Q2::head);
    for (int i = 1, a, b, v, w; i <= m; i++) {
        a = read<int>();
        b = read<int>();
        v = read<int>();
        w = read<int>();
        Q1::ADDE(a, b, v, 0);
        Q2::ADDE(a, b, v, 0);
        Q2::ADDE(a, b, k, w);
    }
    write(Q1::Edmonds_Karp(), 32);
    for (int i = 0; i < m; i++) {
        // printf("E<%d, %d> w : %d -> %d\n", Q2::edges[i << 2].u, Q2::edges[i << 2].v, Q2::edges[i << 2].w, Q1::val[i << 1]);
        Q2::edges[i << 2].w = Q1::edges[i << 1].w;
        Q2::edges[i << 2 | 1].w = Q1::edges[i << 1 | 1].w;
    }
    s = n + 1;
    t = n + 2;
    Q2::ADDE(s, 1, k, 0);
    Q2::ADDE(n, t, k, 0);
    write(Q2::Edmonds_Karp(), 10);
    return 0;
}

// Create File Date : 2020-06-13