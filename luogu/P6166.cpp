//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      高级打字机.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-20.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 998244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? Module(w - P) : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

const int N = 1000000 + 7, SIZ = N << 5;

int n;
int a[N];
int root[N], siz[N];
char val[SIZ];
int l[SIZ], r[SIZ], cnt;

void build_tree(int u, int ml = 1, int mr = n) {
    if (ml == mr) return;
    int mid = (ml + mr) >> 1;
    build_tree(l[u] = ++cnt, ml, mid);
    build_tree(r[u] = ++cnt, mid + 1, mr);
}

void modify_tree(int base, int u, int loc, char newval, int ml = 1, int mr = n) {
    if (ml == mr) {
        val[u] = newval;
        return;
    }
    int mid = (ml + mr) >> 1;
    if (loc <= mid) {
        r[u] = r[base];
        modify_tree(l[base], l[u] = ++cnt, loc, newval, ml, mid);
    } else {
        l[u] = l[base];
        modify_tree(r[base], r[u] = ++cnt, loc, newval, mid + 1, mr);
    }
}

inline void modify_type(int i, int v, char newval) {
    modify_tree(root[v], root[i] = ++cnt, siz[i] = siz[v] + 1, newval);
}

inline void modify_undo(int i, int bef) {
    root[i] = ++cnt;
    l[cnt] = l[root[i - bef]];
    r[cnt] = r[root[i - bef]];
    siz[i] = siz[i - bef];
}

int query_tree(int u, int loc, int ml = 1, int mr = n) {
    if (ml == mr) return val[u];
    int mid = (ml + mr) >> 1;
    if (loc <= mid) return query_tree(l[u], loc, ml, mid);
    else return query_tree(r[u], loc, mid + 1, mr);
}

inline int query(int v, int loc) {
    return query_tree(root[v], loc);
}

char GetOp() {
    static char buf[3];
    scanf("%s", buf);
    return buf[0];
}

signed main() {
    n = read<int>();
    siz[0] = 0;  
    build_tree(root[0] = ++cnt);
    for (int i = 1, cur = 0; i <= n; i++) {
        switch (GetOp()) {
            case 'T' : 
                cur++;
                modify_type(cur, cur - 1, GetOp());
                break;
            case 'U' : 
                cur++;
                modify_undo(cur, read<int>() + 1);
                break;
            case 'P' : 
                putchar(query(cur, read<int>() + 1));
                putchar(10);
                break;
        }
    }
    return 0;
}

// Create File Date : 2020-06-20