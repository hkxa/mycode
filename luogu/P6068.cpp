/*************************************
 * @problem:      P6068 [MdOI2020] GCD? GCD!.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-02-10.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int T, n, ans = 0;

int main()
{
    T = read<int>();
    while (T--) {
        n = read<int>();
        if (n < 6) puts("-1");
        else {
            ans = 0;
            for (int i = 1; i * i <= n; i++) {
                if (n % i) continue;
                if (i >= 6) ans = max(ans, n / i);
                if (n / i >= 6) ans = max(ans, i);
            }
            write(ans, 10);
        }
    }
    return 0;
}