/*************************************
 * problem:      P4401 [IOI2007]Miners 矿工配餐.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-06-28.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

int n;
int food[100007];
int f[2][4][4][4][4];
#define now f[i % 2]
#define last f[(i + 1) % 2]
#define res f[n % 2]

void readFoodIn()
{
    char s[100007];
    scanf("%s", s + 1);
    for (int i = 1; i <= n; i++) {
        switch (s[i]) {
            case 'M' : food[i] = 1; break;
            case 'F' : food[i] = 2; break;
            case 'B' : food[i] = 3; break;
            default  : food[i] = 0; break;
        }
    }
}

int getCoal(int a, int b, int c)
{
    int ret(1);
    if (a && a != b && a != c) ret++; 
    if (b && b != c) ret++;
    return ret;
}

#define FOR_EACH_ELE                            \
for (int A1 = 0; A1 <= 3; A1++)                 \
    for (int A2 = 0; A2 <= 3; A2++)             \
        for (int B1 = 0; B1 <= 3; B1++)         \
            for (int B2 = 0; B2 <= 3; B2++) 

int main()
{
    n = read<int>();
    readFoodIn();
    memset(f, -1, sizeof(f));
    f[0][0][0][0][0] = 0;
    for (int i = 1; i <= n; i++) {
        // printf("food #%d: %c\n", i, food[i] == 1 ? 'M' : (food[i] == 2 ? 'F' : 'B'));
        FOR_EACH_ELE {
            if (last[A1][A2][B1][B2] == -1) continue;
            now[A2][food[i]][B1][B2] = max(now[A2][food[i]][B1][B2], last[A1][A2][B1][B2] + getCoal(A1, A2, food[i]));
            now[A1][A2][B2][food[i]] = max(now[A1][A2][B2][food[i]], last[A1][A2][B1][B2] + getCoal(B1, B2, food[i]));
        }
        memset(last, -1, sizeof(last));
    }
    int ans = 0;
    FOR_EACH_ELE {
        ans = max(ans, res[A1][A2][B1][B2]);
    }
    write(ans);
    return 0;
}