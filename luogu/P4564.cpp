/*************************************
 * @problem:      [CTSC2018]假面.
 * @author:       brealid.
 * @time:         2021-01-08.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;
#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 200 + 5, M = 100 + 5, P = 998244353;

inline int64 qpow(int64 a, int n) {
    int64 res = 1;
    for (; n; n >>= 1) {
        if (n & 1) res = res * a % P;
        a = a * a % P;
    }
    return res;
}

int n, q, hp[N];
int64 inv[N];
int64 p[N][M];

void attack(int goal, int64 per) {
    for (int i = 1; i <= hp[goal]; ++i) {
        p[goal][i - 1] = (p[goal][i - 1] + p[goal][i] * per) % P;
        p[goal][i] = p[goal][i] * (P + 1 - per) % P;
    }
}

int64* get_percent(int k, int64 *p) {
    int64 *result = new int64[k + 1];
    memset(result, 0, sizeof(int64) * (k + 1));
    result[0] = 1;
    for (int i = 0; i < k; ++i)
        for (int j = i + 1; j > 0; --j) {
            result[j] = (result[j] + result[j - 1] * p[i]) % P;
            result[j - 1] = result[j - 1] * (P + 1 - p[i]) % P;
        }
    return result;
}

void query(int k, int64 *p) {
    int64 *cnt = get_percent(k, p), *t = new int64[k + 1];
    // for (int j = 0; j <= k; ++j) printf("(live %d in p %d) ", j, cnt[j]);
    // printf("\n");
    for (int i = 0; i < k; ++i) {
        int inv_pi = qpow(p[i], P - 2);
        t[k - 1] = cnt[k] * inv_pi % P;
        for (int j = k - 1; j > 0; --j)
            t[j - 1] = (cnt[j] - t[j] * (1 - p[i])) % P * inv_pi % P;
        int64 ans = 0;
        for (int j = 0; j < k; ++j)
            ans += t[j] * inv[j + 1] % P;
        // printf("   For person #%d:", i);
        // for (int j = 0; j < k; ++j) printf(" (live %d in p %d)", j, t[j]);
        // printf("\n");
        kout << ans % P * p[i] % P << ' ';
    }
    kout << '\n';
    delete[] cnt;
}

signed main() {
    kin >> n;
    for (int i = 1; i <= n; ++i) {
        kin >> hp[i];
        p[i][hp[i]] = 1;
        inv[i] = qpow(i, P - 2);
    }
    // cerr << "Read HP Finished" << endl;
    kin >> q;
    for (int i = 1, op, k, u, v; i <= q; ++i) {
        kin >> op >> k;
        if (op == 0) {
            kin >> u >> v;
            attack(k, u * qpow(v, P - 2) % P);
        } else {
            int64 *alive = new int64[k];
            for (int i = 0; i < k; ++i) {
                kin >> u;
                alive[i] = P + 1 - p[u][0];
            }
            query(k, alive);
            delete[] alive;
        }
        // cerr << "Deal Operation " << i << " Finished" << endl;
    }
    for (int i = 1; i <= n; ++i) {
        int64 ans = 0;
        for (int j = 1; j <= hp[i]; ++j) ans += j * p[i][j] % P;
        // printf("For person #%d:", i);
        // for (int j = 0; j <= hp[i]; ++j) printf(" (hp %d in p %d)", j, p[i][j]);
        // printf("\n");
        kout << ans % P << ' ';
    }
    kout << '\n';
    return 0;
}