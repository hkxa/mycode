/*************************************
 * problem:      P1197 [JSOI2008]星球大战.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-07-22.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

typedef vector<int> VecGraph;
typedef VecGraph::iterator VGI;

int n, m, q;
VecGraph G[400007];
stack<int> killPlanet;
stack<int> ans;
bool alive[400007];

struct UFS {
    int fa[400007];
    int familyCnt;

    UFS() : familyCnt(0)
    {
        memset(fa, -1, sizeof(fa));
    }

    int find(int u)
    {
        return fa[u] < 0 ? u : fa[u] = find(fa[u]);
    }

    void connect(int x, int y)
    {
        int xx = find(x), yy = find(y);
        if (xx == yy) return;
        if (fa[xx] > fa[yy]) swap(xx, yy);
        fa[xx] += fa[yy];
        fa[yy] = xx;
        familyCnt--;
    }

    void addPoint(int u)
    {
        familyCnt++;
        for (VGI it = G[u].begin(); it != G[u].end(); it++) {
            if (alive[*it]) connect(u, *it);
        }
    }
} fam;

int main()
{
    int x, y;
    n = read<int>();
    m = read<int>();
    for (int i = 0; i < n; i++) {
        alive[i] = true;
    }
    for (int i = 1; i <= m; i++) {
        x = read<int>();
        y = read<int>();
        G[x].push_back(y);
        G[y].push_back(x);
    }
    q = read<int>();
    for (int i = 1; i <= q; i++) {
        x = read<int>();
        alive[x] = false;
        killPlanet.push(x);
    }
    for (int i = 0; i < n; i++) {
        if (alive[i]) {
            fam.addPoint(i);
        }
    }
    ans.push(fam.familyCnt);
    while (!killPlanet.empty())
    {
        int liver = killPlanet.top(); 
        killPlanet.pop();
        alive[liver] = true;
        fam.addPoint(liver);
        ans.push(fam.familyCnt);
    }
    while (!ans.empty())
    {
        write(ans.top(), 10); 
        ans.pop();
    }
    return 0;
}