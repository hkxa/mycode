/*************************************
 * problem:      P2018 消息传递.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-06-10.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

int n;
vector<int> G[1007];
typedef vector<int>::iterator Vec_It;
int f[1007][1007] = {0};
// f[id][father]

int Get_Ans(int u, int fa)
{
    int a[G[u].size()], n = 0;
    if (f[u][fa]) return f[u][fa];
    for (Vec_It vi = G[u].begin(); vi != G[u].end(); vi++) {
        if (*vi == fa) continue;
        a[n++] = Get_Ans(*vi, u);
    }
    sort(a, a + n);
    for (int i = 0; i < n; i++) {
        f[u][fa] = max(f[u][fa], a[i] + n - i);
    }
    // printf("[info] f[%d][%d] = %d.\n", u, fa, f[u][fa]);
    return f[u][fa];
}

int main()
{
    n = read<int>();
    register int father, ans(0x3f3f3f3f);
    for (int i = 2; i <= n; i++) {
        father = read<int>();
        G[father].push_back(i);
        G[i].push_back(father);
    }
    for (int i = 1; i <= n; i++) {
        ans = min(ans, Get_Ans(i, 0));
    }
    write(ans + 1, 10);
    for (int i = 1; i <= n; i++) {
        if (Get_Ans(i, 0) == ans) write(i, 32);
    }
    return 0;
}
