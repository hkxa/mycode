/*************************************
 * problem:      P2613 【模板】有理数取余.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-05-04.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

// MOD - READ
template <typename Int>
inline Int read()       
{
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = ((init << 3) + (init << 1) + (c & 15)) % 19260817;
	return init;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

long long a, b;

long long fexp(long long b, long long n)
{
    long long res = 1;
    while (n) {
        if (n & 1) res = (res * b) % 19260817;
        b = (b * b) % 19260817;
        n >>= 1;
    }
    return res;
}

#define inv(a) fexp(a, 19260815)

int main()
{
    a = read<long long>();
    b = read<long long>();
    if (!b) {
        puts("Angry!");
        return 0;
    }
    write(a * inv(b) % 19260817);
    return 0;
}