T = int(input())
for _ in range(T):
    n = int(input())
    s = input()
    occur = set()
    ans = 0
    for i in range(n):
        occur.add(s[i])
        ans += len(occur)
    print(ans)