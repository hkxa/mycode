/*************************************
 * @problem:      P3258 [JLOI2014]松鼠的新家.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-01-10.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int Log = 19;
int depth[300007];
int fa[300007][Log + 2];
int n, a[300007];
int val[300007], ans[300007];
vector<int> G[300007];

void dfs_LCA(int u, int faNode)
{
    depth[u] = depth[faNode] + 1;
    fa[u][0] = faNode;
    for (int k = 1; k <= Log; k++) {
        fa[u][k] = fa[fa[u][k - 1]][k - 1];
    }
    for (unsigned i = 0; i < G[u].size(); i++) 
        if (G[u][i] != faNode) dfs_LCA(G[u][i], u);
}

int jump(int u, int dep)
{
    for (int i = 0; i <= Log; i++) {
        if (dep & (1 << i)) u = fa[u][i];
    }
    return u;
}

int get_LCA(int u, int v)
{
    if (depth[u] < depth[v]) swap(u, v);
    u = jump(u, depth[u] - depth[v]);
    // printf("u = %d, v = %d.\n", u, v);
    for (int k = Log; k >= 0; k--) {
        if (fa[u][k] != fa[v][k]) {
            u = fa[u][k];
            v = fa[v][k];
        }
    }
    return u != v ? fa[u][0] : u;
}

void dfs_getAns(int u, int faNode)
{
    ans[u] = val[u];
    for (unsigned i = 0; i < G[u].size(); i++) 
        if (G[u][i] != faNode) {
            dfs_getAns(G[u][i], u);
            ans[u] += ans[G[u][i]];
            // printf("[%d] ans %d (+%d) -> %d.\n", u, ans[u], ans[G[u][i]], ans[u] + ans[G[u][i]]);
        }
}

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) a[i] = read<int>();
    for (int i = 1, u, v; i < n; i++) {
        u = read<int>();
        v = read<int>();
        G[u].push_back(v);
        G[v].push_back(u);
    }
    dfs_LCA(1, 0);
    for (int i = 1, lca; i < n; i++) {
        lca = get_LCA(a[i], a[i + 1]);
        // printf("%d, %d's lca = %d.\n", a[i], a[i + 1], lca);
        val[a[i]]++;
        val[a[i + 1]]++;
        val[lca]--;
        val[fa[lca][0]]--;
    }
    dfs_getAns(1, 0);
    ans[a[1]]++;
    for (int i = 1; i <= n; i++) write(ans[i] - 1, 10);
    return 0;
}