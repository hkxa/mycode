/*************************************
 * @problem:      angry.
 * @author:       brealid.
 * @time:         2021-03-27.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

// #define int int64

int64 fpow(int64 a, int n, const int64 P) {
    // a = (a + P) % P;
    int64 ret = 1;
    while (n) {
        if (n & 1) ret = ret * a % P;
        a = a * a % P;
        n >>= 1;
    }
    return ret;
}

template <int64 P>
struct poly : public std::vector<int64> {
    poly<P> operator + (const poly<P> &other) const {
        int n = size();
        poly<P> res(n);
        for (int i = 0; i < n; ++i) res[i] = ((*this)[i] + other[i]) % P;
        return res;
    }
    int64 calc(int64 x) {
        x %= P;
        int n = size();
        int64 px = 1, ret = 0;
        for (int i = 0; i < n; ++i) {
            ret = (ret + (*this)[i] * px) % P;
            px = px * x % P;
        }
        return ret;
    }
};

template <int64 P>
int64 Lagrange(const poly<P> &f, int64 x) {
    int n = f.size();
    int64 ret = 0, fz, fm;
    for (int i = 0; i < n; ++i) {
        fz = fm = 1;
        for (int j = 0; j < n; ++j)
            if (i != j) {
                fz = fz * (x - j) % P;
                fm = fm * (i - j) % P;
            }
        int64 value = fz * fpow(fm, P - 2, P) % P;
        ret = (ret + value * f[i] % P) % P;
    }
    return (ret + P) % P;
}

const int N = 5e5 + 7, K = 500 + 7, P = 1e9 + 7, inv2 = (P + 1) >> 1;

char n[N];
int nModP, len, k;
poly<P> a, presum_a;
int64 pow2[N], C[K][K];
int64 ans = 0;

void get_C() {
    for (int i = 0; i <= k; ++i) {
        C[i][0] = 1;
        for (int j = 1; j <= i; ++j) C[i][j] = (C[i - 1][j - 1] + C[i - 1][j]) % P;
    }
}

int64 fValue[K][K];
int64 f0(int tn, int k) {
    if (tn > k) return 0;
    if (tn == 0) return k == 0;
    if (fValue[tn][k]) return fValue[tn][k];
    int64 ret = f0(tn - 1, k), X = pow2[tn - 1], px = 1;
    for (int j = k; j >= 0; --j) {
        ret = (ret - C[k][j] * px % P * f0(tn - 1, j) % P) % P;
        px = px * X % P;
    }
    return fValue[tn][k] = (ret + P) % P;
}

signed main() {
    kin >> (n + 1) >> k;
    len = strlen(n + 1);
    for (int i = pow2[0] = 1; i <= len; ++i) pow2[i] = (pow2[i - 1] << 1) % P;
    get_C();
    a.resize(k);
    presum_a.resize(k + 1);
    for (int i = 0; i < k; ++i) kin >> a[i];
    presum_a[0] = a.calc(0);
    for (int i = 1; i < k + 1; ++i) presum_a[i] = (presum_a[i - 1] + a.calc(i)) % P;
    for (int i = 1; i <= len; ++i) nModP = (nModP << 1 | (n[i] & 1)) % P;
    ans = Lagrange(presum_a, (nModP + P - 1) % P);
    nModP = 0;
    int negative = -1;
    for (int i = 1; i <= len; ++i) {
        if (n[i] == '1') {
            if (len - i <= k) {
                for (int j1 = 0; j1 < k; ++j1) {
                    int64 sm = 0, px = 1;
                    for (int j2 = j1; j2 >= 0; --j2) {
                        sm = (sm + C[j1][j2] * px % P * f0(len - i, j2) % P) % P;
                        px = px * nModP % P;
                    }
                    sm *= negative;
                    ans = (ans + a[j1] * sm % P) % P;
                }
            }
            nModP = (nModP + pow2[len - i]) % P;
            negative = -negative;
        }
    }
    kout << (ans + P) * inv2 % P << '\n';
    return 0;
}