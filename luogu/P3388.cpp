/*************************************
 * problem:      P3388 【模板】割点（割顶）.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-07-31.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

const int N = 100000 + 100;
vector<long long> e[N];
int n, m;
int dfn[N], low[N], IsGD[N], nd_to, size[N];

void Tarjan(int now, int fa) 
{
    dfn[now] = low[now] = ++nd_to;
    int childCnt = 0;
    for (int i = 0; i < int(e[now].size()); i++) {
        if (dfn[e[now][i]] == 0) {
            Tarjan(e[now][i], fa);
            low[now] = min(low[now], low[e[now][i]]);
            if (low[e[now][i]] >= dfn[now] && now != fa) {
                IsGD[now] = true;
            } else if (now == fa) childCnt++;
        } 
        low[now] = min(low[now], dfn[e[now][i]]);
    }
    if (childCnt >= 2 && now == fa)
        IsGD[now] = true;
}

int main() 
{
    n = read<int>(), m = read<int>();
    for (int i = 1; i <= n; i++) {
        e[i].reserve(4);
    }
    for (int i = 1; i <= m; i++) {
        int s = read<int>(), t = read<int>();
        e[s].push_back(t);
        e[t].push_back(s);
    }
    for (int i = 1; i <= n; i++) {
        if (!dfn[i]) Tarjan(i, i);
    }
    vector<int> ansVec;
    for (int i = 1; i <= n; i++) {
        if (IsGD[i]) ansVec.push_back(i);
    }
    write(ansVec.size(), 10);
    for (vector<int>::iterator it = ansVec.begin(); it != ansVec.end(); it++) {
        write(*it, 32);
    }
    return 0;
}