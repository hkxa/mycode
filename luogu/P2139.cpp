/*************************************
 * @problem:      P2139 小Z的掷骰游戏.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-02-10.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int Mid = 17, ArrSize = Mid * 2 + 3;
int n;
int topnum[ArrSize][ArrSize], high[ArrSize][ArrSize];
int diceTopnumCount[7];

void debug_board()
{
    int l = Mid, r = Mid, t = Mid, b = Mid;
    for (int i = 1; i <= Mid * 2; i++)
        for (int j = 1; j <= Mid * 2; j++)
            if (high[i][j]) {
                l = min(l, j);
                r = max(r, j);
                t = min(t, i);
                b = max(b, i);
            }
    for (int i = t; i <= b; i++) 
        for (int j = l; j <= r; j++)
            if (high[i][j])
                printf("%d[H%d]%c", topnum[i][j], high[i][j], " \n"[j == r]);
            else 
                printf("     %c", " \n"[j == r]);
}

/*   
      /
   1 /
 ---+ 5
  3 |
    |
*/

const int xx[] = {0, 1, 0, -1}, yy[] = {-1, 0, 1, 0};

struct S_dice {
    int x, y;
    int front, top, bottom, left, right, back;
    void clear_data() 
    {
        x = y = Mid;
        front = top = bottom = left = right = back = 0;
    }
    void debug()
    {
        printf("  +-+\n"
               "  |%d|\n"
               "+-+-+-+-+\n"
               "|%d|%d|%d|%d|\n"
               "+-+-+-+-+\n"
               "  |%d|\n"
               "  +-+\n"
               "\n", back, left, top, right, bottom, front);
    }
    void SetFTL_DeriveBRB(const int F, const int T, const int L) 
    {
        top   = T; bottom = 7 - T; 
        left  = L; right  = 7 - L;  
        front = F; back   = 7 - F;
    }
    void SetDeriveAll(const int T, const int F)
    {
        top = T;
        front = F;
        switch (T) {
            case 1 :
                switch (F) {
                    case 2 : SetFTL_DeriveBRB(F, T, 4); break;
                    case 3 : SetFTL_DeriveBRB(F, T, 2); break;
                    case 4 : SetFTL_DeriveBRB(F, T, 5); break;
                    case 5 : SetFTL_DeriveBRB(F, T, 3); break;
                }
                break;
            case 2 :
                switch (F) {
                    case 1 : SetFTL_DeriveBRB(F, T, 3); break;
                    case 3 : SetFTL_DeriveBRB(F, T, 6); break;
                    case 4 : SetFTL_DeriveBRB(F, T, 1); break;
                    case 6 : SetFTL_DeriveBRB(F, T, 4); break;
                }
                break;
            case 3 :
                switch (F) {
                    case 1 : SetFTL_DeriveBRB(F, T, 5); break;
                    case 2 : SetFTL_DeriveBRB(F, T, 1); break;
                    case 5 : SetFTL_DeriveBRB(F, T, 6); break;
                    case 6 : SetFTL_DeriveBRB(F, T, 2); break;
                }
                break;
            case 4 :
                switch (F) {
                    case 1 : SetFTL_DeriveBRB(F, T, 2); break;
                    case 2 : SetFTL_DeriveBRB(F, T, 6); break;
                    case 5 : SetFTL_DeriveBRB(F, T, 1); break;
                    case 6 : SetFTL_DeriveBRB(F, T, 5); break;
                }
                break;
            case 5 :
                switch (F) {
                    case 1 : SetFTL_DeriveBRB(F, T, 4); break;
                    case 3 : SetFTL_DeriveBRB(F, T, 1); break;
                    case 4 : SetFTL_DeriveBRB(F, T, 6); break;
                    case 6 : SetFTL_DeriveBRB(F, T, 3); break;
                }
                break;
            case 6 :
                switch (F) {
                    case 2 : SetFTL_DeriveBRB(F, T, 3); break;
                    case 3 : SetFTL_DeriveBRB(F, T, 5); break;
                    case 4 : SetFTL_DeriveBRB(F, T, 2); break;
                    case 5 : SetFTL_DeriveBRB(F, T, 4); break;
                }
                break;
        }
    }
    void rotate_4faces(int &a, int &b, int &c, int &d)
    {
        const int t = a;
        a = b; b = c; c = d; d = t;
    }
    bool rotate()
    {
        // debug();
        int can = 15;
        const int arr[] = {left, front, right, back};
        for (int i = 0; i < 4; i++) 
            if (arr[i] < 4 || high[x + xx[i]][y + yy[i]] >= high[x][y])
                can &= 15 - (1 << (3 - i));
        if (!can) return false;
        int maxNum = 0, maxPos = 0;
        for (int i = 0; i < 4; i++) 
            if ((can & (1 << (3 - i))) && arr[i] > maxNum) {
                maxNum = arr[i];
                maxPos = i;
            }
        switch (maxPos) {
            case 0 : rotate_4faces(left, top, right, bottom); break;
            case 1 : rotate_4faces(front, top, back, bottom); break;
            case 2 : rotate_4faces(right, top, left, bottom); break;
            case 3 : rotate_4faces(back, top, front, bottom); break;
        }
        x += xx[maxPos];
        y += yy[maxPos];
        return true;
    }
    void overprint()
    {
        topnum[x][y] = top;
        high[x][y]++;
    }
} dice;

int solve_once()
{
    if (scanf("%d", &n) == EOF) return 0;
    for (int i = 1, T, F; i <= n; i++) {
        dice.clear_data();
        T = read<int>();
        F = read<int>();
        dice.SetDeriveAll(T, F);
        while (dice.rotate());
        dice.overprint();
        // debug_board();
    }
    for (int i = 1; i <= Mid * 2; i++) 
        for (int j = 1; j <= Mid * 2; j++) 
            diceTopnumCount[topnum[i][j]]++;
    for (int i = 1; i <= 6; i++)
        write(diceTopnumCount[i], " \n"[i == 6]);
    return 1;
}

int main()
{
    while (solve_once()) {
        memset(topnum, 0, sizeof(topnum));
        memset(diceTopnumCount, 0, sizeof(diceTopnumCount)); 
        memset(high, 0, sizeof(high)); 
    }
}