/*************************************
 * @problem:      P4289 [HAOI2008]移动玩具.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-01-06.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

struct status {
    bool a[4][4];
    operator int() {
        int res = 0;
        for (int i = 0; i < 4; i++) 
            for (int j = 0; j < 4; j++)
                res = res << 1 | a[i][j];
        return res;
    }
    bool operator == (status S) {
        for (int i = 0; i < 4; i++) 
            for (int j = 0; j < 4; j++)
                if (a[i][j] != S.a[i][j]) return false;
        return true;
    }
} t, final;

int dis[65536];
queue<status> q;
int xx[4] = {0, 0, 1, -1}, yy[4] = {-1, 1, 0, 0};

int main()
{
    memset(dis, 0x3f, sizeof(dis));
    char s[5];
    for (int i = 0; i < 4; i++) {
        scanf("%s", s);
        for (int j = 0; j < 4; j++) t.a[i][j] = s[j] & 1;
    }
    for (int i = 0; i < 4; i++) {
        scanf("%s", s);
        for (int j = 0; j < 4; j++) final.a[i][j] = s[j] & 1;
    }
    if (t == final) {
        puts("0");
        return 0;
    }
    q.push(t);
    dis[t] = 0;
    int times = 0;
    while (!q.empty()) {
        times++;
        t = q.front(); q.pop();
        int start_dist = dis[t];
        for (int i = 0; i < 4; i++) 
            for (int j = 0; j < 4; j++) 
                for (int o = 0; o < 4; o++) {
                    int x = i + xx[o], y = j + yy[o];
                    if (x < 0 || x > 3 || y < 0 || y > 3 || t.a[x][y]) continue;
                    swap(t.a[i][j], t.a[x][y]);
                    if (dis[t] == 0x3f3f3f3f) {
                        dis[t] = start_dist + 1;
                        if (t == final) {
                            write(dis[final]);
                            return 0;
                        }
                        q.push(t);
                    }
                    swap(t.a[i][j], t.a[x][y]);
                }
    }
    return 0;
}