/*************************************
 * @problem:      P3879 [TJOI2010]阅读理解.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2019-12-15.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, l, m;
string t;
set<string> appear;
map<string, vector<int> > pos;

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        l = read<int>();
        appear.clear();
        for (int j = 1; j <= l; j++) {
            cin >> t;
            if (appear.find(t) == appear.end()) {
                appear.insert(t);
                pos[t].push_back(i);
            }
        }
    }
    m = read<int>();
    for (int i = 1; i <= m; i++) {
        cin >> t;
        for (size_t j = 0; j < pos[t].size(); j++) {
            write(pos[t][j], 32);
        }
        putchar(10);
    }
    return 0;
}