/*************************************
 * @problem:      [CQOI2017]老C的任务.
 * @author:       brealid.
 * @time:         2020-11-27.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;
#if false /* 需要使用 fread 优化，改此参数为 true */
#define getchar() (i_P1 == i_P2 && (i_P2 = (i_P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), i_P1 == i_P2) ? EOF : *i_P1++)
char BUF[1 << 21], *i_P1 = BUF, *i_P2 = BUF;
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 1e5 + 7;
struct pos {
    int x, y, p;
} p[N];
bool compare_pos_x(const pos &a, const pos &b) { return a.x < b.x; }
bool compare_pos_y(const pos &a, const pos &b) { return a.y < b.y; }
namespace kD_Tree {
    struct node {
        int ls, rs;
        int x, y, p;
        int minx, maxx, miny, maxy;
        int64 p_sum;
        void set_pos(const pos &u) {
            x = minx = maxx = u.x;
            y = miny = maxy = u.y;
            p_sum = p = u.p;
        }
        void pushup(const node *l, const node *r) {
            if (l) {
                minx = min(minx, l->minx);
                miny = min(miny, l->miny);
                maxx = max(maxx, l->maxx);
                maxy = max(maxy, l->maxy);
                p_sum += l->p_sum;
            }
            if (r) {
                minx = min(minx, r->minx);
                miny = min(miny, r->miny);
                maxx = max(maxx, r->maxx);
                maxy = max(maxy, r->maxy);
                p_sum += r->p_sum;
            }
        }
    } tr[N];
    int root, trcnt;
    void build(int &u, int l, int r, bool D) {
        if (l > r) return;
        u = ++trcnt;
        int mid = (l + r) >> 1;
        nth_element(p + l, p + mid, p + r + 1, D ? compare_pos_x : compare_pos_y);
        tr[u].set_pos(p[mid]);
        // printf("node [%d, %d] : choose (%d, %d)>%d\n", l, r, p[mid].x, p[mid].y, p[mid].p);
        build(tr[u].ls, l, mid - 1, !D);
        build(tr[u].rs, mid + 1, r, !D);
        tr[u].pushup(tr[u].ls ? tr + tr[u].ls : NULL, tr[u].rs ? tr + tr[u].rs : NULL);
    }
    int64 query(const int &u, const int &x1, const int &y1, const int &x2, const int &y2) {
        node &now = tr[u];
        if (!u || now.minx > x2 || now.maxx < x1 || now.miny > y2 || now.maxy < y1) return 0;
        if (now.minx >= x1 && now.maxx <= x2 && now.miny >= y1 && now.maxy <= y2) return now.p_sum;
        int64 res = query(tr[u].ls, x1, y1, x2, y2) + query(tr[u].rs, x1, y1, x2, y2);
        if (now.x >= x1 && now.x <= x2 && now.y >= y1 && now.y <= y2) res += now.p;
        return res;
    }
};

signed main() {
    int n, m;
    kin >> n >> m;
    for (int i = 1; i <= n; ++i)
        kin >> p[i].x >> p[i].y >> p[i].p;
    kD_Tree::build(kD_Tree::root, 1, n, 0);
    for (int i = 1, x1, y1, x2, y2; i <= m; ++i) {
        kin >> x1 >> y1 >> x2 >> y2;
        kout << kD_Tree::query(kD_Tree::root, x1, y1, x2, y2) << '\n';
    }
    return 0;
}