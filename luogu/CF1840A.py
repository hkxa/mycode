T = int(input())
for _ in range(T):
    n = int(input())
    s = input()
    ans = ''
    las = ''
    for c in s:
        if c == las:
            ans += c
            las = ''
        elif not las:
            las = c
    print(ans)