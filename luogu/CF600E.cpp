//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      CF600E Lomsat gelral.
 * @user_id:      63720.
 * @user_name:    brealid.
 * @time:         2020-05-19.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}
// #define int int64
const int N = 100000 + 7, NlogN = 100000 * 20 + 7, MaxCol = 100000;
int n, c[N];
vector<int> G[N];
int root[N];            // 每个节点对应的线段树 root（其实就是其本身）
int l[NlogN], r[NlogN]; // 存储左右子节点
int sum[NlogN];         // 结点掌管区间内的 最多的颜色 的个数
int64 ans[NlogN];       // 结点掌管区间区间内的 最多的颜色的编号 的总和
int cnt = 0;            // 动态开点，当前开的最大点编号
int64 NodeAns[N];       // 每个真实树上节点的 Answer

void pushUp(int u) {
    sum[u] = max(sum[l[u]], sum[r[u]]);
    if (sum[l[u]] == sum[r[u]]) ans[u] = ans[l[u]] + ans[r[u]]; // l[u], r[u] 的 sum 相同，ans 为其和
    else if (sum[l[u]] > sum[r[u]]) ans[u] = ans[l[u]]; // 取 l[u], r[u] 中 sum 较大的一者记录 ans
    else ans[u] = ans[r[u]]; // sum[l[u]] < sum[r[u]] 同上
}

/**
 * u : 线段树节点编号
 * co : 将要修改的 color 值，为这个 color 值加上 1 
 * L,R : 当前线段树节点所掌管的区间(不会用数组记录，随 dfs 而计算)(使用大写是为了与 记录线段树节点左右儿子的数组 区分)
 */
void update(int &u, int co, int L, int R) {
    if (!u) {
        // 访问到空节点(NULL)，但还没完成修改操作，应当新建节点
        // 一个小 trick : u 设为 '&'(引用)，在 update 里修改 u 即可以修改真实的 l[Fa_of_u] / r[Fa_of_u]
        u = ++cnt;
    }
    if (co == L && co == R) {
        // 找到要修改的节点
        // 事实上，对于这道题，由于我们知道 update 的叶子节点一定是刚刚被新建的(因为：每个叶子结点只会被 update 一次)
        // 所以实际上不是修改，就是赋值
        sum[u] ++;
        ans[u] = co;  // 只有颜色 co 的节点答案显然是 co, 赋值是为了防止该节点未被初始化
        return;
    }
    int mid = (L + R) >> 1;
    if (co <= mid) update(l[u], co, L, mid);
    else update(r[u], co, mid + 1, R);
    pushUp(u);
}

/**
 * u,v : 将要合并的两棵线段树节点编号(v 合并到 u)
 * l,r : 当前线段树节点所掌管的区间(不会用数组记录，随 dfs 而计算)
 */
int merge(int u, int v, int L, int R) {
    // 如果两棵线段树中，只有 1 棵有掌管 [L, R] 的节点，那么合并后该节点肯定保持不变
    if (!u) return v; 
    if (!v) return u;

    // 叶子节点的合并
    if (L == R) {
        // 由于两个节点都属于叶子节点，所以无需考虑 ans 不同的情况
        sum[u] += sum[v];
        // fprintf(stderr, "u : {.sum = %d, .ans = %d}\n", sum[u], ans[u]);
        // fprintf(stderr, "v : {.sum = %d, .ans = %d}\n", sum[v], ans[v]);
        return u;
    }

    // 非叶子结点的合并
    int mid = (L + R) >> 1;
    l[u] = merge(l[u], l[v], L, mid);     // 左子树的合并
    r[u] = merge(r[u], r[v], mid + 1, R); // 右子树的合并
    pushUp(u); // 本节点的更新
    return u;
}

void dfs(int u, int fa)
{
    // fprintf(stderr, "dfs(%d, %d)\n", u, fa);
    for (int v : G[u]) {
        if (v != fa) {
            dfs(v, u);
            // fprintf(stderr, "merge(%d, %d, %d, %d)\n", root[u], root[v], 1, MaxCol);
            merge(root[u], root[v], 1, MaxCol);
        }
    }
    // fprintf(stderr, "update(%d, %d, %d, %d)\n", root[u], c[u], 1, MaxCol);
    update(root[u], c[u], 1, MaxCol);
    NodeAns[u] = ans[root[u]];
}

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        c[i] = read<int>();
        root[i] = i; // 尚未初始化的开点；初始化会在 update 内一并完成
    }
    cnt = n; // 已经开了 n 个 空的 root 节点
    for (int i = 1, x, y; i < n; i++) {
        x = read<int>();
        y = read<int>();
        G[x].push_back(y);
        G[y].push_back(x);
    }
    dfs(1, 0);
    for (int i = 1; i <= n; i++) {
        write(NodeAns[i], " \n"[i == n]);
        /**
         * 关于 " \n"[i == n] 的解释：
         * [i == n] 可以看成对字符串的下标访问。
         * 若 i != n，取得是这个字符串下标为 0 的值，即 ' '
         * 若 i == n，取得是这个字符串下标为 1 的值，即 '\n'
         * 从而实现了行中空格，行末换行的需求
         * 等价于 i == n ? '\n' : ' '
         */
    }
    return 0;
}