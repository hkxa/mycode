#include <bits/stdc++.h>
using namespace std;

#define max3(a, b, c) max(max(a, b), c)
#define min3(a, b, c) min(min(a, b), c)
const int N = 1e6 + 7;
int n, m;
struct CardSaver {
    int a, b;
} c[N];

int Lmin[N], Lmax[N], Rmin[N], Rmax[N], P[N];

void init_L() {
    multiset<int> s;
    for (int i = 1; i <= n; ++i) s.insert(c[i].a);
    Lmin[0] = c[1].a, Lmax[0] = c[n].a;
    for (int i = 1; i <= n; ++i) {
        s.erase(c[i].a), s.insert(c[i].b);
        Lmin[i] = *s.begin(), Lmax[i] = *s.rbegin();
    }
}

void init_R() {
    multiset<int> s;
    for (int i = n; i >= 1; --i) s.insert(c[i].a);
    Rmin[n + 1] = c[1].a, Rmax[n + 1] = c[n].a;
    for (int i = n; i >= 1; --i) {
        s.erase(c[i].a), s.insert(c[i].b);
        Rmin[i] = *s.begin(), Rmax[i] = *s.rbegin();
    }
}

void init_P() {
    P[n + 1] = n + 1;
    int p = n;
    for (int i = n; i >= 1; --i) {
        if (Rmax[i] - Rmin[i] < Rmax[p] - Rmin[p]) p = i;
        P[i] = p;
    }
}

int F(int l, int r) {
    if (Lmax[l] == c[n].a) {
        if (Rmin[r] == c[1].a) {
            return Rmax[r] - Lmin[l];
        } else {
            return Rmax[r] - min(Lmin[l], Rmin[r]);
        }
    } else {
        if (Rmin[r] == c[1].a) {
            return max(Lmax[l], Rmax[r]) - Lmin[l];
        } else {
            return max(Lmax[l], Rmax[r]) - min(Lmin[l], Rmin[r]);
        }
    }
}

signed main() {
    ios::sync_with_stdio(false);
    cin >> n >> m;
    for (int i = 1; i <= n; ++i) cin >> c[i].a;
    for (int i = 1; i <= n; ++i) cin >> c[i].b;
    init_L(), init_R(), init_P();
    int ans = 2e9;
    for (int i = 0; i <= m; ++i) ans = min(ans, F(i, P[i + 1 + n - m]));
    cout << ans << endl;
    return 0;
}