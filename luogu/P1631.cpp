/*************************************
 * problem:      P1631 序列合并.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-03-31.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

struct sum {
    int col, num;
    bool operator < (const sum &b) const 
    {
        return num > b.num;
    }

    sum () 
    {

    }

    sum (int Col, int Num) 
    {
        col = Col;
        num = Num;
    }
};

priority_queue <sum> q;
int n;
int a[100007], b[100007];
int pos[100007];

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        a[i] = read<int>();
    }
    for (int i = 1; i <= n; i++) {
        b[i] = read<int>();
    }
    for (int i = 1; i <= n; i++) {
        pos[i] = 1;
        q.push(sum(i, a[i] + b[1]));
    }
    for (int i = 1; i <= n; i++) {
        write(q.top().num, ' ');
        pos[q.top().col]++;
        q.push(sum(q.top().col, a[q.top().col] + b[pos[q.top().col]]));
        q.pop();
    }
    return 0;
}