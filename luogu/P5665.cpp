#include <bits/stdc++.h>
using namespace std;
const long long P = (1 << 30) - 1;

template <typename ret_type> inline ret_type read() {
    register ret_type ret_num = 0; register char ch = getchar();
    while (!isdigit(ch)) ch = getchar(); 
    while (isdigit(ch)) { ret_num = (((ret_num << 2) + ret_num) << 1) + (ch & 15); ch = getchar(); } 
    return ret_num;
}

int n, type;
long long sum[40000007];
int opt[40000007];
int q[40000007];
#define front() (q[head])
#define next() (q[head + 1])
#define back() (q[tail])
#define pop_front() ((++head))
#define pop_back() ((--tail))
#define push_back(x) ((q[++tail] = (x)))

namespace SpecialReadln {
    long long x, y, z, b1, b2, b;
    int m;
    int p[100007], l[100007], r[100007];
    struct s_ManyRead {
        template <typename I> inline s_ManyRead operator >> (I &x) { x = read<I>(); return *this; }
    } ManyRead;
};

#define calc(x) (2 * sum[x] - sum[opt[x]])

namespace Subtask_100pts {
    const long long base = 1000000000LL, width = 9;

    struct Answer_Save {
        long long data[18];
        Answer_Save() { memset(data, 0, sizeof(data)); }
        inline void plusSquare(long long x) {
            register long long hig = x / base, low = x % base;
            register int pos = 1;
            data[3] += hig * hig;
            data[2] += 2 * hig * low;
            data[1] += low * low;
            while (pos <= 3 || data[pos] >= base) {
                data[pos + 1] += data[pos] / base;
                data[pos] %= base;
                pos++;
            }
        }
        inline void putSelf() {
            for (int i = 17; i >= 1; i--) {
                if (data[i]) {
                    printf("%lld", data[i]);
                    while (--i) {
                        printf("%0*lld", width, data[i]);
                    }
                    return;
                }
            }
        }
    };

    void solve() {
        register int head = 1, tail = 1;
        for (register int i = 1; i <= n; i++) {
            while (head < tail && calc(next()) <= sum[i]) pop_front();
            opt[i] = front();
            while (head <= tail && calc(back()) >= calc(i)) pop_back();
            push_back(i);
        }
        register Answer_Save ans; register int pos = n;
        while (pos) {
            ans.plusSquare(sum[pos] - sum[opt[pos]]);
            pos = opt[pos];
        }
        ans.putSelf();
    }
};

int main()
{
    n = read<int>();
    type = read<int>();
    if (!type) for (int i = 1; i <= n; i++) sum[i] = sum[i - 1] + read<int>();
    else {
        using namespace SpecialReadln;
        ManyRead >> x >> y >> z >> b2 >> b1 >> m;
        for (register int i = 1; i <= m; i++) ManyRead >> p[i] >> l[i] >> r[i]; 
        register int j = 1;
        while (1 > p[j]) j++;
        sum[1] = b2 % (r[j] - l[j] + 1) + l[j];
        while (2 > p[j]) j++;
        sum[2] = sum[1] + (b1 % (r[j] - l[j] + 1) + l[j]);
        for (register int i = 3, j = 1; i <= n; i++) {
            b = (x * b1 + y * b2 + z) & P;
            b2 = b1; b1 = b;
            while (i > p[j]) j++; 
            sum[i] = sum[i - 1] + (b % (r[j] - l[j] + 1) + l[j]);
        }
    }
    Subtask_100pts::solve();
    return 0;
}