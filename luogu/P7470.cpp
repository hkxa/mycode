/*************************************
 * @problem:      island.
 * @author:       brealid.
 * @time:         2021-03-27.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int MAXN = 1e5 + 5;

int n, a[MAXN], b[MAXN];
int q, ans[MAXN], ql[MAXN], qr[MAXN], c[MAXN], d[MAXN];

pair<int, int> e[MAXN * 2];
int vals[MAXN], cnt_val;

pair<int, int> ee[MAXN * 3];
int cnt_ee;

int root[MAXN], ch[MAXN * 50][2], sum[MAXN * 50], cnt_node;

int new_node(int copy_from) {
    sum[++cnt_node] = sum[copy_from];
    ch[cnt_node][0] = ch[copy_from][0];
    ch[cnt_node][1] = ch[copy_from][1];
    return cnt_node;
}

void insert1(int &rt, int y, int v) {
    int x = rt = new_node(y);;
    ++sum[x];
    for (int i = 23; i >= 0; --i) {
        int t = (v >> i) & 1;
        ch[x][t] = new_node(ch[y][t]);
        ++sum[ch[x][t]];
        x = ch[x][t];
        y = ch[y][t];
    }
}

int query1(int x, int y, int c, int d) {
    int res = 0;
    for (int i = 23; i >= 0; --i) {
        if (!x) break;
        int cc = (c >> i) & 1;
        int dd = (d >> i) & 1;
        if (dd) res += sum[ch[x][cc]] - sum[ch[y][cc]];
        x = ch[x][cc ^ dd];
        y = ch[y][cc ^ dd];
    }
    res += sum[x] - sum[y];
    return res;
}

void insert2(int x, int v) {
    for (int i = 23; i >= 0; --i) {
        int t = ((v >> i) & 1);
        if (!ch[x][t]) ch[x][t] = new_node(0);
        x = ch[x][t];
    }
}

void make_contribution_2(int x, int c, int d) {
    for (int i = 23; i >= 0; --i) {
        if (!x) break;
        int cc = (c >> i) & 1;
        int dd = (d >> i) & 1;
        if (dd && ch[x][cc]) ++sum[ch[x][cc]];
        x = ch[x][cc ^ dd];
    }
    if (x) ++sum[x];
}

int query2(int x, int v) {
    int res = 0;
    for (int i = 23; i >= 0; --i) {
        res += sum[x];
        x = ch[x][(v >> i) & 1];
    }
    res += sum[x];
    return res;
}

void cdq(int l, int r) {
    if (l == r) return;
    int mid = (l + r) >> 1;
    cdq(l, mid);
    cdq(mid + 1, r);
    
    // 1: b[i] > d[j]
    cnt_val = 0;
    for (int i = mid + 1; i <= r; ++i) {
        if (e[i].second > n) continue;
        vals[++cnt_val] = e[i].second;
    }
    sort(vals + 1, vals + cnt_val + 1);
    
    cnt_node = 0;
    for (int i = 1; i <= cnt_val; ++i) insert1(root[i], root[i - 1], a[vals[i]]);
    for (int i = l; i <= mid; ++i) {
        if (e[i].second <= n) continue;
        int idx = e[i].second - n;
        int L = lower_bound(vals + 1, vals + cnt_val + 1, ql[idx]) - vals;
        int R = upper_bound(vals + 1, vals + cnt_val + 1, qr[idx]) - vals - 1;
        if (L > R) continue;
        ans[idx] += query1(root[R], root[L - 1], c[idx], d[idx]);
    }
    // if (l == 1 && r == n + q) fprintf(stderr, "cnt_node = %d, cnt_ee = %d\n", cnt_node, cnt_ee);
    // if (cnt_node > 50e5 || cnt_ee > 3e5) while(1);

    // CASE2: d[j] >= b[i]
    cnt_node = 0;
    new_node(0); // root
    cnt_ee = 0;
    for (int i = mid + 1; i <= r; ++i) {
        if (e[i].second <= n) continue;
        int idx = e[i].second - n;
        insert2(1, c[idx]);
        
        ee[++cnt_ee] = make_pair(ql[idx] - 1, idx + n);
        ee[++cnt_ee] = make_pair(qr[idx], idx + n + q);
    }
    for (int i = l; i <= mid; ++i) {
        if (e[i].second > n) continue;
        int idx = e[i].second;
        ee[++cnt_ee] = make_pair(idx, idx);
    }
    sort(ee + 1, ee + cnt_ee + 1);
    for (int i = 1; i <= cnt_ee; ++i) {
        if (ee[i].second <= n)
            make_contribution_2(1, a[ee[i].second], b[ee[i].second]);
        else if (ee[i].second <= n + q)
            ans[ee[i].second - n] -= query2(1, c[ee[i].second - n]);
        else
            ans[ee[i].second - n - q] += query2(1, c[ee[i].second - n - q]);
    }
    // if (l == 1 && r == n + q) fprintf(stderr, "cnt_node = %d, cnt_ee = %d\n", cnt_node, cnt_ee);
    // if (cnt_node > 50e5 || cnt_ee > 3e5) while(1);
}

int main() {
    kin >> n >> q;
    for (int i = 1; i <= n; ++i) {
        kin >> a[i] >> b[i];
        e[i] = make_pair(b[i], i);
    }
    for (int i = 1; i <= q; ++i) {
        kin >> ql[i] >> qr[i] >> c[i] >> d[i];
        e[i + n] = make_pair(d[i], i + n);
    }

    sort(e + 1, e + n + q + 1);
    cdq(1, n + q);
    
    for (int i = 1; i <= q; ++i)
        kout << ans[i] << '\n';
    return 0;
}