
// **UnAc** **Passed** **30pts** **brute-force**

//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      P3689 [ZJOI2017]多项式.
 * @user_name:    brealid.
 * @time:         2020-06-06.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 988244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

const uint32 NM = 3.6e6 + 7;

uint32 T;
uint32 n, m, k, l, r;
string str;
bitset<NM> f, t, g;

bitset<NM> operator*(bitset<NM> a, bitset<NM> b) {
    bitset<NM> res;
    // printf("+---------------+\n|   ");
    // for (uint32 i = 0; i <= n * m; i++)
    //     putchar(a.test(n * m - i) | 48);
    // printf("  |\n| * ");
    // for (uint32 i = 0; i <= n * m; i++)
    //     putchar(b.test(n * m - i) | 48);
    // printf("  |\n| ------------- |\n| = ");
	for (uint32 i = b._Find_first(); i != b.size(); i = b._Find_next(i)) {
        // printf("i : %d\n", i);
        res ^= (a << i);
    }
    // for (uint32 i = 0; i <= n * m; i++)
    //     putchar(res.test(n * m - i) | 48);
    // printf("  |\n+---------------+\n");
    return res;
}

signed main() {
    T = read<uint32>();
    while (T--) {
        n = read<uint32>();
        m = read<uint32>();
        k = read<uint32>();
        r = n * m + 1 - read<uint32>();
        l = n * m + 1 - read<uint32>();
        cin >> str; f = (bitset<NM>)(str);
        cin >> str; t = (bitset<NM>)(str);
        if (str.size() > r - l + 1) {
            puts("0");
            continue;
        }
        uint32 temp = m;
        g.reset(); g.set(0);
        while (temp) {
            if (temp & 1) g = g * f;
            f = f * f;
            temp >>= 1;
        }
        // for (uint32 i = 0; i <= n * m; i++)
        //     putchar(g.test(n * m - i) | 48);
        // putchar(10);
        uint32 cnt = 0, siz = str.size(), now = 0, Tnum = 0, Mask = (1 << siz) - 1;
        for (uint32 i = 0; i < siz; i++) 
            Tnum = Tnum << 1 | t.test(i);
        for (uint32 i = l; i < l + siz; i++) 
            now = now << 1 | g.test(i);
        if (now == Tnum) cnt++;
        // printf("%d~%d : %d\n", l, l + siz - 1, now);
        for (uint32 i = l + siz; i <= r; i++) {
            now = now << 1 | g.test(i);
            now &= Mask;
            // printf("%d~%d : %d\n", i - siz + 1, i, now);
            if (now == Tnum) cnt++;
        }
        write(cnt, 10);
    }
    return 0;
}

// Create File Date : 2020-06-06