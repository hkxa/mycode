/*************************************
 * problem:      P2014 选课.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-05-04.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

int n, m;
vector<int> G[307];
typedef vector<int>::iterator vii;
// Vector<Int>::Iterator
int val[307], dp[307][307];

void dfs(int uRoot, int tot)
{
    if (tot <= 0) return;
    for (vii it = G[uRoot].begin(); it != G[uRoot].end(); it++) {
        for (int k = 0; k < tot; ++k) { 
            dp[*it][k] = dp[uRoot][k] + val[*it];
        }
        dfs(*it, tot - 1);
        for (int k = 1; k <= tot; ++k) { 
            dp[uRoot][k] = max(dp[uRoot][k], dp[*it][k - 1]);
        }
    }
}

int main()
{
    n = read<int>();
    m = read<int>();
    for (int i = 1; i <= n; i++) {
        G[read<int>()].push_back(i);
        val[i] = read<int>();
    }
    dfs(0, m);
    write(dp[0][m]);
    return 0;
} 