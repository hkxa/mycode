/*************************************
 * problem:      P2158 [SDOI2008]仪仗队.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-05-02.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  
long long n, ans = 0;
long long e[40001];

int main()
{
    n = read<int>();
    if (n == 1) {
        puts("0");
        return 0;
    }
    for (int i = 1; i <= n; i++) {
        e[i] = i;
    }
    for (int i = 2; i <= n; i++) {
        if (e[i] == i) {
            for (int j = i; j <= n; j += i) {
                e[j] = e[j] / i * (i - 1);
            }
        }
    }
    for (int i = 2; i < n; i++) {
        ans += e[i];
    }
    write((ans << 1) + 3, 10);
    return 0;
}