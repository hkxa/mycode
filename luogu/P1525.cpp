#include <cstdio>
#include <algorithm>
#include <vector>
#include <cstring>
#define maxn 20005
using namespace std;
struct data {
    int a, b, c;
};
int n, m;
vector<int> g[maxn], w[maxn];
int vis[maxn] = { 0 };
int q[maxn];
int front, rear;
bool BFS(int s, int m) {
    front = rear = 1;
    q[rear++] = s;
    vis[s] = 1;
    while (front != rear) {
        int i = q[front];
        front++;
        for (int k = 0; k < g[i].size(); k++) {
            if (w[i][k] > m) {
                int j = g[i][k];
                if (vis[i] == vis[j])
                    return 0;
                if (vis[j] == 0) {
                    q[rear++] = j;
                    vis[j] = 3 - vis[i];
                }
            }
        }
    }
    return 1;
}
bool check(int t) {
    memset(vis, 0, sizeof(vis));
    for (int i = 1; i <= n; i++) {
        if (vis[i] == 0) {
            bool ok = BFS(i, t);
            if (!ok)
                return false;
        }
    }
    return true;
}
int main() {
    // freopen("in.txt","r",stdin);
    scanf("%d%d", &n, &m);
    int x, y, z;
    for (int i = 1; i <= m; i++) {
        scanf("%d%d%d", &x, &y, &z);
        g[x].push_back(y);
        g[y].push_back(x);

        w[x].push_back(z);
        w[y].push_back(z);
    }

    int A = 0, B = 1000000000, ans;
    while (A <= B) {
        int C = (A + B) / 2;
        int ok = check(C);
        if (ok)
            B = C - 1, ans = C;
        else
            A = C + 1;
    }
    printf("%d\n", ans);
    return 0;
}