/**
 * P2482 [SDOI2010]猪国杀
 * Writer: U63720 航空信奥
 */
#include <bits/stdc++.h>
using namespace std;

#define OUTPUT_MESSAGE

#ifdef OUTPUT_MESSAGE
# define message(...) printf(__VA_ARGS__)
#else
# define message(...) do { if(0) printf(__VA_ARGS__); } while (0)
#endif

#define HP_MAX 4

#define ID_MP           'M' // 主猪
#define ID_ZP           'Z' // 忠猪
#define ID_FP           'F' // 反猪
#define ID_FPlike       'L' // 类反猪
#define ID_NAN          'N' // 未明身份

#define CARD_peach      'P' // 桃
#define CARD_attack     'K' // 杀
#define CARD_defense    'D' // 闪

#define CARD_duel       'F' // 决斗
#define CARD_invade     'N' // 南猪入侵
#define CARD_arrow      'W' // 万箭齐发
#define CARD_avoid      'J' // 无懈可击

#define CARD_again      'Z' // 猪哥连弩

string del(string a, int pos)
{
    return a.substr(0, pos) + a.substr(pos + 1, a.length() - pos - 1);
}

string getInStr()
{
    string res;
    cin >> res;
    return res;
}

#define getInID()   (getInStr()[0])
#define getInCard() (getInStr()[0])

#define KIND_MP 1           // MP, ZP
#define KIND_FP 2           // FP, FPlike
#define KIND_NP 0           // NAN

int matchKinds(char id)
{
    if (id == ID_MP || id == ID_ZP) return KIND_MP;
    if (id == ID_FP || id == ID_FPlike) return KIND_FP;
    return KIND_NP;
}

struct cardList {
    queue<char> q;
    bool isEnd;

    cardList() : isEnd(0) {}

    void getInList(int m) 
    {
        while (m--) {
            q.push(getInCard());
        }
    }

    char get()
    {
        if (isEnd) {
            return q.front();
        } 
        char res = q.front();
        q.pop();
        if (q.empty()) {
            q.push(res);
            isEnd = 1;
        }
        return res;
    }
} cards;

int n, m;

int MPcnt = 0, FPcnt = 0;
#define ended() MPcnt == 0 || FPcnt == 0

struct pig {
    int hp;
    char identity, surface;
    bool againAttack;
    bool isAttacked;
    int nextPig;
    string card;
    pig();                      // 构造函数
    void getCard();             // 摸牌(1张)
    bool getLeftCard();         // 找到左边第一张可出牌并出牌
    bool isCouldAttack();       // 检查是否可以使用杀
    int  isCouldDuel();         // 检查是否可以决斗 (可以返回目标 id, 失败返回 0)
    void attack();              // 杀
    bool findCard(char cardID)  // 查找并出 cardID 号牌
    bool defense();             // 闪 (成功返回 1, 失败返回 0)
    void duel(int object);      // 决斗
    void invade();              // 南猪入侵
    void arrow();               // 万箭齐发
    void nearDie();             // 濒临死亡
} player[12];

pig::pig()
{
    hp = HP_MAX;
    againAttack = isAttacked = false;
}

void pig::getCard()
{
    card += cards.get();
}

bool pig::getLeftCard()
{
    for (int i = 0; i < card.length(); i++) {
        int pos;
        switch (card[i]) {
            case CARD_peach:
                if (hp < HP_MAX) {
                    del(card, i);
                    hp++;
                    return true;
                }
                break;
            case CARD_attack:
                if (isCouldAttack()) {
                    del(card, i);
                    attack();
                    return true;
                }
                break;
            case CARD_defense:
                break;
            case CARD_duel:
                if (pos = isCouldDuel()) {
                    del(card, i);
                    duel(pos);
                    return true;
                }
                break;
            case CARD_invade:
                del(card, i);
                invade();
                return true;
            case CARD_arrow:
                del(card, i);
                arrow();
                return true;
            case CARD_avoid:
                break;
            case CARD_again:
                if (!againAttack) {
                    del(card, i);
                    againAttack = true;
                    return true;
                }
                break;
        }
    }
}

bool pig::isCouldAttack()
{
    if (isAttacked && !againAttack) return false;
    if (matchKinds(identity) == matchKinds(player[nextPig].surface)) return false;
    if (matchKinds(player[nextPig].surface) == 0) return false;
    switch (identity) {
        case ID_MP:
            surface = ID_MP;
            break;
        case ID_FP:
            surface = ID_FP;
            break;
        default:
            break;
    }
    return true;
}

int pig::isCouldDuel()
{
    int p = nextPig;
    if (identity == ID_FP) {
        surface = ID_FP;
        return 1;
    }
    while (player[p].nextPig != nextPig) {
        switch (identity) {
            case ID_MP:
                if (matchKinds(player[p].surface) == KIND_FP) {
                    return p;
                }
                break;
            case ID_ZP:
                if (matchKinds(player[p].surface) == ID_FP) {
                    surface = ID_ZP;
                    return p;
                }
                break;
            case ID_FP:
                if (matchKinds(player[p].surface) == ID_MP) {
                    surface = ID_FP;
                    return p;
                }
                break;
        }
    }
    return false;
}

void pig::attack()
{
    if (player[nextPig].defense()) return;
    player[nextPig].hp--;
    if (player[nextPig].hp == 0) {
        player[nextPig].nearDie();
        if (player[nextPig].hp == 0) {
            switch (player[nextPig].identity) {
                case ID_MP :
                    quitGame();
                    break;
                case ID_ZP :
                    MPcnt--;
                    if (identity == ID_MP) {
                        card = "";
                        againAttack = 0;
                    }
                    break;
                case ID_FP :
                    FPcnt--;
                    if (FPcnt == 0) {
                        quitGame();
                        break;
                    }
                    getCard();
                    getCard();
                    getCard();
                    break;
            }
        }
    }
}

bool pig::findCard(char cardID)
{
    for (int i = 0; i < card.length(); i++) {
        if (card[i] == cardID) {
            del(card, i);
            return 1;
        }
    }
    return 0;
}

bool pig::defense()
{
    for (int i = 0; i < card.length(); i++) {
        if (card[i] == CARD_defense) {
            del(card, i);
            return 1;
        }
    }
    return 0;
}

void pig::duel(int object)
{
    if (player[object].identity == ID_ZP && identity == ID_MP) {
        if (player[object].findCard(CARD_avoid)) {
            player[object].surface = ID_ZP;
            return;
        }
    }
}

void pig::invade()
{

}

void pig::arrow()
{

}

void pig::nearDie();
{

}

int main()
{
    ios::sync_with_stdio(0);
    scanf("%d%d", &n, &m);
    for (int i = 1; i <= n; i++) {
        player[i].identity = getInID();
        if (player[i].identity == ID_MP) player[i].surface = ID_MP;
        else player[i].surface = ID_NAN;
        if (player[i].identity == ID_FP) FPcnt++;
        else MPcnt++;
        for (int j = 1; j <= 4; j++) {
            player[i].card += getInCard();
        }
        player[i].nextPig = i + 1;
    }
    player[n].nextPig = 1;
    cards.getInList(m);
    for (int p = 1; !ended(); p = player[p].nextPig) {
        player[p].getCard();
        player[p].getCard();
        while (player[p].getLeftCard());
    }
    return 0;
}
