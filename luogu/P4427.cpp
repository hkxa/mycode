/*************************************
 * @problem:      P3258 [JLOI2014]松鼠的新家.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-01-10.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define P 998244353

int n;
const int Log = 19;
long long dep[300007][50], sum[300007][50];
int fa[300007][Log + 2];
vector<int> G[300007];

void dfs_LCA(int u, int faNode)
{
    dep[u][0] = dep[faNode][0] + 1;
    sum[u][0] = (sum[faNode][0] + dep[u][0]) % P;
    for (int i = 1; i < 50; i++) {
        dep[u][i] = dep[u][i - 1] * dep[u][0] % P;
        sum[u][i] = (sum[faNode][i] + dep[u][i]) % P;
    }
    fa[u][0] = faNode;
    for (int k = 1; k <= Log; k++) {
        fa[u][k] = fa[fa[u][k - 1]][k - 1];
    }
    for (unsigned i = 0; i < G[u].size(); i++) 
        if (G[u][i] != faNode) dfs_LCA(G[u][i], u);
}

int jump(int u, int depth)
{
    for (int i = 0; i <= Log; i++) {
        if (depth & (1 << i)) u = fa[u][i];
    }
    return u;
}

int get_LCA(int u, int v)
{
    if (dep[u][0] < dep[v][0]) swap(u, v);
    u = jump(u, dep[u][0] - dep[v][0]);
    // printf("u = %d, v = %d.\n", u, v);
    for (int k = Log; k >= 0; k--) {
        if (fa[u][k] != fa[v][k]) {
            u = fa[u][k];
            v = fa[v][k];
        }
    }
    return u != v ? fa[u][0] : u;
}

int main()
{
    n = read<int>();
    for (int i = 1, u, v; i < n; i++) {
        u = read<int>();
        v = read<int>();
        G[u].push_back(v);
        G[v].push_back(u);
    }
    dep[0][0] = -1;
    dfs_LCA(1, 0);
    // for (int i = 1; i <= n; i++) {
    //     printf("%d: %d %d [fa = %d]\n", i, dep[i][0], sum[i][0], fa[i][0]);
    // }
    int q = read<int>();
    for (int i = 1, u, v, k, lca; i <= q; i++) {
        u = read<int>();
        v = read<int>();
        k = read<int>();
        lca = get_LCA(u, v);
        // printf("%d, %d's lca = %d.\n", u, v, lca);
        // printf("%d %d %d %d\n", sum[u][k - 1], sum[v][k - 1], sum[lca][k - 1], sum[fa[lca][0]][k - 1]);
        write(((sum[u][k - 1] + sum[v][k - 1] - sum[lca][k - 1] - sum[fa[lca][0]][k - 1]) % P + P) % P, 10);
    }
    return 0;
}