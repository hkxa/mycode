/*
	Problem: P1600 天天爱跑步
	Author: 航空信奥 
	Date: 2018/07/28 09:26
	Description: 一个恶心的，困难的问题，咩~咩~咩~~~
*/

// 注：个人习惯，数组元素下标一般从1开始 
#pragma GCC optimize("O1")
#pragma GCC optimize("O2")
#pragma GCC optimize("O3")
// 不要管我，我就要皮，o1o2o3齐开，咩~~~ 
#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <vector>
#include <map>
#define Char_Int(a) ((a) & 15)
#define Int_Char(a) ((a) + '0')
#define rg register

namespace hkxa {
	template <typename _TpInt> inline _TpInt read();
	template <typename _TpInt> inline void write(_TpInt x);
	template <typename _TpSwap> inline void swap(_TpSwap &x, _TpSwap &y);
	
#	define SizeN 300007
#	define SizeLogN 22
#	define tong(a) bucket[a + SizeN]
	
	int n, m;
	int bucket[SizeN * 3]; 
	/* 使用 tong(a) 来读取桶 bucket 里面的数据，防止越界，因为在数组“桶”的使
	   用过程中，下标有可能成为负数。 */
	std::vector<int> lcafrom[SizeN * 2], tofrom[SizeN * 2], lcato[SizeN * 2];
	int roadcount[SizeN * 2];
	/* lcafrom	[x]:	以x为LCA的起点集合 
	   tofrom	[x]:	以x为终点的起点集合 
	   lcato	[x]:	以x为LCA的终点集合  
	   roadcount[x]:	以x为起点的路径条数 */
	int w[SizeN], ans[SizeN];
	/* w  [x]:	观察员x观察的时间节点
	   ans[x]:	观察员x观察到的玩家人数 */ 
	int f[SizeN][SizeLogN + 1], deep[SizeN], dist[SizeN];
	bool use[SizeN] = {0};
	/* LCA用品 : f 程序算法用品 : deep, dist 
	   f : 不用说了吧，大家都懂，倍增求LCA的父亲数组 
	   deep : 每个节点的深度
	   dist : 距离 */
	
	struct EDGE { // 存储边信息
		int e; 
		// 通往e的道路 
		EDGE *next_edge;
		// 下一条边的指针 
		EDGE() : e(0), next_edge(NULL) {}
		// 初始化
	} *v[SizeN], edges[SizeN * 2];
	int ct_edges = 0;
	/* v数组存储了每个点最后的连接的边(指针)，edges数组是树上边的集合，ct_edges 
	   是edges的top（即：上次在edges数组的ct_edges位置添加了边） */ 
	   
	struct Person {
		int s, t;
		int lca;
		int dis;
	} p[SizeN];
	/* 存储人的信息
	   s, t: 如题意
	   lca : s, t的公共祖先LCA 
	   dis : s, t之间的最短路距离 */
	
	inline void Add_edge(int s, int e) // 添加一条从s到e的边 
	{
		ct_edges++;  
		edges[ct_edges].next_edge = v[s];
		v[s] = edges + ct_edges; // 地址赋值 
		v[s]->e = e; 
	}
	
	inline void link(int x, int y) // 添加一条x与y之间的双向边
	{
		Add_edge(x, y);
		Add_edge(y, x);
	} 
	
	void dfs_LCA(int now, int depth) // 倍增求LCA的预处理函数 
	{
	    use[now] = true;
	    deep[now] = depth;
	    for (rg int k = 1; k <= SizeLogN; k++){
	        int j = f[now][k - 1];
	        f[now][k] = f[j][k - 1];
	    }
	    for (rg EDGE *nxt = v[now]; nxt; nxt = nxt->next_edge) {
	    /* 这一行等价于 for (EDGE *nxt = v[now]; nxt != NULL; nxt = nxt->next),
		   C++里非0为真（NULL = 0） */ 
	    	if(!use[nxt->e]) {
			   f[nxt->e][0] = now;
			   dist[nxt->e] = dist[now] + 1;
			   dfs_LCA(nxt->e, depth + 1);
			}
		}
	    use[now] = false;
	} 
	
	inline int jump(int u, int depth) {
	    for (rg int k = 0; k <= SizeLogN; k++) {
	    	if ((depth & (1 << k))) u = f[u][k];
		}
	    return u;
	}
	
	inline int LCA(int u, int v){
	    if (deep[u] < deep[v]) swap(u, v);
	    u = jump(u, deep[u] - deep[v]);
	    for (rg int k = SizeLogN; k >= 0; k--) {
	    	if (f[u][k] != f[v][k]) u = f[u][k], v = f[v][k]; // 倍增，一跃而上 
		}
	    return u == v ? u : f[u][0];
	}
	
	inline void dfs_fromLCA(int now)  // 从from到LCA的路线 
	{
	    use[now] = true; // 打上tag
	    int prev = tong(deep[now] + w[now]);
	    for (rg EDGE *g = v[now]; g; g = g->next_edge) {
	        if (!use[g->e]) dfs_fromLCA(g->e);
		}
	    tong(deep[now]) += roadcount[now];
	    ans[now] += tong(deep[now] + w[now]) - prev;
	    int len = lcafrom[now].size();
	    for (rg int k = 0; k < len; k++) {
	    	tong(deep[lcafrom[now][k]])--;
		}
	    use[now] = false; // 删除tag
	}
	
	inline void dfs_LCAto(int now) // 从LCA到to的路线 
	{
	    use[now] = true; // 打上tag
	    int prev = tong(w[now] - deep[now]);
	    for (rg EDGE *g = v[now]; g; g = g->next_edge) {
	    	if(!use[g->e]) dfs_LCAto(g->e);	
		}
	    int len = tofrom[now].size();
	    for (rg int k = 0; k < len; k++) {
	    	tong(tofrom[now][k])++;
		}
	    ans[now] += tong(w[now] - deep[now]) - prev;
	    len = lcato[now].size();
	    for (rg int k = 0; k < len; k++) {
	       tong(lcato[now][k])--;
		}
	    use[now] = false;
	}
	
	inline int main()
	{	
	    n = read<int>();
	    m = read<int>();
	    for (rg int i = 1; i <= n - 1; i++) {
	    	link(read<int>(), read<int>());
		}
		for (rg int i = 1; i <= n; i++)
			w[i] = read<int>(); 
		f[1][0] = 1;
		dfs_LCA(1, 0); // 倍增求LCA的预处理 
		int S, T; 
	    for(rg int i = 1; i <= m; i++) { // 核心算法之预处理
	        S = read<int>();  
	        T = read<int>();
	        p[i].s = S;
	        p[i].t = T;
	        p[i].lca = LCA(S, T);
	        p[i].dis = dist[S] + dist[T] - dist[p[i].lca] * 2;
	        roadcount[S]++;
	        lcafrom[p[i].lca].push_back(S);
	        tofrom[T].push_back(p[i].dis - deep[T]);
	        lcato[p[i].lca].push_back(p[i].dis - deep[T]);
	    }
	    // 核心算法 - 开始 
	    dfs_fromLCA(1);      // 从下至上（从from到LCA） 
	    dfs_LCAto(1);        // 从上至下（从LCA到to） 
	    for (rg int i = 1; i <= m; i++) {
	    	if(deep[p[i].s] == deep[p[i].lca] + w[p[i].lca]) {
				ans[p[i].lca]--;
			}
		} 
	    for (rg int i = 1; i <= n; i++) {
	    	write<int>(ans[i]);
			putchar(32); 
		}
	    return 0;
	} 
	
	template <typename _TpInt>
	inline _TpInt read()       
	{
	    register _TpInt flag = 1;
	    register char c = getchar();
	    while ((c > '9' || c < '0') && c != '-') 
			c = getchar();
	    if (c == '-') flag = -1, c = getchar();
	    register _TpInt init = Char_Int(c);
	    while ((c = getchar()) <= '9' && c >= '0') 
			init = (init << 3) + (init << 1) + Char_Int(c);
	    return init * flag;
	}
	
	template <typename _TpInt>
	inline void write(_TpInt x)
	{
	    if (x < 0) {
	    	putchar('-');
			write<_TpInt>(~x + 1);
		}
		else {
			if (x > 9) write<_TpInt>(x / 10);	
			putchar(Int_Char(x % 10));
		}
	}
	
	template <typename _TpSwap>
	inline void swap(_TpSwap &x, _TpSwap &y)
	{
	    _TpSwap t = x;
	    x = y;
	    y = t;
	}
}

int main()
{
//	system("ulimit -s 1048576"); 
    hkxa::main();
    return 0;
}