//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      【模板】Prufer 序列.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-31.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 5e6 + 7;

namespace against_cpp11 {
    int n, m;
    int a[N], deg[N], f[N];
    // vector<int> G[N];
    int64 ans = 0;
    void TranslateToPrufer() {
        for (int i = 1; i < n; i++) {
            read >> a[i];
            deg[i]++; deg[a[i]]++;
        }
        int cnt = 0;
        for (int u = 1, t; u <= n; u++) {
            if (deg[u] == 1) {
                deg[u] = 0;
                if (cnt < n - 2) ans ^= (int64)++cnt * a[u];
                t = a[u];
                while (--deg[t] == 1 && t && t < u) {
                    deg[t] = 0;
                    if (cnt < n - 2) ans ^= (int64)++cnt * (t = a[t]);
                }
            }
        }
        write << ans << '\n';
    }
    void TranslateFromPrufer() {
        for (int i = 1; i <= n - 1; i++) {
            if (i == n - 1) a[i] = n;
            else read >> a[i];
            deg[a[i]]++;
        }
        for (int i = 1, j = 1; i <= n; i++, j++) {
            while (deg[j]) ++j; 
            f[j] = a[i];
            // printf("set f[%d] to %d (op-1)\n", j, a[i]);
            while (i < n && !--deg[a[i]] && a[i] < j) f[a[i]] = a[i + 1], ++i;
        }
        for (int i = 1; i < n; i++) ans ^= (int64)i * f[i];
        // for (int i = 1; i < n; i++) write << f[i] << " \n"[i == n - 1]; 
        write << ans << '\n';
    }
    signed main() {
        read >> n >> m;
        if (m == 1) TranslateToPrufer();
        else TranslateFromPrufer();
        return 0;
    }
}

signed main() { return against_cpp11::main(); }