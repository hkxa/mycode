/*************************************
 * @contest:      Codeforces Round #622 (Div. 2).
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-02-23.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;
int m[500007];
int Most;
int64 Max, Now, Ans;

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) m[i] = read<int>();
    for (int i = 1; i <= n; i++) {
        Now = m[i];
        Most = m[i];
        for (int j = i - 1; j >= 1; j--) Now += (Most = min(Most, m[j]));
        Most = m[i];
        for (int j = i + 1; j <= n; j++) Now += (Most = min(Most, m[j]));
        if (Now > Max) {
            Max = Now;
            Ans = i;
        } 
    }
    for (int j = Ans - 1; j >= 1; j--) m[j] = min(m[j], m[j + 1]);
    for (int j = Ans + 1; j <= n; j++) m[j] = min(m[j], m[j - 1]);
    for (int i = 1; i <= n; i++) write(m[i], 32);
    putchar(10);
    return 0;
}