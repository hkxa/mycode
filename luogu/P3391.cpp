//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      P3391 【模板】文艺平衡树.
 * @user_id:      63720.
 * @user_name:    brealid.
 * @time:         2020-05-25.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int N = 100007;

struct node {
    node *fa, *son[2];
    int val, siz, cnt;
    bool rev;
    inline void clear() {
        fa = son[0] = son[1] = NULL;
        val = siz = cnt = 0;
        rev = 0;
    }
    node() {
        clear();
    }
    inline bool which(node *UnrecognizeNode) {
        return son[1] == UnrecognizeNode;
    }
    inline void pushUp() {
        siz = cnt + son[0]->siz + son[1]->siz;
    }
    int getSiz() {
        if (this == NULL) return 0;
        else return siz;
    }
    void pushRev() {
        if (rev) {
            swap(son[0], son[1]);
            son[0]->rev ^= 1;
            son[1]->rev ^= 1;
            rev = 0;
        }
    }
};

struct MemoryPool {
    node UnusedNode[N];
    node *RecyclePool[N]; 
    int cntU, cntR;
    MemoryPool() : cntU(0), cntR(0) {}
    node *GetMem() {
        if (cntR) return RecyclePool[cntR--];
        else return &UnusedNode[cntU++];
    }
    node *Get() {
        node *ret = GetMem();
        ret->fa = ret->son[0] = ret->son[1] = &UnusedNode[0];
        return ret;
    }
    void Recycle(node *trash) {
        trash->clear();
        RecyclePool[++cntR] = trash;
    }
} pool;

node *nullNode = pool.Get(), *root = nullNode;

int n, m;
int opt, x;

// void PreorderTraversal(node* u, int dep = 0) {
//     printf("| ");
//     for (int i = 1; i <= dep; i++) printf("  ");
//     if (u == nullNode) {
//         printf("(NULL)\n");
//         return;
//     }
//     printf("v(%d) siz(%d) cnt(%d) l(%d) r(%d) fa(%d)\n", u->val, u->getSiz(), u->cnt, u->son[0], u->son[1], u->fa);
//     u->pushRev();
//     PreorderTraversal(u->son[0], dep + 1);
//     PreorderTraversal(u->son[1], dep + 1);
// }

void MidorderTraversal(node* u, int rev = 0) {
    if (u == nullNode) return;
    // u->pushRev();
    rev ^= u->rev;
    MidorderTraversal(u->son[rev], rev);
    if (u->val > 0 && u->val <= n) write(u->val, 32);
    MidorderTraversal(u->son[rev ^ 1], rev);
}

void GetAns_MidorderTraversal(node* u) {
    if (u == nullNode) return;
    u->pushRev();
    // rev ^= u->rev;
    GetAns_MidorderTraversal(u->son[0]);
    if (u->val > 0 && u->val <= n) write(u->val, 32);
    GetAns_MidorderTraversal(u->son[1]);
}

void Rotate(node *a, node *&k = nullNode) {
    // printf("Rotate(%d, %d)...\n", a, k);
    node *f = a->fa, *g = f->fa; 
    // printf("f v(%d) l(%d) r(%d) fa(%d) rev(%d)\n", f->val, f->son[0], f->son[1], f->fa, f->rev);
    // printf("g v(%d) l(%d) r(%d) fa(%d) rev(%d)\n", g->val, g->son[0], g->son[1], g->fa, g->rev);
    // printf("pushRev... g. ");
    // g->pushRev();
    // printf("f. ");
    // f->pushRev();
    // printf("a. ");
    // a->pushRev();
    int d = f->which(a);
    // printf("S1... ");
    a->son[d ^ 1]->fa = f;      // Step 1
    f->son[d] = a->son[d ^ 1];  // Step 1
    // printf("S2... ");
    a->fa = g;                  // Step 2
    g->son[g->which(f)] = a;    // Step 2
    // printf("S3... ");
    f->fa = a;                  // Step 3
    a->son[d ^ 1] = f;          // Step 3
    // printf("pushUp... ");
    f->pushUp();
    a->pushUp();
    if (f == k) k = a; 
    // printf("Finished!\n");
    /// @b OriginalNode:a reach @b GoalNode:k
}

// let the node a Rotate to k
inline void Splay(node *a, node *&k) {
    // printf("Splay...\n");
    for (node *f = a->fa; a != k; f = a->fa) {
        // printf("Splay %d %d\n", a, k);
        if (f != k) Rotate(f->which(a) ^ f->fa->which(f) ? a : f, k);
        // PreorderTraversal(root);
        Rotate(a, k);
        // PreorderTraversal(root);
    }
}

inline void Splay(int v, node *&k) {
    Splay(&pool.UnusedNode[v + 1], k);
}

void Insert(int v, node *&u, node *fa = nullNode) {
    // printf("Ins(%d)\n", v);
    if (u != nullNode) u->siz++;
    if (u == nullNode) {
        // printf("Find Node *\n");
        u = pool.Get();
        u->fa = fa;
        u->val = v;
        u->cnt = 1;
        u->siz = 1;
        // printf("Ready to Splay...\n");
        Splay(u, root);
        // printf("Splay Succeed!\n");
    } else if (v == u->val) {
        u->cnt++;
        Splay(u, root);
    } else if (v < u->val) {
        Insert(v, u->son[0], u);
    } else {
        Insert(v, u->son[1], u);
    }
}

int GetVal(int rank) {
    node *u = root;
    while (true) {
        // printf("v(%d) l(%d) r(%d)\n", u->val, u->son[0], u->son[1]);
        u->pushRev();
        if (u->son[0]->getSiz() == rank) return u->val;
        else if (rank <= u->son[0]->getSiz()) u = u->son[0];
        else rank -= u->son[0]->getSiz() + u->cnt, u = u->son[1];
    }
}

int main()
{
    n = read<int>();
    m = read<int>();
    for (int i = 0; i <= n + 2; i++) Insert(i, root);
    for (int i = 1, l, r; i <= m; i++) {
        l = read<int>();
        r = read<int>();
        Splay(GetVal(l), root);
        // root->pushRev();
        // printf("Step 50%%\n");
        Splay(GetVal(r + 2), root->son[1]);
        // root->pushRev();
        root->son[1]->son[0]->rev ^= 1;
        // PreorderTraversal(root);
        // MidorderTraversal(root, 0);
        // putchar(10);
    }
    // PreorderTraversal(root);
    // GetAns_MidorderTraversal(root);
    // putchar(10);
    for (int i = 1; i <= n; i++) write(GetVal(i + 1) - 1, i == n ? 10 : 32);
    return 0;
}