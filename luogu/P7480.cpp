/*************************************
 * @contest:      【LGR-084】洛谷 4 月月赛.
 * @author:       brealid.
 * @time:         2021-04-05.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 1e5 + 7;

struct OilGain {
    int x, p;
    bool operator < (const OilGain &b) const { if (p == b.p) return x < b.x; return p > b.p; }
} og[N];

int n, s, t;
// set<OilGain> s;

namespace val {
    int a[N << 2], cnt;
    inline void add(int x) { a[++cnt] = x; }
    inline int q(int x) { return lower_bound(a + 1, a + cnt + 1, x) - a; }
    void doit() {
        sort(a + 1, a + cnt + 1);
        cnt = unique(a + 1, a + cnt + 1) - a - 1;
    }
}

namespace tr {
    struct func {
        int64 k, b;
        func() : b(1000000000000000000LL) {}
        func(int64 K, int64 B) : k(K), b(B) {}
        int64 calc(int x) { return k * val::a[x] + b; }
    } tr[N << 5];
    void insert(int u, int l, int r, int ml, int mr, func x) {
        if (l >= ml && r <= mr) {
            int mid = (l + r) >> 1;
            if (tr[u].calc(mid) > x.calc(mid)) swap(tr[u], x);
            if (l == r || x.b == 1000000000000000000LL) return;
            if (x.k > tr[u].k) insert(u << 1, l, mid, ml, mr, x);
            else insert(u << 1 | 1, mid + 1, r, ml, mr, x);
            return;
        }
        int mid = (l + r) >> 1;
        if (mid >= ml) insert(u << 1, l, mid, ml, mr, x);
        if (mid < mr) insert(u << 1 | 1, mid + 1, r, ml, mr, x);
    }
    int64 query(int u, int l, int r, int pos) {
        if (l == r) return tr[u].calc(pos);
        int mid = (l + r) >> 1;
        return min(tr[u].calc(pos), pos <= mid ? query(u << 1, l, mid, pos) : query(u << 1 | 1, mid + 1, r, pos));
    }
}

signed main() {
    // freopen("in.txt", "r", stdin);
    kin >> n >> s >> t;
    // val::add(s), val::add(t), val::add(-s), val::add(-t);
    for (int i = 1; i <= n; ++i) {
        kin >> og[i].p >> og[i].x;
        // val::add(og[i].x), val::add(-og[i].x), val::add(og[i].p), val::add(-og[i].p);
        val::add(og[i].x);
    }
    val::doit();
    sort(og + 1, og + n + 1);
    int64 ans = 1000000000000000000LL;
    // if (n <= 5000) {
    //     int64 f[5007];
    //     memset(f, 0x3f, sizeof(f));
    //     for (int i = 1; i <= n; ++i) {
    //         if (og[i].x == s) f[i] = 0;
    //         ans = min(ans, f[i] + (int64)og[i].p * abs(og[i].x - t));
    //         for (int j = 1; j <= n; ++j)
    //             f[j] = min(f[j], f[i] + (int64)og[i].p * abs(og[i].x - og[j].x));
    //     }
    //     kout << ans << '\n';
    //     return 0;
    // }
    for (int i = 1; i <= n; ++i) {
        int wh = val::q(og[i].x);
        int64 cost = (og[i].x == s) ? 0 : tr::query(1, 1, val::cnt, wh);
        // printf("at (x=%d, p=%d): cost %lld\n", og[i].x, og[i].p, cost);
        ans = min(ans, cost + (int64)og[i].p * abs(og[i].x - t));
        // if (wh > val::cnt || wh < 1) return 0;
        tr::insert(1, 1, val::cnt, 1, wh, tr::func(-og[i].p, (int64)og[i].p * og[i].x + cost));
        tr::insert(1, 1, val::cnt, wh, val::cnt, tr::func(og[i].p, (int64)og[i].p * -og[i].x + cost));
    }
    kout << ans << '\n';
    return 0;
}