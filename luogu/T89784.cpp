/*************************************
 * problem:      id_name.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-mm-dd.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int calcReadln()
{
	int lv, score;
	lv = read<int>();
	score = read<int>();
	return (lv + 1) * score / 400000;
}

char getOp()
{
	char buf[4];
	scanf("%s", buf);
	return buf[0];
}

int main()
{
	int n = read<int>();
	priority_queue<int, vector<int>, greater<int> > mx;
	int sum(0), cnt(0);
	int s;
	while (n--) {
		switch (getOp())
		{
			case 'P':
				s = calcReadln();
				if (cnt < 20) {
					mx.push(s);
					sum += s;
					cnt++;
				} else if (s > mx.top()) {
					sum += s - mx.top();
					mx.pop();
					mx.push(sum);
				}
				break;
			case 'Q':
				write(sum, 10);
				break;
			default:
				break;
		}
	}
}

/*
40
P 4 8417571
P 4 8417571
P 4 8417571
P 4 8417571
P 4 8417571
P 4 8417571
P 4 8417571
P 4 8417571
P 4 8417571
P 4 8417571
P 4 8417571
P 4 8417571
P 4 8417571
P 4 8417571
P 4 8417571
P 4 8417571
P 4 8417571
P 4 8417571
P 4 8417571
P 4 8417571
Q
P 4 8417571
P 4 8417571
P 4 8417571
P 4 8417571
P 4 8417571
P 4 8417571
P 4 8417571
P 4 8417571
P 4 8417571
P 4 8417571
Q
P 5 8580237
Q
P 5 8580237
Q
P 5 8580237
P 5 8580237
P 5 8580237
Q*/