//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      P4035 [JSOI2008]球形空间产生器.
 * @user_name:    brealid.
 * @time:         2020-06-07.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 988244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

int n;
struct pos {
    double x[10];
    pos operator + (const pos b) const {
        pos ret;
        for (int i = 0; i < n; i++)
            ret.x[i] = x[i] + b.x[i];
        return ret;
    }
    pos operator - (const pos b) const {
        pos ret;
        for (int i = 0; i < n; i++)
            ret.x[i] = x[i] - b.x[i];
        return ret;
    }
    pos operator * (const double b) const {
        pos ret;
        for (int i = 0; i < n; i++)
            ret.x[i] = x[i] * b;
        return ret;
    }
} a[11];

inline int64 randl() {
    return ((rand() & 1) << 30) | (rand() << 15) | rand();
}

inline double randR() {
    return (double)(randl() ^ randl() ^ randl()) / INT_MAX;
}

inline bool accept(double per) {
    return randR() > per;
}

double dist(pos a, pos b) {
    double ret = 0;
    for (int i = 0; i < n; i++) {
        ret += (a.x[i] - b.x[i]) * (a.x[i] - b.x[i]);
    }
    return sqrt(ret);
}

pos SA(double startT, double eps, double lowerT) {
    double t = startT;
    pos ans, tmp;
    for (int i = 0; i < n; i++)
        ans.x[i] = 0;
    double sum[11] = {0};
    for (int i = 0; i <= n; i++) {
        for (int j = 0; j < n; j++) {
            sum[j] += a[i].x[j];
        }
    }
    for (int i = 0; i < n; i++) {
        ans.x[i] = sum[i] / n;
    }
    double ret[11], average = 0;
    while (t > eps) {
        // if (randR() < 0.004) {
        //     printf("t = %.7lf, ans = { ", t);
        //     for (int i = 0; i < n; i++)
        //         printf("%.3lf ", ans.x[i]);
        //     printf("}\n");
        // }
        average = 0;
        for (int i = 0; i <= n; i++) {
            ret[i] = dist(ans, a[i]);
            average += ret[i];
        }
        average /= (n + 1);
        for (int i = 0; i < n; i++) tmp.x[i] = 0;
        for (int i = 0; i <= n; i++)
            tmp = tmp + (a[i] - ans) * (ret[i] - average);
        for (int i = 0; i < n; i++) ans.x[i] += tmp.x[i] * t;
        t *= lowerT;
    }
    return ans;
}

signed main() {
    srand(time(0) ^ 20170933);
    n = read<int>();
    for (int i = 0; i <= n; i++)
        for (int j = 0; j < n; j++)
            scanf("%lf", a[i].x + j);
    pos ans = SA(1e-3, 1e-5, 0.99995);
    for (int i = 0; i < n; i++)
        printf("%.3lf ", ans.x[i]);
    putchar(10);
    return 0;
}

// Create File Date : 2020-06-07