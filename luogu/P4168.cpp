//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      [Violet 6]蒲公英.
 * @user_name:    brealid/hkxadpall/jmoo/jomoo/zhaoyi20/航空信奥/littleTortoise.
 * @time:         2020-06-10.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// #define int int64

const int N = 100000 + 7, SqrtN = 4000 + 7;

int n, Q, bl, blc;
int a[N];
int Lsh[N];
int mode[SqrtN][SqrtN];
int cnt[N];
vector<int> occur[N];

#define block(v) ((v) / bl)
#define st(v)    max((v) * bl, 1)
#define ed(v)    min(((v) + 1) * bl - 1, n)
#define len(v)   (ed(v) - st(v) + 1)

inline int findOccur(int val, int l, int r) {
    return lower_bound(&occur[val][0], &occur[val][0] + occur[val].size(), r + 1) -
           lower_bound(&occur[val][0], &occur[val][0] + occur[val].size(), l); 
}

inline void UpdateMode(int &pos, int val) {
    if (cnt[val] > cnt[pos] || (cnt[val] == cnt[pos] && val < pos)) pos = val;
}

inline pair<int, int> QueryViolent(int l, int r, int ml, int mr) {
    int ret = n + 1, retOc = -1, oc;
    for (int i = l; i <= r; i++) {
        oc = findOccur(a[i], ml, mr);
        if (oc > retOc || (oc == retOc && a[i] < ret)) {
            ret = a[i];
            retOc = oc;
        }
    }
    return make_pair(ret, retOc);
}

inline pair<int, int> Merge(pair<int, int> a, pair<int, int> b) {
    if (a.second ^ b.second) return a.second > b.second ? a : b;
    else return a.first < b.first ? a : b;
}

signed main() {
    n = read<int>();
    Q = read<int>();
    bl = pow(n * log2(n), 0.255);
    // bl = 200;
    /*
    block -O2 i5 win10 64bit
    120 1.82
    130 2.07
    140 1.90
    150 2.26
    160 2.58
    170 2.49
    180 2.52
    190 2.48
    200 2.38
    220 3.42
    250 3.28
    300 3.56
    */
    blc = block(n);
    for (register int i = 1; i <= n; i++) {
        Lsh[i] = a[i] = read<int>();
    }
    sort(Lsh + 1, Lsh + n + 1);
    for (register int i = 1; i <= n; i++) {
        a[i] = lower_bound(Lsh + 1, Lsh + n + 1, a[i]) - Lsh;
        occur[a[i]].push_back(i);
    }
    for (register int i = 1; i <= n; i++) sort(occur[i].begin(), occur[i].end());
    for (register int i = 0; i <= blc; i++) {
        memset(cnt, 0, sizeof(cnt));
        for (register int j = i; j <= blc; j++) {
            if (j != i) mode[i][j] = mode[i][j - 1];
            for (register int k = st(j), ek = ed(j); k <= ek; k++) {
                cnt[a[k]]++;
                UpdateMode(mode[i][j], a[k]);
            }
            // printf("mode[%d][%d] = %d\n", i, j, Lsh[mode[i][j]]);
        }
    }
    pair<int, int> ans;
    for (int i = 1, l, r, b1, b2, x = 0; i <= Q; i++) {
        l = (read<int>() + x - 1) % n + 1;
        r = (read<int>() + x - 1) % n + 1;
        if (l > r) swap(l, r);
        b1 = block(l);
        b2 = block(r);
        if (b1 == b2) ans = QueryViolent(l, r, l, r);
        else {
            ans = Merge(QueryViolent(l, ed(b1), l, r), QueryViolent(st(b2), r, l, r));
            if (b1 < b2 - 1)    
                ans = Merge(ans, make_pair(mode[b1 + 1][b2 - 1], findOccur(mode[b1 + 1][b2 - 1], l, r)));
        }
        // Lsh[ans.first];
        write(x = Lsh[ans.first], 10);
    }
    return 0;
}

// Create File Date : 2020-06-10

/*
7
7 3 5 1 2 7 3
*/