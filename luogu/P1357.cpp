/*************************************
 * @problem:      P1357 花园.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2019-11-16.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define P 1000000007

int64 n;
int32 m, k;
bool status_ok[64] = {0};
int64 mat[64][64];

int count(int x)
{
    int cnt = 0;
    while (x) {
        if (x & 1) cnt++;
        x >>= 1;
    }
    return cnt;
}

void mul(int64 save[64][64], int64 a[64][64], int64 b[64][64])
{
    for (int i = 0; i < (1 << m); i++) {
        for (int j = 0; j < (1 << m); j++) {
            save[i][j] = 0;
        }
    }
    for (int i = 0; i < (1 << m); i++) {
        for (int j = 0; j < (1 << m); j++) {
            for (int k = 0; k < (1 << m); k++) {
                save[i][j] = (save[i][j] + a[i][k] * b[k][j]) % P;
            }
        }
    }
}

void mul_with(int64 a[64][64], int64 b[64][64])
{
    static int64 tmp[64][64];
    mul(tmp, a, b);
    for (int i = 0; i < (1 << m); i++) {
        for (int j = 0; j < (1 << m); j++) {
            a[i][j] = tmp[i][j];
        }
    }
}

void prepare_matrix()
{
    const int getM1bit = (1 << (m - 1)) - 1;
    for (int i = 0; i < (1 << m); i++)
        if (count(i) <= k) status_ok[i] = 1;
    for (int i = 0; i < (1 << m); i++) {
        if (status_ok[i]) {
            for (int j = 0; j < 2; j++) {
                if (status_ok[(i & getM1bit) << 1 | j]) mat[i][(i & getM1bit) << 1 | j] = 1;
            }
        }
    }
    // printf("The Prepared Matrix : \n");
    // for (int i = 0; i < (1 << m); i++) {
    //     for (int j = 0; j < (1 << m); j++) {
    //         printf("%lld ", mat[i][j]);
    //     }
    //     putchar(10);
    // }
}

int64 quickPow(int64 k)
{
    int64 res[64][64];
    for (int i = 0; i < (1 << m); i++) {
        for (int j = 0; j < (1 << m); j++) {
            res[i][j] = 0;
        }
    }
    for (int i = 0; i < (1 << m); i++) res[i][i] = 1;
    while (k) {
        if (k & 1) mul_with(res, mat);
        mul_with(mat, mat);
        k >>= 1;
    }
    // printf("The Result Matrix : \n");
    // for (int i = 0; i < (1 << m); i++) {
    //     for (int j = 0; j < (1 << m); j++) {
    //         printf("%lld ", res[i][j]);
    //     }
    //     putchar(10);
    // }
    int64 ret = 0;
    for (int i = 0; i < (1 << m); i++) if (status_ok[i]) ret = (ret + res[i][i]) % P;
    return ret;
}

int main()
{
    n = read<int64>();
    m = read<int32>();
    k = read<int32>();
    prepare_matrix();
    write(quickPow(n), 10);
    return 0;
}