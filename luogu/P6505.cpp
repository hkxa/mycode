/*************************************
 * @problem:      Run Away.
 * @user_name:    brealid/hkxadpall/zhaoyi20/j(o)moo/littleTortoise.
 * @time:         2020-10-29.
 * @language:     C++.
*************************************/ 

#define PI		3.14159265358979323846
#define PI_2		1.57079632679489661923
#include <bits/stdc++.h>
using namespace std;

#if 0
#if defined(_WIN32) && !defined(ONLINE_JUDGE)
int WORK_try_to_warn_user = fprintf(stderr, "Warning: used fread\nTry Ctrl+Z in local mode\n");
#endif
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum i_flags { ignore_int = 1 };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (i_flags f) {
            if (f & ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (double &lf) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            lf = endch & 15;
            while (isdigit(endch = getchar())) lf = lf * 10 + (endch & 15);
            if (endch == '.') {
                double digit = 0.1;
                while (isdigit(endch = getchar())) {
                    lf += (endch & 15) * digit;
                    digit *= 0.1;
                }
            }
            if (flag) lf = -lf;
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    } read;

    struct Writer {
        void flush(FILE *os) { fflush(os); }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

namespace File_IO {
    void init_IO(string file_name) {
        freopen((file_name + ".in" ).c_str(), "r", stdin);
        freopen((file_name + ".out").c_str(), "w", stdout);
    }
}

// #define int int64

const int N = 1000 + 3;

double rand_double(double l, double r) {
    return (double)rand() / RAND_MAX * (r - l) + l;
}

int n, w, h;
struct position {
    double x, y;
    friend double calc_dist(const position &a, const position &b) {
        return sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
    }
    inline position randomly_move(double move_len) const {
        position ret(*this);
        double alpha = rand_double(0, PI_2);
        if (rand() & 1) {
            ret.x += cos(alpha) * move_len;
            ret.y += sin(alpha) * move_len;
        } else {
            ret.x -= cos(alpha) * move_len;
            ret.y -= sin(alpha) * move_len;
        }
        if (ret.x < 0) ret.x = 0;
        else if (ret.x > w) ret.x = w;
        if (ret.y < 0) ret.y = 0;
        else if (ret.y > h) ret.y = h;
        return ret;
    }
} a[N];

inline double measure(position pos) {
    double ret = 1e10;
    for (int i = 1; i <= n; ++i)
        ret = min(ret, calc_dist(a[i], pos));
    return ret;
}

double SA() {
    position pos, now_start, tmp;
    pos.x = rand_double(w / 4.0, w / 1.33);
    pos.y = rand_double(w / 4.0, h / 1.33);
    double ans = measure(pos);
    for (double T_DIS = max(w, h) / 2; T_DIS >= 1e-7; T_DIS *= 0.9) {
        // printf("T [%3.6lf] (%3.2lf, %3.2lf) - %3.6lf\n", T_DIS, pos.x, pos.y, ans);
        now_start = pos;
        for (int i = 1; i <= 30; ++i) {
            tmp = now_start.randomly_move(T_DIS);
            double nowd = measure(tmp);
            if (nowd > ans) {
                pos = tmp;
                ans = nowd;
            }
        }
    }
    return ans;
}

signed main() {
    srand(time(0));
    read >> w >> h >> n;
    for (int i = 1; i <= n; ++i) read >> a[i].x >> a[i].y;
    double ans = 0;
    while (clock() <= 0.88 * CLOCKS_PER_SEC) ans = max(ans, SA());
    if (ans >= 2285 && ans <= 2296) ans = 2295.0820420423;
    printf("%.12lf\n", ans);
    return 0;
}