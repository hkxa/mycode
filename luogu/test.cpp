#include <bits/stdc++.h>
using namespace std;
#define random(x) (rand() % (x) + 1)
#define randLR(l, r) (random((r) - (l) + 1) + (l) - 1)
#define prob_name "SP31"

vector<int> WAlist;

int main() 
{
    int n;
    printf("Input Data Count : ");
    scanf("%d", &n);
    system("g++ " prob_name ".cpp -o " prob_name);
    system("g++ " prob_name "-std.cpp -o " prob_name "-std");
    system("g++ " prob_name "-maker.cpp -o " prob_name "-maker");
    for (int i = 1; i <= n; i++) {
        printf("testing data #%d... ", i);
        system(prob_name "-maker");
        printf("1 ... ");
        system(prob_name " <" prob_name ".in >" prob_name ".out");
        printf("2 ... ");
        system(prob_name "-std <" prob_name ".in >" prob_name ".ans");
        printf("3 ... \n");
        if (system("fc " prob_name ".out " prob_name ".ans")) {
            char buf[104];
            if (!WAlist.size()) system("mkdir " prob_name "_WAdata");
            sprintf(buf, "copy " prob_name ".in " prob_name "_WAdata\\" prob_name "_%03d.in", i);
            system(buf);
            WAlist.push_back(i);
            printf("WA\n");
        } else {
            printf("AC\n");
        }
    }
    if (WAlist.size()) {
        printf("Wrong Answer Points %.2lf\n", (double)(n - WAlist.size()) * 100 / n);
        printf("WA in %d", WAlist[0]);
        for (unsigned i = 1; i < WAlist.size(); i++) printf(", %d", WAlist[i]);
        printf("\n");
    } else {
        printf("ALL Accepted\n");
    }
    system("DEL " prob_name ".exe " prob_name "-std.exe " prob_name "-maker.exe");
    system("DEL " prob_name ".in " prob_name ".ans " prob_name ".out");
    return 0;
}