/*************************************
 * @problem:      [GZOI2017]小z玩游戏.
 * @author:       brealid.
 * @time:         2020-11-28.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;
#define getchar() (i_P1 == i_P2 && (i_P2 = (i_P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), i_P1 == i_P2) ? EOF : *i_P1++)
char BUF[1 << 21], *i_P1 = BUF, *i_P2 = BUF;
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 1e5 + 7;

bool IsntPrime[N];
int prime[N], pcnt;
int n, w[N], e[N];

struct edge {
    int to, nxt;
    inline void set_v(const int &v, const int &nx) {
        to = v, nxt = nx;
    }
} adj[N * 30];
int head[N], ecnt;

inline void add_edge(const int &u, const int &v) {
    adj[++ecnt].set_v(v, head[u]);
    head[u] = ecnt;
}

int dfn[N], low[N], dft;
int fam[N], fam_cnt;
bool instack[N];
int sta[N], top;

void tarjan(int u) {
    low[u] = dfn[u] = ++dft;
    sta[++top] = u;
    instack[u] = true;
    for (int i = head[u]; i; i = adj[i].nxt) {
        int v = adj[i].to;
        if (!dfn[v]) {
            tarjan(v);
            low[u] = min(low[u], low[v]);
        } else if (instack[v])
            low[u] = min(low[u], dfn[v]);
    }
    if (dfn[u] == low[u]) {
        ++fam_cnt;
        while (low[sta[top]] >= low[u]) {
            instack[sta[top]] = false;
            fam[sta[top--]] = fam_cnt;
        }
    }
}

void init(int mx) {
    for (int i = 1; i <= mx; ++i)
        for (int j = i + i; j <= mx; j += i)
            add_edge(i, j);
}

signed main() {
    for (int T = kin.get<int>(), mx_value(0), n; T--;) {
        memset(head + 1, 0, sizeof(int) * mx_value);
        memset(dfn + 1, 0, sizeof(int) * mx_value);
        ecnt = fam_cnt = dft = 0;
        kin >> n;
        for (int i = 1; i <= n; ++i) kin >> w[i];
        for (int i = 1; i <= n; ++i) {
            kin >> e[i];
            add_edge(w[i], e[i]);
        }
        mx_value = max(*max_element(w + 1, w + n + 1), *max_element(e + 1, e + n + 1));
        init(mx_value);
        tarjan(1);
        int ans(0);
        for (int i = 1; i <= n; ++i)
            if (fam[w[i]] == fam[e[i]]) ++ans;
        kout << ans << '\n';
    }
}