//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      【模板】三维偏序（陌上花开）.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-17.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 998244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? Module(w - P) : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

const int N = 100003;

int n;
struct D {
    int v, x, y, z, pos;
    bool operator < (const D &b) const {
        return v < b.v;
    }
} x[N];
bitset<N> a[N], t;
int ans[N];

#define ReadData(Dx)            \
for (int i = 1; i <= n; i++) {  \
    Dx[i].x = read<int>();      \
    Dx[i].y = read<int>();      \
    Dx[i].z = read<int>();      \
    Dx[i].pos = i;              \
}

#define SetData(Dx, Th)         \
for (int i = 1; i <= n; i++) {  \
    Dx[i].v = Dx[i].Th;         \
}

#define GenClac(Ds)                                     \
t.reset();                                              \
sort(Ds + 1, Ds + n + 1);                               \
for (int i = 1, j; i <= n; ) {                          \
    t.set(Ds[i].pos);                                   \
    j = i;                                              \
    while (Ds[i].v == Ds[j + 1].v) {                    \
        j++;                                            \
        t.set(Ds[j].pos);                               \
    }                                                   \
    while (i <= j) a[Ds[i++].pos] &= t;                 \
}

signed main() {
    n = read<int>(); read<int>();
    for (int i = 1; i <= n; i++) a[i].set();
    ReadData(x);
    SetData(x, x);
    GenClac(x);
    SetData(x, y);
    GenClac(x);
    SetData(x, z);
    GenClac(x);
    // ---- Get Answer ----
    for (int i = 1; i <= n; i++)
        ans[a[i].count()]++;
    for (int i = 1; i <= n; i++)
        write(ans[i], 10);
    return 0;
}

// Create File Date : 2020-06-17