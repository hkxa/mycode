/*************************************
 * problem:      P1041.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-07-10.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 



#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Float>
inline void writef(Float x)
{
    write((long long)x);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

int n, p;
vector<int> connect[307];
vector<int> son[307];
vector<int> brother[307];
typedef vector<int>::iterator Vec_it;
bool icts[307] = {0}; // IsCouldNotSick

void buildUp(int u, int depth = 1, int fa = 0)
{
    brother[depth].push_back(u);
    for (Vec_it it = connect[u].begin(); it != connect[u].end(); it++) {
        if (*it != fa) {
            son[u].push_back(*it);
            // printf("%d 's father is %d.\n", *it, u);
            buildUp(*it, depth + 1, u);
        }
    }
}

int cnt = 0;
void cns(int u) // CouldNotSick
{
    for (Vec_it it = son[u].begin(); it != son[u].end(); it++) {
        icts[*it] = 1;
        cnt++;
        cns(*it);
    }
}

void cs(int u) // CouldSick
{
    for (Vec_it it = son[u].begin(); it != son[u].end(); it++) {
        icts[*it] = 0;
        // cnt++;
        cs(*it);
    }
}

int ans = 0;
void dfs(int depth, int safe)
{
    bool visd = 0;
    if (brother[depth].size() == 0) {
        // printf("F%d : empty floor.\n", depth);
        if (safe > ans) ans = safe;
        return;
    }
    for (Vec_it it = brother[depth].begin(); it != brother[depth].end(); it++) {
        if (!icts[*it]) {
            // printf("F%d : %d is cutting...\n", depth, *it);
            cnt = 1;
            cns(*it);
            // printf("F%d : save %d person(s)!\n", depth, cnt);
            dfs(depth + 1, safe + cnt);
            visd = 1;
            cs(*it);
        }
    }
    if (!visd) dfs(depth + 1, safe);
}

int main()
{
    n = read<int>();
    p = read<int>();
    int x, y;
    for (int i = 1; i <= p; i++) {
        x = read<int>();
        y = read<int>();
        connect[x].push_back(y);
        connect[y].push_back(x);
    }
    buildUp(1);
    dfs(2, 0);
    write(n - ans, 10);
    return 0;
}