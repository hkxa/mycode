/*************************************
 * problem:      P5462 X龙珠.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-06.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

int n;
int a[100007], pre[100007], nxt[100007], pos[100007];

#define NO_MAN -1
#define DEAD -2

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        a[i] = read<int>();
        pos[a[i]] = i;
        pre[i] = i - 1;
        nxt[i] = i + 1;
    }
    pre[1] = NO_MAN;
    nxt[n] = NO_MAN;
    for(int i = n; i >= 1; i--) {
        if (pos[i] != DEAD && nxt[pos[i]] != NO_MAN) {
            printf("%d %d ", i, a[nxt[pos[i]]]);
            nxt[pre[pos[i]]] = nxt[nxt[pos[i]]];
            pre[nxt[nxt[pos[i]]]] = pre[pos[i]];
            // printf("%d, %d => DEAD\n", pos[i], pos[a[nxt[pos[i]]]]);
            pos[a[nxt[pos[i]]]] = DEAD;
            pos[i] = DEAD;
        }
    }
    return 0;
}
