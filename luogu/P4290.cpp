/*************************************
 * @problem:      id_name.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-mm-dd.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int turn(char ch)
{
	switch (ch) {
		case 'W' : return 0;
		case 'I' : return 1;
		case 'N' : return 2;
		case 'G' : return 3;
		default : return -1;
	}
}

int n;
char s[203];
bool could[4][4][4], f[203][203][4];

void readln()
{
	int W = read<int>(), I = read<int>(), N = read<int>(), G = read<int>();
	while (W--) {
		scanf("%s", s);
		could[turn(s[0])][turn(s[1])][0] = true;
	}
	while (I--) {
		scanf("%s", s);
		could[turn(s[0])][turn(s[1])][1] = true;
	}
	while (N--) {
		scanf("%s", s);
		could[turn(s[0])][turn(s[1])][2] = true;
	}
	while (G--) {
		scanf("%s", s);
		could[turn(s[0])][turn(s[1])][3] = true;
	}
	scanf("%s", s + 1);
	n = strlen(s + 1);
}

int main()
{
	readln();
	for (int i = 1; i <= n; i++) f[i][i][turn(s[i])] = true;
	for (int l = 2; l <= n; l++) 
		for (int i = 1, j = l; j <= n; i++, j++) 
			for (int op = 0, exist; op < 4; op++) {
				exist = 0;
//				printf("try:f[%d][%d][%d]\n", i, j, op);
				for (int k = i + 1; k <= j && !exist; k++) {
					for (int o1 = 0; o1 < 4 && !exist; o1++) {
						for (int o2 = 0; o2 < 4 && !exist; o2++) {
							f[i][j][op] |= f[i][k - 1][o1] && f[k][j][o2] && 
										   could[o1][o2][op];
							if (f[i][j][op]) {
								exist = true;//printf("exist:f[%d][%d][%d]\n", i, j, op);
							}
						}
					}
				}
//				f[i][j][op] |= f[i]
			}
	bool haveAns = 0;
	if (f[1][n][0]) { haveAns = 1; putchar('W'); }
	if (f[1][n][1]) { haveAns = 1; putchar('I'); }
	if (f[1][n][2]) { haveAns = 1; putchar('N'); }
	if (f[1][n][3]) { haveAns = 1; putchar('G'); }
	if (!haveAns) puts("The name is wrong!");
    return 0;
}
