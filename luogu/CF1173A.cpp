#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

int a, b, c;

int main()
{
    a = read<int>();
    b = read<int>();
    c = read<int>();
    if (b + c < a) {
        putchar('+');
        return 0;
    } else if (a + c < b) {
        putchar('-');
        return 0;
    } else if (c == 0 && a == b) {
        putchar('0');
        return 0;
    } else {
        putchar('?');
        return 0;
    }
}