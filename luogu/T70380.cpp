/*************************************
 * problem:      T70380 [LnOI2019]���������̶�.
 * user ID:      63720.
 * user name:    �����Ű�.
 * time:         2019-03-10.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

double P0;
int n, k;

int main()
{
	scanf("%ld%d%d", &P0, &n, &k);
	
	return 0;
} 
