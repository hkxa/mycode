/*************************************
 * problem:      P2265 路边的水沟.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-21.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define puts_return(x) { puts(x); return 0; }
#define write_return(x) { write(x); return 0; }

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, m;

int64 fexp(int64 a, int64 n, int64 P, int64 baseTimeser = 1)
{
    while (n) {
        if (n & 1) baseTimeser = (baseTimeser * a) % P;
        a = (a * a) % P;
        n >>= 1;
    }
    return baseTimeser;
}

#define inv(x, P_mod) fexp(x, P_mod - 2, P_mod)

int64 fac_save[2000007];
int64 fac(int a)
{
    if (fac_save[a] != -1) return fac_save[a];
    else return fac_save[a] = (fac(a - 1) * a % 1000000007);
}

int64 C(int n, int m, int P)
{
    m = min(n - m, m);
    int64 Molecules = fac(n), Denominators = fac(m) * fac(n - m) % P;
    return Molecules * inv(Denominators, P) % P;
}

int main()
{
    memset(fac_save, -1, sizeof(fac_save));
    fac_save[1] = fac_save[0] = 1;
    n = read<int>();
    m = read<int>();
    write(C(n + m, n, 1000000007), 10);
    return 0;
}