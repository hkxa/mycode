//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      P5327 [ZJOI2019]语言.
 * @user_id:      63720.
 * @user_name:    brealid.
 * @time:         2020-05-20.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
typedef long long            int64;

#if true
#define getchar() (i_P1 == i_P2 && (i_P2 = (i_P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), i_P1 == i_P2) ? EOF : *i_P1++)
char BUF[1 << 21], *i_P1 = BUF, *i_P2 = BUF;
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;
// #define int int64
const int N = 100000 + 7, NlogN = 100000 * 300 + 7;
int n, m;
vector<int> G[N];
queue<pair<int, int> > upd[N]; // 需要 upd 的组合
int root[N];                // 每个节点对应的线段树 root（其实就是其本身）
int l[NlogN], r[NlogN];     // 存储左右子节点
int val[NlogN];             // 结点掌管区间内的差分结果
int sum[NlogN];             // 结点掌管区间内的 Answer
int mn[NlogN], mx[NlogN];   // 结点掌管区间区间内的 最小值 & 最大值
int cnt = 0;                // 动态开点，当前开的最大点编号
int64 ans;                  // 总的 Answer

// 树链剖分预备
int dfn[N], idfn[N], dft;
int f[N], beg[N], wson[N], siz[N], dep[N];

// 树链剖分 dfs - 1 : get siz, f
void dfs1(int u, int fa) {
    siz[u] = 1;
    f[u] = fa;
    for (unsigned i = 0; i < G[u].size(); i++) {
        if (G[u][i] != fa) {
            dfs1(G[u][i], u);
            siz[u] += siz[G[u][i]];
            if (siz[G[u][i]] > siz[wson[u]]) 
                wson[u] = G[u][i];
        }
    }
}

// 树链剖分 dfs - 2 : get dfn, idfn, beg, dep
void dfs2(int u, int chainBegin) {
    idfn[dfn[u] = ++dft] = u;
    beg[u] = chainBegin;
    dep[u] = dep[f[u]] + 1;
    if (wson[u]) dfs2(wson[u], chainBegin);
    for (unsigned i = 0; i < G[u].size(); i++) {
        if (G[u][i] != f[u] && G[u][i] != wson[u]) {
            dfs2(G[u][i], G[u][i]);
        }
    }
}

inline int LCA(int u, int v) {
    while (beg[u] != beg[v]) {
        if (dep[beg[u]] < dep[beg[v]]) swap(u, v);
        u = f[beg[u]];
    }
    if (dep[u] < dep[v]) return u;
    else return v;
}

inline int Dis(int u, int v) {
    if (!u || !v) return 0;
    u = idfn[u]; v = idfn[v];
    return dep[u] + dep[v] - dep[LCA(u, v)] * 2;
}

#define MnMxDis(u) Dis(mn[u], mx[u])

inline void pushUp(int u) {
    mn[u] = mn[l[u]] ? mn[l[u]] : mn[r[u]];
    mx[u] = mx[r[u]] ? mx[r[u]] : mx[l[u]];
    sum[u] = sum[l[u]] + sum[r[u]] + Dis(mx[l[u]], mn[r[u]]) + MnMxDis(u) - MnMxDis(l[u]) - MnMxDis(r[u]);
}

/**
 * u : 线段树节点编号
 * co : 将要修改的 color 值，为这个 color 值加上 dif
 * dif : △修改值
 * L,R : 当前线段树节点所掌管的区间(不会用数组记录，随 dfs 而计算)(使用大写是为了与 记录线段树节点左右儿子的数组 区分)
 */
void update(int &u, int co, int dif, int L, int R) {
    if (!u) {
        // 访问到空节点(NULL)，但还没完成修改操作，应当新建节点
        // 一个小 trick : u 设为 '&'(引用)，在 update 里修改 u 即可以修改真实的 l[Fa_of_u] / r[Fa_of_u]
        u = ++cnt;
    }
    if (co == L && co == R) {
        // 找到要修改的节点并修改
        val[u] += dif;
        if (val[u]) mn[u] = mx[u] = L;
        else mn[u] = mx[u] = 0;
        // mn[u] = val[u] ? L : 0;
        // mx[u] = val[u] ? L : 0;
        return;
    }
    int mid = (L + R) >> 1;
    if (co <= mid) update(l[u], co, dif, L, mid);
    else update(r[u], co, dif, mid + 1, R);
    pushUp(u);
}

/**
 * u,v : 将要合并的两棵线段树节点编号(v 合并到 u)
 * l,r : 当前线段树节点所掌管的区间(不会用数组记录，随 dfs 而计算)
 */
void merge(int &u, int v, int L, int R) {
    // 如果两棵线段树中，只有 1 棵有掌管 [L, R] 的节点，那么合并后该节点肯定保持不变
    if (!u || !v) {
        u |= v;
        return;
    }

    // 叶子节点的合并
    if (L == R) {
        // 由于两个节点都属于叶子节点，所以无需考虑 ans 不同的情况
        val[u] += val[v];
        if (val[u]) mn[u] = mx[u] = L;
        else mn[u] = mx[u] = 0;
        // mn[u] = val[u] ? L : 0;
        // mx[u] = val[u] ? L : 0;
        // fprintf(stderr, "u : {.sum = %d, .ans = %d}\n", sum[u], ans[u]);
        // fprintf(stderr, "v : {.sum = %d, .ans = %d}\n", sum[v], ans[v]);
        return;
    }

    // 非叶子结点的合并
    int mid = (L + R) >> 1;
    merge(l[u], l[v], L, mid);     // 左子树的合并
    merge(r[u], r[v], mid + 1, R); // 右子树的合并
    pushUp(u); // 本节点的更新
}

void dfs(int u)
{
    for (unsigned i = 0; i < G[u].size(); i++) {
        if (G[u][i] != f[u]) {
            dfs(G[u][i]);
            merge(root[u], root[G[u][i]], 1, n);
        }
    }
    while (!upd[u].empty()) {
        update(root[u], upd[u].front().first, upd[u].front().second, 1, n);
        upd[u].pop();
    }
    // printf("sum(node = %d) = %d\n", root[u], sum[root[u]] / 2);
    ans += sum[root[u]] / 2;
}

int main()
{
    kin >> n >> m;
    for (int i = 1; i <= n; i++) 
        root[i] = i; // 尚未初始化的开点；初始化会在 update 内一并完成
    cnt = n; // 已经开了 n 个 空的 root 节点
    for (int i = 1, x, y; i < n; i++) {
        kin >> x >> y;
        G[x].push_back(y);
        G[y].push_back(x);
    }
    dfs1(1, 0);
    dfs2(1, 1);
    for (register int i = 1, x, y, l; i <= m; i++) {
        kin >> x >> y;
        l = LCA(x, y);
        upd[x].push(make_pair(dfn[x], 1));
        upd[x].push(make_pair(dfn[y], 1));
        upd[y].push(make_pair(dfn[x], 1));
        upd[y].push(make_pair(dfn[y], 1));
        upd[l].push(make_pair(dfn[x], -1));
        upd[l].push(make_pair(dfn[y], -1));
        upd[f[l]].push(make_pair(dfn[x], -1));
        upd[f[l]].push(make_pair(dfn[y], -1));
    }
    dfs(1);
    kout << (ans >> 1) << '\n';
    return 0;
}

/*
Data 
Input:
7 3
1 7
1 2
2 3
2 4
2 5
5 6
6 7
3 1
4 5
Output:
14
*/