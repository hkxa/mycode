/*************************************
 * @problem:      「联合省选 2020 B」消息传递.
 * @author:       brealid.
 * @time:         2021-04-04.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int MAXN = 4e5 + 7;

int T, n, m;
int head[MAXN], nxt[MAXN], to[MAXN], ecnt;
bool vis[MAXN];
int ans[MAXN];

inline void add_edge(int x, int y) {
	nxt[++ecnt] = head[x];
	to[ecnt] = y;
	head[x] = ecnt;
}
int siz[MAXN], max_part[MAXN], rt, SUM;
inline void getroot(int u, int F) {
	siz[u] = max_part[u] = 1;
	for (int i = head[u]; i; i = nxt[i]) {
		int v = to[i];
		if (v == F || vis[v]) continue;
		getroot(v, u);
		siz[u] += siz[v];
		if (siz[v] > max_part[u]) max_part[u] = siz[v];
	}
	if (SUM - siz[u] > max_part[u]) max_part[u] = SUM - siz[u];
	if (max_part[u] < max_part[rt]) rt = u;
}

vector<pair<int, int> > qry[MAXN];
int bucket[MAXN], dep[MAXN];

inline void getdis(int u, int F) {
	++bucket[dep[u]];
	for (int i = head[u]; i; i = nxt[i]) {
		int v = to[i];
		if (v == F || vis[v]) continue;
		dep[v] = dep[u] + 1;
		getdis(v, u);
	}
}

inline void getans(int u, int F) {
	for (int i = 0, S = qry[u].size(); i < S; ++i) {
		int k = qry[u][i].first - dep[u];
		if (k < 0) continue;
		ans[qry[u][i].second] += bucket[k];
	}
	for (int i = head[u]; i; i = nxt[i]) {
		int v = to[i];
		if (v == F || vis[v]) continue;
		getans(v, u);
	}
}

inline void affect(int u, int F, int V) {
	bucket[dep[u]] += V;
	for (int i = head[u]; i; i = nxt[i]) {
		int v = to[i];
		if (v == F || vis[v]) continue;
		affect(v, u, V);
	}
}

inline void doit(int u) {
	dep[u] = 0;
	getdis(u, 0);
	for (int i = 0, S = qry[u].size(); i < S; ++i) {
		int k = qry[u][i].first - dep[u];
		if (k < 0) continue;
		ans[qry[u][i].second] += bucket[k];
	}
	for (int i = head[u]; i; i = nxt[i]) {
		int v = to[i];
		if (vis[v]) continue;
		affect(v, u, -1);
		getans(v, u);
		affect(v, u, 1);
	}
	affect(u, 0, -1);
}

inline void solve(int u) {
	vis[u] = 1;
	doit(u);
	for (int i = head[u]; i; i = nxt[i]) {
		int v = to[i];
		if (vis[v]) continue;
		SUM = siz[u];
		rt = 0;
		max_part[rt] = 1e9;
		getroot(v, u);
		solve(rt);
	}
	return;
}

inline void init() {
	ecnt = rt = 0;
	max_part[rt] = 1e9;
	memset(head, 0, sizeof(head));
	memset(ans, 0, sizeof(ans));
	memset(vis, 0, sizeof(vis));
	for (int i = 1; i <= n; ++i) qry[i].clear();
}

int main() {
    for (kin >> T; T --> 0;) {
		init();
        kin >> n >> m;
		for (int i = 1, u, v; i < n; ++i) {
            kin >> u >> v;
			add_edge(u, v), add_edge(v, u);
		}
		for (int i = 1, x, k; i <= m; ++i) {
            kin >> x >> k;
			qry[x].push_back(make_pair(k, i));
		}
		SUM = n;
		getroot(1, 0);
		solve(rt);
		for (int i = 1; i <= m; ++i) kout << ans[i] << '\n';
	}
	return 0;
}

/*
struct graph {
    struct edge {
        int to, nxt;
    } *e;
    int _N, _M, *head, cnt;
    void clear() {
        cnt = 0;
        memset(head, 0, sizeof(int) * (_N + 2));
    }
    void init(int N, int M) {
        _N = N, _M = M;
        head = new int[N + 2];
        e = new edge[(M << 1) + 2];
        clear();
    }
    void add_edge(int u, int v) {
        e[++cnt].to = v;
        e[cnt].nxt = head[u];
        head[u] = cnt;
        e[++cnt].to = u;
        e[cnt].nxt = head[v];
        head[v] = cnt;
    }
    graph() {}
    graph(int N, int M) { init(N, M); }
};

struct query_list {
    struct query_node {
        int k, id;
        query_node *nxt;
    } *nodes, **head, *_nodes_begin;
    int _N, _Q;
    void clear() {
        nodes = _nodes_begin;
        memset(head, 0, sizeof(query_node*) * (_N + 2));
    }
    void init(int N, int Q) {
        _N = N, _Q = Q;
        _nodes_begin = new query_node[Q + 2];
        head = new query_node*[N + 2];
        clear();
    }

    void add_query(int id, int u, int k) {
        nodes->id = id;
        nodes->k = k;
        nodes->nxt = head[u];
        head[u] = nodes++;
    }
    query_list() {}
    query_list(int N, int Q) { init(N, Q); }
};

const int N = 1e5 + 7;
int T, n, m;
graph Tree(N, N);
query_list Qry(N, N);
bool vis[N];
int dep[N], cnt[N], ans[N];

namespace RootGeter {
    int siz[N];
    int _TotalSize, _Root, _MaxPart;
    void _GetSize(int u, int fa) {
        siz[u] = 1;
        for (int i = Tree.head[u]; i; i = Tree.e[i].nxt) {
            int v = Tree.e[i].to;
            if (v != fa && !vis[v]) {
                _GetSize(v, u);
                siz[u] += siz[v];
            }
        }
    }
    void _GetRoot(int u, int fa) {
        int MaxPart = _TotalSize - siz[u];
        for (int i = Tree.head[u]; i; i = Tree.e[i].nxt) {
            int v = Tree.e[i].to;
            if (v != fa && !vis[v]) {
                _GetRoot(v, u);
                MaxPart = max(MaxPart, siz[v]);
            }
        }
        if (MaxPart < _MaxPart) {
            _Root = u;
            _MaxPart = MaxPart;
        }
    }
    int GetRoot(int u) {
        _GetSize(u, 0);
        _TotalSize = siz[u];
        _MaxPart = 1e9;
        _GetRoot(u, 0);
        return _Root;
    }
}

void GetDep(int u, int fa) {
    ++cnt[dep[u]];
    for (int i = Tree.head[u]; i; i = Tree.e[i].nxt) {
        int v = Tree.e[i].to;
        if (v != fa && !vis[v]) {
            dep[v] = dep[u] + 1;
            GetDep(v, u);
        }
    }
}

void Remove(int u, int fa) {
    --cnt[dep[u]];
    for (int i = Tree.head[u]; i; i = Tree.e[i].nxt) {
        int v = Tree.e[i].to;
        if (v != fa && !vis[v]) Remove(v, u);
    }
}

void Recover(int u, int fa) {
    ++cnt[dep[u]];
    for (int i = Tree.head[u]; i; i = Tree.e[i].nxt) {
        int v = Tree.e[i].to;
        if (v != fa && !vis[v]) Remove(v, u);
    }
}

void GetAns(int u, int fa) {
    for (query_list::query_node *n = Qry.head[u]; n; n = n->nxt) {
        int k = n->k - dep[u];
        if (k < 0) continue;
        ans[n->id] += cnt[k];
    }
    for (int i = Tree.head[u]; i; i = Tree.e[i].nxt) {
        int v = Tree.e[i].to;
        if (v != fa && !vis[v]) GetAns(v, u);
    }
}

void Contribute(int u) {
    dep[u] = 0;
    GetDep(u, 0);
    for (query_list::query_node *n = Qry.head[u]; n; n = n->nxt) {
        int k = n->k - dep[u];
        if (k < 0) continue;
        ans[n->id] += cnt[k];
    }
    for (int i = Tree.head[u]; i; i = Tree.e[i].nxt) {
        int v = Tree.e[i].to;
        if (!vis[v]) {
            Remove(v, u);
            GetAns(v, u);
            Recover(v, u);
        }
    }
    Remove(u, 0);
    return ;
}

void solve(int u) {
    // printf("[st] solve(%d)\n", u);
    vis[u] = 1;
    Contribute(u);
    for (int i = Tree.head[u]; i; i = Tree.e[i].nxt) {
        int v = Tree.e[i].to;
        if (!vis[v]) solve(RootGeter::GetRoot(v));
    }
    vis[u] = 0;
    // printf("[ex] solve(%d)\n", u);
}

signed main() {
    for (kin >> T; T --> 0;) {
        kin >> n >> m;
        Tree.clear(), Qry.clear();
        for (int i = 1, u, v; i < n; ++i) {
            kin >> u >> v;
            Tree.add_edge(u, v);
        }
        for (int i = 1, u, k; i <= m; ++i) {
            ans[i] = i;
            kin >> u >> k;
            Qry.add_query(i, u, k);
        }
        solve(RootGeter::GetRoot(1));
        for (int i = 1; i <= m; ++i) kout << ans[i] << '\n';
    }
    return 0;
}
*/