/*************************************
 * @problem:      P2889 [USACO07NOV]挤奶的时间Milking Time.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2019-12-22.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 
// 三倍经验 P2439 略有不同
/// 说明：本代码曾出现 RE 错误，现存有调试宏 Throw(val) 和 chk(val, lim)
#include <bits/stdc++.h>
using namespace std;

const int MaxN = 1000 + 7, MaxK = 1000000 + 7;
int n, time_end, r, ans = 0;
int head[MaxK + MaxN], to[MaxN], val[MaxN], nxt[MaxN], edges_count = 0;
int f[MaxK + MaxN];

#define Throw(val) printf("Error : value %s = %d(in function %s) out of limit.\n", #val, val, __FUNCTION__), void()
#define chk(val, lim) if ((val) >= (lim)) { Throw(val); exit(0); }

void add(int u, int v, int w)
{
    // chk(edges_count + 1, MaxN);
    // chk(u, MaxK);
    to[++edges_count] = v;
    val[edges_count] = w;
    nxt[edges_count] = head[u];
    head[u] = edges_count;
}

int main()
{
    memset(head, 0, sizeof(head));
    scanf("%d%d%d", &time_end, &n, &r); 
    for (int i = 1, s, t, w; i <= n; i++) {
        scanf("%d%d%d", &s, &t, &w);
        add(s, t + r, w);
        // chk(t + r, MaxK + MaxN);
    }
    sort(1, 2);
    for (int i = 0; i <= time_end + r; i++) {
        // chk(i, MaxN + MaxK);
        for (int e = head[i]; e; e = nxt[e]) {
            // chk(head[i], MaxN);
            // chk(e, MaxN);
            f[to[e]] = max(f[to[e]], f[i] + val[e]);
        }
        f[i + 1] = max(f[i + 1], f[i]);
        // chk(i + 1, MaxK + MaxN);
    }
    printf("%d", f[time_end + r]);
    return 0;
}