/*************************************
 * @problem:      [NOI2005] 智慧珠游戏.
 * @author:       brealid.
 * @time:         2021-01-02.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;
#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

/// @brief DLX 跳舞链模板
class DancingLinksX {
private:
    struct DancingLinksX_Node {
        int l, r, u, d; // l, r, u, d 是指向左, 右, 上, 下的指针
        int row, col;   // 第 row 行，第 col 列
        DancingLinksX_Node() : l(0), r(0), u(0), d(0), row(0), col(0) {}
    };
    std::vector<DancingLinksX_Node> node; // 存储节点
    std::vector<int> first;               // 存储每行的头结点
    std::vector<int> siz;                 // 存储每列 '1' 的个数
    std::vector<int> result;              // 存放答案
    std::vector<std::vector<int> > Ele;   // 存储每一行的元素
    int cnt;
    /**
     * @brief 删除节点
     * @param u 节点编号
     */
    void remove(int u) {
        DancingLinksX_Node &self = node[u];
        node[self.l].r = self.r;
        node[self.r].l = self.l;
        for (int i = self.u; i != u; i = node[i].u)
            for (int j = node[i].l; j != i; j = node[j].l) {
                node[node[j].d].u = node[j].u;
                node[node[j].u].d = node[j].d;
                --siz[node[j].col];
            }
    }
    /**
     * @brief 恢复节点
     * @param u 节点编号
     */
    void recover(int u) {
        DancingLinksX_Node &self = node[u];
        for (int i = self.d; i != u; i = node[i].d)
            for (int j = node[i].r; j != i; j = node[j].r) {
                node[node[j].d].u = node[node[j].u].d = j;
                ++siz[node[j].col];
            }
        node[self.l].r = node[self.r].l = u;
    }
    /**
     * @brief 核心搜索 (跳跃 dance)
     */
    bool dance() {
        if (!node[0].r) return true;
        int minimum_col = node[0].r;
        for (int i = node[0].r; i != 0; i = node[i].r)
            if (siz[i] < siz[minimum_col]) minimum_col = i;
        remove(minimum_col);
        result.push_back(0);
        for (int i = node[minimum_col].d; i != minimum_col; i = node[i].d) {
            result.back() = node[i].row;
            for (int j = node[i].l; j != i; j = node[j].l) remove(node[j].col);
            if (dance()) return true;
            for (int j = node[i].r; j != i; j = node[j].r) recover(node[j].col);
        }
        result.pop_back();
        recover(minimum_col);
        return false;
    }
public:
    DancingLinksX() : cnt(0) {}
    /**
     * @brief 构造函数
     * @param row 行数
     * @param col 列数
     * @param count_of_1 最多有几个'1'
     */
    DancingLinksX(int row, int col, int count_of_1) { init(row, col, count_of_1); }
    /**
     * @brief 初始化函数
     * @param row 行数
     * @param col 列数
     * @param count_of_1 最多有几个'1'
     */
    void init(int row, int col, int count_of_1) {
        cnt = col;
        Ele.assign(row + 1, vector<int>());
        first.assign(row + 1, 0);
        siz.assign(col + 1, 0);
        node.assign(col + count_of_1 + 1, DancingLinksX_Node());
        for (int i = 0; i <= col; ++i) {
            node[i].l = i - 1;
            node[i].r = i + 1;
            node[i].u = node[i].d = i;
        }
        node[0].l = col;
        node[col].r = 0;
    }
    /**
     * @brief 插入节点
     * @param r 第几行
     * @param c 第几列
     */
    void insert(int r, int c) {
        // if (!r || !c) kout << "Invalid R & C: " << r << ", " << c << '\n';
        Ele[r].push_back(c);
        DancingLinksX_Node &self = node[++cnt];
        self.row = r;
        self.col = c;
        ++siz[c];
        self.u = node[c].u;
        node[self.u].d = cnt;
        self.d = c;
        node[c].u = cnt;
        if (!first[r]) first[r] = self.l = self.r = cnt;
        else {
            self.l = node[first[r]].l;
            self.r = first[r];
            node[node[first[r]].l].r = cnt;
            node[first[r]].l = cnt;
        }
        for (int j = node[first[r]].l; j != first[r]; j = node[j].l);
        for (int j = node[first[r]].r; j != first[r]; j = node[j].r);
        for (int j = node[cnt].l; j != cnt; j = node[j].l);
        for (int j = node[cnt].r; j != cnt; j = node[j].r);
    }
    /**
     * @brief 工作函数（无解返回空 vector）
     * @return vector<int> 答案数组
     */
    const vector<int>& solve() {
        result.clear();
        dance();
        return result;
    }
    const vector<int>& operator [] (const int &id) const {
        return Ele[id];
    }
};

DancingLinksX DLX(1644, 67, 9226);

/*
Column: 55-Block-Exist 12-PCS-Exist
Line:   Place-Way
*/

char chess_board[12][12], ans[56];

namespace Init {
    const int id[11][11] = {
        {0},
        {0, 1},
        {0, 2, 3},
        {0, 4, 5, 6},
        {0, 7, 8, 9, 10},
        {0, 11, 12, 13, 14, 15},
        {0, 16, 17, 18, 19, 20, 21},
        {0, 22, 23, 24, 25, 26, 27, 28},
        {0, 29, 30, 31, 32, 33, 34, 35, 36},
        {0, 37, 38, 39, 40, 41, 42, 43, 44, 45},
        {0, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55}
    };
    int ColumnTotal;

    /// @brief Place piece of 3
    void place_block(int PCS_id, int LnBeg, int LnEnd, int x1, int y1, int x2, int y2, int x3, int y3) {
        for (int x = LnBeg; x <= LnEnd; ++x) {
            for (int y = 1; y + LnBeg - 1 <= x; ++y) {
                ++ColumnTotal;
                DLX.insert(ColumnTotal, id[x + x1][y + y1]);
                DLX.insert(ColumnTotal, id[x + x2][y + y2]);
                DLX.insert(ColumnTotal, id[x + x3][y + y3]);
                DLX.insert(ColumnTotal, PCS_id);
            }
        }
    }

    /// @brief Place piece of 4
    void place_block(int PCS_id, int LnBeg, int LnEnd, int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4) {
        for (int x = LnBeg; x <= LnEnd; ++x) {
            for (int y = 1; y + LnBeg - 1 <= x; ++y) {
                ++ColumnTotal;
                DLX.insert(ColumnTotal, id[x + x1][y + y1]);
                DLX.insert(ColumnTotal, id[x + x2][y + y2]);
                DLX.insert(ColumnTotal, id[x + x3][y + y3]);
                DLX.insert(ColumnTotal, id[x + x4][y + y4]);
                DLX.insert(ColumnTotal, PCS_id);
            }
        }
    }

    /// @brief Place piece of 5
    void place_block(int PCS_id, int LnBeg, int LnEnd, int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4, int x5, int y5) {
        for (int x = LnBeg; x <= LnEnd; ++x) {
            for (int y = 1; y + LnBeg - 1 <= x; ++y) {
                ++ColumnTotal;
                DLX.insert(ColumnTotal, id[x + x1][y + y1]);
                DLX.insert(ColumnTotal, id[x + x2][y + y2]);
                DLX.insert(ColumnTotal, id[x + x3][y + y3]);
                DLX.insert(ColumnTotal, id[x + x4][y + y4]);
                DLX.insert(ColumnTotal, id[x + x5][y + y5]);
                DLX.insert(ColumnTotal, PCS_id);
            }
        }
    }

    void place_A() {
        place_block(56, 1, 9, 0, 0, 1, 0, 1, 1);
        place_block(56, 2, 9, 0, 0, 0, 1, 1, 0);
        place_block(56, 2, 9, 0, 0, 0, 1, 1, 1);
        place_block(56, 2, 9, 1, 0, 0, 1, 1, 1);
    }

    void place_B() {
        place_block(57, 4, 10, 0, 0, 0, 1, 0, 2, 0, 3);
        place_block(57, 1, 7, 0, 0, 1, 0, 2, 0, 3, 0);
    }

    void place_C() {
        place_block(58, 1, 8, 0, 0, 1, 0, 2, 0, 2, 1);
        place_block(58, 2, 8, 0, 0, 0, 1, 1, 1, 2, 1);
        place_block(58, 2, 8, 0, 0, 0, 1, 1, 0, 2, 0);
        place_block(58, 2, 8, 0, 1, 1, 1, 2, 1, 2, 0);
        place_block(58, 2, 9, 0, 0, 1, 0, 1, 1, 1, 2);
        place_block(58, 3, 9, 0, 2, 1, 0, 1, 1, 1, 2);
        place_block(58, 3, 9, 0, 0, 0, 1, 0, 2, 1, 0);
        place_block(58, 3, 9, 0, 0, 0, 1, 0, 2, 1, 2);
    }

    void place_D() {
        place_block(59, 2, 9, 0, 0, 0, 1, 1, 0, 1, 1);
    }

    void place_E() {
        place_block(60, 1, 8, 0, 0, 1, 0, 2, 0, 2, 1, 2, 2);
        place_block(60, 3, 8, 0, 0, 0, 1, 0, 2, 1, 2, 2, 2);
        place_block(60, 3, 8, 0, 0, 0, 1, 0, 2, 1, 0, 2, 0);
        place_block(60, 3, 8, 0, 2, 1, 2, 2, 0, 2, 1, 2, 2);
    }

    void place_F() {
        // 横着
        place_block(61, 3, 9, 1, 0, 1, 1, 1, 2, 1, 3, 0, 1);
        place_block(61, 3, 9, 1, 0, 1, 1, 1, 2, 1, 3, 0, 2);
        place_block(61, 4, 9, 0, 0, 0, 1, 0, 2, 0, 3, 1, 1);
        place_block(61, 4, 9, 0, 0, 0, 1, 0, 2, 0, 3, 1, 2);
        // 竖着
        place_block(61, 1, 7, 0, 0, 1, 0, 2, 0, 3, 0, 1, 1);
        place_block(61, 1, 7, 0, 0, 1, 0, 2, 0, 3, 0, 2, 1);
        place_block(61, 2, 7, 0, 1, 1, 1, 2, 1, 3, 1, 1, 0);
        place_block(61, 2, 7, 0, 1, 1, 1, 2, 1, 3, 1, 2, 0);
    }

    void place_G() {
        // 横着
        place_block(62, 3, 9, 0, 0, 0, 1, 0, 2, 1, 0, 1, 2);
        place_block(62, 3, 9, 0, 0, 0, 2, 1, 0, 1, 1, 1, 2);
        // 竖着
        place_block(62, 2, 8, 0, 0, 0, 1, 1, 0, 2, 0, 2, 1);
        place_block(62, 2, 8, 0, 0, 0, 1, 1, 1, 2, 0, 2, 1);
    }

    void place_H() {
        // 横着
        place_block(63, 2, 9, 0, 0, 0, 1, 1, 0, 1, 1, 1, 2);
        place_block(63, 3, 9, 0, 0, 0, 1, 1, 0, 1, 1, 0, 2);
        place_block(63, 3, 9, 0, 1, 0, 2, 1, 0, 1, 1, 1, 2);
        place_block(63, 3, 9, 0, 0, 0, 1, 0, 2, 1, 1, 1, 2);
        // 竖着
        place_block(63, 1, 8, 0, 0, 1, 0, 1, 1, 2, 0, 2, 1);
        place_block(63, 2, 8, 0, 1, 1, 0, 1, 1, 2, 0, 2, 1);
        place_block(63, 2, 8, 0, 0, 0, 1, 1, 0, 1, 1, 2, 0);
        place_block(63, 2, 8, 0, 0, 0, 1, 1, 0, 1, 1, 2, 1);
    }

    void place_I() {
        // 横着
        place_block(64, 3, 9, 0, 0, 0, 1, 1, 1, 1, 2, 1, 3);
        place_block(64, 3, 9, 0, 0, 0, 1, 0, 2, 1, 2, 1, 3);
        place_block(64, 4, 9, 1, 0, 1, 1, 1, 2, 0, 2, 0, 3);
        place_block(64, 4, 9, 1, 0, 1, 1, 0, 1, 0, 2, 0, 3);
        // 竖着
        place_block(64, 1, 7, 0, 0, 1, 0, 1, 1, 2, 1, 3, 1);
        place_block(64, 1, 7, 0, 0, 1, 0, 2, 0, 2, 1, 3, 1);
        place_block(64, 2, 7, 0, 1, 1, 1, 1, 0, 2, 0, 3, 0);
        place_block(64, 2, 7, 0, 1, 1, 1, 2, 1, 2, 0, 3, 0);
    }

    void place_J() {
        place_block(65, 2, 8, 0, 1, 1, 0, 1, 1, 1, 2, 2, 1);
    }

    void place_K() {
        // 左上-右下
        place_block(66, 1, 8, 0, 0, 1, 0, 1, 1, 2, 1, 2, 2);
        place_block(66, 2, 8, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2);
        // 右上-左下
        place_block(66, 3, 8, 0, 2, 1, 2, 1, 1, 2, 1, 2, 0);
        place_block(66, 3, 8, 0, 2, 0, 1, 1, 1, 1, 0, 2, 0);
    }

    void place_L() {
        // 横着
        place_block(67, 3, 9, 0, 0, 1, 0, 1, 1, 1, 2, 1, 3);
        place_block(67, 4, 9, 0, 3, 1, 0, 1, 1, 1, 2, 1, 3);
        place_block(67, 4, 9, 0, 0, 0, 1, 0, 2, 0, 3, 1, 0);
        place_block(67, 4, 9, 0, 0, 0, 1, 0, 2, 0, 3, 1, 3);
        // 竖着
        place_block(67, 1, 7, 0, 0, 1, 0, 2, 0, 3, 0, 3, 1);
        place_block(67, 2, 7, 0, 0, 1, 0, 2, 0, 3, 0, 0, 1);
        place_block(67, 2, 7, 0, 1, 1, 1, 2, 1, 3, 1, 0, 0);
        place_block(67, 2, 7, 0, 1, 1, 1, 2, 1, 3, 1, 3, 0);
    }

    bool resolve_pcs(char require_pcs) {
        bool occured = false;
        for (int i = 1; i <= 10; ++i)
            for (int j = 1; j <= i; ++j)
                if (chess_board[i][j] == require_pcs) {
                    if (!occured) {
                        ++ColumnTotal;
                        occured = true;
                    }
                    DLX.insert(ColumnTotal, id[i][j]);
                }
        if (occured) DLX.insert(ColumnTotal, require_pcs - 'A' + 56);
        return occured;
    }
}

signed main() {
    for (int i = 1; i <= 10; ++i)
        scanf("%s", chess_board[i] + 1);
    if (!Init::resolve_pcs('A')) Init::place_A();
    if (!Init::resolve_pcs('B')) Init::place_B();
    if (!Init::resolve_pcs('C')) Init::place_C();
    if (!Init::resolve_pcs('D')) Init::place_D();
    if (!Init::resolve_pcs('E')) Init::place_E();
    if (!Init::resolve_pcs('F')) Init::place_F();
    if (!Init::resolve_pcs('G')) Init::place_G();
    if (!Init::resolve_pcs('H')) Init::place_H();
    if (!Init::resolve_pcs('I')) Init::place_I();
    if (!Init::resolve_pcs('J')) Init::place_J();
    if (!Init::resolve_pcs('K')) Init::place_K();
    if (!Init::resolve_pcs('L')) Init::place_L();
    const vector<int> &vRet = DLX.solve();
    if (vRet.empty()) kout << "No solution\n";
    else {
        for (size_t i = 0; i < vRet.size(); ++i) {
            const vector<int> &now = DLX[vRet[i]];
            for (size_t j = 0; j + 1 < now.size(); ++j) 
                ans[now[j]] = now.back() - 56 + 'A';
        }
        for (int i = 1; i <= 10; ++i) {
            for (int j = 1; j <= i; ++j)
                putchar(ans[Init::id[i][j]]);
            putchar('\n');
        }
    }
    // printf("Column Used: %d\n", Init::ColumnTotal);
    // for (int i = 1; i <= Init::ColumnTotal; ++i) {
    //     const vector<int> &now = DLX[i];
    //     for (size_t j = 0; j < now.size(); ++j)
    //         printf("%d%c", now[j], " \n"[j + 1 == now.size()]);
    // }
    return 0;
}