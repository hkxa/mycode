/*************************************
 * problem:      P2015 二叉苹果树.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-07-04.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

int q, n;
int a[107] = {0};
int f[107][107] = {0};

struct Edge {
    int to, apple;
    Edge(int To = 0, int Apple = 0) : to(To), apple(Apple) {}
};

struct MakeTree_DpTree {
    vector<Edge> G[107];
    bool vis[107];

    typedef vector<Edge>::iterator Vec_it;

    void dfs(int u)
    {
        int son[3] = {0}, apple[3] = {0}, cnt = 0;
        vis[u] = 1;
        if (G[u].size() == 1) return;
        for (Vec_it it = G[u].begin(); it != G[u].end(); it++) {
            if (vis[it->to]) continue;
            dfs(it->to);
            son[cnt] = it->to;
            apple[cnt++] = it->apple;
        }
        if (!cnt) return;
        #define lcNum j - 1
        #define rcNum i - j - 1
        for (int i = 1; i <= q; i++) {
            for (int j = 0; j <= i; j++) {
                int appleSum(0);
                if (lcNum >= 0) appleSum += apple[0];
                if (rcNum >= 0) appleSum += apple[1];
                if (j) f[u][i] = max(f[u][i], f[son[0]][lcNum] + f[son[1]][rcNum] + appleSum);
                else f[u][i] = max(f[u][i], f[son[1]][rcNum] + appleSum);
            }
        }
    }
} mt;

int main()
{
    n = read<int>();
    q = read<int>();
    int u, v, cnt;
    for (int i = 1; i < n; i++) {
        u = read<int>();
        v = read<int>();
        cnt = read<int>();
        mt.G[u].push_back(Edge(v, cnt));
        mt.G[v].push_back(Edge(u, cnt));
    }
    mt.dfs(1);
    write(f[1][q], 10);
    return 0;
}