/*************************************
 * problem:      SP503 PRINT.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-14.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define maxn 50000
#define PrimeMaxCnt 12000
#define SubMaxSize 1000000

bool isNotP[maxn + 7];
int pri[PrimeMaxCnt + 7], cnt = 0;
bool tmp[SubMaxSize + 7];

void prepare_sqrtN_prime()
{
    isNotP[1] = true;
    for (int i = 2; i <= maxn; i++) {
        if (!isNotP[i]) {
            pri[++cnt] = i;
            for (int j = i + i; j <= maxn; j += i) {
                isNotP[j] = true;
            }
        }
    }
}

void solveCase()
{
    int l = read<int>(), r = read<int>();
    if (l == 1) l = 2;
    memset(tmp, 0, sizeof(tmp));
    for (int i = 1; i <= cnt; i++) {
        for (int j = max(l / pri[i], 2); j <= r / pri[i]; j++) {
            if (j * pri[i] < l) continue;
            if (j * pri[i] > r) break;
            tmp[j * pri[i] - l] = true;
        }
    }
    for (int i = l; i <= r; i++) {
        if (!tmp[i - l]) write(i, 10);
    }
}

int main()
{
    prepare_sqrtN_prime();
    int Tcases = read<int>();
    while (Tcases--) solveCase();
    return 0;
}