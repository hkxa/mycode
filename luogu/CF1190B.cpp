/*************************************
 * @problem:      Tokitsukaze, CSL and Stone Game.
 * @author:       brealid.
 * @time:         2021-92-21.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 1e5 + 7;

int n, a[N];

signed main() {
    // file_io::set_to_file("CF1190B");
    kin >> n;
    for (int i = 1; i <= n; ++i) kin >> a[i];
    if (n == 1) {
        if (a[1] % 2) kout << "sjfnb\n";
        else kout << "cslnb\n";
        return 0;
    }
    sort(a + 1, a + n + 1);
    bool SameStone_Found = false;
    for (int i = 1; i < n; ++i)
        if (a[i] == a[i + 1]) {
            if (SameStone_Found || !a[i] || (a[i] - 1 == a[i - 1] && i != 1)) {
                kout << "cslnb\n"; // 第一次取石子的时候就必败
                return 0;
            }
            --a[i]; // 第一轮的唯一可能举动
            SameStone_Found = true; // 记录标记(得到的答案应反过来)
        }
    for (int i = n; i > 1; --i) a[i] = a[i] - a[i - 1] - 1; // 将 a 数组转而记录差值(不能相同, 所以需要减 1)
    // 每一步可以将 i 的一个石子放到 i+1 中，全部都到 n+1 的最后一步者赢
    // 与 n+1 同奇偶的石子不用考虑(一旦移动了，立即可以再移动一次)
    int SG = 0;
    for (int i = n; i >= 1; i -= 2) SG += a[i];
    bool is_win = (bool)(SG % 2);
    if (SameStone_Found) is_win = !is_win;
    if (is_win) kout << "sjfnb\n";
    else kout << "cslnb\n";
    return 0;
}