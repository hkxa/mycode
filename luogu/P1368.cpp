/*************************************
 * problem:      P1368 工艺.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-07-27.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;
int a[300007] = {0};

#define relation(a, b) (((a) > (b)) + ((a) >= (b)))
#define less 0
#define equal 1
#define greater 2 

int MinistExpression()
{
    int i = 0, j = 1, k = 0;
    while (i < n && j < n && k < n) {
        switch (relation(a[(i + k) % n], a[(j + k) % n])) {
            case less : 
                j += k + 1; 
                if (i == j) j++;
                k = 0;
                break;
            case equal:
                k++;
                break;
            case greater : 
                i += k + 1; 
                if (i == j) j++;
                k = 0;
                break;
        }
    }
    return min(i, j);
}

int main()
{
    n = read<int>();
    for (int i = 0; i < n; i++) {
        a[i] = read<int>();
    }
    int ansBase = MinistExpression();
    for (int i = 0; i < n; i++) {
        write(a[(ansBase + i) % n], 32);
    }
    return 0;
}