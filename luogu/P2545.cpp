/*************************************
 * @problem:      P2545 [AHOI2004]实验基地.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2019-12-19.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;
int a[2][2007];
long long minn[2007][2007], sum[2007];
#define query(l, r) (sum[r] - sum[(l) - 1])

int main()
{
    memset(minn, 0x3f, sizeof(minn));
    n = read<int>();
    for (int i = 1; i <= n; i++) a[0][i] = read<int>();
    for (int i = 1; i <= n; i++) a[1][i] = read<int>();
    for (int i = 1; i <= n; i++) sum[i] = sum[i - 1] + a[0][i] + a[1][i];
    for (int i = 1; i <= n; i++) {
        minn[i][1] = a[0][i];
        // printf("%lld%c", minn[i][1], " \n"[i == 1]);
        for (int j = 2; j <= i; j++) {
            minn[i][j] = min(minn[i][j - 1], minn[i - 1][j - 1] + a[0][i]);
            // printf("%lld%c", minn[i][j], " \n"[j == i]);
        }
    }
    for (int i = 1; i <= n; i++) {
        for (int j = 2; j <= i; j++) {
            minn[i][j] = min(minn[i][j], minn[i - 1][j - 1]);
        }
    }
    long long ans = -5 * 1e6 - 1;
    for (int l = 1; l <= n; l++) {
        for (int r = l + 2; r <= n; r++) {
            // printf("query %d~%d query=%lld minn[][]=%lld.\n", l, r, query(l, r), minn[r - 1][r - l - 1]);
            ans = max(ans, query(l, r) - minn[r - 1][r - l - 1]);
        }
    }
    write(ans);
    return 0;
}

/*
9
3 4 -2 5 -4 -5 10 12 -8
10 -1 -5 8 7 -3 0 2 9
*/