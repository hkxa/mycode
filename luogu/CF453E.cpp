//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Little Pony and Lord Tirek.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-14.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int MY_INT_MAX = INT_MAX / 3;
const int N = 1e5 + 8, MY_GREATER_THAN_MAXN = 1e5 + 3;

namespace against_cpp11 {
    int n, m, S[N], M[N], R[N], b[N], c[N];
    int64 sumM[N];

    inline pair<int64, int64> operator + (pair<int64, int64> a, pair<int64, int64> b) {
        return make_pair(a.first + b.first, a.second + b.second);
    }

    struct ChairmanTree {
        int op, rt[N], ls[N * 20], rs[N * 20], node_count;
        int64 val[N * 20], sum[N * 20], w[N * 20];
        void update(int &x, int y, int l, int r, int pos, int p) {
            x = ++node_count;
            ls[x] = ls[y];
            rs[x] = rs[y];
            val[x] = val[y] + R[p];
            sum[x] = sum[y] + M[p] - ((op == 0) ? S[p] : 0);
            if (l == r) return;
            int mid = (l + r) >> 1;
            if (pos <= mid) update(ls[x], ls[y], l, mid, pos, p);
            else update(rs[x], rs[y], mid + 1, r, pos, p);
        }
        pair<int64, int64> query(int u, int v, int L, int R, int l, int r) {
            if (L <= l && r <= R)
                return make_pair(val[v] - val[u], sum[v] - sum[u]);
            int mid = (l + r) >> 1;
            pair<int64, int64> ans = make_pair(0, 0);
            if (L <= mid)
                ans = ans + query(ls[u], ls[v], L, R, l, mid);
            if (R > mid)
                ans = ans + query(rs[u], rs[v], L, R, mid + 1, r);
            return ans;
        }
    } T0, T1;

    struct node {
        int l, r, val;
        node(int _l = 0, int _r = 0, int _val = 0) : l(_l), r(_r), val(_val) {}
        inline bool operator < (const node &b) const { return l < b.l; }
    };
    set<node> s;
    inline set<node>::iterator split(int x) {
        set<node>::iterator it = s.lower_bound(node(x));
        if (it != s.end() && it->l == x)
            return it;
        it--;
        int l = it->l, r = it->r, val = it->val;
        s.erase(it);
        s.insert(node(l, x - 1, val));
        return s.insert(node(x, r, val)).first;
    }
    inline int64 ask(int t, int x, int y) {
        set<node>::iterator R = split(y + 1), L = split(x);
        int l, r, val;
        int64 ans = sumM[y] - sumM[x - 1];
        pair<int64, int64> res;
        for (set<node>::iterator it = L; it != R; it++) {
            l = it->l;
            r = it->r;
            val = it->val;
            if (val == 0)
                res = T0.query(T0.rt[l - 1], T0.rt[r], min(MY_GREATER_THAN_MAXN, t - val + 1), MY_GREATER_THAN_MAXN, 0, MY_GREATER_THAN_MAXN);
            else
                res = T1.query(T1.rt[l - 1], T1.rt[r], min(MY_GREATER_THAN_MAXN, t - val + 1), MY_GREATER_THAN_MAXN, 0, MY_GREATER_THAN_MAXN);
            ans += res.first * (t - val) - res.second;
        }
        s.erase(L, R);
        s.insert(node(x, y, t));
        return ans;
    }

    int main() {
        read >> n;
        for (int i = 1; i <= n; i++) {
            read >> S[i] >> M[i] >> R[i];
            b[i] = (R[i] == 0) ? MY_GREATER_THAN_MAXN : (M[i] - S[i]) / R[i] + ((M[i] - S[i]) % R[i] > 0);
            c[i] = (R[i] == 0) ? MY_GREATER_THAN_MAXN : M[i] / R[i] + (M[i] % R[i] > 0);
            sumM[i] = sumM[i - 1] + M[i];
        }
        T0.op = 0;
        T1.op = 1;
        for (int i = 1; i <= n; i++) {
            T0.update(T0.rt[i], T0.rt[i - 1], 0, MY_GREATER_THAN_MAXN, b[i], i);
            T1.update(T1.rt[i], T1.rt[i - 1], 0, MY_GREATER_THAN_MAXN, c[i], i);
        }
        s.insert(node(1, n, 0));
        s.insert(node(n + 1, n + 1, 0));
        read >> m;
        for (int i = 1, t, l, r; i <= m; i++) {
            read >> t >> l >> r;
            write << ask(t, l, r) << '\n';
        }
        return 0;
    }
}

signed main() { return against_cpp11::main(); }