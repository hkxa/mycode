//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Arpa and a research in Mexican wave.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-08.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;
#ifndef M_PI_2
#define M_PI_2		1.57079632679489661923
#endif

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int eps = 1e-8;

namespace against_cpp11 {
    const int N = 1e3 + 7;
    int n;
    struct point {
        int x[5];
        inline int sqr_length() {
            return x[0] * x[0] + x[1] * x[1] + x[2] * x[2] + x[3] * x[3] + x[4] * x[4];
        }
        inline double length() {
            return sqrt(x[0] * x[0] + x[1] * x[1] + x[2] * x[2] + x[3] * x[3] + x[4] * x[4]);
        }
        point operator - (const point &b) const {
            return (point){x[0] - b.x[0], x[1] - b.x[1], x[2] - b.x[2], x[3] - b.x[3], x[4] - b.x[4]};
        }
        int operator * (const point b) const {
            return x[0] * b.x[0] + x[1] * b.x[1] + x[2] * b.x[2] + x[3] * b.x[3] + x[4] * b.x[4];
        }
    } a[N];
    vector<int> answer;
    signed main() {
        read >> n;
        for (int i = 1; i <= n; i++) {
            read >> a[i].x[0] >> a[i].x[1] >> a[i].x[2] >> a[i].x[3] >> a[i].x[4];
        }
        for (int i = 1; i <= n; i++) {
            bool fail = false;
            for (int j = 1; j <= n && !fail; j++) if (i != j) 
                for (int k = 1; k <= n && !fail; k++) if (i != k && j != k) 
                    if (acos((a[j] - a[i]) * (a[k] - a[i]) / ((a[j] - a[i]).length() * (a[k] - a[i]).length())) < M_PI_2) 
                        fail = true;
            if (!fail) answer.push_back(i);
        }
        write << answer.size() << '\n';
        for (size_t i = 0; i < answer.size(); i++)
            write << answer[i] << '\n';
        return 0;
    }
}

signed main() { return against_cpp11::main(); }