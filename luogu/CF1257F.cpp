/*************************************
 * @problem:      Codeforces - Educational #76.
 * @user_name:    hkxadpall.
 * @time:         2019-11-14.
 * @language:     C++.
 * @upload_place: Codeforces.
*************************************/ 
#pragma GCC optimize(2)
#pragma GCC optimize(3)

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define randR(x) (rand() % (x))
#define double_rand_0_to_1() ((double)rand() / RAND_MAX + ((double)rand() / RAND_MAX / RAND_MAX))
inline int count(int x)
{
    int res = 0;
    while (x) {
        if (x & 1) res++;
        x >>= 1;
    }
    return res;
}

int n;
int a[107], cnt[107];

inline double check(int x)
{
    int tot = 0;
    for (int i = 1; i <= n; i++) tot += (cnt[i] = count(a[i] ^ x));
    double average = tot / n, ans = 0;
    for (int i = 1; i <= n; i++) ans += (cnt[i] - average) * (cnt[i] - average);
    return ans;
}

#define startT 5000 // 100->497 1000->122 4000->113 5000->106 10000->107 20000->116 
#define deltaT 0.999 // 0.999->482 0.995->484 0.99->490 0.95->1411
#define endT 0.0013 // 0.0008->115 0.001->106 0.0013->106 0.01->111 0.1->118
#define acceptK 1 // 2->913 1->106 0.9->115
#define eps 0.0003

inline bool SA()
{
    double best = check(0), now;
    int x = 0, op;
    for (int t = startT; t > endT && best >= eps; t *= deltaT) {
        op = 1 << randR(30);
        now = check(x ^ op);
        if (now < best) {
            best = now;
            x ^= op;
        } else if (exp((best - now) / t) < acceptK * double_rand_0_to_1()) {
            best = now;
            x ^= op;
        }
    }
    if (best < eps) {
        write(x);
        return 1;
    } 
    return 0;
}

int main()
{
    srand(20170933 ^ 201709 ^ 2017);
    // srand(time(0) ^ 20170933);
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        a[i] = read<int>();
        cnt[i] = count(a[i]);
    }
    for (int i = 1; i < n; i++) {
        if ((cnt[i] & 1) != (cnt[i + 1] & 1)) {
            puts("-1");
            return 0;
        }
    }
    while (!SA()) if (clock() > 3.8 * CLOCKS_PER_SEC) {
        puts("-1");
        return 0;
    }
    return 0;
}
/*
51
83553484 246871438 229895836 213304455 79100988 217496740 104266894 500690060 246873348 754838668 
748616840 750177693 251587736 230057148 146273416 79088824 212315310 213691436 162725020 162688204 
137821572 247131852 213264590 749120526 217435308 234351821 227728524 749202569 144107657 145222804 
250604684 216517788 263597260 351784076 313846924 203613580 347530668 221441152 202665116 139979916 
233893036 226684300 246860430 212225188 686220428 217557148 213381292 76983448 234220716 78900492 
519499144

75
77185 1021613 441872 1041616 189053 734747 160192 393980 947697 260876 
56893 951602 612033 146260 963428 963123 417879 532574 321333 589123 
736127 479055 400836 602983 469553 644707 974335 645301 530993 124813 
566636 930378 163675 721394 1015152 455468 90958 118222 486907 7363 
1017344 386707 914565 414945 872620 646876 972615 398858 490797 448255 
280965 97781 863132 916400 895157 105123 1032179 132253 167942 641534 
339608 227181 288341 317999 416007 798013 777020 168596 449524 1021568 
117751 287871 83553485 246871439 229895837    
*/