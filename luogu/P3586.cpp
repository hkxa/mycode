/*************************************
 * @problem:      P3586 [POI2015]LOG.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-01-31.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

inline char InputChar()
{
    static char str[3];
    scanf("%s", str);
    return str[0];
}

bool type[1000007]; // 1 for U; 0 for Z
int64 op1[1000007], op2[1000007];
map<int, int> ops; int LSH_cnt = 0;
int nums[1000007];
int a[1000007];
int64 tc[2][1000007];

inline void add(int treeId, int pos, int val)
{ 
    for (; pos <= LSH_cnt; pos += (pos & (-pos))) 
        tc[treeId][pos] += val; 
}

inline int64 sum(int treeId, int pos)
{
    int64 ret = 0;
    for (; pos; pos -= (pos & (-pos))) 
        ret += tc[treeId][pos];
    return ret;
}

int main()
{
    register int n = read<int>(), m = read<int>();
    ops[0] = 1;
    for (register int i = 1; i <= m; i++) {
        if (InputChar() == 'U') type[i] = 1;
        else type[i] = 0;
        op1[i] = read<int>();
        op2[i] = read<int>();
        ops[op2[i]] = 1;
    }
    for (register map<int, int>::iterator it = ops.begin(); it != ops.end(); it++) {
        nums[LSH_cnt] = it->first;
        it->second = LSH_cnt++;
    }
    for (register int i = 1; i <= m; i++) op2[i] = ops[op2[i]];
    for (register int i = 1, cnt = 0; i <= m; i++) {
        if (type[i]) {
            // printf("change {%lld, %lld}\n", q[i].op1, q[i].op2);
            if (a[op1[i]]) {
                cnt--;
                add(0, a[op1[i]], -1);
                add(1, a[op1[i]], -nums[a[op1[i]]]);
            }
            a[op1[i]] = op2[i];
            if (!a[op1[i]]) continue;
            cnt++;
            add(0, op2[i], 1);
            add(1, op2[i], nums[op2[i]]);
        } else {
            // printf("query {%lld, %lld}\n", q[i].op1, q[i].op2);
            // printf("discover : positive_num_cnt = %d, %lld of them <= %lld, the sum of numbers below %lld is %lld\n", cnt, sum(0, q[i].op2), q[i].op2, q[i].op2, sum(1, q[i].op2));
            if ((cnt - sum(0, op2[i])) * nums[op2[i]] + sum(1, op2[i]) >= op1[i] * nums[op2[i]]) puts("TAK");
            else puts("NIE");
        }
    }
    return 0;
}

/*
3 8
U 1 2
U 2 4
U 3 6
Z 1 12
Z 1 13
Z 2 6
Z 2 7
Z 3 2
Z 3 3
Z 4 1

TAK
NIE
TAK
NIE
TAK
NIE
NIE
*/