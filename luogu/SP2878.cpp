//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Knights of the Round Table 圆桌骑士.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-20.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 998244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? Module(w - P) : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

const int N = 1000 + 7;

int n, m;
int a[N][N];
vector<int> v_dcc[N]; int cnt;
int dfn[N], low[N], dft;
int s[N], top;
int col[N];
bool could[N];

void tarjan(int u, int root) {
    // printf("tarjan(%d, %d)\n", u, root);
    low[u] = dfn[u] = ++dft;
    s[++top] = u;
    // int flag = 0;
    for (int v = 1; v <= n; v++) {
        if (!a[u][v]) continue;
        if (!dfn[v]) {
            tarjan(v, root);
            low[u] = min(low[u], low[v]);
            if (low[v] >= dfn[u]) {
                // flag++;
                cnt++;
                do {
                    v_dcc[cnt].push_back(s[top]);
                } while (s[top--] != v);
                v_dcc[cnt].push_back(u);
            }
        } else {
            low[u] = min(low[u], dfn[v]);
        }
    }
    // if (flag && (u != root || flag > 1)) printf("CUT point %d\n", u);
}

bool color(int id, int u, int c) {
    col[u] = c;
    for (size_t i = 0; i < v_dcc[id].size(); i++) {
        int v = v_dcc[id][i];
        if (!a[u][v]) continue;
        if (col[v]) {
            if (c == col[v]) return true;
        } else {
            if (color(id, v, 3 - c)) return true;
        }
    }
    return false;
}

signed main() {
    while (true) {
        n = read<int>();
        m = read<int>();
        if (!n && !m) break;
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++)
                a[i][j] = true;
            a[i][i] = false;
            dfn[i] = 0;
            could[i] = 0;
        }
        while (cnt) v_dcc[cnt--].clear();
        dft = 0;
        for (int i = 1, u, v; i <= m; i++) {
            u = read<int>();
            v = read<int>();
            a[u][v] = a[v][u] = false;
        }
        for (int i = 1; i <= n; i++)
            if (!dfn[i])
                tarjan(i, i);
        for (int i = 1; i <= cnt; i++) {
            // printf("v-DCC %d : { %d", i, v_dcc[i][0]);
            // for (size_t j = 1; j < v_dcc[i].size(); j++) printf(", %d", v_dcc[i][j]);
            // printf(" }\n");
            for (size_t j = 0; j < v_dcc[i].size(); j++)
                col[v_dcc[i][j]] = 0;
            if (color(i, v_dcc[i][0], 1))
                for (size_t j = 0; j < v_dcc[i].size(); j++)
                    could[v_dcc[i][j]] = 1;
        }
        int ans = 0;
        for (int i = 1; i <= n; i++)
            if (could[i]) ans++;
        write(n - ans, 10);
    }
    return 0;
}

// Create File Date : 2020-06-20