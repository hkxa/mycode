/*************************************
 * @contest:      Codeforces Round #643 (Div. 2).
 * @user_name:    hkxadpall.
 * @time:         2020-05-16.
 * @language:     C++.
 * @upload_place: Codeforces.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

bool p[2007];
stack<int> tmp;
vector<int> toQuery;
int64 ans;
int Fill;

void getPrimes()
{
    for (int i = 2; i <= 2000; i++) {
        if (p[i]) continue;
        for (int j = i + i; j <= 2000; j += i) 
            p[j] = 1;
    }
}

int query(int64 Q)
{
    printf("? %lld\n", Q);
    fflush(stdout);
    return read<int>();
}

void clearTmp(int64 ret)
{
    while (!tmp.empty()) {
        if (ret % tmp.top() == 0) toQuery.push_back(tmp.top());
        else Fill = tmp.top();
        tmp.pop();
    }
}

int64 getPow(int p) 
{
    int64 ret = 1;
    while (ret * p <= 1000000000) ret *= p;
    return ret;
}

signed main()
{
    getPrimes();

    int T = read<int>(), cnt;
    while (T--) {
        toQuery.clear();
        int64 Bond = 1;
        for (int i = 2; i <= 630; i++) {
            // printf("i = %d\n", i);
            if (p[i]) continue;
            if (i > 1000000000000000000ll / Bond) {
                clearTmp(query(Bond));
                Bond = 1;
            }
            tmp.push(i);
            Bond *= i;
        }
        clearTmp(query(Bond));
        if (toQuery.size() & 1) toQuery.push_back(Fill);
        ans = 2;
        for (uint32 i = 0; i < toQuery.size(); i += 2) {
            int &p1 = toQuery[i], &p2 = toQuery[i + 1];
            int ret = query(getPow(p1) * getPow(p2));
            cnt = 1;
            while (ret % p1 == 0) {
                cnt++;
                ret /= p1;
            }
            ans *= cnt;
            cnt = 1;
            while (ret % p2 == 0) {
                cnt++;
                ret /= p2;
            }
            ans *= cnt;
        }
        printf("! %lld\n", max(ans, 8LL));
        fflush(stdout);
    }
    return 0;
}