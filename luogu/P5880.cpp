/*************************************
 * @problem:      P5880 【政治】划分.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-01-29.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define P 1000000007

int64 n;
int64 circle_count, sum, ans;

int main()
{
    n = read<int64>();
    for (int64 i = 1, a; i <= n; i++) {
        a = read<int64>();
        if (!a) continue;
        switch (i) {
            case 1:
                ans = (1 + a * (a + 1) / 2) % P;
                sum = a * 2;
                break;
            case 2:
                ans = (ans + sum * a) % P;
                if (!sum) ans = (2 + a * (a - 1)) % P;
                else ans = (ans + a * (a - 1)) % P;
                circle_count = a;
                break;
            default:
                ans += sum * a + circle_count * a * i * 2;
                if (!sum && !circle_count) ans = 2 + a * (a - 1) * i;
                else ans += a * (a - 1) * i;
                sum += i * a * 2;
                ans %= P;
        }
    }
    write(!ans ? 1 : ans, 10);
    return 0;
}