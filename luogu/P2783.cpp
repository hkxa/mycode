//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      有机化学之神偶尔会做作弊.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-15.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 998244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? Module(w - P) : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

const int N = 10007;

int n, m;
vector<int> G[N], H[N];
int dfn[N], low[N], dft;
int fam[N], cnt;

int s[N], top;
bool ins[N];

void tarjan(int u, int fa) {
    low[u] = dfn[u] = ++dft;
    s[++top] = u;
    ins[u] = true;
    for (size_t i = 0; i < G[u].size(); i++) {
        if (G[u][i] == fa) continue;
        if (!dfn[G[u][i]]) {
            tarjan(G[u][i], u);
            low[u] = min(low[u], low[G[u][i]]);
        } else if (ins[G[u][i]]) {
            low[u] = min(low[u], dfn[G[u][i]]);
        }
    }
    if (dfn[u] == low[u]) {
        cnt++;
        while (low[s[top]] >= low[u]) {
            ins[s[top]] = false;
            fam[s[top--]] = cnt;
        }
    }
}

int fa[N][18], dep[N];
bool vis[N];

void dfs(int u, int f) {
    fa[u][0] = f;
    vis[u] = 1;
    dep[u] = dep[f] + 1;
    for (int k = 0; k < 17 && fa[u][k]; k++)
        fa[u][k + 1] = fa[fa[u][k]][k];
    for (size_t i = 0; i < H[u].size(); i++) {
        if (!vis[H[u][i]]) dfs(H[u][i], u);
    }
}

void jump(int &u, int k) {
    for (int i = 0; i < 18; i++)
        if (k & (1 << i)) u = fa[u][i];
}

int lca(int u, int v) {
    if (dep[u] > dep[v]) jump(u, dep[u] - dep[v]);
    else jump(v, dep[v] - dep[u]);
    if (u == v) return u;
    for (int k = 17; fa[u][0] != fa[v][0]; k--)
        if (fa[u][k] != fa[v][k]) {
            u = fa[u][k];
            v = fa[v][k];
        }
    return fa[u][0];
}

void BitWrite(int num) {
    if (!num) return;
    BitWrite(num >> 1);
    putchar(48 | (num & 1));
}

signed main() {
    n = read<int>();
    m = read<int>();
    for (int i = 1, a, b; i <= m; i++) {
        a = read<int>();
        b = read<int>();
        G[a].push_back(b);
        G[b].push_back(a);
    }
    for (int i = 1; i <= n; i++)
        if (!dfn[i]) tarjan(i, 0);
    for (int i = 1; i <= n; i++) {
        for (size_t j = 0; j < G[i].size(); j++) {
            if (fam[i] != fam[G[i][j]]) {
                H[fam[i]].push_back(fam[G[i][j]]);
            }
        }
    }
    dfs(1, 0);
    int Q = read<int>(), a, b;
    while (Q--) {
        a = fam[read<int>()];
        b = fam[read<int>()];
        BitWrite(dep[a] + dep[b] - 2 * dep[lca(a, b)] + 1);
        putchar(10);
    }
    return 0;
}

// Create File Date : 2020-06-15