/*************************************
 * @problem:      CF1904D.
 * @author:       CharmingLakesideJewel.
 * @time:         2023-12-10.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

// #define USE_FREAD  // 使用 fread  读入，去注释符号
// #define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;


const int N = 2e5 + 7;


namespace STmax {
    int st[N][20], lg[N];
    void init(int n, int *a) {
        for (int i = 1; i <= n; ++i) st[i][0] = a[i];
        for (int step = 1; (1 << step) <= n; ++step) {
            for (int i = 1; i + (1 << step) - 1 <= n; ++i) {
                st[i][step] = max(st[i][step - 1], st[i + (1 << (step - 1))][step - 1]);
            }
        }
        lg[1] = 0;
        for (int i = 2; i <= n; ++i) lg[i] = lg[i >> 1] + 1;
    }
    int query(int l, int r) {
        int k = lg[r - l + 1];
        return max(st[l][k], st[r - (1 << k) + 1][k]);
    }
}

namespace STmin {
    int st[N][20], lg[N];
    void init(int n, int *a) {
        for (int i = 1; i <= n; ++i) st[i][0] = a[i];
        for (int step = 1; (1 << step) <= n; ++step) {
            for (int i = 1; i + (1 << step) - 1 <= n; ++i) {
                st[i][step] = min(st[i][step - 1], st[i + (1 << (step - 1))][step - 1]);
            }
        }
        lg[1] = 0;
        for (int i = 2; i <= n; ++i) lg[i] = lg[i >> 1] + 1;
    }
    int query(int l, int r) {
        int k = lg[r - l + 1];
        return min(st[l][k], st[r - (1 << k) + 1][k]);
    }
}

int T, n;
int a[N], b[N];
bool archievable[N];
int closet[N];

signed main() {
    kin >> T;
    while (T--) {
        kin >> n;
        for (int i = 1; i <= n; ++i) kin >> a[i];
        for (int i = 1; i <= n; ++i) kin >> b[i];
        STmax::init(n, a);
        STmin::init(n, b);
        for (int i = 1; i <= n; ++i) {
            archievable[i] = false;
            closet[i] = 0;
        }
        for (int i = 1; i <= n; ++i) {
            closet[a[i]] = i;
            if (closet[b[i]]) {
                if (STmax::query(closet[b[i]], i) <= b[i] && STmin::query(closet[b[i]], i) >= b[i]) {
                    archievable[i] = true;
                }
            }
        }
        for (int i = 1; i <= n; ++i) {
            closet[i] = 0;
        }
        for (int i = n; i >= 1; --i) {
            closet[a[i]] = i;
            if (closet[b[i]]) {
                if (STmax::query(i, closet[b[i]]) <= b[i] && STmin::query(i, closet[b[i]]) >= b[i]) {
                    archievable[i] = true;
                }
            }
        }
        bool flag = true;
        for (int i = 1; i <= n; ++i) {
            if (!archievable[i]) {
                flag = false;
                break;
            }
        }
        if (flag) kout << "YES\n";
        else kout << "NO\n";
    }
    return 0;
}