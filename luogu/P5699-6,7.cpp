
#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
#include<ctime>
#include<cstring>
#include<vector>
using namespace std;
#define MAXN 256
#define MAXD 8
#define DB long double
#define INF 10e18
#define SE second
int n,k;
int A[MAXN+5],sA[MAXN+5],val[MAXN+5],best[MAXN+5];
int sx,sy,cnt[MAXN+5],pcnt[MAXN+5],vis[MAXN+5];
DB T,ans,st,sdist,P[MAXN+5][MAXN+5],Pw[MAXN+5][MAXD+1];
vector< pair<int,int> > G[MAXN+5];
int num;
int read()  
{  
    int x=0,f=1;char s=getchar();  
    while(s<'0'||s>'9'){if(s=='-')f=-1;s=getchar();}  
    while(s>='0'&&s<='9'){x=x*10+s-'0';s=getchar();}  
    return x*f;  
}
void check(int L,int R,int dep)
{
	if(L==R){Pw[sA[L]][dep]=1;return ;}
	int mid=(L+R)/2;
	check(L,mid,dep+1);
	check(mid+1,R,dep+1);
	for(int i=L;i<=R;i++)
		Pw[sA[i]][dep]=0;
	for(int i=L;i<=mid;i++)
		for(int j=mid+1;j<=R;j++)
		{
			Pw[sA[i]][dep]+=Pw[sA[i]][dep+1]*Pw[sA[j]][dep+1]*P[sA[i]][sA[j]];
			Pw[sA[j]][dep]+=Pw[sA[i]][dep+1]*Pw[sA[j]][dep+1]*P[sA[j]][sA[i]];
		}
	if(L==1)sdist+=(Pw[1][dep+1]-Pw[1][dep])*val[dep];
}
void slove(int x)
{
	if(vis[x])return ;
	vis[x]=1;
	A[++num]=x;
	for(int i=0;i<G[x].size();i++)
	{
		int xnt=G[x][i].SE;
		slove(xnt);
	}
}
int main()
{
	freopen("P5699_addition_file\\match7.in","r",stdin);
	freopen("P5699_addition_file\\match7.usr","w",stdout);
	srand(time(NULL));
	n=read();
	while(n>(1<<k))k++;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
		{
			cin>>P[i][j];
			if(P[i][j]==1)cnt[i]++;
		}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
		if(P[i][j]==1)G[i].push_back(make_pair(cnt[j],j));
	for(int i=1;i<=n;i++)
	sort(G[i].begin(),G[i].end());
	slove(1);
	slove(193);
	slove(65);//for #7
	slove(129);
	/*for(int i=1;i<=n;i++)
	{printf("%d ",cnt[i]);pcnt[cnt[i]]++;}
	printf("\n");
	for(int i=1;i<=k;i++)printf("%d ",pcnt[i]);
	printf("\n");
	for(int i=1;i<=n;i++)if(cnt[i]==k-2)printf("%d ",i);
	printf("\n");//打表专用 */ 
	for(int i=k;i>=0;i--)
		scanf("%d",&val[i]);
	for(int i=1;i<=n;i++)sA[i]=A[i];
	sdist=0;
	check(1,n,1);
	sdist+=Pw[1][1]*val[0];
	ans=sdist;
	int P=1000;
	//cout<<ans<<endl;
	for(int i=1;i<=n;i++)
	printf("%d\n",A[i]);
}