/*************************************
 * problem:      P1471 方差. 
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-11-07.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

/**
 * 50pts todo
 * 这是错误解法, t1 与 t2 实现时应在一份函数两个数组来实现。
 * 因为t2依赖t1, 故分开实现会出现很多奇怪的小问题。
 * 2019.11.07 现在不想解决, 留个坑和条思路, 有空时填坑。
 */

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, m;
int op, x, y; double k;
double a[100007];

namespace v2 {
    inline void pushdown(int p);
}

namespace v1 {
    struct data { int size; double val, lazy; data() : size(0), val(0), lazy(0) {} } t[100007 << 4];  
    inline void pushdown(int p) {
        v2::pushdown(p);
        if (t[p].lazy) {
            if (t[p << 1].size) { t[p << 1].val += t[p].lazy * t[p << 1].size; t[p << 1].lazy += t[p].lazy; }
            if (t[p << 1 | 1].size) { t[p << 1 | 1].val += t[p].lazy * t[p << 1 | 1].size; t[p << 1 | 1].lazy += t[p].lazy; }
            t[p].lazy = 0;
        }
    }
    inline void pushup(int p) { t[p].val = t[p << 1].val + t[p << 1 | 1].val; t[p].lazy = 0; }
    inline void build(int p, int l, int r) {
        if (l > r) return; if (l == r) { t[p].val = a[l]; t[p].size = 1; return; } int mid = (l + r) >> 1; 
        build(p << 1, l, mid); build(p << 1 | 1, mid + 1, r); t[p].size = t[p << 1].size + t[p << 1 | 1].size; pushup(p);
    }
    inline void modify(int p, int l, int r, int ml, int mr, double k) {
        if (l > mr || r < ml) return; if (l >= ml && r <= mr) { t[p].val += k * t[p].size; t[p].lazy += k; return; }
        int mid = (l + r) >> 1; pushdown(p);
        modify(p << 1, l, mid, ml, mr, k); modify(p << 1 | 1, mid + 1, r, ml, mr, k);
        pushup(p);
    }
    inline double query(int p, int l, int r, int ml, int mr) {
        if (l > mr || r < ml) return 0; if (l >= ml && r <= mr) return t[p].val; int mid = (l + r) >> 1; 
        pushdown(p); return query(p << 1, l, mid, ml, mr) + query(p << 1 | 1, mid + 1, r, ml, mr);
    }
}

namespace v2 {
    struct data { int size; double val, lazy; data() : size(0), val(0), lazy(0) {} } t[100007 << 4];  
    inline void pushdown(int p) {
        if (t[p].lazy) {
            if (t[p << 1].size) { t[p << 1].val += 2 * t[p].lazy * v1::t[p << 1].val + t[p << 1].size * t[p].lazy * t[p].lazy; t[p << 1].lazy += t[p].lazy; }
            if (t[p << 1 | 1].size) { t[p << 1 | 1].val += 2 * t[p].lazy * v1::t[p << 1 | 1].val + t[p << 1 | 1].size * t[p].lazy * t[p].lazy; t[p << 1 | 1].lazy += t[p].lazy; }
            t[p].lazy = 0;
        }
    }
    inline void pushup(int p) { t[p].val = t[p << 1].val + t[p << 1 | 1].val; t[p].lazy = 0; }
    inline void build(int p, int l, int r) {
        if (l > r) return; if (l == r) { t[p].val = a[l] * a[l]; t[p].size = 1; return; } int mid = (l + r) >> 1; 
        build(p << 1, l, mid); build(p << 1 | 1, mid + 1, r); t[p].size = t[p << 1].size + t[p << 1 | 1].size; pushup(p);
    }
    inline void modify(int p, int l, int r, int ml, int mr, double k) {
        if (l > mr || r < ml) return; if (l >= ml && r <= mr) { t[p].val += v1::t[p].val * (k * 2) + (r - l + 1) * (k * k); t[p].lazy += k; return; }
        int mid = (l + r) >> 1; pushdown(p);
        modify(p << 1, l, mid, ml, mr, k); modify(p << 1 | 1, mid + 1, r, ml, mr, k);
        pushup(p);
    }
    inline double query(int p, int l, int r, int ml, int mr) {
        if (l > mr || r < ml) return 0; if (l >= ml && r <= mr) return t[p].val; int mid = (l + r) >> 1; 
        pushdown(p); return query(p << 1, l, mid, ml, mr) + query(p << 1 | 1, mid + 1, r, ml, mr);
    }
}

inline double pow2(double x) { return x * x; }

int main()
{
    n = read<int>();
    m = read<int>();
    for (int i = 1; i <= n; i++) scanf("%lf", a + i);
    v1::build(1, 1, n);
    v2::build(1, 1, n);
    while (m--) {
        op = read<int>();
        x = read<int>();
        y = read<int>();
        if (op == 1) {
            scanf("%lf", &k);
            v2::modify(1, 1, n, x, y, k);
            v1::modify(1, 1, n, x, y, k);
        } else if (op == 2) {
            printf("%.4lf\n", v1::query(1, 1, n, x, y) / (y - x + 1));
        } else {
            printf("%.4lf\n", v2::query(1, 1, n, x, y) / (y - x + 1) - pow2(v1::query(1, 1, n, x, y) / (y - x + 1)));
        }
    }
    return 0;
}