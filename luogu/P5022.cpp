/*************************************
 * problem:      P5022 旅行.
 * user ID:      85848.
 * user name:    hkxadpall.
 * time:         2019-08-30.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define ForInVecInt(vectorName, iteratorName) for (vector<int>::iterator iteratorName = vectorName.begin(); iteratorName != vectorName.end(); iteratorName++)
#define MemWithNum(array, num) memset(array, num, sizeof(array))
#define Clear(array) MemWithNum(array, 0)
#define MemBint(array) MemWithNum(array, 0x3f)
#define MemInf(array) MemWithNum(array, 0x7f)
#define MemEof(array) MemWithNum(array, -1)

int n, m;
vector<int> G[5007];
pair<int, int> edges[5007], del;
int tmp[5007] = {0}, ans[5007];
bool vis[5007] = {0};
int cnt;

void dfs(int u, int fa)
{
    if (vis[u]) {
        cnt = 0;
        return;
    }
    vis[u] = true;
    tmp[++cnt] = u;
    ForInVecInt(G[u], it) {
        if (*it == fa) continue;
        if (u == del.first && *it == del.second) continue;
        if (u == del.second && *it == del.first) continue;
        dfs(*it, u);
    }
}


int main()
{
    scanf("%d%d", &n, &m);
    int u, v;
    for (int i = 1; i <= m; i++) {
        scanf("%d%d", &u, &v);
        G[u].push_back(v);
        G[v].push_back(u);
        edges[i] = make_pair(u, v);
    }
    for (int i = 1; i <= n; i++) {
        sort(G[i].begin(), G[i].end());
    }
    MemBint(ans);
    if (m == n - 1) {
        cnt = 0;
        del = make_pair(0, 0);
        dfs(1, 0);
        for (int i = 1; i <= n; i++) {
            printf("%d ", tmp[i]);
        }
    } else {
        int betterAnswer = 0;
        for (int i = 1; i <= m; i++) {
            cnt = 0;
            del = make_pair(edges[i].first, edges[i].second);
            Clear(vis);
            dfs(1, 0);
            if (cnt < n) continue;
            // printf("del path %d<-->%d\n", del.first, del.second);
            betterAnswer = false;
            for (int i = 1; i <= n; i++) {
                if (tmp[i] < ans[i]) {
                    betterAnswer = true;
                    break;
                } else if (tmp[i] > ans[i]) {
                    betterAnswer = false;
                    break;
                }
            }
            if (!betterAnswer) continue;
            for (int i = 1; i <= n; i++) {
                ans[i] = tmp[i];
            }
        }
        for (int i = 1; i <= n; i++) {
            printf("%d ", ans[i]);
        }
    }
    return 0;
}