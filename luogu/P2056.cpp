//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      [ZJOI2007]捉迷藏.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-12.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#ifndef DEBUG
# define fprintf(...) {}
# define fflush(...) {}
# define debug(...) {}
#endif

#define MEM_ARR(arr, siz, v) { for (int i = 0; i < siz + 5; i++) arr[i] = v; }

// #define int int64

const int N = 100007;

int n, Q, CntWhite;
vector<pair<int, int> > G[N];
int fa[N]; // 点分树上的父亲
int siz[N], vis[N];
enum POINT_COLOR { black = 0, white = 1 } type[N];

void reverseColor(int x) {
    if (type[x] == black) {
        type[x] = white;
        CntWhite++;
    } else {
        type[x] = black;
        CntWhite--;
    }
}

struct IntegerPowerOfTwo {
    int k[37];
    // 预处理
    void Pretreat() {
        for (int i = 0; i < 20; i++) k[(1 << i) % 37] = i;
    }
    int operator () (int a) {
        return k[a % 37];
    }
} Log2;

struct MultiplyLCA {
    int dep[N], dis[N];
    int f[N][20];
    // 预处理
    void Pretreat(int u, int Father, int Dis = 0) {
        f[u][0] = Father;
        dep[u] = dep[Father] + 1;
        dis[u] = Dis;
        for (int i = 0; i < 19 && f[u][i]; i++)
            f[u][i + 1] = f[f[u][i]][i];
        for (size_t i = 0; i < G[u].size(); i++)
            if (G[u][i].first != Father) 
                Pretreat(G[u][i].first, u, Dis + G[u][i].second);
    }
    // lowbit 优化跳 k 级祖先（时间复杂度 Θ(popcount(k))）
    void jump(int &u, int k) {
        while (k) {
            u = f[u][Log2(k & -k)];
            k -= (k & -k);
        }
    }
    int getLca(int u, int v) {
        if (dep[u] < dep[v]) jump(v, dep[v] - dep[u]);
        else if (dep[u] > dep[v]) jump(u, dep[u] - dep[v]);
        if (u == v) return u; // "剪枝"优化
        for (int k = 18; f[u][0] != f[v][0]; k--) // 循环终止条件优化
            if (f[u][k] != f[v][k]) {
                u = f[u][k];
                v = f[v][k];
            }
        return f[u][0];
    }
    int calcDist(int u, int v) {
        return dis[u] + dis[v] - 2 * dis[getLca(u, v)];
    }
} Graph;

struct Heap {
    priority_queue<int> que, del;
    size_t size() { return que.size() - del.size(); }
    void insert(int x) { que.push(x); }
    void remove(int x) { del.push(x); }
    void pushDelTag() {
        while (!que.empty() && !del.empty() && que.top() == del.top()) {
            que.pop();
            del.pop();
        }
    }
    // [Warn] 未检查 que 是否为空
    int getTop() {
        pushDelTag();
        // fprintf(stderr, "[Info]   [function = %-15s] getTop %d\n", __FUNCTION__, que.top()); fflush(stderr);
        return que.top();
    }
    // [Warn] 未检查 que 的 size 是否 >= 2
    int getTopTwo() {
        int x = getTop(); remove(x);
        int y = getTop(); insert(x);
        fprintf(stderr, "[Info]   [function = %-15s] getTopTwo %d + %d\n", __FUNCTION__, x, y); fflush(stderr);
        return x + y;
    }
    int top() {
        if (size()) return getTop();
        else return INT_MIN;
    }
    int getAnswer() {
        switch (size()) {
            case 0 : return INT_MIN;        // 无白点
            case 1 : return getTop();       // 最远距离为链长度
            default : return getTopTwo();   // size >= 2
        }
    }
} disU[N], disF[N], all;
// disU : 维护原树子树到 u 的 dis
// disF : 维护原树子树到 u 的点分树父亲的 dis
// all  : 维护总 answer

struct HeapDebuger {
    Heap heap, CopyVersion;
    void outputHeapInfo() {
        CopyVersion = heap;
        fprintf(stderr, "[Info]   [function = %-15s] Heap = { ", __FUNCTION__); fflush(stderr);
        if (!CopyVersion.size()) fprintf(stderr, "NULL"); fflush(stderr);
        while (CopyVersion.size()) {
            fprintf(stderr, "%d", CopyVersion.getTop()); fflush(stderr);
            CopyVersion.remove(CopyVersion.getTop());
            if (CopyVersion.size()) fprintf(stderr, ", "); fflush(stderr);
        }
        fprintf(stderr, " } = { "); fflush(stderr);
        
        CopyVersion = heap;
        if (!CopyVersion.que.size()) fprintf(stderr, "NULL"); fflush(stderr);
        while (CopyVersion.que.size()) {
            fprintf(stderr, "%d", CopyVersion.que.top()); fflush(stderr);
            CopyVersion.que.pop();
            if (CopyVersion.que.size()) fprintf(stderr, ", "); fflush(stderr);
        }
        fprintf(stderr, " } - { "); fflush(stderr);
        if (!CopyVersion.del.size()) fprintf(stderr, "NULL"); fflush(stderr);
        while (CopyVersion.del.size()) {
            fprintf(stderr, "%d", CopyVersion.del.top()); fflush(stderr);
            CopyVersion.del.pop();
            if (CopyVersion.del.size()) fprintf(stderr, ", "); fflush(stderr);
        }
        fprintf(stderr, " } [size = %u]\n", heap.size()); fflush(stderr);
    }
    void operator () (Heap *h, const char *HeapName = "HeapArray") {
        for (int i = 1; i <= n; i++) {
            fprintf(stderr, "[Info]   [function = %-15s] %s[%d] : \n", __FUNCTION__, HeapName, i); fflush(stderr);
            heap = h[i];
            outputHeapInfo();
        }
    }
    void operator () (Heap h, const char *HeapName = "HeapArray") {
        heap = h;
        fprintf(stderr, "[Info]   [function = %-15s] %s : \n", __FUNCTION__, HeapName); fflush(stderr);
        outputHeapInfo();
    }
} debug;

struct PointDivideTree {
    // 获得所在子树所有点的 siz
    void getSubtreeSiz(int u) {
        vis[u] = true;
        siz[u] = 1;
        for (size_t i = 0; i < G[u].size(); i++) {
            int v = G[u][i].first;
            if (!vis[v]) {
                getSubtreeSiz(v);
                siz[u] += siz[v];
            }
        }
        vis[u] = false;
    }

    // 获得所在子树的根（已维护 siz)
    void getSubtreeRoot(int u, const int sizAll, int &root, int &rootSiz) {
        vis[u] = true;
        int Tmax = 0;
        for (size_t i = 0; i < G[u].size(); i++) {
            int v = G[u][i].first;
            if (!vis[v]) {
                getSubtreeRoot(v, sizAll, root, rootSiz);
                if (siz[v] > Tmax) Tmax = siz[v];
            }
        }
        if (sizAll - siz[u] > Tmax) Tmax = sizAll - siz[u];
        if (Tmax < rootSiz) {
            root = u;
            rootSiz = Tmax;
        }
        vis[u] = false;
    }

    // 获得所在子树的根（已封装）
    inline int getRoot(int u) {
        int root, rootSiz = n + 1;
        getSubtreeSiz(u);
        getSubtreeRoot(u, siz[u], root, rootSiz);
        return root;
    }

    // 获得初始堆
    void GetInitiallyHeap(int u, int root, int rootFa) { 
        vis[u] = true;
        // disU[root].insert(Graph.calcDist(u, root));
        if (rootFa) disF[root].insert(Graph.calcDist(u, rootFa));
        for (size_t i = 0; i < G[u].size(); i++)
            if (!vis[G[u][i].first])
                GetInitiallyHeap(G[u][i].first, root, rootFa);
        vis[u] = false;
    }

    // 获得点分树（预处理）
    void GetPointDivideTree(int u) {
        disU[u].insert(0); // 自身
        GetInitiallyHeap(u, u, fa[u]);
        vis[u] = true;
        for (size_t i = 0; i < G[u].size(); i++) {
            int v = G[u][i].first;
            if (!vis[v]) {
                v = getRoot(v);
                fa[v] = u;
                GetPointDivideTree(v);
                // disU[v].insert(0);
                // disF[v].insert(Graph.calcDist(u, v));
                disU[u].insert(disF[v].top());
            }
        }
        all.insert(disU[u].getAnswer());
        vis[u] = false;
    }
} pvt; // Point Divide Tree

struct ProblemSolver {
    // 总预处理
    void pretreat() {                           fprintf(stderr, "[Info]   [function = %-15s] enter solver.pretreat()\n", __FUNCTION__); fflush(stderr);
        Log2.Pretreat();                        fprintf(stderr, "[Info]   [function = %-15s] finish Log2.pretreat()\n", __FUNCTION__); fflush(stderr);
        int root = pvt.getRoot(1);
        Graph.Pretreat(root, 0);                fprintf(stderr, "[Info]   [function = %-15s] finish Graph.pretreat()\n", __FUNCTION__); fflush(stderr);
        pvt.GetPointDivideTree(root);           fprintf(stderr, "[Info]   [function = %-15s] finish pvt.pretreat()\n", __FUNCTION__); fflush(stderr);
    }
    // 树点黑变白
    void turnOn(int x) {
        all.remove(disU[x].getAnswer());
        fprintf(stderr, "[Info]   [function = %-15s] all remove %d\n", __FUNCTION__, disU[x].getAnswer()); fflush(stderr);
        disU[x].insert(0); // x 节点自身
        all.insert(disU[x].getAnswer());
        fprintf(stderr, "[Info]   [function = %-15s] all insert %d\n", __FUNCTION__, disU[x].getAnswer()); fflush(stderr);
        // 维护 all 堆的数据正确性

        int OriginNode = x;
        while (fa[x]) {
            // 跳父亲
            int f = fa[x];
            all.remove(disU[f].getAnswer());                   // all 先删除冗余信息
            fprintf(stderr, "[Info]   [function = %-15s] all remove %d\n", __FUNCTION__, disU[f].getAnswer()); fflush(stderr);
            if (disU[f].size()) disU[f].remove(disF[x].top()); // disU 先删除冗余信息
            fprintf(stderr, "[Info]   [function = %-15s] disU[%d] remove %d\n", __FUNCTION__, f, disF[x].top()); fflush(stderr);
            disF[x].insert(Graph.calcDist(OriginNode, f));              // disF 直接维护新信息
            fprintf(stderr, "[Info]   [function = %-15s] disF[%d] insert %d\n", __FUNCTION__, x, Graph.calcDist(OriginNode, f)); fflush(stderr);
            debug(disF[x], "disF[x]");
            disU[f].insert(disF[x].top());                     // disU 加入修改后的信息 
            fprintf(stderr, "[Info]   [function = %-15s] disU[%d] insert %d\n", __FUNCTION__, f, disF[x].top()); fflush(stderr);
            all.insert(disU[f].getAnswer());                   // all 加入修改后的信息
            fprintf(stderr, "[Info]   [function = %-15s] all insert %d\n", __FUNCTION__, disU[f].getAnswer()); fflush(stderr);
            x = f;
        }
    }
    // 树点白变黑
    void turnOff(int x) {
        all.remove(disU[x].getAnswer());
        fprintf(stderr, "[Info]   [function = %-15s] all remove %d\n", __FUNCTION__, disU[x].getAnswer()); fflush(stderr);
        disU[x].remove(0); // x 节点自身
        all.insert(disU[x].getAnswer());
        fprintf(stderr, "[Info]   [function = %-15s] all insert %d\n", __FUNCTION__, disU[x].getAnswer()); fflush(stderr);
        // 维护 all 堆的数据正确性

        int OriginNode = x;
        while (fa[x]) {
            // 跳父亲
            int f = fa[x];
            all.remove(disU[f].getAnswer());                   // all 先删除冗余信息
            fprintf(stderr, "[Info]   [function = %-15s] all remove %d\n", __FUNCTION__, disU[f].getAnswer()); fflush(stderr);
            if (disU[f].size()) disU[f].remove(disF[x].top()); // disU 先删除冗余信息
            fprintf(stderr, "[Info]   [function = %-15s] disU[%d] remove %d\n", __FUNCTION__, f, disF[x].top()); fflush(stderr);
            disF[x].remove(Graph.calcDist(OriginNode, f));              // disF 直接维护新信息
            fprintf(stderr, "[Info]   [function = %-15s] disF[%d] remove %d\n", __FUNCTION__, x, Graph.calcDist(OriginNode, f)); fflush(stderr);
            debug(disF[x], "disF[x]");
            disU[f].insert(disF[x].top());                     // disU 加入修改后的信息 
            fprintf(stderr, "[Info]   [function = %-15s] disU[%d] insert %d\n", __FUNCTION__, f, disF[x].top()); fflush(stderr);
            all.insert(disU[f].getAnswer());                   // all 加入修改后的信息
            fprintf(stderr, "[Info]   [function = %-15s] all insert %d\n", __FUNCTION__, disU[f].getAnswer()); fflush(stderr);
            x = f;
        }
    }
    int getAnswer() {
        int ret = all.getTop();
        if (ret < -1e8) return 0;
        else return ret;
    }
} solver;

char getOpt() {
    static char buf[3];
    scanf("%s", buf);
    return buf[0];
}

signed main() {
    CntWhite = n = read<int>();
    MEM_ARR(type, n, white);                    fprintf(stderr, "[Info]   [function = %-15s] set type[1..n] to white\n", __FUNCTION__); fflush(stderr);
    for (int i = 1, a, b, c; i < n; i++) {
        a = read<int>();
        b = read<int>();
        // c = read<int>();
        G[a].push_back(make_pair(b, 1));
        G[b].push_back(make_pair(a, 1));
    }
    solver.pretreat();                          fprintf(stderr, "[Info]   [function = %-15s] passing solver.pretreat()\n", __FUNCTION__); fflush(stderr);
    Q = read<int>();
    debug(all, "all");
    debug(disU, "disU");
    debug(disF, "disF");
    while (Q--) {
        if (getOpt() == 'C') {                  fprintf(stderr, "[Info]   [function = %-15s] Receive operation C(hange)\n", __FUNCTION__); fflush(stderr);
            // C(hange)
            int x = read<int>();
            if (type[x] == white) solver.turnOff(x);
            else solver.turnOn(x);
            reverseColor(x);
            debug(all, "all");
            debug(disU, "disU");
            debug(disF, "disF");
        } else {                                fprintf(stderr, "[Info]   [function = %-15s] Receive operation A(sk)\n", __FUNCTION__); fflush(stderr);
            // G(ame)
            fprintf(stderr, "[output] [function = %-15s] ", __FUNCTION__); fflush(stderr);
            if (CntWhite == 0) puts("-1");
            else write(solver.getAnswer(), 10);
            fflush(stdout);
        }
    }
    return 0;
}

// Create File Date : 2020-06-12