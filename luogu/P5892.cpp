/*************************************
 * @problem:      [IOI2014]holiday 假期.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-26.
 * @language:     C++.
 * @fastio_ver:   20200820.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore space, \t, \r, \n
            ch = getchar();
            while (ch == ' ' || ch == '\t' || ch == '\r' || ch == '\n') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            Int flag = 1;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = -1, endch = getchar();
            i = endch & 15;
            while (isdigit(endch = getchar())) i = (i << 3) + (i << 1) + (endch & 15);
            if (flag == -1) i = -i;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            Int flag = 1;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = -1, endch = getchar();
            Int i = endch & 15;
            while (isdigit(endch = getchar())) i = (i << 3) + (i << 1) + (endch & 15);
            if (flag == -1) i = -i;
            return i;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64
// #define int128 int64
// #define int128 __int128

const int N = 1e5 + 7, SIZ = N << 5;

int b[N], a[N], s, d, n, top, T[N];
int L[SIZ], R[SIZ], cnt[SIZ], tot;
int64 ans, tr[SIZ];

inline void upd(int &now, int pre, int l, int r, int x) {
    now = ++tot;
    L[now] = L[pre];
    R[now] = R[pre];
    tr[now] = tr[pre] + b[x];
    cnt[now] = cnt[pre] + 1;
    // if (l == r) fprintf(stderr, "upd final node id %d value %d pre %d\n", now, x, pre);
    if (l == r) return;
    int mid = (l + r) >> 1;
    if (mid >= x) upd(L[now], L[pre], l, mid, x);
    else upd(R[now], R[pre], mid + 1, r, x);
}

int64 query(int now, int pre, int l, int r, int k) {
    // if (cnt[now] - cnt[pre] <= k) fprintf(stderr, "~ query final node id %d value %d pre %d (ret = %d)\n", now, k, pre, tr[now] - tr[pre]);
    if (cnt[now] - cnt[pre] <= k) return tr[now] - tr[pre];
    // if (l == r) fprintf(stderr, "^ query final node id %d value %d pre %d (ret = %d)\n", now, k, pre, (tr[now] - tr[pre]) / (cnt[now] - cnt[pre]) * k);
    if (l == r) return (tr[now] - tr[pre]) / (cnt[now] - cnt[pre]) * k;
    int mid = (l + r) >> 1, num = cnt[R[now]] - cnt[R[pre]];
    if (num < k) return tr[R[now]] - tr[R[pre]] + query(L[now], L[pre], l, mid, k - num);
    else return query(R[now], R[pre], mid + 1, r, k);
}

void solve1(int l, int r, int L, int R) {
    if (l > r) return;
    int mid = (l + r) >> 1, id = L;
    int64 num = 0;
    for (int i = L; i <= R; ++i) {
        int kk = i - mid + s - mid;
        if (i == s) kk = i - mid;
        if (kk > d) continue;
        int64 x = query(T[i], T[mid - 1], 1, top, d - kk);
        if (x > num) num = x, id = i, ans = max(ans, x);
    }
    solve1(l, mid - 1, L, id);
    solve1(mid + 1, r, id, R);
}

void solve2(int l, int r, int L, int R) {
    if (l > r) return;
    int mid = (l + r) >> 1, id = R;
    int64 num = 0;
    for (int i = L; i <= R; ++i) {
        int kk = mid - i + mid - s;
        if (i == s) kk = mid - i;
        if (kk > d) continue;
        int64 x = query(T[mid], T[i - 1], 1, top, d - kk);
        if (x > num) num = x, id = i, ans = max(ans, x);
    }
    solve2(l, mid - 1, L, id);
    solve2(mid + 1, r, id, R);
}

int main() {
    read >> n >> s >> d;
    s++;
    for (int i = 1; i <= n; ++i) {
        read >> a[i];
        b[i] = a[i];
    }
    sort(b + 1, b + n + 1);
    top = unique(b + 1, b + n + 1) - b - 1;
    for (int i = 1; i <= n; ++i) {
        a[i] = lower_bound(b + 1, b + top + 1, a[i]) - b;
        upd(T[i], T[i - 1], 1, top, a[i]);
        // fprintf(stderr, "a[%d] = %d (original : %d)\n", i, a[i], b[a[i]]);
    }
    // fprintf(stderr, "passing #1\n");
    solve1(1, s, s, n);
    // fprintf(stderr, "passing #2\n");
    solve2(s, n, 1, s);
    // fprintf(stderr, "passing #3\n");
    write << ans << '\n';
    return 0;
}