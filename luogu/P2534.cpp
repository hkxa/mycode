/*************************************
 * problem:      P2534 [AHOI2012]铁盘整理.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-07-31.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, a[53], b[53];

inline int h() 
{
    int cnt = 0;
    for (int i = 1; i <= n; ++i)
        cnt += abs(a[i] - a[i + 1]) != 1;
    return cnt;
}

bool dfs(int g, int f, int pre) 
{
    if (!h()) return true;
    if (g + h() > f) return false;
    for (int i = 1; i <= n; i++) {
        if (i == pre) continue; 
        reverse(a + 1, a + i + 1); 
        if (dfs(g + 1, f, i)) return true;
        reverse(a + 1, a + i + 1); 
    }
    return false;
}

int main() 
{
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        b[i] = a[i] = read<int>();
    }
    sort(b + 1, b + n + 1);
    for (int i = 1; i <= n; i++) {
        a[i] = lower_bound(b + 1, b + n + 1, a[i]) - b;
    }
    a[n + 1] = n + 1;
    int depth = 0;
    while (true) { 
        if (dfs(0, depth, 0)) {
            write(depth, 10);
            return 0;
        }
        depth++;
    }
    return 0;
}