//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Lizards and Basements 2.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-15.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

namespace against_cpp11 {
    const int N = 307;
    int n, a, b;
    int h[N], answer;
#   define cost(id, g) (h[id] / g + 1)
    vector<pair<int, int> > ans;
    vector<pair<int, int> > record;
    int minist = INT_MAX;
    void deal_bound() {
        int cnt = cost(0, b);
        h[0] -= cnt * b;
        h[1] -= cnt * a;
        h[2] -= cnt * b;
        answer += cnt;
        record.push_back(make_pair(1, cnt));
        if (h[n - 1] < 0) return;
        cnt = cost(n - 1, b);
        h[n - 3] -= cnt * b;
        h[n - 2] -= cnt * a;
        h[n - 1] -= cnt * b;
        record.push_back(make_pair(n - 2, cnt));
        answer += cnt;
    }
    void solve(int u, int tot) {
        if (tot >= minist) return;
        if (u >= n - 1) {
            minist = tot;
            ans = record;
            return;
        }
        if (h[u] < 0) {
            solve(u + 1, tot);
            return;
        }
        int cnt = cost(u, a);
        record.push_back(make_pair(u, 0));
        for (int i = 0; i <= cnt; i++) {
            record.back().second = i;
            int used = (h[u] - i * a) / b + 1;
            if (h[u] - i * a < 0) used = 0;
            h[u + 1] -= i * b + used * a;
            h[u + 2] -= used * b;
            record.push_back(make_pair(u + 1, used));
            solve(u + 1, tot + i + used);
            record.pop_back();
            h[u + 1] += i * b + used * a;
            h[u + 2] += used * b;
        }
        record.pop_back();
    }
    signed main() {
        read >> n >> a >> b;
        for (int i = 0; i < n; i++)
            read >> h[i];
        deal_bound();
        solve(1, 0);
        answer += minist;
        write << answer << '\n';
        for (size_t i = 0; i < ans.size(); i++) {
            for (int j = 0; j < ans[i].second; j++)
                write << ans[i].first + 1 << ' ';
        }
        putchar(10);
        return 0;
    }
}

signed main() { return against_cpp11::main(); }