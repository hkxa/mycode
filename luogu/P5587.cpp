/*************************************
 * problem:      P5587 打字练习.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-10-13.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

int n1 = 0, n2 = 0, T, cnt = 0;
string sample[10007];
string R[10007];

void deal(string &a)
{
    static size_t i, j;
    for (i = 0, j = 0; i < a.length(); i++, j++) {
        if (a[i] == '<') {
            if (j) j--;
            j--;
        } else a[j] = a[i];
    }
    a = a.substr(0, j);
}

int main()
{
    getline(cin, sample[++n1]);
    while (sample[n1] != "EOF") {
        getline(cin, sample[++n1]);
    }
    getline(cin, R[++n2]);
    while (R[n2] != "EOF") {
        getline(cin, R[++n2]);
    }
    cin >> T;
    for (int i = 1; i < min(n1, n2); i++) {
        deal(sample[i]);
        deal(R[i]);
        for (size_t j = 0; j < min(sample[i].length(), R[i].length()); j++) cnt += (sample[i][j] == R[i][j]);
    }
    cout << (cnt * 60 / T);
    return 0;
}