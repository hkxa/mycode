T = int(input())
for i in range(T):
    a, b = map(int, input().split())
    xK, yK = map(int, input().split())
    xQ, yQ = map(int, input().split())
    ans = 0
    positions = set([
        (xK + a, yK + b), (xK + a, yK - b), (xK - a, yK + b), (xK - a, yK - b),
        (xK + b, yK + a), (xK + b, yK - a), (xK - b, yK + a), (xK - b, yK - a)
    ])
    for pos in positions:
        dif1, dif2 = pos[0] - xQ, pos[1] - yQ
        if {abs(dif1), abs(dif2)} == {a, b}:
            ans += 1
    print(ans)
