//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      T132641 中子衰变.
 * @user_id:      63720.
 * @user_name:    brealid.
 * @time:         2020-05-30.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
       return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
       return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// #define int int64

int n, task;
int a[2048 + 7];
int x, y;

inline void Read() {
    x = read<int>();
    y = read<int>();
    a[x] = y;
}

inline void order(int cmd) {
    write(cmd, 10);
    fflush(stdout);
    // if (cmd == 1) Read();
}

inline void output(int pl, int type) {
    write(pl, 32);
    write(type, 10);
    a[pl] = type;
    fflush(stdout);
}

inline bool end() {
    for (int i = 1; i <= n; i++) {
        if (a[i] || (a[i - 1] && a[i + 1] && a[i - 1] != a[i + 1])) continue;
        return false;
    }
    return true;
}

inline void expand(int &l, int &r) {
    l = r = (n + 1) >> 1;
    if (!a[l]) return;
    while (a[l - 1] == a[l]) l--;
    while (a[r + 1] == a[r]) r++;
}

inline bool SAmE(int l, int r) {
    if (l > r) swap(l, r);
    for (int i = l; i < r; i++)
        if (a[i] != a[i + 1]) return 0;
    return 1;
}

signed main() {
    n = read<int>(); 
    read<int>(); // task_id
    order(1);
    if (n % 2 == 0) {
        for (int i = 1; i <= (n >> 1); i++) {
            Read();
            output(n - x + 1, y);
        }
    } else {
        int l, r;
        while (!end()) {
            Read();
            if (end()) break;
            expand(l, r);
            if (l <= x && x <= r) {
                // if (r == n) output(l - 1, a[l]);
                // else output(r + 1, a[r]);
                if (l - 2 >= 1 && (!a[l - 2] || a[l - 2] == a[l])) output(l - 1, a[l]);
                else if (r + 2 <= n && (!a[r + 2] || a[r + 2] == a[r])) output(r + 1, a[r]);
                else for (int i = 1; i <= n; i++) {
                    if (!a[i] && (!a[i - 1] || !a[i + 1] || a[i - 1] == a[i + 1])) {
                        if (a[i - 1] || a[i + 1]) output(i, a[i - 1] | a[i + 1]);
                        else output(i, 1);
                        break;
                    }
                }
            } else {
                if (x == l - 2 && y == a[l]) output(l - 1, y);
                else if (x == r + 2 && y == a[r]) output(r + 1, y);
                else if (!a[n - x + 1]) output(n - x + 1, -y);
                else printf("QAQ I failed\n");
            }
        }
    }
    return 0;
}

// Create File Date : 2020-05-30

/*
wasa855,Hacked : n = 7 操作 1: 2 1 回答 : 6 -1 操作 2 : 4 1 回答 : 5 1 （不合法）
*/