/**
 * Do not mind how heavily it is raining,
 * as there's always sun shinning after the rain passed.
 */
#include <bits/stdc++.h>

const int N = 2e6 + 7;
const double alpha = 0.75;

struct position {
    double v[2];
} pos[N];

namespace kD_Tree {
    struct kD_Tree_Node {
        double v[2];
        double minv[2], maxv[2];
        int siz;
        int l, r;
        bool different_from(const double &x, const double &y) const {
            return x != v[0] || y != v[1];
        }
    } t[N];
    int kD_Tree_temp_node_cnt, kD_Tree_Node_cnt, root;
    inline bool cmp_v0_inc(const position &a, const position &b) {
        return a.v[0] != b.v[0] ? a.v[0] < b.v[0] : a.v[1] < b.v[1];
    }
    inline bool cmp_v1_inc(const position &a, const position &b) {
        return a.v[1] != b.v[1] ? a.v[1] < b.v[1] : a.v[0] < b.v[0];
    }
    typedef bool cmp_v_inc(const position &a, const position &b);
    cmp_v_inc *cmp_func[2] = { cmp_v0_inc, cmp_v1_inc };
    inline double pow2(const double rn) {
        return rn * rn;
    }
    inline double euclid_dist(const kD_Tree_Node &a, const double &x, const double &y) {
        return pow2(a.v[0] - x) + pow2(a.v[1] - y);
    }
    inline double nearest(const kD_Tree_Node &a, const double &x, const double &y) {
        return pow2(std::max(std::max(a.minv[0] - x, x - a.maxv[0]), 0.0)) + pow2(std::max(std::max(a.minv[1] - y, y - a.maxv[1]), 0.0));
    }
    inline void pushup(int u) {
        t[u].minv[0] = t[u].maxv[0] = t[u].v[0];
        t[u].minv[1] = t[u].maxv[1] = t[u].v[1];
        t[u].siz = 1;
        if (t[u].l) {
            t[u].minv[0] = std::min(t[u].minv[0], t[t[u].l].minv[0]);
            t[u].maxv[0] = std::max(t[u].maxv[0], t[t[u].l].maxv[0]);
            t[u].minv[1] = std::min(t[u].minv[1], t[t[u].l].minv[1]);
            t[u].maxv[1] = std::max(t[u].maxv[1], t[t[u].l].maxv[1]);
            t[u].siz += t[t[u].l].siz;
        }
        if (t[u].r) {
            t[u].minv[0] = std::min(t[u].minv[0], t[t[u].r].minv[0]);
            t[u].maxv[0] = std::max(t[u].maxv[0], t[t[u].r].maxv[0]);
            t[u].minv[1] = std::min(t[u].minv[1], t[t[u].r].minv[1]);
            t[u].maxv[1] = std::max(t[u].maxv[1], t[t[u].r].maxv[1]);
            t[u].siz += t[t[u].r].siz;
        }
    }
    void build(int &u, int l, int r, int D) {
        if (l > r) return;
        u = ++kD_Tree_Node_cnt;
        int mid = (l + r) >> 1;
        std::nth_element(pos + l, pos + mid, pos + r + 1, cmp_func[D]);
        t[u].v[0] = pos[mid].v[0];
        t[u].v[1] = pos[mid].v[1];
        build(t[u].l, l, mid - 1, !D);
        build(t[u].r, mid + 1, r, !D);
        pushup(u);
    }
    double ans;
    void find_nearest(const int &u, const double &x, const double &y, bool SelfOccured = false) {
        if (SelfOccured || t[u].different_from(x, y)) ans = std::min(ans, euclid_dist(t[u], x, y));
        else SelfOccured = true;
        register double dl = nearest(t[t[u].l], x, y), dr = nearest(t[t[u].r], x, y);
        if (dl < dr) {
            if (t[u].l && dl < ans) find_nearest(t[u].l, x, y, SelfOccured);
            if (t[u].r && dr < ans) find_nearest(t[u].r, x, y, SelfOccured);
        } else {
            if (t[u].r && dr < ans) find_nearest(t[u].r, x, y, SelfOccured);
            if (t[u].l && dl < ans) find_nearest(t[u].l, x, y, SelfOccured);
        }
    }
    inline double query_nearest(const double &x, const double &y) {
        ans = 1e12;
        find_nearest(root, x, y);
        return ans;
    }
}

int n, m;
double minn = 1e12, maxx = -1e12;
signed main() {
    // kin >> n;
    scanf("%d", &n);
    for (register int i = 1; i <= n; ++i)
        scanf("%lf%lf", pos[i].v, pos[i].v + 1);
    kD_Tree::build(kD_Tree::root, 1, n, 0);
    for (register int i = 1; i <= n; ++i)
        minn = std::min(minn, kD_Tree::query_nearest(pos[i].v[0], pos[i].v[1]));
    printf("%.4lf\n", sqrt(minn));
    return 0;
}