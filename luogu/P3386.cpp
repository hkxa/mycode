/*************************************
 * problem:      P3386 ��ģ�塿����ͼƥ��.
 * user ID:      63720.
 * user name:    �����Ű�.
 * time:         2019-03-07.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 
/*************************************
 * record ID:    R16982464.
 * time:         1408 ms
 * memory:       7900 KB
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

int head[2007], next[2000007], to[2000007], top = 0;
int link[2007];
bool used[2007];
int n, m, e;

void add_edge(int u, int v)
{
	if (u > n || v > m) return;
	top++;
	to[top] = n + v;
	next[top] = head[u];
	head[u] = top;
	top++;
	to[top] = u;
	next[top] = head[n + v];
	head[n + v] = top;
}

bool dfs(int u) 
{
    for (int i = head[u]; i != -1; i = next[i]) {
        int &v = to[i];
        if (!used[v]) {
            used[v] = true;
            if (link[v] == -1 || dfs(link[v])) {
                link[v] = u;
                return true;
            }
        }
    }
    return false;
}

int hungarian() 
{
    int res = 0;
    for (int u = 1; u <= n; u++) {
        memset(used, 0, sizeof(used));
        if (dfs(u)) res++;
    }
    return res;
}

int main()
{
	memset(link, -1, sizeof(link));
	memset(head, -1, sizeof(head));
	n = read<int>();
	m = read<int>();
	e = read<int>();
	int u, v;
	for (int i = 1; i <= e; i++) {
		u = read<int>();
		v = read<int>();
		add_edge(u, v);
	}
	write(hungarian());
	return 0;
}
