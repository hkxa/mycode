/*************************************
 * @problem:      [CEOI2017]Building Bridges.
 * @author:       brealid.
 * @time:         2020-11-17.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#if true
#define getchar() (i_P1 == i_P2 && (i_P2 = (i_P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), i_P1 == i_P2) ? EOF : *i_P1++)
char BUF[1 << 21], *i_P1 = BUF, *i_P2 = BUF;
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        inline Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        inline Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        inline Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        inline Reader& operator >> (double &lf) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF && endch != '.') endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            if (endch != '.') {
                lf = (endch & 15);
                while (isdigit(endch = getchar())) lf = lf * 10 + (endch & 15);
            } else lf = 0;
            if (endch == '.') {
                double len = 1;
                while (isdigit(endch = getchar())) lf += (endch & 15) * (len *= 0.1);
            }
            if (flag) lf = -lf;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        inline Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        inline Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        inline Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        inline Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 1e5 + 7, MaxH = 1e6;

int n;
int64 h[N];

struct funct {
    int64 k, b;
    funct() : k(0), b(1000000000000000000LL) {}
    funct(const int64 &K, const int64 &B) : k(K), b(B) {}
    int64 operator () (int val) const {
        return k * val + b;
    }
} tr[MaxH << 2];

void segt_insert(const int &u, const int &l, const int &r, const funct &f) {
    if (tr[u].k == 0 && tr[u].b == 1000000000000000000LL) {
        tr[u] = f;
        return;
    }
    if (l == r) {
        if (f(l) < tr[u](l)) tr[u] = f;
        return;
    }
    int mid = (l + r) >> 1;
    if (f.k < tr[u].k) {
        if (f(mid) < tr[u](mid)) segt_insert(u << 1, l, mid, tr[u]), tr[u] = f;
        else segt_insert(u << 1 | 1, mid + 1, r, f);
    } else {
        if (f(mid) < tr[u](mid)) segt_insert(u << 1 | 1, mid + 1, r, tr[u]), tr[u] = f;
        else segt_insert(u << 1, l, mid, f);
    }
}

int64 segt_query(const int &u, const int &l, const int &r, const int &pos) {
    if (l == r) return tr[u](pos);
    int mid = (l + r) >> 1;
    return min(tr[u](pos), pos <= mid ? segt_query(u << 1, l, mid, pos) : segt_query(u << 1 | 1, mid + 1, r, pos));
}

signed main() {
    kin >> n;
    for (int i = 1; i <= n; ++i) kin >> h[i];
    int64 s_tot = kin.get<int>();
    segt_insert(1, 1, MaxH, funct(-2 * h[1], h[1] * h[1] - s_tot));
    for (int i = 2; i <= n; ++i) {
        int64 f = s_tot + h[i] * h[i] + segt_query(1, 1, MaxH, h[i]);
        s_tot += kin.get<int>();
        if (i == n) kout << f << '\n';
        else segt_insert(1, 1, MaxH, funct(-2 * h[i], f + h[i] * h[i] - s_tot));
    }
    return 0;
}