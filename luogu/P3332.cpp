/*************************************
 * @problem:      [ZJOI2013]K大数查询.
 * @author:       brealid.
 * @time:         2020-12-20.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;
#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 5e4 + 7, TrSIZ = N << 8;
int n, m;

namespace segt {
    struct node {
        int64 val;
        int ls, rs;
    } tr[TrSIZ];
    int cnt;
    void insert(int &u, int l, int r, int pos, int d) {
        if (!u) u = ++cnt;
        tr[u].val += d;
        if (l == r) return;
        int mid = (l + r) >> 1;
        if (pos <= mid) insert(tr[u].ls, l, mid, pos, d);
        else insert(tr[u].rs, mid + 1, r, pos, d);
    }
    // int64 query(int u, int l, int r, int bound) {
    //     if (!u || l > bound) return 0;
    //     if (r <= bound) return tr[u].val;
    //     int mid = (l + r) >> 1;
    //     return query(tr[u].ls, l, mid, bound) + query(tr[u].rs, mid + 1, r, bound);
    // }
}

namespace btr {
    struct list_node {
        int v, nxt;
    } lis[207];
    int rt1[N], rt2[N];
    // int64 query(int ml, int mr, int range) {
    //     int64 res1 = 0, res2 = 0, res3 = 0;
    //     for (int p = mr; p; p ^= p & -p) {
    //         res1 += segt::query(rt1[p], 1, n, range);
    //         res3 += segt::query(rt2[p], 1, n, range);
    //     }
    //     for (int p = ml - 1; p; p ^= p & -p) {
    //         res2 += segt::query(rt1[p], 1, n, range);
    //         res3 -= segt::query(rt2[p], 1, n, range);
    //     }
    //     return res1 * (mr + 1) - res2 * ml - res3;
    // }
    int ask(int ml, int mr, int64 kth) {
        // kth = query(ml, mr, n) - kth + 1;
        int add1 = 0, add2 = 0, del1 = 0, del2 = 0, cnt = 0;
        for (int p = mr; p; p ^= p & -p) {
            lis[++cnt].v = rt1[p], lis[cnt].nxt = add1, add1 = cnt;
            lis[++cnt].v = rt2[p], lis[cnt].nxt = add2, add2 = cnt;
        }
        for (int p = ml - 1; p; p ^= p & -p) {
            lis[++cnt].v = rt1[p], lis[cnt].nxt = del1, del1 = cnt;
            lis[++cnt].v = rt2[p], lis[cnt].nxt = del2, del2 = cnt;
        }
        int l = 1, r = n, mid;
        while (l < r) {
            mid = (l + r) >> 1;
            int64 res1 = 0, res2 = 0, res3 = 0;
            for (int p = add1; p; p = lis[p].nxt) res1 += segt::tr[segt::tr[lis[p].v].rs].val;
            for (int p = add2; p; p = lis[p].nxt) res3 += segt::tr[segt::tr[lis[p].v].rs].val;
            for (int p = del1; p; p = lis[p].nxt) res2 += segt::tr[segt::tr[lis[p].v].rs].val;
            for (int p = del2; p; p = lis[p].nxt) res3 -= segt::tr[segt::tr[lis[p].v].rs].val;
            int64 sum = res1 * (mr + 1) - res2 * ml - res3;
            if (sum < kth) {
                for (int p = add1; p; p = lis[p].nxt) lis[p].v = segt::tr[lis[p].v].ls;
                for (int p = add2; p; p = lis[p].nxt) lis[p].v = segt::tr[lis[p].v].ls;
                for (int p = del1; p; p = lis[p].nxt) lis[p].v = segt::tr[lis[p].v].ls;
                for (int p = del2; p; p = lis[p].nxt) lis[p].v = segt::tr[lis[p].v].ls;
                kth -= sum;
                r = mid;
            } else {
                for (int p = add1; p; p = lis[p].nxt) lis[p].v = segt::tr[lis[p].v].rs;
                for (int p = add2; p; p = lis[p].nxt) lis[p].v = segt::tr[lis[p].v].rs;
                for (int p = del1; p; p = lis[p].nxt) lis[p].v = segt::tr[lis[p].v].rs;
                for (int p = del2; p; p = lis[p].nxt) lis[p].v = segt::tr[lis[p].v].rs;
                l = mid + 1;
            }
        }
        return l;
    }
    void insert(int l, int r, int number) {
        for (int p = l; p <= n; p += p & -p) {
            segt::insert(rt1[p], 1, n, number, 1);
            segt::insert(rt2[p], 1, n, number, l);
        }
        for (int p = r + 1; p <= n; p += p & -p) {
            segt::insert(rt1[p], 1, n, number, -1);
            segt::insert(rt2[p], 1, n, number, -r - 1);
        }
    }
}

signed main() {
    kin >> n >> m;
    int64 c;
    for (int i = 1, op, l, r; i <= m; ++i) {
        kin >> op >> l >> r >> c;
        if (op == 1) btr::insert(l, r, c);
        else kout << btr::ask(l, r, c) << '\n';
    }
    return 0;
}