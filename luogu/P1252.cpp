/*************************************
 * problem:      P1252 马拉松接力赛.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-05-28.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

int plr[6][11];

struct Recorder {
    int sum, cnt[6];
    Recorder()
    {
        sum = 0;
        memset(cnt, 0, sizeof(cnt));
    }
} ans;

template <typename structName>
structName createEmptyStruct()
{
    return structName();
}

void dfs(int id, int rest, Recorder tot)
{
    if (id >= 6) {
        if (ans.sum > tot.sum || !ans.sum) ans = tot;
        return;
    }
    int s, e;
    if (id == 5) {
        if (rest > 10) return;
        s = e = rest;
    } else {
        s = max(1, rest - (5 - id) * 10);
        e = min(10, rest + id - 5);
    }
    for (int i = s; i <= e; i++) {
        tot.sum += plr[id][i];
        tot.cnt[id] = i;
        dfs(id + 1, rest - i, tot);
        tot.sum -= plr[id][i];
        tot.cnt[id] = 0;
    }
}

int main()
{
    for (int i = 1; i <= 5; i++) {
        for (int j = 1; j <= 10; j++) {
            plr[i][j] = read<int>();
        }
    }
    dfs(1, 25, createEmptyStruct<Recorder>());
    write(ans.sum, 10);
    for (int i = 1; i <= 5; i++) {
        write(ans.cnt[i], 32);
    }
    return 0;
}