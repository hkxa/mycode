/*************************************
 * @problem:      id_name.
 * @user_id:      63720.
 * @user_name:    brealid.
 * @time:         2020-mm-dd.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int N = 100000 + 7;

struct Edge {
    int v;
    bool w;
    Edge(int V, bool W) : v(V), w(W) {}
};

int n, m;
char buf[3];
vector<Edge> G[N];
int tot, tot1, tot0, col[N], least = -1, ans[N];
bool want;
stack<int> vis;

bool dfs(int u) {
    vis.push(u);
    if (col[u]) tot1++; 
    else tot0++;
    for (unsigned i = 0; i < G[u].size(); i++) {
        bool change = want ^ G[u][i].w ^ col[u];
        if (~col[G[u][i].v]) {
            if (col[G[u][i].v] != change) return true;
        } else {
            col[G[u][i].v] = change;
            if (dfs(G[u][i].v)) return true;
        }
    } 
    return false;
}

void tryTo(int toBe) {
    want = toBe;
    memset(col, -1, sizeof(col));
    tot = 0;
    for (int i = 1; i <= n; i++)
        if (!~col[i]) {
            while (!vis.empty()) vis.pop();
            tot1 = tot0 = 0;
            col[i] = 1;
            if (dfs(i)) return;
            tot += min(tot1, tot0);
            if (tot1 > tot0) {
                while (!vis.empty()) {
                    col[vis.top()] ^= 1;
                    vis.pop();
                }
            }
        }
    if (!~least || tot < least) {
        // printf("least upd to %d in tryTo(%d, %d)\n", tot, toBe, RootCol);
        least = tot;
        for (int i = 1; i <= n; i++)
            ans[i] = col[i];
    }
}

int main()
{
    n = read<int>();
    m = read<int>();
    for (int i = 1, u, v; i <= m; i++) {
        u = read<int>();
        v = read<int>();
        scanf("%s", buf);
        buf[0] = buf[0] == 'R';
        G[u].push_back(Edge(v, buf[0]));
        G[v].push_back(Edge(u, buf[0]));
    }
    tryTo(0);
    tryTo(1);
    write(least, 10);
    if (~least) 
        for (int i = 1; i <= n; i++)
            if (ans[i]) write(i, 32);
    putchar(10);
    return 0;
}