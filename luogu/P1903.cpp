/*************************************
 * @problem:      [国家集训队]数颜色 / 维护队列.
 * @author:       brealid.
 * @time:         2021-03-22.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 133333 + 7, Col_MAX = 1e6 + 7;

int n, q, cq, cu, ans[N];
int col[N], cnt[Col_MAX];
int block_size, belong[N];

struct question {
    int id, l, r, tim;
    bool operator < (const question &b) const {
        if (belong[l] ^ belong[b.l]) return belong[l] < belong[b.l];
        if (belong[r] ^ belong[b.r]) return belong[r] < belong[b.r];
        // return tim < b.tim;
        if ((belong[r] ^ belong[b.r]) & 1) return tim < b.tim;
        return tim > b.tim;
        // return (belong[l] ^ belong[b.l]) ? belong[l] < belong[b.l] :
        //       ((belong[r] ^ belong[b.r]) ? belong[r] < belong[b.r] : tim < b.tim);
    }
} qry[N];

struct update {
    int p, x;
} upd[N];

signed main() {
    kin >> n >> q;
    for (int i = 1; i <= n; ++i) kin >> col[i];
    for (int i = 1; i <= q; ++i) {
        if (kin.get<char>() == 'Q') {
            qry[++cq].tim = cu;
            qry[cq].id = cq;
            kin >> qry[cq].l >> qry[cq].r;
        } else {
            ++cu;
            kin >> upd[cu].p >> upd[cu].x;
        }
    }
    block_size = pow(n, 0.66);
    for (int i = 1; i <= n; ++i) belong[i] = i / block_size;
    sort(qry + 1, qry + cq + 1);
    for (int i = 1, l = 1, r = 0, t = 0, now = 0; i <= cq; ++i) {
        while (r < qry[i].r) if (++cnt[col[++r]] == 1) ++now;
        while (l > qry[i].l) if (++cnt[col[--l]] == 1) ++now;
        while (r > qry[i].r) if (!--cnt[col[r--]]) --now;
        while (l < qry[i].l) if (!--cnt[col[l++]]) --now;
        while (t < qry[i].tim) {
            ++t;
            if (upd[t].p >= l && upd[t].p <= r) {
                if (++cnt[upd[t].x] == 1) ++now;
                if (!--cnt[col[upd[t].p]]) --now;
            }
            swap(col[upd[t].p], upd[t].x);
        }
        while (t > qry[i].tim) {
            if (upd[t].p >= l && upd[t].p <= r) {
                if (++cnt[upd[t].x] == 1) ++now;
                if (!--cnt[col[upd[t].p]]) --now;
            }
            swap(col[upd[t].p], upd[t].x);
            --t;
        }
        // for (int i = 1; i <= n; ++i)
        //     printf("col[%d] = %d\n", i, col[i]);
        ans[qry[i].id] = now;
    }
    for (int i = 1; i <= cq; ++i) kout << ans[i] << '\n';
    for (int i = 1; i <= cu; ++i)
        if (upd[i].p == 1) printf("Modify pos %d with %d\n", upd[i].p, upd[i].x);
    return 0;
}