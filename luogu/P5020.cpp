/*************************************
 * problem:      P5020.
 * user ID:      85848.
 * user name:    hkxadpall.
 * time:         2019-08-29.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

int n, a[100 + 7], ans;
bool f[25000 + 7];

int aCase()
{
    scanf("%d", &n);
    for (int i = 1; i <= n; i++) {
        scanf("%d", a + i);
    }
    sort(a + 1, a + n + 1);
    ans = n;
    memset(f, 0, sizeof(f));
    f[0] = 1;
    for (int i = 1; i <= n; i++) {
        if (f[a[i]]) {
            ans--;
            continue;
        }
        for (int j = a[i]; j <= a[n]; j++) {
            if (f[j - a[i]]) f[j] = true;
        }
    }
    printf("%d\n", ans);
    return 0;
}

int main()
{
    int TCases;
    scanf("%d", &TCases);
    while (TCases--) {
        aCase();
    }
    return 0;
}