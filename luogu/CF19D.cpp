/*************************************
 * @problem:      Points.
 * @user_name:    brealid/hkxadpall/zhaoyi20/j(o)moo/littleTortoise.
 * @time:         2020-09-15.
 * @language:     C++.
 * @fastio_ver:   20200827.
*************************************/

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char int8;
typedef unsigned char uint8;
typedef short int16;
typedef unsigned short uint16;
typedef int int32;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio
{
    enum io_flags
    {
        // simple io flags
        ignore_int = 1 << 0,   // input
        char_enter = 1 << 1,   // output
        flush_stdout = 1 << 2, // output
        flush_stderr = 1 << 3, // output
        // combination io flags
        endline = char_enter | flush_stdout // output
    };
    enum number_type_flags
    {
        // simple io flags
        output_double_stable = 1 << 0, // output
        output_double_faster = 1 << 1  // output
        // combination io flags
    };

    struct Reader
    {
        char endch;
        Reader() { endch = '\0'; }
        Reader &operator>>(io_flags f)
        {
            if (f & ignore_int)
            {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF)
                    endch = getchar();
                while (isdigit(endch = getchar()))
                    ;
            }
            return *this;
        }
        Reader &operator>>(char &ch)
        {
            // ignore character set : ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t')
                ch = getchar();
            return *this;
        }
        Reader &operator>>(double &lf)
        {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF)
                endch = getchar();
            if (endch == '-')
                flag = 1, endch = getchar();
            lf = endch & 15;
            while (isdigit(endch = getchar()))
                lf = lf * 10 + (endch & 15);
            if (endch == '.')
            {
                double digit = 0.1;
                while (isdigit(endch = getchar()))
                {
                    lf += (endch & 15) * digit;
                    digit *= 0.1;
                }
            }
            if (flag)
                lf = -lf;
            return *this;
        }
        template <typename Int>
        Reader &operator>>(Int &d)
        {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF)
                endch = getchar();
            if (endch == '-')
                flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar()))
                d = (d << 3) + (d << 1) + (endch & 15);
            if (flag)
                d = -d;
            return *this;
        }
        template <typename Int>
        inline Int get()
        {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF)
                endch = getchar();
            if (endch == '-')
                flag = 1, endch = getchar();
            Int d = endch & 15;
            while (isdigit(endch = getchar()))
                d = (d << 3) + (d << 1) + (endch & 15);
            if (flag)
                d = -d;
            return d;
        }
    } read;

    struct Writer
    {
        Writer &operator<<(io_flags f)
        {
            if (f & char_enter)
                putchar(10);
            if (f & flush_stdout)
                fflush(stdout);
            if (f & flush_stderr)
                fflush(stderr);
            return *this;
        }
        Writer &operator<<(const char *ch)
        {
            while (*ch)
                putchar(*(ch++));
            return *this;
        }
        Writer &operator<<(const char ch)
        {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer &operator<<(Int x)
        {
            static char buffer[33];
            static int top = 0;
            if (!x)
            {
                putchar('0');
                return *this;
            }
            if (x < 0)
                putchar('-'), x = ~x + 1;
            while (x)
            {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top)
                putchar(buffer[top--]);
            return *this;
        }
        inline void operator()(double val, int eps_digit = 6, char endch = '\0', number_type_flags flg = output_double_faster)
        {
            if (flg & output_double_stable)
            {
                static char output_format[10];
                sprintf(output_format, "%%.%dlf", eps_digit);
                printf(output_format, val);
            }
            else if (flg & output_double_faster)
            {
                if (val < 0)
                {
                    putchar('-');
                    val = -val;
                }
                (*this) << (int64)val;
                val -= (int64)val;
                putchar('.');
                double eps_number = 0.5;
                for (int i = 1; i <= eps_digit; i++)
                    eps_number /= 10;
                val += eps_number;
                while (eps_digit--)
                {
                    val *= 10;
                    putchar((int)val | '0');
                    val -= (int)val;
                }
            }
            else
            {
                (*this) << "1.#ERROR_NOT_IDENTIFIED_FLAG";
            }
            if (endch)
                putchar(endch);
        }
    } write;
} // namespace Fastio
using namespace Fastio;
namespace File_IO
{
    void init_IO()
    {
        freopen("Points.in", "r", stdin);
        freopen("Points.out", "w", stdout);
    }
} // namespace File_IO

// #define int int64

const int N = 200000 + 7, TnodeCNT = 1600000 + 7;

int n, tcnt;
int temp[TnodeCNT], VAL[TnodeCNT];
set<int> Linear_set[TnodeCNT];

struct Query {
    int opt, x, y;
} Operation[N];

struct TREE_NODE {
    int v;
    inline void update(const TREE_NODE &_ls, const TREE_NODE _rs) {
        if (!(~(_ls.v))) v = _rs.v;
        else if (!(~(_rs.v))) v = _ls.v;
        else {
            if (VAL[_ls.v] >= VAL[_rs.v]) v = _ls.v;
            else v = _rs.v;
        }
    }
} TrNode[TnodeCNT];

void Hash_init() {
    int *START = temp + 1, *END = temp + tcnt + 1;
    sort(START, END);
    for (int i = 1; i <= n; ++i) {
        Operation[i].x = lower_bound(START, END, Operation[i].x) - temp;
        Operation[i].y = lower_bound(START, END, Operation[i].y) - temp;
    }
}

void modify(int l, int r, int p, int aim) {
    if (l > r) return;
    if ((l > aim) || (r < aim)) return;
    if (l == r) {
        TrNode[p].v = l;
        return;
    }
    int mid = (l + r) >> 1, dp = p << 1, ddp = dp | 1;
    modify(l, mid, dp, aim);
    modify(mid + 1, r, ddp, aim);
    TrNode[p].update(TrNode[dp], TrNode[ddp]);
}

int query(int l, int r, int p, int aim, int v) {
    if (l > r || r < aim || !(~(TrNode[p].v)) || VAL[TrNode[p].v] <= v) return -1;
    if (l == r) return TrNode[p].v;
    int mid = (l + r) >> 1, ls = p << 1, rs = ls | 1;
    if (mid >= r) return query(l, mid, ls, aim, v);
    else if (mid < l) return query(mid + 1, r, rs, aim, v);
    else {
        int _ans = query(l, mid, ls, aim, v);
        if (~_ans) return _ans;
        else return query(mid + 1, r, rs, aim, v);
    }
}

int main() {
    read >> n;
    for (int i = 1; i <= n; ++i){
        Query &now = Operation[i];
        char ch;
        read >> ch >> now.x >> now.y;
        if (ch == 'a') now.opt = 1;
        else if (ch == 'r') now.opt = 3;
        else now.opt = 2;
        temp[++tcnt] = now.x;
        temp[++tcnt] = now.y;
    }
    Hash_init();
    memset(TrNode, -1, sizeof(TrNode));
    for (int i = 1; i <= n; ++i) {
        Query &now = Operation[i];
        switch (now.opt) {
            case 1: {
                Linear_set[now.x].insert(now.y);
                if (now.y == *(--Linear_set[now.x].end())) {
                    VAL[now.x] = now.y;
                    modify(1, tcnt, 1, now.x);
                }
                break;
            }
            case 2: {
                int k = query(1, tcnt, 1, now.x + 1, now.y);
                if (!~k) {
                    puts("-1");
                    break;
                }
                write << temp[k] << ' ';
                set<int>::iterator it = Linear_set[k].upper_bound(now.y);
                write << temp[*it] << '\n';
                break;
            }
            case 3: {
                if (now.y == *(--Linear_set[now.x].end())) {
                    Linear_set[now.x].erase(now.y);
                    if (Linear_set[now.x].empty()) VAL[now.x] = 0;
                    else VAL[now.x] = *(--Linear_set[now.x].end());
                    modify(1, tcnt, 1, now.x);
                }
                else Linear_set[now.x].erase(now.y);
                break;
            }
        }
    }
    return 0;
}