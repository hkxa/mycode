/*************************************
 * @problem:      CF1844C.
 * @author:       brealid.
 * @time:         2023-07-11.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

// #define USE_FREAD  // 使用 fread  读入，去注释符号
// #define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int K = 4e3 + 7;

int T, n, m, k;
int x[K], y[K], op[K];

#define ID(i, j, op) ((((i) - 1) * (m - 1) + (j) - 1) * 2 + (op))

int fa[K * K * 2];
int find(int u) {
    return fa[u] == u ? u : fa[u] = find(fa[u]);
}
#define merge(u, v) (fa[find(u)] = find(v))


signed main() {
    kin >> T;
    while (T--) {
        bool isYes = true;
        kin >> n >> m >> k;
        for (int i = 1; i <= (n - 1) * (m - 1) * 2; ++i) fa[i] = i;
        for (int i = 1, x1, y1, x2, y2; i <= k; ++i) {
            kin >> x1 >> y1 >> x2 >> y2;
            if (y2 == y1 + 1) {
                x[i] = x1, y[i] = y1, op[i] = 1;
            } else {
                x[i] = x1, y[i] = y1 - 1, op[i] = 2;
            }
            for (int j = 1; j < i; ++j) {
                if (x[i] == x[j] && y[i] == y[j]) {
                    if (op[i] != op[j]) isYes = false;
                }
                if (x[i] != x[j] && y[i] != y[j]) {
                    // printf("[DEBUG] %d %d\n", i, j);
                    if (op[i] == op[j]) {
                        merge(ID(x[i], y[i], 1), ID(x[j], y[j], 1));
                        merge(ID(x[i], y[i], 2), ID(x[j], y[j], 2));
                        merge(ID(x[i], y[j], 1), ID(x[j], y[i], 1));
                        merge(ID(x[i], y[j], 2), ID(x[j], y[i], 2));
                        // printf("[DEBUG] %d %d == %d %d\n", x[i], y[i], x[j], y[j]);
                        // printf("[DEBUG] %d %d == %d %d\n", x[i], y[j], x[j], y[i]);
                    } else {
                        merge(ID(x[i], y[i], 1), ID(x[j], y[j], 2));
                        merge(ID(x[i], y[i], 2), ID(x[j], y[j], 1));
                        merge(ID(x[i], y[j], 1), ID(x[j], y[i], 2));
                        merge(ID(x[i], y[j], 2), ID(x[j], y[i], 1));
                        // printf("[DEBUG] %d %d != %d %d\n", x[i], y[i], x[j], y[j]);
                        // printf("[DEBUG] %d %d != %d %d\n", x[i], y[j], x[j], y[i]);
                    }
                }
            }
        }
        for (int i = 1; i < n && isYes; ++i)
            for (int j = 1; j < m && isYes; ++j)
                if (find(ID(i, j, 1)) == find(ID(i, j, 2)))
                    isYes = false;
        kout << (isYes ? "YES\n" : "NO\n");
    }
    return 0;
}