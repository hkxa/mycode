/*************************************
 * @problem:      P3807 【模板】卢卡斯定理.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2019-11-15.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int64 ksm(int64 a, int n, int p) 
{
    int64 res = 1;
    while (n) {
        if (n & 1) res = res * a % p;
        a = a * a % p;
        n >>= 1;
    }
    return res;
}
int64 fac[100007];
#define inv(x, p) ksm(x, p - 2, p)
int64 C(int n, int m, int p) { return m >= n ? fac[m] * inv(fac[n] * fac[m - n] % p, p) % p : 0; }
int64 Lucas(int n, int m, int p) { return m ? Lucas(n / p, m / p, p) * C(n % p, m % p, p) % p : 1; }
void facPre(int p) { fac[0] = 1; for (int i = 1; i <= p; i++) fac[i] = fac[i - 1] * i % p; }

int main()
{
    int T = read<int>(), n, m, p;
    while (T--) {
        n = read<int>();
        m = read<int>();
        p = read<int>();
        facPre(p);
        write(Lucas(n, n + m, p), 10);
    }
    return 0;
}