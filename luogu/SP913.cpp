//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      qtree2.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-19.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 1e4 + 7, logN = 14;

int n;
int dep[N] = {0, 1}, dis[N];
vector<pair<int, int> > G[N];
int fa[N][logN + 2];

char GetOpt() {
    static char buffer[9];
    scanf("%s", buffer);
    return buffer[1];
}

void dfs(int u) {
    for (int i = 0; i < logN; i++)
        fa[u][i + 1] = fa[fa[u][i]][i];
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i].first, w = G[u][i].second;
        if (v != fa[u][0]) {
            fa[v][0] = u;
            dep[v] = dep[u] + 1;
            dis[v] = dis[u] + w;
            dfs(v);
        }
    }
}

int jump(int u, int k) {
    for (int i = 0; i <= logN; i++)
        if (k & (1 << i)) u = fa[u][i];
    return u;
}

int query_LCA(int u, int v) {
    if (dep[u] < dep[v]) swap(u, v);
    u = jump(u, dep[u] - dep[v]);
    if (u == v) return u;
    for (int k = logN; fa[u][0] != fa[v][0]; k--)
        if (fa[u][k] != fa[v][k]) {
            u = fa[u][k];
            v = fa[v][k];
        }
    return fa[u][0];
}

signed main() {
    int Tcases = read.get_int<int>();
    bool firstCase = 1;
    char opt; int a, b, k, lca;
    while (Tcases--) {
        if (firstCase) firstCase = 0;
        else putchar(10);
        read >> n;
        for (int i = 1; i <= n; i++) G[i].clear();
        for (int i = 1, a, b, l; i < n; i++) {
            read >> a >> b >> l;
            G[a].push_back(make_pair(b, l));
            G[b].push_back(make_pair(a, l));
        }
        dfs(1);
        while ((opt = GetOpt()) != 'O') {
            read >> a >> b;
            lca = query_LCA(a, b);
            if (opt == 'I') {
                write << dis[a] + dis[b] - 2 * dis[lca] << '\n';
            } else {
                read >> k; k--;
                int depa = dep[a] - dep[lca], depb = dep[b] - dep[lca];
                if (k <= depa) write << jump(a, k) << '\n';
                else write << jump(b, depa + depb - k) << '\n';
            }
        }
    }
    return 0;
}