/*************************************
 * problem:      P1107 [BJWC2008]雷涛的小猫.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-16.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define puts_return(x) { puts(x); return 0; }
#define write_return(x) { write(x); return 0; }

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, h, delta;
int kaki[2007][2007] = {0};
int g[2007] = {0};
int f[2007][2007] = {0};
// f[High][TreeId]

int main()
{
    n = read<int>();
    h = read<int>();
    delta = read<int>();
    int N_i, T_ij;
    for (int i = 1; i <= n; i++) {
        N_i = read<int>();
        while (N_i--) {
            T_ij = read<int>();
            kaki[T_ij][i]++;
        }
    }
    for (int i = h; i >= 1; i--) {
        for (int j = 1; j <= n; j++) f[i][j] = f[i + 1][j] + kaki[i][j];
        if (h - delta >= i) for (int j = 1; j <= n; j++) f[i][j] = max(f[i][j], g[i + delta] + kaki[i][j]);
        for (int j = 1; j <= n; j++) g[i] = max(g[i], f[i][j]);
        // for (int j = 1; j <= n; j++) write(f[i][j], 32);
        // putchar('#');
        // putchar(' ');
        // putchar('m');
        // putchar('a');
        // putchar('x');
        // putchar(' ');
        // putchar('=');
        // putchar(' ');
        // write(g[i], 10);
    }
    write(g[1], 10);
    return 0;
}