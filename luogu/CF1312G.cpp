/*************************************
 * @problem:      Autocompletion.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-25.
 * @language:     C++.
 * @fastio_ver:   20200820.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            static char buffer[2];
            scanf("%s", buffer);
            ch = buffer[0];
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            Int flag = 1;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = -1, endch = getchar();
            i = endch & 15;
            while (isdigit(endch = getchar())) i = (i << 3) + (i << 1) + (endch & 15);
            if (flag == -1) i = -i;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            Int flag = 1;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = -1, endch = getchar();
            Int i = endch & 15;
            while (isdigit(endch = getchar())) i = (i << 3) + (i << 1) + (endch & 15);
            if (flag == -1) i = -i;
            return i;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64
// #define int128 int64
// #define int128 __int128

const int N = 1e6 + 7;

int n, k;
int cnt[N];
int dft;
int ch[N][26], trie_node_cnt;
int end_pos[N];
int f[N];
int ask[N];

int get_pos(int from, int chr) {
    if (ch[from][chr]) return ch[from][chr];
    else return ch[from][chr] = ++trie_node_cnt;
}

void dp(int u, int fa, int fa_minn_get) {
    if (u) f[u] = f[fa] + 1;
    dft += cnt[u];
    if (cnt[u]) f[u] = min(f[u], fa_minn_get + dft);
    // printf("f[%d] = %d\n", u, f[u]);
    int self_minn_get = min(fa_minn_get, f[u] - (dft - cnt[u]));
    for (int i = 0; i < 26; ++i)
        if (ch[u][i]) dp(ch[u][i], u, self_minn_get);
}

signed main() {
    read >> n;
    char chr;
    for (int i = 1, x; i <= n; i++) {
        read >> x >> chr;
        end_pos[i] = get_pos(end_pos[x], chr - 'a');
    }
    read >> k;
    for (int i = 1; i <= k; i++) ++cnt[ask[i] = end_pos[read.get<int>()]];
    dp(0, 0, 0);
    for (int i = 1; i <= k; i++) write << f[ask[i]] << " \n"[i == k];
    return 0;
}