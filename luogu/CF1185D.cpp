/*************************************
 * @problem:      Extra Element.
 * @author:       brealid.
 * @time:         2021-02-24.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 2e5 + 7;

int n;
struct node {
    int v, id;
    bool operator < (const node &b) const {
        return v < b.v;
    }
} b[N];
map<int, int> cnt_diff;

signed main() {
    kin >> n;
    for (int i = 1; i <= n; ++i) {
        b[i].id = i;
        kin >> b[i].v;
    }
    sort(b + 1, b + n + 1);
    for (int i = 1; i < n; ++i) ++cnt_diff[b[i + 1].v - b[i].v];
    if (n <= 3 || cnt_diff.size() == 1) {
        kout << b[1].id << '\n';
        return 0;
    }
    if (cnt_diff.size() == 2) {
        pair<int, int> p1 = *cnt_diff.begin(), p2 = *++cnt_diff.begin();
        if (p1.second == 2)
            for (int i = 1; i < n; ++i)
                if (b[i + 1].v - b[i].v == p1.first) {
                    kout << b[i + 1].id << '\n';
                    return 0;
                }
        if (p2.second == 2)
            for (int i = 1; i < n; ++i)
                if (b[i + 1].v - b[i].v == p2.first) {
                    kout << b[i + 1].id << '\n';
                    return 0;
                }
    }
    if (cnt_diff.size() == 3) {
        pair<int, int> p1 = *cnt_diff.begin(), p2 = *++cnt_diff.begin(), p3 = *--cnt_diff.end();
        if (p3.first == n - 3 && p1.first + p2.first == p3.first)
            for (int i = 2; i < n; ++i)
                if (b[i + 1].v - b[i - 1].v == p3.first) {
                    kout << b[i].id << '\n';
                    return 0;
                }
        if (p2.first == n - 3 && p1.first + p3.first == p2.first)
            for (int i = 1; i < n; ++i)
                if (b[i + 1].v - b[i - 1].v == p2.first) {
                    kout << b[i].id << '\n';
                    return 0;
                }
        if (p1.first == n - 3 && p2.first + p3.first == p1.first)
            for (int i = 1; i < n; ++i)
                if (b[i + 1].v - b[i - 1].v == p1.first) {
                    kout << b[i].id << '\n';
                    return 0;
                }
    }
    for (int i = 2; i < n; ++i) {
        ++cnt_diff[b[i + 1].v - b[i - 1].v];
        --cnt_diff[b[i + 1].v - b[i].v];
        --cnt_diff[b[i].v - b[i - 1].v];
        if (cnt_diff[b[i + 1].v - b[i - 1].v] == n - 2) {
            kout << b[i].id << '\n';
            return 0;
        }
        --cnt_diff[b[i + 1].v - b[i - 1].v];
        ++cnt_diff[b[i + 1].v - b[i].v];
        ++cnt_diff[b[i].v - b[i - 1].v];
    }
    --cnt_diff[b[2].v - b[1].v];
    if (cnt_diff[b[3].v - b[2].v] == n - 2) {
        kout << b[1].id << '\n';
        return 0;
    }
    ++cnt_diff[b[2].v - b[1].v];

    --cnt_diff[b[n].v - b[n - 1].v];
    if (cnt_diff[b[2].v - b[1].v] == n - 2) {
        kout << b[n].id << '\n';
        return 0;
    }
    ++cnt_diff[b[n].v - b[n - 1].v];
    kout << "-1\n";
    return 0;
}