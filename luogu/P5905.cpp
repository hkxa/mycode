#pragma GCC optimize(1)
#pragma GCC optimize(2)
#pragma GCC optimize(3)
#pragma GCC optimize("-Ofast")
#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

struct Reader {
    char endch;
    Reader() { endch = '\0'; }
    Reader& operator >> (char &c) {
        c = getchar();
        while (isspace(c)) c = getchar();
        endch = '\0';
        return *this;
    }
    template <typename Int>
    Reader& operator >> (Int &i) {
        i = baseFastio::read<Int>(endch);
        return *this;
    }
    inline char get_nxt() {
        if (!endch) endch = getchar();
        return endch;
    }
} read;

struct Writer {
    Writer& operator << (const char *ch) {
        // char *p = ch;
        while (*ch) putchar(*(ch++));
        return *this;
    }
    Writer& operator << (const char ch) {
        putchar(ch);
        return *this;
    }
    template <typename Int>
    Writer& operator << (const Int i) {
        baseFastio::write(i);
        return *this;
    }
} write;

// #define int int64

const int N = 3007, M = 9007;

int n, m;
int head[N], to[M], val[M], nxt[M], cnt;
int h[N];       // SPFA
bool inq[N];    // SPFA & Johnson
int passed[N];  // SPFA
int dis[N];     // Johnson

#define AddEdge(u, v, w) { to[++cnt] = v; val[cnt] = w; nxt[cnt] = head[u]; head[u] = cnt; }

struct DijNode {
    int u, d;
    DijNode() {}
    DijNode(int U) : u(U), d(dis[U]) {}
    bool operator < (const DijNode &b) const {
        return d > b.d;
    }
};

inline int spfa() {
    for (int i = 1; i <= n; i++) h[i] = 1000000000;
    h[0] = 0;
    inq[0] = 1;
    queue<int> q;
    q.push(0);
    while (!q.empty()) {
        int u = q.front(); q.pop();
        inq[u] = 0;
        for (int e = head[u]; ~e; e = nxt[e]) {
            int v = to[e], w = val[e];
            if (h[v] > h[u] + w) {
                h[v] = h[u] + w;
                passed[v] = passed[u] + 1;
                if (passed[v] > n) return EXIT_FAILURE;
                if (!inq[v]) {
                    q.push(v);
                    inq[v] = 1;
                }
            }
        }
    }
    return EXIT_SUCCESS;
}

inline void dijkstra(int u) {
    static priority_queue<DijNode> q;
    for (int i = 1; i <= n; i++) dis[i] = 1000000000;
    dis[u] = 0;
    inq[u] = 1;
    q.push(DijNode(u));
    while (!q.empty()) {
        int u = q.top().u; q.pop();
        for (int e = head[u]; ~e; e = nxt[e]) {
            int v = to[e], w = val[e] + h[u] - h[v];
            if (dis[v] > dis[u] + w) {
                dis[v] = dis[u] + w;
                if (!inq[v]) {
                    q.push(DijNode(v));
                    inq[v] = 1;
                }
            }
        }
        inq[u] = 0;
    }
}

signed main()
{
    read >> n >> m;
    for (int i = 0; i <= n; i++) head[i] = -1;
    for (int i = 1; i <= n; i++) AddEdge(0, i, 0);
    for (int i = 1, u, v, w; i <= m; i++) {
        read >> u >> v >> w;
        AddEdge(u, v, w);
    }
    if (spfa()) {
        puts("-1");
        return 0;
    }
    int64 ans;
    for (int u = 1; u <= n; u++) {
        dijkstra(u);
        ans = 0;
        for (int v = 1; v <= n; v++) {
            if (dis[v] == 1000000000) ans += (int64)v * 1000000000;
            else ans += (int64)v * (dis[v] - h[u] + h[v]);
        }
        write << ans << '\n';
    }
    return 0;
}