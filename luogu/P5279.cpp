/*************************************
 * @problem:      P5279 [ZJOI2019]麻将.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-03-28. (9 days)
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#ifdef DEBUG
# define passing() cerr << "passing line [" << __LINE__ << "]." << endl
# define debug(...) printf(__VA_ARGS__)
# define show(x) cerr << #x << " = " << (x) << endl
#else
# define passing() do if (0) cerr << "passing line [" << __LINE__ << "]." << endl; while(0)
# define debug(...) do if (0) printf(__VA_ARGS__); while(0)
# define show(x) do if (0) cerr << #x << " = " << (x) << endl; while(0)
#endif

#define P 998244353
#define inv(x) fpow(x, P - 2)
#define Div(a, b) (fac[a] * ifac[b] % P)
#define C(n, m) (fac[n] * ifac[m] % P * ifac[(n) - (m)] % P)
#define min3(a, b, c) min(min(a, b), c)
#define Add(SavePl, ToAdd)      \
 do { SavePl += (ToAdd); if (SavePl >= P) SavePl -= P; } while(0)
#define UpdMax(SavePl, ToUpd)   \
 do { if ((SavePl) < (ToUpd)) (SavePl) = (ToUpd); } while (0)
#define ForInMatrix(i, j)       \
 for (int i = 0; i < 3; i++)    \
 for (int j = 0; j < 3; j++)

int64 fpow(int64 a, int n, int Mod = P) {
    int64 res = 1;
    while (n) {
        if (n & 1) res = res * a % Mod;
        a = a * a % Mod;
        n >>= 1;
    }
    return res;
}

int64 fac[407], ifac[407];

int n, m, a[107];
struct DpMatrix {
    int f[3][3];
    DpMatrix() { memset(f, -1, sizeof(f)); }
    inline int* operator [] (int x) { return f[x]; }
    inline bool operator < (DpMatrix x) const {
        ForInMatrix(i, j) {
            if (f[i][j] < x[i][j]) return true;
            if (f[i][j] > x[i][j]) return false;
        }
        return false;
    }
    inline bool operator != (DpMatrix x) const {
        ForInMatrix(i, j)
            if (f[i][j] != x[i][j]) return true;
        return false;
    }
    #define usedCard (i + j + k)
    inline void refresh(DpMatrix from, int cnt) {
        ForInMatrix(i, j)
            if (from[i][j] != -1)
                for (int k = 0; k < 3 && usedCard <= cnt; k++)
                    UpdMax(f[j][k], min(from[i][j] + i + (cnt - usedCard) / 3, 4));
    }
    #undef usedCard
    inline bool IsHu()
    {
        ForInMatrix(i, j)
            if (f[i][j] >= 4) return true;
        return false;
    }
};
struct AutomationNode {
    int nxt[5];
    DpMatrix f0, f1;
    int t; // t == -1 : 胡牌状态; otherwise : 对子个数;
    AutomationNode() { t = nxt[0] = nxt[1] = nxt[2] = nxt[3] = nxt[4] = 0; }
    inline bool operator < (const AutomationNode &b) const {
        if (t != b.t) return t < b.t;
        if (f0 != b.f0) return f0 < b.f0;
        return f1 < b.f1;
    }
    inline bool IsHu()
    {
        return t == -1 || t >= 7 || f1.IsHu();
    }
    inline AutomationNode BeginStatu()
    {
        AutomationNode ret;
        ret.f0[0][0] = 0;
        return ret;
    }
    inline AutomationNode HuStatu()
    {
        AutomationNode ret;
        ret.t = -1;
        return ret;
    }
    inline AutomationNode operator + (int x)
    {
        if (IsHu()) return HuStatu();
        AutomationNode res;
        res.t = t;
        res.f0.refresh(f0, x);
        res.f1.refresh(f1, x);
        if (x >= 2) {
            res.f1.refresh(f0, x - 2);
            res.t++;
        }
        if (res.IsHu()) return HuStatu();
        return res;
    }
};
AutomationNode nodes[2092 + 7]; 
map<AutomationNode, int> MapFinder;
int cnt = 0;
#define InsertNode(node) (MapFinder[nodes[++cnt] = node] = cnt)
#define FindNode(node) (MapFinder.count(node) ? MapFinder[node] : InsertNode(node))

struct HuAutomation {
    int f[107][407][2092 + 7];
    HuAutomation() {
        memset(f, 0, sizeof(f));
    }
    inline void ExpendNode(int x)
    {
        for (int j = 0; j <= 4; j++)
            nodes[x].nxt[j] = FindNode(nodes[x] + j);
    }
    inline void ExpendAll() {
        InsertNode(nodes[0].HuStatu());
        InsertNode(nodes[0].BeginStatu());
        for (int i = 2, last; i <= cnt; i++) {
            last = cnt + 1;
            ExpendNode(i);
            // if (last <= cnt) printf("%d : expend %d~%d\n", i, last, cnt);
        }
    }
    inline void DP() {
        f[0][0][2] = 1;
        for (int i = 1; i <= n; i++)  {
            for (int j = m; j >= 0; j--) {
                for (int k = 1; k <= cnt; k++) {
                    if (f[i - 1][j][k]) {
                        passing();
                        for (int t = 0; t <= 4 - a[i]; t++) {
                            // printf("Add(f[%d][%d][%d], f[%d][%d][%d] * C(%d, %d) %% P)\n", i, j + t, nodes[k].nxt[a[i] + t], i - 1, j, k, 4 - a[i], t);
                            Add(f[i][j + t][nodes[k].nxt[a[i] + t]], f[i - 1][j][k] * C(4 - a[i], t) % P);
                        }
                    }
                }
            }
        }
    }
} Automation;

inline void initFac()
{
    fac[0] = ifac[0] = 1;
    for (int i = 1; i <= m; i++) fac[i] = fac[i - 1] * i % P;
    ifac[m] = inv(fac[m]);
    for (int i = m - 1; i >= 1; i--) ifac[i] = ifac[i + 1] * (i + 1) % P;
};

#define OutputNode(x)           \
printf("------------------\n"); \
printf("t = %d\n", nodes[x].t); \
printf("[f0]\n");               \
ForInMatrix(i, j)               \
    write(nodes[x].f0[i][j], j == 2 ? 10 : 32); \
printf("[f1]\n");               \
ForInMatrix(i, j)               \
    write(nodes[x].f1[i][j], j == 2 ? 10 : 32);

int main()
{
    Automation.ExpendAll();
    // show(cnt);
    // show(FindNode(nodes[0].HuStatu()));
    // show(FindNode(nodes[0].BeginStatu()));
    // OutputNode(16);
    // if (cnt != 2092) return EXIT_FAILURE;
    n = read<int>();
    m = 4 * n - 13;
    initFac();
    for (int i = 1; i <= 13; i++) {
        a[read<int>()]++;
        read<int>();
    }
    Automation.DP();
    int64 ans = 0;
    show(Automation.f[n][m - 1][5]);
    for (int i = m; i >= 1; i--)
        for (int j = 2; j <= cnt; j++)
            Add(ans, Automation.f[n][i][j] * fac[i] % P * fac[m - i] % P);
    show(ans);
    write((ans * ifac[m] + 1) % P, 10);
    return 0;
}

/*
1 1 1
2 2 
3 3 3
4 4  
5 5 5
Answer - 1

5
1 1
1 2
1 3
2 1
2 2
3 1
3 2
3 3
4 1
4 2
5 1
5 2
5 3
*/

/*
9
1 1
1 2
1 3
2 1
3 1
4 1
5 1
6 1
7 1
8 1
9 1
9 2
9 3

9 1 1 1 2 1 3 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 9 2 9 3
*/

/*
9
1 1
1 2
1 3
1 4
2 1
2 2
2 3
2 4
3 1
3 2
3 3
3 4
4 1
*/