/*************************************
 * problem:      P2573 [SCOI2012]滑雪.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-10-13.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define ForInVec(vectorType, vectorName, iteratorName) for (vector<vectorType>::iterator iteratorName = vectorName.begin(); iteratorName != vectorName.end(); iteratorName++)
#define ForInVI(vectorName, iteratorName) ForInVec(int, vectorName, iteratorName)
#define ForInVE(vectorName, iteratorName) ForInVec(Edge, vectorName, iteratorName)
#define MemWithNum(array, num) memset(array, num, sizeof(array))
#define Clear(array) MemWithNum(array, 0)
#define MemBint(array) MemWithNum(array, 0x3f)
#define MemInf(array) MemWithNum(array, 0x7f)
#define MemEof(array) MemWithNum(array, -1)
#define ensuref(condition) do { if (!(condition)) exit(0); } while(0)

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

struct FastIOer {
#   define createReadlnInt(type)            \
    FastIOer& operator >> (type &x)         \
    {                                       \
        x = read<type>();                   \
        return *this;                       \
    }
    createReadlnInt(short);
    createReadlnInt(unsigned short);
    createReadlnInt(int);
    createReadlnInt(unsigned);
    createReadlnInt(long long);
    createReadlnInt(unsigned long long);
#   undef createReadlnInt

#   define createReadlnReal(type)           \
    FastIOer& operator >> (type &x)         \
    {                                       \
        char c;                             \
        x = read<long long>(c);             \
        if (c != '.') return *this;         \
        type r = (x >= 0 ? 0.1 : -0.1);     \
        while (isdigit(c = getchar())) {    \
            x += r * (c & 15);              \
            r *= 0.1;                       \
        }                                   \
        return *this;                       \
    }
    createReadlnReal(double);
    createReadlnReal(float);
    createReadlnReal(long double);

#   define createWritelnInt(type)           \
    FastIOer& operator << (type &x)         \
    {                                       \
        write<type>(x);                     \
        return *this;                       \
    }
    createWritelnInt(short);
    createWritelnInt(unsigned short);
    createWritelnInt(int);
    createWritelnInt(unsigned);
    createWritelnInt(long long);
    createWritelnInt(unsigned long long);
#   undef createWritelnInt
    
    FastIOer& operator >> (char &x)
    {
        x = getchar();
        return *this;
    }
    
    FastIOer& operator << (char x)
    {
        putchar(x);
        return *this;
    }
    
    FastIOer& operator << (const char *x)
    {
        int __pos = 0;
        while (x[__pos]) {
            putchar(x[__pos++]);
        }
        return *this;
    }
} fast;
#define int long long
int n, m;
int h[1000007];
int ans1 = 0, ans2 = 0;
bool couldReach[1000007];

struct KruskalEdge {
    int u, v, dis;
    KruskalEdge() {} 
    KruskalEdge(int _u, int _v, int _dis) : u(_u), v(_v), dis(_dis) {} 
    void func_input()
    {
        u = read<int>();
        v = read<int>();
        dis = read<int>();
        if (h[u] < h[v]) swap(u, v);
    }
    bool operator < (const KruskalEdge &k) const { return h[v] != h[k.v] ? h[v] > h[k.v] : dis < k.dis; }
} e[1000007];

struct Edge {
    int to, dis;
    Edge() {} 
    Edge(int _to, int _dis) : to(_to), dis(_dis) {} 
};

vector<Edge> G[1000007];

struct UFS {
    int fa[100007];
    int familyCnt;

    UFS() : familyCnt(0)
    {
        memset(fa, -1, sizeof(fa));
    }

    int find(int u)
    {
        return fa[u] < 0 ? u : fa[u] = find(fa[u]);
    }

    void connect(int x, int y)
    {
        int xx = find(x), yy = find(y);
        if (xx == yy) return;
        if (fa[xx] > fa[yy]) swap(xx, yy);
        fa[xx] += fa[yy];
        fa[yy] = xx;
        familyCnt--;
    }

    bool isFamily(int u, int v)
    {
        return find(u) == find(v);
    }
} u;

void dfs(int p)
{
    for (unsigned i = 0; i < G[p].size(); i++) {
        if (!couldReach[G[p][i].to]) {
            couldReach[G[p][i].to] = 1;
            ans1++;
            dfs(G[p][i].to); 
        }
    }
} 

signed main()
{
    n = read<int>();
    m = read<int>();
    for (int i = 1; i <= n; i++) h[i] = read<int>();
    for (int i = 1; i <= m; i++) {
        e[i].func_input();
        G[e[i].u].push_back(Edge(e[i].v, e[i].dis));
        if (h[e[i].v] == h[e[i].u]) G[e[i].v].push_back(Edge(e[i].u, e[i].dis));
    }
    couldReach[1] = 1;
    ans1 = 1;
    dfs(1);
    write(ans1, 32);
    sort(e + 1, e + m + 1);
    for (int i = 1; i <= m; i++) {
        if (couldReach[e[i].u] && couldReach[e[i].v] && !u.isFamily(e[i].u, e[i].v)) {
            ans2 += e[i].dis;
            u.connect(e[i].u, e[i].v);
        }
    }
    write(ans2, 10);
    return 0;
}