/*************************************
 * problem:      P1737 [NOI2016]旷野大计算.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-04-05.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 
/*************************************
 * now_type:	 loading
 * now_bak:		 16 
 * next_step:    Next_todo (pigeon) 
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

void A(void)
{
    puts("I");
    puts("I");
    puts("+ 1 2");
    puts("+ 3 3");
    puts("- 4");
    puts("O 5");
    return;
}

void B(void)
{
    puts("");
    return;
}

void C(void)
{
    puts("I");
    puts("- 1");
    puts("P 1 2");
    puts("O 3");
    return;
}

void D(void)
{
    puts("");
    return;
}

void E(void)
{
    puts("");
    return;
}

void F(void)
{
    puts("");
    return;
}

void G(void)
{
    puts("");
    return;
}

void H(void)
{
    puts("");
    return;
}

void I(void)
{
    puts("");
    return;
}

void J(void)
{
    puts("");
    return;
}

typedef void Func(void);
Func *solve[11] = {
    NULL, A, B, C, D, E, F, G, H, I, J
};

int main()
{
    int c = read<int>();
    solve[c]();
}