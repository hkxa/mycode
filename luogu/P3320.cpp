//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      P3320 [SDOI2015]寻宝游戏.
 * @user_id:      63720.
 * @user_name:    brealid.
 * @time:         2020-05-20.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define N 100007

int n, m;
vector<pair<int, int> > G[N];
bool status[N];

int dfn[N], dft;
int f[N], beg[N], wson[N], siz[N], dep[N];
int64 dis[N];

struct comp {
    bool operator () (int a, int b) { return dfn[a] < dfn[b]; }
};
set<int, comp> s;
set<int, comp>::iterator it;
int64 ans;

// 树链剖分 dfs - 1 : get siz, f, dis
void dfs1(int u, int fa) {
    siz[u] = 1;
    f[u] = fa;
    for (auto v : G[u]) {
        if (v.first != fa) {
            dis[v.first] = dis[u] + v.second;
            dfs1(v.first, u);
            siz[u] += siz[v.first];
            if (siz[v.first] > siz[wson[u]]) 
                wson[u] = v.first;
        }
    }
}

// 树链剖分 dfs - 2 : get dfn, beg, dep
void dfs2(int u, int chainBegin) {
    dfn[u] = ++dft;
    beg[u] = chainBegin;
    dep[u] = dep[f[u]] + 1;
    if (wson[u]) dfs2(wson[u], chainBegin);
    for (auto v : G[u]) {
        if (v.first != f[u] && v.first != wson[u]) {
            dfs2(v.first, v.first);
        }
    }
}

inline int LCA(int u, int v) {
    while (beg[u] != beg[v]) {
        if (dep[beg[u]] < dep[beg[v]]) swap(u, v);
        u = f[beg[u]];
    }
    if (dep[u] < dep[v]) return u;
    else return v;
}

inline int64 QueryDis(int x, int y)
{
    if (!x || !y || x == n + 1 || y == n + 1) return 0;
    int l = LCA(x, y);
    return dis[x] + dis[y] - 2 * dis[l];
}

inline int64 calc(int x, int y, int z) {
    // printf("calc(%d, %d, %d)\n", x, y, z);
    return QueryDis(x, y) + QueryDis(y, z) - QueryDis(x, z);
}

int main() {
    n = read<int>();
    m = read<int>();
    for (int i = 1, x, y, z; i < n; i++) {
        x = read<int>();
        y = read<int>();
        z = read<int>();
        G[x].push_back(make_pair(y, z));
        G[y].push_back(make_pair(x, z));
    }
    dfs1(1, 0);
    dfs2(1, 1);
    // for (int i = 1; i <= n; i++) write(dfn[i], i == n ? 10 : 32);
    for (int i = 1, x, y, z; i <= m; i++) {
        y = read<int>();
        if (status[y]) {
            x = (it = s.lower_bound(y)) == s.begin() ? *--s.end() : *--it;
            z = (it = s.upper_bound(y)) == s.end() ? *s.begin() : *it;
            ans -= calc(x, y, z);
            s.erase(y);
        } else {
            s.insert(y);
            x = (it = s.lower_bound(y)) == s.begin() ? *--s.end() : *--it;
            z = (it = s.upper_bound(y)) == s.end() ? *s.begin() : *it;
            ans += calc(x, y, z);
        }
        status[y] ^= 1;
        if (s.size() >= 2) write(ans, 10);
        else puts("0");
    }
    return 0;
}