//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      【模板】可持久化线段树 2（主席树）.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-19.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 998244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? Module(w - P) : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

const int N = 2e5 + 7, SIZ = 1e7 + 7;

int n, m;
int a[N], LSH[N];
int root[N], val[SIZ], l[SIZ], r[SIZ], cnt;

void build_tree(int u, int ml = 1, int mr = n) {
    if (ml == mr) return;
    int mid = (ml + mr) >> 1;
    build_tree(l[u] = ++cnt, ml, mid);
    build_tree(r[u] = ++cnt, mid + 1, mr);
}

void modify_tree(int base, int u, int loc, int diffValue, int ml = 1, int mr = n) {
    if (ml == mr) {
        val[u] = val[base] + diffValue;
        return;
    }
    int mid = (ml + mr) >> 1;
    if (loc <= mid) {
        r[u] = r[base];
        modify_tree(l[base], l[u] = ++cnt, loc, diffValue, ml, mid);
        val[u] = val[l[u]] + val[r[u]];
    } else {
        l[u] = l[base];
        modify_tree(r[base], r[u] = ++cnt, loc, diffValue, mid + 1, mr);
        val[u] = val[l[u]] + val[r[u]];
    }
}

inline void modify(int i, int v, int loc, int diffValue) {
    modify_tree(root[v], root[i] = ++cnt, loc, diffValue);
}

// Query in Tree{u - v}
int query_tree(int u, int v, int k, int ml = 1, int mr = n) {
    if (ml == mr) return ml;
    int mid = (ml + mr) >> 1;
    int LefSize = val[l[u]] - val[l[v]];
    if (k <= LefSize) return query_tree(l[u], l[v], k, ml, mid);
    else return query_tree(r[u], r[v], k - LefSize, mid + 1, mr);
}

inline int query(int l, int r, int kth) {
    return LSH[query_tree(root[r], root[l - 1], kth)];
}

signed main() {
    n = read<int>();
    m = read<int>();
    build_tree(root[0] = ++cnt);
    for (int i = 1; i <= n; i++) LSH[i] = a[i] = read<int>();
    sort(LSH + 1, LSH + n + 1);
    for (int i = 1; i <= n; i++) {
        a[i] = lower_bound(LSH + 1, LSH + n + 1, a[i]) - LSH;
        modify(i, i - 1, a[i], 1);
    }
    for (int i = 1, l, r, k; i <= m; i++) {
        l = read<int>();
        r = read<int>();
        k = read<int>();
        write(query(l, r, k), 10);
    }
    return 0;
}

// Create File Date : 2020-06-18