// luogu-judger-enable-o2
// luogu-judger-enable-o2
/*************************************
 * problem:      P4884 多少个1？.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-05-03.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = -x;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

// #define ll __int128
typedef __int128 ll;

ll modP, K; 
#define mulX 25LL
ll mul(ll a, ll b) 
{ 
    // This function is for longlong times longlong
    // won't yìchū
    if (a < 1e9 && b < 1e9) return (a * b) % modP;
    else {
        ll L = a * (b >> mulX) % modP * (1LL << mulX) % modP;
        ll R = a * (b & ((1LL << mulX) - 1LL)) % modP;
        return (L + R) % modP;
    }
    return EOF;
}
#undef mulX

ll fexp(ll a, ll b) 
{ 
    // fastPow
    ll res = 1;
    while (b) {
        if (b & 1) res = mul(res, a) % modP;
        a = mul(a, a) % modP;
        b >>= 1;
    }
    return res;
}

namespace case1 {
    void solve()
    {
        ll n = 1, res = 1;
        while (res != K) {
            n++;
            res = (res << 3) + (res << 1) + 1;
            while (res >= modP) res -= modP;
        }
        write(n);
    }
}

typedef map<ll, ll> HashTable;

#define magicMod(o) (((o) % modP + modP) % modP)

namespace case2 {
    void BSGS()
    {
        K = ((K << 3) + K + 1) % modP;
        // printf("K = %lld.\n", K);
        ll m = sqrt((long long)modP) + 1;
        HashTable ht;
        for (ll b = 0, res = K; b < m; b++) {
            ht[res] = b;
            res = (res * 10) % modP;
        }
        for (ll a = 1, tt = fexp(10, m), t = tt; a <= m + 1; a++, t = mul(t, tt) % modP)
        {
            HashTable::iterator b = ht.find(t);
            if (b == ht.end()) continue;
            // printf("%lld * %lld - %lld.\n", a, m, b->second);
            write(magicMod(mul(a, m) - b->second), 10);
            return;
        }
        return;
    }
}


int main()
{
    K = read<ll>();
    modP = read<ll>();
    if (modP == 2 || modP == 3 || modP == 5) {
        // This case should use exBSGS
        // I'm so jvruo that I want use bàolì to solve this case
        case1::solve();
    } else {
        // This case should use common BSGS
        // so I use it
        case2::BSGS();
    }
    return 0;
}