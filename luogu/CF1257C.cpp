/*************************************
 * @problem:      Codeforces - Educational #76.
 * @user_name:    hkxadpall.
 * @time:         2019-11-13.
 * @language:     C++.
 * @upload_place: Codeforces.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, a[200007];
int occ[200007];

int main()
{
    int t = read<int>(), ans;
    while (t--) {
        n = read<int>();
        for (int i = 1; i <= n; i++) occ[i] = 0;
        ans = n + 1;
        for (int i = 1; i <= n; i++) {
            a[i] = read<int>();
            if (occ[a[i]]) {
                ans = min(ans, i - occ[a[i]] + 1);
            }
            occ[a[i]] = i;
        }
        write(ans != n + 1 ? ans : -1, 10);
    }
    return 0;
}