/*************************************
 * @problem:      P2939 [USACO09FEB]改造路Revamping Trails.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2019-11-16.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, m, k;
struct Edge {
    int v, l;
};
vector<Edge> G[220007];
int dis[220007];
// bool vis[220007];

#define getPoint(Floor, p) ((Floor) * n + p)

int main()
{
    n = read<int>();
    m = read<int>();
    k = read<int>();
    for (int i = 1, u, v, l; i <= m; i++) {
        u = read<int>();
        v = read<int>();
        l = read<int>();
        G[getPoint(0, u)].push_back((Edge){getPoint(0, v), l});
        G[getPoint(0, v)].push_back((Edge){getPoint(0, u), l});
        for (int j = 1; j <= k; j++) {
            G[getPoint(j, u)].push_back((Edge){getPoint(j, v), l});
            G[getPoint(j, v)].push_back((Edge){getPoint(j, u), l});
            G[getPoint(j - 1, u)].push_back((Edge){getPoint(j, v), 0});
            G[getPoint(j - 1, v)].push_back((Edge){getPoint(j, u), 0});
        }
    }
    memset(dis, 0x3f, sizeof(dis));
    dis[getPoint(0, 1)] = 0;
    priority_queue<pair<int, int>, vector<pair<int, int> >, greater<pair<int, int> > > q;
    q.push(make_pair(0, getPoint(0, 1)));
    while (!q.empty()) {
        while (!q.empty() && dis[q.top().second] != q.top().first) q.pop();
        if (q.empty()) break;
        int fr = q.top().second; q.pop();
        for (unsigned i = 0; i < G[fr].size(); i++) {
            int &v = G[fr][i].v, &l = G[fr][i].l;
            if (dis[v] > dis[fr] + l) {
                dis[v] = dis[fr] + l;
                // printf("dis[%d, %d] from dis[%d, %d](=%d) + l(=%d)\n", (v - 1) / n, (v - 1) % n + 1, (fr - 1) / n, (fr - 1) % n + 1, dis[fr], l);
                q.push(make_pair(dis[v], v));
            }
        }
    }
    write(dis[getPoint(k, n)]);
    return 0;
}