//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Qtree4 - Query on a tree IV.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-02.
 * @language:     C++.
*************************************/ 
#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#ifndef DEBUG
# define fprintf(...) {}
# define fflush(...) {}
# define debug(...) {}
#endif

#define MEM_ARR(arr, siz, v) { for (int i = 0; i < siz + 5; i++) arr[i] = v; }

// #define int int64

const int N = 100007, MY_INT_MIN = INT_MIN / 3;

int n, Q, CntWhite;
int head[N], val[N << 1], to[N << 1], nxt[N << 1], edge_cnt;
int fa[N]; // 点分树上的父亲
int siz[N], vis[N];
const bool black = 0, white = 1;
bool type[N];

#define ADDE(u, v, w) { val[++edge_cnt] = w; to[edge_cnt] = v; nxt[edge_cnt] = head[u]; head[u] = edge_cnt; }
#define linkE(u, v, w) { ADDE(u, v, w); ADDE(v, u, w); }

void reverseColor(int x) {
    if (type[x] == black) {
        type[x] = white;
        CntWhite++;
    } else {
        type[x] = black;
        CntWhite--;
    }
}

int IntegerPowerOfTwo_k[37];
// 预处理
void IntegerPowerOfTwo_Pretreat() {
    for (int i = 0; i < 20; i++) IntegerPowerOfTwo_k[(1 << i) % 37] = i;
}
#define Log2(num) IntegerPowerOfTwo_k[(num) % 37]

struct TreeChainSplit_LCA {
    int dep[N], dis[N];
    int siz[N], wson[N];
    int tcs_fa[N], beg[N];
    // 预处理
    void Pretreat(int u, int Father, int Dis = 0) {
        tcs_fa[u] = Father;
        dep[u] = dep[Father] + 1;
        dis[u] = Dis;
        siz[u] = 1;
        wson[u] = 0;
        for (int e = head[u]; e; e = nxt[e]) {
            int &v = to[e];
            if (v != Father) {
                Pretreat(v, u, Dis + val[e]);
                siz[u] += siz[v];
                if (siz[v] > siz[wson[u]]) wson[u] = v;
            }
        }
    }
    void Pretreat2(int u, int ChainBegin) {
        beg[u] = ChainBegin;
        if (!wson[u]) return;
        Pretreat2(wson[u], ChainBegin);
        for (int e = head[u]; e; e = nxt[e]) {
            int &v = to[e];
            if (v != tcs_fa[u] && v != wson[u]) 
                Pretreat2(v, v);
        }
    }
    inline int calcDist(int u, int v) {
        int ans = dis[u] + dis[v];
        while (beg[u] != beg[v]) {
            if (dep[beg[u]] < dep[beg[v]]) swap(u, v);
            u = tcs_fa[beg[u]];
        }
        if (dep[u] < dep[v]) return ans - 2 * dis[u];
        else return ans - 2 * dis[v];
    }
} Graph;

struct Heap {
    std::priority_queue<int> que, del;
    size_t size() { return que.size() - del.size(); }
    void insert(int x) { que.push(x); }
    void remove(int x) { del.push(x); }
    // [Warn] 未检查 que 是否为空
    inline int getTop() {
        while (!del.empty() && que.top() == del.top()) {
            que.pop();
            del.pop();
        }
        return que.top();
    }
    // [Warn] 未检查 que 的 size 是否 >= 2
    inline int getTopTwo() {
        while (!del.empty() && que.top() == del.top()) {
            que.pop(); del.pop();
        }
        int x = que.top(); del.push(x);
        while (!del.empty() && que.top() == del.top()) {
            que.pop(); del.pop();
        }
        int y = que.top(); que.push(x);
        
        return x + y;
    }
    inline int top() {
        if (size()) return getTop();
        else return MY_INT_MIN;
    }
    inline int getAnswer() {
        switch (size()) {
            case 0 : return MY_INT_MIN;        // 无白点
            case 1 : return getTop();       // 最远距离为链长度
            default : return getTopTwo();   // size >= 2
        }
    }
} disU[N], disF[N], all;
// disU : 维护原树子树到 u 的 dis
// disF : 维护原树子树到 u 的点分树父亲的 dis
// all  : 维护总 answer

struct PointDivideTree {
    // 获得所在子树所有点的 siz
    void getSubtreeSiz(int u) {
        vis[u] = true;
        siz[u] = 1;
        for (int e = head[u]; e; e = nxt[e]) {
            int &v = to[e];
            if (!vis[v]) {
                getSubtreeSiz(v);
                siz[u] += siz[v];
            }
        }
        vis[u] = false;
    }

    // 获得所在子树的根（已维护 siz)
    int GSR_sizAll, GSR_root, GSR_rootSiz;
    void getSubtreeRoot(int u) {
        vis[u] = true;
        int Tmax = 0;
        for (int e = head[u]; e; e = nxt[e]) {
            int &v = to[e];
            if (!vis[v]) {
                getSubtreeRoot(v);
                if (siz[v] > Tmax) Tmax = siz[v];
            }
        }
        if (GSR_sizAll - siz[u] > Tmax) Tmax = GSR_sizAll - siz[u];
        if (Tmax < GSR_rootSiz) {
            GSR_root = u;
            GSR_rootSiz = Tmax;
        }
        vis[u] = false;
    }

    // 获得所在子树的根（已封装）
    inline int getRoot(int u) {
        getSubtreeSiz(u);
        GSR_sizAll = siz[u];
        GSR_rootSiz = n + 1;
        getSubtreeRoot(u);
        return GSR_root;
    }

    // 获得初始堆
    void GetInitiallyHeap(int u, int root, int rootFa) { 
        vis[u] = true;
        // disU[root].insert(Graph.calcDist(u, root));
        if (rootFa) disF[root].que.push(Graph.calcDist(u, rootFa));
        for (int e = head[u]; e; e = nxt[e]) {
            int &v = to[e];
            if (!vis[v])
                GetInitiallyHeap(v, root, rootFa);
        }
        vis[u] = false;
    }

    // 获得点分树（预处理）
    void GetPointDivideTree(int u) {
        disU[u].que.push(0); // 自身
        GetInitiallyHeap(u, u, fa[u]);
        vis[u] = true;
        for (int e = head[u]; e; e = nxt[e]) {
            int &v = to[e];
            if (!vis[v]) {
                v = getRoot(v);
                fa[v] = u;
                GetPointDivideTree(v);
                disU[u].que.push(disF[v].top());
            }
        }
        all.que.push(disU[u].getAnswer());
        vis[u] = false;
    }
} pvt; // Point Divide Tree

struct ProblemSolver {
    // 总预处理
    void pretreat() {                           
        IntegerPowerOfTwo_Pretreat();                        
        int root = pvt.getRoot(1);
        Graph.siz[0] = 0;
        Graph.Pretreat(root, 0);       
        Graph.Pretreat2(root, root);                
        pvt.GetPointDivideTree(root);           
    }
    // 树点黑变白
    void turnOn(int x) {
        all.remove(disU[x].getAnswer());
        
        disU[x].insert(0); // x 节点自身
        all.insert(disU[x].getAnswer());
        
        // 维护 all 堆的数据正确性

        int OriginNode = x, f;
        while (fa[x]) {
            // 跳父亲
            f = fa[x];
            all.remove(disU[f].getAnswer());                   // all 先删除冗余信息
            
            if (disU[f].size()) disU[f].remove(disF[x].top()); // disU 先删除冗余信息
            
            disF[x].insert(Graph.calcDist(OriginNode, f));              // disF 直接维护新信息
            
            // debug(disF[x], "disF[x]");
            disU[f].insert(disF[x].top());                     // disU 加入修改后的信息 
            
            all.insert(disU[f].getAnswer());                   // all 加入修改后的信息
            
            x = f;
        }
    }
    // 树点白变黑
    void turnOff(int x) {
        all.remove(disU[x].getAnswer());
        
        disU[x].remove(0); // x 节点自身
        all.insert(disU[x].getAnswer());
        
        // 维护 all 堆的数据正确性

        int OriginNode = x, f;
        while (fa[x]) {
            // 跳父亲
            f = fa[x];
            all.remove(disU[f].getAnswer());                   // all 先删除冗余信息
            
            bool tag = 0;
            if (disU[f].size()) tag = 1;
            if (tag) disU[f].remove(disF[x].top()); // disU 先删除冗余信息
            
            disF[x].remove(Graph.calcDist(OriginNode, f));              // disF 直接维护新信息
            
            // debug(disF[x], "disF[x]");
            if (tag)  disU[f].insert(disF[x].top());                     // disU 加入修改后的信息 
            
            all.insert(disU[f].getAnswer());                   // all 加入修改后的信息
            
            x = f;
        }
    }
    int getAnswer() {
        int ret = all.getTop();
        if (ret < -1e8) return 0;
        else return ret;
    }
} solver;

char getOpt() {
    static char buf[3];
    scanf("%s", buf);
    return buf[0];
}

signed main() {
    CntWhite = n = read<int>();
    MEM_ARR(type, n, white);                    
    for (int i = 1, a, b, c; i < n; i++) {
        a = read<int>();
        b = read<int>();
        c = read<int>();
        linkE(a, b, c);
    }
    solver.pretreat();                          
    Q = read<int>();
    while (Q--) {
        if (getOpt() == 'C') {                  
            // C(hange)
            int x = read<int>();
            if (type[x] == white) solver.turnOff(x);
            else solver.turnOn(x);
            reverseColor(x);
        } else {                                
            // G(ame)
            if (CntWhite == 0) puts("They have disappeared.");
            else write(max(solver.getAnswer(), 0), 10);
        }
    }
    return 0;
}

// Create File Date : 2020-06-12