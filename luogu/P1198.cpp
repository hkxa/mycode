/*************************************
 * problem:      P1198 [JSOI2008]最大数.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-03-10.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 
/*************************************
 * now_type:	 loading
 * now_bak:		 思路错误，写了树状数组，正解线段树
 * next_step:    鸽子 
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

int q;
int D;

class JSOI2008_maxnum_tree {
  private:
  	int n;
  	int las;
  	int tail;
  	int value[200007];
  	inline int lowbit(int num)
  	{
  		return num & -num;
	}
	inline int sum_1toPos(int pos)
	{
		int res = 0;
		while (pos >= 1) {
			res =  (res + value[pos]) % D;
			pos -= lowbit(pos);	
		}
		return res;
	}
  public:
	JSOI2008_maxnum_tree() {
		memset(value, 0, sizeof(value));
		n = 200002;
		las = 0;
		tail = 0;
	}
	inline void add(int diff)
	{
		diff = (diff + las) % D;
		int pos = ++tail;
//		printf("Update pos %d with diff %d.\n", pos, diff);
		while (pos <= n) {
			value[pos] = (value[pos] + diff) % D;
			pos += lowbit(pos);	
		}
	}
	inline int getsum(int L)
	{
//		printf("Getsum from %d to %d.\n", tail - L + 1, tail);
		return las = (sum_1toPos(tail) - sum_1toPos(tail - L) + D) % D;
	}
	inline int getmax(int L)
	{
//		printf("Getsum from %d to %d.\n", tail - L + 1, tail);
		return las = (max_1toPos(tail) - max_1toPos(tail - L) + D) % D;
	}
} tree;

int main()
{
	q = read<int>();
	D = read<int>();
	char str[5];
	int tmp;
	while (q--) {
		scanf("%s", str);
		tmp = read<int>();
		switch (str[0]) {
			case 'A' :
				tree.add(tmp);
				break;
			case 'Q' :
				write(tree.sum(tmp));
				putchar(10);
				break;
			default:
				printf("input error.");
				return 1;
		}
	}
	return 0;
} 
