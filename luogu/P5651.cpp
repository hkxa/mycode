/*************************************
 * @problem:      Monthly Contest III.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2019-11-13.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, m, Q;
struct Edge {
    int b, l;
};
vector<Edge> G[100007];
int x, y;
int dis[100007];

queue<int> q;
#define to G[fr][i].b
#define len G[fr][i].l
void Prepare()
{
    memset(dis, 0x7f, sizeof(dis));
    dis[1] = 0;
    q.push(1);
    while (!q.empty()) {
        int fr = q.front(); q.pop();
        for (unsigned i = 0; i < G[fr].size(); i++) {
            if (dis[to] > (dis[fr] ^ len)) {
                dis[to] = dis[fr] ^ len;
                q.push(to);
            }
        }
    }  
}
#undef u

int main()
{
    n = read<int>();
    m = read<int>();
    Q = read<int>();
    for (int i = 1, a, b, l; i <= m; i++) {
        a = read<int>();
        b = read<int>();
        l = read<int>();
        G[a].push_back((Edge){b, l});
        G[b].push_back((Edge){a, l});
    }
    Prepare();
    int x, y;
    while (Q--) {
        x = read<int>();
        y = read<int>();
        // printf("dis[%d] = %d, dis[%d] = %d\n", x, dis[x], y, dis[y]);
        write(dis[x] ^ dis[y], 10);
    }
    return 0;
}
/*
3 3 3
1 2 2
2 3 3
1 3 7
1 2
1 3
2 3
*/