/*************************************
 * problem:      P3597 [POI2015]WYC.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-09.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, m;
long long k;
char buf[13];

#define EMPTY_MATRIX 0
#define UNIT_MATRIX 1
struct matrix {
    long long m[203][203];
    matrix(int matrixType = EMPTY_MATRIX) 
    {
        memset(m, 0, sizeof(m));
        if (matrixType == UNIT_MATRIX) for (int i = 0; i <= 130; i++) m[i][i] = 1;
    }
    matrix operator * (const matrix &Timeser)
    {
        matrix res;
        for (int i = 0; i <= n * 3; i++) {
            for (int k = 0; k <= n * 3; k++) {
                for (int j = 0; j <= n * 3; j++) {
                    res.m[i][j] += m[i][k] * Timeser.m[k][j];
                }
            }
        }
        return res;
    }
};

matrix G[67], tmp, now(UNIT_MATRIX);

bool count(matrix G)
{
    long long tot(0);
    for (int i = 1; i <= n; i++) {
        tot += G.m[i][0] - 1;
        if (tot >= k) return true;
    }
    return false;
}


int main()
{
    n = read<int>();
    m = read<int>();
    k = read<long long>();
    int u, v, c;
    G[0].m[0][0] = 1;
    for (int i = 1; i <= m; i++) {
        u = read<int>();
        v = read<int>();
        c = read<int>();
        G[0].m[u + (c - 1) * n][v]++;
    }
    for (int i = 1; i <= n; i++) {
        // G[0].m[i + n * 2][i + n * 3] = 1;
        G[0].m[i + n][i + n * 2] = 1;
        G[0].m[i][i + n] = 1;
        G[0].m[i][0] = 1;
    }
    int k;
    for (k = 1; k <= 66; k++) {
        if (k == 66) {
            puts("-1");
            return 0;
        }
        G[k] = G[k - 1] * G[k - 1];
        if (count(G[k])) break;
    }
    long long ans(0);
    for (k--; k >= 0; k--) {
        tmp = now * G[k];
        if (!count(tmp)) {
            now = tmp;
            ans += 1LL << k;
        }
    }
    write(ans, 10);
    return 0;
}