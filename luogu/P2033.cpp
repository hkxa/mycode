/*************************************
 * @problem:      P2033 Chessboard Dance.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-02-02.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int xx[] = {1, 0, -1, 0}, yy[] = {0, -1, 0, 1};

#define pos_exist(x, y) (((x) >= 1) && ((x) <= 8) && ((y) >= 1) && ((y) <= 8))

struct pos {
    char chess[10][10];
    int x, y;
    uint8 dir : 2;
    bool move() 
    {
        int nx = x + xx[dir], ny = y + yy[dir];
        char last_chess = '.';
        if (!pos_exist(nx, ny)) return false;
        // printf("move ok! (%d, %d) move to (%d, %d)\n", x, y, nx, ny);
        x += xx[dir];
        y += yy[dir];
        while (pos_exist(nx, ny) && chess[nx][ny] != '.') {
            swap(last_chess, chess[nx][ny]);
            nx += xx[dir];
            ny += yy[dir];
        }
        if (pos_exist(nx, ny)) swap(last_chess, chess[nx][ny]);
        return true;
    }
    void move(int n)
    {
        while (n && move()) n--; 
    }
    void turn(char ch)
    {
        switch (ch) {
            case 'l' : dir--; break; // left
            case 'r' : dir++; break; // right
            case 'b' : dir ^= 2; break; // back;
        }
    }
    void inputChessboard()
    {
        for (int i = 1; i <= 8; i++) {
            scanf("%s", chess[i] + 1);
            for (int j = 1; j <= 8; j++) {
                switch (chess[i][j]) {
                    case 'v': chess[i][j] = '.'; x = i; y = j; dir = 0; break;
                    case '<': chess[i][j] = '.'; x = i; y = j; dir = 1; break;
                    case '^': chess[i][j] = '.'; x = i; y = j; dir = 2; break;
                    case '>': chess[i][j] = '.'; x = i; y = j; dir = 3; break;
                    default: break;
                } 
            }
        }
    }
    void outputChessboard() 
    {
        switch (dir) {
            case 0: chess[x][y] = 'v'; break;
            case 1: chess[x][y] = '<'; break;
            case 2: chess[x][y] = '^'; break;
            case 3: chess[x][y] = '>'; break;
        }
        for (int i = 1; i <= 8; i++)
            printf("%s\n", chess[i] + 1);
    }
} dancer;

int main()
{
    dancer.inputChessboard();
    char buf[7];
    scanf("%s", buf);
    while (buf[0] != '#') {
        if (buf[0] == 'm') dancer.move(read<int>());
        else {
            scanf("%s", buf);
            dancer.turn(buf[0]);
        }
        scanf("%s", buf);
    }
    dancer.outputChessboard();
    return 0;
}