/*************************************
 * @problem:      T116769 【数学 1.1.1】平均变化率.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-01-18.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

int n;
double k[14], x1, x2;

double f(double x)
{
    double res = 0, power = 1;
    for (int i = 0; i <= n; i++) {
        res += k[i] * power;
        power *= x;
    }
    return res;
}

int main()
{
    scanf("%d", &n);
    for (int i = 0; i <= n; i++) scanf("%lf", k + i);
    scanf("%lf%lf", &x1, &x2);
    printf("%.3lf", (f(x2) - f(x1)) / (x2 - x1));
    return 0;
}