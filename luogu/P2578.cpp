//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      P2578 [ZJOI2005]九数码游戏.
 * @user_name:    brealid.
 * @time:         2020-06-06.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 988244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

void NoAnswer() { puts("UNSOLVABLE"); exit(0); }
const int fac[10] = {1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880};

struct node {
    int data[9];
    node() {}
    node(const int *a) { 
        for (int i = 0; i < 9; i++) data[i] = a[i];
    }
    bool operator < (const node &b) const {
        for (int i = 0; i < 9; i++) 
            if (data[i] ^ b.data[i]) return data[i] < b.data[i];
        return 0;
    }
};
// map<node, int> id; 
struct MAPer {
    int operator [] (node a) {
        int ret = 0;
        for (int i = 0; i < 9; i++) {
            ret += a.data[i] * fac[8 - i];
            for (int j = i + 1; j < 9; j++)
                if (a.data[j] > a.data[i]) a.data[j]--;
            // printf("[i = %d] ret = %d\n", i, ret);
        }
        return ret;
    }
} id;

node status[362880];
int cnt = 0;
int circle(int sid) {
    static int a[9];
    node t = status[sid];
    a[0] = t.data[3]; a[1] = t.data[0]; a[2] = t.data[1];
    a[3] = t.data[6]; a[4] = t.data[4]; a[5] = t.data[2];
    a[6] = t.data[7]; a[7] = t.data[8]; a[8] = t.data[5];
    return id[node(a)];
}
int midmov(int sid) {
    static int a[9];
    node t = status[sid];
    a[0] = t.data[0]; a[1] = t.data[1]; a[2] = t.data[2];
    a[3] = t.data[5]; a[4] = t.data[3]; a[5] = t.data[4];
    a[6] = t.data[6]; a[7] = t.data[7]; a[8] = t.data[8];
    return id[node(a)];
}
int _circle(int sid) {
    static int a[9];
    node t = status[sid];
    a[0] = t.data[1]; a[1] = t.data[2]; a[2] = t.data[5];
    a[3] = t.data[0]; a[4] = t.data[4]; a[5] = t.data[8];
    a[6] = t.data[3]; a[7] = t.data[6]; a[8] = t.data[7];
    return id[node(a)];
}
int _midmov(int sid) {
    static int a[9];
    node t = status[sid];
    a[0] = t.data[0]; a[1] = t.data[1]; a[2] = t.data[2];
    a[3] = t.data[4]; a[4] = t.data[5]; a[5] = t.data[3];
    a[6] = t.data[6]; a[7] = t.data[7]; a[8] = t.data[8];
    return id[node(a)];
}
int mini[362880], fa[362880];
void PrepareData() {
    int a[9] = {0, 1, 2, 3, 4, 5, 6, 7, 8};
    do {
        // if (cnt != id[node(a)]) printf("cnt = %d\n", cnt);
        mini[cnt] = 56;
        status[cnt++] = node(a);
    } while (next_permutation(a, a + 9));
}
void bfs(int now) {
    int t;
    queue<int> q;
    q.push(now);
    mini[now] = 0;
    while (!q.empty()) {
        now = q.front(); q.pop();
        t = circle(now);
        if (mini[t] > mini[now] + 1) {
            mini[t] = mini[now] + 1;
            fa[t] = 1;
            q.push(t);
        }
        t = midmov(now);
        if (mini[t] > mini[now] + 1) {
            mini[t] = mini[now] + 1;
            fa[t] = 2;
            q.push(t);
        }
    }
}

void GetPerm(int u) {
    if (fa[u]) {
        if (fa[u] == 1) GetPerm(_circle(u));
        else GetPerm(_midmov(u));
    }
    for (int i = 0; i < 9; i++) {
        printf("%d%c", status[u].data[i], " \n"[i % 3 == 2]);
    }
    putchar(10);
}

signed main() {
    PrepareData();
    int a[9], s;
    for (int i = 0; i < 9; i++) a[i] = read<int>();
    s = id[node(a)];
    // for (int i = 0; i < 9; i++) a[i] = read<int>();
    // t = id[node(a)];
    bfs(s);
    if (mini[0] == 56) NoAnswer();
    else write(mini[0], 10);
    GetPerm(0);
    return 0;
}

// Create File Date : 2020-06-06