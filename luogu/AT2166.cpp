/*************************************
 * problem:      id_name.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-mm-dd.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define puts_return(x) { puts(x); return 0; }
#define write_return(x) { write(x); return 0; }

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;
int a[4][100003];
int id[100003], cnt[2];

#define FOR_IN_ROW(i) for (int i = 1; i <= 3; i++)
#define FOR_IN_COL(j) for (int j = 1; j <= n; j++)
#define FOR_IN_GRID(i, j) FOR_IN_ROW(i) FOR_IN_COL(j)
#define ReNo() puts_return("No");
#define ReYes() puts_return("Yes");
#define ifReNo(x) { if (x) ReNo(); }

int main()
{
    n = read<int>();
    FOR_IN_GRID(i, j) {
        a[i][j] = read<int>();
    }
    FOR_IN_COL(i) {
        ifReNo(a[2][i] % 3 != 2); 
        ifReNo(abs(a[1][i] - a[2][i]) != 1 || abs(a[1][i] - a[2][i]) != 1); 
        ifReNo(((a[2][i] % 6 / 3) & (i & 1)));
        id[i] = (a[2][i] + 1) / 3;
        cnt[i & 1] ^= a[1][i] > a[3][i];
    }
    FOR_IN_COL(i) {
        while (id[i] != i) {
            cnt[(i & 1) ^ 1] ^= 1;
            swap(id[i], id[id[i]]);
        }
    }
    ifReNo(cnt[0] || cnt[1]);
    ReYes();
    return 0;
}