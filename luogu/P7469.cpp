/*************************************
 * @problem:      block.
 * @author:       brealid.
 * @time:         2021-03-27.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 3e3 + 7, K = 500 + 7;
const int64 P1 = 1e9 + 7, P2 = 1e9 + 9, K1 = 131, K2 = 137;

int nxt[N][26];
char buf[N];
int n, alice[N], bob[N];
int head[26];
int64 HASH_1[N], POWER_1[N], HASH_2[N], POWER_2[N];

namespace HashMap {
    int head[1048576], v1[9000007], v2[9000007], nxt[9000007], cnt;
    void insert(int64 x1, int64 x2) {
        int wh = (x1 & 1047552) | (x2 & 1023);
        for (int p = head[wh]; p; p = nxt[p])
            if (v1[p] == x1 && v2[p] == x2) return;
        nxt[++cnt] = head[wh];
        head[wh] = cnt;
        v1[cnt] = x1, v2[cnt] = x2;
    }
}

#define hash1(l, r) ((HASH_1[(r)] - HASH_1[(l) - 1] * POWER_1[(r) - (l) + 1] % P1 + P1) % P1)
#define hash2(l, r) ((HASH_2[(r)] - HASH_2[(l) - 1] * POWER_2[(r) - (l) + 1] % P2 + P2) % P2)

signed main() {
    // file_io::set_to_file("block");
    kin >> n >> (buf + 1);
    for (int i = 1; i <= n; ++i) alice[i] = buf[i] - 'a';
    kin >> (buf + 1);
    HASH_1[0] = POWER_1[0] = HASH_2[0] = POWER_2[0] = 1;
    for (int i = 1; i <= n; ++i) {
        bob[i] = buf[i] - 'a';
        HASH_1[i] = (HASH_1[i - 1] * K1 + bob[i] + 1) % P1;
        POWER_1[i] = POWER_1[i - 1] * K1 % P1;
        HASH_2[i] = (HASH_2[i - 1] * K2 + bob[i] + 1) % P2;
        POWER_2[i] = POWER_2[i - 1] * K2 % P2;
    }
    memset(head, -1, sizeof(head));
    for (int i = n; i >= 0; --i) {
        memcpy(nxt[i], head, sizeof(head));
        head[alice[i]] = i;
    }
    for (int i = 1; i <= n; ++i) {
        int pos = 0, j = i;
        while (j <= n) {
            pos = nxt[pos][bob[j]];
            if (!~pos) break;
            // s.insert(hash(i, j));
            HashMap::insert(hash1(i, j), hash2(i, j));
            // printf("Matched: '%s':%lld\n", string(buf + 1).substr(i - 1, j - i + 1).c_str(), hash(i, j));
            ++j;
        }
    }
    // kout << s.size() << '\n';
    kout << HashMap::cnt << '\n';
    return 0;
}