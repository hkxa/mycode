//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      【模板】快速沃尔什变换 (FWT).
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-14.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

int fpow(int a, int n, const int P) {
    int ret = 1;
    while (n) {
        if (n & 1) ret = (int64)ret * a % P;
        a = (int64)a * a % P;
        n >>= 1;
    }
    return ret;
}

namespace FWT {
    void FWT_OR(int *a, int n, int MUL = 1, const int P = 998244353) {
        for (int s = 2, t = 1; s <= n; s <<= 1, t <<= 1)
            for (int i = 0; i < n; i += s)
                for (int j = 0; j < t; j++)
                    a[i + j + t] = (a[i + j + t] + (int64)a[i + j] * MUL) % P;
    }
    void OR(int *a, int *b, int *c, int n, const int P = 998244353) {
        FWT_OR(a, n, 1, P);
        FWT_OR(b, n, 1, P);
        for (int i = 0; i < n; i++) c[i] = (int64)a[i] * b[i] % P;
        FWT_OR(c, n, P - 1, P);
    }
    void FWT_AND(int *a, int n, int MUL = 1, const int P = 998244353) {
        for (int s = 2, t = 1; s <= n; s <<= 1, t <<= 1)
            for (int i = 0; i < n; i += s)
                for (int j = 0; j < t; j++)
                    a[i + j] = (a[i + j] + (int64)a[i + j + t] * MUL) % P;
    }
    void AND(int *a, int *b, int *c, int n, const int P = 998244353) {
        FWT_AND(a, n, 1, P);
        FWT_AND(b, n, 1, P);
        for (int i = 0; i < n; i++) c[i] = (int64)a[i] * b[i] % P;
        FWT_AND(c, n, P - 1, P);
    }
    void FWT_XOR(int *a, int n, int MUL = 1, const int P = 998244353) {
        for (int s = 2, t = 1; s <= n; s <<= 1, t <<= 1)
            for (int i = 0; i < n; i += s)
                for (int j = 0; j < t; j++) {
                    int a0 = a[i + j], a1 = a[i + j + t];
                    a[i + j] = (a0 + a1) % P;
                    a[i + j + t] = (a0 - a1 + P) % P;
                    a[i + j] = (int64)a[i + j] * MUL % P;
                    a[i + j + t] = (int64)a[i + j + t] * MUL % P; 
                }
    }
    void XOR(int *a, int *b, int *c, int n, const int P = 998244353) {
        FWT_XOR(a, n, 1, P);
        FWT_XOR(b, n, 1, P);
        for (int i = 0; i < n; i++) c[i] = (int64)a[i] * b[i] % P;
        FWT_XOR(c, n, fpow(2, P - 2, P), P);
    }
}

namespace against_cpp11 {
    const int N = 1e6 + 7;
    int n;
    int A[N], B[N];
    int a[N], b[N], c[N];
    signed main() {
        read >> n;
        n = (1 << n);
        for (int i = 0; i < n; i++) read >> A[i];
        for (int i = 0; i < n; i++) read >> B[i];
        for (int i = 0; i < n; i++) {
            a[i] = A[i];
            b[i] = B[i];
        }
        FWT::OR(a, b, c, n);
        for (int i = 0; i < n; i++)
            write << c[i] << " \n"[i == n - 1];
        for (int i = 0; i < n; i++) {
            a[i] = A[i];
            b[i] = B[i];
        }
        FWT::AND(a, b, c, n);
        for (int i = 0; i < n; i++)
            write << c[i] << " \n"[i == n - 1];
        for (int i = 0; i < n; i++) {
            a[i] = A[i];
            b[i] = B[i];
        }
        FWT::XOR(a, b, c, n);
        for (int i = 0; i < n; i++)
            write << c[i] << " \n"[i == n - 1];
        return 0;
    }
}

signed main() { return against_cpp11::main(); }