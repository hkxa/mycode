#include <stdio.h>
int m;
int t[1000] = {0}, i = 1;
int main()
{
    scanf("%d", &m);
    while (m) {
        t[i++] = (m + 1) >> 1;
        m -= (m + 1) >> 1;
    }
    printf("%d\n", i - 1);
    while (--i) {
        printf("%d ", t[i]);
    }
}