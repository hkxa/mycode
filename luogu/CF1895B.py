T = int(input())

for _ in range(T):
    n = int(input())
    a = sorted(list(map(int, input().split())))
    print(a[2 * n - 1] - a[n] + a[n - 1] - a[0])
    for i in range(n):
        print(a[i], a[i + n])