/*************************************
 * @problem:      Serega and Fun.
 * @author:       brealid.
 * @time:         2021-01-08.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 1e5 + 7, M = 350;

int n, q, block;
deque<int> v[M];
int lef[M], rig[M], where[N];
int c[M][N];

signed main() {
    kin >> n;
    block = sqrt(n);
    for (int i = 1, col; i <= n; ++i) {
        kin >> col;
        where[i] = (i - 1) / block + 1;
        rig[where[i]] = i;
        if (!lef[where[i]]) lef[where[i]] = i;
        v[where[i]].push_back(col);
        ++c[where[i]][col];
    }
    kin >> q;
    for (int i = 1, op, l, r, bl, br, k, ans = 0; i <= q; ++i) {
        kin >> op >> l >> r;
        l = (l + ans - 1) % n + 1;
        r = (r + ans - 1) % n + 1;
        if (l > r) swap(l, r);
        bl = where[l];
        br = where[r];
        if (op == 1) {
            if (bl == br) {
                int x = v[bl][r - lef[bl]];
                v[bl].erase(v[bl].begin() + (r - lef[bl]));
                v[bl].insert(v[bl].begin() + (l - lef[bl]), x);
            } else {
                int x = v[br][r - lef[br]];
                ++c[bl][x], --c[br][x];
                v[br].erase(v[br].begin() + (r - lef[br]));
                v[bl].insert(v[bl].begin() + (l - lef[bl]), x);
                for (int i = bl; i < br; ++i) {
                    --c[i][v[i].back()];
                    ++c[i + 1][v[i].back()];
                    v[i + 1].push_front(v[i].back());
                    v[i].pop_back();
                }
            }
        } else {
            kin >> k;
            k = (k + ans - 1) % n + 1;
            ans = 0;
            if (bl == br) {
                for (int i = l; i <= r; ++i)
                    if (v[bl][i - lef[bl]] == k) ++ans;
            } else {
                for (int i = l; i <= rig[bl]; ++i)
                    if (v[bl][i - lef[bl]] == k) ++ans;
                for (int i = lef[br]; i <= r; ++i)
                    if (v[br][i - lef[br]] == k) ++ans;
                for (int i = bl + 1; i < br; ++i)
                    ans += c[i][k];
            }
            kout << ans << '\n';
        }
    }
    return 0;
}