/*************************************
 * problem:      P2675 《瞿葩的数字游戏》T3-三角圣地.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-10-06.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define ForInVec(vectorType, vectorName, iteratorName) for (vector<vectorType>::iterator iteratorName = vectorName.begin(); iteratorName != vectorName.end(); iteratorName++)
#define ForInVI(vectorName, iteratorName) ForInVec(int, vectorName, iteratorName)
#define ForInVE(vectorName, iteratorName) ForInVec(Edge, vectorName, iteratorName)
#define MemWithNum(array, num) memset(array, num, sizeof(array))
#define Clear(array) MemWithNum(array, 0)
#define MemBint(array) MemWithNum(array, 0x3f)
#define MemInf(array) MemWithNum(array, 0x7f)
#define MemEof(array) MemWithNum(array, -1)
#define ensuref(condition) do { if (!(condition)) exit(0); } while(0)

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define int long long

int n, a[1000007];

int power(int a, int n, int base = 1)
{
    while (n) {
        if (n & 1) base = base * a % 10007;
        a = a * a % 10007;
        n >>= 1;
    }
    return base;
}

int basic_C(int n, int m) 
{
    static bool inited = false;
    static int factor[10077];
    if (!inited) {
        factor[1] = factor[0] = 1;
        for (int i = 2; i <= n; i++) factor[i] = factor[i - 1] * i % 10007;
        inited = true;
    }
    if (m == n || m == 0) return 1;
    if (m > n) return 0;
    return factor[n] * power(factor[m] * factor[n - m] % 10007, 10007 - 2) % 10007;
}

int Lucas(int n, int m) 
{
    if (m == n || m == 0) return 1;
    if (m > n) return 0;
    return basic_C(n % 10007, m % 10007) * Lucas(n / 10007, m / 10007) % 10007;
}

signed main() 
{
    n = read<int>();
    for (int i = 1; i <= n; i += 2) a[i / 2] = i;
    for (int i = 2; i <= n; i += 2) a[n - i / 2] = i;
    int ans = 0;
    for (int i = 0; i < n; i++) 
        ans = (ans + Lucas(n - 1, i) * a[i]) % 10007;
    write(ans);
    return 0;
}