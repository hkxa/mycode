/*************************************
 * @problem:      Drazil and Park.
 * @author:       brealid.
 * @time:         2021-02-02.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 1e5 + 7;

int n, m;
int d[N], h[N];
int64 s[N << 1];
int64 up[N << 1], dn[N << 1];

namespace tr{
    int64 mx_up[N << 3], mn_dn[N << 3], mxd[N << 3];

    void build(int u, int l, int r) {
        if (l == r) {
            mx_up[u] = up[l];
            mn_dn[u] = dn[l];
            mxd[u] = 0;
            return;
        }
        int mid = (l + r) >> 1, ls = u << 1, rs = ls | 1;
        build(ls, l, mid);
        build(rs, mid + 1, r);
        mx_up[u] = max(mx_up[ls], mx_up[rs]);
        mn_dn[u] = min(mn_dn[ls], mn_dn[rs]);
        mxd[u] = max(mx_up[rs] - mn_dn[ls], max(mxd[ls], mxd[rs]));
    }

    int64 mn_dn_now;

    int64 __query(int u, int l, int r, int ml, int mr) {
        if (l >= ml && r <= mr) {
            int64 res = max(mxd[u], mx_up[u] - mn_dn_now);
            mn_dn_now = min(mn_dn_now, mn_dn[u]);
            return res;
        }
        int64 res = 0;
        int mid = (l + r) >> 1, ls = u << 1, rs = ls | 1;
        if (mid >= ml) res = __query(ls, l, mid, ml, mr);
        if (mid < mr) res = max(res, __query(rs, mid + 1, r, ml, mr));
        return res;
    }

    int64 query(int l, int r) {
        mn_dn_now = 0x3f3f3f3f3f3f3f3f;
        return __query(1, 1, 2 * n, l, r);
    }
}

signed main() {
    kin >> n >> m;
    for (int i = 1; i <= n; ++i) kin >> d[i];
    for (int i = 1; i <= n; ++i) kin >> h[i];
    for (int i = 1; i <= n; ++i) s[i] = s[i - 1] + d[i - 1];
    s[n + 1] = s[n] + d[n];
    for (int i = n + 2; i <= 2 * n; ++i) s[i] = s[i - 1] + d[i - n - 1];
    for (int i = 1; i <= n; ++i) {
        dn[i] = s[i] - h[i] * 2;
        up[i] = s[i] + h[i] * 2;
    }
    for (int i = n + 1; i <= 2 * n; ++i) {
        dn[i] = s[i] - h[i - n] * 2;
        up[i] = s[i] + h[i - n] * 2;
    }
    tr::build(1, 1, 2 * n);
    // for (int i = 1; i <= 2 * n; ++i)
    //     printf("up[%-2d] = %-2lld  dn[%-2d] = %-2lld\n", i, up[i], i, dn[i]);
    for (int i = 1, a, b; i <= m; ++i) {
        kin >> a >> b;
        if (a <= b) kout << tr::query(b + 1, n + a - 1) << '\n';
        else kout << tr::query(b + 1, a - 1) << '\n';
    }
    // For each query [l, r], get max{up[j] - dn[i]}, l <= i < j <= r
    return 0;
}