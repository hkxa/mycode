/*************************************
 * problem:      P1379 八数码难题.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-02-24.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 
/*************************************
 * record ID:    R16624846.
 * status:       AC 100 pts.
 * time:         8119 ms
 * memory:       11876 KB
 * ststus - AC:  31 blocks, 100 pts.
 * ststus - PC:  0 blocks, 0 pts.
 * ststus - WA:  0 blocks, 0 pts.
 * ststus - RE:  0 blocks, 0 pts. 
 * ststus - TLE: 0 blocks, 0 pts. 
 * ststus - MLE: 0 blocks, 0 pts. 
 * ststus - OLE: 0 blocks, 0 pts. 
*************************************/ 
#include <bits/stdc++.h>
#define swap(a, b) { typeof(a) t = a; a = b; b = t; }
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

const int dx[4] = {-1,  0, 0, 1};
const int dy[4] = { 0, -1, 1, 0};

map<long long, long long> m;
long long x, tmp_div;
queue<long long> q;
int t[3][3];
int sx, sy;
int tx, ty;
long long tmp;

int main()
{
    x = read<long long>();
    q.push(x);
    m[x] = 0;
    while(!q.empty()) {
        x = q.front();
        q.pop();
//        printf("bfs element %lld.\n", x);
        if (x == 123804765LL) {
        	break;	
		}
		tmp_div = x;
        for (int i = 2; i >= 0; i--) {
            for (int j = 2; j >= 0; j--) {
                t[i][j] = tmp_div % 10;
				tmp_div /= 10;
                if (t[i][j] == 0) {
                	sx = i;
                	sy = j;
				}
            }
        }
        for (int i = 0; i < 4; i++) {
            tx = sx + dx[i];
			ty = sy + dy[i];
			tmp = 0;
            if (tx < 0 || ty < 0 || tx > 2 || ty > 2) {
				continue; 
			}
            t[sx][sy] = t[tx][ty];
            t[tx][ty] = 0;
            for (int i = 0; i <= 2; i++) {
            	for (int j = 0; j <= 2; j++) {
            		tmp = (((tmp << 2) + tmp) << 1) + t[i][j];
				}
			}
            if (!m.count(tmp)) {
                m[tmp] = m[x] + 1;
                q.push(tmp);
            }
            t[tx][ty] = t[sx][sy];
            t[sx][sy] = 0;
        }
    }
    write<long long>(m[123804765]);
    return 0;
}
