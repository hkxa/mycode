/*************************************
 * problem:      P5190 [COCI 2010] PROGRAM.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-mm-dd.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define MaxN 1000007

int n, k, q;
int Xi, Li, Ri;
int cnt[MaxN], seq[MaxN];
int64 sum[MaxN];

int main()
{
    n = read<int>();
    k = read<int>();
    while (k--) {      
        Xi = read<int>();
        cnt[Xi]++;
    }
    for (int i = 1; i < n; i++) {
        if (!cnt[i]) continue;
        for (int j = 0; j < n; j += i) {
            seq[j] += cnt[i];
        }
    }
    sum[0] = seq[0];
    for (int i = 1; i < n; i++) {
        sum[i] = sum[i - 1] + seq[i];
    }
    q = read<int>();
    while (q--) {
        Li = read<int>();
        Ri = read<int>();
        if (Li) write(sum[Ri] - sum[Li - 1], 10);
        else write(sum[Ri], 10);
    }
    return 0;   
}