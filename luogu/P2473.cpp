/*************************************
 * problem:      P2473 [SCOI2008]奖励关.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-05-30.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

int price[20], pre[20];
double f[100 + 5][32768 + 5];
int k, n;

namespace DoubleTool {
    double max(double a , double b)
    {
        return a > b ? a : b;
    }
}

int main()
{
    int readTmp;
    k = read<int>();
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        price[i] = read<int>();
        while (readTmp = read<int>()) {
            pre[i] |= 1 << (readTmp - 1);
        }
    }
    for (int i = k; i > 0; i--) {
        for (int j = 0; j < (1 << n); j++) {
            for (int l = 1; l <= n; l++) {
                if ((~j) & pre[l]) {
                    f[i][j] += f[i + 1][j];
                } else {
                    f[i][j] += DoubleTool::max(f[i + 1][j] , f[i + 1][j | (1 << (l - 1))] + price[l]);
                }
            }
            f[i][j] /= n;
        }
    }
    printf("%.6lf\n" , f[1][0]);
    return 0;
}