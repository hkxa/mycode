//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      P4501 [ZJOI2018]胖.
 * @user_id:      63720.
 * @user_name:    brealid.
 * @time:         2020-05-29.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
       return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
       return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define int int64
#define inf 0x3f3f3f3f3f3f3f3f

const int N = 262144 + 7, lgN = 20;
int n, m;
int64 w[N];
int k;
struct Road {
    int a;
    int64 l;
    bool operator < (const Road &other) const {
        return a < other.a;
    }
} r[N];
int64 ans;

#define dist(i, j) (w[j] - w[i])

namespace MathTool {
    int bin[lgN], lg2[N];
    inline void pretreat() {
        bin[0] = 1;
        for (int i = 1; i <= 18; i++) {
            bin[i] = bin[i - 1] << 1;
            lg2[bin[i]] = i;
        } 
        for (int i = 1; i <= 200000; i++) {
            if (!lg2[i]) lg2[i] = lg2[i - 1];
        }
    }
};

struct STtable {
    int tab[N][lgN];
    inline void clear() {
        // memset(tab, 0x3f, sizeof(tab));
    }
    inline void pretreat() {
        using namespace MathTool;
        for (int j = 1; j <= lg2[k]; j++) {
            for (int i = 1; i + bin[j] - 1 <= k; i++) {
                tab[i][j] = min(tab[i][j - 1], tab[i + bin[j - 1]][j - 1]);
            }
        }
    }
    inline int query(int l, int r) {
        using namespace MathTool;
        int d = lg2[r - l + 1];
        return min(tab[l][d], tab[r - bin[d] + 1][d]);
    }
} lef, rig;

int lower(int V) {
    int L = 1, R = k, mid, ans = k + 1;
    while (L <= R) {
        mid = (L + R) >> 1;
        if (r[mid].a < V) L = mid + 1;
        else R = mid - 1, ans = mid;
    }
    return ans;
}

int upper(int V) {
    int L = 1, R = k, mid, ans = k + 1;
    while (L <= R) {
        mid = (L + R) >> 1;
        if (r[mid].a <= V) L = mid + 1;
        else R = mid - 1, ans = mid;
    }
    return ans;
}

signed main() {
    MathTool::pretreat();
    n = read<int>();
    m = read<int>();
    for (int i = 1; i < n; i++) {
        w[i + 1] = w[i] + read<int>();
    }
    for (int i = 1; i <= m; i++) {
        ans = 0;
        k = read<int>();
        for (int j = 1; j <= k; j++) {
            r[j].a = read<int>();
            r[j].l = read<int>();
        }
        sort(r + 1, r + k + 1);
        lef.clear(); rig.clear();
        for (int j = 1; j <= k; j++) {
            lef.tab[j][0] = r[j].l + w[r[j].a];
            rig.tab[j][0] = r[j].l - w[r[j].a];
        }
        lef.pretreat(); rig.pretreat();
        for (int j = 1; j <= k; j++) {            
            int ql = 1, qr = r[j].a - 1, L = r[j].a, R = r[j].a, mid;
            int64 nowdis, idx, dl, dr;
            while (qr >= ql) {
                mid = (ql + qr) >> 1;
                nowdis = r[j].l + dist(mid, r[j].a);
                idx = r[j].a - mid;
                dl = dr = inf;
                
                int qql = lower(max(1ll, mid - idx));
                int qqr = upper(mid) - 1;
                if (qql <= qqr) dl = rig.query(qql, qqr);
                
                qql = lower(mid);
                qqr = upper(r[j].a - 1) - 1;
                if (qql <= qqr) dr = lef.query(qql, qqr);
                
                if (dl + w[mid] <= nowdis || dr - w[mid] <= nowdis) ql = mid + 1;
                else qr = mid - 1, L = mid;
            }
            ql = r[j].a + 1, qr = n;
            while (qr >= ql) {
                bool Correct = 1;
                int mid = (ql + qr) >> 1;
                nowdis = r[j].l + dist(r[j].a, mid);
                idx = mid - r[j].a;
                dl = dr = inf;
                
                int qql = lower(mid);
                int qqr = upper(min(mid + idx - 1, n)) - 1;
                if (qql <= qqr) dr = lef.query(qql, qqr);
                
                qql = lower(r[j].a + 1);
                qqr = upper(mid) - 1;
                if (qql <= qqr) dl = rig.query(qql, qqr);
                
                if (dl + w[mid] <= nowdis) Correct = 0;
                if (dr - w[mid] <= nowdis) Correct = 0;
                if (mid + idx <= n) {
                    qqr = upper(min(mid + idx, n)) - 1;
                    if (r[qqr].a == mid + idx)
                        if (lef.tab[qqr][0] - w[mid] < nowdis)
                            Correct = 0;
                }
                if (Correct) ql = mid + 1, R = mid;
                else qr = mid - 1;
            }
            ans += R - L + 1;
            // printf("point %d manage [%d, %d]\n", r[j].a, L, R);
        }
        write(ans, 10);
    }
    return 0;
}

// Create File Date : 2020-05-29