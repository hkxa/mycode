/*************************************
 * @problem:      Sudoku.
 * @author:       brealid.
 * @time:         2020-12-27.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;
#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 4) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

class DancingLinksX {
private:
    struct DancingLinksX_Node {
        int l, r, u, d; // l, r, u, d 是指向左, 右, 上, 下的指针
        int row, col;   // 第 row 行，第 col 列
        DancingLinksX_Node() : l(0), r(0), u(0), d(0), row(0), col(0) {}
    };
    std::vector<DancingLinksX_Node> node; // 存储节点
    std::vector<int> first;               // 存储每行的头结点
    std::vector<int> siz;                 // 存储每列 '1' 的个数
    std::vector<int> result;              // 存放答案
    int cnt;
    bool dance() {
        if (!node[0].r) return true;
        int minimum_col = node[0].r;
        for (int i = node[0].r; i != 0; i = node[i].r)
            if (siz[i] < siz[minimum_col]) minimum_col = i;
        remove(minimum_col);
        result.push_back(0);
        for (int i = node[minimum_col].d; i != minimum_col; i = node[i].d) {
            result.back() = node[i].row;
            for (int j = node[i].l; j != i; j = node[j].l) remove(node[j].col);
            if (dance()) return true;
            for (int j = node[i].r; j != i; j = node[j].r) recover(node[j].col);
        }
        result.pop_back();
        recover(minimum_col);
        return false;
    }
public:
    DancingLinksX() : cnt(0) {}
    /**
     * @param row 行数
     * @param col 列数
     * @param count_of_1 最多有几个'1'
     */
    DancingLinksX(int row, int col, int count_of_1) { init(row, col, count_of_1); }
    /**
     * @param row 行数
     * @param col 列数
     * @param count_of_1 最多有几个'1'
     */
    void init(int row, int col, int count_of_1) {
        cnt = col;
        first.assign(row + 1, 0);
        siz.assign(col + 1, 0);
        node.assign(col + count_of_1 + 1, DancingLinksX_Node());
        for (int i = 0; i <= col; ++i) {
            node[i].l = i - 1;
            node[i].r = i + 1;
            node[i].u = node[i].d = i;
        }
        node[0].l = col;
        node[col].r = 0;
    }
    void insert(int r, int c) {
        DancingLinksX_Node &self = node[++cnt];
        self.row = r;
        self.col = c;
        ++siz[c];
        self.u = node[c].u;
        node[self.u].d = cnt;
        self.d = c;
        node[c].u = cnt;
        if (!first[r]) first[r] = self.l = self.r = cnt;
        else {
            self.l = node[first[r]].l;
            self.r = first[r];
            node[node[first[r]].l].r = cnt;
            node[first[r]].l = cnt;
        }
        for (int j = node[first[r]].l; j != first[r]; j = node[j].l);
        for (int j = node[first[r]].r; j != first[r]; j = node[j].r);
        for (int j = node[cnt].l; j != cnt; j = node[j].l);
        for (int j = node[cnt].r; j != cnt; j = node[j].r);
    }
    void remove(int u) {
        DancingLinksX_Node &self = node[u];
        node[self.l].r = self.r;
        node[self.r].l = self.l;
        for (int i = self.u; i != u; i = node[i].u)
            for (int j = node[i].l; j != i; j = node[j].l) {
                node[node[j].d].u = node[j].u;
                node[node[j].u].d = node[j].d;
                --siz[node[j].col];
            }
    }
    void recover(int u) {
        DancingLinksX_Node &self = node[u];
        for (int i = self.d; i != u; i = node[i].d)
            for (int j = node[i].r; j != i; j = node[j].r) {
                node[node[j].d].u = node[node[j].u].d = j;
                ++siz[node[j].col];
            }
        node[self.l].r = node[self.r].l = u;
    }
    const vector<int>& solve() {
        result.clear();
        dance();
        return result;
    }
};

signed main() {
    for (bool IsFirstCase = true;;) {
        DancingLinksX DLX(4096, 1024, 16384);
        char str[20];
        for (int i = 1, w; i <= 16; ++i) {      // i 行
            if (scanf("%s", str + 1) != 1) return 0;
            for (int j = 1; j <= 16; ++j) {     // j 列
                if (str[j] != '-') {
                    w = str[j] & 31;
                    int r = (i - 1) * 256 + (j - 1) * 16 + w;
                    int b = (i - 1) / 4 * 4 + (j + 3) / 4;
                    DLX.insert(r, (i - 1) * 16 + w);
                    DLX.insert(r, (j - 1) * 16 + w + 256);
                    DLX.insert(r, (b - 1) * 16 + w + 512);
                    DLX.insert(r, (i - 1) * 16 + j + 768);
                    continue;
                }
                for (w = 1; w <= 16; ++w) {  // 数字 w
                    int r = (i - 1) * 256 + (j - 1) * 16 + w;
                    int b = (i - 1) / 4 * 4 + (j + 3) / 4;
                    DLX.insert(r, (i - 1) * 16 + w);
                    DLX.insert(r, (j - 1) * 16 + w + 256);
                    DLX.insert(r, (b - 1) * 16 + w + 512);
                    DLX.insert(r, (i - 1) * 16 + j + 768);
                }
            }
        }
        if (!IsFirstCase) kout << '\n';
        else IsFirstCase = false;
        vector<int> ans = DLX.solve();
        if (ans.empty()) kout << "No Solution!\n";
        else {
            sort(ans.begin(), ans.end());
            for (size_t i = 0; i < ans.size(); ++i) {
                kout << char((ans[i] - 1) % 16 + 'A');
                if (i % 16 == 15) kout << '\n';
            }
        }
    }
    return 0;
}