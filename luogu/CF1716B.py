def solve():
    n = int(input())
    print(n)
    for i in range(2, n+2):
        print(*range(2, i), 1, *range(i, n + 1))

T = int(input())
while T:
    solve()
    T -= 1