cases = int(input())

rl = lambda x: map(x, input().split())

for _ in range(cases):
    n, m, k = rl(int)
    a = list(rl(int))
    b = list(rl(int))
    if k > 10:
        k = (k - 10) % 2 + 10
    for i in range(k):
        if i % 2 == 0:
            # a: swap
            mn = min(a)
            mx = max(b)
            if mn < mx:
                ax = a.index(mn)
                bx = b.index(mx)
                a[ax], b[bx] = b[bx], a[ax]
        else:
            # b: swap
            mx = max(a)
            mn = min(b)
            if mn < mx:
                ax = a.index(mx)
                bx = b.index(mn)
                a[ax], b[bx] = b[bx], a[ax]
    print(sum(a))