/*************************************
 * problem:      P1341 无序字母对.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-03-16.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 
/*************************************
 * record ID:    R17308955.
 * time:         30 ms
 * memory:       788 KB
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
	char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

int n;
int G[52][52] = {0}, degree[52];
int s[53 * 53 + 7], top = 0;
bool vis[52] = {0};

int get_id(char ch)
{
	if (ch >= 'A' && ch <= 'Z') return ch - 'A';
	else return ch - 'a' + 26;
}

char get_ch(int id)
{
	if (id < 26) return id + 'A';
	else return id + 'a' - 26;
}

class answer {
  private:
	char str[2704 + 5];
	int tail;
  public:
    int oddCount;
  	answer() : tail(0) {}
  	void push_back(int id)
  	{
  		str[tail++] = get_ch(id);
	}
	void putAns()
	{
		str[tail] = '\0';
		// printf("%c%s", str[tail - 1], str);
//        printf(str);
//        if (oddCount == 0) putchar(str[0]);
		// printf("%s%c", str, str[0]);
		if (tail != n + 1) return puts("No Solution"), void();
		for (int i = tail - 1; i >= 0; i--) {
			putchar(str[i]);
		}
//		putchar(str[tail - 1]);
	}
} ans;

void connect(int u, int v)
{
	degree[u]++;
	degree[v]++;
	G[u][v]++;
	G[v][u]++;
}

void readEdge()
{
	static char str[3];
	scanf("%s", str);
	connect(get_id(str[0]), get_id(str[1]));
}

void fail()
{
	printf("No Solution");
	return;
}

bool eulerTest(int &start)
{
	ans.oddCount = 0;
	start = -1;
    int evenStart = -1;
	for (int i = 0; i < 52; i++) {
		if (degree[i] & 1) {
			ans.oddCount++;
			if (start == -1) start = i;
		} else if (degree[i] && evenStart == -1) {
			evenStart = i;
		}
	}
    if (start == -1) start = evenStart;
	// return ans.oddCount == 0;
	return ans.oddCount == 0 || ans.oddCount == 2;
}

void dfs(int p)
{
//	printf("-->%c", get_ch(p));
	s[++top] = p;
	// vis[p] = 1; 
//    bool visd = 0;
	for (int i = 0; i < 52; i++) {
		if (G[p][i]) {
//			printf("(%c test-find %c)", get_ch(p), get_ch(i));
            // if (visd) ans.push_back(s[top]);
            // else visd = 1;
//            ans.push_back(s[top]);
//            visd = 1;
            // printf("push(%c).\n", get_ch(s[top]));
            G[p][i]--;
            G[i][p]--;
            dfs(i);
        }
	}
	 ans.push_back(s[top--]);
//    if (!visd) ans.push_back(s[top]);
//    top--;
//    printf("|");
}

void euler()
{
	int start = -1;
	if (!eulerTest(start)) {
        fail();
        return;
    }
//    printf("[s]");
	dfs(start);
//	printf("-->[e]\n");
    ans.putAns();
}

//bool Bigger(int a, int b)
//{
//    return a > b;
//}

int main()
{
	n = read<int>();
	for (int i = 1; i <= n; i++) {
		readEdge();
	}
    /*
	for (int i = 0; i < 52; i++) {
        if (G[i].size() >= 2) {
            // printf("sort %d\n", i);
            // sort(G[i].begin(), G[i].end(), Bigger);
            sort(G[i].begin(), G[i].end());
        }
	}
    */
	euler();
    return 0;
}
