//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      「APIO2019」桥梁.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-01.
 * @language:     C++.
*************************************/ 
#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

struct Reader {
    char endch;
    Reader() { endch = '\0'; }
    template <typename Int>
    Reader& operator >> (Int &i) {
        i = baseFastio::read<Int>(endch);
        return *this;
    }
    template <typename Int>
    inline Int get_int() {
        return baseFastio::read<Int>();
    }
    inline char get_nxt() {
        return endch;
    }
} read;

struct Writer {
    Writer& operator << (const char *ch) {
        // char *p = ch;
        while (*ch) putchar(*(ch++));
        return *this;
    }
    Writer& operator << (const char ch) {
        putchar(ch);
        return *this;
    }
    template <typename Int>
    Writer& operator << (const Int i) {
        baseFastio::write(i);
        return *this;
    }
} write;

// #define int int64

const int N = 5e4 + 7, M = 2e5 + 7, Q = 1e5 + 7;

int n, m, q;
struct Edge {
    int id, x, y, val;
    bool modified;
} e[M];
int WhereEdge[M], NewlyVal[M];
int op[Q], x[Q], y[Q];

#define AddEdge(u, v, w) e[i] = (Edge){i, u, v, w, false}

namespace Graph {
    bool EdgeSortCmp_val_inc(const Edge &a, const Edge &b) { return a.val < b.val; }
    bool EdgeSortCmp_val_dec(const Edge &a, const Edge &b) { return a.val > b.val; }

    void sortEdges() {
        sort(e + 1, e + m + 1, EdgeSortCmp_val_dec);
    }

    void init_WhereEdge() {
        for (int i = 1; i <= m; i++) WhereEdge[e[i].id] = i;
    }
}

namespace UFS {
    int op_stack[N][2], stack_top;
    int fa[N], siz[N];
    void init(int node_cnt) {
        stack_top = 0;
        for (int i = 1; i <= node_cnt; i++) {
            fa[i] = i;
            siz[i] = 1;
        }
    }
    int find(int u) {
        return u == fa[u] ? u : find(fa[u]);
    }
    void merge(int u, int v) {
        u = find(u); v = find(v);
        if (u == v) return;
        if (siz[u] < siz[v]) swap(u, v);
        op_stack[++stack_top][0] = u;
        op_stack[stack_top][1] = v;
        fa[v] = u;
        siz[u] += siz[v];
    }
    void rollback(int tim) {
        while (stack_top > tim) {
            int u = op_stack[stack_top][0], v = op_stack[stack_top][1];
            fa[v] = v;
            siz[u] -= siz[v];
            stack_top--;
        }
    }
}

namespace BlockQueries {
    int S;
    struct Operation {
        int id;
        int x, y;
        int answer;
        Operation() {}
        Operation(int ID, int X, int Y) : id(ID), x(X), y(Y) {}
    } modify[Q], query[Q];
    int cntm, cntq;

    bool OperationSortCmp_id_inc(const Operation &a, const Operation &b) { return a.id < b.id; }
    bool OperationSortCmp_id_dec(const Operation &a, const Operation &b) { return a.id > b.id; }
    bool OperationSortCmp_x_inc(const Operation &a, const Operation &b) { return a.x < b.x; }
    bool OperationSortCmp_x_dec(const Operation &a, const Operation &b) { return a.x > b.x; }
    bool OperationSortCmp_y_inc(const Operation &a, const Operation &b) { return a.y < b.y; }
    bool OperationSortCmp_y_dec(const Operation &a, const Operation &b) { return a.y > b.y; }

    void DealQuery(int l, int r) {
        // fprintf(stderr, "BlockQueries::DealQuery(%d, %d)\n", l, r);
        // init data
        cntm = cntq = 0;
        Graph::init_WhereEdge();
        UFS::init(n);
        // pretreat operations
        for (int i = l; i <= r; i++) 
            if (op[i] == 1) {
                modify[++cntm] = Operation(i, WhereEdge[x[i]], y[i]);
                e[WhereEdge[x[i]]].modified = true;
            } else
                query[++cntq] = Operation(i, x[i], y[i]);
        // sort operations
        sort(query + 1, query + cntq + 1, OperationSortCmp_y_dec);
        // deal operations
        for (int i = 1, e_cnt = 1, last_top; i <= cntq; i++) {
            while (e_cnt <= m && e[e_cnt].val >= query[i].y) {
                // if (!e[e_cnt].modified) printf("Merge Unmodified Edge {%d, %d}\n", e[e_cnt].x, e[e_cnt].y);
                if (!e[e_cnt].modified) UFS::merge(e[e_cnt].x, e[e_cnt].y);
                e_cnt++;
            }
            last_top = UFS::stack_top;
            for (int j = 1; j <= cntm; j++) NewlyVal[modify[j].x] = 0;
            for (int j = 1; j <= cntm; j++)
                if (modify[j].id < query[i].id)
                    NewlyVal[modify[j].x] = modify[j].y;
                else 
                    if (!NewlyVal[modify[j].x])
                        NewlyVal[modify[j].x] = e[modify[j].x].val;
            // for (int j = 1; j <= cntm; j++)
            //     printf("NewlyVal[%d] = %d\n", modify[j].x, NewlyVal[modify[j].x]);
            for (int j = 1; j <= cntm; j++)
                if (NewlyVal[modify[j].x] >= query[i].y) {
                    // printf("Merge Modified Edge {%d, %d} (NewlyVal = %d)\n", e[modify[j].x].x, e[modify[j].x].y, NewlyVal[modify[j].x]); 
                    UFS::merge(e[modify[j].x].x, e[modify[j].x].y);
                    NewlyVal[modify[j].x] = 0;
                }
            query[i].answer = UFS::siz[UFS::find(query[i].x)];
            // printf("ans[QueryId %d] = %d\n", query[i].id, query[i].answer);
            UFS::rollback(last_top);
        }
        // violent-change edge data
        for (int i = l; i <= r; i++) 
            if (op[i] == 1) {
                e[WhereEdge[x[i]]].val = y[i];
                e[WhereEdge[x[i]]].modified = false;
            }
        Graph::sortEdges();
        // sort operations for answer
        sort(query + 1, query + cntq + 1, OperationSortCmp_id_inc);
        for (int i = 1; i <= cntq; i++)
            write << query[i].answer << '\n';
    }
}

signed main()
{
    read >> n >> m;
    for (int i = 1, u, v, w; i <= m; i++) {
        read >> u >> v >> w;
        AddEdge(u, v, w);
    }
    Graph::sortEdges();
    read >> q;
    for (int i = 1; i <= q; i++)
        read >> op[i] >> x[i] >> y[i];
    BlockQueries::S = sqrt(q * log2(max(max(n, m), 2)));
    // printf("Block = %d\n", BlockQueries::S);
    for (int l = 1, r = BlockQueries::S; l <= q; l = r + 1, r = r + BlockQueries::S)
        BlockQueries::DealQuery(l, min(r, q));
    return 0;
}