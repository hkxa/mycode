/*************************************
 * @problem:      Common ancestor.
 * @author:       brealid.
 * @time:         2021-01-30.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 52, Bet = 26;

char s1[N], s2[N];
int l1, l2;
int n;
vector<pair<int, int> > to[Bet];
bool f1[N][N][Bet], f2[N][N][Bet];
int f[N][N];

void dp1(int n, char s[N], bool f[N][N][Bet]) {
    for (int i = 1; i <= n; ++i) f[i][i][s[i] -= 'a'] = true;
    for (int j = 2; j <= n; ++j)
        for (int i = j - 1; i >= 1; --i)
            for (int ch = 0; ch < 26; ++ch)
                for (int k = i; k < j && !f[i][j][ch]; ++k)
                    for (const pair<int, int> &u : to[ch])
                        if (f[i][k][u.first] && f[k + 1][j][u.second]) {
                            f[i][j][ch] = true;
                            break;
                        }
}

signed main() {
    kin >> (s1 + 1) >> (s2 + 1) >> n;
    l1 = strlen(s1 + 1);
    l2 = strlen(s2 + 1);
    char S_buffer[6];
    for (int i = 1; i <= n; ++i) {
        kin >> S_buffer;
        to[S_buffer[0] - 'a'].push_back(make_pair(S_buffer[3] - 'a', S_buffer[4] - 'a'));
    }
    dp1(l1, s1, f1);
    dp1(l2, s2, f2);
    memset(f, 0x3f, sizeof(f));
    f[0][0] = 0;
    for (int i = 1; i <= l1; ++i)
        for (int j = 1; j <= l2; ++j)
            for (int _i = 1; _i <= i; ++_i)
                for (int _j = 1; _j <= j; ++_j)
                    for (int ch = 0; ch < 26; ++ch)
                        if (f1[_i][i][ch] && f2[_j][j][ch])
                            f[i][j] = min(f[i][j], f[_i - 1][_j - 1] + 1);
    if (f[l1][l2] == 0x3f3f3f3f) kout << "-1\n";
    else kout << f[l1][l2] << '\n';
    return 0;
}