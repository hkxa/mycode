//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      嗅探器.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-18.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 998244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? Module(w - P) : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

const int N = 200000 + 3;

int n, m;
vector<int> G[N];
int dfn[N], low[N], dft;
int siz[N];
int S, T;
int ans;

void Exit_And_PrintAnswer() {
    if (ans ^ (n + 1)) write(ans, 10);
    else puts("No solution");
    exit(0);
}

void tarjan(int u, int fa = 0) {
    low[u] = dfn[u] = ++dft;
    int cnt = 0;
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i];
        if (v == fa) continue;
        if (!dfn[v]) {
            tarjan(v, u);
            low[u] = min(low[u], low[v]);
            if (low[v] >= dfn[u]) cnt++;
            if (cnt && u != S && dfn[T] >= dfn[v] && low[v] >= dfn[u]) ans = min(ans, u);
        } else {
            low[u] = min(low[u], dfn[v]);
        }
    }
}

signed main() {
    n = read<int>();
    ans = n + 1;
    // m = read<int>();
    for (int u = read<int>(), v = read<int>(); u && v; u = read<int>(), v = read<int>()) {
        G[u].push_back(v);
        G[v].push_back(u);
    }
    S = read<int>();
    T = read<int>();
    tarjan(S);
    // for (int i = 1; i <= n; i++) printf("dfn[%d] = %d, low[%d] = %d\n", i, dfn[i], i, low[i]);
    Exit_And_PrintAnswer();
    return 0;
}

// Create File Date : 2020-06-18