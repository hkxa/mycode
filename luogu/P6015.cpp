/*************************************
 * @contest:      【LGR-067】洛谷 1 月月赛 II & CSGRound 3.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-01-28.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, a[1000007], K;
int exist[1000007], exiCnt = 0;
#define pushAns(x) (exist[++exiCnt] = (x))

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) a[i] = read<int>();
    K = read<int>();
    a[n + 1] = K + 1;
    int i = 0, j = 0;
    int64 si = 0, sj = 0, maxX = 0;
    for (int x = 1; x <= K; x++) {
        // printf("x = %d\n", x);
        if (sj + a[j + 1] > x && si > sj) {
            pushAns(x);
            continue;
        }
        while (i <= n && si + a[i + 1] <= x) {
            si += a[++i], sj -= a[i];
            while (j <= n && sj + a[j + 1] <= x) sj += a[++j];
            if (si > sj) {
                pushAns(x);
                break;
            }
        }
        // while (i <= n && si + a[i + 1] <= x) si += a[++i], sj -= a[i], printf("i = %d, j = %d, Si = %lld, Sj = %lld.\n", i, j, si, sj);
        // while (j <= n && sj + a[j + 1] <= x) sj += a[++j], printf("i = %d, j = %d, Si = %lld, Sj = %lld.\n", i, j, si, sj);
        // if (si > sj) pushAns(x);
        // printf("x = %d : i = %d, j = %d, Si = %lld, Sj = %lld.\n", x, i, j, si, sj);
    }
    write(exiCnt, 10);
    for (int i = 1; i <= exiCnt; i++) write(exist[i], 32);
    putchar(10);
    return 0;
}