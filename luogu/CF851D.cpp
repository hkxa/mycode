//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Arpa and a list of numbers.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-08.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int eps = 1e-8;

namespace against_cpp11 {
    const int N = 5e5 + 7, VAL = 1e6 + 7;
    int n, a[N];
    int64 x, y, ans, now;
    bool is_not_prime[VAL];
    int primes[N], cnt;
    int used[N];
    struct prime_and_its_count {
        int prime;
        int cnt;
        // bool operator < (const prime_and_its_count &b) const {
        //     return cnt ^ b.cnt ? cnt > b.cnt : prime < b.prime;
        // }
        inline int64 guess() const {
            return (n - cnt) * min(x, y);
        }
        bool operator < (const prime_and_its_count &b) const {
            return guess() < b.guess();
        }
    } p[N];
    void prepare_primes_table(int num) {
        for (int i = 2; i < num; i++) {
            if (!is_not_prime[i]) {
                primes[++cnt] = i;
                p[cnt].prime = i;
                for (int j = i + i; j < num; j += i)
                    is_not_prime[j] = true;
            }
        }
    }    
    signed main() {
        prepare_primes_table(VAL);
        // printf("primes cnt = %d\n", cnt);
        read >> n >> x >> y;
        ans = y * n;
        // bool is_all_a_are_1 = 1;
        for (int i = 1, num, j; i <= n; i++) {
            read >> a[i]; 
            num = a[i];
            j = 1;
            while (num != 1 && primes[j] * primes[j] <= num) {
                if (num % primes[j] == 0) {
                    p[j].cnt++;
                    while (num % primes[j] == 0) num /= primes[j];
                }
                j++;
            }
            if (num != 1) p[lower_bound(primes + 1, primes + cnt + 1, num) - primes].cnt++;
            if (a[i] % 2 == 0) ans -= y;
            // if (is_all_a_are_1 && a[i] != 1) is_all_a_are_1 = false;
        }
        ans = min(ans, x * n);
        sort(p + 1, p + cnt + 1);
        for (int i = 1, prim; i <= cnt && p[i].guess() < ans; i++) {
            now = 0;
            int increase_min = prim = p[i].prime;
            bool all_using_option_x = !p[i].cnt;
            for (int i = 1; i <= n && now < ans; i++) {
                if (a[i] % prim) {
                    if (x < y * (prim - a[i] % prim)) now += x;
                    else {
                        all_using_option_x = false;
                        now += y * (prim - a[i] % prim);
                    }
                    increase_min = min(increase_min, prim - a[i] % prim);
                }
            }
            if (all_using_option_x) now += increase_min * y - x;
            if (now < ans) ans = now;
        }
        write << ans << '\n';
        return 0;
    }
}

signed main() { return against_cpp11::main(); }