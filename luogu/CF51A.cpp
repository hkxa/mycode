/*************************************
 * problem:      CF51A Cheaterius's Problem.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-04-18.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 
/*************************************
 * record ID:    R18334205.
 * time:         480 ms
 * memory:       4 KB
*************************************/ 


#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

struct card {
    int a[2][2];
    void readIn() 
    {
        int d1, d2;
        d1 = read<int>();
        d2 = read<int>();
        a[0][0] = (d1 / 10) - 1;
        a[0][1] = (d1 % 10) - 1; 
        a[1][0] = (d2 / 10) - 1;
        a[1][1] = (d2 % 10) - 1; 
    }

    void turn()
    {
        int t = a[0][1];
        a[0][1] = a[0][0];
        a[0][0] = a[1][0];
        a[1][0] = a[1][1];
        a[1][1] = t;
    }

    void split(int ori)
    {
        a[1][1] = ori % 6;
        ori /= 6;
        a[1][0] = ori % 6;
        ori /= 6;
        a[0][1] = ori % 6;
        ori /= 6;
        a[0][0] = ori;
    }

    operator int ()
    {
        return a[0][0] * 216 + a[0][1] * 36 + a[1][0] * 6 + a[1][1];
    }

    void beMin()
    {
        card t = *this;
        int a;
        a = t;
        t.turn();
        a = min(a, (int)t);
        t.turn();
        a = min(a, (int)t);
        t.turn();
        a = min(a, (int)t);
        t.turn();
        split(a);
    }
};

set <int> s;

bool match(card c)
{
    c.beMin();
    if (s.find(c) != s.end()) return true;
    s.insert(c);
    return false;
}

int n, ans = 0;
card t;

int main()
{
    n = read<int>();
    while (n--) {
        t.readIn();
        if (!match(t)) ans++;
    }
    write(ans);
    return 0;
}