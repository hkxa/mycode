/*************************************
 * @problem:      P4162 [SCOI2009]最长距离.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-01-12.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int MaxN = 30 + 3;

int n, m, t;
char mp[MaxN][MaxN];
int xx[4] = {0, 0, 1, -1}, yy[4] = {1, -1, 0, 0};
int dis[MaxN * MaxN];
int ans = 0;

#define id(x, y) ((x) * 31 + (y))
#define sqr_dis(i, j, x, y) (((i) - (x)) * ((i) - (x)) + ((j) - (y)) * ((j) - (y)))

void spfa(int s)
{
    queue<int> q;
    q.push(s);
    memset(dis, 0x3f, sizeof(dis));
    dis[s] = mp[s / 31][s % 31] & 1;
    int now, x, y, mx, my;
    while (!q.empty()) {
        now = q.front(); q.pop();
        x = now / 31; 
        y = now % 31;
        for (int o = 0; o < 4; o++) {
            mx = x + xx[o];
            my = y + yy[o];
            if (mx < 1 || mx > n || my < 1 || my > n) continue;
            if (dis[id(mx, my)] > dis[now] + (mp[mx][my] & 1)) {
                dis[id(mx, my)] = dis[now] + (mp[mx][my] & 1);
                q.push(id(mx, my));
            }
        }
    }
}

int main()
{
    n = read<int>();
    m = read<int>();
    t = read<int>();
    for (int i = 1; i <= n; i++) scanf("%s", mp[i] + 1);
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++) {
            spfa(id(i, j));
            for (int x = 1; x <= n; x++) {
                for (int y = 1; y <= m; y++) {
                    if (dis[id(x, y)] <= t && ans < sqr_dis(i, j, x, y)) {
                        ans = sqr_dis(i, j, x, y);
                    }
                }
            }
        }
    }
    printf("%.6lf", sqrt(ans));
    return 0;
}