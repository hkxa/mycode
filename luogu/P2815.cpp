/*************************************
 * @problem:      P2815 IPv6地址压缩.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020--.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

char strReadIn[40];
char c[8][5];
bool isAllZero[8];

int main()
{
    scanf("%s", strReadIn);
    int cnt = 0, mx = 0, mxPos = -1, nowPos = -1;
    for (int i = 0; i < 8; i++) {
        strncpy(c[i], strReadIn + i * 5, 4);
        isAllZero[i] = true;
        for (int j = 0; j < 4 && isAllZero[i]; j++)
            isAllZero[i] &= (c[i][j] == '0');
        if (isAllZero[i]) {
            cnt++;
            if (nowPos == -1) nowPos = i;
            if (cnt > mx) {
                mx = cnt;
                mxPos = nowPos;
            }
        } else {
            nowPos = -1;
            cnt = 0;
        }
    }
    // printf("mx = %d, mxPos = %d.\n", mx, mxPos);
    for (int i = 0; i < 8; i++) {
        if ((i != 0 && (i >= mxPos + mx || i <= mxPos)) ||
            (mxPos == 0 && i == 0) ||
            (mxPos + mx == 8 && i == 7)) putchar(':');
        if (i >= mxPos + mx || i < mxPos) {
            if (isAllZero[i]) putchar('0');
            else {
                char *out = c[i];
                while (*out == '0') out++;
                printf("%s", out);
            }
        }
    }
    putchar(10);
    return 0;
}