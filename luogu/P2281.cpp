/*************************************
 * @problem:      P2281 [HNOI2003]多项式的加法和乘法.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-01-06.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

/**
 * unfinished
 * 令人感觉很糟糕的题目
 * 数据没有给够，题目说得模凌两可，无法理解
 */

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define IsTrueChar(c) (isdigit(c) || isupper(c) || (c == '-' || c == '+' || c == '^'))

struct mo { // monomial
    long long k, n[26];
    mo() : k(1) { memset(n, 0, sizeof(n)); }
    mo operator * (mo other) {
        // printf("monomial [times] %s * %s.\n", toString().c_str(), other.toString().c_str());
        mo res;
        res.k = k * other.k;
        for (int i = 0; i < 26; i++) res.n[i] = n[i] + other.n[i];
        return res;
    }
    bool operator < (const mo other) const {
        for (int i = 0; i < 26; i++) {
            if (n[i] < other.n[i]) return true;
            if (n[i] > other.n[i]) return false;
        }
        return false;
    }
    bool isConst() const {
        for (int i = 0; i < 26; i++) {
            if (n[i]) return false;
        }
        return true;
    }
    bool similarTerm(const mo other) const {
        for (int i = 0; i < 26; i++) {
            if (n[i] != other.n[i]) return false;
        }
        return true;
    }
    string toString() {
        stringstream ret;
        if (isConst() || (k != 1 && k != -1)) ret << k;
        if (!isConst() && k == -1) ret << '-';
        for (int i = 0; i < 26; i++) {
            if (n[i]) {
                ret << (char)('A' + i);
                if (n[i] != 1) ret << '^' << n[i];
            }
        }
        return ret.str();
    }
};

struct line {
    char ch;
    bool ended;
    line() : ended(0) {}
    string get()
    {
        if (ended) {
            ended = false;
            return "";
        }
        string ret;
        ch = getchar();
        // printf("Read Ch %d.\n", ch);
        while (!IsTrueChar(ch)) {
            if (ch == '\n' || ch == '\r') return "";
            ch = getchar();
            // printf("Read Ch %d.\n", ch);
        }
        while (IsTrueChar(ch)) {
            ret += ch;
            ch = getchar();
            // printf("Read Ch %d.\n", ch);
        }
        if (ch == '\n' || ch == '\r') ended = true;
        return ret;
    }
} get;

long long stoi(string s)
{
    bool nag = false;
    if (s[0] == '-') nag = true;
    long long res = 0;
    for (unsigned i = nag; i < s.length(); i++) {
        res = res * 10 + s[i] - '0';
        // printf("see digit %c; res become %lld.\n", s[i], res);
    }
    // printf("stoi : receive %s, return %lld\n", s.c_str(), nag ? -res : res);
    return nag ? -res : res;
}

class multi { // multinomial
  private:
    int n;
    mo a[20007];
    void combine()
    {
        std::sort(a + 1, a + n + 1); 
        int i = 1, j = 0;
        while (i <= n) {
            if (j && a[i].similarTerm(a[j])) a[j].k += a[i].k;
            else a[++j] = a[i];
            i++;
        }
        n = j;
    }
  public:
    multi(bool start = 1) : n(1)
    {
        if (!start) return;
        string s;
        bool firstTag = 1, nagTag = 0;
        while ((s = get.get()) != "") {
            // printf("get block %s.\n", s.c_str());
            if (s == "+") { 
                if (!firstTag) {
                    if (nagTag) a[n].k = -a[n].k;
                    n++;
                }
                nagTag = false;
            } else if (s == "-") { 
                if (!firstTag) {
                    if (nagTag) a[n].k = -a[n].k;
                    n++;
                }
                nagTag = true;
            } else if (isdigit(s[0]) || s[0] == '-') {
                a[n].k = stoi(s);
            } else {
                int ch = s[0] - 'A';
                if (s.length() > 1) a[n].n[ch] += stoi(s.substr(2, s.length() - 2));
                else a[n].n[ch]++;
            }
            firstTag = 0;
        }
        if (nagTag) a[n].k = -a[n].k;
        // printf("&********************* ended *********************&\n");
    }

    multi operator + (multi x) 
    {
        multi ret = *this;
        for (int i = 1; i <= x.n; i++) {
            ret.a[n + i] = x.a[i];
        }
        ret.n = n + x.n;
        ret.combine();
        return ret;
    }

    multi operator * (multi x) 
    {
        multi ret(0);
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= x.n; j++) {
                ret.a[(i - 1) * x.n + j] = a[i] * x.a[j];
            }
        }
        ret.n = n * x.n;
        // printf("before combine : %s\n", ret.toString().c_str());
        ret.combine();
        // printf("after combine : %s\n", ret.toString().c_str());
        return ret;
    }

    string toString()
    {
        std::sort(a + 1, a + n + 1); 
        string ret;
        for (int i = 1; i < n; i++) {
            ret += a[i].toString();
            if (a[i + 1].k > 0) ret += '+';
        }
        return ret + a[n].toString();
    }
} a, b;

int main()
{
    // printf("a = %s; b = %s.\n", a.toString().c_str(), b.toString().c_str());
    cout << (a + b).toString() << endl;
    cout << (a * b).toString() << endl;
    return 0;
}