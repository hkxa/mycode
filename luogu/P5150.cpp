/*************************************
 * problem:      P5150 生日礼物.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-19.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define puts_return(x) { puts(x); return 0; }
#define write_return(x) { write(x); return 0; }

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int64 n, ans = 1;
int32 sqrtn;

int main()
{
    sqrtn = sqrt(n = read<int64>());
    for (int i = 2, cnt = 0; i <= sqrtn; i++, cnt = 0) {
        while (n % i == 0) {
            n /= i;
            cnt++;
        } 
        if (cnt) ans *= (cnt << 1 | 1);
    }
    if (n > 1) ans *= 3;
    write(ans);
    return 0;
}