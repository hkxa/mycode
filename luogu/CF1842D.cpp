/*************************************
 * @problem:      CF1842C.
 * @author:       brealid.
 * @time:         2023-06-24.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

// #define USE_FREAD  // 使用 fread  读入，去注释符号
// #define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 100 + 7;
int n, m;
bool used[N];
int64 mp[N][N], dis[N];

vector<pair<string, int> > ans;

signed main() {
    kin >> n >> m;
    for (int i = 1; i <= n; ++i)
        for (int j = 1; j <= n; ++j)
            if (i != j) mp[i][j] = 0x3f3f3f3f3f3f3f3f;
    for (int i = 1, u, v, w; i <= m; ++i) {
        kin >> u >> v >> w;
        mp[u][v] = mp[v][u] = w;
    }
    for (int i = 1; i <= n; ++i) dis[i] = mp[1][i];
    used[1] = true;
    int64 lastDis = 0;
    for (int cnt = 2; cnt <= n; ++cnt) {
        // kout << "cnt = " << cnt << '\n';
        // find dis min
        int64 min_dis = 0x3f3f3f3f3f3f3f3f;
        int min_pos = 0;
        for (int i = 1; i <= n; ++i) {
            if (!used[i] && dis[i] < min_dis) {
                min_dis = dis[i];
                min_pos = i;
            }
        }
        string ss;
        for (int i = 1; i <= n; ++i)
            if (used[i]) ss += '1';
            else ss += '0';
        ans.push_back(make_pair(ss, min_dis - lastDis));
        lastDis = min_dis;
        used[min_pos] = true;
        if (min_pos == n) break;
        for (int i = 1; i <= n; ++i) {
            if (!used[i] && dis[i] > min_dis + mp[min_pos][i]) {
                dis[i] = min_dis + mp[min_pos][i];
            }
        }
    }
    if (dis[n] == 0x3f3f3f3f3f3f3f3f) {
        kout << "inf";
        return 0;
    }
    kout << dis[n] << ' ' << ans.size() << '\n';
    for (int i = 0; i < ans.size(); ++i)
        kout << ans[i].first.c_str() << ' ' << ans[i].second << '\n';
    return 0;
}