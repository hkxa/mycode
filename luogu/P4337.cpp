/*************************************
 * @problem:      [ZJOI2018]线图.
 * @author:       brealid.
 * @time:         2020-11-22.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;
#if false /* 需要使用 fread 优化，改此参数为 true */
#define getchar() (i_P1 == i_P2 && (i_P2 = (i_P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), i_P1 == i_P2) ? EOF : *i_P1++)
char BUF[1 << 21], *i_P1 = BUF, *i_P2 = BUF;
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 5e3 + 7, P = 998244353;

int n, k;
vector<int> G[N];

namespace k_2 {
    int query() {
        int64 ans = 0;
        for (int i = 1; i <= n; ++i)
            ans += G[i].size() * (G[i].size() - 1);
        return (ans >> 1) % P;
    }
}

namespace k_3 {
    int query() {
        int64 ans = 0;
        for (int u = 1; u <= n; ++u) {
            ans += (int64)G[u].size() * (G[u].size() - 1) * (G[u].size() - 2);
            for (size_t i = 0; i < G[u].size(); ++i)
                ans += (G[u].size() - 1) * (G[G[u][i]].size() - 1);
        }
        return (ans >> 1) % P;
    }
}

signed main() {
    kin >> n >> k;
    for (int i = 1, u, v; i < n; ++i) {
        kin >> u >> v;
        G[u].push_back(v);
        G[v].push_back(u);
    }
    switch (k) {
        case 2: kout << k_2::query() << '\n'; break;
        case 3: kout << k_3::query() << '\n'; break;
    }
    return 0;
}