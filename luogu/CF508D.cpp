/*************************************
 * @problem:      Tanya and Password.
 * @author:       brealid.
 * @time:         2021-01-24.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

int id(char x) {
    if (x <= '9') return x - '0';           // must be integer;
    else if (x <= 'Z') return x - 'A' + 10; // must be upper-alpha;
    else return x - 'a' + 36;               // must be lower-alpha;
}

char chr(int d) {
    if (d < 10) return d + '0';             // must be integer;
    else if (d < 36) return d - 10 + 'A';   // must be upper-alpha;
    else return d - 36 + 'a';               // must be lower-alpha;
}

int id(char x, char y) {
    return id(x) * 62 + id(y);
}

const int N = 2e5 + 7, M = 62 * 62 + 7;

void report_NO() {
    kout << "NO\n";
    exit(0);
}

int n;
vector<int> G[M];
int ind[M], cur[M];
int visit[N], cnt = 0;

void euler(int u) {
    while (cur[u] < G[u].size()) {
        int v = G[u][cur[u]++];
        euler(v);
    }
    visit[++cnt] = u;
}

signed main() {
    kin >> n;
    char x, y, z;
    for (int i = 1, u, v; i <= n; ++i) {
        kin >> x >> y >> z;
        u = id(x, y), v = id(y, z);
        G[u].push_back(v);
        ++ind[v];
    }
    int st = -1, ed = -1;
    for (int i = 0; i < 62 * 62; ++i)
        if (G[i].size() == ind[i] + 1) {
            if (~st) report_NO();
            st = i;
        } else if (G[i].size() == ind[i] - 1) {
            if (~ed) report_NO();
            ed = i;
        } else if (G[i].size() != ind[i]) report_NO();
    if (!~st || !~ed)
        for (int i = 0; i < 62 * 62; ++i)
            if (!G[i].empty()) st = i;
    euler(st);
    if (cnt != n + 1) report_NO();
    kout << "YES\n" << chr(visit[cnt] / 62);
    for (int i = cnt; i >= 1; --i)
        kout << chr(visit[i] % 62);
    kout << '\n';
    return 0;
}