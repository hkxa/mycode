/*************************************
 * @problem:      P5281 [ZJOI2019]Minimax搜索.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-04-26.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 
#ifdef DEBUG
# define passing() cerr << "passing line [" << __LINE__ << "]." << endl
# define debug(...) fprintf(stderr, __VA_ARGS__)
# define show(x) cerr << #x << " = " << (x) << endl
#else
# define passing() do if (0) cerr << "passing line [" << __LINE__ << "]." << endl; while(0)
# define debug(...) do if (0) fprintf(stderr, __VA_ARGS__); while(0)
# define show(x) do if (0) cerr << #x << " = " << (x) << endl; while(0)
#endif
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read() {
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c) {
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x) {
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch) {
    write(x);
    putchar(nextch);
}
#define int int64

// Const Data
const int N = 200000 + 7, wxw = 998244353;

int kpow(int a, int n) {
    int ret = 1;
    while (n) {
        if (n & 1) ret = (int64)ret * a % wxw;
        a = (int64)a * a % wxw;
        n >>= 1;
    }
    return ret;
}
#define inv(num) kpow(num, wxw - 2)

// Base Data
int n, l, r;
// Tree Data
vector<int> G[N];
int fa[N], Pow2leafCnt[N], siz[N];
bool dep[N];
int val[N], dp[N];
// Tree Chain Splitting
int dfn[N], id[N], low[N], dfnCnt = 0;
#define belong(x, f) (dfn[x] >= dfn[f] && dfn[x] <= low[f])
bool type[N];
int wson[N], beg[N], end[N];
int root[N];
#define leaf(x) (!wson[x])
int modifyData[N][2], modifyCnt[N];
#define pushModifyData(pos, data) (modifyData[pos][modifyCnt[pos]++] = (data))

// Save Answer
int ans[N];

// Data Struct: y = kx + b (mod 998244353)
struct Data {
    int k, b;
    Data() : k(0), b(0) {}
    Data(int K, int B) : k(K), b(B) {}
    inline int val() { return (k + b) % wxw; }
    inline Data operator * (const Data &other) {
        return Data((int64)k * other.k % wxw, (int64)(k * other.b + b) % wxw);
    }
    inline Data operator *= (const Data &other) {
        b = (int64)(k * other.b + b) % wxw;
        k = (int64)k * other.k % wxw;
        return *this;
    }
} initial[N];

// Pre Solve : fa, dep, siz, leaf, wson
void dfs1(int u, int fat) {
    fa[u] = fat;
    dep[u] = !dep[fat];
    siz[u] = 1;
    Pow2leafCnt[u] = 1;
    if (G[u].size() == 1 && u != 1) {
        val[u] = u;
        Pow2leafCnt[u] = 2;
        return;
    }
    if (dep[u]) val[u] = 1;
    else val[u] = n;
    for (uint32 i = 0; i < G[u].size(); i++) {
        if (G[u][i] != fat) {
            dfs1(G[u][i], u);
            Pow2leafCnt[u] = (int64)Pow2leafCnt[u] * Pow2leafCnt[G[u][i]] % wxw;
            siz[u] += siz[G[u][i]];
            if (siz[G[u][i]] >= siz[wson[u]]) wson[u] = G[u][i];
            if (dep[u]) val[u] = max(val[u], val[G[u][i]]);
            else val[u] = min(val[u], val[G[u][i]]);
        }
    }
}

// Pre Solve : dfn, low, beg, end, initial
int dfs2(int u, int chainBeg) {
    if (val[u] != val[1]) {
        id[dfn[u] = ++dfnCnt] = u;
        if (val[fa[u]] != val[1]) beg[u] = chainBeg;
        else beg[u] = u;
    }
    // initial[u].b = Pow2leafCnt[u];
    if (wson[u]) end[u] = dfs2(wson[u], beg[u]);
    else return end[u] = u;
    for (uint32 i = 0; i < G[u].size(); i++) {
        if (G[u][i] != fa[u] && G[u][i] != wson[u]) {
            dfs2(G[u][i], G[u][i]);
        }
    }
    low[u] = dfnCnt;
    return end[u];
}

int dfs_NonAnswerChainSon(int u, int fat, int Root) {
    root[u] = Root;
    if (leaf(u)) {
        if (type[Root]) {
            if (dep[u]) dp[u] = 2 - (u <= val[1]);
            else dp[u] = u <= val[1];
            // debug("TypeRoot = 1, u = %d, val[1] - u = %d\n", u, val[1] - u);
            if (u <= val[1]) pushModifyData(val[1] - u, u);
        } else {
            if (dep[u]) dp[u] = u >= val[1];
            else dp[u] = 2 - (u >= val[1]);
            // debug("TypeRoot = 0, u = %d, u - val[1] = %d\n", u, u - val[1]);
            if (u >= val[1]) pushModifyData(u - val[1], u);
        }
        return initial[u].k = dp[u];
    }
    int Ret = wxw - 1;
    dp[u] = dfs_NonAnswerChainSon(wson[u], u, Root);
    for (uint32 i = 0; i < G[u].size(); i++) {
        if (G[u][i] != fat && G[u][i] != wson[u]) {
            Ret = (int64)Ret * dfs_NonAnswerChainSon(G[u][i], u, Root) % wxw;
        }
    }
    initial[u].k = Ret;
    initial[u].b = Pow2leafCnt[u];
    dp[u] = (Pow2leafCnt[u] + (int64)dp[u] * Ret) % wxw;
    return dp[u]; /* check */
}

int dfs_chain(int u) {
    int Ans = 1;
    for (uint32 i = 0; i < G[u].size(); i++) {
        if (G[u][i] == fa[u]) continue;
        else if (val[G[u][i]] == val[u]) Ans = (int64)Ans * dfs_chain(G[u][i]) % wxw;
        else {
            dfs2(G[u][i], G[u][i]);
            fa[G[u][i]] = 0;
            type[G[u][i]] = dep[u];
            Ans = (int64)Ans * dfs_NonAnswerChainSon(G[u][i], u, G[u][i]) % wxw;
        }
    }
    return Ans;
}

// Line Tree
namespace LineTree {
    Data t[N * 4];
    inline void pushUp(int u) {
        t[u] = t[u << 1] * t[u << 1 | 1];
    }

    void build(int u, int l, int r) {
        if (l == r) t[u] = initial[id[l]];
        else {
            int mid = (l + r) >> 1;
            build(u << 1, l, mid);
            build(u << 1 | 1, mid + 1, r);
            pushUp(u);
        }
    }

    void update(int u, int l, int r, int pos) {
        // show(u); show(l); show(r); show(pos);
        // if (l > pos || r < pos) {
        //     debug("the program should be throw to dustbin!");
        //     exit(1);
        // }
        if (l == pos && r == pos) t[u] = initial[id[pos]];
        else {
            int mid = (l + r) >> 1;
            if (pos <= mid) update(u << 1, l, mid, pos);
            else update(u << 1 | 1, mid + 1, r, pos);
            pushUp(u);
        }
    }

    Data query(int u, int l, int r, int ml, int mr) {
        if (l > mr || r < ml) return Data(1, 0);
        if (l >= ml && r <= mr) return t[u];
        int mid = (l + r) >> 1;
        return query(u << 1, l, mid, ml, mr) * query(u << 1 | 1, mid + 1, r, ml, mr);
    }
}

int Ans = 1;
void modify(int u) {
    using namespace LineTree;
    Data x;
    int Root = root[u];
    // debug("Root = %d\n", Root);
    // debug("Ans(%d) /= %d\n", Ans, dp[Root]);
    Ans = (int64)Ans * inv(dp[Root]) % wxw; // 去除影响
    initial[u].k = ((type[Root] ^ dep[u]) ? 2 : 0);
    // debug("initial[u] = {%d, %d}\n", initial[u].k, initial[u].b);
    // passing();
    // show(dfnCnt);
    // show(dfn[u]);
    update(1, 1, dfnCnt, dfn[u]);
    // for (int i = 1; i <= n; i++) debug("[%d, %d]%c", t[i].k, t[i].b, " \n"[i == n]);
    // passing();
    while (fa[beg[u]]) {
        // debug("enter u %d\n", u);
        const int &f = fa[beg[u]];
        initial[f].k = (int64)initial[f].k * inv(dp[beg[u]]) % wxw; // 去除影响
        // debug("initial[f] = {%d, %d}\n", initial[f].k, initial[f].b);
        x = query(1, 1, dfnCnt, dfn[beg[u]], dfn[end[u]]); 
        dp[beg[u]] = x.val();
        initial[f].k = (int64)initial[f].k * dp[beg[u]] % wxw; // 重新加入影响
        update(1, 1, dfnCnt, dfn[f]);
        // for (int i = 1; i <= n; i++) debug("[%d, %d]%c", t[i].k, t[i].b, " \n"[i == n]);
        u = f;
    }
    x = query(1, 1, dfnCnt, dfn[beg[Root]], dfn[end[Root]]);
    // debug("x = {%d, %d}\n", x.k, x.b);
    dp[Root] = x.val();
    // debug("Ans(%d) *= %d\n", Ans, dp[Root]);
    Ans = (int64)Ans * dp[Root] % wxw; // 重新加入影响
}

signed main()
{
#ifdef _WIN32
    // freopen(".in.txt", "r", stdin);
#endif
    n = read<int>();
    l = read<int>();
    r = read<int>();
    for (int i = 1, u, v; i < n; i++) {
        u = read<int>();
        v = read<int>();
        G[u].push_back(v);
        G[v].push_back(u);
    }
    dfs1(1, 0);
    // passing();
    Ans = dfs_chain(1);
    // passing();
    LineTree::build(1, 1, dfnCnt);
    // for (int i = 1; i <= n; i++) debug("%d%c", val[i], " \n"[i == n]);
    // for (int i = 1; i <= n; i++) debug("%d%c", dfn[i], " \n"[i == n]);
    // for (int i = 1; i <= n; i++) debug("%d%c", beg[i], " \n"[i == n]);
    // for (int i = 1; i <= n; i++) debug("%d%c", fa[i], " \n"[i == n]);
    // for (int i = 1; i <= n; i++) debug("[%d, %d]%c", initial[i].k, initial[i].b, " \n"[i == n]);
    // for (int i = 1; i <= n; i++) debug("[%d, %d]%c", LineTree::t[i].k, LineTree::t[i].b, " \n"[i == n]);
    // passing();
    ans[n] = Pow2leafCnt[1] - 1;
    for (int k = n - 1; k >= 1; k--) {
        // show(k);
        // show(modifyCnt[k]);
        for (int i = 0; i < modifyCnt[k]; i++) {
            // debug("After modify %d : \n", modifyData[k][i]);
            modify(modifyData[k][i]);
            // debug("Ans = %d\n", Ans);
        }
        ans[k] = (Pow2leafCnt[1] - Ans + wxw) % wxw;
    }
    for (int k = l; k <= r; k++) 
        write((ans[k] - ans[k - 1] + wxw) % wxw, 32);
    putchar(10);
    // debug("dfnCnt = %d\n", dfnCnt);
    return 0;
}