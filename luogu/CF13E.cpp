//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Holes.
 * @user_id:      63720.
 * @user_name:    brealid.
 * @time:         2020-05-27.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;
// #define int int64

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int N = 2e5 + 7;
struct lct {
    int ch[N][2], fa[N], siz[N], tagrev[N], Max[N], val[N];
    bool isroot(int x) {
        return ch[fa[x]][0] != x && ch[fa[x]][1] != x;
    }
    void rev(int x) {
        tagrev[x] ^= 1;
        swap(ch[x][0], ch[x][1]);
    }
    void pushdown(int x) {
        if (!isroot(x)) pushdown(fa[x]);
        if (!x || !tagrev[x]) return;
        tagrev[x] = 0;
        rev(ch[x][0]);
        rev(ch[x][1]);
    }
    void update(int x) {
        if (!x) return;
        siz[x] = siz[ch[x][0]] + siz[ch[x][1]] + 1;
        Max[x] = max(val[x], max(Max[ch[x][0]], Max[ch[x][1]])); 
    }
    void rotate(int x) {
        int y = fa[x], z = fa[y], opt = (ch[y][1] == x), w = ch[x][opt ^ 1];
        if (!isroot(y)) ch[z][ch[z][1] == y] = x;
        fa[x] = z;
        ch[y][opt] = w;
        fa[w] = y;
        ch[x][opt ^ 1] = y;
        fa[y] = x;
        update(y);
        update(x);
    }
    void splay(int x) {
        int y, z;
        pushdown(x);
        while (!isroot(x)) {
            y = fa[x];
            z = fa[y];
            if (!isroot(y)) rotate(((ch[y][0] == x) ^ (ch[z][0] == y)) ? x : y);
            rotate(x);
        }
    }
    void access(int x) {
        for (int y = 0; x; y = x, x = fa[x]) splay(x), ch[x][1] = y, update(x);
    }
    void mktrot(int x) {
        access(x);
        splay(x);
        rev(x);
    }
    void mkvrot(int x) {
        access(x);
        splay(x);
    }
    pair<int, int> query(int x, int y) {
        mktrot(x);
        mkvrot(y);
        return make_pair(Max[y], siz[y] - 1);
    }
    void link(int x, int y) {
        mktrot(x);
        mkvrot(y);
        fa[x] = y;
    }
    void cut(int x, int y) {
        mktrot(x);
        mkvrot(y);
        ch[y][0] = fa[x] = 0;
        update(y);
    }
} s;

int n, m, a[N], opt, x, y;

int main() {
    n = read<int>();
    m = read<int>();
    for (int i = 1; i <= n; i++)
        s.siz[i] = 1, s.Max[i] = s.val[i] = i; 
    for (int i = 1; i <= n; i++) {
        a[i] = read<int>();
        if (i + a[i] <= n) s.link(i, i + a[i]);
        else s.link(i, n + 1);
    }
    for (int i = 1; i <= m; i++) {
        opt = read<int>();
        if (opt == 1) {
            x = read<int>();
            pair<int, int> ret = s.query(x, n + 1);
            write(ret.first, 32);
            write(ret.second, 10);
            continue;
        } else {
            x = read<int>();
            y = read<int>();
            if (x + a[x] <= n) {
                s.cut(x, x + a[x]);
                if (x + y <= n) s.link(x, x + y);
                else s.link(x, n + 1);
            } else {
                s.cut(x, n + 1);
                if (x + y <= n) s.link(x, x + y);
                else s.link(x, n + 1);
            }
            a[x] = y;
        }
    }
    return 0;
}