/*************************************
 * problem:      P3865 【模板】ST表.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-04.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, lgMax, queryCnt, l, r, lenLg;
int a[100000 + 7];
int mx[100000 + 7][30];
int lg2[100000 + 7];

void getLog2(int dataSize)
{
    lg2[0] = -1;
    for (int i = 1; i <= dataSize; i++) lg2[i] = lg2[i >> 1] + 1;
    lgMax = lg2[dataSize];
}

int main()
{
    n = read<int>();
    getLog2(n);
    queryCnt = read<int>();
    for (int i = 1; i <= n; i++) {
        mx[i][0] = a[i] = read<int>();
        // printf("max[%d][%d] = %d.\n", i, 0, mx[i][0]);
    }
    for (int i = 1; i <= lgMax; i++) {
        for (int j = 1; j + (1 << i) - 1 <= n; j++) {
            mx[j][i] = max(mx[j][i - 1],  mx[j + (1 << (i - 1))][i - 1]);
            // printf("max[%d][%d] = %d.\n", j, i, mx[j][i]);
        }
    }
    while (queryCnt--) {
        l = read<int>();
        r = read<int>();
        lenLg = lg2[r - l + 1];
        // printf("query max(mx[%d][%d], mx[%d][%d]).\n", l, lenLg, r - (1 << lenLg) + 1, lenLg);
        write(max(mx[l][lenLg], mx[r - (1 << lenLg) + 1][lenLg]), 10);
    }
    return 0;
}