/*************************************
 * @problem:      Coloring Brackets.
 * @author:       brealid.
 * @time:         2021-01-29.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;
#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 700 + 3, P = 1e9 + 7;

int n;
char s[N];
/**
 * 0: Not-colored
 * 1: OneSide-colored
 * 2: Both-colored
 */
int64 f[N][N][3], ok[N][N];

signed main() {
    kin >> (s + 1);
    n = strlen(s + 1);
    for (int i = 1; i <= n; ++i) f[i][i - 1][0] = ok[i][i - 1] = 1;
    for (int j = 2; j <= n; ++j) {
        for (int i = j - 1; i >= 1; --i) {
            int64 *now = f[i][j];
            if (s[i] == '(' && s[j] == ')' && ok[i + 1][j - 1]) {
                ok[i][j] = true;
                int64 *last = f[i + 1][j - 1];
                now[1] = (last[0] + last[1] * 3 + last[2] * 2) % P;
            }
            for (int sep = i; sep < j && !ok[i][j]; ++sep)
                if (ok[i][sep] && ok[sep + 1][j]) {
                    ok[i][j] = true;
                    int64 *l = f[i][sep], *r = f[sep + 1][j];
                    now[0] = (l[0] * (r[0] + r[1] * 2) + l[1] * (r[0] * 2 + r[1] * 2)) % P;
                    now[1] = (l[1] * (r[0] + r[1] * 2) + l[2] * (r[0] * 2 + r[1] * 2)) % P;
                    now[2] = (l[1] * (r[1] + r[2] * 2) + l[2] * (r[1] * 2 + r[2] * 2)) % P;
                }
            // printf("f[%d][%d] = {%lld, %lld, %lld}\n", i, j, now[0], now[1], now[2]);
        }
    }
    int64 ans = (f[1][n][0] + f[1][n][1] * 2 + f[1][n][1] * 2 + f[1][n][2] * 4) % P;
    kout << ans << '\n';
    return 0;
}