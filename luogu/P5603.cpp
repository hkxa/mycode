#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, m;
int ind_static[500007];
int ind[500007];
vector<int> G[500007];
priority_queue<int, vector<int>, greater<int> > q;
set<int> s;
set<int>::iterator it;
int mx, cnt;

int main()
{
    n = read<int>();
    m = read<int>();
    for (int i = 1, u, v; i <= m; i++) {
        u = read<int>();
        v = read<int>();
        G[u].push_back(v);
        ind_static[v]++;
    }
    memcpy(ind, ind_static, sizeof(ind_static));
    for (int i = 1; i <= n; i++) if (!ind[i]) q.push(i);
    mx = cnt = 0;
    while (!q.empty()) {
        int t = q.top();
        if (t > mx) {
            mx = t;
            cnt++;
        }
        q.pop();
        for (unsigned i = 0; i < G[t].size(); i++) {
            ind[G[t][i]]--;
            if (!ind[G[t][i]]) {
                q.push(G[t][i]);
            }
        }
    }
    write(cnt, 10);
    // ↑ques1 ↓ques2
    memcpy(ind, ind_static, sizeof(ind_static));
    for (int i = 1; i <= n; i++) if (!ind[i]) s.insert(i);
    mx = cnt = 0;
    int t;
    while (!s.empty()) {
        while (*s.begin() < mx && !s.empty()) {
            int t = *s.begin();
            // printf("del %d\n", t);
            s.erase(s.begin());
            for (unsigned i = 0; i < G[t].size(); i++) {
                ind[G[t][i]]--;
                if (!ind[G[t][i]]) s.insert(G[t][i]);
            }
        }
        if (s.empty()) break;
        it = s.end();
        it--;
        t = *it;
        // printf("get %d\n", t);
        s.erase(it);
        mx = t;
        cnt++;
        for (unsigned i = 0; i < G[t].size(); i++) {
            ind[G[t][i]]--;
            if (!ind[G[t][i]]) s.insert(G[t][i]);
        }
    }
    write(cnt, 10);
    return 0;
}