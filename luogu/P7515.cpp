#include <bits/stdc++.h>
using namespace std;
#define max3(a, b, c) max(max(a, b), c)
#define max4(a, b, c, d) max(max(a, b), max(c, d))
#define min3(a, b, c) min(min(a, b), c)
#define min4(a, b, c, d) min(min(a, b), min(c, d))
bool zy_MemoryTestFlag_1;

const int N = 300 + 7;

int T, n, m, b[N][N];

void lxn_AK_IOI() {
    cout << "NO\n";
}

void n2m2() {
    cout << "YES\n";
    for (int i = 1, cnt = 4; i <= 2; ++i) {
        for (int j = 1; j <= 2; ++j) {
            int now = max(b[1][1] - 1000000 * --cnt, 0);
            cout << now << ' ';
            b[1][1] -= now;
        }
        cout << '\n';
    }
}

void n3m2() {
    int up_min = max(b[1][1] - 2000000, 0), up_max = min(b[1][1], 2000000);
    int dn_min = max(b[2][1] - 2000000, 0), dn_max = min(b[2][1], 2000000);
    if (up_min > dn_max || dn_min > up_max) lxn_AK_IOI();
    else {
        cout << "YES\n";
        int mid = max(up_min, dn_min);
        cout << max(b[1][1] - mid - 1000000, 0) << ' ' << min(b[1][1] - mid, 1000000) << '\n';
        cout << max(mid - 1000000, 0) << ' ' << min(mid, 1000000) << '\n';
        cout << max(b[2][1] - mid - 1000000, 0) << ' ' << min(b[2][1] - mid, 1000000) << '\n';
    }
}

void n2m3() {
    int up_min = max(b[1][1] - 2000000, 0), up_max = min(b[1][1], 2000000);
    int dn_min = max(b[1][2] - 2000000, 0), dn_max = min(b[1][2], 2000000);
    if (up_min > dn_max || dn_min > up_max) lxn_AK_IOI();
    else {
        cout << "YES\n";
        int mid = max(up_min, dn_min);
        cout << max(b[1][1] - mid - 1000000, 0) << ' ' << max(mid - 1000000, 0) << ' ' << max(b[1][2] - mid - 1000000, 0) << '\n';
        cout << min(b[1][1] - mid, 1000000) << ' ' << min(mid, 1000000) << ' ' << min(b[1][2] - mid, 1000000) << '\n';
    }
}

const int Unlimited = 1e7;

namespace ns_n3m3 {
    int a[4][4] = {0};
}
void n3m3() {
    using namespace ns_n3m3;
    // for (int i = 1; i < n; ++i)
    //     for (int j = 1; j < m; ++j) {
    //         ;
    //     }
    ;;
    int ed = min(min4(b[1][1], b[1][2], b[2][1], b[2][2]), 1000000);
    for (a[2][2] = max(max4(b[1][1], b[1][2], b[2][1], b[2][2]) - 3000000, 0); a[2][2] <= ed; ++a[2][2]) {
        // WAY1
        a[1][1] = b[1][1] - a[2][2], a[1][3] = b[1][2] - a[2][2], a[3][1] = b[2][1] - a[2][2], a[3][3] = b[2][2] - a[2][2];
        a[1][2] = a[2][1] = a[2][3] = a[3][2] = 0;
        {
            int adjust = (a[1][2] * 2 - a[1][1] + a[1][3]) >> 2;
            a[1][2] -= adjust;
            a[1][1] += adjust, a[1][3] += adjust;
        }
        {
            int adjust = (a[3][2] * 2 - a[3][1] + a[3][3]) >> 2;
            a[3][2] -= adjust;
            a[3][1] += adjust, a[3][3] += adjust;
        }
        {
            int adjust = (a[2][1] * 2 - a[1][1] + a[3][1]) >> 2;
            a[2][1] -= adjust;
            a[1][1] += adjust, a[3][1] += adjust;
        }
        {
            int adjust = (a[2][3] * 2 - a[1][3] + a[3][3]) >> 2;
            a[2][3] -= adjust;
            a[1][3] += adjust, a[3][3] += adjust;
        }
        if (max(a[1][1], a[1][3]) > 1000000) {
            int t = min4(max(a[1][1], a[1][3]) - 1000000, Unlimited, a[1][1], a[1][3]); // , 1000000 - a[1][2]
            a[1][1] -= t, a[1][3] -= t, a[1][2] += t;
        }
        if (max(a[1][1], a[3][1]) > 1000000) {
            int t = min4(max(a[1][1], a[3][1]) - 1000000, Unlimited, a[1][1], a[3][1]);
            a[1][1] -= t, a[3][1] -= t, a[2][1] += t;
        }
        if (max(a[1][3], a[3][3]) > 1000000) {
            int t = min4(max(a[1][3], a[3][3]) - 1000000, Unlimited, a[1][3], a[3][3]);
            a[1][3] -= t, a[3][3] -= t, a[2][3] += t;
        }
        if (max(a[3][1], a[3][3]) > 1000000) {
            int t = min4(max(a[3][1], a[3][3]) - 1000000, Unlimited, a[3][1], a[3][3]);
            a[3][1] -= t, a[3][3] -= t, a[3][2] += t;
        }
        // if (a[1][2] > 1000000 && max(a[1][1], a[1][3]) < 1000000) {
        //     int t = min(a[1][2] - 1000000, Unlimited, a[3][1], a[3][3]);
        // }
        if (max(a[1][2], a[3][2]) > 1000000) {
            int t = min(max(a[1][2], a[3][2]) - 1000000, 1000000 - max(a[2][1], a[2][3]));
            a[1][2] -= t, a[3][2] -= t, a[2][1] += t, a[2][3] += t;
        }
        if (max(a[2][1], a[2][3]) > 1000000) {
            int t = min(max(a[2][1], a[2][3]) - 1000000, 1000000 - max(a[1][2], a[3][2]));
            a[2][1] -= t, a[2][3] -= t, a[1][2] += t, a[3][2] += t;
        }
        if ((*max_element((int*)a, (int*)a + 16) <= 1000000) && (*min_element((int*)a, (int*)a + 16) >= 0)) {
            cout << "YES\n";
            for (int i = 1; i <= n; ++i) {
                for (int j = 1; j <= m; ++j)
                    cout << a[i][j] << ' ';
                cout << '\n';
            }
            return;
        }
        // WAY2
        a[1][1] = b[1][1] - a[2][2], a[1][3] = b[1][2] - a[2][2], a[3][1] = b[2][1] - a[2][2], a[3][3] = b[2][2] - a[2][2];
        a[1][2] = a[2][1] = a[2][3] = a[3][2] = 0;
        if (max(a[1][1], a[1][3]) > 1000000) {
            int t = min4(max(a[1][1], a[1][3]) - 1000000, Unlimited, a[1][1], a[1][3]); // , 1000000 - a[1][2]
            a[1][1] -= t, a[1][3] -= t, a[1][2] += t;
        }
        if (max(a[1][1], a[3][1]) > 1000000) {
            int t = min4(max(a[1][1], a[3][1]) - 1000000, Unlimited, a[1][1], a[3][1]);
            a[1][1] -= t, a[3][1] -= t, a[2][1] += t;
        }
        if (max(a[1][3], a[3][3]) > 1000000) {
            int t = min4(max(a[1][3], a[3][3]) - 1000000, Unlimited, a[1][3], a[3][3]);
            a[1][3] -= t, a[3][3] -= t, a[2][3] += t;
        }
        if (max(a[3][1], a[3][3]) > 1000000) {
            int t = min4(max(a[3][1], a[3][3]) - 1000000, Unlimited, a[3][1], a[3][3]);
            a[3][1] -= t, a[3][3] -= t, a[3][2] += t;
        }
        // if (a[1][2] > 1000000 && max(a[1][1], a[1][3]) < 1000000) {
        //     int t = min(a[1][2] - 1000000, Unlimited, a[3][1], a[3][3]);
        // }
        if (max(a[1][2], a[3][2]) > 1000000) {
            int t = min(max(a[1][2], a[3][2]) - 1000000, 1000000 - max(a[2][1], a[2][3]));
            a[1][2] -= t, a[3][2] -= t, a[2][1] += t, a[2][3] += t;
        }
        if (max(a[2][1], a[2][3]) > 1000000) {
            int t = min(max(a[2][1], a[2][3]) - 1000000, 1000000 - max(a[1][2], a[3][2]));
            a[2][1] -= t, a[2][3] -= t, a[1][2] += t, a[3][2] += t;
        }
        if ((*max_element((int*)a, (int*)a + 16) <= 1000000) && (*min_element((int*)a, (int*)a + 16) >= 0)) {
            cout << "YES\n";
            for (int i = 1; i <= n; ++i) {
                for (int j = 1; j <= m; ++j)
                    cout << a[i][j] << ' ';
                cout << '\n';
            }
            return;
        }
#ifdef zy_debug
        if (a[2][2] == 1000000) {
            for (int i = 1; i <= n; ++i) {
                for (int j = 1; j <= m; ++j)
                    cout << a[i][j] << ' ';
                cout << '\n';
            }
        }
#endif
    }
    lxn_AK_IOI();
}

void nm3() {
    if (n == 3 && m == 3) n3m3();
    else if (n == 2 && m == 2) n2m2();
    else if (n == 3 && m == 2) n3m2();
    else if (n == 2 && m == 3) n2m3();
    else lxn_AK_IOI();
}

struct For_m2_VALUES {
    bool type;
    int v;
    // type(0): value = {start} + v
    // type(1): value = -{start} + v
    For_m2_VALUES(bool TYP, int VL) : type(TYP), v(VL) {}
    friend For_m2_VALUES operator - (int v, For_m2_VALUES g) {
        return For_m2_VALUES(!g.type, v - g.v);
    }
};

void m2() {
    int l = 0, r = 2000000;
    For_m2_VALUES cur(0, 0);
    for (int i = 1; i < n; ++i) {
        For_m2_VALUES vi = b[i][1] - cur;
        if (vi.type == 0) l = max(l, -vi.v), r = min(r, 2000000 - vi.v);
        else l = max(l, vi.v - 2000000), r = min(r, vi.v);
        cur = vi;
    }
    // fprintf(stderr, "[%d, %d]\n", l, r);
    if (l > r) {
        lxn_AK_IOI();
        return;
    }
    int previ = l;
    cout << "YES\n" << max(previ - 1000000, 0) << ' ' << min(previ, 1000000) << '\n';
    for (int i = 1; i < n; ++i) {
        int current = b[i][1] - previ;
        cout << max(current - 1000000, 0) << ' ' << min(current, 1000000) << '\n';
        previ = current;
    }
}

bool zy_MemoryTestFlag_2;
signed main() {
#ifndef zy_debug
    // freopen("matrix.in", "r", stdin);
    // freopen("matrix.out", "w", stdout);
#endif
    ios::sync_with_stdio(false);
    for (cin >> T; T--;) {
        cin >> n >> m;
        for (int i = 1; i < n; ++i)
            for (int j = 1; j < m; ++j)
                cin >> b[i][j];
        if (n <= 3 && m <= 3) nm3();
        else if (m == 2) m2();
        else lxn_AK_IOI();
    }
    cout << flush;
#ifdef zy_debug
    cerr << "Time Used: " << clock() << endl;
    cerr << "Memory Used: " << (&zy_MemoryTestFlag_2 - &zy_MemoryTestFlag_1) / 1048576.0 << endl;
#endif
    return 0;
}