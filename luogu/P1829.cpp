/*************************************
 * @problem:      [国家集训队]Crash的数字表格 / JZPTAB.
 * @user_name:    brealid.
 * @time:         2020-11-13.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

void File_IO_init(string file_name) {
    freopen((file_name + ".in" ).c_str(), "r", stdin);
    freopen((file_name + ".out").c_str(), "w", stdout);
}

const int N = 1e7 + 7, P = 20101009, inv6 = 16750841;

bool IsntPrime[N];
int primes[N], pcnt;
int mu[N];
int64 sum[N];

void init(int maxx) {
    sum[1] = mu[1] = 1;
    for (int i = 2; i <= maxx; ++i) {
        if (!IsntPrime[i]) primes[++pcnt] = i, mu[i] = -1;
        for (int p = 1; p <= pcnt && i * primes[p] <= maxx; ++p) {
            IsntPrime[i * primes[p]] = true;
            if (i % primes[p] == 0) break;
            mu[i * primes[p]] = -mu[i];
        }
        sum[i] = (sum[i - 1] + (int64)i * i * mu[i]) % P;
    }
}

inline int64 sum_i1(int64 n) { return (n * (n + 1) >> 1) % P; }

#define query_sum(l, r) (sum[r] - sum[(l) - 1])

inline int64 solve(int n, int m) {
    int64 ans = 0;
    for (int l = 1, r; l <= min(n, m); l = r + 1) {
        r = min(n / (n / l), m / (m / l));
        ans += query_sum(l, r) * sum_i1(n / l) % P * sum_i1(m / l) % P;
    }
    return ans % P;
}

signed main() {
    int n, m;
    read >> n >> m;
    init(min(n, m));
    int64 ans = 0;
    for (int l = 1, r; l <= min(n, m); l = r + 1) {
        r = min(n / (n / l), m / (m / l));
        ans += ((int64)(l + r) * (r - l + 1) >> 1) * solve(n / l, m / l) % P;
    }
    write << (ans % P + P) % P << '\n';
    return 0;
}