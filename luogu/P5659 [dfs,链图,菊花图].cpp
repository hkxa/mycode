#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read() {
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    Int n = c & 15;
    while (isdigit(c = getchar())) n = (((n << 2) + n) << 1) + (c & 15);
    return n;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;
int head[4007], to[4007], nxt[4007], edges_cnt = 0;
int num[2007], where[2007], tmp[2007];
int ans[2007];
int Degree[2007];
bitset<2048> vis, empty;

int UFS_fa[4007];
#define UFS_init(n) { for (int i = 1; i <= (n); i++) UFS_fa[i] = i; }
int find(int x) { return UFS_fa[x] == x ? x : UFS_fa[x] = find(UFS_fa[x]); }
#define UFS_merge(x, y) ((UFS_fa[find(x)] = find(y)), void())

void add(int u, int v)
{
    to[edges_cnt] = v;
    nxt[edges_cnt] = head[u];
    head[u] = edges_cnt;
    ++edges_cnt;
    to[edges_cnt] = u;
    nxt[edges_cnt] = head[v];
    head[v] = edges_cnt;
    ++edges_cnt;
}

// ↓↓↓ 暴力/骗分 part ↓↓↓

void updAns()
{
    for (int i = 1; i <= n; i++) tmp[num[i]] = i;
    for (int i = 1; i <= n; i++) {
        if (tmp[i] < ans[i]) break;
        if (tmp[i] > ans[i]) return;
    }
    for (int i = 1; i <= n; i++) ans[i] = tmp[i];
}

void dfs(int u)
{
    if (u == n) {
        updAns();
        return;
    }
    for (int i = 0; i < n - 1; i++) {
        if (!vis.test(i)) {
            vis.set(i);
            swap(num[to[i * 2]], num[to[i * 2 + 1]]);
            dfs(u + 1);
            swap(num[to[i * 2]], num[to[i * 2 + 1]]);
            vis.reset(i);
        }
    }
}

int baoli()
{
    vis &= empty;
    dfs(1);
    for (int i = 1; i <= n; i++) printf("%d ", ans[i]);
    putchar(10);
    return 0;
}

int JuHuaTu(int u)
{
    vis &= empty;
    UFS_init(n);
    for (int i = 1; i < n; i++) {
        for (int j = 1; j <= n; j++) {
            if (!vis.test(j) && find(tmp[i]) != find(j)) {
                UFS_merge(tmp[i], j);
                vis.set(j);
                write(j, 32);
                break;
            }
        }
    }
    for (int i = 1; i <= n; i++) {
        if (!vis.test(i)) {
            write(i, 10);
            break;
        }
    }
    return 0;
}

int lis[2048];
int mark[2048]; // edge<lis_i, lis_{i + 1}>  | 1:del_L->del_R | 2:del_R->del_L | 0:unmarked |

bool chkL(int x, int i) // 往左走 ←
{
    if (mark[where[x]] == 2 || mark[where[i]] == 2) return false;
    for (int j = where[x] - 1; j > where[i]; j--) if (mark[j] == 1) return false;
    return true;
}

void markL(int x, int i) // 往左走 ←
{
    mark[where[x]] = mark[where[i]] = 1;
    mark[1] = mark[n] = 0; // clear dirty data
    for (int j = where[x] - 1; j > where[i]; j--) mark[j] = 2;
}

bool chkR(int x, int i) // 往右走 →
{
    if (mark[where[x]] == 1 || mark[where[i]] == 1) return false;
    for (int j = where[x] + 1; j < where[i]; j++) if (mark[j] == 2) return false;
    return true;
}

void markR(int x, int i) // 往右走 →
{
    mark[where[x]] = mark[where[i]] = 2;
    mark[1] = mark[n] = 0; // clear dirty data
    for (int j = where[x] + 1; j < where[i]; j++) mark[j] = 1;
}

int lianTu(int u)
{
    memset(mark, 0, sizeof(mark));
    vis &= empty;
    for (int i = 1; i <= n; i++) {
        lis[i] = u;
        where[u] = i;
        for (int j = head[u]; ~j; j = nxt[j]) {
            if (to[j] != lis[i - 1]) {
                u = to[j];
                break;
            }
        }
    }
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= n; j++) {
            if (!vis.test(j) && where[tmp[i]] != where[j]) {
                if (where[tmp[i]] < where[j]) {
                    if (chkR(tmp[i], j)) {
                        markR(tmp[i], j);
                        ans[i] = j;
                        vis.set(j); 
                        break;
                    }
                } else {
                    if (chkL(tmp[i], j)) {
                        markL(tmp[i], j);
                        ans[i] = j;
                        vis.set(j); 
                        break; 
                    }
                }
            }
        }
    }
    for (int i = 1; i <= n; i++) write(ans[i], 32);
    putchar(10);
    return 0;
}

// ↑↑↑ 暴力/骗分 part ↑↑↑

int correct_solution()
{
    return 0;
}

int solve_once()
{
    memset(head, -1, sizeof(head));
    memset(Degree, 0, sizeof(Degree));
    edges_cnt = 0;
    n = read<int>();
    ans[1] = n + 1;
    for (int i = 1; i <= n; i++) num[tmp[i] = read<int>()] = i;
    for (int u, v, i = 1; i < n; i++) {
        u = read<int>();
        v = read<int>();
        add(u, v);
        Degree[u]++;
        Degree[v]++;
    }
    int cnt1 = 0, cnt2 = 0, pos;
    for (int i = 1; i <= n; i++) if (Degree[i] == 2) cnt2++; else if (Degree[i] == 1) cnt1++, pos = i;
    if (cnt1 == 2 && cnt2 == n - 2) return lianTu(pos);
    for (int i = 1; i <= n; i++) if (Degree[i] == n - 1) return JuHuaTu(i);
    if (n <= 10) baoli();
    correct_solution();
    return 0;
}

int main()
{
    int T = read<int>();
    while (T--) solve_once();
    return 0;
}