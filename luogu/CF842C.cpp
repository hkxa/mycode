//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Ilya And The Tree.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-07.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

namespace against_cpp11 {
    const int N = 2e5 + 7;
    int n;
    int a[N], fa[N];
    int ans[N], gcd_tot[N];
    vector<int> G[N];
    int gcd(int a, int b) {
        return a ? gcd(b % a, a) : b;
    }
    int fpow(int a, int b) {
        int ret = 1;
        while (b) {
            if (b & 1) ret *= a;
            a *= a;
            b >>= 1;
        }
        return ret;
    }
    int prime[N], cnt;
    bool is_not_prime[N];
    void prepare_table() {
        for (int i = 2; i < N; i++) {
            if (!is_not_prime[i]) {
                prime[++cnt] = i;
                for (int j = i + i; j < N; j += i) 
                    is_not_prime[j] = true;
            }
        }
    }
    void dfs(int u, int fa_id) {
        fa[u] = fa_id;
        if (fa_id) {
            gcd_tot[u] = gcd(gcd_tot[fa_id], a[u]);
            ans[u] = max(gcd_tot[fa_id], gcd(ans[fa_id], a[u]));
        } else {
            gcd_tot[u] = a[u];
            ans[u] = a[u];
        }
        int g = a[u], v = fa_id, cnt = 13;
        while (v && cnt--) {
            ans[u] = max(ans[u], gcd(gcd_tot[fa[v]], g));
            g = gcd(g, a[v]);
            v = fa[v];
        }
        for (size_t i = 0; i < G[u].size(); i++)
            if (G[u][i] != fa_id) dfs(G[u][i], u);
    }
    signed main() {
        prepare_table();
        read >> n;
        for (int i = 1; i <= n; i++) read >> a[i];
        for (int i = 1, u, v; i < n; i++) {
            read >> u >> v;
            G[u].push_back(v);
            G[v].push_back(u);
        }
        dfs(1, 0);
        for (int i = 1; i <= n; i++) write << ans[i] << " \n"[i == n];
        return 0;
    }
}

signed main() { return against_cpp11::main(); }