/*************************************
 * @problem:      celebration.
 * @author:       brealid.
 * @time:         2021-07-26.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 3e5 + 7;

int n, m, q, k;
vector<int> G[N], T[N];

namespace TEST_1000 {
    bool s_reach[N], reach_t[N];
    void dfs_s(int u) {
        s_reach[u] = true;
        for (int x: G[u])
            if (!s_reach[x]) dfs_s(x);
    }
    void dfs_t(int u) {
        reach_t[u] = true;
        for (int x: T[u])
            if (!reach_t[x]) dfs_t(x);
    }
    int query(int u, int v) {
        memset(s_reach, 0, sizeof(bool) * (n + 1));
        memset(reach_t, 0, sizeof(bool) * (n + 1));
        dfs_s(u), dfs_t(v);
        int ans = 0;
        for (int i = 1; i <= n; ++i)
            if (s_reach[i] && reach_t[i]) ++ans;
        return ans;
    }
    void solve() {
        int x[2], y[2];
        for (int i = 1, u, v; i <= q; ++i) {
            kin >> u >> v;
            for (int i = 0; i < k; ++i) {
                kin >> x[i] >> y[i];
                G[x[i]].push_back(y[i]);
                T[y[i]].push_back(x[i]);
            }
            kout << query(u, v) << '\n';
            for (int i = 0; i < k; ++i) {
                G[x[i]].pop_back();
                T[y[i]].pop_back();
            }
        }
    }
}

signed main() {
    // file_io::set_to_file("celebration");
    kin >> n >> m >> q >> k;
    for (int i = 1, u, v; i <= m; ++i) {
        kin >> u >> v;
        G[u].push_back(v);
        T[v].push_back(u);
    }
    if (n <= 1000 && q <= 1000) TEST_1000::solve();
    return 0;
}