/*************************************
 * @problem:      [国家集训队]飞飞侠.
 * @user_name:    brealid/hkxadpall/zhaoyi20/j(o)moo/littleTortoise.
 * @time:         2020-09-07.
 * @language:     C++.
 * @fastio_ver:   20200827.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        // simple io flags
        ignore_int = 1 << 0,    // input
        char_enter = 1 << 1,    // output
        flush_stdout = 1 << 2,  // output
        flush_stderr = 1 << 3,  // output
        // combination io flags
        endline = char_enter | flush_stdout // output
    };
    enum number_type_flags {
        // simple io flags
        output_double_stable = 1 << 0,  // output
        output_double_faster = 1 << 1   // output
        // combination io flags
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f & ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore character set : ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (double &lf) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            lf = endch & 15;
            while (isdigit(endch = getchar())) lf = lf * 10 + (endch & 15);
            if (endch == '.') {
                double digit = 0.1;
                while (isdigit(endch = getchar())) {
                    lf += (endch & 15) * digit;
                    digit *= 0.1;
                }
            }
            if (flag) lf = -lf;
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            Int d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return d;
        }
    } read;

    struct Writer {
        Writer& operator << (io_flags f) {
            if (f & char_enter) putchar(10);
            if (f & flush_stdout) fflush(stdout);
            if (f & flush_stderr) fflush(stderr);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
        inline void operator () (double val, int eps_digit = 6, char endch = '\0', number_type_flags flg = output_double_faster) {
            if (flg & output_double_stable) {
                static char output_format[10];
                sprintf(output_format, "%%.%dlf", eps_digit);
                printf(output_format, val);
            } else if (flg & output_double_faster) {
                if (val < 0) {
                    putchar('-');
                    val = -val;
                }
                double eps_number = 0.5;
                for (int i = 1; i <= eps_digit; i++) eps_number /= 10;
                val += eps_number;
                (*this) << (int64)val;
                val -= (int64)val;
                putchar('.');
                while (eps_digit--) {
                    val *= 10;
                    putchar((int)val | '0');
                    val -= (int)val;
                }
            } else {
                (*this) << "1.#ERROR_NOT_IDENTIFIED_FLAG";
            }
            if (endch) putchar(endch);
        }
    } write;
}
using namespace Fastio;
namespace File_IO {
    void init_IO() {
        freopen("[国家集训队]飞飞侠.in", "r", stdin);
        freopen("[国家集训队]飞飞侠.out", "w", stdout);
    }
}

template<typename T>
struct dynamic_array {
    typedef T                                       value_type;
    typedef size_t                                  size_type;
    typedef value_type*                             iterator;
    typedef const value_type*                       const_iterator;
    typedef value_type&                             reference;
    typedef const value_type&                       const_reference;
    typedef std::reverse_iterator<iterator>         reverse_iterator;
    typedef const std::reverse_iterator<iterator>   const_reverse_iterator;
    value_type *pStart, *pFinish, *pEnd_of_Storage;
    dynamic_array() {
        pFinish = pStart = new value_type[4];
        pEnd_of_Storage = pStart + 4;
    }
    template<typename Init_Iterator>
    dynamic_array(Init_Iterator x, Init_Iterator y) {
        pFinish = pStart = new value_type[4];
        pEnd_of_Storage = pStart + 4;
        while (x != y) {
            if (pFinish == pEnd_of_Storage) resize_memory((pEnd_of_Storage - pStart) << 1);
            *(pFinish++) = *(x++);
        }
    }  
    dynamic_array(const dynamic_array<value_type> &from) {
        pFinish = pStart = new value_type[from.max_size()];
        pEnd_of_Storage = pStart + from.max_size();
        while (pFinish - pStart < from.size()) {
            *pFinish = from[pFinish - pStart];
            ++pFinish;
        }
    }
    dynamic_array<value_type> operator = (dynamic_array<value_type> &from) { 
        delete[] pStart;
        pFinish = pStart = new value_type[from.max_size()];
        pEnd_of_Storage = pStart + from.max_size();
        while (pFinish - pStart < from.size()) {
            *pFinish = *from[pFinish - pStart];
            ++pFinish;
        }
    }
    dynamic_array<value_type> operator = (dynamic_array<value_type> from) { *this = from; }
    ~dynamic_array() { delete[] pStart; }
    iterator begin() const { return pStart; }
    iterator end() const { return pFinish; }
    reverse_iterator rbegin() const { return reverse_iterator(pFinish); }
    reverse_iterator rend() const { return reverse_iterator(pStart); }
    bool empty() const { return pStart == pFinish; }
    size_t size() const { return pFinish - pStart; }
    size_t max_size() const { return pEnd_of_Storage - pStart; }
    reference front() { return *pStart; }
    reference back() { return *(pFinish - 1); }
    reference operator [] (int pos) { return *(pStart + pos); }
    const_reference front() const { return *pStart; }
    const_reference back() const { return *(pFinish - 1); }
    const_reference operator [] (int pos) const { return *(pStart + pos); }
    void resize_memory(size_t new_size) {
        iterator neo = new value_type[new_size];
        iterator neo_ed = neo + new_size, neo_t = neo;
        iterator orig = pStart;
        while (orig != pFinish && neo_t != neo_ed)
            *(neo_t++) = *(orig++);
        delete[] pStart;
        pStart = neo;
        pFinish = neo_t;
        pEnd_of_Storage = neo_ed;
    }
    void resize(size_t new_size) {
        resize_memory(new_size);
        while (pFinish != pEnd_of_Storage) *(pFinish++) = value_type();
    }
    void push_back(value_type &val) {
        if (pFinish == pEnd_of_Storage) resize_memory((pEnd_of_Storage - pStart) << 1);
        *(pFinish++) = val;
    }
    void push_back(value_type val) {
        if (pFinish == pEnd_of_Storage) resize_memory((pEnd_of_Storage - pStart) << 1);
        *(pFinish++) = val;
    }
    void pop_back() {
        --pFinish;
        if ((pFinish - pStart) < ((pEnd_of_Storage - pStart) >> 1) && ((pEnd_of_Storage - pStart) >> 1) >= (size_t)4) 
            resize_memory((pEnd_of_Storage - pStart) >> 1);
    }
    void swap(dynamic_array<value_type> &b) {
        swap(pStart, b.pStart);
        swap(pFinish, b.pFinish);
        swap(pEnd_of_Storage, b.pEnd_of_Storage);
    }
};

// #define int int64

const int N = 300 + 7, NODE_MAX = N * N * 3;

int n, m, sq;
bool useful_node[N][N];
int A[N][N], B[N][N], id[N][N];

int tid[N][N << 2];
int id_cnt;
dynamic_array<pair<int, int> > G[NODE_MAX];

void trans_pos(int &x, int &y) {
    int X = m + x - y, Y = x + y - 1;
    x = X; y = Y;
}

void init_edges(int tn, int u, int l, int r) {
    if (l == r) {
        if (useful_node[tn][l]) tid[tn][u] = id[tn][l];
        return;
    }
    tid[tn][u] = ++id_cnt;
    // printf("ln %d range [%d, %d] : id %d\n", tn, l, r, id_cnt);
    int mid = (l + r) >> 1;
    init_edges(tn, u << 1, l, mid);
    init_edges(tn, u << 1 | 1, mid + 1, r);
    if (tid[tn][u << 1]) G[tid[tn][u]].push_back(make_pair(tid[tn][u << 1], 0));
    if (tid[tn][u << 1 | 1]) G[tid[tn][u]].push_back(make_pair(tid[tn][u << 1 | 1], 0));
}

namespace insert_edges_values {
    int tn, ml, mr, fromid, eval;
}
void insert_edges(int u, int l, int r) {
    using namespace insert_edges_values;
    if (l >= ml && r <= mr) {
        // printf("ln %d range[%d, %d]\n", tn, l, r);
        G[fromid].push_back(make_pair(tid[tn][u], eval));
        return;
    }
    int mid = (l + r) >> 1;
    if (ml <= mid) insert_edges(u << 1, l, mid);
    if (mr > mid) insert_edges(u << 1 | 1, mid + 1, r);
}

int64 dis[NODE_MAX];
int x1, _y1, x2, y2, x3, y3;
inline void dijkstra(int sx, int sy) {
    memset(dis, 0x3f, sizeof(int64) * (id_cnt + 1));
    dis[id[sx][sy]] = 0;
    priority_queue<pair<int, int>, dynamic_array<pair<int, int> > > q;
    // queue<int> q;
    q.push(make_pair(0, id[sx][sy]));
    int status = 0;
    while (!q.empty()) {
        int u = q.top().second; q.pop();
        if (u == id[x1][_y1]) status |= 1;
        else if (u == id[x2][y2]) status |= 2;
        else if (u == id[x3][y3]) status |= 4;
        if (status == 7) return;
        // int u = q.front(); q.pop();
        for (dynamic_array<pair<int, int> >::iterator it = G[u].begin(); it != G[u].end(); it++) {
            int v = it->first, w = it->second;
            if (dis[v] > dis[u] + w) {
                dis[v] = dis[u] + w;
                q.push(make_pair(-dis[v], v));
            }
        }
    }
    // printf("st_pos {%d, %d} : \n", (sx + sy - m + 1) / 2, (sy - sx + m + 1) / 2);
    // for (int i = 1; i <= id_cnt; i++)
    //     printf("%-2d%c", i, " \n"[i == id_cnt]);
    // for (int i = 1; i <= id_cnt; i++)
    //     printf("%-2lld%c", dis[i] != 0x3f3f3f3f3f3f3f3f ? dis[i] : -1, " \n"[i == id_cnt]);
    // for (int i = 1; i <= n; i++)
    //     for (int j = 1; j <= m; j++) 
    //         printf("%-2lld%c", dis[id[m + i - j][i + j - 1]] != 0x3f3f3f3f3f3f3f3f ? 
    //                            dis[id[m + i - j][i + j - 1]] : -1, " \n"[j == m]);
}

signed main() {
    // freopen("zhber.out", "w", stderr);
    // File_IO::init_IO();
    read >> n >> m;
    sq = n + m - 1;
    for (int i = 1; i <= n; i++)
        for (int j = 1; j <= m; j++) {
            read >> A[m + i - j][i + j - 1];
            useful_node[m + i - j][i + j - 1] = true;
        }
    for (int i = 1; i <= n; i++)
        for (int j = 1; j <= m; j++) 
            read >> B[m + i - j][i + j - 1];
    // 45°旋转后建矩形
    for (int i = 1; i <= sq; i++) {
        for (int j = 1; j <= sq; j++)
            id[i][j] = ++id_cnt;
        init_edges(i, 1, 1, sq);
    }
    for (int i = 1; i <= sq; i++) {
        for (int j = 1; j <= sq; j++)
            if (A[i][j]) {
                int up = max(i - A[i][j], 1), dn = min(i + A[i][j], sq);
                insert_edges_values::ml = max(j - A[i][j], 1);
                insert_edges_values::mr = min(j + A[i][j], sq);
                insert_edges_values::fromid = id[i][j];
                insert_edges_values::eval = B[i][j];
                // printf("(%d, %d) : expand ((%d, %d), (%d, %d))\n", i, j, up, max(j - A[i][j], 1), dn, min(j + A[i][j], sq));
                for (int k = up; k <= dn; k++) {
                    insert_edges_values::tn = k;
                    insert_edges(1, 1, sq);
                }
            }
    }
    fprintf(stderr, "%.3lf [%d, %d]\n", (double)clock() / CLOCKS_PER_SEC, id_cnt, N * N * 3);
    // for (int i = 1; i <= n; i++)
    //     for (int j = 1; j <= m; j++) 
    //         printf("[%d, %d](%d)%c", m + i - j, i + j - 1, id[m + i - j][i + j - 1], " \n"[j == m]);
    read >> x1 >> _y1 >> x2 >> y2 >> x3 >> y3;
    trans_pos(x1, _y1); trans_pos(x2, y2); trans_pos(x3, y3);
    int64 X_val = 0, Y_val = 0, Z_val = 0;
    dijkstra(x1, _y1); Y_val += dis[id[x2][y2]]; Z_val += dis[id[x3][y3]];
    dijkstra(x2, y2); X_val += dis[id[x1][_y1]]; Z_val += dis[id[x3][y3]];
    dijkstra(x3, y3); X_val += dis[id[x1][_y1]]; Y_val += dis[id[x2][y2]];
    int64 ans_val = min(min(X_val, Y_val), Z_val);
    if (ans_val >= 0x3f3f3f3f3f3f3f3f) {
        write << "NO" << endline;
        return 0;
    }
    if (X_val == ans_val) write << 'X' << endline;
    else if (Y_val == ans_val) write << 'Y' << endline;
    else write << 'Z' << endline;
    write << ans_val << endline;
    return 0;
}