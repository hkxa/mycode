/*************************************
 * @problem:      P2780 [AHOI2016初中组]游戏.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-02-10.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int MaxN = 250 + 3, MaxAB = 75000 + MaxN + 3;

int n, k, a, b;
int x[MaxN];
bitset<MaxAB> could[MaxN];
set<int> s;
set<int>::iterator it;
int ans = 0, tmp;

int main()
{
    n = read<int>();
    k = read<int>();
    a = read<int>();
    b = read<int>();
    for (int i = 1; i <= n; i++) x[i] = read<int>();
    could[0].set(0);
    for (int i = 1; i <= n; i++) {
        for (int j = k; j >= 1; j--) {
            could[j] |= (could[j - 1] << x[i]);
        }
    }
    for (int i = 0; i <= 75250; i++) {
        if (could[k].test(i)) {
            s.insert(i);
            // printf("set : insert %d.\n", i);
        }
    }
    for (int i = a; i <= b; i++) {
        it = s.lower_bound(i);
        tmp = MaxAB;
        if (it != s.end()) tmp = min(tmp, *it - i);
        if (it != s.begin()) tmp = min(tmp, i - *(--it));
        ans = max(ans, tmp);
        // printf("if choose t = %d, mininum = %d.\n", i, tmp);
    }
    write(ans, 10);
    // 1 3 16 73 2341 12343 
    // search A
    // max{<A} min{>A}
    // find(A)
    return 0;
}