/*************************************
 * problem:      P2730 魔板 Magic Squares.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-11-10.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

inline string A(string s) { return (string)"" + s[4] + s[5] + s[6] + s[7] + s[0] + s[1] + s[2] + s[3]; }
inline string B(string s) { return (string)"" + s[3] + s[0] + s[1] + s[2] + s[7] + s[4] + s[5] + s[6]; }
inline string C(string s) { return (string)"" + s[0] + s[5] + s[1] + s[3] + s[4] + s[6] + s[2] + s[7]; }

string goal, t;
map<string, string> cost;
queue<string> q;

int main()
{
    for (int i = 0; i < 8; i++) {
        cin >> t;
        goal += t;
    }
    swap(goal[4], goal[7]);
    swap(goal[5], goal[6]);
    // cout << goal << endl;
    cost["12348765"] = "";
    q.push("12348765");
    while (!q.empty()) {
        if (cost.find(goal) != cost.end()) break;
        t = q.front(); q.pop();
        if (cost.find(A(t)) == cost.end()) cost[A(t)] = cost[t] + 'A', q.push(A(t));
        if (cost.find(B(t)) == cost.end()) cost[B(t)] = cost[t] + 'B', q.push(B(t));
        if (cost.find(C(t)) == cost.end()) cost[C(t)] = cost[t] + 'C', q.push(C(t));
    }
    cout << cost[goal].length() << endl << cost[goal] << endl;
    return 0;
}