/*************************************
 * problem:      SP35 EQBOX.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-15.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define puts_return(x) { puts(x); return 0; }
#define write_return(x) { write(x); return 0; }

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

bool tryEQBOX(int64 A, int64 B, int64 X, int64 Y)
{
    static double BC, BE, CI, DE, DF, DH, EF, EH, angleEDF, angleEDH, angleFDH;
    if (A > B) swap(A, B);
    if (X > Y) swap(X, Y);
    if (X < A && Y < B) return true;
    if (X * Y > A * B) return false;
    EF = X;
    DF = Y;
    EH = BC = B;
    DE = sqrt(EF * EF + DF * DF);
    DH = sqrt(DE * DE - EH * EH);
    angleEDF = atan(EF / DF);
    angleEDH = asin(EH / DE);
    angleFDH = angleEDH - angleEDF;
    BE = EF * sin(angleFDH);
    CI = DH + BE * 2;
    // printf("angle ∠FDH = %.6lf.\n", angleFDH * 180 / M_PI);
    return CI < A;
}

int main()
{
    int64 T = read<int64>(), a, b, x, y;
    while (T--) {
        a = read<int64>();
        b = read<int64>();
        x = read<int64>();
        y = read<int64>();
        puts(tryEQBOX(a, b, x, y) ? "Escape is possible." : "Box cannot be dropped.");
    }
    return 0;
}