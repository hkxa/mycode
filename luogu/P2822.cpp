/*************************************
 * problem:      P2822 组合数问题.
 * user ID:      85848.
 * user name:    hkxadpall.
 * time:         2019-09-16.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define ForInVec(vectorType, vectorName, iteratorName) for (vector<vectorType>::iterator iteratorName = vectorName.begin(); iteratorName != vectorName.end(); iteratorName++)
#define ForInVI(vectorName, iteratorName) ForInVec(int, vectorName, iteratorName)
#define ForInVE(vectorName, iteratorName) ForInVec(Edge, vectorName, iteratorName)
#define MemWithNum(array, num) memset(array, num, sizeof(array))
#define Clear(array) MemWithNum(array, 0)
#define MemBint(array) MemWithNum(array, 0x3f)
#define MemInf(array) MemWithNum(array, 0x7f)
#define MemEof(array) MemWithNum(array, -1)
#define ensuref(condition) do { if (!(condition)) exit(0); } while(0)

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

struct FastIOer {
#   define createReadlnInt(type)            \
    FastIOer& operator >> (type &x)         \
    {                                       \
        x = read<type>();                   \
        return *this;                       \
    }
    createReadlnInt(short);
    createReadlnInt(unsigned short);
    createReadlnInt(int);
    createReadlnInt(unsigned);
    createReadlnInt(long long);
    createReadlnInt(unsigned long long);
#   undef createReadlnInt

#   define createWritelnInt(type)           \
    FastIOer& operator << (type x)          \
    {                                       \
        write<type>(x);                     \
        return *this;                       \
    }
    createWritelnInt(short);
    createWritelnInt(unsigned short);
    createWritelnInt(int);
    createWritelnInt(unsigned);
    createWritelnInt(long long);
    createWritelnInt(unsigned long long);
#   undef createWritelnInt
    
    FastIOer& operator >> (char &x)
    {
        x = getchar();
        return *this;
    }
    
    FastIOer& operator << (char x)
    {
        putchar(x);
        return *this;
    }
    
    FastIOer& operator << (const char *x)
    {
        int __pos = 0;
        while (x[__pos]) {
            putchar(x[__pos++]);
        }
        return *this;
    }
} fast;

int t, k, n, m;
long long C[2007][2007], sum[2007][2007];

void prepareC()
{
    C[0][0] = 1;
    for (int i = 1; i <= 2001; i++) {
        for (int j = 1; j <= i; j++) {
            C[i][j] = (C[i - 1][j - 1] + C[i - 1][j]) % k;
            // if (i < 8 && j < 8) printf("%d%c", C[i][j], " \n"[i == j]);
        }
    }
    for (int i = 1; i <= 2001; i++) {
        for (int j = 1; j <= 2001; j++) {
            sum[i][j] = sum[i - 1][j] + sum[i][j - 1] - sum[i - 1][j - 1] + (!C[i][j] && j <= i);
        }
    }
    /*
    TODO
1 5 6 6

1
1 1
1 2 1
1 3 3 1
1 4 1 4 1
1 0 0 0 0 1
1 1 0 0 0 1 1
    */
}

int main()
{
    fast >> t >> k;
    prepareC();
    while (t--) {
        fast >> n >> m;
        fast << sum[n + 1][m + 1] << '\n';
    }
    return 0;
}