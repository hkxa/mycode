/*************************************
 * @problem:      Strange Function.
 * @user_name:    brealid/hkxadpall/zhaoyi20/j(o)moo/littleTortoise.
 * @time:         2020-08-28.
 * @language:     C++.
 * @fastio_ver:   20200827.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        // simple io flags
        ignore_int = 1 << 0,    // input
        char_enter = 1 << 1,    // output
        flush_stdout = 1 << 2,  // output
        flush_stderr = 1 << 3,  // output
        // combination io flags
        endline = char_enter | flush_stdout // output
    };
    enum number_type_flags {
        // simple io flags
        output_double_stable = 1 << 0,  // output
        output_double_faster = 1 << 1   // output
        // combination io flags
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f & ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore character set : ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (double &lf) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            lf = endch & 15;
            while (isdigit(endch = getchar())) lf = lf * 10 + (endch & 15);
            if (endch == '.') {
                double digit = 0.1;
                while (isdigit(endch = getchar())) {
                    lf += (endch & 15) * digit;
                    digit *= 0.1;
                }
            }
            if (flag) lf = -lf;
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            Int d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return d;
        }
    } read;

    struct Writer {
        Writer& operator << (io_flags f) {
            if (f & char_enter) putchar(10);
            if (f & flush_stdout) fflush(stdout);
            if (f & flush_stderr) fflush(stderr);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
        inline void operator () (double val, int eps_digit = 6, char endch = '\0', number_type_flags flg = output_double_faster) {
            if (flg & output_double_stable) {
                static char output_format[10];
                sprintf(output_format, "%%.%dlf", eps_digit);
                printf(output_format, val);
            } else if (flg & output_double_faster) {
                if (val < 0) {
                    putchar('-');
                    val = -val;
                }
                (*this) << (int64)val;
                val -= (int64)val;
                putchar('.');
                double eps_number = 0.5;
                for (int i = 1; i <= eps_digit; i++) eps_number /= 10;
                val += eps_number;
                while (eps_digit--) {
                    val *= 10;
                    putchar((int)val | '0');
                    val -= (int)val;
                }
            } else {
                (*this) << "1.#ERROR_NOT_IDENTIFIED_FLAG";
            }
            if (endch) putchar(endch);
        }
    } write;
}
using namespace Fastio;
namespace File_IO {
    void init_IO() {
        freopen("Strange Function.in", "r", stdin);
        freopen("Strange Function.out", "w", stdout);
    }
}

// #define int int64
// #define int128 int64
// #define int128 __int128

const int Mod = 998244353;
const int MN = 2025;
 
inline void Add(int &x, int y) { x -= (x += y) >= Mod ? Mod : 0; }
 
int N, K;
 
void Solve1() {
	static int Arr[MN], Ans;
	Arr[0] = 1;
	for (int i = 1; i <= N; ++i)
		for (int j = i; j <= N; ++j)
			Add(Arr[j], Arr[j - i]);
	for (int i = 1; i <= N; ++i) Add(Ans, Arr[i]);
	printf("%d\n", Ans);
}
 
int f[MN][MN], g[MN][MN];
void Solve2() {
	int Ans = N;
	for (int i = 1; i <= N; ++i) f[i][i] = 1;
	for (int i = 2, s = 3; s <= N; s += ++i) {
		for (int j = N / (s - i); j > 1; --j)
			for (int k = (s - i) * j; k <= N; ++k)
				Add(f[j - 1][k], f[j][k]);
		for (int j = 1; s * j <= N; ++j)
			for (int k = s * j; k <= N; ++k)
				Add(Ans, g[j][k] = f[j][k - i * j]);
		swap(f, g);
	}
	printf("%d\n", Ans);
}
 
int Ans, stk[MN], tp;
vector<int> arr1, arr2;
inline bool chk() {
	arr1.clear();
	for (int i = 1; i <= tp; ++i) arr1.push_back(stk[i]);
	for (int t = 1; t < K; ++t) {
		int sum = 0;
		for (int x : arr1) sum += x;
		if (sum > N) return 0;
		arr2.clear(); int i = arr1.size();
		for (int x : arr1) { while (x--) arr2.push_back(i); --i; }
		reverse(arr2.begin(), arr2.end());
		swap(arr1, arr2);
	}
	int sum = 0;
	for (int x : arr1) sum += x;
	if (sum > N) return 0;
	return ++Ans, 1;
}
bool DFS(int sum, int mx) {
	if (sum && !chk()) return 0;
	for (int i = mx; i <= N - sum; ++i) {
		stk[++tp] = i;
		bool d = DFS(sum + i, i);
		--tp;
		if (!d) break;
	}
	return 1;
}
void Solve3() {
	DFS(0, 1);
    write << Ans << '\n';
}
 
int main() {
    // File_IO::init_IO();
    read >> N >> K;
	if (K == 1) Solve1();
	else if (K == 2) Solve2();
	else if (K >= 9) puts("1");
	else Solve3();
	return 0;
}