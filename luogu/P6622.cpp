/*************************************
 * @problem:      「联合省选 2020 A | B」信号传递.
 * @author:       brealid.
 * @time:         2021-04-04.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int M = 23, STATE_MAX = 1 << 23;

int n, m, k, mask;
int adj[M][M];
int f[STATE_MAX];
// const double f_MemoryMB = sizeof(f) / 1048576.0;
int lg2[37];

int popcnt(int val) {
    static const int table[] = {0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5};
    int nRet(table[val & 31]);
    while (val >>= 5) nRet += table[val & 31];
    return nRet;
}

int Sav[M][STATE_MAX >> 1];
#define contrib_1(u, relate) (adj[relate][u] + adj[u][relate] * k)
#define contrib_2(u, relate) (-adj[u][relate] + adj[relate][u] * k)

// int g2(int u, int status) {
//     int nRet(0);
//     for (int i = 0; i < m; ++i) {
//         if ((status >> i) & 1) nRet += adj[i][u] + adj[u][i] * k;
//         else if (i ^ u) nRet += -adj[u][i] + adj[i][u] * k;
//     }
//     return nRet;
// }

int g(int u, int status) {
    if (status >= 4194304) return g(u, status ^ 4194304) + contrib_1(u, 22) - contrib_2(u, 22);
    int &nRet = Sav[u][status];
    if (!status || nRet) return nRet;
    int bit = status & -status, id = lg2[bit % 37];
    // assert(bit == (1 << id));
    // printf("[%d, %d] contrib_1(%d, %d) = %d + %d * %d\n", bit, id, u, id, adj[id][u], adj[u][id], k);
    // printf("g(%d, %d) = %d [%d | %d | %d] (%d)\n", u, status, g(u, status ^ bit) + contrib_1(u, id) - contrib_2(u, id), g(u, status ^ bit), contrib_1(u, id), contrib_2(u, id), g2(u, status));
    return nRet = g(u, status ^ bit) + contrib_1(u, id) - contrib_2(u, id);
}

signed main() {
    for (int i = 0; i < 36; ++i) lg2[(1ll << i) % 37] = i;
    kin >> n >> m >> k;
    mask = (1 << m) - 1;
    for (int i = 2, last = kin.get<int>() - 1, now; i <= n; ++i) {
        kin >> now;
        ++adj[last][now -= 1];
        last = now;
    }
    for (int u = 0; u < m; ++u)
        for (int j = 0; j < m; ++j)
            if (u ^ j) Sav[u][0] += contrib_2(u, j);
    // for (int u = 0; u < m; ++u)
    //     printf("g(%d, %d) = %d (%d)\n", u, 0, Sav[u][0], g2(u, 0));
    memset(f, 0x7f, sizeof(f));
    f[0] = 0;
    for (int status = 0; status < mask; ++status) {
        for (int i = 0; i < m; ++i) {
            if ((status >> i) & 1) continue;
            int nxt = status | (1 << i);
            // assert((nxt ^ (1 << i)) == status);
            // assert(popcnt(nxt) == __builtin_popcount(nxt));
            f[nxt] = min(f[nxt], f[status] + g(i, status) * popcnt(nxt));
        }
    }
    kout << f[mask] << '\n';
    return 0;
}