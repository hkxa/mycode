#include <bits/stdc++.h> 
using namespace std; 
int m, s, c, ans;
int pos[207], diff[207];

bool cmp(int x, int y)
{
    return x > y;
}

int main() 
{ 
    scanf("%d%d%d", &m, &s, &c);
    m--;
    for (int i = 1; i <= c; i++)
        scanf("%d", pos + i);
    if (m >= c) {
        printf("%d\n", c);
        return 0;
    }
    sort(pos + 1, pos + c + 1);
    for(int i = 1; i < c; i++)
        diff[i] = pos[i + 1] - pos[i];
    sort(diff + 1, diff + c, cmp);
    ans = pos[c] - pos[1] + 1;
    for (int i = 1; i <= m; i++)
        ans = ans - diff[i] + 1;
    printf("%d", ans);
    return 0;
} 