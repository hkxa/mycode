/*************************************
 * problem:      P2657 [SCOI2009]windy数.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-07.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int numLen(int num)
{
    int ret(0);
    while (num) {
        num /= 10;
        ret++;
    }
    return ret;
}

long long f[13][13];
// f[第几位][这一位的数字]

void windyPrepare()
{
    for (int i = 0; i <= 9; i++) f[1][i] = 1;
    for (int i = 2; i <= 11; i++) {
        for (int j = 0; j <= 9; j++) {
            for (int k = 0; k <= 9; k++) {
                if (abs(j - k) >= 2) f[i][j] += f[i - 1][k];
            }
        }
    }
    /** 
     * i\j 0  1  2  3  4  5  6  7  8  9
     *  1  1  1  1  1  1  1  1  1  1  1 
     *  2  8  7  7  7  7  7  7  7  7  8
     *  3  57 50 51 51 51 51 51 51 50 57
     * ... .. .. .. .. .. .. .. .. .. ..
     */
}

long long reserveNum(long long num)
{
    int a[15], tp = 0, nC = 0;
    long long ret = 0;
    while (num) {
        a[tp++] = num % 10;
        num /= 10;
    }
    while (nC < tp) {
        ret = (ret * 10) + a[nC++];
    }
    return ret;
}

int windy(int x)
{
    long long len = numLen(x), ans(0),  mxi = reserveNum(x);
    // printf("mxi = %lld.\n", mxi);
    // if (len <= 1) return x + 1;
    for (int i = 1; i < len; i++) {
        for (int j = 1; j <= 9; j++) {
            ans += f[i][j];
            // printf("[func #1] ans += f[%d][%d] (%d + %d = %d)\n", i, j, ans - f[i][j], f[i][j], ans);
        }
        // x /= 10;
    }
    int las = mxi % 10;
    mxi /= 10;
    for (int i = 1; i < las; i++) {
        ans += f[len][i];
    }
    for (int i = len - 1; i >= 1; i--) {
        for (int j = 0; j < mxi % 10; j++) {
            if (abs(j - las) >= 2) ans += f[i][j]; 
        } 
        if (abs(mxi % 10 - las) < 2) break;
        las = mxi % 10;
        mxi /= 10;
    }
    return ans;
}

int main()
{
    windyPrepare();
    long long A = read<long long>(), B = read<long long>();
    // write(windy(A), 32);
    // write(windy(B + 1), 10);
    write(windy(B + 1) - windy(A));
    return 0;
}