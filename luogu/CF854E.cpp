//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Boredom.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-10.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

namespace against_cpp11 {
    const int N = 200000 + 7, SIZ = N * 25;
    int n, m;
    int p[N];
    int root[N];
    int ls[SIZ], rs[SIZ], sum[SIZ], tree_node_count;
    int tree_insert(int original_node, int l, int r, int x) {
        int u = ++tree_node_count;
        sum[u] = sum[original_node] + 1;
        int mid = (l + r) >> 1;
        if (l == r) return u;
        if (x <= mid) {
            rs[u] = rs[original_node];
            ls[u] = tree_insert(ls[original_node], l, mid, x);
        } else {
            ls[u] = ls[original_node];
            rs[u] = tree_insert(rs[original_node], mid + 1, r, x);
        }
        return u;
    }
    int tree_query(int u, int v, int l, int r, int ml, int mr) {
        if (l > mr || r < ml) return 0;
        if (l >= ml && r <= mr) return sum[v] - sum[u];
        int mid = (l + r) >> 1;
        return tree_query(ls[u], ls[v], l, mid, ml, mr) + tree_query(rs[u], rs[v], mid + 1, r, ml, mr);
    }
    inline int64 count_way(int64 number) {
        return number * (number - 1) / 2;
    }
    inline int64 query_sum(int a, int b, int c, int d) {
        if (a > b || c > d) return 0;
        return count_way(tree_query(root[a - 1], root[b], 1, n, c, d));
    }
    signed main() {
        read >> n >> m;
        for (int i = 1; i <= n; i++) {
            read >> p[i];
            root[i] = tree_insert(root[i - 1], 1, n, p[i]);
        }
        for (int i = 1, l, r, d, u; i <= m; i++) {
            read >> l >> d >> r >> u;
            write << count_way(n) - count_way(l - 1) - count_way(d - 1) - count_way(n - r) - count_way(n - u)
                   + query_sum(1, l - 1, 1, d - 1) + query_sum(1, l - 1, u + 1, n)
                   + query_sum(r + 1, n, 1, d - 1) + query_sum(r + 1, n, u + 1, n) << '\n'; 
        }
        return 0;
    }
}

signed main() { return against_cpp11::main(); }