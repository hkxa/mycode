/*************************************
 * @problem:      「ZJOI2016」小星星.
 * @author:       brealid.
 * @time:         2020-12-03.
*************************************/
#include <bits/stdc++.h>
typedef long long int64;

const int N = 18;
int n, m;
bool enable[N];
int orig[N][N], now[N][N];
int64 f[N][N];

void dp(int u, int fa) {
    for (int i = 1; i <= n; ++i) f[u][i] = 1;
    for (int v = 1; v <= n; ++v) {
        if (!now[u][v] || v == fa) continue;
        dp(v, u);
        for (int i = 1; i <= n; ++i) {
            int64 tot = 0;
            for (int j = 1; j <= n; ++j)
                if (enable[i] && enable[j] && orig[i][j])
                    tot += f[v][j];
            f[u][i] *= tot;
        }
    }
}

signed main() {
    std::cin >> n >> m;
    for (int i = 1, u, v; i <= m; ++i) {
        std::cin >> u >> v;
        orig[u][v] = orig[v][u] = true;
    }
    for (int i = 1, u, v; i < n; ++i) {
        std::cin >> u >> v;
        now[u][v] = now[v][u] = true;
    }
    int mask = 1 << n;
    int64 ans = 0;
    for (int b = 0; b < mask; ++b) {
        int removed = 0;
        for (int j = 1; j <= n; ++j) {
            enable[j] = (b >> (j - 1)) & 1;
            if (!enable[j]) ++removed;
        }
        dp(1, 0);
        int64 res = 0;
        for (int i = 1; i <= n; ++i) res += f[1][i];
        if (removed & 1) ans -= res;
        else ans += res;
    }
    std::cout << ans << std::endl;
    return 0;
}