/*************************************
 * problem:      P1337 [JSOI2004]平衡点 / 吊打XXX.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-11-07.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define BigRand() (rand() * RAND_MAX + rand())
#define RandDouble(l, r) (BigRand() * ((r) - (l)) / 1073741824.0 + (l))

const double start_temperature = 10000;
const double delta_t = 0.996;
const double end_eps = 1e-14;
const double answer_decide_eps = 0.065;
const double answer_diff_eps = 0.0005;

long double dist(int i, double px, double py);
long double calcAnswer(double x, double y);

int n;
int x[1007], y[1007], w[1007];
double mx = 0, my = 0; 
int wtot = 0;
double dx, dy, now;
double ansx, ansy;
long double best = 1e15;

int main()
{
    srand(time(0) * 20170933 + 'O' * 'I');
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        x[i] = read<int>();
        y[i] = read<int>();
        w[i] = read<int>();
        mx += x[i] * w[i];
        my += y[i] * w[i];
        wtot += w[i];
    }
    mx /= wtot;
    my /= wtot;
    best = calcAnswer(mx, my);
    ansx = mx;
    ansy = my;
    for (double t = start_temperature; t > end_eps; t *= delta_t) {
        dx = RandDouble(-t, +t);
        dy = RandDouble(-t, +t);
        // printf("(%.3lf, %.3lf) ---(%.3lf, %.3lf)---> (%.3lf, %.3lf)\n", mx, my, dx, dy, mx + dx, my + dy);
        now = calcAnswer(mx + dx, my + dy);
        if (now < best) {
            mx += dx;
            my += dy;
            best = now;
            ansx = mx;
            ansy = my;
        } else if (exp((now - best) * (now - best) / t) < RandDouble(0, 1)) {
            mx += dx;
            my += dy;
        }
    }
    for (mx = ansx - answer_decide_eps; mx <= ansx + answer_decide_eps; mx += answer_diff_eps) {
        for (my = ansy - answer_decide_eps; my < ansy + answer_decide_eps; my += answer_diff_eps) {
            now = calcAnswer(mx, my);
            if (now < best) {
                best = now;
                ansx = mx;
                ansy = my;
            }
        }
    }
    printf("%.3lf %.3lf\n", ansx, ansy);
    return 0;
}
 
long double dist(int i, double px, double py)
{
    return sqrt((x[i] - px) * (x[i] - px) + (y[i] - py) * (y[i] - py));
}

long double calcAnswer(double x, double y)
{
    long double res = 0;
    for (int i = 1; i <= n; i++) {
        res += dist(i, x, y) * w[i];
    }
    return res;
}