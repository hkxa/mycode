/*************************************
 * @problem:      [SDOI2016]生成魔咒.
 * @user_name:    brealid/hkxadpall/zhaoyi20/j(o)moo/littleTortoise.
 * @time:         2020-08-30.
 * @language:     C++.
 * @fastio_ver:   20200827.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        // simple io flags
        ignore_int = 1 << 0,    // input
        char_enter = 1 << 1,    // output
        flush_stdout = 1 << 2,  // output
        flush_stderr = 1 << 3,  // output
        // combination io flags
        endline = char_enter | flush_stdout // output
    };
    enum number_type_flags {
        // simple io flags
        output_double_stable = 1 << 0,  // output
        output_double_faster = 1 << 1   // output
        // combination io flags
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f & ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore character set : ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (double &lf) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            lf = endch & 15;
            while (isdigit(endch = getchar())) lf = lf * 10 + (endch & 15);
            if (endch == '.') {
                double digit = 0.1;
                while (isdigit(endch = getchar())) {
                    lf += (endch & 15) * digit;
                    digit *= 0.1;
                }
            }
            if (flag) lf = -lf;
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            Int d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return d;
        }
    } read;

    struct Writer {
        Writer& operator << (io_flags f) {
            if (f & char_enter) putchar(10);
            if (f & flush_stdout) fflush(stdout);
            if (f & flush_stderr) fflush(stderr);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
        inline void operator () (double val, int eps_digit = 6, char endch = '\0', number_type_flags flg = output_double_faster) {
            if (flg & output_double_stable) {
                static char output_format[10];
                sprintf(output_format, "%%.%dlf", eps_digit);
                printf(output_format, val);
            } else if (flg & output_double_faster) {
                if (val < 0) {
                    putchar('-');
                    val = -val;
                }
                double eps_number = 0.5;
                for (int i = 1; i <= eps_digit; i++) eps_number /= 10;
                val += eps_number;
                (*this) << (int64)val;
                val -= (int64)val;
                putchar('.');
                while (eps_digit--) {
                    val *= 10;
                    putchar((int)val | '0');
                    val -= (int)val;
                }
            } else {
                (*this) << "1.#ERROR_NOT_IDENTIFIED_FLAG";
            }
            if (endch) putchar(endch);
        }
    } write;
}
using namespace Fastio;
namespace File_IO {
    void init_IO() {
        freopen("[SDOI2016]生成魔咒.in", "r", stdin);
        freopen("[SDOI2016]生成魔咒.out", "w", stdout);
    }
}

// #define int int64

const int N = 1e5 + 6;

int n, m, a[N], b[N];
int sa[N], rk[N], tp[N], tx[N], he[N], st[N][20];
int64 ans;
set<int> s;

inline void tsort() {
	for (int i = 1; i <= m; i++) tx[i] = 0;
	for (int i = 1; i <= n; i++) ++tx[rk[i]];
	for (int i = 1; i <= m; i++) tx[i] += tx[i-1];
	for (int i = n; i; i--) sa[tx[rk[tp[i]]]--] = tp[i];
}

inline bool pd(int i, int w) {
	return tp[sa[i-1]] == tp[sa[i]] && tp[sa[i-1]+w] == tp[sa[i]+w];
}

inline void SA() {
	for (int i = 1; i <= n; i++) {
		rk[i] = a[i] = lower_bound(b + 1, b + m + 1, a[i]) - b;
		tp[i] = i;
	}
	tsort();
	for (int w = 1, p = 0; p < n; m = p, w <<= 1) {
		p = 0;
		for (int i = 1; i <= w; i++) tp[++p] = n - w + i;
		for (int i = 1; i <= n; i++)
			if (sa[i] > w) tp[++p] = sa[i] - w;
		tsort();
		swap(rk, tp);
		rk[sa[1]] = p = 1;
		for (int i = 2; i <= n; i++)
			rk[sa[i]] = pd(i, w) ? p : ++p;
	}
	int p = 0;
	for (int i = 1; i <= n; i++) {
		if (p) --p;
		int j = sa[rk[i]-1];
		while (a[i+p] == a[j+p]) ++p;
		he[rk[i]] = p;
	}
}

inline void make_ST() {
	for (int i = 1; i <= n; i++) st[i][0] = he[i];
	int w = log(n) / log(2);
	for (int k = 1; k <= w; k++)
		for (int i = 1; i <= n; i++) {
			if (i + (1 << k) > n + 1) break;
			st[i][k] = min(st[i][k-1], st[i+(1<<(k-1))][k-1]);
		}
}

inline int get(int l, int r) {
	int k = log(r - l + 1) / log(2);
	return min(st[l][k], st[r-(1<<k)+1][k]);
}

template<typename Iter> Iter pre(Iter x) { return --x; }
template<typename Iter> Iter nxt(Iter x) { return ++x; }

signed main() {
    // File_IO::init_IO();
	read >> n;
	for (int i = 1; i <= n; i++) b[i] = a[i] = read.get<int>();
	sort(b + 1, b + n + 1);
	m = unique(b + 1, b + n + 1) - b - 1;
	reverse(a + 1, a + n + 1);
	SA();
	make_ST();
	for (int i = n; i; i--) {
		s.insert(rk[i]);
		set<int>::iterator it = s.find(rk[i]);
		int k = 0;
		if (it != s.begin()) {
			int p = *pre(it);
			k = get(p + 1, rk[i]);
		}
		if (nxt(it) != s.end()) {
			int p = *nxt(it);
			k = max(k, get(rk[i] + 1, p));
		}
		ans += n + 1 - i - k;
        write << ans << endline;
	}
	return 0;
}

/*
1
3
6
9
12
17
22
*/