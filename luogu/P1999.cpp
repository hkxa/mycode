/*************************************
 * problem:      P1999 高维正方体.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-06-14.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

#define pMod 1000000007
long long n, m, ans;

long long fexp(long long x, long long y)
{
    long long ans = 1;
    while (y) {
        if (y & 1) ans = ans * x % pMod;
        x = x * x % pMod;
        y >>= 1;
    }
    return ans;
}

#define inv(x) fexp(x, pMod - 2)
#define modDiv(x, y) ((x) * inv(y) % pMod);

int main()
{
    n = read<int>();
    m = read<int>();
    if (n < m) {
        puts("0");
        return 0;
    }
    ans = fexp(2, n);
    for (int i = 1; i <= m; i++)
        ans = modDiv(ans * (n - i + 1) % pMod, 2 * i);
    write(ans);
    return 0;
}