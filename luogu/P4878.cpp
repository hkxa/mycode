//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Layout 排队布局.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-14.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 988244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

const int N = 100000 + 7;

int n, ML, MD;
vector<pair<int, int> > G[N];
int pos[N], enter[N];
bool inQ[N];

void SPFA(int s) {
    queue<int> q;
    q.push(s);
    memarr(n, 0x3fffffff, pos);
    memarr(n, 0, inQ, enter);
    pos[s] = 0;
    inQ[s] = 1;
    while (!q.empty()) {
        int u = q.front(); q.pop();
        inQ[u] = 0;
        for (size_t i = 0; i < G[u].size(); i++) {
            int v = G[u][i].first, w = G[u][i].second;
            if (pos[v] > pos[u] + w) {
                pos[v] = pos[u] + w;
                enter[v] = enter[u] + 1;
                if (enter[v] >= n) {
                    puts("-1");
                    exit(0);
                }
                if (!inQ[v]) {
                    q.push(v);
                    inQ[v] = true;
                }
            }
        }
    }
}

int vis[N];
void reach_n(int u) {
    vis[u] = true;
    for (size_t i = 0; i < G[u].size(); i++) {
        if (!vis[G[u][i].first]) reach_n(G[u][i].first);
    }
}

signed main() {
    n = read<int>();
    ML = read<int>();
    MD = read<int>();
    for (int i = 1, a, b, L; i <= ML; i++) {
        a = read<int>();
        b = read<int>();
        L = read<int>();
        if (a >= b) printf("W");
        G[a].push_back(make_pair(b, L));
    }
    for (int i = 1, a, b, D; i <= MD; i++) {
        a = read<int>();
        b = read<int>();
        D = read<int>();
        if (a >= b) printf("W");
        G[b].push_back(make_pair(a, -D));
    }
    reach_n(1);
    for (int i = 1; i < n; i++) G[i + 1].push_back(make_pair(i, 0));
    for (int i = 1; i <= n; i++) G[0].push_back(make_pair(i, 0));
    SPFA(0);
    SPFA(1);
    // for (int i = 1; i <= n; i++) write(pos[n], " \n"[i == n]);
    if (!vis[n]) puts("-2");
    else write(pos[n], 10);
    return 0;
}

// Create File Date : 2020-06-14