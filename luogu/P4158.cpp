/*************************************
 * problem:      P4158 [SCOI2009]粉刷匠.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-12.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

struct counter {
    int sum[50 + 3][50 + 3];
    counter() { memset(sum, 0, sizeof(sum)); }
    int count(int lineId, int l, int r) { return sum[lineId][r] - sum[lineId][l - 1]; }
    void init(int arr[50 + 3][50 + 3], int n, int m)
    {
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= m; j++) {
                sum[i][j] = sum[i][j - 1] + arr[i][j];
            }
        }
    }
};

int n, m, t;
int goal[50 + 3][50 + 3];
int f[50 + 3][2500 + 3], g[50 + 3][2500 + 3][50 + 3];
int ans = 0;
counter wall;

int main()
{
    char buf[50 + 3];
    n = read<int>();
    m = read<int>();
    ans = t = read<int>();
    for (int i = 1; i <= n; i++) {
        scanf("%s", buf + 1);
        for (int j = 1; j <= m; j++) {
            goal[i][j] = buf[j] & 1;
        }
    }
    wall.init(goal, n, m);
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= min(m, t); j++) {
            for (int k = 1; k <= m; k++) {
                for (int l = j - 1; l < k; l++) {
                    g[i][j][k] = max(g[i][j][k], g[i][j - 1][l] + max(wall.count(i, l + 1, k), k - l - wall.count(i, l + 1, k)));
                }
            }
        }
    }
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= t; j++) {
            for (int k = 1; k <= min(j, m); k++) {
                f[i][j] = max(f[i][j], f[i - 1][j - k] + g[i][k][m]);
            }
        }
    }
    for (int i = 1; i <= t; i++) {
        ans = max(ans, f[n][i]);
    }
    write(ans, 10);
    return 0;
}
