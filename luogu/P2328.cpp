/*************************************
 * @problem:      P2328 [SCOI2005]超级格雷码.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2019-12-23.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

char ch[36 + 3] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
int n, B, limit;
int now[16 + 3], diff[16 + 3];

int main()
{
    n = read<int>();
    B = read<int>();
    limit = pow(B, n);
    for (int i = 1; i <= n; i++) now[i] = 0, diff[i] = 1;
    for (int i = 1; i <= limit; i++) {
        for (int i = 1; i <= n; i++) putchar(ch[now[i]]);
        putchar(10);
        for (int i = n; i >= 1; i--) {
            now[i] += diff[i];
            if (now[i] == B || now[i] == -1) {
                now[i] -= diff[i];
                diff[i] = -diff[i];
            } else break;
        }
    }
    return 0;
}