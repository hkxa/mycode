//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      [IOI2011]Race.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-11.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 988244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

const int N = 200007;

int n, k, Q;
int vis[N];
int cnt;
struct Info {
    int dis, cnt;
} h[N];
int to[N << 1], val[N << 1], nxt[N << 1], head[N];
int bucket[1000007], ans = N;

int siz[N], root, rootMax;
void basicFindRoot_getSiz(int u) {
    siz[u] = 1;
    vis[u] = true;
    for (int e = head[u]; ~e; e = nxt[e]) {
        if (!vis[to[e]]) {
            basicFindRoot_getSiz(to[e]);
            siz[u] += siz[to[e]];
        }
    }
    vis[u] = false;
}

void basicFindRoot_getRoot(int u, int si) {
    int tMax = 1;
    vis[u] = true;
    for (int e = head[u]; ~e; e = nxt[e]) {
        const int &v = to[e];
        if (!vis[v]) {
            basicFindRoot_getRoot(v, si);
            tMax = max(tMax, siz[v]);
        }
    }
    tMax = max(tMax, si - siz[u]);
    if (tMax < rootMax) {
        root = u;
        rootMax = tMax;
    }
    vis[u] = false;
}

inline void findRoot(int u) {
    rootMax = n + 1;
    // tc = clock();
    basicFindRoot_getSiz(u);
    // tGS += clock() - tc;
    // tc = clock();
    basicFindRoot_getRoot(u, siz[u]);
    // tGR += clock() - tc;
}

void getDist(int u, int dis, int ecnt) {
	if (dis > k) return;
    h[cnt++] = (Info){dis, ecnt};
    // if (h[cnt - 1].dis < 0)
    //     printf("Amazing but true : you set dis(j = %d) to negative : %d\n", cnt - 1, h[cnt - 1].dis);
    vis[u] = true;
    for (int e = head[u]; ~e; e = nxt[e]) {
        // if (to[e] > n)
        //     printf("How could it be? I mean, how could the to[e] > n? u = %d, e = %d, to[e] = %d\n", u, e, to[e]);
        if (!vis[to[e]]) getDist(to[e], dis + val[e], ecnt + 1);
    }
    vis[u] = false;
}

void calcAnswer(int u) {	
	bucket[0] = 0;
    cnt = 0;
    for (int e = head[u]; ~e; e = nxt[e]) {
		if (!vis[to[e]]) {
            int RecentCnt = cnt;
            // if (to[e] > n)
            //     printf("How could it be? I mean, how could the to[e] > n? u = %d, e = %d, to[e] = %d\n", u, e, to[e]);
            getDist(to[e], val[e], 1);
            // printf("RecentCnt(%d) ~ cnt(%d)\n", RecentCnt, cnt);
            for (int j = RecentCnt; j < cnt; j++) {
                // if (ans != 5 && bucket[k - h[j].dis] + h[j].cnt == 5) {
                //     printf("EMM.. So here is it : h[j] = {%d, %d}\n", h[j].dis, h[j].cnt);
                // }
                ans = min(ans, bucket[k - h[j].dis] + h[j].cnt);
            }
            for (int j = RecentCnt; j < cnt; j++) 
                bucket[h[j].dis] = min(bucket[h[j].dis], h[j].cnt);
            // for (int j = RecentCnt; j < cnt; j++) 
            //     printf("bucket[%d] => %d\n", h[j].dis, bucket[h[j].dis]);
        }
	}
    // printf("calcAnswer : %d : ans => %d\n", u, ans);
	for (int i = 0; i < cnt; i++) bucket[h[i].dis] = 0x3fffffff;
}

void solve(int u) {
    // printf("solve(%d)\n", u);
    vis[u] = true;
    calcAnswer(u);
    for (int e = head[u]; ~e; e = nxt[e]) {
        if (!vis[to[e]]) {
            findRoot(to[e]);
            solve(root);
        }
    }
    vis[u] = false;
}

signed main() {
    n = read<int>();
    k = read<int>();
    memarr(n, -1, head);
    memarr(k, 0x3fffffff, bucket);
    for (int i = 1, u, v, w; i < n; i++) {
        u = read<int>() + 1;
        v = read<int>() + 1;
        w = read<int>();
        to[++cnt] = v; val[cnt] = w; nxt[cnt] = head[u]; head[u] = cnt;
        to[++cnt] = u; val[cnt] = w; nxt[cnt] = head[v]; head[v] = cnt;
    }
    findRoot(1);
    solve(root);
    if (ans == N) puts("-1");
    else write(ans, 10);
    return 0;
}

// Create File Date : 2020-06-11

/*
7 10
1 6 13 
6 3 9 
3 5 7 
4 1 3 
2 4 20 
4 7 2 
1 2 3 4 5 6 7 8 9 10

*/