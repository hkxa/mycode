/*************************************
 * @problem:      Complete Mirror.
 * @author:       brealid.
 * @time:         2021-02-25.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 1e5 + 7;

int n, fa[N], siz[N], max_part[N];
vector<int> g[N];

void Answer_And_Exit(int ans) {
    kout << ans << '\n';
    exit(0);
}

bool BruteForceCheck(int root) {
    queue<int> q;
    q.push(root);
    fa[root] = 0;
    while (!q.empty()) {
        queue<int> now;
        int dgr = g[q.front()].size(), u;
        for (; !q.empty(); q.pop()) {
            if ((int)g[u = q.front()].size() != dgr) return false;
            for (uint32 i = 0; i < g[u].size(); ++i) {
                int v = g[u][i];
                if (v != fa[u]) {
                    fa[v] = u;
                    now.push(v);
                }
            }
        }
        swap(q, now);
    }
    return true;
}

void CalcSize(int u, int father) {
    siz[u] = 1;
    for (uint32 i = 0; i < g[u].size(); ++i) {
        int v = g[u][i];
        if (v != father) {
            CalcSize(v, u);
            siz[u] += siz[v];
            if (siz[v] > max_part[u]) max_part[u] = siz[v];
        }
    }
    max_part[u] = max(max_part[u], n - siz[u]);
}

int TestWeightHeart() {
    CalcSize(1, 0);
    int WeightHeart = 1, MinDivision = *min_element(max_part + 1, max_part + n + 1);
    for (int i = 1; i <= n; ++i)
        if (MinDivision == max_part[i]) {
            if (BruteForceCheck(i)) Answer_And_Exit(i);
            else WeightHeart = i;
        }
    return WeightHeart;
}

int DistinctNode;
int IsChain(int u, int fa) {
    if (g[u].size() > 2) return -1;
    if (g[u].size() == 1) {
        DistinctNode = u;
        return 1;
    }
    int Next = (g[u][0] == fa ? g[u][1] : g[u][0]);
    int nRet = IsChain(Next, u);
    if (~nRet) return nRet + 1;
    else return -1;
}

signed main() {
    kin >> n;
    if (n == 1) {
        kout << "1\n";
        return 0;
    }
    for (int i = 1, u, v; i < n; ++i) {
        kin >> u >> v;
        g[u].push_back(v), g[v].push_back(u);
    }
    int root = TestWeightHeart(), len_chain = 0;
    for (uint32 i = 0; i < g[root].size(); ++i) {
        int v = g[root][i];
        int len = IsChain(v, root);
        if (!~len) continue;
        if (len_chain) {
            if (len != len_chain && BruteForceCheck(DistinctNode))
                Answer_And_Exit(DistinctNode);
        } else {
            len_chain = len;
            if (BruteForceCheck(DistinctNode))
                Answer_And_Exit(DistinctNode);
        }
    }
    Answer_And_Exit(-1);
    return 0;
}