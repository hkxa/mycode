/*************************************
 * problem:      P1494 [国家集训队]小Z的袜子.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-07-28.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, m, block;
int a[500007];
int bl[500007];
long long cnt[1000007] = {0};
pair<long long, long long> res[500007];

struct ques {
    int id;
    int l, r;
    inline bool operator < (const ques &other) const {
        return bl[l] == bl[other.l] ? 
               ((bl[l] & 1) ? r < other.r : r > other.r) : 
               l < other.l;
    }
    inline void input(int thisId)
    {
        id = thisId;
        l = read<int>();
        r = read<int>();
    }
} q[500007];

template <typename I>
I gcd(I a, I b)
{
    return a == 0 ? b : gcd(b % a, a);
} 

namespace mos {
    long long nowAns = 0;
    inline void add(int pos)
    {
        cnt[a[pos]]++;
        if (cnt[a[pos]] > 1) {
            nowAns += (cnt[a[pos]] - 1) << 1;
        }
    }
    inline void del(int pos)
    {
        cnt[a[pos]]--;
        if (cnt[a[pos]] > 0) {
            nowAns -= cnt[a[pos]] << 1;
        }
    }
    inline pair<long long, long long> calcRes(long long x, long long y)
    {
        if (!x) return make_pair(0, 1);
        long long gcdd = gcd(x, y);
        return make_pair(x / gcdd, y / gcdd);
    }
}

#define Qlen(id) (q[id].r - q[id].l)

int main()
{
    n = read<int>();
    m = read<int>();
    // block = n > 233 ? 233 : n;
    block = sqrt(n);
    for (int i = 1; i <= n; i++) {
        a[i] = read<int>(); 
        bl[i] = i / block;
    } 
    for (int i = 1; i <= m; i++) {
        q[i].input(i); 
    } 
    sort(q + 1, q + m + 1);
    for (int i = q[1].l; i <= q[1].r; i++) mos::add(i);
    res[q[1].id] = mos::calcRes(mos::nowAns, (long long)Qlen(1) * (Qlen(1) + 1));
    // res[1] = mos::calcRes(mos::nowAns, Qlen(1));
    int xl = q[1].l, xr = q[1].r;
    for (int i = 2; i <= m; i++) {
        int &l = q[i].l, &r = q[i].r;
        if (l == r) res[q[i].id] = make_pair(0, 1);
        while (xl < l) mos::del(xl++);
        while (xl > l) mos::add(--xl);
        while (xr < r) mos::add(++xr);
        while (xr > r) mos::del(xr--);
        res[q[i].id] = mos::calcRes(mos::nowAns, (long long)(r - l + 1) * (r - l));
    }
    for (int i = 1; i <= m; i++) {
        write(res[i].first, '/');
        write(res[i].second, '\n');
        // printf("%lld/%lld\n", res[i].first, res[i].second);
    }
    return 0;
} 