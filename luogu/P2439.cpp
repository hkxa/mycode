/*************************************
 * @problem:      P2439 [SDOI2005]阶梯教室设备利用.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2019-12-22.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 
// 双倍经验 P1868 数据范围不同 读入略有不同
#include <bits/stdc++.h>
using namespace std;

const int MaxN = 10000 + 7, MaxK = 30000 + 7;
int n, MaxEnd = -1;
int head[MaxK], to[MaxN], nxt[MaxN], edges_count = 0;
int f[MaxK];

void add(int u, int v)
{
    to[++edges_count] = v;
    nxt[edges_count] = head[u];
    head[u] = edges_count;
}

int main()
{
    scanf("%d", &n);
    for (int i = 1, s, t; i <= n; i++) {
        scanf("%d%d", &s, &t);
        add(s, t);
        MaxEnd = max(MaxEnd, t);
    }
    for (int i = 0; i <= MaxEnd; i++) {
        for (int e = head[i]; e; e = nxt[e]) {
            f[to[e]] = max(f[to[e]], f[i] + to[e] - i);
        }
        f[i + 1] = max(f[i + 1], f[i]);
    }
    printf("%d", f[MaxEnd]);
    return 0;
}