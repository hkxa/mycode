/*************************************
 * problem:      P2915 [USACO08NOV]��ţ�������Mixed Up Cows.
 * user ID:      63720.
 * user name:    �����Ű�.
 * time:         2019-02-27.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 
/*************************************
 * record ID:    R16722942.
 * status:       AC 100 pts.
 * time:         140 ms
 * memory:       9536 KB
 * ststus - AC:  10 blocks, 100 pts.
 * ststus - PC:  0 blocks, 0 pts.
 * ststus - WA:  0 blocks, 0 pts.
 * ststus - RE:  0 blocks, 0 pts. 
 * ststus - TLE: 0 blocks, 0 pts. 
 * ststus - MLE: 0 blocks, 0 pts. 
 * ststus - OLE: 0 blocks, 0 pts. 
*************************************/ 
#include <stdio.h>
#include <math.h>

#define ch8 "%c" 
#define i16 "%d"
#define i32 "%ld"
#define i64 "%lld"
#define ui32 "%u"
#define ui64 "%llu"
#define flt "%f"
#define db "%lf"
#define ldb "%Lf"
#define read(format, x) scanf(format, &x)
#define write(format, x) printf(format, x)

long int n, s[21] = {0};
long int K;
long long f[21][65536 + 7] = {0};

int main()
{
    read(i32, n);
    read(i32, K);
    for (int i = 1; i <= n; i++) {
        read(i32, s[i]);
        f[i][1 << (i - 1)] = 1;
    }
    for (int i = 0; i <= (1 << n) - 1; i++) {
        for (int j = 1; j <= n; j++) {
            if (i & (1 << (j - 1))) {
                for (int k = 1; k <= n; k++) {
                    if ((i | (1 << (k - 1))) != i && abs(s[j] - s[k]) > K) {
                        f[k][i | (1 << (k - 1))] += f[j][i];
					}
                }
            }
        }
    }
    long long ans = 0;
    for (int i = 1; i <= n; i++) {
        ans += f[i][(1 << n) - 1];
    }
    write(i64, ans);
    return 0;
}
