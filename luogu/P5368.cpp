// 关于 0 的特判 重要!!!
/*************************************
 * @problem:      [PKUSC2018]真实排名.
 * @author:       brealid.
 * @time:         2021-02-01.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 1e5 + 7, P = 998244353;

struct comb {
    int64 qpow(int64 a, int n) {
        int64 ret(1);
        while (n) {
            if (n & 1) ret = ret * a % P;
            a = a * a % P;
            n >>= 1;
        }
        return ret;
    }
    int64 fac[N], ifac[N];
    comb(int maxx) {
        fac[0] = ifac[0] = 1;
        for (int i = 1; i <= maxx; ++i) fac[i] = fac[i - 1] * i % P;
        ifac[maxx] = qpow(fac[maxx], P - 2);
        for (int i = maxx; i > 1; --i) ifac[i - 1] = ifac[i] * i % P;
    }
    int64 operator () (int n, int m) {
        if (n < m || m < 0) return 0;
        return fac[n] * ifac[m] % P * ifac[n - m] % P;
    }
};

int n, k;
int a[N];
int sorted_a[N];

int count_lt(int a) {
    return lower_bound(sorted_a + 1, sorted_a + n + 1, a) - sorted_a - 1;
}

int count_ge(int a) {
    return sorted_a + n + 1 - lower_bound(sorted_a + 1, sorted_a + n + 1, a);
}

signed main() {
    kin >> n >> k;
    comb C(n);
    for (int i = 1; i <= n; ++i) {
        kin >> a[i];
        sorted_a[i] = a[i];
    }
    sort(sorted_a + 1, sorted_a + n + 1);
    for (int i = 1, x; i <= n; ++i) {
        if (a[i] == 0) {
            kout << C(n, k) << '\n';
            continue;
        }
        x = count_lt((a[i] + 1) >> 1) + count_ge(a[i]);
        int64 ans1 = C(x - 1, k);
        x = n - (count_lt(a[i]) + count_ge(2 * a[i]));
        int64 ans2 = C(n - x, k - x);
        kout << (ans1 + ans2) % P << '\n';
    }
    return 0;
}