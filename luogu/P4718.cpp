//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      【模板】Pollard-Rho算法.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-17.
 * @language:     C++.
*************************************/ 
#pragma GCC optimize(3)
#pragma GCC optimize("Ofast")
#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

int T;
int64 n;

namespace zymath {
    inline int64 qmul(int64 a, int64 b, int64 p) {
#ifdef ONLINE_JUDGE 
        return (__int128)a * b % p;
#endif
        int64 ret = 0;
        while (b) {
            if (b & 1) if ((ret += a) >= p) ret -= p;
            if ((a <<= 1) >= p) a -= p; 
            b >>= 1;
        }
        return ret;
    }
    inline int64 qpow(int64 a, int64 b, int64 p) {
        int64 ret = 1;
        while (b) {
            if (b & 1) ret = qmul(ret, a, p);
            a = qmul(a, a, p);
            b >>= 1;
        }
        return ret;
    }
    inline int64 gcd(int64 a, int64 b) {
        if (!a || !b) return a | b;
        int t = __builtin_ctzll(a | b);
        a >>= __builtin_ctzll(a);
        do {
            b >>= __builtin_ctzll(b) ;
            if (a > b) swap(a, b);
            b -= a;
        } while(b);
        return a << t;
    }
}

namespace NumberTheory {
    namespace Miller_Rabin_Prime {
        const int prime[] = {2, 61, 137};
        const size_t prime_cnt = sizeof(prime) / sizeof(int);
    }
}
class Miller_Rabin {
  public:
    inline bool operator () (int64 a) {
        using namespace NumberTheory::Miller_Rabin_Prime;
        for (size_t prime_id = 0; prime_id < prime_cnt; prime_id++)
            if (a == prime[prime_id]) return true;
        if (a < 2 || !(a & 1)) return false;
        int64 val = a - 1, cnt2 = 0;
        while (!(val & 1)) {
            val >>= 1;
            cnt2++;
        }
        for (size_t prime_id = 0; prime_id < prime_cnt; prime_id++) {
            int64 now = zymath::qpow(prime[prime_id] % a, val, a), newer_value;
            for (int i = 0; i < cnt2; i++) {
                newer_value = zymath::qmul(now, now, a);
                if (newer_value == 1 && now != 1 && now != a - 1) return false;
                now = newer_value;
            }
            if (now != 1) return false;
        }
        return true;
    }
};
class Pollard_Rho {
  private:
    Miller_Rabin test_prime;
    mt19937 rnd;
    inline int64 gen_next(int64 u, int64 c, int64 P) {
        return (zymath::qmul(u, u, P) + c) % P;
    }
    inline int64 get_a_factor(int64 a) {
        if (test_prime(a)) return a;
        while (true) {
            int64 c = rnd() % a, x = rnd() % a;
            int64 u = gen_next(x, c, a), v = gen_next(x, c, a);
            // int64 u = gen_next(x, c, a), v = gen_next(gen_next(x, c, a), c, a);
            int64 now = 1;
            // printf("Key : c = %lld, x = %lld\n", c, x);
            for (int goal = 1; goal <= 16384; goal <<= 1) {
                int cnt = 0;
                while (cnt <= goal) {
                    // printf("cnt = %d, goal = %d\n", cnt, goal);
                    u = gen_next(u, c, a);
                    if (u != v) now = zymath::qmul(now, abs(u - v), a);
                    if (++cnt % 127 == 0) {
                        int64 g = zymath::gcd(now, a);
                        if (g > 1) return g;
                    }
                    // v = gen_next(gen_next(v, c, a), c, a);
                }
                int64 g = zymath::gcd(now, a);
                if (g > 1) return g;
                v = u;
            }
        }
    }
  public:
    Pollard_Rho() : rnd(chrono::steady_clock::now().time_since_epoch().count()) {}
    int64 resolve_maximum_factor(int64 a) {
        if (test_prime(a)) return a;
        if (a == 1) return 1;
        if (!(a & 1)) {
            while (!(a & 1)) a >>= 1;
            if (a == 1) return 2;
        }
        int64 ret = get_a_factor(a);
        return max(resolve_maximum_factor(a / ret), resolve_maximum_factor(ret));
    }
};

signed main() {
    Pollard_Rho pr;
    int T = read.get_int<int>();
    int64 a, ret;
    while (T--) {
        read >> a;
        ret = pr.resolve_maximum_factor(a);
        if (ret == a) puts("Prime");
        else write << ret << '\n';
    }
    return 0;
}