//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      [USACO06NOV] Round Numbers S.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-21.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 998244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? Module(w - P) : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

int l, r;
int f[32 + 3][32 + 3][32 + 3][2];
// f[第 bit 位][count(0)][count(1)]

int solve(int n) {
    memset(f, 0, sizeof(f));
    for (int i = 31; i >= 0; i--) 
        if (n & (1 << i)) {
            f[i][0][1][1] = 1;
            while (--i >= 0) f[i][0][1][0] = 1;
        }
    for (int i = 31; i >= 0; i--)
        for (int j = 31; j >= 0; j--)
            for (int k = 31; k >= 0; k--)
                if (n & (1 << i)) {
                    if (j) f[i][j][k][0] += f[i + 1][j - 1][k][0] + f[i + 1][j - 1][k][1];
                    if (k) f[i][j][k][0] += f[i + 1][j][k - 1][0];
                    if (k) f[i][j][k][1] += f[i + 1][j][k - 1][1];
                    // if (f[i][j][k][0] || f[i][j][k][1]) printf("bit %d on : f[%d][%d][%d][%d] = %d, f[%d][%d][%d][%d] = %d\n", i, i, j, k, 0, f[i][j][k][0], i, j, k, 1, f[i][j][k][1]);
                } else {
                    if (j) f[i][j][k][0] += f[i + 1][j - 1][k][0];
                    if (k) f[i][j][k][0] += f[i + 1][j][k - 1][0];
                    if (j) f[i][j][k][1] += f[i + 1][j - 1][k][1];
                    // if (f[i][j][k][0] || f[i][j][k][1]) printf("bit %d off: f[%d][%d][%d][%d] = %d, f[%d][%d][%d][%d] = %d\n", i, i, j, k, 0, f[i][j][k][0], i, j, k, 1, f[i][j][k][1]);
                }
    int ans = 0;
    for (int i = 0; i <= 31; i++)
        for (int j = 0; j <= i; j++)
            ans += f[0][i][j][0] + f[0][i][j][1];
    // printf("solve(%d) = %d\n", n, ans);
    return ans;
}

signed main() {
    l = read<int>();
    r = read<int>();
    write(solve(r) - solve(l - 1), 10);
    return 0;
}

// Create File Date : 2020-06-21