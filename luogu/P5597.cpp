/*************************************
 * @problem:      P5597 【XR-4】复读.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-01-26.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

char orig_tree[2007];
int l[2007], r[2007], dep[2007], n = 1;
int ml[2007], mr[2007], cnt = 1;

int build(int p, int u)
{
    // printf("build(%d, %d)\n", p, u);
    switch (orig_tree[p]) {
        case '0' : return p;
        case '1' : 
            l[u] = ++n;
            dep[n] = dep[u] + 1;
            return build(p + 1, n);
        case '2' :
            r[u] = ++n;
            dep[n] = dep[u] + 1;
            return build(p + 1, n);
        case '3' :
            l[u] = ++n;
            dep[n] = dep[u] + 1;
            p = build(p + 1, n);
            r[u] = ++n;
            dep[n] = dep[u] + 1;
            return build(p + 1, n);
        default : 
            puts("Input Data Error!");
            return -1;
    }
}

void mid_g(int u, int *l, int *r)
{
    printf("%d : ", u);
    if (!l[u] && !r[u]) printf("no son");
    if (l[u]) printf("l.%d ", l[u]);
    if (r[u]) printf("r.%d ", r[u]);
    putchar(10);
    if (l[u]) mid_g(l[u], l, r);
    if (r[u]) mid_g(r[u], l, r);
}

int overlay(int u, int now, int goal, int set)
{
    if (!l[u] && !r[u]) return 0;
    if (now == goal) return u;
    int t, ret = 0;
    if (l[u]) {
        if (!ml[set]) ml[set] = ++cnt;
        if (t = overlay(l[u], l[now], goal, ml[set])) ret = t;
    } 
    if (r[u]) {
        if (!mr[set]) mr[set] = ++cnt;
        if (t = overlay(r[u], r[now], goal, mr[set])) ret = t;
    }
    return ret;
}
/*
       1
     2 
   3   7
 4 
5 6

            1
      2          9
    3   6     10   13
   4 5 7 8  11 12 14 15    
*/
int main()
{
    scanf("%s", orig_tree);
    dep[1] = 0;
    build(0, 1); 
    // mid_g(1, l, r);
    int ans = 0x3f3f3f3f;
    for (int i = 2, p; i <= n; i++) {
        memset(ml, 0, sizeof(ml));
        memset(mr, 0, sizeof(mr));
        p = cnt = 1;
        while (p = overlay(p, 1, i, 1));
        // printf("cnt = %d; dep[%d] = %d;\n", cnt, i, dep[i]);
        if (2 * cnt - dep[i] < ans) ans = 2 * cnt - dep[i];
    }
    write(ans - 2, 10);
    return 0;
}