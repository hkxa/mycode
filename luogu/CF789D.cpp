//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Weird journey.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-11.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

namespace against_cpp11 {
    const int N = 1e6 + 7;
    int n, m;
    vector<int> G[N];
    int dfn[N], low[N], dft;
    int64 ans, cnt_self_circle;
    bool instack[N];
    int sta[N], top;
    int fam[N], fam_cnt;
    void make_vis_tag(int u) {
        dfn[u] = true;
        for (size_t i = 0; i < G[u].size(); i++)
            if (!dfn[G[u][i]]) make_vis_tag(G[u][i]);
    }
    bool check_if_not_connected() {
        for (int i = 1; i <= n; i++) 
            if (!G[i].empty()) {
                make_vis_tag(i);
                break;
            }
        for (int i = 1; i <= n; i++) {
            if (!dfn[i] && !G[i].empty()) return true;
            else dfn[i] = 0;
        }
        return false;
    }
    void dfs(int u, int fa) {
        low[u] = dfn[u] = ++dft;
        sta[++top] = u;
        instack[u] = true;
        for (size_t i = 0; i < G[u].size(); i++) {
            int &v = G[u][i];
            if (v == fa) continue;
            if (!dfn[v]) {
                dfs(v, u);
                low[u] = min(low[u], low[v]);
            } else if (instack[u]) {
                low[u] = min(low[u], dfn[v]);
            }
        }
        if (low[u] == dfn[u]) {
            ++fam_cnt;
            while (sta[top] != u) {
                instack[sta[top]] = false;
                fam[sta[top--]] = fam_cnt;
            }
            instack[sta[top]] = false;
            fam[sta[top--]] = fam_cnt;
        }
    }
    signed main() {
        read >> n >> m;
        if (m < 2) {
            puts("0");
            return 0;
        }
        for (int i = 1, u, v; i <= m; i++) {
            read >> u >> v;
            if (u != v) {
                G[u].push_back(v);
                G[v].push_back(u);
            } else {
                G[u].push_back(u);
                cnt_self_circle++;
            }
        }
        if (check_if_not_connected()) {
            puts("0");
            return 0;
        }
        // ans = m * (m - 1) / 2;
        // dfs(1, 0);
        for (int i = 1; i <= n; i++) {
            int64 siz = G[i].size();
            for (size_t j = 0; j < G[i].size(); j++)
                if (G[i][j] == i) siz--;
            ans += siz * (siz - 1) / 2;
        }
        ans += cnt_self_circle * (cnt_self_circle - 1) / 2 + cnt_self_circle * (m - cnt_self_circle);
        write << ans << '\n';
        return 0;
    }
}

signed main() { return against_cpp11::main(); }