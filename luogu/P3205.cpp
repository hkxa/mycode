/*************************************
 * problem:      P3205 [HNOI2010]合唱队.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-03.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;
int a[1007];
int l[1007][1007], r[1007][1007];
/**
 * l[i][j] [i, j]区间的答案(最后一个进队的是i)
 * r[i][j] [i, j]区间的答案(最后一个进队的是j)
 */

#define P 19650827

int main()
{
    n = read<int>();
    memset(l, 0, sizeof(l));
    memset(r, 0, sizeof(r));
    for (int i = 1; i <= n; i++) {
        a[i] = read<int>();
        l[i][i] = 1;
    }
    for (int len = 1; len < n; len++) {
        for (int i = 1, j = 1 + len; j <= n; i++, j++) {
            if (a[i] < a[i + 1]) l[i][j] += l[i + 1][j];
            if (a[i] < a[j]) l[i][j] += r[i + 1][j];
            if (a[j] > a[j - 1]) r[i][j] += r[i][j - 1];
            if (a[i] < a[j]) r[i][j] += l[i][j - 1];
            l[i][j] %= P;
            r[i][j] %= P;
        }
    }
    write((l[1][n] + r[1][n]) % P);
    return 0;
}