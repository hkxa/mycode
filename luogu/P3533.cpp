//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Rendezvous 会合.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-09.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 988244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

const int N = 500000 + 7;

// [Shape_of_the_graph] 基环树森林
int n, q;
int a[N], f[N][20];
int dep[N];
int vis[N];     // vis : 所处(非强)联通子图编号
int circle[N];  // circle : 是否为[圈]节点 & 所处(非强)联通子图[圈]编号
int cirDis[N];  // cirDis : 所处[圈]中的 dist [from_any_point]
int cirSiz[N];  // cirSiz : [圈]的大小
int top[N];     // top : 隶属于的[圈]节点
int cnt = 0;    // cnt : [总](非强)联通子图编号
vector<int> son[N];

void Presolve(int u, const int tag, const int From) {
    // printf("Presolve(%d)\n", u);
    if (!circle[u]) f[u][0] = a[u];
    for (int k = 1; k < 20; k++)
        f[u][k] = f[f[u][k - 1]][k - 1];
    vis[u] = tag;
    dep[u] = dep[f[u][0]] + 1;
    top[u] = From;
    for (size_t i = 0; i < son[u].size(); i++)
        if (!circle[son[u][i]]) Presolve(son[u][i], tag, From);
}

void MarkCircle(int u, const int tag) {
    if (circle[u]) return;
    else if (vis[u]) {
        while (!circle[u]) {
            // printf("!!! circle node %d\n", u);
            circle[u] = tag;
            u = a[u];
            Presolve(u, tag, u);
        }
    } else {
        vis[u] = tag;
        MarkCircle(a[u], tag);
    }
}

void GetCirInfo(int u) {
    if (cirDis[a[u]]) return;
    cirDis[a[u]] = cirDis[u] + 1;
    cirSiz[circle[u]]++;
    GetCirInfo(a[u]);
}

int jump(int &u, int k) {
    for (int i = 0; i < 20; i++)
        if (k & (1 << i))
            u = f[u][i];
    return k;
}

pair<int, int> RoadToLca(int a, int b) {
    int ar = 0, br = 0;
    if (dep[a] > dep[b]) ar = jump(a, dep[a] - dep[b]);
    else if (dep[a] < dep[b]) br = jump(b, dep[b] - dep[a]);
    if (a == b) return make_pair(ar, br);
    for (int k = 19; k >= 0; k--)
        if (f[a][k] != f[b][k]) {
            a = f[a][k];
            b = f[b][k];
            ar += (1 << k);
            br += (1 << k);
        }
    return make_pair(ar + 1, br + 1);
}

signed main() {
    n = read<int>();
    q = read<int>();
    for (register int i = 1; i <= n; i++) {
        a[i] = read<int>();
        son[a[i]].push_back(i);
    }
    for (register int i = 1; i <= n; i++)
        if (!vis[i]) MarkCircle(i, ++cnt);
    for (register int i = 1; i <= n; i++)
        if (circle[i] && !cirDis[i]) GetCirInfo(i);
    for (register int i = 1, a, b; i <= q; i++) {
        a = read<int>();
        b = read<int>();
        if (vis[a] != vis[b]) {
            puts("-1 -1");
            continue;
        }
        if (top[a] == top[b]) {
            pair<int, int> ret = RoadToLca(a, b);
            printf("%d %d\n", ret.first, ret.second);
            continue;
        }
        register int ar = dep[a] - 1, br = dep[b] - 1;
        register int fa = top[a], fb = top[b];
        register int road1 = abs(cirDis[fa] - cirDis[fb]), road2 = cirSiz[circle[fa]] - road1;
        if (cirDis[fa] > cirDis[fb]) {
            if (br + road1 < ar + road2) {
                printf("%d %d\n", ar, br + road1);
            } else if (br + road1 > ar + road2) {
                printf("%d %d\n", ar + road2, br);
            } else if (br > ar) {
                printf("%d %d\n", ar, br + road1);
            } else if (br < ar) {
                printf("%d %d\n", ar + road2, br);
            } else {
                printf("%d %d\n", max(ar, br + road1), min(ar, br + road1));
            } 
        } else {
            if (br + road2 < ar + road1) {
                printf("%d %d\n", ar, br + road2);
            } else if (br + road2 > ar + road1) {
                printf("%d %d\n", ar + road1, br);
            } else if (br > ar) {
                printf("%d %d\n", ar, br + road2);
            } else if (br < ar) {
                printf("%d %d\n", ar + road1, br);
            } else {
                printf("%d %d\n", max(ar, br + road2), min(ar, br + road2));
            } 
        }
    }
    return 0;
}

// Create File Date : 2020-06-09