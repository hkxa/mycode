#include <bits/stdc++.h>
using namespace std;

const int N = 1007, M = 2e5 + 7;
int n, m;
int f[N][N];
int g[M];
int ans[M];

signed main() {
    scanf("%d%d", &n, &m);
    for (int i = 1, u, v; i <= m; ++i)
        scanf("%d%d", &u, &v), f[u][v] = i;
    for (int k = n, now_f; k >= 1; --k) {
        for (int i = k + 1; i <= n; ++i) ++g[min(f[k][i], f[i][k])];
        for (int i = 1; i <= n; ++i) {
            if (!(now_f = f[i][k])) continue;
            if (i > k)
                for (int j = 1; j < k; ++j)
                    f[i][j] = max(f[i][j], min(now_f, f[k][j]));
            else
                for (int j = 1; j <= n; ++j)
                    f[i][j] = max(f[i][j], min(now_f, f[k][j]));
        }
    }
    ans[m + 1] = n;
    for (int i = m; i >= 1; --i)
        ans[i] = ans[i + 1] + g[i];
    for (int i = 1; i <= m + 1; ++i)
        printf("%d%c", ans[i], " \n"[i == m + 1]);
    return 0;
}

/*
f(u, G) 求的是：G 中与 u 通过 >i 的点可以与 u 互达的点 i 个数
*/