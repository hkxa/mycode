/*************************************
 * problem:      P1627 [CQOI2009]中位数.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-06-09.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

long long n, b;
long long a[100007];
long long baseL[200027], baseR[200027];

#define l(dI) baseL[(dI) + 100007]
#define r(dI) baseR[(dI) + 100007]

long long getDiff(long long base)
{
    if (base < b) return -1;
    else if (base > b) return 1;
    else return 0;
}

long long sum[100007] = {0};

int main()
{
    n = read<long long>();
    b = read<long long>();
    long long posMid;
    for (int i = 1; i <= n; i++) {
        a[i] = getDiff(read<long long>());
        if (!a[i]) posMid = i;
        // printf("%lld ", a[i]);
    }
    // printf("posMid = %d.\n", posMid);
    // puts("");
    long long ans(0);
    // for (long long i = posMid - 1; i >= 1; i--) {
    //     sum[i] = sum[i + 1] + a[i];
    // }
    // for (long long i = posMid + 1; i <= n; i++) {
    //     sum[i] = sum[i - 1] + a[i];
    // }
    for (long long i = 1; i <= n; i++) {
        sum[i] = sum[i - 1] + a[i];
    }
    for (long long i = posMid - 1; i >= 0; i--) {
        l(sum[i])++;
    }
    for (long long i = posMid; i <= n; i++) {
        ans += l(sum[i]);
    }
    write(ans, 10);
    return 0;
}