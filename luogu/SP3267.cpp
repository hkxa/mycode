/*************************************
 * problem:      SP3267 DQUERY - D-query.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-03-10.
 * language:     C++.
 * upload place: Luogu.
*************************************/
/*************************************
 * special_type: 双倍经验 
 * problem:      P1972 [SDOI2009]HH的项链.
*************************************/ 
/*************************************
 * record ID:    R17080288.
 * time:         100 ms
 * memory:       18432 KB
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

int n, a[500007], m, block;
int las[1000007] = {0};
int ans[500007];

class HH_necklace_tree {
  private:
  	int value[500007];
  	inline int lowbit(int num)
  	{
  		return num & -num;
	}
	inline int sum_1toPos(int pos)
	{
		int res = 0;
		while (pos >= 1) {
			res += value[pos];
			pos -= lowbit(pos);	
		}
		return res;
	}
  public:
	HH_necklace_tree() {
		memset(value, 0, sizeof(value));
	}
	inline void add(int pos, int diff)
	{
//		printf("Update pos %d with diff %d.\n", pos, diff);
		while (pos <= n) {
			value[pos] += diff;
			pos += lowbit(pos);	
		}
	}
	inline int sum(int l, int r)
	{
		return sum_1toPos(r) - sum_1toPos(l - 1);
	}
} tree;

struct ques {
	int id;
	int l, r;
	bool operator < (const ques &other) const {
		return r ^ other.r ? r < other.r : l < other.l;
	}
	void input(int thisId)
	{
		id = thisId;
		l = read<int>();
		r = read<int>();
	}
} q[500007];

int main()
{
	n = read<int>();
	for (int i = 1; i <= n; i++) {
		a[i] = read<int>(); 
	} 
	m = read<int>();
	for (int i = 1; i <= m; i++) {
		q[i].input(i); 
	} 
	sort(q + 1, q + m + 1);
//	for (int i = 1; i <= m; i++) {
//		printf("%d-%d-%d.\n", q[i].id, q[i].l, q[i].r);
//	} 
	int i = 1;
	int xr = 1;
	while (xr <= n) {
		if (las[a[xr]]) tree.add(las[a[xr]], -1);
		tree.add(xr, 1);
		las[a[xr]] = xr;
		while (q[i].r == xr) {
			ans[q[i].id] = tree.sum(q[i].l, q[i].r);
//			printf("get %d\'s ans %d when %d.\n", q[i].id, ans[q[i].id], xr);
			i++;
		}
		xr++;
	}
	for (int i = 1; i <= m; i++) {
		write(ans[i]);
		putchar(10);
	}
	return 0;
} 
