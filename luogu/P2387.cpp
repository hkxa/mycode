/*************************************
 * @problem:      [NOI2014] 魔法森林.
 * @author:       brealid.
 * @time:         2021-02-04.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 5e4 + 7;

int n, m;

struct UnionFindSet {
    int fa[N + 5];
    UnionFindSet() {
        memset(fa, -1, sizeof(fa));
    }
    // 重置清零
    void reset(int n = sizeof(fa) / sizeof(int)) {
        memset(fa, -1, n * sizeof(int));
    }
    // 寻找 u 的祖先
    int find(int u) {
        return fa[u] < 0 ? u : fa[u] = find(fa[u]);
    }
    // 获取 u 所属的连通块的大小
    inline int get_size(int u) {
        return -fa[find(u)];
    }
    // 判断两个节点是否处于一棵树中
    inline bool is_family(int u, int v) {
        return find(u) == find(v);
    }
    // 合并两棵树，成功返回 true，已经在同一棵树中了返回 false
    inline bool merge(int u, int v) {
        u = find(u), v = find(v);
        if (u == v) return false;
        if (fa[u] > fa[v]) swap(u, v);
        fa[u] += fa[v];
        fa[v] = u;
        return true;
    }
} ufs;

struct Edge {
    int u, v, a, b;
} e[N << 1];

int ans = 1e9;

bool check(int lim_a, int lim_b) {
    ufs.reset(n + 1);
    for (int i = 1; i <= m; ++i) {
        if (e[i].a <= lim_a && e[i].b <= lim_b) {
            ufs.merge(e[i].u, e[i].v);
            if (ufs.is_family(1, n)) {
                ans = min(ans, lim_a + lim_b);
                return true;
            }
        }
    }
    return false;
}

int max_b;

bool check_a(int lim_a) {
    int l = 0, r = max_b;
    while (l <= r) {
        int mid = (l + r) >> 1;
        // printf("check_a: checking (%d, %d)\n", lim_a, mid);
        if (check(lim_a, mid)) r = mid - 1;
        else l = mid + 1;
    }
    return check(lim_a, max_b);
}

signed main() {
    kin >> n >> m;
    int l = 0, r = 0;
    for (int i = 1; i <= m; ++i) {
        kin >> e[i].u >> e[i].v >> e[i].a >> e[i].b;
        r = max(r, e[i].a);
        max_b = max(max_b, e[i].b);
    }
    while (l <= r) {
        int mid = (l + r) >> 1;
        // printf("main: checking (%d, )\n", mid);
        if (check_a(mid)) r = mid - 1;
        else l = mid + 1;
    }
    for (int i = l; i <= r + 20; ++i) check_a(i);
    kout << (ans == (int)1e9 ? -1 : ans) << '\n';
    return 0;
}