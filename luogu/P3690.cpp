//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      P3690 【模板】Link Cut Tree （动态树）.
 * @user_id:      63720.
 * @user_name:    brealid.
 * @time:         2020-05-27.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;
// #define int int64

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// #define INFO_SPLAY
// #define INFO_ACCESS
// #define INFO_FINDROOT
// #define INFO_MAKEROOT
// #define INFO_PREFER
// #define INFO_LINK
// #define INFO_CUT
// #define INFO_OPTION
// #define INFO_AFTER_DEALING

const int N = 100000 + 7;

#define lc son[u][0]
#define rc son[u][1]

int v[N], f[N], s[N];
int son[N][2];
bool rev[N];

inline bool isroot(int);        // [splay 中] 判断一个节点是否为 root
inline bool which(int);         // [splay 中] 判断一个节点是父亲节点的哪个孩子
inline void reverse(int);       // [splay 中] 懒标记地翻转左右孩子
inline void pushdown(int);      // [splay 中] 下传翻转子树的懒标记
inline void pushdown_all(int);  // [splay 中] 对 splay 的根节点到 u 的所有节点进行 pushdown 操作
inline void pushup(int);        // [splay 中] 更新 s[u] (splay 子树的 xor 和)
inline void rotate(int);        // [splay 中] 旋转操作
inline void splay(int);         // [splay 中] 使节点成为所在 splay 的根
inline void access(int);        // [原树 中] 使原树根节点到 u 的路径成为 Preferred Path
inline void makeroot(int);      // [原树 中] 使节点成为所在原树的根
inline int findroot(int);       // [原树 中] 返回所在原树的根
inline void prefer(int, int);   // [原树 中] 使原树 u 到 v 的路径成为 Preferred Path
inline void link(int, int);     // [原树 中] 若 u, v 尚未连接，连接 u, v
inline void cut(int, int);      // [原树 中] 若 u, v 已有连接，断开 u, v

int n, m;
int opt, x, y;

inline bool isroot(int u) { return u != son[f[u]][0] && u != son[f[u]][1]; }
inline bool which(int u) { return u == son[f[u]][1]; }
inline void reverse(int u) { swap(lc, rc); rev[u] ^= 1; }
inline void pushup(int u) { s[u] = s[lc] ^ s[rc] ^ v[u]; }
inline void pushdown(int u) { if (rev[u]) { reverse(lc); reverse(rc); rev[u] = 0; } }
inline void pushdown_all(int u) { if (!isroot(u)) { pushdown_all(f[u]); } pushdown(u); }

inline void rotate(int u) {
    int v = f[u], w = f[v], d = which(u);
    if (!isroot(v)) son[w][which(v)] = u;
    f[son[u][!d]] = v;
    son[v][d] = son[u][!d];
    son[u][!d] = v;
    f[u] = w;
    f[v] = u;
    pushup(v);
    pushup(u);
}

inline void splay(int u) {
#ifdef INFO_SPLAY
    for (int i = 1; i <= n; i++) printf("{s = %d, lc = %d, rc = %d} %c", s[i], son[i][0], son[i][1], " \n"[i == n]);
    printf("splay(%d)\n", u);
#endif
    pushdown_all(u);
    for (int v = f[u]; !isroot(u); v = f[u]) {
        if (!isroot(v)) rotate(which(u) ^ which(v) ? u : v);
        rotate(u);
    }
    pushup(u);
}
inline void access(int u) { 
#ifdef INFO_ACCESS
    for (int i = 1; i <= n; i++) printf("{s = %d, lc = %d, rc = %d} %c", s[i], son[i][0], son[i][1], " \n"[i == n]);
    printf("access(%d)\n", u);
#endif
    for (int v = 0; u; u = f[v = u]) { 
        splay(u); 
        rc = v; 
        pushup(u); 
    } 
}

inline int findroot(int u) { 
#ifdef INFO_FINDROOT
    for (int i = 1; i <= n; i++) printf("{s = %d, lc = %d, rc = %d} %c", s[i], son[i][0], son[i][1], " \n"[i == n]);
    printf("findroot(%d)\n", u);
#endif
    access(u); 
    splay(u); 
    while (lc) { 
        pushdown(u); 
        u = lc; 
    } 
    splay(u); 
    return u; 
}

inline void makeroot(int u) { 
#ifdef INFO_MAKEROOT
    for (int i = 1; i <= n; i++) printf("{s = %d, lc = %d, rc = %d} %c", s[i], son[i][0], son[i][1], " \n"[i == n]);
    printf("makeroot(%d)\n", u); 
#endif
    access(u); 
    splay(u); 
    reverse(u); 
}

inline void prefer(int u, int v) { 
#ifdef INFO_PREFER
    printf("prefer(%d, %d)\nORIGINAL : ", u, v); 
    for (int i = 1; i <= n; i++) printf("{s = %d, lc = %d, rc = %d} %c", s[i], son[i][0], son[i][1], " \n"[i == n]);
#endif
    makeroot(u); 
#ifdef INFO_PREFER
    printf("after MAKEROOT : ");
    for (int i = 1; i <= n; i++) printf("{s = %d, lc = %d, rc = %d} %c", s[i], son[i][0], son[i][1], " \n"[i == n]);
#endif
    access(v); 
#ifdef INFO_PREFER
    printf("after ACCESS : ");
    for (int i = 1; i <= n; i++) printf("{s = %d, lc = %d, rc = %d} %c", s[i], son[i][0], son[i][1], " \n"[i == n]);
#endif
    splay(v);
#ifdef INFO_PREFER
    printf("after SPLAY : ");
    for (int i = 1; i <= n; i++) printf("{s = %d, lc = %d, rc = %d} %c", s[i], son[i][0], son[i][1], " \n"[i == n]);
#endif
}

inline void link(int u, int v) {
#ifdef INFO_LINK
    for (int i = 1; i <= n; i++) printf("{s = %d, lc = %d, rc = %d} %c", s[i], son[i][0], son[i][1], " \n"[i == n]);
    printf("link(%d, %d)\n", u, v); 
#endif
    makeroot(u); 
    if (findroot(v) != u) f[u] = v; 
}

inline void cut(int u, int v) { 
#ifdef INFO_CUT
    for (int i = 1; i <= n; i++) printf("{s = %d, lc = %d, rc = %d} %c", s[i], son[i][0], son[i][1], " \n"[i == n]);
    printf("cut(%d, %d)\n", u, v);
#endif
    makeroot(u); 
    if (findroot(v) == u && f[v] == u && !lc) { 
        f[v] = rc = 0; pushup(u); 
    } 
}

signed main()
{
    n = read<int>();
    m = read<int>();
    for (int i = 1; i <= n; i++) s[i] = v[i] = read<int>();
    while (m--) {
        opt = read<int>();
        x = read<int>();
        y = read<int>();
#ifdef INFO_OPTION
        printf("option = %d, augument = %d, %d\n", opt, x, y);
#endif
        switch (opt) {
            case 0 : prefer(x, y); write(s[y], 10); break;
            case 1 : link(x, y); break;
            case 2 : cut(x, y); break;
            case 3 : splay(x); v[x] = y; pushup(x); break;
            default: break;
        }
#ifdef INFO_AFTER_DEALING
        for (int i = 1; i <= n; i++) printf("{s = %d, lc = %d, rc = %d} %c", s[i], son[i][0], son[i][1], " \n"[i == n]);
#endif
    }
    return 0;
}