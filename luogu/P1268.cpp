/*************************************
 * @problem:      id_name.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2019-mm-dd.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;
int dis[31][31];
int ans = 0, mn;

int main()
{
    while (n = read<int>()) {
        for (int i = 1; i <= n; i++) {
            for (int j = i + 1; j <= n; j++) {
                dis[i][j] = dis[j][i] = read<int>();
            }
        }
        ans = dis[1][2];
        // printf("ans += %d.\n", dis[1][2]);
        for (int i = 3; i <= n; i++) {
            mn = (dis[1][i] + dis[2][i] - dis[1][2]) / 2;
            for (int j = 1; j < i; j++) {
                for (int k = j + 1; k < i; k++) {
                    if (mn > (dis[j][i] + dis[k][i] - dis[j][k]) / 2) {
                        mn = (dis[j][i] + dis[k][i] - dis[j][k]) / 2; 
                    }
                }
            }
            ans += mn;
            // printf("ans += %d.\n", mn);
        }
        write(ans, 10);
    }
    return 0;
}