/*************************************
 * problem:      P1119 灾后重建.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-06.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, m;
int t[207];
int G[207][207];

#define relax(u, v, k) (G[u][v] = min(G[u][v], G[u][k] + G[k][v]))
void floyd(int k)
{
    // printf("floyd(%d)\n", k);
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            relax(i, j, k);
        }
    }
}

int main()
{
    memset(G, 0x3f, sizeof(G));
    n = read<int>();
    m = read<int>();
    for (int i = 0; i < n; i++) {
        G[i][i] = 0;
        t[i] = read<int>();
    }
    int u, v;
    while (m--) {
        u = read<int>(); 
        v = read<int>();
        G[u][v] = G[v][u] = read<int>();
    }
    int Query = read<int>(), nowT, nowK = 0;
    while (Query--) {
        u = read<int>();
        v = read<int>();
        nowT = read<int>();
        while (nowK < n && t[nowK] <= nowT) floyd(nowK++);
        if (u >= nowK || v >= nowK) puts("-1");
        else write(G[u][v] != 0x3f3f3f3f ? G[u][v] : -1, 10);
    }
    return 0;
}