/*************************************
 * problem:      P2324 [SCOI2005]��ʿ����.
 * user ID:      63720.
 * user name:    �����Ű�.
 * time:         2019-03-12.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 
/*************************************
 * record ID:    R17163590.
 * time:         633 ms
 * memory:       788 KB
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x, char nextch = ' ')
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10, 0);
    putchar((x % 10) | 48);
    if (nextch != 0) putchar(nextch);
}  

char goal[10][10] = {
	"      ",
	" 11111", 
	" 01111", 
	" 00*11", 
	" 00001", 
	" 00000", 
};

char now[10][10];

int compare()
{
	int dif = 0;
	for (int i = 1; i <= 5; i++) {
		for (int j = 1; j <= 5; j++) {
			if (goal[i][j] != now[i][j]) dif++;
		}
	}
	return dif;
}

int ans = 25;
int X, Y;
int xx[8] = {-2, -2, -1, -1, +1, +1, +2, +2}, 
	yy[8] = {-1, +1, -2, +2, -2, +2, -1, +1};

void dfs(int cnt, int lasdo)
{
	int want = compare();
	if (cnt + want > 16) return;
	if (cnt >= ans) return;
	if (want == 0) {
		ans = cnt;
		return;
	}
	for (int i = 0; i < 8; i++) {
		if (i + lasdo == 7) continue;
		int mx = X + xx[i],
			my = Y + yy[i];
		if (mx < 1 || mx > 5 || my < 1 || my > 5) continue;
		int SaveX = X, SaveY = Y;
		swap(now[X][Y], now[mx][my]);
		X = mx, Y = my;
		dfs(cnt + 1, i);
		X = SaveX, Y = SaveY;
		swap(now[X][Y], now[mx][my]);
	}
}

void doOnce()
{
	for (int i = 1; i <= 5; i++) {
		scanf("%s", now[i] + 1);
		for (int j = 1; j <= 5; j++) {
			if (now[i][j] == '*') X = i, Y = j;
		}
	}
	ans = 25;
	dfs(0, 10);
	write(ans != 25 ? ans : -1, '\n');
}

int main()
{
	int T = read<int>();
	while (T--) doOnce();
	return 0;
}
