/*************************************
 * problem:      P5316 恋恋的数学题.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-04-21.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 
/*************************************
 * now_type:	 loading
 * now_bak:		 pigeon 
 * next_step:    don't know (pigeon) 
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

struct big_integer
{
    static const int base = 10000;
    static const int width = 4;

    vector<int> v;
    bool is_negative;

    big_integer(long long num = 0)
    {
        *this = num;
        is_negative = false;
    }

    big_integer operator = (long long val)
    {
        v.clear();

        if (val < 0)
        {
            val = -val;
            is_negative = true;
        }
        do
        {
            v.push_back(val % base);
            val /= base;
        } while (val > 0);

        return *this;
    }

    big_integer operator = (const string &str) 
    {
        v.clear();

        string str2 = str;
        if (str2[0] == '-')
        {
            is_negative = true;
            for (string::iterator i = str2.begin(); i < str2.end() - 1; i++)
            {
                *i = *(i + 1);
            }
            *(str2.end() - 1) = '\0';
            str2.resize(str2.length() - 1);
        }

        int cur, len = (str2.length() - 1) / width + 1;
        for (int i = 0; i < len; i++)
        {
            int end = str2.length() - i * width;
            int begin = max(0, end - width);
            sscanf(str2.substr(begin, end - begin).c_str(), "%d", &cur);
            v.push_back(cur);
        }

        return *this;
    }

    big_integer operator + (const big_integer &addend) const 
    {
        big_integer sum;
        sum.v.clear();

        if (is_negative == true)
        {
            if (addend.is_negative == true) sum.is_negative = true;
            else return addend - (-*this);
        }
        else if (addend.is_negative == true)
        {
            return *this - (-addend);
        }

        for (int i = 0, carry = 0; carry != 0 || i < v.size() || i < addend.v.size(); i++)
        {
            int cur = carry;
            if (i < v.size()) cur += v[i];
            if (i < addend.v.size()) cur += addend.v[i];
            carry = cur / base;
            cur %= base;
            sum.v.push_back(cur);
        }

        return sum;
    }

    big_integer operator - (const big_integer &subtrahend) const 
    {
        big_integer difference;
        difference.v.clear();

        if (is_negative == false && subtrahend.is_negative == false && *this < subtrahend)
            return -(subtrahend - *this);
        if (is_negative == true && subtrahend.is_negative == false)
            return -(-*this + subtrahend);
        else if (is_negative == false && subtrahend.is_negative == true)
            return *this + (-subtrahend);
        else if (is_negative == true && subtrahend.is_negative == true)
            return -subtrahend - (-*this);

        for (int i = 0, carry = 0; carry != 0 || i < v.size(); i++)
        {
            int cur = v[i] + carry;
            if (i < subtrahend.v.size()) cur -= subtrahend.v[i];
            if (cur >= 0) carry = 0;
            else
            {
                carry = -1;
                cur += base;
            }
            difference.v.push_back(cur);
        }

        for (int i = difference.v.size() - 1; i >= 0; i--)
        {
            if (difference.v[i] == 0) difference.v.resize(difference.v.size() - 1);
            else break;
        }
        if (difference.v.size() == 0) difference.v.resize(1, 0);

        return difference;
    }

    big_integer operator - () const 
    {
        big_integer big_int = *this;
        big_int.is_negative = !(is_negative);
        return big_int;
    }

    big_integer operator * (const big_integer &multiplier) const
    {
        big_integer product;
        product.v.clear();

        if (is_negative != multiplier.is_negative) product.is_negative = true;

        for (int i = 0; i < v.size(); i++)
        {
            for (int j = 0; j < multiplier.v.size(); j++)
            {
                if (i + j >= product.v.size()) product.v.resize(i + j + 1, 0);
                product.v[i + j] += this->v[i] * multiplier.v[j];
                if (product.v[i + j] >= base)
                {
                    if (i + j + 1 >= product.v.size()) product.v.resize(i + j + 1 + 1, 0);
                    product.v[i + j + 1] += product.v[i + j] / base;
                    product.v[i + j] %= base;
                }
            }
        }

        for (int i = product.v.size() - 1; i >= 0; i--)
        {
            if (product.v[i] == 0) product.v.resize(product.v.size() - 1);
            else break;
        }
        if (product.v.size() == 0) product.v.resize(1, 0);

        return product;
    }

    big_integer operator / (const big_integer &divisor) const // Division
    {
        big_integer divisor__ = divisor;
        big_integer quotient = 0;

        if (is_negative != (divisor < 0)) quotient.is_negative = true;

        big_integer remainder = 0;
        for (int i = v.size() - 1; i >= 0; i--)
        {
            remainder = remainder * base + v[i];

            int lwr_bound = 0, upr_bound = base - 1, mid;
            big_integer mid_bi;
            while (lwr_bound <= upr_bound)
            {
                mid = (lwr_bound + upr_bound) / 2;
                mid_bi = (long long)mid;
                big_integer bi = remainder - mid_bi * divisor;
                if (remainder - mid_bi * divisor < divisor && remainder - mid_bi * divisor >= 0) break;
                else if (remainder - mid_bi * divisor >= divisor) lwr_bound = mid + 1;
                else upr_bound = mid - 1;
            }

            quotient = quotient * base + mid_bi;
            remainder = remainder - mid_bi * divisor;
        }

        for (int i = quotient.v.size() - 1; i >= 0; i--)
        {
            if (quotient.v[i] == 0) quotient.v.resize(quotient.v.size() - 1);
            else break;
        }
        if (quotient.v.size() == 0) quotient.v.resize(1, 0);

        return quotient;
    }

    big_integer operator % (const big_integer &divisor) const 
    {
        big_integer quotient = *this / divisor;
        big_integer remainder = *this - quotient * divisor;
        return remainder;
    }

    big_integer operator += (const big_integer &addend)
    {
        *this = *this + addend;
        return *this;
    }

    big_integer operator -= (const big_integer &subtrahend) 
    {
        *this = *this - subtrahend;
        return *this;
    }

    big_integer operator *= (const big_integer &multiplier) 
    {
        *this = *this * multiplier;
        return *this;
    }

    big_integer operator /= (const long long &divisor) 
    {
        *this = *this / divisor;
        return *this;
    }

    big_integer operator %= (const long long &divisor) 
    {
        *this = *this / divisor;
        return *this;
    }

    big_integer operator ++ () 
    {
        big_integer addend = (long long)1;
        *this = *this + addend;
        return *this;
    }

    big_integer operator -- () 
    {
        big_integer subtrahend = (long long)1;
        *this = *this - subtrahend;
        return *this;
    }

    big_integer operator ++ (int) 
    {
        big_integer addend = (long long)1;
        *this = *this + addend;
        return *this;
    }

    big_integer operator -- (int) 
    {
        big_integer subtrahend = (long long)1;
        *this = *this - subtrahend;
        return *this;
    }

    big_integer operator ^ (int k) 
    {
        big_integer ans = 1, ta = (*this);
        while (k) {
        	if (k & 1) ans *= ta;
        	k >>= 1;
        	ta *= ta;
        }
//		cout << ans << endl;
        return ans;
    }

    bool operator < (const big_integer &big_int) const 
    {
        if (is_negative == true && big_int.is_negative == false) return true;
        if (is_negative == false && big_int.is_negative == true) return false;
        if (is_negative == true && big_int.is_negative == true)
        {
            big_integer big_int1, big_int2;
            big_int1 = *this; big_int1.is_negative = false;
            big_int2 = big_int; big_int2.is_negative = false;
            return big_int2 < big_int1;
        }

        if (v.size() != big_int.v.size()) return v.size() < big_int.v.size();
        for (int i = v.size() - 1; i >= 0; i--)
        {
            if (v[i] != big_int.v[i]) return v[i] < big_int.v[i];
        }

        return false;
    }

    bool operator > (const big_integer &big_int) const
    {
        return big_int < *this;
    }

    bool operator <= (const big_integer &big_int) const 
    {
        return !(*this > big_int);
    }

    bool operator >= (const big_integer &big_int) const 
    {
        return !(*this < big_int);
    }

    bool operator != (const big_integer &big_int) const 
    {
        return *this < big_int || *this > big_int;
    }

    bool operator == (const big_integer &big_int) const 
    {
        return !(*this != big_int);
    }

    friend istream &operator >> (istream &in, big_integer &big_int) 
    {
        string str;
        if (!(in >> str)) return in;
        big_int = str;
        return in;
    }

    friend ostream &operator << (ostream &out, big_integer &big_int)
    {
        if (big_int.is_negative == true) out << '-';
        out << big_int.v.back();

        for (int i = big_int.v.size() - 1 - 1; i >= 0; i--)
        {
            char str[base + 5];
            sprintf(str, "%0*d", width, big_int.v[i]);
            out << str;
        }

        return out;
    }
};

big_integer sqrtBint(big_integer ori)
{
    big_integer l, r, mid;
    l = 1;
    r = ori;

    while (l <= r) {
    	mid = (l + r) / 2;
    	if (mid * mid == ori) {
    		return mid;
    	} else if (mid * mid < ori) {
    		l = mid + 1;
    	} else {
    		r = mid - 1;
        }
        // cout << l << ", " << r << endl;
    }
    return mid;
}

int T, k;

void doCase1()
{
    // long long a, b;
    // a = read<long long>();
    // b = read<long long>();
    // write()
    write(read<long long>(), 32);
    write(read<long long>(), 10);
}

typedef big_integer bint;

bint gcd(bint a, bint b)
{
    return a == 0 ? b : gcd(b % a, a);
}

bint lcm(bint a, bint b)
{
    return a  / gcd(a, b) * b;
}

struct Sans3 {
    bint a, b, c;
    bool chk;
    Sans3(bint A, bint B, bint C) : a(A), b(B), c(C), chk(1) {}
    Sans3() : chk(0) {}
};

#define test(A, B, C)                                                   \
{                                                                       \
    a1 = times / a / A;                                                 \
    a2 = times / b / B;                                                 \
    a3 = times / c / C;                                                 \
    if (gcd(a1, a2) == c && gcd(a1, a3) == b && gcd(a2, a3) == a) {     \
        return Sans3(a1, a2, a3);                                       \
    }                                                                   \
}

Sans3 wrk(bint a, bint b, bint c, bint A, bint B, bint C)
{
    bint times = a * b * c * A * B * C;
    // cout << times << endl;
    times = sqrtBint(times);
    // cout << times << endl;
    bint a1, a2, a3;
    test(A, B, C);
    test(A, C, B);
    test(B, A, C);
    test(B, C, A);
    test(C, A, B);
    test(C, B, A);
    return Sans3();
}

void doCase2()
{
    bint a, b, c, A, B, C;
    cin >> a >> b >> c >> A >> B >> C;
    Sans3 s = wrk(a, b, c, A, B, C);
    cout << s.a << ' ' << s.b << ' ' << s.c << endl;
    return;
}

#define enum_3id_Start                              \
for (int i1 = 1; i1 <= 6; i1++) {                   \
    for (int i2 = 1; i2 <= 6; i2++) {               \
        if (i1 == i2) continue;                     \
        for (int i3 = 1; i3 <= 6; i3++) {           \
            if (i1 == i3 || i2 == i3) continue;     \
            int j1, j2, j3;                         

#define enum_3id_End } } }

#define get_rest_3id()                                                      \
for (j1 = 1; j1 <= 6; j1++) {                                               \
    if (j1 != i1 && j1 != i2 && j1 != i3) break;                            \
}                                                                           \
for (j2 = 1; j2 <= 6; j2++) {                                               \
    if (j2 != i1 && j2 != i2 && j2 != i3 && j1 != j2) break;                \
}                                                                           \
for (j3 = 1; j3 <= 6; j3++) {                                               \
    if (j3 != i1 && j3 != i2 && j3 != i3 && j1 != j3 && j2 != j3) break;    \
}

#define chked() s.a == t.a && s.b == t.b

#define basic_match_st()                                                    \
{                                                                           \
    if (chked()) break;                                                     \
    swap(t.b, t.c);                                                         \
    if (chked()) break;                                                     \
    swap(t.a, t.c);                                                         \
    if (chked()) break;                                                     \
    swap(t.b, t.c);                                                         \
    if (chked()) break;                                                     \
    swap(t.a, t.c);                                                         \
    if (chked()) break;                                                     \
    swap(t.b, t.c);                                                         \
    if (chked()) break;                                                     \
}

#define match_st()                                                          \
bool ismatched = 1;                                                         \
do {                                                                        \
    basic_match_st()                                                        \
    swap(s.a, s.c);                                                         \
    basic_match_st()                                                        \
    swap(s.b, s.c);                                                         \
    basic_match_st()                                                        \
    ismatched = 0;                                                          \
} while(0);

#define basicMatch(a, b)                                                    \
for (int i = 1; i <= 6; i++) {                                              \
    if (gcd(a, b) == g[i]) {                                                \
        for (int j = 1; j <= 6; j++) {                                      \
            if (lcm(a, b) == l[j]) {                                        \
                g[i] = 0;                                                   \
                l[j] = 0;                                                   \
                match++;                                                    \
                i = 7;                                                      \
                break;                                                      \
            }                                                               \
        }                                                                   \
    }                                                                       \
}

bool testmatch(bint a1, bint a2, bint a3, bint a4, bint g[7], bint l[7])
{
    int match = 0;
    basicMatch(a1, a2);
    basicMatch(a1, a3);
    basicMatch(a1, a4);
    basicMatch(a2, a3);
    basicMatch(a2, a4);
    basicMatch(a3, a4);
    // printf("matched = %d.\n", match);
    return match == 6;
}

void doCase3()
{
    bint a[7], b[7];
    for (int i = 1; i <= 6; i++) {
        cin >> a[i];
    }
    for (int i = 1; i <= 6; i++) {
        cin >> b[i];
    }
    enum_3id_Start {
        Sans3 s = wrk(a[i1], a[i2], a[i3], b[i1], b[i2], b[i3]);
        if (!s.chk) continue;
        get_rest_3id();
        Sans3 t = wrk(a[i1], a[j1], a[j2], b[i1], b[j1], b[j2]);
        if (!t.chk) continue;
        match_st();
        // cout << "DEBUG : " << s.a << ' ' << s.b << ' ' << s.c << ' ' << t.a << ' ' << t.b << ' ' << t.c << endl;
        if (!ismatched) continue;
        if (!testmatch(s.a, s.b, s.c, t.c, a, b)) continue;
        cout << s.a << ' ' << s.b << ' ' << s.c << ' ' << t.c << endl;
        return;
    } enum_3id_End;
    return;
}

int main()
{
    T = read<int>();
    k = read<int>();
    if (k == 2) {
        while (T--) doCase1();
    } else if (k == 3) {
        while (T--) doCase2();
    } else if (k == 4) {
        while (T--) doCase3();
    }
    return 0;
}