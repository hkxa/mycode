/*************************************
 * @problem:      id_name.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2019-mm-dd.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int64 n, k;
struct Matrix {
    int64 a[103][103];
    Matrix() { memset(a, 0, sizeof(a)); }
    void init() { for (int64 i = 1; i <= 3; i++) a[i][i] = 1; }
    Matrix operator * (const Matrix &b) {
        Matrix res;
        for (int64 i = 1; i <= 3; i++) {
            for (int64 j = 1; j <= 3; j++) {
                for (int64 k = 1; k <= 3; k++) {
                    res.a[i][j] = (res.a[i][j] + a[i][k] * b.a[k][j]) % 1000000007;
                }
            }
        }
        return res;
    }
    Matrix operator ^ (int64 n) {
        Matrix res, now = *this;
        res.init();
        while (n) {
            if (n & 1) res = res * now; 
            now = now * now;
            n >>= 1;
        }
        return res;
    }
} a;

int Once()
{
    k = read<int64>();
    a.a[1][1] = 1;
    a.a[1][2] = 0;
    a.a[1][3] = 1;
    a.a[2][1] = 1;
    a.a[2][2] = 0;
    a.a[2][3] = 0;
    a.a[3][1] = 0;
    a.a[3][2] = 1;
    a.a[3][3] = 0;
    a = a ^ k;
    write(a.a[2][1], 10);
    return 0;
}

int main()
{
    int T = read<int>();
    while (T--) Once();
    return 0;
}