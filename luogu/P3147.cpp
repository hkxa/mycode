/*************************************
 * @problem:      id_name.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-mm-dd.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int MaxN = 262144, MaxK = 58;

int n;
int f[MaxN + 3][MaxK + 3];
int ans = 0;

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) f[i][read<int>()] = i + 1;
    for (int k = 1; k <= MaxK; k++) {
        for (int i = 1; i <= n; i++) {
            if (!f[i][k]) f[i][k] = f[f[i][k - 1]][k - 1];
            // if (f[i][k]) printf("f[%d][%d] = %d.\n", i, k, f[i][k]);
            if (f[i][k]) ans = max(ans, k);
        }
    }
    write(ans, 10);
    return 0;
}