#include<bits/stdc++.h>
using namespace std;

int n, k, minest = 0x7fffffff;
int f[501][501];

struct book {
    int h, w;
    bool operator < (const book &cer) const {
        return h < cer.h;
    }
} a[1001];

int main()
{
    memset(f, 20, sizeof(f));

    scanf("%d%d", &n, &k);
    k = n - k;
    for (int i = 1; i <= n; i++)
        scanf("%d %d", &a[i].h, &a[i].w);

    sort(a + 1, a + n + 1);

    for (int i = 1; i <= n; i++)
        f[i][1] = 0;

    for (int i = 2; i <= n; i++)
        for (int j = 1; j <= i - 1; j++)
            for (int l = 2; l <= min(i, k); l++)
                f[i][l] = min(f[i][l], f[j][l - 1] + abs(a[i].w - a[j].w));

    for (int i = k; i <= n; i++)
        minest = min(minest, f[i][k]);

    printf("%d", minest);
    return 0;
}