/*************************************
 * problem:      P5270 无论怎样神树大人都会删库跑路.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-03-26.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

int n, Q, m;
int R[100007];
queue <int> q; 

struct str {
	int len;
    vector <int> v;
    void input()
    {
    	v.resize(len = read<int>());
    	for (int i = 0; i < len; i++) {
    		v[i] = read<int>();
		}
	}
} a[100007];

enum readLoading {
    firstRead = 1, 
    secondRead = 2
};

struct goalString {
    int T, want;
    int goalType[100007];
    int now[100007];
    int cnt;

    goalString() 
    {
        cnt = 0;
        want = 0;
        memset(goalType, 0, sizeof(goalType));
        memset(now, 0, sizeof(now));
    }

    void input(readLoading type)
    {
    	int tmp;
        switch (type) {
            case firstRead:
                T = read<int>();
                break;
            case secondRead:
                for (int i = 1; i <= T; i++) {
                    goalType[tmp = read<int>()]++;
                    if (goalType[tmp] == 1) want++;
                }
                break;
            default:
                printf("Error!");
                break;
        }
    }

    void push(int num) 
    {
        now[num]++;
        if (now[num] == goalType[num]) cnt++;
        else if (now[num] == goalType[num] + 1) cnt--;
    } 

    void pop(int num) 
    {
        now[num]--;
        if (now[num] == goalType[num]) cnt++;
        else if (now[num] == goalType[num] - 1) cnt--;
    }

    int getAns()
    {
//    	printf("getAns() : cnt = %d. (diff = %d)\n", cnt, want - cnt);
        return cnt == want;
    }
} goal;

void add(str target)
{
	for (int i = 0; i < target.len; i++) {
		goal.push(target.v[i]); 
		q.push(target.v[i]); 
	}
	while (q.size() > goal.T) {
		goal.pop(q.front());
		q.pop();
	}
}

int main()
{
    n = read<int>();
    goal.input(firstRead);
    Q = read<int>();
    goal.input(secondRead);
    for (int i = 1; i <= n; i++) {
    	a[i].input();
	}
	m = read<int>();
    for (int i = 1; i <= m; i++) {
    	R[i] = read<int>();
	}
	int ans = 0, cnt = 0;
	for (int i = 0; i < Q; i++) {
		add(a[R[i % m + 1]]);
		if (q.size() < goal.T) continue;
		else cnt++;
		ans += goal.getAns();
		if (cnt >= m) {
			int rest = Q - i - 1;
			if (rest >= m) {
				ans += rest / m * ans;
				Q -= rest / m * m;
			}
		}
	} 
	write(ans);
	return 0; 
}
