//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      P4338 [ZJOI2018]历史.
 * @user_id:      63720.
 * @user_name:    brealid.
 * @time:         2020-05-27.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;
#define int int64

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int N = 400000 + 7;

#define lc son[u][0]
#define rc son[u][1]

int f[N];
int v[N], exV[N], s[N];
int son[N][2];
bool rev[N];
vector<int> G[N];

inline bool isroot(int);        // [splay 中] 判断一个节点是否为 root
inline bool which(int);         // [splay 中] 判断一个节点是父亲节点的哪个孩子
inline void rotate(int);        // [splay 中] 旋转操作
inline void splay(int);         // [splay 中] 使节点成为所在 splay 的根
inline void access(int);        // [原树 中] 使原树根节点到 u 的路径成为 Preferred Path

int n, m;
int opt, x, y;
int ans;

inline bool isroot(int u) { return u != son[f[u]][0] && u != son[f[u]][1]; }
inline bool which(int u) { return u == son[f[u]][1]; }
inline void reverse(int u) { swap(lc, rc); rev[u] ^= 1; }
inline void pushup(int u) { s[u] = s[lc] + s[rc] + v[u] + exV[u]; }

inline void rotate(int u) {
    int v = f[u], w = f[v], d = which(u);
    if (!isroot(v)) son[w][which(v)] = u;
    f[son[u][!d]] = v;
    son[v][d] = son[u][!d];
    son[u][!d] = v;
    f[u] = w;
    f[v] = u;
    pushup(v);
    pushup(u);
}

inline void splay(int u) {
    for (int v = f[u]; !isroot(u); v = f[u]) {
        if (!isroot(v)) rotate(which(u) ^ which(v) ? u : v);
        rotate(u);
    }
    pushup(u);
}
inline void access(int u) { 
    for (int v = 0; u; u = f[v = u]) { 
        splay(u); 
        rc = v; 
        pushup(u); 
    } 
}

#define GetContribution() (rc ? ((t - h) << 1) : ((v[u] << 1) > t ? ((t - v[u]) << 1) : t - 1))
inline void modify(int u, int w)
{
    splay(u);
    int t = s[u] - s[lc], h = s[rc];
    ans -= GetContribution();
    s[u] += w;
    v[u] += w;
    t += w;
    if ((h << 1) < t + 1) {
        exV[u] += h;
        rc = 0;
    }
    ans += GetContribution();
    pushup(u);
    int y;
    for (u = f[y = u]; u; u = f[y = u]) {
        u = f[y];
        splay(u);
        t = s[u] - s[lc], h = s[rc];
        ans -= GetContribution();
        s[u] += w;
        exV[u] += w;
        t += w;
        if ((h << 1) < t + 1) {
            exV[u] += h;
            h = 0;
            rc = 0;
        }
        if ((s[y] << 1) > t) {
            exV[u] -= s[y];
            h = s[y];
            rc = y;
        }
        ans += GetContribution();
        pushup(u);
    }
}
void dfs(int u, int fa)
{
    f[u] = fa;
    s[u] = v[u];
    int p = 0, mx = v[u];
    for (unsigned i = 0; i < G[u].size(); i++) {
        int v = G[u][i];
        if (v ^ fa) {
            dfs(v, u);
            s[u] += s[v];
            if (s[v] > mx) mx = s[p = v];
        }
    }
    ans += min(s[u] - 1, (s[u] - mx) << 1);
    if ((mx << 1) >= s[u] + 1) rc = p;
    exV[u] = s[u] - s[rc] - v[u];
}

signed main()
{
    n = read<int>();
    m = read<int>();
    for (int i = 1; i <= n; i++) v[i] = read<int>();
    for (int i = 1, u, v; i < n; i++) {
        u = read<int>();
        v = read<int>();
        G[u].push_back(v);
        G[v].push_back(u);
    }
    dfs(1, 0);
    write(ans, 10);
    for (int i = 1, xi, wi; i <= m; i++) {
        xi = read<int>();
        wi = read<int>();
        modify(xi, wi);
        write(ans, 10);
    }
    return 0;
}