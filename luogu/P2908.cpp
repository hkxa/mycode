#include <stdio.h>
#include <bits/stdc++.h>
using namespace std;

int n, m;
string name[1007];
string energy[107];


void stringToLower(string &s)
{
    for (int i = 0; i < s.length(); i++) {
        if (s[i] >= 'A' && s[i] <= 'Z') s[i] += 'a' - 'A';
    }
}

bool match(string fa, string son)
{
    for (int i = 0, j = 0; i < fa.length(); i++) {
        if (fa[i] == son[j]) {
            j++;
            if (j == son.length()) return 1;
        }
    }
    return 0;
}

int main()
{
    cin >> n >> m;
    for (int i = 1; i <= n; i++) {
        cin >> name[i];
        stringToLower(name[i]);
    }
    for (int i = 1; i <= m; i++) {
        cin >> energy[i];
        stringToLower(energy[i]);
    }
    for (int i = 1; i <= n; i++) {
        int cnt(0);
        for (int j = 1; j <= m; j++) {
            if (match(name[i], energy[j])) cnt++;
        }
        cout << cnt << endl;
    }
    return 0;
}