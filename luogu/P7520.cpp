// Liu Xinnan AK IOI!
// xaero give me strength!!
// Towards your dream, no matter meet what!!!
#define SX2021_ProblemName "dominator"
#include <bits/stdc++.h>
using namespace std;
typedef long long int64;
#ifdef Test_TimMem
namespace s67d9f98euxm2u331r4y { bool MemTester1; }
#endif

inline void FastIn(int &val) {
//	cin >> val;return;
    char ch = getchar();
    while (ch < '0' || ch > '9') ch = getchar();
    val = ch & 15;
    while ((ch = getchar()) >= '0' && ch <= '9') val = (((val << 2) + val) << 1) + (ch & 15);
}

inline void PrintInt(int val) {
    if (val > 9) PrintInt(val / 10);
    putchar('0' | (val % 10));
}
#define OutInt(Val, End) PrintInt(Val), putchar(End)

inline void UpdMax(int &a, int b) { if (a < b) a = b; }
inline void UpdMin(int &a, int b) { if (a > b) a = b; }

const int N = 3007, Q = 2e4 + 7;
int n, m, q;
struct ToEdge {
    int v, nxt;
} e[N << 1];
int head[N], ecnt;
#define AddE(fr, to) e[++ecnt].v = to, e[ecnt].nxt = head[fr], head[fr] = ecnt

namespace So_Naive {
    bool chk() { return n <= 100 && q <= 100; }
    bool vis[N];
    void TryGo(int u) {
        if (vis[u]) return;
        vis[u] = true;
        for (int i = head[u]; i; i = e[i].nxt) TryGo(e[i].v);
    }
    int D[N], G_D[N];
    void display() {
        memset(D, 0, sizeof(D));
        for (int u = 1; u <= n; ++u) {
            memset(vis, 0, sizeof(vis));
            vis[u] = true;
            TryGo(1);
            for (int v = 1; v <= n; ++v)
                if (!vis[v]) ++D[v];
        }
    }
    void solve() {
        display();
        for (int i = 1; i <= n; ++i) G_D[i] = D[i];
        // for (int i = 1; i <= n; ++i) printf("size(D[%d]) = %d\n", i, n - D[i]);
        for (int i = 1, s, t; i <= q; ++i) {
            FastIn(s), FastIn(t), AddE(s, t);
            display();
            int ans = 0;
            for (int i = 1; i <= n; ++i)
                if (G_D[i] != D[i]) ++ans; // , printf("Changed:%d\n", i)
            OutInt(ans, '\n');
            head[s] = e[ecnt--].nxt;
        }
    }
}

namespace TreePart {
    bool chk() { return m == n - 1; }
    int Father[N][13], dep[N];
    void dfs(int u, int ff) {
        dep[u] = dep[ff] + 1;
        Father[u][0] = ff;
        for (int i = 0; i < 12; ++i) Father[u][i + 1] = Father[Father[u][i]][i];
        for (int i = head[u]; i; i = e[i].nxt) dfs(e[i].v, u);
    }
    int qLCA(int u, int v) {
        if (dep[u] < dep[v]) swap(u, v);
        for (int k = 12; dep[u] > dep[v]; --k)
            if (dep[u] - (1 << k) >= dep[v]) u = Father[u][k];
        if (u == v) return u;
        for (int k = 12; Father[u][0] != Father[v][0]; --k)
            if (Father[u][k] != Father[v][k]) u = Father[u][k], v = Father[v][k];
        return Father[u][0];
    }
    void solve() {
    	dfs(1, 0);
        for (int i = 1, s, t; i <= q; ++i) {
            FastIn(s), FastIn(t);
            // printf("LCA(%d, %d) = %d\n", s, t, qLCA(s, t));
            OutInt(max(dep[t] - dep[qLCA(s, t)] - 1, 0), '\n');
        }
    }
}

namespace Failure_Try {
    int dfn[N], low[N], dft;
    int fam[N], fam_cnt;
    int s[N], top;
    bool instack[N];
    void tarjan(int u) {
    	// cerr << "tarjan(" << u << ") </> Start" << endl;
        low[u] = dfn[u] = ++dft;
        instack[s[++top] = u] = true;
        for (int i = head[u]; i; i = e[i].nxt) {
            int v = e[i].v;
            if (!dfn[v]) {
                // tarjan(v);
                low[u] = min(low[u], low[v]);
            } else if (instack[v]) {
                low[u] = min(low[u], dfn[v]);
            }
        }
    	// cerr << "tarjan(" << u << ") </> Across" << endl;
        if (low[u] == dfn[u]) {
            ++fam_cnt;
            while (dfn[s[top]] >= dfn[u]) {
                fam[s[top]] = fam_cnt;
                instack[s[top--]] = false;
            }
        }
    	// cerr << "tarjan(" << u << ") </> End" << endl;
    }
    void tarjan() {
        memset(dfn, 0, sizeof(dfn));
        dft = fam_cnt = 0;
        tarjan(1);
    }
    namespace T {
        struct ToEdge {
            int v, nxt;
        } e[N << 1];
        int head[N], ecnt;
        #define T_AddE(fr, to) e[++ecnt].v = to, e[ecnt].nxt = head[fr], head[fr] = ecnt
    }
    int D[N], G_D[N];
    void display() {
    	// cerr << "display </> Start" << endl;
        tarjan();
    	// cerr << "display </> Across" << endl;
        for (int u = 1; u <= n; ++u)
            for (int i = head[u]; i; i = e[i].nxt) {
                int v = e[i].v;
                if (fam[u] != fam[v]) T_AddE(fam[u], fam[v]);
            }
    	// cerr << "display </> End" << endl;
    }
    void solve() {
        // These are quite near to std, but not std, can only gain 0pts
        // So, it call So_Naive::solve()
        So_Naive::solve();
        return;
        display();
        for (int i = 1; i <= n; ++i) G_D[i] = D[i];
        for (int i = 1, s, t; i <= q; ++i) {
            FastIn(s), FastIn(t), AddE(s, t);
            display();
            int ans = 0;
            for (int i = 1; i <= n; ++i)
                if (G_D[i] != D[i]) ++ans;
            OutInt(ans, '\n');
            head[s] = e[ecnt--].nxt;
        }
    }
}

#ifdef Test_TimMem
namespace s67d9f98euxm2u331r4y { bool MemTester2; }
#endif
int main() {
#ifndef zy_NoFreopen_
    // freopen(SX2021_ProblemName ".in", "r", stdin);
    // freopen(SX2021_ProblemName ".out", "w", stdout);
#endif
    ios::sync_with_stdio(false);
    FastIn(n), FastIn(m), FastIn(q);
    for (int i = 1, s, t; i <= m; ++i) FastIn(s), FastIn(t), AddE(s, t);
    if (So_Naive::chk()) So_Naive::solve();
    else if (TreePart::chk()) TreePart::solve();
    else Failure_Try::solve();
#ifdef Test_TimMem
    cerr << "Tim Used: " << clock() << "ms" << endl;
    cerr << "Mem Used: " << (&s67d9f98euxm2u331r4y::MemTester2 - &s67d9f98euxm2u331r4y::MemTester1) / 1048576.0 << "mb" << endl;
#endif
    return 0;
}
