// Liu Xinnan AK IOI!
// xaero give me strength!!
// Towards your dream, no matter meet what!!!
#define SX2021_ProblemName "ranklist"
#include <bits/stdc++.h>
using namespace std;
typedef long long int64;
#ifdef Test_TimMem
namespace s67d9f98euxm2u331r4y { bool MemTester1; }
#endif

inline void FastIn(int &val) {
//	cin >> val;return;
    char ch = getchar();
    while (ch < '0' || ch > '9') ch = getchar();
    val = ch & 15;
    while ((ch = getchar()) >= '0' && ch <= '9') val = (((val << 2) + val) << 1) + (ch & 15);
}

inline void PrintInt(int val) {
    if (val > 9) PrintInt(val / 10);
    putchar('0' | (val % 10));
}
#define OutInt(Val, End) PrintInt(Val), putchar(End)

inline void UpdMax(int &a, int b) { if (a < b) a = b; }
inline void UpdMin(int &a, int b) { if (a > b) a = b; }

const int N = 13, M = 503;

int n, m, a[N];

namespace NaiveMethod {
    bool chk() { return n <= 11; }
    void solve() {
        int64 ans = 0;
        int order[N];
        for (int i = 0; i < n; ++i) order[i] = i;
        do {
            // 1 2 1
            // 3 2 1
            int need = 0;
            for (int i = 0; i < n; ++i)
                need = max(need, a[i] + (i < order[0]));
            int remain = m - (need - a[order[0]]), cost = need - a[order[0]];
            // if (order[0] == 2 && order[1] == 0 && order[2] == 1) printf("Person %d: Score %d + %d\n", 3, a[2], need - a[2]);
            for (int i = 1; i < n && remain >= 0; ++i) {
                int p_now = a[order[i]];
                if (order[i] > order[i - 1]) ++need;
                if (p_now >= need) need = p_now + cost;
                else {
                    UpdMax(cost, need - p_now);
                    remain -= cost;
                    need = p_now + cost;
                }
                // if (order[0] == 2 && order[1] == 0 && order[2] == 1) printf("Person %d: Score %d + %d\n", order[i] + 1, a[order[i]], need - a[order[i]]);
            }
            if (remain >= 0) {
                ++ans;
                // printf("ok: %d %d %d\n", order[2], order[1], order[0]);
            }
        } while (next_permutation(order, order + n));
        OutInt(ans, '\n');
    }
}

namespace TryToDP {
    bool chk() { return n <= 8 && m <= 100; }
    const int N = 8, M = 103;
    int64 f[1 << N][M][M << 1]; // f[status][need][remain]
    void solve() {
        int mask = (1 << n) - 1;
        int MaxUsing = m * 2;
        f[0][0][m] = 1;
        for (int state = 1; state <= mask; ++state) {
            for (int p = 0; p < n; ++p) {
                if (!((state >> p) & 1)) continue;
                int prev = state ^ (1 << p);
                for (int remain = 0; remain <= m; ++remain)
                    for (int require = 0; require <= MaxUsing; ++require)
                        for (int SelfUse = max(require - a[p], 0); SelfUse <= remain; ++SelfUse)
                            f[state][a[p] + SelfUse][remain - SelfUse] += f[prev][require][remain];
            }
        }
        int64 ans = 0;
        for (int require = 0; require <= MaxUsing; ++require) ans += f[mask][require][0];
        OutInt(ans, '\n');
    }
}

#ifdef Test_TimMem
namespace s67d9f98euxm2u331r4y { bool MemTester2; }
#endif
int main() {
#ifndef zy_NoFreopen_
    // freopen(SX2021_ProblemName ".in", "r", stdin);
    // freopen(SX2021_ProblemName ".out", "w", stdout);
#endif
    ios::sync_with_stdio(false);
    FastIn(n), FastIn(m);
    int MaxA = -1e6, MinA = 1e6;
    for (int i = 0; i < n; ++i) {
        FastIn(a[i]);
        UpdMax(MaxA, a[i]);
        UpdMin(MinA, a[i]);
    }
    if (MaxA - MinA > m) {
        OutInt(0, '\n');
        return 0;
    } else {
        for (int i = 0; i < n; ++i) a[i] -= MinA;
        MaxA -= MinA;
    }
    if (NaiveMethod::chk()) NaiveMethod::solve();
#ifdef Test_TimMem
    cerr << "Tim Used: " << clock() << "ms" << endl;
    cerr << "Mem Used: " << (&s67d9f98euxm2u331r4y::MemTester2 - &s67d9f98euxm2u331r4y::MemTester1) / 1048576.0 << "mb" << endl;
#endif
    return 0;
}
