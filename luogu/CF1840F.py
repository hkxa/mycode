import sys


T_cases = int(sys.stdin.readline())
for _ in range(T_cases):
    n, m = map(int, sys.stdin.readline().split())
    r = int(sys.stdin.readline())
    a = [[[0 for _ in range(r + 1)] for _ in range(m + 1)] for _ in range(n + 1)] # a[x][y][delay]
    a[0][0][0] = 1
    for _ in range(r):
        t, d, coord = map(int, sys.stdin.readline().split())
        if d == 1:
            for y in range(m + 1):
                delay = t - coord - y
                if 0 <= delay <= r:
                    a[coord][y][delay] = -1
        else:
            for x in range(n + 1):
                delay = t - x - coord
                if 0 <= delay <= r:
                    a[x][coord][delay] = -1
    for i in range(n + 1):
        for j in range(m + 1):
            for k in range(r + 1):
                if a[i][j][k] != 1:
                    continue
                if k != r and a[i][j][k + 1] == 0:
                    a[i][j][k + 1] = 1
                if i != n and a[i + 1][j][k] == 0:
                    a[i + 1][j][k] = 1
                if j != m and a[i][j + 1][k] == 0:
                    a[i][j + 1][k] = 1
    success = -1
    for k in range(r + 1):
        if a[n][m][k] == 1:
            success = n + m + k
            break
    print(success)