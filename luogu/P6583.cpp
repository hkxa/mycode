//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      T131085 座位调查.
 * @user_id:      63720.
 * @user_name:    brealid.
 * @time:         2020-05-30.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
       return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
       return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define int int64

// const int N = 1003, P = 998244353;

// inline int64 kpow(int64 a, int n) {
//     int64 r = 1;
//     while (n) {
//         if (n & 1) r = r * a;
//         a = a * a;
//         n >>= 1;
//     }
//     return r;
// }

inline int64 calc2(int64 n) {
    return n & (-n);
}

inline int64 calc5(int64 n) {
    return n % 5 == 0 ? 5 * calc5(n / 5) : 1;
}

inline int64 CalcRest(int64 n) {
    return n / calc2(n) / calc5(n);
}

int64 n;

// __int128 calc(int64 n, int64 m) {
//     __int128 ans = 0;
//     int64 s = sqrt(n);
//     if (m > s) {
//         for (int64 i = 1; i <= s && (n / i) > (n / s); i++)
//             ans += n / i;
//         for (int64 res = n / s; res >= 1; res--) {
//             ans += res * (min(n / res, m) - min(n / (res + 1), m));
//             // printf("%lld, %lld\n", res, min(n / res, m) - min(n / (res + 1), m));
//         }
//     } else for (int64 i = 1; i <= m; i++)
//             ans += n / i;
    
//     // printf("calc(%lld, %lld) = ", n, m); write(ans, 10);
//     return ans;
// }

vector<int64> vec;
map<int64, int> vis;
int x[100007];

int64 get25(int64 x) {
    return x - x / 2 - x / 5 + x / 10;
    // 容斥
}

void find5(int64 n) {
    if (!n) return;
    // printf("find5(%lld)\n", n);
    if (vis.find(n) != vis.end()) vis[n]++;
    else {
        vec.push_back(n);
        vis[n] = 1;
    }
    find5(n / 5);
}

void find2(int64 n) {
    if (!n) return;
    find2(n / 2);
    find5(n);
}

signed main() {
    n = read<int64>();
    if (n <= 10000000 && 0) {
        int64 ans = 0;
        for (int i = 1; i <= n; i++)
            ans += n / CalcRest(i);
        write(ans, 10);
    } else {
        find2(n); 
        sort(vec.begin(), vec.end(), greater<int64>());
        for (unsigned i = 0; i < vec.size(); i++) x[i] = vis[vec[i]];
        // for (unsigned i = 0; i < vec.size(); i++) printf("vis[%lld] = %d\n", vec[i], x[i]);
        int64 ans = 0;
        for (int64 l = 1, r; l <= n; l = r + 1) {
            r = n / (n / l);
            int64 L = get25(l - 1), R = get25(r), Val = n / l;
            for (unsigned i = 0; i < vec.size(); i++) {
                int64 m = vec[i];
                if (m < l) break;
                if (m >= r) ans += (R - L) * Val * x[i];
                else ans += (get25(m) - L) * Val * x[i];
            }
            // printf("l = %lld, ans = %lld\n", l, ans);
        }
        write(ans, 10);
    }
    return 0;
}

// Create File Date : 2020-05-30

// 100 : 2185