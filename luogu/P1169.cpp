/*************************************
 * problem:      P1169 [ZJOI2007]棋盘制作.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-03.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

/*
3 3

1 0 1
0 1 0
1 0 0
  ||
  \/
1 1 1
1 1 1
1 1 0
*/

int n, m;
bool a[2007][2007];
int l[2007][2007] = {0}, r[2007][2007] = {0}, up[2007][2007] = {0};
/**
 * l  : 向左可以到达的最左节点编号
 * r  : 向右可以到达的最左节点编号
 * up : 向上可以到达的行数
 */

void getBlock(int x, int y)
{
    static int tmp;
    tmp = read<int>();
    a[x][y] = (tmp != ((x + y) & 1));
}

int main()
{
    int square = 1, matrix = 1;
    n = read<int>();
    m = read<int>();
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++) {
            // putchar('.');
            getBlock(i, j);
            up[i][j] = 1;
            l[i][j] = r[i][j] = j;
        }
        // puts("");
    }
    // puts("");
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j < m; j++) {
            // putchar('.');
            if (a[i][j] == a[i][j + 1]) l[i][j + 1] = l[i][j];
        }
        // puts(".");
    }
    // puts("");
    for (int i = 1; i <= n; i++) {
        for (int j = m; j > 1; j--) {
            // putchar('.');
            if (a[i][j] == a[i][j - 1]) r[i][j - 1] = r[i][j];
        }
        // puts(".");
    }
    // puts("");
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++) {
            // putchar('.');
            if (i > 1 && a[i][j] == a[i - 1][j]) {
                l[i][j] = max(l[i][j], l[i - 1][j]);
                r[i][j] = min(r[i][j], r[i - 1][j]);
                // UP-2-lines : 使 up 数组的值正确
                up[i][j] = up[i - 1][j] + 1;
            }
            int d = r[i][j] - l[i][j] + 1; 
            int h = up[i][j];
            int len = min(d, h);
            square = max(square, len * len);
            matrix = max(matrix, d * h);
        }
        // puts("");
    }
    // puts("");
    write(square, 10);
    write(matrix, 10);
    return 0;
}