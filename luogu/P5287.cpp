/*************************************
 * @problem:      [HNOI2019]JOJO.
 * @user_name:    brealid.
 * @time:         2020-11-08.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if true
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

const int N = 1e5 + 7, M = 10000 + 7, P = 998244353;

int n;
int val[N], pos[N], cnt;
vector<int> to[N];
int ans[N];
int rt[N][26], mx[N][26];
int top, sta_val[N], sta_len[N];

inline int64 C_x_2(int64 x) {
    return (x * (x + 1) >> 1) % P;
}

struct SegTreeNode {
    int ls, rs, lzy, nxt, sum;
} tr[M << 8];
int trcnt;

#define copy_node(u) ((tr[++trcnt] = tr[u]), trcnt)

#define covernode(u, v, len) do {       \
    tr[u].lzy = (v);                    \
    tr[u].sum = (int64)(v) * (len) % P; \
} while(0)

#define pushdown(u) do {                            \
    if (tr[u].lzy) {                                \
        tr[u].ls = copy_node(tr[u].ls);             \
        tr[u].rs = copy_node(tr[u].rs);             \
        covernode(tr[u].ls, tr[u].lzy, mid - l + 1);\
        covernode(tr[u].rs, tr[u].lzy, r - mid);    \
        tr[u].lzy = 0;                              \
    }                                               \
} while(0)

void modify(int &u, int l, int r, int RightBound, int NewVal, int NewNxt) {
    u = copy_node(u);
    if (l == r) tr[u].nxt = NewNxt;
    if (r < RightBound || l == r) {
        covernode(u, NewVal, r - l + 1);
        return;
    }
    int mid = (l + r) >> 1;
    pushdown(u);
    modify(tr[u].ls, l, mid, RightBound, NewVal, NewNxt);
    if (mid < RightBound) modify(tr[u].rs, mid + 1, r, RightBound, NewVal, NewNxt);
    tr[u].sum = (tr[tr[u].ls].sum + tr[tr[u].rs].sum) % P;
}

void query(int &u, int l, int r, int RightBound, int &Result, int &Nxt) {
    if (l == r) Nxt = tr[u].nxt;
    if (r < RightBound || l == r) {
        Result = (Result + tr[u].sum) % P;
        return;
    }
    int mid = (l + r) >> 1;
    pushdown(u);
    query(tr[u].ls, l, mid, RightBound, Result, Nxt);
    if (mid < RightBound) query(tr[u].rs, mid + 1, r, RightBound, Result, Nxt);
}

void dfs(int u) {
    ++top;
    int ch = val[u] / M, len = val[u] % M, nxt = 0;
    sta_val[top] = val[u];
    sta_len[top] = sta_len[top - 1] + len;
    if (top == 1) ans[u] = C_x_2(len - 1);
    else {
        ans[u] = (ans[u] + C_x_2(min(mx[top][ch], len))) % P;
        query(rt[top][ch], 1, 10000, len, ans[u], nxt);
        if (!nxt && sta_val[1] / M == ch && sta_len[1] < len) {
            nxt = 1;
            ans[u] = (ans[u] + (int64)sta_len[1] * max(0, len - mx[top][ch])) % P;
        }
    }
	mx[top][ch] = max(mx[top][ch], len);
    modify(rt[top][ch], 1, 10000, len, sta_len[top - 1], top);
    for (size_t i = 0; i < to[u].size(); ++i) {
        int v = to[u][i];
        ans[v] = ans[u];
		memcpy(mx[top + 1], mx[nxt + 1], sizeof(mx[top + 1]));
		memcpy(rt[top + 1], rt[nxt + 1], sizeof(rt[top + 1]));
        dfs(v);
    }
	--top;
}

signed main() {
    read >> n;
    int k; char ch;
    for (int i = 1; i <= n; ++i) {
        if (read.get<int>() == 1) {
            read >> k >> ch;
            val[pos[i] = ++cnt] = (ch - 'a') * M + k;
            to[pos[i - 1]].push_back(pos[i]);
        } else pos[i] = pos[read.get<int>()];
    }
    for (size_t i = 0; i < to[0].size(); ++i) {
        trcnt = 0;
        memset(rt[1], 0, sizeof(rt[1]));
        memset(mx[1], 0, sizeof(mx[1]));
        dfs(to[0][i]);
    }
    for (int i = 1; i <= n; ++i) write << ans[pos[i]] << '\n';
    return 0;
}