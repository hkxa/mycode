/*************************************
 * problem:      P4101 [HEOI2014]人人尽说江南好.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-06-04.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

int main()
{
    int n, m, cases = read<int>();
    while (cases--) {
        n = read<int>();
        m = read<int>();
        int ans = (n / m) * (m - 1);
        int rest = n % m;
        if (rest != 0) rest--;
        ans += rest;
        write(!(ans & 1), 10);
    }
    return 0;
}