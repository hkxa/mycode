//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      Codeforces - Codeforces Round #659 (Div. 1).
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-24.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 1e5 + 7, Alp = 20, AlpMask = 1 << 20;

int T, n;
char a[N], b[N];
int adj[N], both_adj[N];
bitset<AlpMask> f;
bitset<Alp> vis;
int SubG_cnt, ans;

void dfs(int u) {
    vis.set(u);
    for (size_t i = 0; i < 20; i++)
        if ((both_adj[u] & (1 << i)) && !vis.test(i))
            dfs(i);
}

signed main() {
    read >> T;
    while (T--) {
        read >> n;
        scanf("%s", a + 1);
        scanf("%s", b + 1);
        memset(adj, 0, sizeof(adj));
        memset(both_adj, 0, sizeof(both_adj));
        f.reset();
        vis.reset();
        SubG_cnt = ans = 0;
        for (int i = 1; i <= n; i++)
            if (a[i] != b[i]) {
                adj[a[i] - 'a'] |= (1 << (b[i] - 'a'));
                both_adj[a[i] - 'a'] |= (1 << (b[i] - 'a'));
                both_adj[b[i] - 'a'] |= (1 << (a[i] - 'a'));
            }
        for (int i = 0; i < 20; i++) 
            if (!vis.test(i)) dfs(i), SubG_cnt++;
        f.set(0);
        for (int i = 0; i < AlpMask; i++)
            if (f.test(i)) {
                ans = max(ans, __builtin_popcount(i));
                for (int j = 0; j < 20; j++)
                    if (!(i & (1 << j)) && !(adj[j] & i))
                        f.set(i | (1 << j));
            }
        write << 2 * Alp - SubG_cnt - ans << '\n';
    }
    return 0;
}