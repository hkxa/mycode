//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Qtree5 - Query on a tree V.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-02.
 * @language:     C++.
*************************************/ 
#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#ifndef DEBUG
# define fprintf(...) {}
# define fflush(...) {}
# define debug(...) {}
#endif

#define MEM_ARR(arr, siz, v) { for (int i = 0; i < siz + 5; i++) arr[i] = v; }

// #define int int64

const int N = 100007, MY_INT_MAX = INT_MAX / 3;

int n, Q, CntWhite;
int head[N], to[N << 1], nxt[N << 1], edge_cnt;
int fa[N]; // 点分树上的父亲
int siz[N], vis[N];
const bool black = 0, white = 1;
bool type[N];

#define ADDE(u, v) { to[++edge_cnt] = v; nxt[edge_cnt] = head[u]; head[u] = edge_cnt; }
#define linkE(u, v) { ADDE(u, v); ADDE(v, u); }

void reverseColor(int x) {
    if (type[x] == black) {
        type[x] = white;
        CntWhite++;
    } else {
        type[x] = black;
        CntWhite--;
    }
}

int IntegerPowerOfTwo_k[37];
// 预处理
void IntegerPowerOfTwo_Pretreat() {
    for (int i = 0; i < 20; i++) IntegerPowerOfTwo_k[(1 << i) % 37] = i;
}
#define Log2(num) IntegerPowerOfTwo_k[(num) % 37]

struct TreeChainSplit_LCA {
    int dep[N];
    int siz[N], wson[N];
    int tcs_fa[N], beg[N];
    // 预处理
    void Pretreat1(int u, int Father) {
        tcs_fa[u] = Father;
        dep[u] = dep[Father] + 1;
        siz[u] = 1;
        wson[u] = 0;
        for (int e = head[u]; e; e = nxt[e]) {
            int &v = to[e];
            if (v != Father) {
                Pretreat1(v, u);
                siz[u] += siz[v];
                if (siz[v] > siz[wson[u]]) wson[u] = v;
            }
        }
    }
    void Pretreat2(int u, int ChainBegin) {
        beg[u] = ChainBegin;
        if (!wson[u]) return;
        Pretreat2(wson[u], ChainBegin);
        for (int e = head[u]; e; e = nxt[e]) {
            int &v = to[e];
            if (v != tcs_fa[u] && v != wson[u]) 
                Pretreat2(v, v);
        }
    }
    inline int calcDist(int u, int v) {
        int ans = dep[u] + dep[v];
        while (beg[u] != beg[v]) {
            if (dep[beg[u]] < dep[beg[v]]) swap(u, v);
            u = tcs_fa[beg[u]];
        }
        if (dep[u] < dep[v]) return ans - 2 * dep[u];
        else return ans - 2 * dep[v];
    }
} Graph;

struct Heap {
    priority_queue<int, vector<int>, greater<int> > que, del;
    size_t size() { return que.size() - del.size(); }
    void push_erase_tag() {
        while (!del.empty() && que.top() == del.top()) {
            que.pop();
            del.pop();
        }
    }
    void insert(int x) { que.push(x); }
    void erase(int x) { del.push(x); }
    inline int top() {
        if (size()) {
            push_erase_tag();
            return que.top();
        } else return MY_INT_MAX;
    }
} dis[N];
// dis : 维护原树子树到 u 的 dis

struct PointDivideTree {
    // 获得所在子树所有点的 siz
    void getSubtreeSiz(int u) {
        vis[u] = true;
        siz[u] = 1;
        for (int e = head[u]; e; e = nxt[e]) {
            int &v = to[e];
            if (!vis[v]) {
                getSubtreeSiz(v);
                siz[u] += siz[v];
            }
        }
        vis[u] = false;
    }

    // 获得所在子树的根（已维护 siz)
    int GSR_sizAll, GSR_root, GSR_rootSiz;
    void getSubtreeRoot(int u) {
        vis[u] = true;
        int Tmax = 0;
        for (int e = head[u]; e; e = nxt[e]) {
            int &v = to[e];
            if (!vis[v]) {
                getSubtreeRoot(v);
                if (siz[v] > Tmax) Tmax = siz[v];
            }
        }
        if (GSR_sizAll - siz[u] > Tmax) Tmax = GSR_sizAll - siz[u];
        if (Tmax < GSR_rootSiz) {
            GSR_root = u;
            GSR_rootSiz = Tmax;
        }
        vis[u] = false;
    }

    // 获得所在子树的根（已封装）
    inline int getRoot(int u) {
        getSubtreeSiz(u);
        GSR_sizAll = siz[u];
        GSR_rootSiz = n + 1;
        getSubtreeRoot(u);
        return GSR_root;
    }

    // 获得点分树（预处理）
    void GetPointDivideTree(int u) {
        vis[u] = true;
        for (int e = head[u]; e; e = nxt[e]) {
            int v = to[e];
            if (!vis[v]) {
                v = getRoot(v);
                fa[v] = u;
                GetPointDivideTree(v);
            }
        }
        vis[u] = false;
    }
} pvt; // Point Divide Tree

struct ProblemSolver {
    // 总预处理
    void pretreat() {                           
        IntegerPowerOfTwo_Pretreat();                        
        int root = pvt.getRoot(1);
        Graph.siz[0] = 0;
        Graph.Pretreat1(root, 0);       
        Graph.Pretreat2(root, root);                
        pvt.GetPointDivideTree(root);           
    }
    // 树点黑变白
    void turnOn(int x) {
        int OriginNode = x;
        while (x) {
            dis[x].insert(Graph.calcDist(OriginNode, x));
            x = fa[x];
        }
    }
    // 树点白变黑
    void turnOff(int x) {
        int OriginNode = x;
        while (x) {
            dis[x].erase(Graph.calcDist(OriginNode, x));
            x = fa[x];
        }
    }
    int getAnswer(int x) {
        int OriginNode = x, ans = n + 1;
        while (x) {
            ans = min(ans, Graph.calcDist(OriginNode, x) + dis[x].top());
            x = fa[x];
        }
        return ans;
    }
} solver;

signed main() {
    n = read<int>();
    for (int i = 1, a, b; i < n; i++) {
        a = read<int>();
        b = read<int>();
        linkE(a, b);
    }
    solver.pretreat(); 
    Q = read<int>();
    int opt, x;
    while (Q--) {
        opt = read<int>();
        x = read<int>();
        if (opt == 0) {                  
            if (type[x] == white) solver.turnOff(x);
            else solver.turnOn(x);
            reverseColor(x);
        } else {                                
            if (CntWhite == 0) puts("-1");
            else write(solver.getAnswer(x), 10);
        }
    }
    return 0;
}

// Create File Date : 2020-06-12