/*************************************
 * @problem:      [PKUSC2018]PKUSC.
 * @author:       brealid.
 * @time:         2021-05-13.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 200 + 7, M = 500 + 7;
const double PI = 3.14159265358979323846;

int n, m;
struct vec2d {
    double x, y;
    vec2d() {}
    vec2d(const double &X, const double &Y) : x(X), y(Y) {}
    vec2d operator + (const vec2d &b) const { return vec2d(x + b.x, y + b.y); }
    vec2d operator - (const vec2d &b) const { return vec2d(x - b.x, y - b.y); }
    vec2d operator * (const double &b) const { return vec2d(x * b, y * b); }
    vec2d operator / (const double &b) const { return vec2d(x / b, y / b); }
    double length() const { return sqrt(x * x + y * y); }
    double length_sqr() const { return x * x + y * y; }
    vec2d nomalize() const { double len = length(); return *this / len; }
    vec2d rotate90() const { return vec2d(-y, x); }
} enemy[N], attack[M];
struct line {
    vec2d a, b;
    line() {}
    line(const vec2d &A, const vec2d &B) : a(A), b(B) {}
    vec2d to_vec2d() const { return b - a; }
};

double dot(vec2d a, vec2d b) { return a.x * b.x + a.y * b.y; }
double cross(vec2d a, vec2d b) { return a.x * b.y - b.x * a.y; }
double angle(vec2d a, vec2d b) { return acos(dot(a, b) / (a.length() * b.length())); }
double dis(vec2d a, vec2d b) { return (a - b).length(); }
double dis(line s, vec2d p) { return abs(cross(s.to_vec2d(), p - s.a)) / dis(s.a, p); }

double inTriangle(vec2d a, vec2d b, vec2d p) {
    int d = dis(line(a, b), p), len_a = a.length(), len_b = b.length(), len_p = p.length();
    vec2d unit_ab = line(a, b).to_vec2d().nomalize();
    int flag = cross(a, b) > 0 ? 1 : -1;
    if (p.x == 0 && p.y == 0) {
        if (cross(a, b) == 0) return 0;
        else return angle(a, b) * flag;
    }
    if (d >= len_p) return angle(a, b) * flag;
    if (len_p >= len_a && len_p >= len_b) return 0;
    int x1 = sqrt(p.length_sqr() - d * d), xD = sqrt(len_a * len_a - d * d);
    vec2d h1 = a + unit_ab * (xD - x1), h2 = a + unit_ab * (xD + x1);
    bool h1_on = dot(h1 - a, b - a) > 0, h2_on = dot(h2 - a, b - a) > 0;
    if (h1_on) {
        if (h2_on) return angle(h1, h2) * flag;
        else return angle(h1, b) * flag;
    } else {
        if (h2_on) return angle(a, h2) * flag;
        else return 0;
    }
}

signed main() {
    kin >> n >> m;
    for (int i = 1, x, y; i <= n; ++i) {
        kin >> x >> y;
        enemy[i].x = x, enemy[i].y = y;
    }
    for (int i = 1, x, y; i <= m; ++i) {
        kin >> x >> y;
        attack[i].x = x, attack[i].y = y;
    }
    double p = 0;
    for (int i = 1; i <= n; ++i) {
        p += inTriangle(attack[m], attack[1], enemy[i]);
        // printf("enemy(%.lf, %.lf) in Triangle((%.lf, %.lf), (%.lf, %.lf)): %.5lf\n", enemy[i].x, enemy[i].y, attack[m].x, attack[m].y, attack[1].x, attack[1].y, inTriangle(attack[m], attack[1], enemy[i]));
        for (int j = 1; j < m; ++j) {
            p += inTriangle(attack[j], attack[j + 1], enemy[i]);
            // printf("enemy(%.lf, %.lf) in Triangle((%.lf, %.lf), (%.lf, %.lf)): %.5lf\n", enemy[i].x, enemy[i].y, attack[j].x, attack[j].y, attack[j + 1].x, attack[j + 1].y, inTriangle(attack[j], attack[j + 1], enemy[i]));
        }
    }
    printf("%.5lf\n", p / (2 * PI));
    return 0;
}