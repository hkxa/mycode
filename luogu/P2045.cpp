#include <cmath>
#include <queue>
#include <cstdio>
#include <cctype>
#include <vector>
#include <cstring>
#include <algorithm>
using namespace std;

/*
样例：
3 1
1 2 3
0 2 1
1 4 2
[11]
3 2
1 2 3
0 2 1
1 4 2
[15]
3 3
1 2 3
0 2 1
1 4 2
[16]
*/


struct Edge {
    int u, v, w;
    int cost;
    int next;
    Edge() {}
    Edge(int _u, int _v, int _w, int _cost, int _next) : u(_u), v(_v), w(_w), cost(_cost), next(_next) {}
};

int n, k, s, t;
Edge edges[300007]; 
int head[5007], top = 0;
int could[5007];
int road[5007];
int pre[5007];

template <typename _Tp>
void queue_clear(queue<_Tp> &a)
{
    queue<_Tp> empty;
    swap(a, empty);
}
queue <int> q;

void _add_once(int u, int v, int w, int cost)
{
    edges[top] = Edge(u, v, w, cost, head[u]);
    head[u] = top++;
}

void add(int u, int v, int w, int cost)
{
    _add_once(u, v, w, cost);
    _add_once(v, u, 0, -cost);
}

// 如果你想查看一些 DEBUG ，请去掉下一行开头的注释符号
// #define TEST_OPEN
// 【警告：仅限小数据（如小样例）】 

#ifndef TEST_OPEN 
#define fprintf(...)  
#endif

int dis[10007];
bool SPFA()
{
	queue_clear(q);
    bool vis[10007] = {0};
    memset(dis, 0x3f, sizeof(dis));
    memset(could, 0x3f, sizeof(could));
    dis[s] = 0;
    vis[s] = 1;
    q.push(s);
    while (!q.empty()) {
        int now = q.front();
        q.pop();
        vis[now] = 0;
        for (int nNow = head[now]; nNow != -1; nNow = edges[nNow].next) {
            Edge &e = edges[nNow];
            if (e.w > 0 && dis[e.v] > dis[now] + e.cost) {
                dis[e.v] = dis[now] + e.cost;
                road[e.v] = nNow;
                could[e.v] = min(could[now], e.w);
                pre[e.v] = now;
                if (!vis[e.v]) {
                    q.push(e.v);
                    vis[e.v] = 1;
                }
            }
        }
    }
    if (dis[t] != 0x3f3f3f3f) return true; // success
    else return false; // unsuccess
}

void EK_MCMF() 
{
    // EK_MCMF : 基于 EK 的 MCMF 算法 
	//           (Edmonds Karp's "Min Cost Max Flow" algorithm)
    int maxflow = 0, mincost = 0;
#ifdef TEST_OPEN
    int count = 0;
#endif
    while (SPFA()) {
        for (int pNow = t; pNow != s; pNow = pre[pNow]) {
            edges[road[pNow]].w -= could[t];
            edges[road[pNow] ^ 1].w += could[t];
        }
        maxflow += could[t];
        mincost += dis[t] * could[t];
    }
    printf("%d", -mincost);
#ifdef TEST_OPEN
    printf("\nDUBUG:maxflow = %d.\n", maxflow);
#endif
}

enum IN_OUT {
    IN = 0, 
    OUT = 1
};

int getrank(int x, int y, IN_OUT status)
{
    return status * n * n + (x - 1) * n + y;
}

int main()
{
    memset(head, -1, sizeof(head));
    scanf("%d%d", &n, &k);
    s = 0;
    t = getrank(n, n, OUT) + 1;
    add(s, getrank(1, 1, IN), k, 0);
    add(getrank(n, n, OUT), t, k, 0);
    int aij;
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= n; j++) {
            scanf("%d", &aij);
            add(getrank(i, j, IN), getrank(i, j, OUT), 1, -aij);
            add(getrank(i, j, IN), getrank(i, j, OUT), k - 1, 0);
        }
    }
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j < n; j++) {
            add(getrank(i, j, OUT), getrank(i, j + 1, IN), k, 0);
        }
    }
    for (int i = 1; i < n; i++) {
        for (int j = 1; j <= n; j++) {
            add(getrank(i, j, OUT), getrank(i + 1, j, IN), k, 0);
        }
    }
    EK_MCMF();
    return 0;
}