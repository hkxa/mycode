/*************************************
 * @problem:      [湖南集训]谈笑风生.
 * @user_name:    brealid/hkxadpall/zhaoyi20/j(o)moo/littleTortoise.
 * @time:         2020-09-20.
 * @language:     C++.
 * @fastio_ver:   20200913.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0,    // input
        flush_stdout = 1 << 1,  // output
        flush_stderr = 1 << 2,  // output
    };
    enum number_type_flags {
        output_double_stable = 1 << 0,  // output
        output_double_faster = 1 << 1   // output
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f & ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore character set = {' ', '\r', '\n', '\t'}
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (double &lf) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            lf = endch & 15;
            while (isdigit(endch = getchar())) lf = lf * 10 + (endch & 15);
            if (endch == '.') {
                double digit = 0.1;
                while (isdigit(endch = getchar())) {
                    lf += (endch & 15) * digit;
                    digit *= 0.1;
                }
            }
            if (flag) lf = -lf;
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            Int d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return d;
        }
    } read;

    struct Writer {
        Writer& operator << (io_flags f) {
            if (f & flush_stdout) fflush(stdout);
            if (f & flush_stderr) fflush(stderr);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
        inline void operator () (double val, int eps_digit = 6, char endch = '\0', number_type_flags flg = output_double_faster) {
            if (flg & output_double_stable) {
                static char output_format[10];
                sprintf(output_format, "%%.%dlf", eps_digit);
                printf(output_format, val);
            } else if (flg & output_double_faster) {
                if (val < 0) {
                    putchar('-');
                    val = -val;
                }
                double eps_number = 0.5;
                for (int i = 1; i <= eps_digit; i++) eps_number /= 10;
                val += eps_number;
                (*this) << (int64)val;
                val -= (int64)val;
                if (eps_digit) putchar('.');
                while (eps_digit--) {
                    val *= 10;
                    putchar((int)val | '0');
                    val -= (int)val;
                }
            } else {
               (*this) << '1';
                if (eps_digit) {
                    (*this) << ".E";
                    for (int i = 2; i <= eps_digit; i++) (*this) << '0';
                }
            }
            if (endch) putchar(endch);
        }
    } write;
}
using namespace Fastio;

namespace File_IO {
    void init_IO(const char *file_name) {
        char buff[107];
        sprintf(buff, "%s.in", file_name);
        freopen(buff, "r", stdin);
        sprintf(buff, "%s.out", file_name);
        freopen(buff, "w", stdout);
    }
}

// #define int int64

const int N = 3e5 + 7;

struct chairman_segment_tree {
    int RangeN;
    int root_id[N];
    int ls[N << 6], rs[N << 6], cnt;
    int64 val[N << 6];
    void modify(int &u, int v, int l, int r, int index, int dif) {
        if (l > r) return;
        u = ++cnt;
        val[u] = val[v] + dif;
        if (l == r) return;
        int mid = (l + r) >> 1;
        if (index <= mid) {
            rs[u] = rs[v];
            modify(ls[u], ls[v], l, mid, index, dif);
        } else {
            ls[u] = ls[v];
            modify(rs[u], rs[v], mid + 1, r, index, dif);
        }
    }
    void apply(int tree_id, int index, int v) {
        modify(root_id[tree_id], root_id[tree_id - 1], 1, RangeN, index, v);
    }
    int64 query(int u, int v, int l, int r, int ml, int mr) {
        if (l >= ml && r <= mr) return val[u] - val[v];
        if (l > mr || r < ml) return 0;
        int mid = (l + r) >> 1;
        return query(ls[u], ls[v], l, mid, ml, mr) + query(rs[u], rs[v], mid + 1, r, ml, mr);
    }
    int64 ask(int dep_from, int dep_limit, int tree_id_from, int tree_cnt) {
        return query(root_id[tree_id_from + tree_cnt - 1], root_id[tree_id_from - 1], 1, RangeN, dep_from + 1, dep_from + dep_limit);
    }
} cm_smt; // ChairMan SegMent Tree

int n, q;
int dfn[N], id[N], dep[N], siz[N], dft;
vector<int> G[N];

void dfs(int u, int fa) {
    dep[u] = dep[fa] + 1;
    siz[u] = 1;
    id[dfn[u] = ++dft] = u;
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i];
        if (v == fa) continue;
        dfs(v, u);
        siz[u] += siz[v];
    }
}

signed main() {
    // File_IO::init_IO("[湖南集训]谈笑风生");
    read >> n >> q;
    cm_smt.RangeN = n;
    for (int i = 1, u, v; i < n; i++) {
        read >> u >> v;
        G[u].push_back(v);
        G[v].push_back(u);
    }
    dfs(1, 0);
    for (int i = 1; i <= n; i++)
        cm_smt.apply(i, dep[id[i]], siz[id[i]] - 1);
    for (int i = 1, p, k; i <= q; i++) {
        read >> p >> k;
        int64 ans = (int64)min(dep[p] - 1, k) * (siz[p] - 1);
        ans += cm_smt.ask(dep[p], k, dfn[p], siz[p]);
        write << ans << '\n';
    }
    return 0;
}