/*************************************
 * problem:      P2184 贪婪大陆.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-05-01.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

template <typename Int>
class super_BIT {
// #define DEBUG_super_BIT
  private:
  	Int n;
  	Int *basicSum, *delta1, *delta2;
  	
    Int lowbit(Int x) 
    {
        return x & (-x);
    }
    
    void arr_add(Int *arr, Int pos, Int x) 
    {
        while (pos <= n) {
        	arr[pos] += x;
            pos += lowbit(pos);
        }
    }
    
    Int arr_getsum(Int *arr, Int pos)
    {
        Int sum = 0;
        while (pos) {
            sum += arr[pos];
            pos -= lowbit(pos);
        }
        return sum;
    }
    
    void free_space()
    {
        delete[] basicSum;
        delete[] delta1;
        delete[] delta2;
    }
  public:
  	super_BIT() 
    {
  		basicSum = NULL;
  		delta1 = NULL;
  		delta2 = NULL;
    }
  	~super_BIT() 
    {
        free_space(); 
    }
  	
  	void init(Int size, Int *arr = NULL)
  	{
  		free_space();
  		n = size;
  		basicSum = new Int[size + 1];
  		delta1 = new Int[size + 1];
  		delta2 = new Int[size + 1];
  		memset(basicSum, 0, (size + 1) * sizeof(Int));
  		memset(delta1, 0, (size + 1) * sizeof(Int));
  		memset(delta2, 0, (size + 1) * sizeof(Int));
        if (arr != NULL) {
            for (Int i = 1; i <= n; i++) {
                basicSum[i] = basicSum[i - 1] + arr[i];
            }
        }
    }
    
    void modify(Int l, Int r, Int x)
    {
        arr_add(delta1, l, x);
        arr_add(delta1, r + 1, -x);
        arr_add(delta2, l, x * l);
        arr_add(delta2, r + 1, -x * (r + 1));
    }
    
    Int getsum(Int r)
    {
        return basicSum[r] + arr_getsum(delta1, r) * (r + 1) - arr_getsum(delta2, r);
    }
    
    Int query(Int l, Int r)
    {
        return getsum(r) - getsum(l - 1);
    }

    void debug()
    {
#ifdef DEBUG_super_BIT
        printf("HKXA-lib::super_BIT debug():\n    ");
        for (Int i = 1; i <= n; i++) {
            printf("%d ", query(i, i));
        }
        printf("\n\n");
#endif
    }
    
    void sample(string problem)
    {
        if (problem == "LOJ - #132") {
            Int N, Q;
            Int *a = new Int[1000001];
            N = read<Int>();
            Q = read<Int>();
            for (Int i = 1; i <= N; i++) {
                a[i] = read<Int>();
            }
            init(N, a);
            Int type, l, r, x;
            while (Q--) {
                type = read<Int>();
                if (type == 1) {
                    l = read<Int>();
                    r = read<Int>();
                    x = read<Int>();
                    modify(l, r, x);
                } else {
                    l = read<Int>();
                    r = read<Int>();
                    write(query(l, r), 10);
                }
                debug();
            }
        } else {
            // do
        }
    }
}; 

int n, m;
int Q, L, R;
super_BIT<int> l, r;

int main()
{
    n = read<int>();
    m = read<int>();
    l.init(n);
    r.init(n);
    for (int i = 0; i < m; i++) {
        Q = read<int>();
        L = read<int>();
        R = read<int>();
        if (Q == 1) {
            l.modify(L, L, 1);
            r.modify(R, R, 1);
        } else {
            write(l.query(1, R) - r.query(1, L - 1), 10);
        }
        l.debug();
        r.debug();
    }
}