/*************************************
 * @problem:      移花接木.
 * @author:       brealid.
 * @time:         2020-11-28.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;
#define getchar() (i_P1 == i_P2 && (i_P2 = (i_P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), i_P1 == i_P2) ? EOF : *i_P1++)
char BUF[1 << 21], *i_P1 = BUF, *i_P2 = BUF;
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

template <int ModNum>
struct mint {
    int num;
    mint() : num(0) {}
    mint(int val) : num((val % ModNum + ModNum) % ModNum) {}
    mint(int64 val) : num((val % ModNum + ModNum) % ModNum) {}
    operator int() const { return num; }
    operator bool() const { return num; }
    mint pow(int K) const {
        mint ret = 1, now = *this;
        while (K) {
            if (K & 1) ret *= now;
            now *= now;
            K >>= 1;
        }
        return ret;
    }
    mint inv() const { return this->pow(ModNum - 2); }
    mint& operator ++ () { if (++num >= ModNum) num -= ModNum; return *this; }
    mint operator ++ (int) { mint ret = *this; ++*this; return ret; }
    mint& operator -- () { if (--num < 0) num += ModNum; return *this; }
    mint operator -- (int) { mint ret = *this; --*this; return ret; }
    mint operator - () const { return mint(-num); }
    mint operator << (const int &b) const { return mint((int64)num << b); }
    mint operator >> (const int &b) const { return mint(num >> b); }
    mint operator + (const mint &b) const { return mint(num + b.num); }
    mint operator - (const mint &b) const { return mint(num - b.num); }
    mint operator * (const mint &b) const { return mint((int64)num * b.num); }
    mint operator / (const mint &b) const { return *this * b.inv(); }
    mint operator % (const mint &b) const { return mint(num % b.num); }
    mint operator + (const int &b) const { return *this + mint(b); }
    mint operator - (const int &b) const { return *this - mint(b); }
    mint operator * (const int &b) const { return *this * mint(b); }
    mint operator / (const int &b) const { return *this / mint(b); }
    mint operator % (const int &b) const { return *this % mint(b); }
    mint& operator += (const mint &b) { return *this = *this + b; }
    mint& operator -= (const mint &b) { return *this = *this - b; }
    mint& operator *= (const mint &b) { return *this = *this * b; }
    mint& operator /= (const mint &b) { return *this = *this / b; }
    mint& operator %= (const mint &b) { return *this = *this % b; }
    friend mint operator + (const int &a, const mint &b) { return mint(a) + b; }
    friend mint operator - (const int &a, const mint &b) { return mint(a) - b; }
    friend mint operator * (const int &a, const mint &b) { return mint(a) * b; }
    friend mint operator / (const int &a, const mint &b) { return mint(a) / b; }
    friend mint operator % (const int &a, const mint &b) { return mint(a) % b; }
    bool operator < (const mint &b) { return num < b.num; }
    bool operator > (const mint &b) { return num > b.num; }
    bool operator <= (const mint &b) { return num <= b.num; }
    bool operator >= (const mint &b) { return num >= b.num; }
    bool operator == (const mint &b) { return num == b.num; }
    bool operator != (const mint &b) { return num != b.num; }
    bool operator < (const int &b) { return num < b; }
    bool operator > (const int &b) { return num > b; }
    bool operator <= (const int &b) { return num <= b; }
    bool operator >= (const int &b) { return num >= b; }
    bool operator == (const int &b) { return num == b; }
    bool operator != (const int &b) { return num != b; }
    friend bool operator < (const int &a, const mint &b) { return a < b.num; }
    friend bool operator > (const int &a, const mint &b) { return a > b.num; }
    friend bool operator <= (const int &a, const mint &b) { return a <= b.num; }
    friend bool operator >= (const int &a, const mint &b) { return a >= b.num; }
    friend bool operator == (const int &a, const mint &b) { return a == b.num; }
    friend bool operator != (const int &a, const mint &b) { return a != b.num; }
};

const int P = 1e9 + 7;

int T, a, b, h;

mint<P> sum_geometric_progression(mint<P> k, mint<P> n) {
    return k == 1 ? n : ((k.pow(n) - 1) / (k - 1));
}

signed main() {
    for (kin >> T; T--;) {
        kin >> a >> b >> h;
        mint<P> del_mid = sum_geometric_progression(b, h) * max(a - b, 0);
        mint<P> del_leaf = mint<P>(b).pow(h) * a;
        kout << (del_mid + del_leaf).num << '\n';
    }
    return 0;
}