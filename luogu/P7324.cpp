/*************************************
 * @problem:      expr.
 * @author:       brealid(赵奕).
 * @time:         2021-02-05.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

const int M = 11, N = 5e4 + 17, P = 1e9 + 7;

int n, m, q;
int a[M][N];
char qs[N];
// int cnt_q[N], power2[N];
int bracket_l[N], bracket_r[N];
struct node {
    int v, ls, rs, situation;
} tr[N];
int tr_cnt;

void build(int self, int l, int r) {
    if (bracket_l[r] == l) {
        build(self, l + 1, r - 1);
        return;
    }
    if (l == r) {
        tr[self].v = qs[l] - '0';
        tr[self].situation = 1;
        return;
    }
    if (qs[r] != ')') {
        build(tr[self].ls = ++tr_cnt, l, r - 2);
        build(tr[self].rs = ++tr_cnt, r, r);
        tr[self].situation = (int64)tr[tr[self].ls].situation * tr[tr[self].rs].situation % P;
        switch (qs[r - 1]) {
            case '<': tr[self].v = 16; break;
            case '>': tr[self].v = 32; break;
            case '?': tr[self].v = 48; tr[self].situation = (tr[self].situation << 1) % P; break;
        }
        return;
    }
    build(tr[self].ls = ++tr_cnt, l, bracket_l[r] - 2);
    build(tr[self].rs = ++tr_cnt, bracket_l[r] + 1, r - 1);
    tr[self].situation = (int64)tr[tr[self].ls].situation * tr[tr[self].rs].situation % P;
    switch (qs[bracket_l[r] - 1]) {
        case '<': tr[self].v = 16; break;
        case '>': tr[self].v = 32; break;
        case '?': tr[self].v = 48; tr[self].situation = (tr[self].situation << 1) % P; break;
    }
}

// 返回答案是 1 的方案数
int dp_mem[1024 + 3][N];
int g(int status, int u) {
    if (~dp_mem[status][u]) return dp_mem[status][u];
    if (tr[u].v < 10) return dp_mem[status][u] = ((status >> tr[u].v) & 1); // Leaf Node
    int64 l_1 = g(status, tr[u].ls), r_1 = g(status, tr[u].rs);
    int64 l = tr[tr[u].ls].situation, r = tr[tr[u].rs].situation;
    int64 l_0 = l - l_1, r_0 = r - r_1;
    switch (tr[u].v) {
        case 16: return dp_mem[status][u] = (l_1 * r_1) % P;
        case 32: return dp_mem[status][u] = (l * r - l_0 * r_0) % P;
        case 48: return dp_mem[status][u] = (l * r - l_0 * r_0 + l_1 * r_1) % P;
    }
}

int comp_val_id, what[10];
bool comp_a(int x, int y) { return a[x][comp_val_id] < a[y][comp_val_id]; }

signed main() {
    memset(dp_mem, -1, sizeof(dp_mem));
    kin >> n >> m;
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            kin >> a[i][j];
    kin >> (qs + 1);
    q = strlen(qs + 1);
    stack<int> bracket;
    // power2[0] = 1;
    for (int i = 1; i <= q; ++i) {
        // power2[i] = (power2[i - 1] << 1) % P;
        // cnt_q[i] = cnt_q[i - 1] + (qs[i] == '?');
        if (qs[i] == '(') bracket.push(i);
        else if (qs[i] == ')') {
            bracket_l[i] = bracket.top();
            bracket_r[bracket.top()] = i;
            bracket.pop();
        }
    }
    build(tr_cnt = 1, 1, q);
    int64 ans = 0;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) what[j] = j;
        comp_val_id = i;
        sort(what, what + m, comp_a);
        // what[x].rank = x
        int status = (1 << m) - 1;
        for (int j = 0; j < m; ++j) {
            int d = a[what[j]][i];
            if (j) d -= a[what[j - 1]][i];
            if (d) ans = (ans + (int64)d * g(status, 1)) % P;
            status &= ~(1 << what[j]);
        }
    }
    kout << (ans + P) % P << '\n';
    return 0;
}