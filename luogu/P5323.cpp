/*************************************
 * @problem:      [BJOI2019]光线.
 * @author:       brealid.
 * @time:         2020-11-19.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;
#if false /* 需要使用 fread 优化，改此参数为 true */
#define getchar() (i_P1 == i_P2 && (i_P2 = (i_P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), i_P1 == i_P2) ? EOF : *i_P1++)
char BUF[1 << 21], *i_P1 = BUF, *i_P2 = BUF;
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

template <int ModNum>
struct mint {
    int num;
    mint(int val = 0) : num(val) {
        while (val >= ModNum) val -= ModNum;
        while (val < 0) val += ModNum;
    }
    mint(int64 val) : num(int(val % ModNum)) {}
    operator bool() const { return num; }
    mint inv() {
        mint ret = 1, now = *this;
        int K = ModNum - 2;
        while (K) {
            if (K & 1) ret *= now;
            now *= now;
            K >>= 1;
        }
        return ret;
    }
    mint operator - () const { return mint(-num); }
    mint operator << (const int &b) const { return mint((int64)num << b); }
    mint operator >> (const int &b) const { return mint(num >> b); }
    mint operator + (const mint &b) const { return mint(num + b.num); }
    mint operator - (const mint &b) const { return mint(num - b.num); }
    mint operator * (const mint &b) const { return mint((int64)num * b.num); }
    mint operator / (const mint &b) const { return *this * b.inv(); }
    mint operator % (const mint &b) const { return mint(num % b.num); }
    mint operator + (const int &b) const { return *this + mint(b); }
    mint operator - (const int &b) const { return *this - mint(b); }
    mint operator * (const int &b) const { return *this * mint(b); }
    mint operator / (const int &b) const { return *this / mint(b); }
    mint operator % (const int &b) const { return *this % mint(b); }
    mint& operator += (const mint &b) { return *this = *this + b; }
    mint& operator -= (const mint &b) { return *this = *this - b; }
    mint& operator *= (const mint &b) { return *this = *this * b; }
    mint& operator /= (const mint &b) { return *this = *this / b; }
    mint& operator %= (const mint &b) { return *this = *this % b; }
    friend mint operator + (const int &a, const mint &b) { return mint(a) + b; }
    friend mint operator - (const int &a, const mint &b) { return mint(a) - b; }
    friend mint operator * (const int &a, const mint &b) { return mint(a) * b; }
    friend mint operator / (const int &a, const mint &b) { return mint(a) / b; }
    friend mint operator % (const int &a, const mint &b) { return mint(a) % b; }
    bool operator < (const mint &b) { return num < b.num; }
    bool operator > (const mint &b) { return num > b.num; }
    bool operator <= (const mint &b) { return num <= b.num; }
    bool operator >= (const mint &b) { return num >= b.num; }
    bool operator == (const mint &b) { return num == b.num; }
    bool operator != (const mint &b) { return num != b.num; }
};

const int N = 5e5 + 7, P = 1e9 + 7, inv100 = 570000004;

int n;
mint<P> a, b, reflect = 0, ans = 1;

signed main() {
    kin >> n;
    for (int i = 1; i <= n; ++i) {
        kin >> a.num >> b.num;
        a *= inv100, b *= inv100;
        mint<P> reflect_now = (P + 1 - reflect * b).inv();
        ans *= a * reflect_now;
        reflect = b + a * a * reflect * reflect_now;
    }
    kout << ans.num << '\n';
    return 0;
}