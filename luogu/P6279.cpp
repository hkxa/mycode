//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Favorite Colors 最喜欢的颜色.
 * @user_name:    brealid.
 * @time:         2020-05-31.
 * @language:     C++.
 * @upload_place: luogu.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// #define int int64
const int N = 200007;
int n, m;
vector<int> G[N];

int fa[N];
int dfn[N], dft, son[N];
int dep[N], depMax;
int col[N], cnt = 0;
bool vis[N];

int find(int u) {
    // return u != fa[u] ? fa[u] = find(fa[u]) : u;
    return fa[u] < 0 ? u : (fa[u] = find(fa[u]));
}

void combine(int u, int v) {
    // printf("combine(%d, %d)\n", u, v);
    int fu = find(u), fv = find(v);
    if (fu != fv) fa[fu] = fv;
    // if (fu != fv) {
    //     // assert(fu >= 0);
    //     // assert(fv >= 0);
    //     // assert(fa[fu] < 0);
    //     // assert(fa[fv] < 0);
    //     if (fa[fu] < fa[fv]) swap(fu, fv);
    //     fa[fu] += fa[fv];
    //     fa[fv] = fu;
    // }
    // fa[find(u)] = find(v);
}

void dfs(int u) {
    // printf("dfs(%d)\n", u);
    if (G[u].size() < 2) return;
    vector<int> T(G[u]);
    G[u].clear();
    int New = find(T[0]);
    for (unsigned i = 1; i < T.size(); i++) {
        if (G[find(T[i])].size() > G[New].size()) New = find(T[i]);
    }
    for (unsigned i = 0; i < T.size(); i++) {
        int y = find(T[i]);
        if (y == New) continue;
        for (unsigned j = 0; j < G[y].size(); j++) {
            G[New].push_back(G[y][j]);
        }
        G[y].clear();
        combine(y, New);
    }
    G[u].push_back(New);
    T.clear();
    dfs(New);
}

signed main() {
    n = read<int>();
    m = read<int>();
    memset(fa, -1, sizeof(fa));
    for (int i = 1, a, b; i <= m; i++) {
        a = read<int>();
        b = read<int>();
        G[a].push_back(b);
    }
    // for (int i = 1; i <= n; i++) fa[i] = i;
    for (int i = 1; i <= n; i++) dfs(i);
    for (int i = 1, f; i <= n; i++) {
        if (!col[f = find(i)]) col[f] = ++cnt;
        write(col[f], 10);
    }
    return 0;
}

// Create File Date : 2020-05-31

/**
 * Problem: 4698
 * User: zhaoyi20
 * Language: C++
 */