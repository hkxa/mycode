/*************************************
 * problem:      P3953 逛公园.
 * user ID:      85848.
 * user name:    hkxadpall.
 * time:         2019--09-13.
 * language:     C++.
 * upload place: Luogu.
**********P***************************/ 

#include <bits/stdc++.h>
using namespace std;
#define MemWithNum(array, num) memset(array, num, sizeof(array))
#define Clear(array) MemWithNum(array, 0)
#define MemBint(array) MemWithNum(array, 0x3f)
#define MemInf(array) MemWithNum(array, 0x7f)
#define MemEof(array) MemWithNum(array, -1)
#define ensuref(condition) do { if (!(condition)) exit(0); } while(0)

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

struct FastIOer {
#   define createReadlnInt(type)            \
    FastIOer& operator >> (type &x)         \
    {                                       \
        x = read<type>();                   \
        return *this;                       \
    }
    createReadlnInt(short);
    createReadlnInt(unsigned short);
    createReadlnInt(int);
    createReadlnInt(unsigned);
    createReadlnInt(long long);
    createReadlnInt(unsigned long long);
#   undef createReadlnInt

#   define createWritelnInt(type)           \
    FastIOer& operator << (type x)          \
    {                                       \
        write<type>(x);                     \
        return *this;                       \
    }
    createWritelnInt(short);
    createWritelnInt(unsigned short);
    createWritelnInt(int);
    createWritelnInt(unsigned);
    createWritelnInt(long long);
    createWritelnInt(unsigned long long);
#   undef createWritelnInt
    
    FastIOer& operator >> (char &x)
    {
        x = getchar();
        return *this;
    }
    
    FastIOer& operator << (char x)
    {
        putchar(x);
        return *this;
    }
    
    FastIOer& operator << (const char *x)
    {
        int __pos = 0;
        while (x[__pos]) {
            putchar(x[__pos++]);
        }
        return *this;
    }
} fast;

int n, m, k, p;
queue<int> q;
int vecSize[100007] = {0}, revVecSize[100007] = {0};
vector<int> to[100007], w[100007], revTo[100007], revW[100007];
bool inQueue[100007], dfsErr;
int dis[100007], revDis[100007];
int save[100007][52];

void clear()
{
    // for (int i = 0; i < 100007; i++) {
    //     to[i].clear();
    //     w[i].clear();
    //     revTo[i].clear();
    //     revW[i].clear();
    // }
    dfsErr = 0;
    Clear(vecSize);
    Clear(revVecSize);
    MemEof(save);
    // while (!q.empty()) q.pop();
    // Clear(inQueue);
}

void pushVector(vector<int> *v1, vector<int> *v2, int a, int b, int c, int *vecSize)
{
    if (vecSize[a] < v1[a].size()) {
        v1[a][vecSize[a]] = b;
        v2[a][vecSize[a]++] = c;
    } else {
        vecSize[a]++;
        v1[a].push_back(b);
        v2[a].push_back(c);
    }
}

void input()
{
    fast >> n >> m >> k >> p;
    int a, b, c;
    for (int i = 1; i <= m; i++) {
        fast >> a >> b >> c;
        pushVector(to, w, a, b, c, vecSize);
        pushVector(revTo, revW, b, a, c, revVecSize);
    }
    // putchar('%');
}

void spfa(vector<int> to[], vector<int> w[], int vecSize[], int dis[], int s)
{
    memset(dis, 0x3f, sizeof(int) * 100007);
    // Clear(inQueue);
    dis[s] = 0;
    inQueue[s] = 1;
    q.push(s);
    int x;
    while (!q.empty()) {
        x = q.front();
        q.pop();
        for (int i = 0; i < vecSize[x]; i++) {
            if (dis[to[x][i]] > dis[x] + w[x][i]) {
                dis[to[x][i]] = dis[x] + w[x][i];
                if (!inQueue[to[x][i]]) {
                    q.push(to[x][i]);
                    inQueue[to[x][i]] = true;
                }
            }
        }
        inQueue[x] = false;
    }
}

#define cost(u, i) (revDis[to[u][i]] + w[u][i] - revDis[u])

struct Zero {
    bool isZero;
    int zeroStart;
    Zero(bool val1 = 0, int val2 = 0) : isZero(val1), zeroStart(val2) {}
    bool operator == (const int nowPos) {
        return isZero && zeroStart == nowPos;
    }
    Zero operator + (const pair<int, int> p) {
        if (w[p.first][p.second] == 0) {
            if (!isZero) {
                return Zero(1, p.first);
            } else return Zero(1, zeroStart);
        } else return Zero();
    }
};

int dfs(int u, int rest, Zero z)
{
    if (z == u) {
        dfsErr = 1;
        return -1;
    }
    if (rest < 0) return 0;
    if (save[u][rest] != -1) return save[u][rest];
    // if (u == n) return 1;
    int tot = (u == n);
    for (int i = 0; i < vecSize[u]; i++) {
        tot = (tot + dfs(to[u][i], rest - cost(u, i), z + make_pair(u, i))) % p;
        if (dfsErr) return -1;
    }
    return save[u][rest] = tot;
}

int solve()
{
    spfa(to, w, vecSize, dis, 1);
    spfa(revTo, revW, revVecSize, revDis, n);
    return dfs(1, k, Zero());
}

int main()
{
    int T = read<int>();
    while (T--) {
        clear();
        input();
        fast << solve() << '\n';
    }
    return 0;
}