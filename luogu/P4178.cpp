//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Tree.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-11.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 988244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

const int N = 40007;

int n, k;
int vis[N];
int h[N], cnt;
vector<pair<int, int> > G[N];
int ans = 0;

int siz[N], root, rootMax;
void basicFindRoot_getSiz(int u) {
    siz[u] = 1;
    vis[u] = true;
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i].first, w = G[u][i].second;
        if (!vis[v]) {
            basicFindRoot_getSiz(v);
            siz[u] += siz[v];
        }
    }
    vis[u] = false;
}
void basicFindRoot_getRoot(int u, int si) {
    int tMax = 1;
    vis[u] = true;
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i].first, w = G[u][i].second;
        if (!vis[v]) {
            basicFindRoot_getRoot(v, si);
            tMax = max(tMax, siz[v]);
        }
    }
    tMax = max(tMax, si - siz[u]);
    if (tMax < rootMax) {
        root = u;
        rootMax = tMax;
    }
    vis[u] = false;
}

void findRoot(int u) {
    rootMax = n + 1;
    basicFindRoot_getSiz(u);
    basicFindRoot_getRoot(u, siz[u]);
}

void flatten(int u, int Dis) {
    vis[u] = true;
    h[cnt++] = Dis;
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i].first, w = G[u][i].second;
        if (!vis[v]) {
            flatten(v, Dis + w);
        }
    }
    vis[u] = false;
}

int calcAnswer(int u, int Dis) {
    cnt = 0;
    flatten(u, Dis);
    sort(h, h + cnt);
    int res = 0;
    // printf("flatten : { %d", h[0]);
    // for (int i = 1; i < cnt; i++) printf(", %d", h[i]);
    // printf(" }\n");
    for (int i = 0, j = cnt - 1; i < j; i++) {
        while (i < j && h[i] + h[j] > k) j--;
        res += j - i;
    }
    // printf("calcAnswer(%d, %d) = %d\n", u, Dis, res);
    return res;
}

void solve(int u) {
    // printf("solve(%d)\n", u);
    ans += calcAnswer(u, 0);
    vis[u] = true;
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i].first, w = G[u][i].second;
        if (!vis[v]) {
            ans -= calcAnswer(v, w);
        }
    }
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i].first, w = G[u][i].second;
        if (!vis[v]) {
            findRoot(v);
            solve(root);
        }
    }
    vis[u] = false;
}

signed main() {
    n = read<int>();
    for (int i = 1, u, v, w; i < n; i++) {
        u = read<int>();
        v = read<int>();
        w = read<int>();
        G[u].push_back(make_pair(v, w));
        G[v].push_back(make_pair(u, w));
    }
    k = read<int>();
    findRoot(1);
    solve(root);
    write(ans, 10);
    return 0;
}

// Create File Date : 2020-06-11