//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      【模板】扫描线.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jomoo/jmoo/航空信奥.
 * @time:         2020-06-09.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 988244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64
const int N = 2e5 + 7;

int n;
struct line {
    int x, y1, y2, k;
    bool operator < (const line &b) const {
        return x < b.x;
    }
} l[N];

int lsh[N], dif[N];
int64 ans = 0;
int64 len[N << 4]; int cnt[N << 4];

void Update(int ml, int mr, int dif, int u = 1, int l = 0, int r = n - 1) {
    if (l >= ml && r <= mr) {
        cnt[u] += dif;
        if (cnt[u] == 1)  len[u] = lsh[r] - lsh[l];
        else if (cnt[u] == 0) len[u] = len[u << 1] + len[u << 1 | 1];
        // printf("len[%d (%d~%d)] = %lld\n", u, l, r, len[u]);
        return;
    }
    int mid = (l + r) >> 1;
    if (mid > ml) Update(ml, mr, dif, u << 1, l, mid);
    if (mid < mr) Update(ml, mr, dif, u << 1 | 1, mid, r);
    if (cnt[u] == 0) len[u] = len[u << 1] + len[u << 1 | 1];
    // printf("len[%d (%d~%d)] = %lld\n", u, l, r, len[u]);
}

signed main() {
    n = read<int>();
    for (int i = 0, x1, y1, x2, y2; i < n; i++) {
        x1 = read<int>();
        y1 = read<int>();
        x2 = read<int>();
        y2 = read<int>();
        l[i << 1] = (line){x1, y1, y2, 1};
        l[i << 1 | 1] = (line){x2, y1, y2, -1};
        lsh[i << 1] = y1;
        lsh[i << 1 | 1] = y2;
    }
    n <<= 1;
    sort(lsh, lsh + n);
    sort(l, l + n);
    for (int i = 0; i < n; i++) {
        l[i].y1 = lower_bound(lsh, lsh + n, l[i].y1) - lsh;
        l[i].y2 = lower_bound(lsh, lsh + n, l[i].y2) - lsh;
    }
    Update(l[0].y1, l[0].y2, l[0].k);
    for (int i = 1; i < n; i++) {
        ans += len[1] * (l[i].x - l[i - 1].x);
        Update(l[i].y1, l[i].y2, l[i].k);
        // printf("ans -> %lld\n", ans);
    }
    write(ans, 10);
    return 0;
}

// Create File Date : 2020-06-09