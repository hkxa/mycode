/*************************************
 * @problem:      P3594 [POI2015]WIL-Wilcze doły.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-03-03.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, d;
int64 p;
int a[2000007];
int64 S[2000007];
pair<int, int64> q[2000007]; int head = 1, tail = 0;
int ans = 0;
#define sum second
#define id first

int main()
{
    n = read<int>();
    p = read<int64>();
    d = read<int>();
    for (int i = 1; i <= n; i++) {
        a[i] = read<int>();
        S[i] = S[i - 1] + a[i];
    }
    int i = 1, j = d;
    while (j <= n) {
        while (head <= tail && q[tail].sum < S[j] - S[j - d]) tail--;
        q[++tail] = make_pair(j - d + 1, S[j] - S[j - d]);
        while (S[j] - S[i - 1] - q[head].sum > p) {
            i++;
            if (q[head].id < i) head++;
        }
        ans = max(ans, j - i + 1);
        // for (int k = 1; k <= n; k++) 
        //     if (k >= i && k <= j) putchar('*');
        //     else putchar(32);
        // putchar(10);
        j++;
    }
    write(ans, 10);
    return 0;
}

/*
9 7 2
3 4 1 9 4 1 7 1 3
*/