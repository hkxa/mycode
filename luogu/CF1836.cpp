#include <bits/stdc++.h>
using namespace std;
typedef long long int64;

const int N = 1e6 + 7;

int n, k;
int64 m, a[N];
int64 rgl[N], rgr[N];
int cnt;


int main() {
    ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
    cin >> n >> m >> k;
    for (int i = 1; i <= n; ++i) cin >> a[i];
    if (m == 0) {
        if (n < k) cout << "1 0";
        else cout << "0 0";
        return 0;
    }
    sort(a + 1, a + n + 1);
    a[n + 1] = m;
    for (int i = k - 1; i <= n - k; ++i) {
        try_for_ans();
    }
    return 0;
}