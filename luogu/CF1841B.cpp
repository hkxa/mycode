#include <bits/stdc++.h>
using namespace std;


int T, q, x;

int main() {
    scanf("%d", &T);
    while (T--) {
        scanf("%d", &q);
        int seq_begin, seq_end;
        bool depart = false;
        for (int i = 0; i < q; ++i) {
            scanf("%d", &x);
            if (!i) {
                seq_begin = seq_end = x;
                putchar('1');
            } else if (depart) {
                if (seq_end <= x && x <= seq_begin) {
                    seq_end = x;
                    putchar('1');
                } else {
                    putchar('0');
                }
            } else {
                if (seq_end <= x) {
                    seq_end = x;
                    putchar('1');
                } else if (x <= seq_begin) {
                    seq_end = x;
                    putchar('1');
                    depart = true;
                } else {
                    putchar('0');
                }
            }
        }
        putchar('\n');
    }
}