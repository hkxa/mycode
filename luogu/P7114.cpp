#include <bits/stdc++.h>
typedef long long int64;

const int N = (1 << 20) + 7;
int T, n;
char s[N];
int tot_suf[N];
int now[26], cnt[N][27];
unsigned power131[N], hash_val[N];

signed main() {
    power131[0] = 1;
    for (int i = 1; i < N; ++i)
        power131[i] = power131[i - 1] * 131;
    scanf("%d", &T);
    while (T--) {
        scanf("%s", s + 1);
        n = strlen(s + 1);
        int tot = 0;
        memset(now, 0, sizeof(now));
        for (int i = 1; i <= n; ++i) {
            hash_val[i] = hash_val[i - 1] * 131 + (s[i] & 31);
            memcpy(cnt[i], cnt[i - 1], sizeof(cnt[i - 1]));
            if (++now[s[i] - 'a'] & 1) ++tot;
            else --tot;
            for (int j = tot; j <= 26; ++j) ++cnt[i][j];
        }
        memset(now, 0, sizeof(now));
        tot_suf[n + 1] = 0;
        for (int i = n; i >= 1; --i)
            if (++now[s[i] - 'a'] & 1) tot_suf[i] = tot_suf[i + 1] + 1;
            else tot_suf[i] = tot_suf[i + 1] - 1;
        int64 ans = 0;
        for (int ABlen = 2; ABlen < n; ++ABlen)
            for (int ABiL = ABlen; ABiL < n && (ABiL == ABlen || hash_val[ABlen] == hash_val[ABiL] - hash_val[ABiL - ABlen] * power131[ABlen]); ABiL += ABlen)
                ans += cnt[ABlen - 1][tot_suf[ABiL + 1]];
        printf("%lld\n", ans);
    }
    return 0;
}