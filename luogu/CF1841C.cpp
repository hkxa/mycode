#include <bits/stdc++.h>
using namespace std;


const int N = 2e5 + 7;
const int p10[5] = {1, 10, 100, 1000, 10000};

int T, n;
char s[N];
int f[N][5];

int main() {
    scanf("%d", &T);
    while (T--) {
        scanf("%s", s + 1);
        n = strlen(s + 1);
        f[0][0] = f[0][1] = f[0][2] = f[0][3] = f[0][4] = 0;
        for (int i = 1; i <= n; ++i) {
            int current = s[i] - 'A';
            for (int j = current + 1; j < 5; ++j)
                f[i][j] = f[i - 1][j] - p10[current];
            for (int j = 0; j <= current; ++j)
                f[i][j] = f[i - 1][current] + p10[current];
        }
        int current_value = 0, maximum_suffix = 0;
        int ans = -2e9;
        for (int i = n; i >= 1; --i) {
            int current = s[i] - 'A';
            // modify i
            for (int newv = 0; newv < maximum_suffix; ++newv)
                ans = max(ans, current_value + f[i - 1][maximum_suffix] - p10[newv]);
            for (int newv = maximum_suffix; newv < 5; ++newv)
                ans = max(ans, current_value + f[i - 1][newv] + p10[newv]);
            // update self
            if (current >= maximum_suffix) {
                maximum_suffix = current;
                current_value += p10[current];
            } else {
                current_value -= p10[current];
            }
        }
        printf("%d\n", ans);
    }
}