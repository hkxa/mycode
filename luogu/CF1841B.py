#TLE
import sys


input_from_stdin = sys.stdin.readline

T = int(input_from_stdin())
for _ in range(T):
    n = int(input_from_stdin())
    seq_begin = seq_end = -1
    ans = ''
    status_depart = False
    for x in map(int, input_from_stdin().split()):
        if seq_begin == -1:
            seq_begin = seq_end = x
            ans += '1'
        elif status_depart:
            if seq_end <= x <= seq_begin:
                seq_end = x
                ans += '1'
            else:
                ans += '0'
        else:
            if seq_end <= x:
                seq_end = x
                ans += '1'
            elif x <= seq_begin:
                seq_end = x
                ans += '1'
                status_depart = True
            else:
                ans += '0'
    print(ans)