/*************************************
 * @problem:      P5536 【XR-3】核心城市.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-01-27.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, k;
int dep[100007], fa[100007], deepest[100007], root;
vector<int> G[100007];

int dfs(int u, int father)
{
    dep[u] = dep[father] + 1;
    fa[u] = father;
    int nRet, nRes = u;
    for (vector<int>::iterator it = G[u].begin(); it != G[u].end(); it++) {
        if (*it != father) {
            nRet = dfs(*it, u);
            if (dep[nRet] > dep[nRes]) nRes = nRet;
        }
    }
    deepest[u] = dep[nRes];
    return nRes;
}

struct comp {
    bool operator () (const int a, const int b) const {
        return deepest[a] - dep[a] < deepest[b] - dep[b];
    }
};

int main()
{
    n = read<int>();
    k = read<int>();
    for (int i = 1, u, v; i < n; i++) {
        u = read<int>();
        v = read<int>();
        G[u].push_back(v);
        G[v].push_back(u);
    }
    root = dfs(1, 0);
    // printf("root = %d.\n", root);
    dep[0] = -1;
    root = dfs(root, 0);
    // printf("root = %d.\n", root);
    for (int up = dep[root] / 2; up; up--) root = fa[root];
    dfs(root, 0);
    priority_queue<int, vector<int>, comp> q;
    q.push(root);
    // printf("root = %d.\n"
    //        " node_id | fa | dep | deepest\n"
    //        "---------+----+-----+---------\n", root);
    // for (int i = 1; i <= n; i++) printf("    %d    | %d  |  %d  |    %d\n", i, fa[i], dep[i], deepest[i]);
    while (k--) {
        int todo = q.top(); q.pop();
        // printf("pop %d.\n", todo);
        for (vector<int>::iterator it = G[todo].begin(); it != G[todo].end(); it++) 
            if (*it != fa[todo]) q.push(*it);
    }
    write(deepest[q.top()] - dep[q.top()] + 1, 10);
    return 0;
}
/*
6 3
1 2
2 3
2 4
1 5
5 6
------> 1
7 3
1 2
2 3
3 4
4 5
5 6
6 7
------> 2
6 3
1 2
2 3
3 4
4 5
5 6
------> 2
*/