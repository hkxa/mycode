#include <bits/stdc++.h>
using namespace std;
typedef long long int64;

// #ifdef LOCAL
// #define LL_FORMAT "%I64D"
// #else
// #define LL_FORMAT "%lld"
// #endif


const int N = 2e5 + 7;

int T, n;
int64 m;
vector<int> whicha[N];
struct segment {
    int l, r, down_row;
    segment(int _l, int _r, int _d) : l(_l), r(_r), down_row(_d) {}
    bool operator < (const segment &b) const {
        return r < b.r;
    }
};
set<segment> s;
map<int, int64, greater<int> > mp;

int main() {
    scanf("%d", &T);
    while (T--) {
        scanf("%d", &n);
        for (int i = 1; i <= n; ++i) whicha[i].clear();
        for (int i = 1, ai; i <= n; ++i) {
            scanf("%d", &ai);
            whicha[ai].push_back(i);
        }
        scanf("%lld", &m);
        s.clear();
        s.insert(segment(1, n, n));
        mp.clear();
        int64 ans = 0;
        for (int i = n; i >= 1; --i) {
            for (int col: whicha[i]) {
                auto it = s.lower_bound(segment(col, col, 0));
                if (it == s.end()) continue;
                segment now = *it;
                s.erase(it);
                mp[now.r - now.l + 1] += now.down_row - i;
                if (now.l <= col - 1) s.insert(segment(now.l, col - 1, i));
                if (col + 1 <= now.r) s.insert(segment(col + 1, now.r, i));
            }
        }
        for (auto seg: s) mp[seg.r - seg.l + 1] += seg.down_row;
        for (auto pr: mp) {
            // printf("pr: %d %lld\n", pr.first, pr.second);
            if (m < pr.first * pr.second) {
                ans += m - (m + pr.first - 1) / pr.first;
                break;
            }
            m -= pr.first * pr.second;
            ans += (pr.first - 1) * pr.second;
        }
        printf("%lld\n", ans);
    }
}