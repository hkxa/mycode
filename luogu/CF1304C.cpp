/*************************************
 * @problem:      CF1304C Air Conditioner.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-02-16.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int T, n, ml, mr, t, las, l, r;

int main()
{
    T = read<int>();
    while (T--) {
        n = read<int>();
        ml = mr = read<int>();
        las = 0;
        while (n--) {
            t = read<int>();
            l = read<int>();
            r = read<int>();
            ml = ml - (t - las);
            mr = mr + (t - las);
            las = t;
            if (ml > r || mr < l) {
                puts("NO");
                while (n--) read<int>(), read<int>(), read<int>();
                break;
            } else if (!n) puts("YES");
            ml = max(ml, l);
            mr = min(mr, r);
        }
    }
    return 0;
}