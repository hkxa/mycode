/*************************************
 * @problem:      P2880 [USACO07JAN]平衡的阵容Balanced Lineup.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2019-11-12.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

struct Log2er {
    int _log2[50007];
    void init(int n)
    {
        _log2[0] = -1;
        for (int i = 1; i <= n; i++) _log2[i] = _log2[i >> 1] + 1;
    }
    int operator () (const int pos) const { return _log2[pos]; }
};

#define MxLg 16
template <typename _compType, typename _log2_geter = Log2er>
struct ST {
    _compType _comp;
    _log2_geter lg;
    int table[50007][18], n;
    void start_with(int n, int *a) 
    {
        lg.init(n);
        for (int i = 1; i <= n; i++) table[i][0] = a[i];
        for (int k = 1; k <= MxLg; k++) {
            for (int i = 1; i <= n - (1 << k) + 1; i++) {
                // printf("table[%d][%d] = _comp(table[%d][%d], table[%d][%d])\n", i, k, i, k - 1, i + (1 << (k - 1)), k - 1);
                table[i][k] = _comp(table[i][k - 1], table[i + (1 << (k - 1))][k - 1]) ? table[i][k - 1]: table[i + (1 << (k - 1))][k - 1];
                // printf("%d %d %d\n", table[i][k], table[i][k - 1], table[i + (1 << (k - 1))][k - 1]);
            }
        }
    }
    int query(int l, int r)
    {
        int lg_num = lg(r - l + 1);
        // printf("query table[%d][%d], table[%d][%d]\n", l, lg_num, r - (1 << lg_num) + 1, lg_num);
        return _comp(table[l][lg_num], table[r - (1 << lg_num) + 1][lg_num]) ? table[l][lg_num] : table[r - (1 << lg_num) + 1][lg_num];
    }
};

ST<less<int> > mn;
ST<greater<int> > mx;
#define init_all(n, arr) (mn.start_with(n, arr), mx.start_with(n, arr))
#define get(l, r) (mx.query(l, r) - mn.query(l, r))

int a[50007], n, q;

int main()
{
    n = read<int>();
    q = read<int>();
    for (int i = 1; i <= n; i++) a[i] = read<int>();
    init_all(n, a);
    for (int i = 1, l, r; i <= q; i++) {
        l = read<int>();
        r = read<int>();
        write(get(l, r), 10);
    }
    return 0;
}

