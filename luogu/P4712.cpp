/*************************************
 * @problem:      P4712 「生物」能量流动.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2019-12-28.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, p = 0;
int a[100006], r[100006];
double has[100006], ans = 0; 

int main()
{
    n = read<int>();
    has[0] = read<int>();
    for (int i = 1; i <= n; i++) {
        a[i] = read<int>();
        r[i] = read<int>();
        has[i] = 0;
    }
    for (int i = 1; i <= n; i++) {
        while (has[i] < a[i]) {
            if (p > r[i]) {
                puts("-1");
                return 0;
            }
            if (has[p] * 0.2 > (a[i] - has[i])) {
                // printf("%d eat %d energy %.2lf (get %.2lf)\n", i, p, 5 * (a[i] - has[i]), (a[i] - has[i]));
                has[p] -= 5 * (a[i] - has[i]);
                has[i] = a[i];
            } else {
                // printf("%d eat %d energy %.2lf (get %.2lf)\n", i, p, has[p], 0.2 * has[p]);
                has[i] += has[p] * 0.2;
                has[p] = 0;
                p++;
            } 
        }
    }
    // printf("p = %d.\n", p);
    for (int i = p; i <= n; i++) ans += has[i];
    printf("%.10lf", ans * 0.2);
    return 0;
}