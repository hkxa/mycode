/*************************************
 * @problem:      Cats Transport.
 * @author:       brealid.
 * @time:         2021-01-23.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;
#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 1e5 + 7, Person = 100 + 3;

int n, m, p;
int D[N], t[N];
int64 f[Person][N];
int64 t_sum[N];
struct funct {
    int64 k, b;
    funct(int64 K = 0, int64 B = 0) : k(K), b(B) {}
    int64 calc(int64 x) { return k * x + b; }
} q[N];
int head, tail;

bool is_better(const funct &a, const funct &b, const funct &n) {
    double intersect_an = double(a.b - n.b) / (n.k - a.k);
    double intersect_bn = double(b.b - n.b) / (n.k - b.k);
    return intersect_an <= intersect_bn;
}

signed main() {
    kin >> n >> m >> p;
    for (int i = 2; i <= n; ++i) D[i] = D[i - 1] + kin.get<int>();
    for (int i = 1, id, tim; i <= m; ++i) {
        kin >> id >> tim;
        t[i] = tim - D[id];
    }
    sort(t + 1, t + m + 1);
    for (int i = 1; i <= m; ++i)
        t_sum[i] = t_sum[i - 1] + t[i];
    // f[p][id] = min(f[p - 1][j] + (id - j) * t[id] - (sum_t[id] - sum_t[j]))
    memset(f[0], 0x3f, sizeof(f[0]));
    for (int i = 1; i <= p; ++i) {
        head = tail = 1;
        for (int which = 1; which <= m; ++which) {
            int64 now = t[which];
            while (head < tail && q[head].calc(now) > q[head + 1].calc(now)) ++head;
            f[i][which] = q[head].calc(now) + which * now - t_sum[which];
            // printf("(%lld + %lld) ", q[head].calc(now), which * t[which] - t_sum[which]);
            // printf("Feeder %d at cat %d: wait %lld second(s)\n", i, which, f[i][which]);
            funct cur(-which, f[i - 1][which] + t_sum[which]);
            while (head < tail && is_better(cur, q[tail], q[tail - 1])) --tail;
            q[++tail] = cur;
        }
    }
    kout << f[p][m] << '\n';
    return 0;
}
// 0 0 0 8 9 10