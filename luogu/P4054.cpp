/*************************************
 * @problem:      P4054 [JSOI2009]计数问题.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-01-21.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, m;
int tre[303][303][303];
int col[303][303];

#define lowbit(x) ((x) & (-(x)))

inline void add(int v, int x, int y, int z) 
{
    for (int i = x; i <= n; i += lowbit(i))
        for (int j = y; j <= m; j += lowbit(j))
            tre[v][i][j] += z;
}

inline int query(int v, int x, int y) {
    int ret = 0;
    for (int i = x; i > 0; i -= lowbit(i))
        for (int j = y; j > 0; j -= lowbit(j))
            ret += tre[v][i][j];
    return ret;
}

int main()
{
    n = read<int>();
    m = read<int>();
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++) {
            col[i][j] = read<int>();
            add(col[i][j], i, j, 1);
        } 
    }
    int q = read<int>(), x1, x2, y1, y2, c;
    while (q--) {
        if (read<int>() == 1) {
            x1 = read<int>();
            y1 = read<int>();
            add(col[x1][y1], x1, y1, -1);
            col[x1][y1] = read<int>();
            add(col[x1][y1], x1, y1, 1);
        } else {
            x1 = read<int>() - 1;
            x2 = read<int>();
            y1 = read<int>() - 1;
            y2 = read<int>();
            c = read<int>();
            printf("%d\n", query(c, x2, y2) - query(c, x1, y2) - query(c, x2, y1) + query(c, x1, y1));
        }
    }
    return 0;
}