/*************************************
 * @problem:      [清华集训2012]模积和.
 * @user_name:    brealid.
 * @time:         2020-11-12.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

const int64 P = 19940417, inv2 = 9970209, inv6 = 3323403;

int64 sum_i1(int64 l, int64 r) { return ((int64)(l + r) * (r - l + 1) >> 1) % P; }
int64 presum_i2(int64 n) { return (int64)n * (n + 1) % P * (n << 1 | 1) % P * inv6 % P; }
int64 sum_i2(int64 l, int64 r) { return (presum_i2(r) - presum_i2(l - 1) + P) % P; }
int64 solve_1(int64 n, int64 len) {
    int64 ret = 0;
    for (int64 l = 1, r; l <= len; l = r + 1) {
        r = min(n / (n / l), len);
        ret += sum_i1(l, r) * (n / l) % P;
    }
    return ret % P;
}
int64 solve_2(int64 n, int64 m) {
    int64 ret = 0;
    for (int64 l = 1, r; l <= min(n, m); l = r + 1) {
        r = min(n / (n / l), m / (m / l));
        ret += sum_i2(l, r) * (n / l) * (m / l) % P;
    }
    return ret % P;
}

int64 n, m;

signed main() {
    read >> n >> m;
    int64 F1_n = (n * n - solve_1(n, n)) % P;
    int64 F1_m = (m * m - solve_1(m, m)) % P;
    int64 F2_whole = min(n, m) * n % P * m % P;
    int64 F2_n = solve_1(n, min(n, m));
    int64 F2_m = solve_1(m, min(n, m));
    int64 F2_nm = solve_2(n, m);
    int64 F1 = F1_n * F1_m % P;
    int64 F2 = (F2_whole - m * F2_n - n * F2_m + F2_nm) % P;
    int64 ans = F1 - F2;
    write << (ans % P + P) % P << '\n';
    return 0;
}