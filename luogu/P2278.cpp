/*************************************
 * problem:      P2278 [HNOI2003]操作系统.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-03-31.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

struct node {
	int id, start, cost, pri;

	bool operator < (const node &other) const 
    {
		return pri != other.pri ? pri < other.pri : start > other.start;
	}
} t;

bool input(node &nd)
{
    return scanf("%d%d%d%d", &nd.id, &nd.start, &nd.cost, &nd.pri) != EOF;
}

priority_queue <node> wait;

int end = 0;

int main()
{
	while (input(t)) {
        while (!wait.empty() && end + wait.top().cost <= t.start) {
            node f = wait.top();
            wait.pop();
            end += f.cost;
            write(f.id, ' ');
            write(end, '\n');
        }
        if (!wait.empty()) {
            node f = wait.top();
            wait.pop();
            f.cost -= t.start - end;
            wait.push(f);
        }
        wait.push(t);
        end = t.start;
    }
    while (!wait.empty()){
        node f = wait.top();
        wait.pop();
        end += f.cost;
        write(f.id, ' ');
        write(end, '\n');
    }
    return 0;
}

