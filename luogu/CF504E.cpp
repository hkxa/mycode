/*************************************
 * @problem:      Misha and LCP on Tree.
 * @user_name:    brealid/hkxadpall/zhaoyi20/j(o)moo/littleTortoise.
 * @time:         2020-08-30.
 * @language:     C++.
 * @fastio_ver:   20200827.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        // simple io flags
        ignore_int = 1 << 0,    // input
        char_enter = 1 << 1,    // output
        flush_stdout = 1 << 2,  // output
        flush_stderr = 1 << 3,  // output
        // combination io flags
        endline = char_enter | flush_stdout // output
    };
    enum number_type_flags {
        // simple io flags
        output_double_stable = 1 << 0,  // output
        output_double_faster = 1 << 1   // output
        // combination io flags
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f & ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore character set : ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (double &lf) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            lf = endch & 15;
            while (isdigit(endch = getchar())) lf = lf * 10 + (endch & 15);
            if (endch == '.') {
                double digit = 0.1;
                while (isdigit(endch = getchar())) {
                    lf += (endch & 15) * digit;
                    digit *= 0.1;
                }
            }
            if (flag) lf = -lf;
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            Int d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return d;
        }
    } read;

    struct Writer {
        Writer& operator << (io_flags f) {
            if (f & char_enter) putchar(10);
            if (f & flush_stdout) fflush(stdout);
            if (f & flush_stderr) fflush(stderr);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
        inline void operator () (double val, int eps_digit = 6, char endch = '\0', number_type_flags flg = output_double_faster) {
            if (flg & output_double_stable) {
                static char output_format[10];
                sprintf(output_format, "%%.%dlf", eps_digit);
                printf(output_format, val);
            } else if (flg & output_double_faster) {
                if (val < 0) {
                    putchar('-');
                    val = -val;
                }
                double eps_number = 0.5;
                for (int i = 1; i <= eps_digit; i++) eps_number /= 10;
                val += eps_number;
                (*this) << (int64)val;
                val -= (int64)val;
                putchar('.');
                while (eps_digit--) {
                    val *= 10;
                    putchar((int)val | '0');
                    val -= (int)val;
                }
            } else {
                (*this) << "1.#ERROR_NOT_IDENTIFIED_FLAG";
            }
            if (endch) putchar(endch);
        }
    } write;
}
using namespace Fastio;
namespace File_IO {
    void init_IO() {
        freopen("Misha and LCP on Tree.in", "r", stdin);
        freopen("Misha and LCP on Tree.out", "w", stdout);
    }
}

// #define int int64

typedef pair<int, int> Hash;

const int N = 3e5 + 7, P = 1e9 + 7;
const Hash B = make_pair(131, 13331);
int n, m, dep[N], f[N], siz[N], wson[N], dfn[N], id[N], top[N], num;
Hash p[N], h1[N], h2[N];
char c[N];
vector<int> e[N];

inline Hash operator + (Hash a, Hash b) {
	return make_pair((a.first + b.first) % P, (a.second + b.second) % P);
}

inline Hash operator - (Hash a, Hash b) {
	return make_pair((a.first - b.first + P) % P, (a.second - b.second + P) % P);
}

inline Hash operator * (Hash a, Hash b) {
	return make_pair((int64)a.first * b.first % P, (int64)a.second * b.second % P);
}

inline Hash H(bool o, int x, int k) {
	if (o) return h2[x - k + 1] - h2[x + 1] * p[k];
	return h1[x + k - 1] - h1[x - 1] * p[k];
}

void dfs(int u) {
	siz[u] = 1;
    for (size_t i = 0; i < e[u].size(); i++) {
        int v = e[u][i];
		if (v != f[u]) {
			f[v] = u;
            dep[v] = dep[u] + 1;
            dfs(v);
            siz[u] += siz[v];
			if (siz[v] > siz[wson[u]]) wson[u] = v;
		}
    }
}

void dfs(int u, int p) {
	top[u] = p;
    dfn[u] = ++num;
    id[num] = u;
	if (wson[u]) dfs(wson[u], p);
    for (size_t i = 0; i < e[u].size(); i++) {
        int v = e[u][i];
		if (v != f[u] && v != wson[u]) dfs(v, v);
    }
}

inline int lca(int x, int y) {
	while (top[x] != top[y]) {
		if (dep[top[x]] > dep[top[y]]) swap(x, y);
		y = f[top[y]];
	}
	if (dep[x] > dep[y]) swap(x, y);
	return x;
}

inline vector<pair<int, int> > get(int x, int y) {
	int z = lca(x, y);
	vector<pair<int, int> > o, w;
	while (dep[top[x]] > dep[z]) {
        o.push_back(make_pair(x, top[x]));
        x = f[top[x]];
    }
	o.push_back(make_pair(x, z));
	while (dep[top[y]] > dep[z]) {
        w.push_back(make_pair(top[y], y));
        y = f[top[y]];
    }
	if (y != z) w.push_back(make_pair(wson[z], y));
	while (!w.empty()) o.push_back(w.back()), w.pop_back();
	return o;
}

signed main() {
    // File_IO::init_IO();
    read >> n;
    scanf("%s", c + 1);
    p[0] = make_pair(1, 1);
    p[1] = B;
	for (int i = 1, x, y; i < n; i++) {
        read >> x >> y;
        e[x].push_back(y);
        e[y].push_back(x);
        p[i + 1] = p[i] * B;
    }
	dep[1] = 1, dfs(1), dfs(1, 1);
	for (int i = 1; i <= n; i++) {
		h1[i] = h1[i - 1] * B + make_pair(c[id[i]], c[id[i]]);
		h2[n + 1 - i] = h2[n + 2 - i] * B + make_pair(c[id[n + 1 - i]], c[id[n + 1 - i]]);
    }
    read >> m;
	while (m--) {
		int a, b, c, d, ans = 0;
		uint32 s = 0, t = 0;
        read >> a >> b >> c >> d;
		vector<pair<int, int>> f = get(a, b), g = get(c, d);
		while (s < f.size() && t < g.size()) {
			int df1 = dfn[f[s].first], df2 = dfn[f[s].second];
			int dg1 = dfn[g[t].first], dg2 = dfn[g[t].second];
			bool of = df1 > df2, og = dg1 > dg2;
			int lf = (of ? df1 - df2 : df2 - df1) + 1;
			int lg = (og ? dg1 - dg2 : dg2 - dg1) + 1;
			int len = min(lf, lg);
			Hash hf = H(of, df1, len);
			Hash hg = H(og, dg1, len);
			if (hf == hg) {
				if (len == lf) ++s;
				else f[s].first = id[df1 + (of ? -1 : 1) * len];
				if (len == lg) ++t;
				else g[t].first = id[dg1 + (og ? -1 : 1) * len];
				ans += len;
			} else {
				int l = 1, r = len;
				while (l < r) {
					int mid = (l + r) >> 1;
					hf = H(of, df1, mid);
					hg = H(og, dg1, mid);
					if (hf == hg) l = mid + 1;
					else r = mid;
				}
				ans += l - 1; 
                break;
			}
		}
        write << ans << endline; 
	}
	return 0;
}