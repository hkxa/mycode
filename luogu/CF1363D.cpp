//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      Codeforces - Codeforces Round #646 (Div. 2).
 * @user_name:    hkxadpall.
 * @time:         2020-05-31.
 * @language:     C++.
 * @upload_place: Codeforces.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// #define int int64

int t;
int n, k;
vector<int> s[1024 + 7], rest;
bool occur[1024 + 7];
int ans[10 + 7], mx;

int query(vector<int> &t) {
    printf("? ");
    printf("%d ", t.size());
    for (unsigned i = 0; i < t.size(); i++)
        printf("%d ", t[i]);
    putchar(10);
            cout << endl;
    fflush(stdout);
    return read<int>();
}

char re[107];

signed main() {
    t = read<int>();
    while (t--) {
        n = read<int>();
        k = read<int>();
        memset(occur, 0, sizeof(occur));
        for (int i = 1, c, a; i <= k; i++) {
            c = read<int>();
            s[i].clear();
            while (c--) {
                a = read<int>();
                s[i].push_back(a);
                occur[a] = 1;
            }
        }
        rest.clear();
        for (int i = 1; i <= n; i++) {
            if (!occur[i]) rest.push_back(i);
        }
        int ret = 0;
        if (rest.size()) ret = query(rest);
        mx = ret;
        for (int i = 0; i < 10 && k >= (1 << i); i++) {
            printf("? ");
            int siz = 0;
            for (int j = 1; j <= k; j++) {
                if (j & (1 << i)) {
                    siz += s[j].size();
                }
            }
            printf("%d ", siz);
            for (int j = 1; j <= k; j++) {
                if (j & (1 << i)) {
                    for (int p = 0; p < s[j].size(); p++)
                    printf("%d ", s[j][p]);
                }
            }
            printf("\n");
            cout << endl;
            fflush(stdout);
            ans[i] = read<int>();
            mx = max(mx, ans[i]);
        }
        int Oc = 0, Ar;
        for (int i = 0; i < 10 && k >= (1 << i); i++) {
            if (ans[i] == mx) Oc |= (1 << i);
        }
        printf("? ");
        int siz = rest.size();
        for (int j = 1; j <= k; j++) {
            if (j != Oc) {
                siz += s[j].size(); 
            }
        }
        printf("%d ", siz);
        for (int p = 0; p < rest.size(); p++)
            printf("%d ", rest[p]);
        for (int j = 1; j <= k; j++) {
            if (j != Oc) {
                for (int p = 0; p < s[j].size(); p++)
                    printf("%d ", s[j][p]);
            }
        }
        printf("\n");
            cout << endl;
        fflush(stdout);
        Ar = read<int>();
        printf("! ");
        for (int j = 1; j <= k; j++) {
            if (j != Oc) write(mx, 32);
            else write(Ar, 32);
        }
        putchar(10);
            cout << endl;
        fflush(stdout);
        scanf("%s", re);
        if (re[0] == 'I') return 0;
    }
    return 0;
}