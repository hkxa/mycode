def solve():
    n = int(input())
    if n == 1:
        print(2)
    else:
        print((n + 2) // 3)

T = int(input())
while T:
    solve()
    T -= 1