/*************************************
 * problem:      P1816 忠诚. 
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-11-07.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, m;
int op, x, y; int64 k;
int64 a[100007];

struct data {
    int64 val;
    data() : val(0x7fffffff) {}
} t[100007 << 4];  

inline void build(int p, int l, int r)
{
    if (l > r) return;
    if (l == r) {
        t[p].val = a[l];
        return;
    }
    int mid = (l + r) >> 1;
    build(p << 1, l, mid);
    build(p << 1 | 1, mid + 1, r);
    t[p].val = min(t[p << 1].val, t[p << 1 | 1].val);
}

inline int64 query(int p, int l, int r, int ml, int mr)
{
    if (l > mr || r < ml) return 0x7fffffff;
    if (l >= ml && r <= mr) return t[p].val;
    int mid = (l + r) >> 1;
    return min(query(p << 1, l, mid, ml, mr), query(p << 1 | 1, mid + 1, r, ml, mr));
}

int main()
{
    n = read<int>();
    m = read<int>();
    for (int i = 1; i <= n; i++) a[i] = read<int>();
    build(1, 1, n);
    while (m--) {
        x = read<int>();
        y = read<int>();
        write(query(1, 1, n, x, y), 32);
    }
    return 0;
}