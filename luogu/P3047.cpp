/*************************************
 * problem:      P3047 [USACO12FEB]附近的牛Nearby Cows.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-20.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define puts_return(x) { puts(x); return 0; }
#define write_return(x) { write(x); return 0; }

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int c[100003];
int depth[100003];
int size[100003][23]; // size[node][第几代子孙]
int n, k;
vector<int> G[100003];

void init(int u, int fa)
{
    depth[u] = depth[fa] + 1;
    size[u][0] = c[u];
    for (int to : G[u]) {
        if (to == fa) continue;
        init(to, u);
        for (int i = 1; i <= k; i++) {
            size[u][i] += size[to][i - 1];
        }
    }
}

void dfs(int u, int fa)
{
    for (int to : G[u]) {
        if (to == fa) continue;
        for (int i = k; i >= 2; i--) {
            size[to][i] += size[u][i - 1] - size[to][i - 2];
        }
        size[to][1] += size[u][0];
        dfs(to, u);
    }
}

int main()
{
    n = read<int>();
    k = read<int>();
    int u, v;
    for (int i = 1; i < n; i++) {
        u = read<int>();
        v = read<int>();
        G[u].push_back(v);
        G[v].push_back(u);
    }
    for (int i = 1; i <= n; i++) {
        c[i] = read<int>();
    }
    init(1, 0);
    dfs(1, 0);
    int ans;
    for (int i = 1; i <= n; i++) {
        ans = 0;
        for (int j = 0; j <= k; j++) {
            ans += size[i][j];
        }
        write(ans, 10);
    }
    return 0;
}

/*
    1
    |
   / \
  2   5
  |
 / \
3   4
|
6
*/