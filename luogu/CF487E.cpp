//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      【CF Round #278】Tourists.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-11.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
        inline char get_option() {
            static char ForRead;
            ForRead = getchar();
            while (!isalnum(ForRead)) ForRead = getchar();
            return ForRead;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64
const int N = 200007;

namespace against_cpp11 {
    int n, m, q;
    vector<int> original_graph[N], G[N];
    namespace tarjan_values {
        int dfn[N], low[N], dft;
        bool instack[N];
        int sta[N], top;
    }
    int w[N];
    int square_node_cnt;
    int fa[N];
    namespace tree_chain_split_values {
        int dep[N], siz[N], wson[N];
        int dfn[N], beg[N], dft;
    }
    multiset<int> s[N];
    void tarjan(int u) {
        using namespace tarjan_values;
        low[u] = dfn[u] = ++dft;
        sta[++top] = u;
        instack[u] = true;
        for (size_t i = 0; i < original_graph[u].size(); i++) {
            int &v = original_graph[u][i];
            if (!dfn[v]) {
                tarjan(v);
                if (dfn[u] <= low[v]) {
                    ++square_node_cnt;
                    do {
                        instack[sta[top]] = false;
                        G[square_node_cnt].push_back(sta[top]);
                        G[sta[top]].push_back(square_node_cnt);
                    } while (sta[top--] != v);
                    G[square_node_cnt].push_back(u);
                    G[u].push_back(square_node_cnt);
                }
                low[u] = min(low[u], low[v]);
            } else if (instack[v] && 1) {
                low[u] = min(low[u], dfn[v]);
            }
        }
    }
    void TCS_dfs1(int u, int f) {
        using namespace tree_chain_split_values;
        fa[u] = f;
        dep[u] = dep[f] + 1;
        siz[u] = 1;
        for (size_t i = 0; i < G[u].size(); i++) {
            int &v = G[u][i];
            if (v != f) {
                TCS_dfs1(v, u);
                siz[u] += siz[v];
                if (siz[v] > siz[wson[u]]) wson[u] = v;
            }
        }
    }
    void TCS_dfs2(int u, int c_beg) {
        using namespace tree_chain_split_values;
        dfn[u] = ++dft;
        beg[u] = c_beg;
        if (wson[u]) TCS_dfs2(wson[u], c_beg);
        for (size_t i = 0; i < G[u].size(); i++) {
            int &v = G[u][i];
            if (v != fa[u] && v != wson[u]) TCS_dfs2(v, v);
        }
    }
    void pre_solve_muiltset(int u) {
        using namespace tree_chain_split_values;
        for (size_t i = 0; i < G[u].size(); i++) {
            int &v = G[u][i];
            if (v != fa[u]) {
                pre_solve_muiltset(v);
                if (u > n) s[u].insert(w[v]);
            }
        }
    }
    namespace seg_tree {
        int t[N << 2];
        void update(int u, int l, int r, int pos, int new_value) {
            if (l == r) return void(t[u] = new_value);
            int mid = (l + r) >> 1;
            if (pos <= mid) update(u << 1, l, mid, pos, new_value);
            else update(u << 1 | 1, mid + 1, r, pos, new_value);
            t[u] = min(t[u << 1], t[u << 1 | 1]);
        }
        int query(int u, int l, int r, int ml, int mr) {
            if (l > mr || r < ml) return INT_MAX;
            if (l >= ml && r <= mr) return t[u];
            int mid = (l + r) >> 1;
            return min(query(u << 1, l, mid, ml, mr), query(u << 1 | 1, mid + 1, r, ml, mr));
        }
        void update(int pos, int val) {
            update(1, 1, square_node_cnt, pos, val);
        }
        int query(int l, int r) {
            return query(1, 1, square_node_cnt, l, r);
        }
        void build_segment_tree() {
            using namespace tree_chain_split_values;
            memset(t, 0x7f, sizeof(t));
            // for (int i = 1; i <= n; i++) fprintf(stderr, "circle node %d : dfn = %d, w = %d\n", i, dfn[i], w[i]);
            // for (int i = n + 1; i <= square_node_cnt; i++) fprintf(stderr, "square node %d : dfn = %d, val = %d\n", i, dfn[i], *s[i].begin());
            for (int i = 1; i <= n; i++) update(dfn[i], w[i]);
            for (int i = n + 1; i <= square_node_cnt; i++) update(dfn[i], *s[i].begin());
        }
    };
    int query_chain_min(int u, int v) {
        using namespace tree_chain_split_values;
        int res = INT_MAX;
        while (beg[u] != beg[v]) {
            if (dep[beg[u]] < dep[beg[v]]) swap(u, v);
            // printf("query_chain_min : chain %d ~ %d\n", beg[u], u);
            res = min(res, seg_tree::query(dfn[beg[u]], dfn[u]));
            u = fa[beg[u]];
        }
        if (dep[u] > dep[v]) swap(u, v);
        // printf("query_chain_min : chain %d ~ %d\n", u, v);
        res = min(res, seg_tree::query(dfn[u], dfn[v]));
        if (u > n) res = min(res, w[fa[u]]); // square node
        return res;
    }
    signed main() {
        read >> n >> m >> q;
        for (int i = 1; i <= n; i++) read >> w[i];
        for (int i = 1, u, v; i <= m; i++) {
            read >> u >> v;
            original_graph[u].push_back(v);
            original_graph[v].push_back(u);
        }
        square_node_cnt = n;
        // fprintf(stderr, "line [%d] function %s : passing, without RE or TLE\n", __LINE__, __FUNCTION__);
        tarjan(1);
        // fprintf(stderr, "line [%d] function %s : passing, without RE or TLE\n", __LINE__, __FUNCTION__);
        TCS_dfs1(1, 0);
        // fprintf(stderr, "line [%d] function %s : passing, without RE or TLE\n", __LINE__, __FUNCTION__);
        TCS_dfs2(1, 1);
        // fprintf(stderr, "line [%d] function %s : passing, without RE or TLE\n", __LINE__, __FUNCTION__);
        pre_solve_muiltset(1);
        // fprintf(stderr, "line [%d] function %s : passing, without RE or TLE\n", __LINE__, __FUNCTION__);
        seg_tree::build_segment_tree();
        // fprintf(stderr, "line [%d] function %s : passing, without RE or TLE\n", __LINE__, __FUNCTION__);
        for (int i = 1, a, b; i <= q; i++) {
            if (read.get_option() == 'A') {
                read >> a >> b;
                // fprintf(stderr, "line [%d] function %s : Receive option %c %d %d\n", __LINE__, __FUNCTION__, 'A', a, b);
                // fprintf(stderr, "line [%d] function %s : output value : ", __LINE__, __FUNCTION__);
                write << query_chain_min(a, b) << '\n';
                // fprintf(stderr, "line [%d] function %s : Option deal finished\n", __LINE__, __FUNCTION__);
            } else {
                using tree_chain_split_values::dfn;
                read >> a >> b;
                // fprintf(stderr, "line [%d] function %s : Receive option %c %d %d\n", __LINE__, __FUNCTION__, 'C', a, b);
                if (a != 1) {
                    multiset<int> &pos = s[fa[a]];
                    pos.erase(pos.find(w[a]));
                    pos.insert(b);
                    seg_tree::update(dfn[fa[a]], *pos.begin());
                }
                w[a] = b;
                seg_tree::update(dfn[a], b);
                // fprintf(stderr, "line [%d] function %s : Option deal finished\n", __LINE__, __FUNCTION__);
            }
        }
        return 0;
    }
}

signed main() { return against_cpp11::main(); }