/*************************************
 * problem:      P2048 [NOI2010]超级钢琴.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-13.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, k, L, R;
int note[500007], sum_1toI[500007] = {0};
#define sum(S_l, S_r) (sum_1toI[S_r] - sum_1toI[(S_l - 1)])
#define Max_N (500000 + 3)
#define Max_LogN (19 + 3)
#define RMQ_SIZE Max_N][Max_LogN  

namespace RMQ {
    int st[RMQ_SIZE], id[RMQ_SIZE];
    int log2[Max_N];

    void init_log()
    {
        log2[0] = -1;
        for (int i = 1; i <= n; i++) log2[i] = log2[i >> 1] + 1;
    }

    void make_st()
    {
        for (int i = 1; i <= n; i++) {
            st[i][0] = sum_1toI[i];
            id[i][0] = i;
        }
        static int for_limit;
        for (int k = 1; k <= 19; k++) {
            for_limit = n - (1 << (k - 1));
            for (int i = 1; i <= for_limit; i++) {
                if (st[i][k - 1] > st[i + (1 << (k - 1))][k - 1]) {
                    st[i][k] = st[i][k - 1];
                    id[i][k] = id[i][k - 1];
                } else {
                    st[i][k] = st[i + (1 << (k - 1))][k - 1];
                    id[i][k] = id[i + (1 << (k - 1))][k - 1];
                }
            }
        }
    }

    pair<int, int> query(int l, int r)
    {
        static int lg;
        lg = log2[r - l + 1];
        if (st[l][lg] > st[r - (1 << lg) + 1][lg]) return make_pair(st[l][lg], id[l][lg]);
        else return make_pair(st[r - (1 << lg) + 1][lg], id[r - (1 << lg) + 1][lg]);
    }
}

// element @size = 4 [s, l, r, e]
struct ele {
    int s, l, r;
    int e;

    ele(int StartPoint, int EndSubL, int EndSubR) : 
        s(StartPoint), l(EndSubL), r(EndSubR), e(RMQ::query(EndSubL, EndSubR).second) 
    {
        // printf("q.push %d ~ <%d>%d<%d> (%d)\n", s, l, e, r, sum(s, e));
    }

    bool operator < (const ele &other) const {
        return sum(s, e) < sum(other.s, other.e);
    }
};

int main()
{
    n = read<int>();
    k = read<int>();
    L = read<int>();
    R = read<int>();
    for (int i = 1; i <= n; i++) {
        note[i] = read<int>();
        sum_1toI[i] = sum_1toI[i - 1] + note[i];
    }
    RMQ::init_log();
    RMQ::make_st();
    priority_queue<ele> q;
    for (int i = 1; i + L - 1 <= n; i++) {
        q.push(ele(i, i + L - 1, min(i + R - 1, n)));
    }
    long long ans(0);
    while (k--) {
        ele mx = q.top();
        q.pop();
        ans += sum(mx.s, mx.e);
        // printf("ans += %d ~ <%d>%d<%d> (%d)\n", mx.s, mx.l, mx.e, mx.r, sum(mx.s, mx.e));
        if (mx.e != mx.l) q.push(ele(mx.s, mx.l, mx.e - 1));
        if (mx.e != mx.r) q.push(ele(mx.s, mx.e + 1, mx.r));
    }
    write(ans, 10);
    return 0;
}