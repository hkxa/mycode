/*************************************
 * @problem:      Watching Fireworks is Fun.
 * @author:       brealid.
 * @time:         2021-01-09.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;
#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 150000 + 5, M = 300 + 5;

int n, m, d;
int a[M], b[M], t[M];
int64 *now, *lst;
int q[N], head, tail;

signed main() {
    kin >> n >> m >> d;
    t[0] = 1;
    for (int i = 1; i <= m; ++i) kin >> a[i] >> b[i] >> t[i];
    int ArraySize = sizeof(int64) * (n + 1);
    memset(now = new int64[n + 1], 0, ArraySize);
    memset(lst = new int64[n + 1], 0, ArraySize);
    for (int i = 1; i <= m; ++i) {
        int max_moving = min(int64(t[i] - t[i - 1]) * d, int64(n));
        head = 1, tail = 0;
        for (int j = 1; j <= n; ++j) {
            if (head <= tail && q[head] < j - max_moving) ++head;
            while (head <= tail && lst[q[tail]] <= lst[j]) --tail;
            q[++tail] = j;
            now[j] = lst[q[head]] + b[i] - abs(a[i] - j);
        }
        head = 1, tail = 0;
        for (int j = n; j >= 1; --j) {
            if (head <= tail && q[head] > j + max_moving) ++head;
            while (head <= tail && lst[q[tail]] <= lst[j]) --tail;
            q[++tail] = j;
            now[j] = max(now[j], lst[q[head]] + b[i] - abs(a[i] - j));
        }
        swap(now, lst);
    }
    int64 ans = 0xcfcfcfcfcfcfcfcf;
    for (int i = 1; i <= n; ++i) ans = max(ans, lst[i]);
    kout << ans << '\n';
    return 0;
}