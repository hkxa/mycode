#include <stdio.h>
#define lowbit(x) ((x) & (-x))

int n, a[1000007];
int t[1000007];
void add(int pos)
{
    while (pos <= n) {
        t[pos]++;
        pos += lowbit(pos);
    }
}

int query(int pos)
{
    int cnt = 0;
    while (pos) {
        cnt += t[pos];
        pos -= lowbit(pos);
    }
    return cnt;
}

long long KangTuo_arrayToNum()
{
    long long res = 0, th = 1, cnt;
    for (int i = n; i >= 1; i--) {
        cnt = query(a[i]);
        add(a[i]);
        res = (res + cnt * th) % 998244353;
        th = th * (n - i + 1) % 998244353;
    }
    return res;
}

int main()
{
    scanf("%d", &n);
    for (int i = 1; i <= n; i++) {
        scanf("%d", a + i);
    }
    printf("%lld", (KangTuo_arrayToNum() + 1) % 998244353);
}