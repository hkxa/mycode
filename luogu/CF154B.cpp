/*************************************
 * problem:      CF154B Colliders.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-11-11.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, m;
bool isnt[100007];
int table[20007], cnt = 0;
int has[100007];
bool on[100007];

void preparePrimes()
{
    for (int i = 2; i <= 100000; i++) {
        if (!isnt[i]) {
            table[++cnt] = i;
            for (int j = i + i; j <= 100000; j += i) {
                isnt[j] = 1;
            }
        }
    }
}

int test(int x)
{
    int bak = x;
    for (int i = 1; i <= cnt && table[i] * table[i] <= bak; i++) {
        if (x % table[i] == 0 && has[table[i]]) return has[table[i]];
        while (x % table[i] == 0) x /= table[i];
    }
    if (has[x]) return has[x];
    return 0;
}

void add(int x)
{
    on[x] = 1;
    int bak = x;
    for (int i = 1; i <= cnt && table[i] * table[i] <= bak; i++) {
        while (x % table[i] == 0) {
            has[table[i]] = bak;
            x /= table[i];
        }
    }
    if (x != 1) has[x] = bak;
}

void del(int x)
{
    on[x] = 0;
    int bak = x;
    for (int i = 1; i <= cnt && table[i] * table[i] <= bak; i++) {
        while (x % table[i] == 0) {
            has[table[i]] = 0;
            x /= table[i];
        }
    }
    if (x != 1) has[x] = 0;
}

int main()
{
    preparePrimes();
    n = read<int>();
    m = read<int>();
    char buf[3]; int t, x;
    for (int i = 1; i <= m; i++) {
        scanf("%s", buf);
        x = read<int>();
        switch (buf[0]) {
            case '+' :
                if (on[x]) puts("Already on");
                else if (t = test(x)) printf("Conflict with %d\n", t);
                else add(x), puts("Success");
                break;
            case '-' :
                if (!on[x]) puts("Already off");
                else del(x), puts("Success");
                break;
        }
    }
    return 0;
}