/*************************************
 * @problem:      code.
 * @author:       brealid.
 * @time:         2021-07-28.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

namespace kmath/* 数学资源库的名字 */ {
    typedef unsigned uint32;
    typedef long long int64;
    typedef unsigned long long uint64;

    template <typename T>
    inline T gcd_BitOptimize_ull(T a, T b) {
        if (!a || !b) return a | b;
        int t = __builtin_ctzll(a | b);
        a >>= __builtin_ctzll(a);
        do {
            b >>= __builtin_ctzll(b);
            if (a > b) swap(a, b);
            b -= a;
        } while(b);
        return a << t;
    }
    template <typename T>
    inline T gcd_BitOptimize_ui(T a, T b) {
        if (!a || !b) return a | b;
        int t = __builtin_ctz(a | b);
        a >>= __builtin_ctz(a);
        do {
            b >>= __builtin_ctz(b);
            if (a > b) swap(a, b);
            b -= a;
        } while(b);
        return a << t;
    }
    template <typename T>
    T gcd_EuclidMethod(T a, T b) {
        return a ? gcd_EuclidMethod(b % a, a) : b;
    }

    template <typename T>
    inline T fmul(T a, T b, T p) {
        T ret = 0;
        while (b) {
            if (b & 1) if ((ret += a) >= p) ret -= p;
            if ((a <<= 1) >= p) a -= p; 
            b >>= 1;
        }
        return ret;
    }
    template <typename T>
    inline T fpow_FmulMethod(T a, T b, T p) {
        T ret = 1;
        while (b) {
            if (b & 1) ret = fmul(ret, a, p);
            a = fmul(a, a, p);
            b >>= 1;
        }
        return ret;
    }
    template <typename T, typename UpTurn = int64>
    inline T fpow(T a, T b, T p) {
        T ret = 1;
        while (b) {
            if (b & 1) ret = (UpTurn)ret * a % p;
            a = (UpTurn)a * a % p;
            b >>= 1;
        }
        return ret;
    }
    template <typename T> inline T gcd(T a, T b) { return gcd_EuclidMethod(a, b); }
    template <> inline int64 gcd(int64 a, int64 b) { return gcd_BitOptimize_ull(a, b); }
    template <> inline uint64 gcd(uint64 a, uint64 b) { return gcd_BitOptimize_ull(a, b); }
    template <> inline int gcd(int a, int b) { return gcd_BitOptimize_ui(a, b); }
    template <> inline uint32 gcd(uint32 a, uint32 b) { return gcd_BitOptimize_ui(a, b); }

    template <typename T>
    inline T lcm(T n, T m) {
        return n / gcd(n, m) * m;
    }

    template<typename Ta, typename Tb, typename ...Targ>
    inline Ta gcd(Ta a, Tb b, Targ ...arg) {
        return gcd(gcd(a, b), arg...);
    }

    template<typename Ta, typename Tb, typename ...Targ>
    inline Ta lcm(Ta a, Tb b, Targ ...arg) {
        return lcm(lcm(a, b), arg...);
    }

    template <typename T>
    void exgcd(T n, T m, T &x, T &y) {
        if (n == 1) x = 1, y = 0;
        else exgcd(m, n % m, y, x), y -= n / m * x;
    }

    template <typename T>
    T inv(T a, T m) {
        T x, y;
        exgcd(a, m, x, y);
        return (x % m + m) % m;
    }

    template <typename T>
    inline T inv_FimaMethod(T a, T m) {
        return fpow(a, m - 2, m);
    }

    template<int N, int64 ModP>
    struct CombinationNumber {
        int64 fac[N + 2], ifac[N + 2];
        CombinationNumber() {
            fac[0] = 1;
            for (int i = 1; i <= N; ++i) fac[i] = fac[i - 1] * i % ModP;
            ifac[N] = inv(fac[N], ModP);
            for (int i = N; i >= 1; --i) ifac[i - 1] = ifac[i] * i % ModP;
        }
        inline int64 C(int n, int m) {
            return fac[n] * ifac[n - m] % ModP * ifac[m] % ModP;
        }
        inline int64 P(int n, int m) {
            return fac[n] * ifac[n - m] % ModP;
        }
    };
};

const int N = 1e5 + 7, P = 998244353;

// struct fraction {
//     int64 s, m;
//     fraction() {}
//     fraction(int64 S, int64 M): s(S), m(M) {}
// };

int n, q;
char s[N << 1];

namespace TEST_S {
    int f[N << 1], len;
    bool check() {
        return n <= 5000 && q <= 5000;
    }
    void make_f() {
        f[1] = 0;
        f[len = 2] = 1;
        for (int i = 1; i <= n; ++i)
            if (s[i] == 'W') ++f[len];
            else if (f[len] == 1) ++f[len - 1];
            else {
                --f[len];
                f[++len] = 1;
                f[++len] = 1;
            }
        // kout << "[";
        // for (int i = 1; i <= len; ++i) kout << f[i] << ", ";
        // kout << "\b\b] ";
    }
    void calc(int64 &s, int64 &m) {
        s = f[len], m = 1;
        for (int i = len - 1; i >= 1; --i) {
            int64 t = s;
            s = (m + s * f[i]) % P;
            m = t;
        }
    }
    void APPEND() {
        kin >> s[++n];
    }
    void FLIP() {
        static int l, r;
        kin >> l >> r;
        while (l <= r)
            if (s[l] == 'E') s[l++] = 'W';
            else s[l++] = 'E';
    }
    void REVERSE() {
        static int l, r;
        kin >> l >> r;
        while (l < r) swap(s[l++], s[r--]);
    }
    void solve() {
        char ss[20];
        int64 s, m;
        make_f(); calc(s, m);
        kout << s << ' ' << m << '\n';
        for (int i = 1; i <= q; ++i) {
            kin >> ss;
            if (ss[0] == 'A') APPEND();
            else if (ss[0] == 'F') FLIP();
            else REVERSE();
            make_f(); calc(s, m);
            kout << s << ' ' << m << '\n';
        }
    }
}

namespace TEST_A {
    namespace Core {
        int64 fib[N << 1];
        // int64 E[N << 1], W[N << 1];
        void init() {
            fib[1] = fib[2] = 1;
            for (int i = 3; i <= 200003; ++i)
                fib[i] = (fib[i - 1] + fib[i - 2]) % P;
        }
    }
    bool check() {
        for (int i = 1; i < n; ++i)
            if ((s[i] != 'E' || s[i + 1] != 'W') && (s[i] != 'W' || s[i + 1] != 'E')) return false;
        return true;
    }
    void solve() {
        Core::init();
        char state = s[1], op[20];
        int len = n, l, r;
        if (state == 'E') kout << Core::fib[len + 2] << ' ' << Core::fib[len + 1] << '\n';
        else kout << Core::fib[len + 1] << ' ' << Core::fib[len + 2] << '\n';
        for (int i = 1; i <= q; ++i) {
            kin >> op;
            if (op[0] == 'A') {
                kin >> op; // Ignore Character Remain
                ++len;
            } else if (op[0] == 'F') {
                kin >> l >> r;
                state = (state == 'E' ? 'W' : 'E');
            } else {
                kin >> l >> r;
                if ((r ^ l) & 1) state = (state == 'E' ? 'W' : 'E');
            }
            if (state == 'E') kout << Core::fib[len + 2] << ' ' << Core::fib[len + 1] << '\n';
            else kout << Core::fib[len + 1] << ' ' << Core::fib[len + 2] << '\n';
        }
    }
}

struct matrix {
    int64 a, b, c, d;
    matrix() {}
    matrix(int64 A, int64 B, int64 C, int64 D) : a(A), b(B), c(C), d(D) {}
    matrix operator * (const matrix &y) const {
        return matrix((a * y.a + b * y.c) % P, (a * y.b + b * y.d) % P,
                      (c * y.a + d * y.c) % P, (c * y.b + d * y.d) % P);
    }
    pair<int64, int64> mulR(int64 x, int64 y) const {
        return make_pair((a * x + b * y) % P, (c * x + d * y) % P);
    }
};

namespace TEST_BC {
    bool check() {
        return true;
    }
    int64 x, y;
    matrix now(1, 0, 0, 1);
    void deal(char ch) {
        if (ch == 'W') ++y;
        else if (y == 1) ++x;
        else {
            now = now * matrix(x, 1, 1, 0) * matrix(y - 1, 1, 1, 0);
            x = y = 1;
        }
    }
    void solve() {
        x = 0, y = 1;
        for (int i = 1; i <= n; ++i) deal(s[i]);
        pair<int64, int64> ans = (now * matrix(x, 1, 1, 0)).mulR(y, 1);
        kout << ans.first << ' ' << ans.second << '\n';
        char op[9];
        for (int i = 1; i <= q; ++i) {
            kin >> op >> op;
            deal(op[0]);
            ans = (now * matrix(x, 1, 1, 0)).mulR(y, 1);
            kout << ans.first << ' ' << ans.second << '\n';
        }
    }
}

signed main() {
    kin >> n >> q >> (s + 1);
    if (TEST_S::check()) TEST_S::solve();
    else if (TEST_A::check()) TEST_A::solve();
    else if (TEST_BC::check()) TEST_BC::solve();
    return 0;
}