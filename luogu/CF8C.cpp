//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Looking for Order.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-15.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

namespace against_cpp11 {
    const int N = 307;
    int X, Y;
    int n, mask;
    int x[N], y[N];
    int dis[N][N];
    int f[1 << 24];
    pair<int, int> from[1 << 24];
#   define dis(a, b) (x[a] * x[a] + y[a] * y[a] + x[b] * x[b] + y[b] * y[b] + (x[a] - x[b]) * (x[a] - x[b]) + (y[a] - y[b]) * (y[a] - y[b]))
    void outout_way(int status) {
        if (!status) {
            putchar('0');
            putchar(' ');
            return;
        }
        int a = from[status].first, b = from[status].second;
        if (a == b) {
            outout_way(status ^ (1 << a));
            write << a + 1 << ' ';
        } else {
            outout_way(status ^ (1 << a) ^ (1 << b));
            write << a + 1 << ' ' << b + 1 << ' ';
        }
        putchar('0');
        putchar(status == mask - 1 ? '\n' : ' ');
    }
    int get_lowerest_0_bit(const int &u) {
        int p = 0;
        while (u & (1 << p)) p++;
        return p;
    }
    signed main() {
        read >> X >> Y >> n;
        mask = 1 << n;
        for (int i = 0; i < n; i++) {
            read >> x[i] >> y[i];
            x[i] -= X;
            y[i] -= Y;
            for (int j = 0; j <= i; j++) dis[i][j] = dis[j][i] = dis(i, j);
        }
        for (int i = 1; i < mask; i++) f[i] = INT_MAX >> 1;
        for (int i = 0; i < mask - 1; i++) {
            int k = get_lowerest_0_bit(i);
            for (int j = k; j < n; j++) if (!(i & (1 << j))) {
                int &where = f[i | (1 << j) | (1 << k)], val = f[i] + dis[j][k];
                if (val < where) {
                    where = val;
                    from[i | (1 << j) | (1 << k)] = make_pair(j, k);
                }
            }
        }
        write << f[mask - 1] << '\n';
        outout_way(mask - 1);
        return 0;
    }
}

signed main() { return against_cpp11::main(); }