/*************************************
 * problem:      P1038 神经网络.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-07.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

struct Edge {
    int u, w;
};

int n, p;
int c[107];
vector<Edge> go[107];
vector<Edge> from[107];
int ind[107] = {0};
int outd[107] = {0};

int main()
{
    n = read<int>();
    p = read<int>();
    int status, Ui;
    for (int i = 1; i <= n; i++) {
        status = read<int>();
        Ui = read<int>();
        if (!status) c[i] = status - Ui;
        else c[i] = status;
    }
    int u, v, w;
    while (p--) {
        u = read<int>();
        v = read<int>();
        w = read<int>();
        go[u].push_back((Edge){v, w});
        from[v].push_back((Edge){u, w});
        ind[v]++;
        outd[u]++;
    }
    queue<int> q;
    for (int i = 1; i <= n; i++) {
        if (!ind[i]) q.push(i);
    }
    while (!q.empty()) {
        int u = q.front();
        q.pop();
        // printf("pop %d.\n", u);
        for (unsigned i = 0; i < go[u].size(); i++) {
            ind[go[u][i].u]--;
            // printf("inD[%d] : %d - 1 = %d.\n", go[u][i].u, ind[go[u][i].u] + 1, ind[go[u][i].u]);
            if (!ind[go[u][i].u]) {
                for (unsigned j = 0; j < from[go[u][i].u].size(); j++) {
                    if (c[from[go[u][i].u][j].u] > 0) c[go[u][i].u] += c[from[go[u][i].u][j].u] * from[go[u][i].u][j].w;
                }
                // printf("final : c[%d] = %d.\n", go[u][i].u, c[go[u][i].u]);
                q.push(go[u][i].u);
            }
        }
    }
    bool hasOutNode = false;
    for (int i = 1; i <= n; i++) {
        if (!outd[i]) {
            if (c[i] > 0) {
                write(i, 32);
                write(c[i], 10);
                hasOutNode = true;
            }
        }
    }
    if (!hasOutNode) puts("NULL");
    return 0;
}