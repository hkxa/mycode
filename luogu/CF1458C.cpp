/*************************************
 * @problem:      Latin Square.
 * @author:       brealid.
 * @time:         2021-02-04.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 1e3 + 7, M = 1e5 + 7;

int T, n, m;
int d[3], p[3], a[N][N][3], ans[N][N];
char op[M];

signed main() {
    for (kin >> T; T--;) {
        for (int i = 0; i < 3; ++i) d[p[i] = i] = 0;
        kin >> n >> m;
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < n; ++j) {
                a[i][j][0] = i;
                a[i][j][1] = j;
                a[i][j][2] = kin.get<int>() - 1;
            }
        kin >> (op + 1);
        for (int i = 1; i <= m; ++i)
            switch (op[i]) {
                case 'L': --d[1]; break;
                case 'R': ++d[1]; break;
                case 'U': --d[0]; break;
                case 'D': ++d[0]; break;
                case 'I': swap(d[1], d[2]); swap(p[1], p[2]); break;
                case 'C': swap(d[0], d[2]); swap(p[0], p[2]); break;
            }
        for (int i = 0; i < 3; ++i) d[i] = (d[i] % n + n) % n;
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < n; ++j) {
                int x = (a[i][j][p[0]] + d[0]) % n;
                int y = (a[i][j][p[1]] + d[1]) % n;
                int v = (a[i][j][p[2]] + d[2]) % n;
                ans[x][y] = v;
            }
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < n; ++j)
                kout << ans[i][j] + 1 << " \n"[j == n - 1];
        // kout << '\n';
    }
    return 0;
}

/*
    R(i,3) (((f[i]%=n)+=n)%=n);
    R(i,n)R(j,n){
        R(t,3) p[t]=(a[i][j][o[t]]+f[t])%n;
        b[p[0]][p[1]]=p[2];
    }
    R(i,n)R(j,n) cout<<b[i][j]+1<<" \n"[n-j==1];
    cout<<'\n';  
}
int main(){
    ios::sync_with_stdio(0);
    cin.tie(0),cout.tie(0);
    int t; for(cin>>t;t--;Main());
    return 0;
}
祝大家学习愉快！

 
*/