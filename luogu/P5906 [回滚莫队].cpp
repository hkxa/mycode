/*************************************
 * @problem:      【模板】回滚莫队&不删除莫队.
 * @author:       brealid.
 * @time:         2021-03-22.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 2e5 + 7, B = 1000 + 7;

int n, m;
int block, bcnt, belong[N], bl[B], br[B];
int a[N];
int Answer[N];

struct question {
    int l, r, id;
    bool operator < (const question &b) const {
        return (belong[l] ^ belong[b.l]) ? (belong[l] < belong[b.l]) : (r < b.r);
    }
} qry[N];

void discrete() {
    int Values[N];
    for (int i = 1; i <= n; ++i) Values[i] = a[i];
    sort(Values + 1, Values + n + 1);
    for (int i = 1; i <= n; ++i) a[i] = lower_bound(Values + 1, Values + n + 1, a[i]) - Values;
}

int clear_stk[N], top;
int first[N], last[N];

signed main() {
    kin >> n;
    block = pow(n, 0.50);
    bcnt = (n + block - 1) / block;
    for (int b = 1; b <= bcnt; ++b) {
        bl[b] = (b - 1) * block + 1;
        br[b] = min(b * block, n);
        for (int i = bl[b]; i <= br[b]; ++i) belong[i] = b;
    }
    for (int i = 1; i <= n; ++i) kin >> a[i];
    discrete();
    kin >> m;
    for (int i = 1; i <= m; ++i) {
        kin >> qry[i].l >> qry[i].r;
        qry[i].id = i;
    }
    sort(qry + 1, qry + m + 1);
    for (int i = 1, j = 1; i <= m; i = j) {
        int ans = 0, p = br[belong[qry[i].l]];
        for (; belong[qry[i].l] == belong[qry[j].l]; ++j) {
            if (belong[qry[j].l] == belong[qry[j].r]) {
                int now = 0;
                for (int p = qry[j].l; p <= qry[j].r; ++p) {
                    if (first[a[p]]) now = max(now, p - first[a[p]]);
                    else first[a[p]] = p, clear_stk[++top] = a[p];
                }
                Answer[qry[j].id] = now;
                for (; top; --top) first[clear_stk[top]] = last[clear_stk[top]] = 0;
                continue;
            }
            while (p < qry[j].r) {
                if (first[a[++p]]) ans = max(ans, p - first[a[p]]);
                else first[a[p]] = p, clear_stk[++top] = a[p];
                last[a[p]] = p;
            }
            int now = ans;
            for (int u = br[belong[qry[j].l]]; u >= qry[j].l; --u) {
                if (last[a[u]]) now = max(now, last[a[u]] - u);
                else last[a[u]] = u;
            }
            Answer[qry[j].id] = now;
            for (int u = br[belong[qry[j].l]]; u >= qry[j].l; --u)
                if (last[a[u]] == u) last[a[u]] = 0;
        }
        for (; top; --top) first[clear_stk[top]] = last[clear_stk[top]] = 0;
    }
    for (int i = 1; i <= m; ++i)
        kout << Answer[i] << '\n';
    return 0;
}