//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Nikita and game.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-07.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 3e5 + 7, INT_MIN_DIV_3 = INT_MIN / 3;

// struct heap {
//     priority_queue<int> que, del;
//     void push_del_tag() { 
//         while (!del.empty() && que.top() == del.top()) {
//             que.pop();
//             del.pop();
//         }
//     }
//     void insert(int u) { que.push(u); }
//     void erase(int u) { del.push(u); }
//     void top() { 
//         push_del_tag();
//         return que.empty() ? INT_MIN_DIV_3 : que.top();
//     }
// } dis[N], disf[N];

map<int, int> dis[N], disf[N];
map<int, int>::iterator it;

namespace against_cpp11 {
    int n, y[N], diam[N];
    int LCA_fa[N][20], dep[N];
    vector<int> G[N];
    void update_father_list(int u) {
        for (int i = 0; i < 19; i++)
            LCA_fa[u][i + 1] = LCA_fa[LCA_fa[u][i]][i];
    }
    void jump(int &u, int delta) {
        for (int k = 0; k < 20; k++)
            if (delta & (1 << k)) u = LCA_fa[u][k];
    }
    int LCA(int u, int v) {
        if (dep[u] > dep[v]) jump(u, dep[u] - dep[v]);
        else if (dep[u] < dep[v]) jump(v, dep[v] - dep[u]);
        if (u == v) return u;
        for (int k = 19; LCA_fa[u][0] != LCA_fa[v][0]; k--)
            if (LCA_fa[u][k] != LCA_fa[v][k]) {
                u = LCA_fa[u][k];
                v = LCA_fa[v][k];
            }
        return LCA_fa[u][0];
    }
    int dist(int u, int v) {
        return dep[u] + dep[v] - 2 * dep[LCA(u, v)];
    }
    vector<int> s1, s2;
    signed main() {
        read >> n;
        s1.push_back(1);
        n++;
        for (int i = 2, d1, d2, maxd = 0; i <= n; i++) {
            read >> y[i];
            LCA_fa[i][0] = y[i];
            update_father_list(i);
            dep[i] = dep[y[i]] + 1;
            G[y[i]].push_back(i);
            G[i].push_back(y[i]);
            d1 = s1.empty() ? 0 : dist(s1[0], i);
            d2 = s2.empty() ? 0 : dist(s2[0], i);
            if (d1 >= d2 && d1 > maxd) {
                maxd = d1;
                for (size_t j = 0; j < s2.size(); j++)
                    if (dist(i, s2[j]) == maxd)
                        s1.push_back(s2[j]);
                s2.clear();
                s2.push_back(i);
            } else if (d2 > d1 && d2 > maxd) {
                maxd = d2;
                for (size_t j = 0; j < s1.size(); j++)
                    if (dist(i, s1[j]) == maxd)
                        s2.push_back(s1[j]);
                s1.clear();
                s1.push_back(i);
            } else if (d1 == maxd) {
                s2.push_back(i);
            } else if (d2 == maxd) {
                s1.push_back(i);
            }
            write << s1.size() + s2.size() << '\n';
        }
        return 0;
    }
}

signed main() { return against_cpp11::main(); }