/*************************************
 * problem:      P2899 [USACO08JAN]手机网络Cell Phone Network.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-19.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define puts_return(x) { puts(x); return 0; }
#define write_return(x) { write(x); return 0; }

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, a[10007], fa[10007], depth[10007], type[10007];
int ans = 0;
vector<int> G[10007];

bool cmp(int x, int y) 
{
    return depth[x] > depth[y];
}

void dfs(int p, int f)
{
    fa[p] = f;
    depth[p] = depth[f] + 1;
    for (int nxt : G[p]) {
        if (nxt != f) dfs(nxt, p);
    }
}

#define HAS_SignalTower 2
#define NEAR_SignalTower 1
#define NO_SignalTower 0

int main()
{
    scanf("%d", &n);
    memset(type, NO_SignalTower, sizeof(type));
    int u, v;
    for (int i = 1; i <= n; i++) {
        a[i] = i;
    }
    for (int i = 2; i <= n; i++) {
        u = read<int>();
        v = read<int>();
        G[u].push_back(v);
        G[v].push_back(u);
    }
    dfs(1, 0);
    sort(a + 1, a + n + 1, cmp);
    for (int i = 1; i <= n; i++) {
        if ((type[fa[a[i]]] != HAS_SignalTower) && (type[a[i]] == NO_SignalTower)) {
            ans++;
            // printf("build in %d.\n", fa[a[i]]);
            type[a[i]] = NEAR_SignalTower;
            type[fa[a[i]]] = HAS_SignalTower;
            type[fa[fa[a[i]]]] = NEAR_SignalTower;
        }
    }
    write(ans);
    return 0;
}

