import sys


record = {}
current = 0

def do_add(k):
    global record, current
    print('+', k, flush=True)
    current += k
    result = int(input())
    if result in record:
        print('!', current - record[result], flush=True)
        sys.exit(0)
    record[result] = current

record[int(input())] = 0
for _ in range(1000):
    do_add(1)
for _ in range(1000):
    do_add(1000)