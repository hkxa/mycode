//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Dynamic Rankings.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-03.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

namespace against_cpp11 {
    const int N = 2e5 + 7, SIZ = 4e7 + 7;

#   define lowbit(x) ((x) & -(x))

    int n, m;
    int a[N], LSH[N], LSH_cnt;
    int root[N], val[SIZ], l[SIZ], r[SIZ], cnt;
    struct Query {
        bool type;
        int l, r, val;
    } q[N];

    inline char GetOpt() {
        static char ch;
        while (!isupper(ch = getchar()));
        return ch;
    }

    void modify_tree(int &u, int loc, int diffValue, int ml = 1, int mr = LSH_cnt) {
        if (!u) u = ++cnt;
        val[u] += diffValue;
        if (ml == mr) return;
        int mid = (ml + mr) >> 1;
        if (loc <= mid) modify_tree(l[u], loc, diffValue, ml, mid);
        else modify_tree(r[u], loc, diffValue, mid + 1, mr);
    }

    inline void modify(int pos, int loc, int diffValue) {
        while (pos <= LSH_cnt) {
            modify_tree(root[pos], loc, diffValue);
            pos += lowbit(pos);
        }
    }

    int uNow[N], vNow[N], uCnt, vCnt;

    int query_tree(int k, int ml = 1, int mr = LSH_cnt) {
        if (ml == mr) return ml;
        int mid = (ml + mr) >> 1, LefSize = 0;
        for (int i = 1; i <= uCnt; i++) LefSize += val[l[uNow[i]]];
        for (int i = 1; i <= vCnt; i++) LefSize -= val[l[vNow[i]]];
        if (k <= LefSize) {
            for (int i = 1; i <= uCnt; i++) uNow[i] = l[uNow[i]];
            for (int i = 1; i <= vCnt; i++) vNow[i] = l[vNow[i]];
            return query_tree(k, ml, mid);
        } else {
            for (int i = 1; i <= uCnt; i++) uNow[i] = r[uNow[i]];
            for (int i = 1; i <= vCnt; i++) vNow[i] = r[vNow[i]];
            return query_tree(k - LefSize, mid + 1, mr);
        }
    }

    inline int query(int l, int r, int kth) {
        // printf("query(%d, %d, %d)\n", l, r, kth);
        vCnt = uCnt = 0; 
        while (l) {
            // printf("Query Tree#%d : k - %d dif %d\n", l, kth, -1);
            vNow[++vCnt] = root[l];
            l -= lowbit(l);
        }
        while (r) {
            // printf("Query Tree#%d : k - %d dif %d\n", r, kth, 1);
            uNow[++uCnt] = root[r];
            r -= lowbit(r);
        }
        return query_tree(kth);
    }

    signed main() {
        read >> n >> m;
        for (int i = 1; i <= n; i++) LSH[i] = a[i] = read.get_int<int>();
        LSH_cnt = n;
        for (int i = 1; i <= m; i++) {
            if (GetOpt() == 'Q') {
                q[i].type = 0;
                read >> q[i].l >> q[i].r >> q[i].val;
            } else {
                q[i].type = 1;
                read >> q[i].l >> q[i].val;
                LSH[++LSH_cnt] = q[i].val;
            }
        }
        sort(LSH + 1, LSH + LSH_cnt + 1);
        for (int i = 1; i <= n; i++) {
            a[i] = lower_bound(LSH + 1, LSH + LSH_cnt + 1, a[i]) - LSH;
            modify(i, a[i], 1);
        }
        for (int i = 1; i <= m; i++) {
            if (q[i].type == 0) {
                write << LSH[query(q[i].l - 1, q[i].r, q[i].val)] << '\n';
            } else {
                modify(q[i].l, a[q[i].l], -1);
                a[q[i].l] = lower_bound(LSH + 1, LSH + LSH_cnt + 1, q[i].val) - LSH;
                modify(q[i].l, a[q[i].l], 1);
            }
        }
        return 0;
    }
}

signed main() { return against_cpp11::main(); }