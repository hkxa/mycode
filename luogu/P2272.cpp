//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      [ZJOI2007]最大半连通子图.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-16.
 * @language:     C++.
*************************************/ 
#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int X;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= X ? Module(w - X) : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % X; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

const int N = 100000 + 7;

int n, m;
vector<int> G[N], H[N];
int dfn[N], low[N], dft;
int fam[N], siz[N], cnt;

int s[N], top;
bool ins[N];
int ind[N];

int mx[N], ct[N];

void tarjan(int u) {
    low[u] = dfn[u] = ++dft;
    s[++top] = u;
    ins[u] = true;
    for (size_t i = 0; i < G[u].size(); i++) {
        if (!dfn[G[u][i]]) {
            tarjan(G[u][i]);
            low[u] = min(low[u], low[G[u][i]]);
        } else if (ins[G[u][i]]) {
            low[u] = min(low[u], dfn[G[u][i]]);
        }
    }
    if (dfn[u] == low[u]) {
        cnt++;
        while (low[s[top]] >= low[u]) {
            ins[s[top]] = false;
            fam[s[top--]] = cnt;
            siz[cnt]++;
        }
    }
}

signed main() {
    n = read<int>();
    m = read<int>();
    X = read<int>();
    for (int i = 1, a, b; i <= m; i++) {
        a = read<int>();
        b = read<int>();
        G[a].push_back(b);
    }
    for (int i = 1; i <= n; i++)
        if (!dfn[i]) tarjan(i);
    for (int i = 1; i <= n; i++)
        for (size_t j = 0; j < G[i].size(); j++) 
            if (fam[i] != fam[G[i][j]]) {
                H[fam[i]].push_back(fam[G[i][j]]);
            }
    for (int i = 1; i <= cnt; i++) {
        sort(H[i].begin(), H[i].end());
        H[i].erase(unique(H[i].begin(), H[i].end()), H[i].end());
        for (size_t j = 0; j < H[i].size(); j++) {
            // printf("%d -> %d\n", i, H[i][j]);
            ind[H[i][j]]++;
        }
    }
    queue<int> q;
    for (int i = 1; i <= cnt; i++) 
        if (!ind[i]) {
            mx[i] = siz[i];
            ct[i] = 1;
            q.push(i);
        }
    int u;
    while (!q.empty()) {
        u = q.front(); q.pop();
        for (size_t i = 0; i < H[u].size(); i++) {
            if (mx[H[u][i]] < mx[u] + siz[H[u][i]]) {
                mx[H[u][i]] = mx[u] + siz[H[u][i]];
                ct[H[u][i]] = ct[u];
            } else if (mx[H[u][i]] == mx[u] + siz[H[u][i]])
                ct[H[u][i]] = add(ct[H[u][i]], ct[u]);
            if (!--ind[H[u][i]])
                q.push(H[u][i]);
        }
    }
    int K = 0, C = 0;
    for (int i = 1; i <= cnt; i++) {
        if (mx[i] > K) {
            K = mx[i];
            C = ct[i];
        } else if (mx[i] == K)
            C = add(C, ct[i]);
    }
    write(K, 10);
    write(C, 10);
    return 0;
}

/*
6 7 20070603
1 2
2 1
1 3
2 3
2 4
5 6
6 4
*/

// Create File Date : 2020-06-15