//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      round 最少零.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-30.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

namespace AgainstCpp11 {
    const int N = 1e3 + 7;

    int n;
    int n2[N][N], n5[N][N];
    int f1[N][N], f2[N][N];
    char r1[N][N], r2[N][N];
    int x, y;

    typedef int IntArray[N][N];
    typedef char CharArray[N][N];

    void find_minist(IntArray mp, IntArray f, CharArray r) {
        for (int i = 1; i <= n; i++) f[i][0] = f[0][i] = 1e8;
        f[0][1] = 0;
        for (int i = 1; i <= n; i++)
            for (int j = 1; j <= n; j++)
                if (f[i - 1][j] < f[i][j - 1]) {
                    f[i][j] = f[i - 1][j] + mp[i][j];
                    r[i][j] = 'D';
                } else {
                    f[i][j] = f[i][j - 1] + mp[i][j];
                    r[i][j] = 'R';
                }
    }

    void outputAnswer(CharArray road, int x, int y) {
        if (x != 1 || y != 1) {
            if (road[x][y] == 'D') outputAnswer(road, x - 1, y);
            else outputAnswer(road, x, y - 1);
            putchar(road[x][y]);
        }
        if (x == n && y == n) putchar(10);
    }

    signed main() {
        read >> n;
        for (int i = 1, t; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                read >> t;
                if (!t) {
                    n2[i][j] = n5[i][j] = 1e7;
                    x = i; y = j;
                } else {
                    while (t % 2 == 0) {
                        n2[i][j]++;
                        t /= 2;
                    }
                    while (t % 5 == 0) {
                        n5[i][j]++;
                        t /= 5;
                    }
                }
            }
        }
        find_minist(n2, f1, r1);
        find_minist(n5, f2, r2);
        if ((!x && !y) || min(f1[n][n], f2[n][n]) <= 1) {
            write << min(f1[n][n], f2[n][n]) << '\n';
            if (f1[n][n] < f2[n][n]) outputAnswer(r1, n, n);
            else outputAnswer(r2, n, n);
        } else {
            puts("1");
            for (int i = 1; i < x; i++) putchar('D');
            for (int j = 1; j < y; j++) putchar('R');
            for (int i = x; i < n; i++) putchar('D');
            for (int j = y; j < n; j++) putchar('R');
            putchar(10);
        }
        return 0;
    }
}

signed main() { return AgainstCpp11::main(); }