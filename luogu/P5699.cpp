// run P5699 <P5699_addition_file\match1.in >P5699_addition_file\match1.usr
// P5699_addition_file\checker P5699_addition_file\match1.in P5699_addition_file\match1.usr EMPTY.txt
// run P5699 -f -q <P5699_addition_file\match10.in >P5699_addition_file\match10.usr & P5699_addition_file\checker P5699_addition_file\match10.in P5699_addition_file\match10.usr EMPTY.txt
#pragma GCC optimize("-O3")
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    register Int flag = 1;
    register char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    register Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    register Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    register Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;

int las[256 + 7], a[256 + 7], ans[256 + 7];
double now, vans;
double P[256 + 7][256 + 7];
int award[8 + 3];
double f[256 + 7][8 + 3];

int ans6_10[11][512 + 7] = {
    { 1, 47, 5, 53, 31, 8, 32, 62, 34, 43, 59, 20, 51, 63, 18, 45, 26, 15, 33, 60, 55, 48, 11, 56, 39, 52, 30, 16, 57, 37, 17, 25, 35, 64, 3, 29, 4, 27, 21, 40, 6, 19, 44, 61, 14, 2, 24, 22, 9, 10, 13, 42, 7, 54, 28, 12, 46, 58, 41, 23, 38, 49, 50, 36  },
    { 1, 21, 13, 5, 33, 54, 9, 55, 56, 3, 41, 17, 60, 14, 15, 50, 49, 40, 30, 29, 7, 4, 52, 62, 12, 32, 34, 37, 44, 10, 16, 53, 47, 23, 38, 31, 43, 42, 39, 20, 64, 19, 58, 8, 28, 25, 36, 24, 35, 51, 2, 27, 48, 11, 18, 46, 57, 61, 59, 45, 26, 63, 6, 22, 193, 213, 205, 197, 225, 246, 201, 247, 248, 195, 233, 209, 252, 206, 207, 242, 241, 232, 222, 221, 199, 196, 244, 254, 204, 224, 226, 229, 236, 202, 208, 245, 239, 215, 230, 223, 235, 234, 231, 212, 256, 211, 250, 200, 220, 217, 228, 216, 227, 243, 194, 219, 240, 203, 210, 238, 249, 253, 251, 237, 218, 255, 198, 214, 65, 85, 77, 69, 97, 118, 73, 119, 120, 67, 105, 81, 124, 78, 79, 114, 113, 104, 94, 93, 71, 68, 116, 126, 76, 96, 98, 101, 108, 74, 80, 117, 111, 87, 102, 95, 107, 106, 103, 84, 128, 83, 122, 72, 92, 89, 100, 88, 99, 115, 66, 91, 112, 75, 82, 110, 121, 125, 123, 109, 90, 127, 70, 86, 129, 149, 141, 133, 161, 182, 137, 183, 184, 131, 169, 145, 188, 142, 143, 178, 177, 168, 158, 157, 135, 132, 180, 190, 140, 160, 162, 165, 172, 138, 144, 181, 175, 151, 166, 159, 171, 170, 167, 148, 192, 147, 186, 136, 156, 153, 164, 152, 163, 179, 130, 155, 176, 139, 146, 174, 185, 189, 187, 173, 154, 191, 134, 150  },
    { 1, 126, 128, 125, 124, 123, 127, 121, 117, 120, 122, 119, 116, 115, 114, 118, 99, 97, 100, 102, 107, 103, 101, 104, 113, 109, 111, 110, 105, 106, 108, 112, 65, 66, 68, 72, 73, 70, 69, 71, 79, 77, 83, 78, 74, 76, 75, 80, 90, 89, 94, 92, 95, 93, 96, 98, 85, 87, 91, 88, 81, 84, 82, 86, 9, 8, 6, 7, 3, 5, 2, 4, 17, 14, 15, 16, 10, 11, 12, 13, 19, 18, 20, 22, 27, 23, 21, 24, 25, 28, 26, 29, 33, 30, 32, 31, 63, 64, 67, 61, 58, 62, 57, 60, 50, 52, 49, 54, 53, 56, 59, 55, 51, 47, 48, 46, 42, 45, 41, 44, 35, 36, 34, 40, 38, 43, 37, 39  },
    { 1, 42, 52, 120, 73, 10, 51, 97, 121, 78, 20, 70, 108, 90, 104, 15, 63, 110, 4, 46, 74, 40, 102, 24, 83, 65, 106, 81, 69, 89, 13, 25, 11, 98, 113, 95, 12, 21, 93, 100, 37, 68, 47, 115, 55, 9, 84, 62, 64, 57, 111, 126, 75, 3, 67, 14, 117, 60, 123, 16, 6, 43, 91, 66, 39, 94, 34, 125, 28, 112, 22, 35, 58, 79, 7, 82, 107, 101, 88, 127, 114, 128, 61, 50, 29, 59, 105, 2, 124, 85, 103, 41, 71, 119, 33, 76, 31, 17, 87, 32, 118, 36, 96, 38, 19, 109, 45, 5, 116, 8, 56, 23, 44, 99, 122, 80, 18, 72, 77, 27, 26, 92, 54, 30, 53, 86, 48, 49  },
    { 1, 58, 3, 49, 107, 84, 29, 45, 48, 86, 115, 17, 102, 47, 7, 50, 21, 42, 94, 26, 66, 91, 72, 60, 18, 38, 125, 104, 119, 16, 30, 51, 89, 34, 56, 55, 28, 117, 70, 6, 22, 33, 4, 90, 10, 95, 39, 108, 19, 109, 76, 81, 88, 65, 100, 36, 5, 85, 63, 32, 31, 62, 79, 69, 14, 114, 105, 8, 123, 2, 118, 27, 59, 20, 99, 57, 110, 13, 37, 87, 112, 53, 25, 122, 75, 71, 68, 15, 52, 61, 46, 97, 93, 23, 54, 82, 74, 103, 116, 128, 78, 111, 35, 80, 124, 41, 121, 96, 11, 98, 113, 101, 92, 126, 77, 12, 9, 67, 24, 40, 44, 43, 127, 64, 120, 83, 106, 73  } 
};

#define n16_StartT 20170933
#define n16_EndT 0.01
#define n16_LowerT 0.998
#define n16_Kexp 5.8e5
#define n16_ExchangeTimes 1 
// #define n16_SAtimes 18
#define n16_accept(dif, t) (rand() > exp((dif) / (t)) * n16_Kexp)

#define nMx_StartT 20170933
#define nMx_EndT 0.01
#define nMx_LowerT 0.9927
#define nMx_Kexp 6e5
#define nMx_ExchangeTimes 1 
#define nMx_SAtimes 18
#define nMx_accept(dif, t) (rand() > exp((dif) / (t)) * nMx_Kexp)
// #define nMx_accept(dif, t) (0)
// 13589844.591100177

double check(int L, int R, int dep)
{
	if (L == R) {
        f[a[L]][dep] = 1;
	    if (L == 1) return (f[1][dep + 1] - f[1][dep]) * award[dep];
        else return 0;
    }
	register int mid = (L + R) >> 1;
    register double res = 0;
	res += check(L, mid, dep + 1);
	res += check(mid + 1, R, dep + 1);
	for (register int i = L; i <= R; i++)
		f[a[i]][dep] = 0;
	for (register int i = L; i <= mid; i++)
		for (int j = mid + 1; j <= R; j++) {
			f[a[i]][dep] += f[a[i]][dep + 1] * f[a[j]][dep + 1] * P[a[i]][a[j]];
			f[a[j]][dep] += f[a[i]][dep + 1] * f[a[j]][dep + 1] * P[a[j]][a[i]];
		}
	if (L == 1) res += (f[1][dep + 1] - f[1][dep]) * award[dep];
    return res;
}

#define get() (check(1, n, 1) + f[1][1] * award[0])

void SA(const bool Ha)
{
    if (Ha) random_shuffle(a + 2, a + n + 1);
    register double temperature = n16_StartT;
    while (temperature >= n16_EndT && clock() < CLOCKS_PER_SEC * 0.85) {
        for (register int i = 1; i <= n; i++) las[i] = a[i];
        swap(a[rand() % (n - 1) + 2], a[rand() % (n - 1) + 2]);
        // for (register int i = 0; i < ExchangeTimes; i++) swap(a[rand() % (n - 1) + 2], a[rand() % (n - 1) + 2]);
        now = get();
        if (now > vans) {
            vans = now;
            for (int i = 1; i <= n; i++) ans[i] = a[i];
        } else if (!n16_accept(abs(now - vans), temperature)) 
            for (register int i = 1; i <= n; i++) a[i] = las[i];
        temperature *= n16_LowerT;
    }
}

void MxSA(const bool Ha)
{
    if (Ha) random_shuffle(a + 2, a + n + 1);
    register double temperature = nMx_StartT;
    while (temperature >= nMx_EndT && clock() < CLOCKS_PER_SEC * 0.85) {
        for (register int i = 1; i <= n; i++) las[i] = a[i];
        swap(a[rand() % (n - 1) + 2], a[rand() % (n - 1) + 2]);
        // for (register int i = 0; i < ExchangeTimes; i++) swap(a[rand() % (n - 1) + 2], a[rand() % (n - 1) + 2]);
        now = get();
        if (now > vans) {
            vans = now;
            for (int i = 1; i <= n; i++) ans[i] = a[i];
        } else if (!nMx_accept(abs(now - vans), temperature)) 
            for (register int i = 1; i <= n; i++) a[i] = las[i];
        temperature *= nMx_LowerT;
    }
}

int main()
{
    srand(time(0) ^ clock() ^ 20170933);
    n = read<int>();
    for (register int i = 1; i <= n; i++) a[i] = i;
    for (register int i = 1; i <= n; i++) 
        for (register int j = 1; j <= n; j++)
            scanf("%lf", P[i] + j);
    for (register int i = log2(n) + 0.1; i >= 0; i--) 
        scanf("%d", award + i);
    bool F = 0;
    if (n <= 16) {
        while (clock() < CLOCKS_PER_SEC * 0.85) {
            SA(!F);
            if (!F) F = 1;
        }
        // for (int i = 0; i < n16_SAtimes; i++) SA();
    } else {
        if (n == 128) {
            for (int j = 8; j <= 10; j++) {
                for (int i = 1; i <= n; i++) {
                    a[i] = ans6_10[j - 6][i - 1];
                }
                now = get();
                if (now > vans) {
                    vans = now;
                    for (int i = 1; i <= n; i++) ans[i] = a[i];
                } 
            }
            for (int i = 1; i <= n; i++) a[i] = ans[i];
        } else if (n == 256) {
            for (int i = 1; i <= n; i++) {
                ans[i] = a[i] = ans6_10[1][i - 1];
            }
            vans = now = get();
        } else if (n == 64) {
            for (int i = 1; i <= n; i++) {
                ans[i] = a[i] = ans6_10[0][i - 1];
            }
            vans = now = get();
        }
        while (clock() < CLOCKS_PER_SEC * 0.85) {
            MxSA(!F);
            if (!F) F = 1;
        }
        // for (int i = 0; i < nMx_SAtimes; i++) SA();
    }
    for (register int i = 1; i <= n; i++) write(ans[i], 10);
    return 0;
}