//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      可持久化并查集.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-17.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 2e5 + 7, SGT_SIZE = N * 40;

int n, m;

struct SGT_node {
    unsigned fa : 19;
    unsigned siz : 19;
    unsigned l : 25;
    unsigned r : 25;
} t[SGT_SIZE];
int cnt;
int root[N];

void build(int u, int l, int r) {
    if (l == r) {
        t[u].fa = l;
        t[u].siz = 1;
        return;
    }
    int mid = (l + r) >> 1;
    build(t[u].l = ++cnt, l, mid);
    build(t[u].r = ++cnt, mid + 1, r);
}

int query(int u, int l, int r, int pos) {
    if (l == r) return u;
    int mid = (l + r) >> 1;
    if (pos <= mid) return query(t[u].l, l, mid, pos);
    else return query(t[u].r, mid + 1, r, pos);
}

void modify_siz(int lst, int u, int l, int r, int pos, int new_val) {
    if (l == r) {
        t[u].fa = t[lst].fa;
        t[u].siz = new_val;
        return;
    }
    int mid = (l + r) >> 1;
    if (pos <= mid) {
        t[u].r = t[lst].r;
        int lst_l = t[lst].l;
        modify_siz(lst_l, t[u].l = ++cnt, l, mid, pos, new_val);
    } else {
        t[u].l = t[lst].l;
        int lst_r = t[lst].r;
        modify_siz(lst_r, t[u].r = ++cnt, mid + 1, r, pos, new_val);
    }
}

void modify_fa(int lst, int u, int l, int r, int pos, int new_val) {
    // printf("modify_fa(%d, %d, %d, %d, %d, %d)\n", lst, u, l, r, pos, new_val);
    if (l == r) {
        t[u].fa = new_val;
        t[u].siz = t[lst].siz;
        return;
    }
    int mid = (l + r) >> 1;
    if (pos <= mid) {
        t[u].r = t[lst].r;
        int lst_l = t[lst].l;
        modify_fa(lst_l, t[u].l = ++cnt, l, mid, pos, new_val);
    } else {
        t[u].l = t[lst].l;
        int lst_r = t[lst].r;
        modify_fa(lst_r, t[u].r = ++cnt, mid + 1, r, pos, new_val);
    }
}

void roll_back(int lst, int now) {
    root[now] = ++cnt;
    t[cnt].l = t[root[lst]].l;
    t[cnt].r = t[root[lst]].r;
}

int query_fa(int ed, int u) {
    int faId = query(root[ed], 1, n, u);
    return u == t[faId].fa ? faId : query_fa(ed, t[faId].fa);
}

void unify(int ed, int u, int v) {
    roll_back(ed - 1, ed);
    u = query_fa(ed, u);
    v = query_fa(ed, v);
    if (t[u].fa == t[v].fa) return;
    if (t[u].siz < t[v].siz) swap(u, v);
    modify_siz(root[ed], root[ed], 1, n, t[u].fa, t[u].siz + t[v].siz);
    modify_fa(root[ed], root[ed], 1, n, t[v].fa, t[u].fa);
}

int query_family(int ed, int u, int v) {
    roll_back(ed - 1, ed);
    u = query_fa(ed, u);
    v = query_fa(ed, v);
    return t[u].fa == t[v].fa;
}

signed main() {
    read >> n >> m;
    build(root[0] = ++cnt, 1, n);
    // for (int j = 1; j <= n; j++) 
    //     printf("%d%c", t[query(root[0], 1, n, j)].fa, " \n"[j == n]);
    for (int i = 1, opt, a, b; i <= m; i++) {
        read >> opt;
        if (opt == 1) {
            read >> a >> b;
            unify(i, a, b);
            // for (int j = 1; j <= n; j++) 
            //     printf("[%d, %d]%c", t[query(root[i], 1, n, j)].fa, t[query(root[i], 1, n, j)].siz, " \n"[j == n]);
        } else if (opt == 2) {
            read >> a;
            roll_back(a, i);
        } else {
            read >> a >> b;
            write << query_family(i, a, b) << '\n';
        }
    }
    return 0;
}