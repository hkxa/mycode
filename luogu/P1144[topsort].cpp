//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      最短路计数.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-10.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 100003;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

int n, m;
vector<int> G[100007], U[100007];
int dis[100007], cnt[100007];
int ind[100007];

void spfa() {
    queue<int> q;
    q.push(1);
    memarr(n, 0x3fffffff, dis);
    dis[1] = 0;
    cnt[1] = 1;
    while (!q.empty()) {
        int f = q.front();
        q.pop();
        for (size_t i = 0; i < G[f].size(); i++) {
            int to = G[f][i];
            if (dis[to] > dis[f] + 1) {
                dis[to] = dis[f] + 1;
                q.push(to);
            }
        }
    }
    for (int f = 1; f <= n; f++)
        for (size_t i = 0; i < G[f].size(); i++) {
            int to = G[f][i];
            if (dis[to] == dis[f] + 1) {
                U[f].push_back(to);
                ind[to]++;
            }
        }
}

void topsort() {
    queue<int> q;
    cnt[1] = 1;
    q.push(1);
    while (!q.empty()) {
        int f = q.front();
        q.pop();
        for (size_t i = 0; i < U[f].size(); i++) {
            int to = U[f][i];
            cnt[to] = add(cnt[to], cnt[f]);
            if (!--ind[to]) q.push(to);
        }
    }
}

signed main() {
    n = read<int>();
    m = read<int>();
    for (int i = 1, a, b; i <= m; i++) {
        a = read<int>();
        b = read<int>();
        G[a].push_back(b);
        G[b].push_back(a);
    }
    spfa();
    topsort();
    for (int i = 1; i <= n; i++) write(cnt[i], 10);
    return 0;
}

// Create File Date : 2020-06-10