/*************************************
 * @problem:      P2559 [AHOI2002]哈利·波特与魔法石.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-01-30.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int cost[] = {0, 2, 6, 4, 8, 6, 10, 14};
int s, e, n;
int dis[107];
bool vis[107];
vector<pair<int, int> > G[10007];
#define to first
#define val second

int main()
{
    for (int i = 1; i <= 7; i++) {
        if (read<int>()) cost[i] /= 2;
    }
    s = read<int>();
    e = read<int>();
    n = read<int>();
    for (int i = 1, f, t, v; i <= n; i++) {
        f = read<int>();
        t = read<int>();
        v = read<int>();
        G[f].push_back(make_pair(t, cost[v]));
        G[t].push_back(make_pair(f, cost[v]));
    }
    memset(dis, 0x3f, sizeof(dis));
    queue<int> q;
    dis[s] = 0;
    vis[s] = 1;
    q.push(s);
    while (!q.empty()) {
        int t = q.front(); q.pop();
        // printf("pop %d : dis[%d] = %d.\n", t, t, dis[t]);
        for (unsigned i = 0; i < G[t].size(); i++) {
            if (dis[G[t][i].to] > dis[t] + G[t][i].val) {
                dis[G[t][i].to] = dis[t] + G[t][i].val;
                if (!vis[G[t][i].to]) {
                    vis[G[t][i].to] = 1;
                    q.push(G[t][i].to);
                }
            }
        }
        vis[t] = 0;
    }
    write(dis[e], 10);
    return 0;
}