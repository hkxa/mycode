// 100pts
#include <bits/stdc++.h>
using namespace std;
#define openFile(x) freopen(x".in","r",stdin);freopen(x".out","w",stdout)

char READ_CH;
template <typename I>
inline I read()
{
    bool READ_NEG_FLAG = 0;
    READ_CH = getchar();
    while (!isdigit(READ_CH) && READ_CH != '-') READ_CH = getchar();
    if (READ_CH == '-') READ_NEG_FLAG = 1, READ_CH = getchar();
    I num = READ_CH & 15;
    while (isdigit(READ_CH = getchar())) num = (((num << 2) + num) << 1) + (READ_CH & 15);
    if (READ_NEG_FLAG) num = ~num + 1;
    return num;
}

int n;
char s[500007];
int fa[500007];
long long ans[500007], final_ans = 0;

inline bool check(string x)
{
    // 由于下面的逻辑原因，这里 check 的实际是翻转的串
    // printf("check %s\n", x.c_str());
    if (x.size() & 1) return 0;
    // printf("S1\n");
    int cnt = 0;
    for (unsigned i = 0; i < x.size(); i++) {
        if (x[i] == '(') { // 若串 x 未被翻转，应写成 x[i] == ')' 
            if (!cnt) return 0;
            else cnt--;
        } else cnt++;
    }
    // printf("S2 : %d\n", !cnt);
    return !cnt;
}

int Subtask_1to4()
{
    ans[1] = 0;
    string t;
    for (int i = 2, u; i <= n; i++) {
        // printf("\r%d...", i);
        ans[i] = ans[fa[i]];
        t = s[i];
        u = i;
        while (u) {
            if (check(t)) ans[i]++;
            u = fa[u];
            t += s[u];
        }
        // printf("ans[%d] = %d.\n", i, ans[i]);
        final_ans ^= i * ans[i];
    }
    printf("%lld", final_ans);
    return 0;
}

int cnt[2007][2007];
int Subtask_5to10()
{
    ans[1] = 0;
    if (s[1] == '(') cnt[1][1] = 1;
    else cnt[1][1] = 500007;
    for (int i = 2, u; i <= n; i++) {
        // printf("\r%d...", i);
        ans[i] = ans[fa[i]];
        u = i;
        while (u) {
            if (s[i] == '(') cnt[i][u] = cnt[fa[i]][u] + 1;
            else cnt[i][u] = cnt[fa[i]][u] - 1;
            if (cnt[i][u] < 0) cnt[i][u] = 500007;
            if (!cnt[i][u]) ans[i]++;
            // printf("cnt[%d][%d] = %d.\n", i, u, cnt[i][u]);
            u = fa[u];
        }
        final_ans ^= i * ans[i];
    }
    printf("%lld", final_ans);
    return 0;
}

long long _tong[1000007];
int difPos = 0;
#define tong(x) _tong[x + difPos + 500001]
int Subtask_11to14_listShape()
{
    ans[1] = 0;
    int Rcnt = 0;
    if (s[1] == '(') tong(1)++;
    else difPos++, Rcnt++;
    // for (int i = 0; i <= n; i++) printf("%d ", tong(i));
    // printf("\n");
    for (int i = 2, u; i <= n; i++) {
        // printf("\r%d...", i);
        if (s[i] == '(') {
            difPos--;
            // tong(i - 2 * Rcnt)++;
            tong(1)++;
        } else tong(0) = 0, difPos++, Rcnt++;
        // tong(0) = 0;
        // printf("difPos = %d; ", difPos);
        ans[i] = ans[i - 1] + tong(0);
        // printf("ans[%d] = %d.\n", i, ans[i]);
        final_ans ^= i * ans[i];
        // for (int i = 0; i <= n; i++) printf("%d ", tong(i));
        // printf("\n");
    }
    printf("%lld", final_ans);
    return 0;
}

vector<int> G[500007];
int Rcnt = 0;
void dfs(int u)
{
    // printf("difPos = %d.\n", difPos);
    long long t = 0;
    if (s[u] == '(') {
        difPos--;
        tong(1)++;
    } else swap(tong(0), t), difPos++, Rcnt++;
    ans[u] = ans[fa[u]] + tong(0);
    final_ans ^= u * ans[u];
    for (unsigned i = 0; i < G[u].size(); i++) dfs(G[u][i]);
    if (s[u] == '(') {
        tong(1)--;
        difPos++;
    } else difPos--, Rcnt--, swap(tong(0), t);
}

int Subtask_15to20()
{
    for (int i = 1; i <= n; i++) {
        G[fa[i]].push_back(i);
    }
    dfs(1);
    printf("%lld", final_ans);
    return 0;
}

int main()
{
    openFile("brackets");
    bool is_11to14 = 1;
    n = read<int>();
    scanf("%s", s + 1);
    fa[1] = 0;
    for (int i = 2; i <= n; i++) {
        fa[i] = read<int>();
        if (fa[i] != i - 1) is_11to14 = 0;
    } 
    // return Subtask_1to4();
    // return Subtask_5to10();
    // return Subtask_11to14_listShape();
    return Subtask_15to20();
    // if (n <= 200) return Subtask_1to4();
    // if (n <= 2000) return Subtask_5to10();
    // if (is_11to14) return Subtask_11to14_listShape();
    // Subtask_15to20();
    // return 0;
}

/*
10
(()()))(()
1 2 3 4 5 6 7 8 9
58
*/