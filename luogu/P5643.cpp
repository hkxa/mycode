/*************************************
 * problem:      contest 17734 C - P5463 小鱼比可爱（加强版）.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-07-16.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  
#define int __int128
int n;
int a[1000007], pos[1000007], rank[1000007];

struct common_BIT {
    int C[1000007];
    common_BIT()
    {
        memset(C, 0, sizeof(C));
    }
#   define lowbit(x) ((x) & (-(x)))
    void add(int x, int delta)
    {
        while (x <= n)
        {
            C[x] += delta;
            x += lowbit(x);
        }
    }
    int sum(int x)
    {
        int res(0);
        while (x)
        {
            res += C[x];
            x -= lowbit(x);
        }
        return res;
    }
#   undef lowbit
} BIT;

void pts_60()
{
    long long ans(0);
    for (int i = 1; i <= n; i++) {
        for (int j = i + 1; j <= n; j++) {
            if (a[i] > a[j]) ans += i * (n - j + 1);
        }
    }
    write(ans, 10);
}

bool cmp(const int &x, const int &y)
{
    return a[x] > a[y];
}

void pts_100()
{
    for (int i = 1; i <= n; i++) {
        pos[i] = i;
    }
    sort(pos + 1, pos + n + 1, cmp);
    for (int i = 1; i <= n; i++) {
        rank[pos[i]] = i;
    }
    int nowRank = 1;
    for (int i = 1; i < n; i++) {
        if (a[pos[i]] != a[pos[i + 1]]) nowRank++;
        rank[pos[i + 1]] = nowRank;
    }
    for (int i = 1; i <= n; i++) {
        rank[i] = nowRank - rank[i] + 1;
    }
    // for (int i = 1; i <= n; i++) {
    //     printf("%d ", rank[i]);
    // }
    // printf("\n");
    int ans(0);
    for (int i = n; i >= 1; i--) {
        ans += BIT.sum(rank[i] - 1) * i;
        BIT.add(rank[i], n - i + 1);
    }
    write(ans, 10);
}

signed main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        a[i] = read<int>();
    }
    // pts_60();
    pts_100();
    return 0;
}