//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      Codeforces Round #647 (Div. 1).
 * @user_name:    hkxadpall.
 * @time:         2020-06-04.
 * @language:     C++.
 * @upload_place: Codeforces.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 988244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

const int N = 5e5 + 7;

int n, m, t[N];
vector<int> G[N], T[N];
int _order[N], cnt = 0;
int vis[N]; int has[N]; int siz;
#define order(i) (order[++cnt] = (i)) 

int dfs(int u) {
    vis[u] = true;
    int Mx = t[u];
    siz++;
    for (unsigned i = 0; i < G[u].size(); i++) 
        if (!vis[G[u][i]])
            Mx = max(Mx, dfs(G[u][i]));
    return Mx;
}

void dfs2(int u) {
    vis[u] = 2;
    has[t[u]]++;
    for (unsigned i = 0; i < G[u].size(); i++) 
        if (vis[G[u][i]] == 1)
            dfs2(G[u][i]);
}

bool check(int u) {
    siz = 0;
    int mx = dfs(u);
    if (mx > siz) return true;
    memarr(mx, 0, has);
    dfs2(u);
    for (int i = 1; i <= mx; i++)
        if (has[i] != 1) return true;
    return false;
}

signed main() {
    n = read<int>();
    m = read<int>();
    for (int i = 1, u, v; i <= m; i++) {
        u = read<int>();
        v = read<int>();
        G[u].push_back(v);
        G[v].push_back(u);
    }
    for (int i = 1; i <= n; i++) {
        t[i] = read<int>();
        T[t[i]].push_back(i);
    }
    // for (int i = 1; i <= n; i++) {
    //     if (!vis[i]) {
    //         if (check(i)) {
    //             puts("-1");
    //             return 0;
    //         }
    //     }
    // }
    for (int i = 1; i <= n; i++) 
        for (size_t j = 0; j < T[i].size(); j++) {
            int u = T[i][j];
            if (G[u].size() < i - 1) {
                puts("-1");
                return 0;
            }
            memarr(i, 0, vis);
            for (size_t k = 0; k < G[u].size(); k++) {
                vis[t[G[u][k]]]++;
                if (t[G[u][k]] == i) {
                    puts("-1");
                    return 0;
                }
            }
            for (int k = 1; k < i; k++) {
                if (vis[k] == 0) {
                    puts("-1");
                    // printf("on node %d\n", u);
                    return 0;
                }
            }
        }
    for (int i = 1; i <= n; i++) 
        for (size_t j = 0; j < T[i].size(); j++) 
            write(T[i][j], 32);
    putchar(10);
    return 0;
}

/*
5 4
1 2
2 3
1 3
4 5
2 1 2 2 1

5 4
1 2
2 3
1 3
4 5
2 1 3 2 1

7 15
1 2
1 3
1 6
3 6
3 4
1 5
4 5
2 6
5 6
3 5
2 7
3 7
4 7
5 7
6 7
4 1 3 1 5 2 4
*/