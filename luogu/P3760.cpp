/*************************************
 * @problem:      [TJOI2017]异或和.
 * @author:       brealid.
 * @time:         2020-11-28.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
    };
}
Fastio::Reader kin;

const int N = 1e5 + 7, V = 1e6 + 7;

struct BinaryIndexTree {
    int tr[V];
    void reset() {
        memset(tr, 0, sizeof(tr));
    }
    void inc(int u, int d = 1) {
        for (++u; u < V; u += u & -u) tr[u] += d;
    }
    int qry(int u) {
        int ret(0);
        for (++u; u; u ^= u & -u) ret += tr[u];
        return ret;
    }
    int qry(int l, int r) {
        return qry(r) - qry(l - 1);
    }
} c0, c1;

int n, a[N];

signed main() {
    kin >> n;
    for (int i = 1; i <= n; ++i) {
        kin >> a[i];
        a[i] += a[i - 1];
    }
    int ans = 0;
    for (int b = 0; (1 << b) <= a[n]; ++b) {
        int mask = (1 << b) - 1, sum = 0;
        c0.reset();
        c1.reset();
        for (int i = 0; i <= n; ++i) {
            int num = a[i] & mask;
            if (a[i] & (1 << b)) {
                sum += c0.qry(0, num) + c1.qry(num + 1, mask);
                c1.inc(num);
            } else {
                sum += c1.qry(0, num) + c0.qry(num + 1, mask);
                c0.inc(num);
            }
        }
        if (sum & 1) ans |= 1 << b;
    }
    cout << ans << endl;
    return 0;
}