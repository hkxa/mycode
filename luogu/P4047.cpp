/*************************************
 * @problem:      P4047 [JSOI2010]部落划分.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-01-10.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int s, p;
int x[1000 + 7], y[1000 + 7];

int fa[1000 + 7];
void init(int cnt) { for (int i = 1; i <= cnt; i++) fa[i] = i; }
int find(int u) { return fa[u] == u ? u : fa[u] = find(fa[u]); }

struct Edge { int u, v; double dis; bool operator < (const Edge &b) const { return dis < b.dis; } } e[5000000 + 7]; int e_cnt = 0;
#define pow2(xx) ((xx) * (xx))
Edge make_edge(int i, int j) { return (Edge){i, j, sqrt(pow2(x[i] - x[j]) + pow2(y[i] - y[j]))}; }

int main()
{
    p = read<int>();
    s = read<int>();
    init(p);
    for (int i = 1; i <= p; i++) { x[i] = read<int>(); y[i] = read<int>(); }
    for (int i = 2; i <= p; i++) for (int j = 1; j < i; j++) e[++e_cnt] = make_edge(i, j);
    sort(e + 1, e + e_cnt + 1);
    for (int i = 1, need = p - s + 1; need; i++) {
        int fu = find(e[i].u), fv = find(e[i].v);
        if (fu != fv) {
            fa[fu] = fv;
            need--;
            if (!need) {
                // printf("edge {%d, %d} => ans\n", e[i].u, e[i].v);
                printf("%.2lf", e[i].dis);
                return 0;
            }
        }
    }
    puts("0.00");
    return 0;
}

/*
  |  |  |  |  |  |  |  |
-- -- -- -- -- -- -- -- -- 
  |  |  |  |  |  |  |  |
-- --#--#-- -- -- -- -- -- 
  |  |  |  |  |  |  |  |
-- --#--#-- --#--#-- -- -- 
  |  |  |  |  |  |  |  |
-- -- -- -- -- --#-- -- -- 
  |  |  |  |  |  |  |  |
-- -- -- -- -- -- -- -- -- 
  |  |  |  |  |  |  |  |
-- --#--#-- -- -- -- -- -- 
  |  |  |  |  |  |  |  |
-- -- -- -- -- -- -- -- -- 
  |  |  |  |  |  |  |  |
*/