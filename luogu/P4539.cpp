/*************************************
 * @problem:      P4539 [SCOI2006]zh_tree.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2019-12-20.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define Max_N (30 + 3)

int n;
double k, c;
int a[Max_N], pre_sum[Max_N];
int f[Max_N][Max_N];
#define sum(l, r) (pre_sum[r] - pre_sum[(l) - 1])

int main()
{
    memset(f, 0x3f, sizeof(f));
    n = read<int>();
    scanf("%lf%lf", &k, &c);
    for (int i = 1; i <= n; i++) {
        a[i] = read<int>();
        pre_sum[i] = pre_sum[i - 1] + a[i];
        f[i][i] = a[i];
        f[i + 1][i] = 0;
    }
    f[1][0] = 0;
    for (int l = 1; l < n; l++) {
        for (int i = 1, j = 1 + l; j <= n; i++, j++) {
            for (int k = i; k <= j; k++) {
                f[i][j] = min(f[i][j], f[i][k - 1] + f[k + 1][j] + sum(i, j));
            }
            // printf("f[%d][%d] = %d.\n", i, j, f[i][j]);
        }
    }
    printf("%.3lf", f[1][n] * k / sum(1, n) + c);
    return 0;
}