/*************************************
 * @problem:      [USACO15OPEN]被困在haybales
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-02-03.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
       return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
       return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

long long n, b, sp;
struct haybale {
    long long h, pos;
    bool operator < (const haybale &b) const { return pos < b.pos; }
} a[100007];

long long ans = 1e18;

int main()
{
    n = read<int>();
    b = read<int>();
    for (int i = 1; i <= n; i++) {
        a[i].h = read<int>();
        a[i].pos = read<int>();
    }
    long long l, r;
    sort(a + 1, a + n + 1);
    for (int i = 1; i <= n; i++) {
        if (a[i].pos > b) {
            sp = i;
            break;
        }
    }
    l = sp - 1;
    r = sp;
    while (l >= 1 && r <= n) {
        if (a[r].pos - a[l].pos <= a[r].h && a[r].pos - a[l].pos <= a[l].h) {
            puts("0");
            return 0;
        }
        if (a[r].pos - a[l].pos > a[r].h) {
            r++;
            continue;
        }
        if (a[r].pos - a[l].pos > a[l].h) {
            ans = min(a[r].pos - a[l].pos - a[l].h, ans);
            l--;
        }

    }
    l = sp - 1;
    r = sp;
    while (l >= 1 && r <= n) {
        if (a[r].pos - a[l].pos <= a[r].h && a[r].pos - a[l].pos <= a[l].h) {
            puts("0");
            return 0;
        }
        if (a[r].pos - a[l].pos > a[l].h) {
            l--;
            continue;
        }
        if (a[r].pos - a[l].pos > a[r].h) {
            ans = min(a[r].pos - a[l].pos - a[r].h, ans);
            r++;
        }

    }
    if (ans == 1e18) puts("-1");
    else write(ans, 10);
    return 0;
}