#include <bits/stdc++.h>
using namespace std;

int main() {
    vector<int> nums;
    int q;
    scanf("%d", &q);
    while (q--) {
        int t, x;
        scanf("%d %d", &t, &x);
        if (t == 1) {
            nums.insert(upper_bound(nums.begin(), nums.end(), x), x);
        } else if (t == 2) {
            nums.erase(lower_bound(nums.begin(), nums.end(), x));
        } else if (t == 3) {
            printf("%d\n", lower_bound(nums.begin(), nums.end(), x) - nums.begin() + 1);
        } else if (t == 4) {
            printf("%d\n", nums[x - 1]);
        } else if (t == 5) {
            printf("%d\n", *--lower_bound(nums.begin(), nums.end(), x));
        } else {
            printf("%d\n", *upper_bound(nums.begin(), nums.end(), x));
        }
    }
    return 0;
}