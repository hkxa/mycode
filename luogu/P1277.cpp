/*************************************
 * problem:      P1277 拼字游戏.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-08.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
// #include <windows.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int row[4], col[4], dia[2];
int rre[4], cre[4], dre[2];
int mat[4][4] = {0};
int G[17][4][4] = {0};

#define FOR_IN_MATRIX(i, j)      \
for (int i = 0; i < 4; i++)      \
    for (int j = 0; j < 4; j++)  

#define now G[GraphId]
#define las G[GraphId - 1]
bool logic(int GraphId)
{
    int count, tot;
    bool updated = true;
    while (updated) {
        // printf("G#%d : (logic)\n", GraphId);
        // FOR_IN_MATRIX(i, j) {
        //     write(now[i][j], j == 3 ? '\n' : ' ');
        // }
        updated = false;
        for (int i = 0; i < 4; i++) {
            count = 0;
            tot = 0;
            for (int j = 0; j < 4; j++) {
                if (now[i][j]) count++, tot += now[i][j];
            }
            if (count != 4 && row[i] == tot) return true;
            if (count == 3) {
                for (int j = 0; j < 4; j++) {
                    if (!now[i][j]) now[i][j] = row[i] - tot;
                }
                updated = true;
            } else if (count == 4 && row[i] != tot) return true;
        }
        for (int j = 0; j < 4; j++) {
            count = 0;
            tot = 0;
            for (int i = 0; i < 4; i++) {
                if (now[i][j]) count++, tot += now[i][j];
            }
            if (count != 4 && col[j] == tot) return true;
            if (count == 3) {
                for (int i = 0; i < 4; i++) {
                    if (!now[i][j]) now[i][j] = col[j] - tot;
                }
                updated = true;
            } else if (count == 4 && col[j] != tot) return true;
        }
        count = 0;
        tot = 0;
        for (int i = 0; i < 4; i++) {
            if (now[i][i]) count++, tot += now[i][i];
        }
        if (count != 4 && dia[0] == tot) return true;
        if (count == 3) {
            for (int i = 0; i < 4; i++) {
                if (!now[i][i]) now[i][i] = dia[0] - tot;
            }
            updated = true;
        } else if (count == 4 && dia[0] != tot) return true;
        count = 0;
        tot = 0;
        for (int i = 0; i < 4; i++) {
            if (now[i][3 - i]) count++, tot += now[i][3 - i];
        }
        if (count != 4 && dia[1] == tot) return true;
        if (count == 3) {
            for (int i = 0; i < 4; i++) {
                if (!now[i][3 - i]) now[i][3 - i] = dia[1] - tot;
            }
            updated = true;
        } else if (count == 4 && dia[1] != tot) return true;
        if ((bool)now[0][0] + (bool)now[1][0] + (bool)now[2][0] + (bool)now[0][3] + (bool)now[1][2] + (bool)now[2][1] == 5) {
            int a1 = row[0] - (now[0][0] + now[1][0] + now[2][0]);
            int a2 = dia[1] - (now[0][3] + now[1][2] + now[2][1]);
            // if (a1 == a2) return true;
            if (!now[0][0]) now[0][0] = a1 - a2;
            if (!now[1][0]) now[1][0] = a1 - a2;
            if (!now[2][0]) now[2][0] = a1 - a2;
            if (!now[0][3]) now[0][3] = a2 - a1;
            if (!now[1][2]) now[1][2] = a2 - a1;
            if (!now[2][1]) now[2][1] = a2 - a1;
            updated = true;
        }
    }
    return false;
}

bool getRest(int GraphId)
{
    for (int i = 0; i < 4; i++) {
        rre[i] = row[i] + 1;
        for (int j = 0; j < 4; j++) {
            rre[i] -= max(now[i][j], 1);
        }
        if (rre[i] < 1) return true;
    }
    for (int j = 0; j < 4; j++) {
        cre[j] = col[j] + 1;
        for (int i = 0; i < 4; i++) {
            cre[j] -= max(now[i][j], 1);
        }
        if (cre[j] < 1) return true;
    }
    dre[0] = dia[0] + 1 - (max(now[0][0], 1) + max(now[1][1], 1) + max(now[2][2], 1) + max(now[3][3], 1));
    dre[1] = dia[1] + 1 - (max(now[0][3], 1) + max(now[2][1], 1) + max(now[1][2], 1) + max(now[3][0], 1));
    if (dre[0] < 1 || dre[1] < 1) return true;
    return false;
}

bool check(int GraphId)
{
    FOR_IN_MATRIX(i, j) {
        if (now[i][j] < 1) return false;
    }
    return true;
}

bool push(int GraphId)
{
    FOR_IN_MATRIX(i, j) {
        now[i][j] = las[i][j];
    }
    // printf("G#%d : \n", GraphId);
    // FOR_IN_MATRIX(i, j) {
    //     write(now[i][j], j == 3 ? '\n' : ' ');
    // }
    if (getRest(GraphId)) {
        // printf("return because of function 'getRest'\n");
        return false;
    }
    // getRest(GraphId);
    FOR_IN_MATRIX(i, j) {
        if (!now[i][j]) {
            int mx = min(rre[i], cre[j]);
            if (i == j) mx = min(mx, dre[0]);
            if (i + j == 3) mx = min(mx, dre[1]);
            for (int num = 1; num <= mx; num++) {
                // printf("G#%d : testing (%d, %d) with number %d.\n", GraphId, i, j, num);
                now[i][j] = num;
                if (logic(GraphId)) {
                    FOR_IN_MATRIX(_i, _j) {
                        now[_i][_j] = las[_i][_j];
                    }
                    // printf("continue because-of function 'logic'\n");
                    continue;
                }
                // printf("G#%d : \n", GraphId + 1);
                // FOR_IN_MATRIX(_i, _j) {
                //     write(G[GraphId][_i][_j], _j == 3 ? '\n' : ' ');
                // }
                // logic(GraphId);
                if (push(GraphId + 1)) return true;
                FOR_IN_MATRIX(_i, _j) {
                    now[_i][_j] = las[_i][_j];
                }
            }
            // now[i][j] = 0;
            // FOR_IN_MATRIX(_i, _j) {
            //     now[_i][_j] = las[_i][_j];
            // }
            return false;
        }
    }
    if (!check(GraphId)) return false;
    FOR_IN_MATRIX(i, j) {
        write(now[i][j], j == 3 ? '\n' : ' ');
    }
    return true;
    // return false;
}

int main()
{
    int x, y;
    for (int i = 0; i < 4; i++) row[i] = read<int>();
    for (int i = 0; i < 4; i++) col[i] = read<int>();
    for (int i = 0; i < 2; i++) dia[i] = read<int>();
    for (int i = 0; i < 4; i++) {
        x = read<int>();
        y = read<int>();
        G[0][x][y] = mat[x][y] = read<int>();
    }
    logic(0);
    push(1);
    return 0;
}

/*

15 14 14 11 15 18 9 12 14 15 3 3 2 3 1 4 2 3 4 1 3 2 

* * * *
* * * 2
* * * 4
* 4 * 2

5 5 1 4 
3 5 4 2 
4 4 2 4 
3 4 2 2 

 * * * .
 * * * .
 * . * .
 . . . .

 */