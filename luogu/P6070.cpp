/*************************************
 * @problem:      P6070 [MdOI2020] Decrease.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-02-10.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, m, k;
int a[5003][5003], S[5003][5003], t;
int64 ans = 0;

int main()
{
    n = read<int>();
    m = read<int>();
    k = read<int>();
    for (int i = 1, x, y, z; i <= m; i++) {
        x = read<int>();
        y = read<int>();
        z = read<int>();
        a[x][y] = z;
    }
    for (int i = 1; i <= n; i++) 
        for (int j = 1; j <= n; j++) 
            S[i][j] = a[i][j] - a[i][j - 1];
    for (int i = 1; i <= n - k + 1; i++) {
        for (int j = 1; j <= n - k + 1; j++) {
            if (S[i][j]) {
                ans += abs(t = S[i][j]);
                for (int l = i; l <= i + k - 1; l++) {
                    S[l][j] -= t;
                    S[l][j + k] += t;
                }
            }
        }
    }
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= n; j++) {
            if (S[i][j]) {
                puts("-1");
                return 0;
            }
        }
    }
    // There : i, j can't from n - k + 2, as
    // +++++---
    // +++++---
    // +++++---
    // +++++---
    // +++++---
    // -----///
    // -----///
    // -----///
    // area['/'] & area['-'] are both need except area['+']
    write(ans, 10);
    return 0;
}