/*************************************
 * @contest:      Codeforces Round #643 (Div. 2).
 * @user_name:    hkxadpall.
 * @time:         2020-05-16.
 * @language:     C++.
 * @upload_place: Codeforces.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int T;
int64 a, K;
int minn, maxi;

bool cal(int64 a)
{
    minn = 9;
    maxi = 0;
    while (a) {
        minn = min(minn, (int)(a % 10));
        maxi = max(maxi, (int)(a % 10));
        a /= 10;
    }
    return minn;
}

int main()
{
    T = read<int>();
    while (T--) {
        a = read<int64>();
        K = read<int64>();
        while (cal(a) && --K) a += minn * maxi;
        write(a, 10);
    }
    return 0;
}