/*************************************
 * problem:      P1560.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-07-18.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

class roadBlock_Dealer {
  private:
    string getInBuf;
  public:
    struct position {
        int x, y;
        position() {}
        position(int _X, int _Y) : x(_X), y(_Y) {}
    };
    position readIn()
    {
        cin >> getInBuf;
        int x, y;
        x = getInBuf[0] - 'A' + 1;
        sscanf(getInBuf.c_str() + 1, "%d", &y);
        return position(x, y);
    }
    roadBlock_Dealer& operator >> (position &tPos) 
    {
        tPos = readIn();
        return *this;
    }
} rbder;

int n, m;
bool isBlock[127][127] = {0};
int ans = 0;
bool isReached[127][127] = {0};
int tx[4] = {0, 0, -1, 1}, ty[4] = {-1, 1, 0, 0};

void show()
{
#ifdef showMoreDebug
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= n; j++) {
            if (isBlock[j][i]) putchar('#');
            else if (isReached[j][i]) putchar('@');
            else putchar('.');
        }
        putchar('\n');
    }
    for (int wait = 0; wait < 7e7; wait++);
#endif
}

void dfs(int x, int y, int dist)
{
    ans = max(ans, dist);
#ifdef DEBUG
    printf("%d                      \n", dist);
    printf("now Pos (%d, %d)   \n", x, y); 
#endif
    show();
    for (int direct = 0; direct < 4; direct++) {
        int nx = x + tx[direct];
        int ny = y + ty[direct];
        int cnt = 0;
        while (true) {
            show();
#ifdef DEBUG
            printf("->now Pos (%d, %d)   \r", nx, ny); 
            for (int wait = 0; wait < 7e7; wait++);
#endif
            if (isReached[nx][ny]) {
                ans = max(ans, dist + cnt);
#ifdef DEBUG
                printf("%d                      \n", dist + cnt);
#endif
                nx -= tx[direct];
                ny -= ty[direct];
                while (nx != x || ny != y) {
                    isReached[nx][ny] = false;
                    nx -= tx[direct];
                    ny -= ty[direct];
                }
                break;
            }
            if (isBlock[nx][ny] || nx < 1 || nx > n || ny < 1 || ny > n) {
                if (nx - tx[direct] == x && ny - ty[direct] == y) {
                    nx -= tx[direct];
                    ny -= ty[direct];
                    while (nx != x || ny != y) {
                        isReached[nx][ny] = false;
                        nx -= tx[direct];
                        ny -= ty[direct];
                    }
                    break;
                }
                dfs(nx - tx[direct], ny - ty[direct], dist + cnt);
                nx -= tx[direct];
                ny -= ty[direct];
                while (nx != x || ny != y) {
                    isReached[nx][ny] = false;
                    nx -= tx[direct];
                    ny -= ty[direct];
                }
                break;
            }
            isReached[nx][ny] = true;
            nx += tx[direct];
            ny += ty[direct];
            cnt++;
        }
    }
}

int main()
{
    roadBlock_Dealer::position tPos;
    n = read<int>();
    m = read<int>();
    for (int i = 1; i <= m; i++) {
#ifdef DEBUG
        printf("roadBlock %d input : ", i);
#endif
        rbder >> tPos;
#ifdef DEBUG
        printf("(%d, %d) \n", tPos.x, tPos.y);
#endif
        isBlock[tPos.x][tPos.y] = true;
    }
    isReached[1][1] = true;
    dfs(1, 1, 1);
    write(ans, 10);
    return 0;
}