/*************************************
 * problem:      P3617 电阻网络.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-05-02.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

struct NODE {
    int from[2], vf[2], cf;
    int to[2], vt[2], ct;
    NODE() : cf(0), ct(0) {}
    double R;
    void calc();
} node[100007];

int ci = 2;

void NODE::calc()
{
    switch (cf) {
        case 1:
            R = node[from[0]].R + vf[0];
            // printf("calc node %d : R = %3lf. [from %d]\n", ci++, R, from[0]);
            break;
        case 2:
            double R1 = node[from[0]].R + vf[0];
            double R2 = node[from[1]].R + vf[1];
            if (R1 == 0 || R2 == 0) {
                R = 0;
            } else {
                R = (R1 * R2) / (R1 + R2);
            }
            // printf("calc node %d : R = %3lf. [from %d & %d]\n", ci++, R, from[0], from[1]);
            break;
    }
}

int n, m;
int a, b, c;

int main()
{
    n = read<int>();
    m = read<int>();
    while (m--) {
        a = read<int>();
        b = read<int>();
        c = read<int>();
        node[a].to[node[a].ct] = b;
        node[a].vt[node[a].ct++] = c;
        node[b].from[node[b].cf] = a;
        node[b].vf[node[b].cf++] = c;
    }
    node[1].R = 0;
    for (int i = 2; i <= n; i++) {
        node[i].calc();
    }
    printf("%.3lf\n", node[n].R);
    return 0;
}