/*************************************
 * @contest:      Codeforces Round #643 (Div. 2).
 * @user_name:    hkxadpall.
 * @time:         2020-05-16.
 * @language:     C++.
 * @upload_place: Codeforces.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int64 A, B, C, D;
int64 ans;

int main()
{
    A = read<int>();
    B = read<int>();
    C = read<int>();
    D = read<int>();
    for (int x = A; x <= B; x++) {
        int mnY = max(B, C - x + 1);
        // when y reach D - x + 1, z reach the limit
        int sepY = min(C, D - x + 1);
        if (sepY >= mnY) {
            ans += (int64)(x + mnY - C + x + sepY - C) * (sepY - mnY + 1) / 2;
            // write(ans, 10);
            // for the rest
            ans += (int64)(C - sepY) * (D - C + 1);
            // write(ans, 10);
        } else {
            ans += (int64)(C - B + 1) * (D - C + 1);
        }
    }
    write(ans, 10);
    return 0;
}