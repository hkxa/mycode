/*************************************
 * problem:      P2413 yyy loves physics IV.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-21.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define puts_return(x) { puts(x); return 0; }
#define write_return(x) { write(x); return 0; }

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;
double p[26];
char s[1 << 27];

double query(int l, int r)
{
    if (l == r) {
        // printf("query(%d, %d) : %.4lf.\n", l, r, p[s[l] - 'A']);
        return p[s[l] - 'A'];
    }
    int lcnt = 0, dote = 0;
    for (int i = l; i <= r; i++) {
        switch (s[i]) {
            case '(' : lcnt++; break;
            case ')' : lcnt--; break;
            case ',' : dote += !lcnt; break;
            default : break;
        }
    }
    double ans = 1;
    if (dote) {
        int lasDote = l;
        for (int i = l; i <= r; i++) {
            switch (s[i]) {
                case '(' : lcnt++; break;
                case ')' : lcnt--; break;
                case ',' : 
                    if (!lcnt) {
                        ans *= 1 - query(lasDote, i - 1);
                        lasDote = i + 1;
                    }
                    break;
                default : break;
            }
        }
        ans *= 1 - query(lasDote, r);
        // printf("query(%d, %d) : %.4lf.\n", l, r, 1 - ans);
        return 1 - ans;
    } else {
        int lasl = l + 1;
        for (int i = l; i <= r; i++) {
            switch (s[i]) {
                case '(' : 
                    if (!lcnt)
                        lasl = i + 1; 
                    lcnt++; 
                    break;
                case ')' : 
                    lcnt--;
                    if (!lcnt) {
                        ans *= query(lasl, i - 1);
                        lasl = i + 2;
                    }
                    break;
                default : break;
            }
        }
        // printf("query(%d, %d) : %.4lf.\n", l, r, ans);
        return ans;
    }
}

int main()
{
    n = read<int>();
    scanf("%s", s);
    for (int j = 0; j < n; j++) scanf("%lf", p + j);
    printf("%.4lf", query(0, strlen(s) - 1));
    return 0;
}

/*
10
(A,B,(A,B,(A,B,(A,B,(A,B,(A,B,(A,B,(A,B,C,D,E,F)(G,H,I,J))))))))(H)(I)(J)(C,D,E)
0.0618 0.0382 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 
*/