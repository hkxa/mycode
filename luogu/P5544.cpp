/*************************************
 * @problem:      [JSOI2016]炸弹攻击1.
 * @author:       brealid.
 * @time:         2020-11-21.
*************************************/
#define MATH_E 2.7182818284590452354
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;
#if false /* 需要使用 fread 优化，改此参数为 true */
#define getchar() (i_P1 == i_P2 && (i_P2 = (i_P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), i_P1 == i_P2) ? EOF : *i_P1++)
char BUF[1 << 21], *i_P1 = BUF, *i_P2 = BUF;
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 10 + 2, M = 1000 + 3;

int n, m, R;
int x[N], y[N], r[N];
int p[M], q[M];

double dist(double x1, double y1, double x2, double y2) {
    return sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
}

int count(double X, double Y, double d = R) {
    for (int i = 1; i <= n; ++i)
        d = min(d, dist(X, Y, x[i], y[i]) - r[i]);
    if (d < 0) return 0;
    int cnt = 0;
    for (int i = 1; i <= m; ++i)
        if (dist(X, Y, p[i], q[i]) <= d) ++cnt;
    return cnt;
}

double rand_double() {
    return (double)((uint32)rand() ^ ((uint32)rand() << 13) ^ ((uint32)rand() << 26)) / 4294967295u;
}

double ans_x, ans_y;
int SA(double x, double y) {
    int ans = count(x, y);
    for (double t = 2600; t >= 1e-8; t *= 0.998) {
        double now_x = x + (rand_double() - 0.5) * t * RAND_MAX;
        double now_y = y + (rand_double() - 0.5) * t * RAND_MAX;
        int now = count(now_x, now_y);
        if (now > ans) {
            ans_x = x = now_x;
            ans_y = y = now_y;
            ans = now;
        } else if (exp(-(now - ans) / t) < rand_double()) {
            x = now_x;
            y = now_y;
        }
    }
    return ans;
}

signed main() {
    srand(time(0));
    kin >> n >> m >> R;
    for (int i = 1; i <= n; ++i) kin >> x[i] >> y[i] >> r[i];
    double average_x = 0, average_y = 0;
    for (int i = 1; i <= m; ++i) {
        kin >> p[i] >> q[i];
        average_x += p[i];
        average_y += q[i];
    }
    average_x /= m;
    average_y /= m;
    ans_x = average_x;
    ans_y = average_y;
    int ans = 0;
    for (int i = 1; i <= 10; ++i) ans = max(ans, SA(ans_x, ans_y));
    // while (clock() < 0.8 * CLOCKS_PER_SEC) ans = max(ans, SA(ans_x, ans_y));
    kout << ans << '\n';
    return 0;
}