/*************************************
 * @problem:      CF1917E.
 * @author:       CharmingLakesideJewel.
 * @time:         2023-12-24.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

// #define USE_FREAD  // 使用 fread  读入，去注释符号
// #define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 1000 + 5;

int T, n, k;
bool mat[N][N];
int draw_first2block = 0;

void reset() {
    for (int i = 1; i <= n; ++i)
        for (int j = 1; j <= n; ++j)
            mat[i][j] = 0;
    draw_first2block = 0;
}

void draw6() {
    mat[1][1] = mat[1][2] = mat[2][1] = mat[2][3] = mat[3][2] = mat[3][3] = 1;
    draw_first2block = 1;
}

void add_block(int cnt) {
    int i = 1, j = 0;
    while (cnt) {
        if (++j > n / 2) {
            ++i;
            j = 1;
        }
        if (i <= 2 && j <= 2 && draw_first2block) continue;
        --cnt;
        mat[i * 2][j * 2] = mat[i * 2 - 1][j * 2] = mat[i * 2][j * 2 - 1] = mat[i * 2 - 1][j * 2 - 1] = 1;
    }
}

void reverse_mat() {
    for (int i = 1; i <= n; ++i)
        for (int j = 1; j <= n; ++j)
            mat[i][j] ^= 1;
}

signed main() {
    kin >> T;
    while (T--) {
        kin >> n >> k;
        bool success = false;
        if (k % 4 == 0) {
            success = true;
            reset();
            add_block(k / 4);
        } else {
            bool reversed = false;
            if (k * 2 > n * n) {
                reversed = true;
                k = n * n - k;
            }
            if (k % 4 == 2 && k != 2) {
                success = true;
                reset();
                draw6();
                add_block((k - 6) / 4);
                if (reversed) reverse_mat();
            }
        }
        if (success) {
            kout << "Yes\n";
            for (int i = 1; i <= n; ++i) {
                for (int j = 1; j <= n; ++j)
                    kout << (mat[i][j] ? '1' : '0') << " \n"[j == n];
            }
        } else kout << "No\n";
    }
    return 0;
}