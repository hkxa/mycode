/*************************************
 * @problem:      「伪模板」主席树.
 * @author:       brealid.
 * @time:         2021-02-02.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

namespace Network_MaxFlow {
    typedef long long int64;
    const int Net_Node = 500/*最大节点数*/, Net_Edge = 5000/*最大边数(无需开两倍)*/;
    const int64 inf = 0x3f3f3f3f3f3f3f3f;
    struct edge {
        int to, nxt_edge;
        int64 flow;
    } e[Net_Edge * 2 + 5];
    int depth[Net_Node + 5], head[Net_Node + 5], cur[Net_Node + 5], ecnt = 1;
    int node_total, st, ed;
    // 清零，此函数适用于多组数据
    void clear() {
        memset(head, 0, sizeof(head));
        ecnt = 1;
        st = ed = 0;
    }
    // 添加边（正向边和反向边均会自动添加）
    inline void add_edge(const int &from, const int &to, const int64 &flow) {
        // Add "positive going edge"
        e[++ecnt].to = to;
        e[ecnt].flow = flow;
        e[ecnt].nxt_edge = head[from];
        head[from] = ecnt;
        // Add "reversed going edge"
        e[++ecnt].to = from;
        e[ecnt].flow = 0;
        e[ecnt].nxt_edge = head[to];
        head[to] = ecnt;
    }
    // Dinic 算法 bfs 函数
    inline bool dinic_bfs() {
        memset(depth, 0x3f, sizeof(int) * (node_total + 1));
        memcpy(cur, head, sizeof(int) * (node_total + 1));
        std::queue<int> q;
        q.push(st);
        depth[st] = 0;
        while (!q.empty()) {
            int u = q.front(); q.pop();
            for (int i = head[u]; i; i = e[i].nxt_edge)
                if (depth[e[i].to] > depth[u] + 1 && e[i].flow) {
                    depth[e[i].to] = depth[u] + 1;
                    q.push(e[i].to);
                }
        }
        return depth[ed] != 0x3f3f3f3f;
    }
    // Dinic 算法 dfs 函数
    int64 dinic_dfs(int u, int64 now) {
        if (u == ed) return now;
        int64 max_flow = 0, nRet;
        for (int &i = cur[u]; i && now; i = e[i].nxt_edge)
            if (depth[e[i].to] == depth[u] + 1 && (nRet = dinic_dfs(e[i].to, std::min(now, e[i].flow)))) {
                now -= nRet;
                max_flow += nRet;
                e[i].flow -= nRet;
                e[i ^ 1].flow += nRet;
            }
        return max_flow;
    }
    // Dinic 算法总工作函数，需要提供节点数，起始点（默认 1），结束点（默认 node_count)
    int64 dinic_work(int node_count, int start_node = 1, int finish_node = -1) {
        node_total = node_count;
        st = start_node;
        ed = ~finish_node ? finish_node : node_count;
        int64 max_flow = 0;
        while (dinic_bfs())
            max_flow += dinic_dfs(st, inf);
        return max_flow;
    }
}

enum type { J, HK, W, YYY, E };
type read_type() {
    static char s[10];
    kin >> s;
    if (!strcmp(s, "J")) return J;
    else if (!strcmp(s, "HK")) return HK;
    else if (!strcmp(s, "W")) return W;
    else if (!strcmp(s, "YYY")) return YYY;
    else if (!strcmp(s, "E")) return E;
    else return (type)-1;
}

const int N = 100 + 3;

int n, m;
type pa[N], pb[N];
bool win[5][5];
int la[N], lb[N];

signed main() {
    win[J][HK] = win[J][W] = true;
    win[HK][W] = win[HK][E] = true;
    win[W][YYY] = win[W][E] = true;
    win[YYY][J] = win[YYY][HK] = true;
    win[E][J] = win[E][YYY] = true;
    kin >> n >> m;
    int S = 2 * n + 1, snj = 2 * n + 2, byx = 2 * n + 3;
    Network_MaxFlow::add_edge(S, snj, m);
    int a_YYY = 0, b_YYY = 0;
    for (int i = 1; i <= n; ++i) 
        if ((pa[i] = read_type()) == YYY) ++a_YYY;
    for (int i = 1; i <= n; ++i) 
        if ((pb[i] = read_type()) == YYY) ++b_YYY;
    for (int i = 1; i <= n; ++i) {
        kin >> la[i];
        if (pa[i] == J) la[i] += a_YYY;
        Network_MaxFlow::add_edge(snj, i, la[i]);
        // printf("E-Part I: %d -> %d (%d)\n", snj, i, la[i]);
    }
    for (int i = 1; i <= n; ++i) {
        kin >> lb[i];
        if (pb[i] == J) lb[i] += b_YYY;
        Network_MaxFlow::add_edge(i + n, byx, lb[i]);
        // printf("E-Part II: %d -> %d (%d)\n", i, byx, lb[i]);
    }
    for (int i = 1; i <= n; ++i)
        for (int j = 1; j <= n; ++j)
            if (win[pa[i]][pb[j]]) Network_MaxFlow::add_edge(i, j + n, 1);
    kout << Network_MaxFlow::dinic_work(2 * n + 3, S, byx) << '\n';
    return 0;
}