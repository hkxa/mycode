/*************************************
 * problem:      P3372 【模板】线段树 1. 
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-11-07.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, m;
int op, x, y; int64 k;
int64 a[100007];

struct data {
    int size;
    int64 val, lazy;
} t[100007 << 4] = {{0, 0, 0}};  

inline void pushdown(int p)
{
    if (t[p].lazy) {
        if (t[p << 1].size) {
            t[p << 1].val += t[p].lazy * t[p << 1].size;
            t[p << 1].lazy += t[p].lazy;
        }
        if (t[p << 1 | 1].size) {
            t[p << 1 | 1].val += t[p].lazy * t[p << 1 | 1].size;
            t[p << 1 | 1].lazy += t[p].lazy;
        }
        t[p].lazy = 0;
    }
}

inline void pushup(int p)
{
    t[p].val = t[p << 1].val + t[p << 1 | 1].val;
    t[p].lazy = 0;
}

inline void build(int p, int l, int r)
{
    // printf("build point %d seq [%d, %d]\n", p, l, r);
    if (l > r) return;
    if (l == r) {
        t[p].val = a[l];
        t[p].size = 1;
        return;
    }
    int mid = (l + r) >> 1;
    build(p << 1, l, mid);
    build(p << 1 | 1, mid + 1, r);
    t[p].size = t[p << 1].size + t[p << 1 | 1].size;
    pushup(p);
}

inline void modify(int p, int l, int r, int ml, int mr, int64 k)
{
    // printf("modify point %d seq [%d, %d]\tgoal_seq [%d, %d] diff %lld\n", p, l, r, ml, mr, k);
    if (l > mr || r < ml) return;
    if (l >= ml && r <= mr) {
        t[p].val += k * t[p].size;
        t[p].lazy += k;
        return;
    }
    int mid = (l + r) >> 1;
    pushdown(p);
    modify(p << 1, l, mid, ml, mr, k);
    modify(p << 1 | 1, mid + 1, r, ml, mr, k);
    pushup(p);
}

inline int64 query(int p, int l, int r, int ml, int mr)
{
    // printf("query point %d seq [%d, %d]\tgoal_seq [%d, %d]\n", p, l, r, ml, mr);
    if (l > mr || r < ml) return 0;
    // if (l >= ml && r <= mr) printf("matched! val = %d\n", t[p].val);
    if (l >= ml && r <= mr) return t[p].val;
    int mid = (l + r) >> 1;
    pushdown(p);
    return query(p << 1, l, mid, ml, mr) + query(p << 1 | 1, mid + 1, r, ml, mr);
}

int main()
{
    n = read<int>();
    m = read<int>();
    for (int i = 1; i <= n; i++) a[i] = read<int>();
    build(1, 1, n);
    while (m--) {
        op = read<int>();
        x = read<int>();
        y = read<int>();
        if (op == 1) {
            k = read<int>();
            modify(1, 1, n, x, y, k);
        } else {
            write(query(1, 1, n, x, y), 10);
        }
    }
    return 0;
}