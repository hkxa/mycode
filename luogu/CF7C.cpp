/*************************************
 * problem:      CF7C Line.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-05-18.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

long long A, B, C, g;

long long x, y;
long long res;

long long ex_gcd(long long a, long long b, long long &x, long long &y)
{
    if (!b) {
        x = 1;
        y = 0;
        return a;
    }
    res = ex_gcd(b, a % b, y, x);
    y -= a / b * x;
    return res;
}

// ax + by = c

int main()
{
    A = read<int>();
    B = read<int>();
    C = -read<int>();
    g = ex_gcd(A, B, x, y);
    if (C % g) {
        puts("-1");
        return 0;
    }
    A /= g;
    B /= g;
    C /= g;
    write(C * x, 32);
    write(C * y, 10);
    return 0;
}