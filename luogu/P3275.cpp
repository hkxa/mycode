/*************************************
 * @problem:      P3275 [SCOI2011]糖果.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-02-09.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

struct Edge {
    int to, v;
};
#define make_edge(u, v) ((Edge){(u), (v)})
vector<Edge> G[100007];
int n, m;
int dis[100007], by[100007];
bool inq[100007];
int vis[100007];

int main()
{
    // [read data]
    n = read<int>();
    m = read<int>();
    for (int i = 1, x, a, b; i <= m; i++) {
        x = read<int>();
        a = read<int>();
        b = read<int>();
        switch (x) {
            case 1 : 
                G[a].push_back(make_edge(b, 0)); 
                G[b].push_back(make_edge(a, 0));
                break; // a = b (a <= b; b <= a)
            case 2 : 
                G[a].push_back(make_edge(b, 1));
                break; // a < b (a + 1 <= b)
            case 3 : 
                G[b].push_back(make_edge(a, 0));
                break; // a >= b (b <= a)
            case 4 : 
                G[b].push_back(make_edge(a, 1));
                break; // a > b (b + 1 < a)
            case 5 : 
                G[a].push_back(make_edge(b, 0));
                break; // a <= b
        }
    }
    for (int i = 1; i <= n; i++) G[0].push_back(make_edge(i, 0));
    // [/read data] 
    // [spfa]
    queue<int> q;
    q.push(0);
    memset(dis, 0xcf, sizeof(dis));
    dis[0] = 0;
    by[0] = n + 1;
    while (!q.empty()) {
        int fr = q.front(); 
        if (dis[fr] > n) {
            puts("-1");
            return 0;
        }
        q.pop();
        inq[fr] = 0;
        for (unsigned i = 0; i < G[fr].size(); i++) {
            if (dis[G[fr][i].to] < dis[fr] + G[fr][i].v) {
                dis[G[fr][i].to] = dis[fr] + G[fr][i].v;
                if (G[fr][i].to == by[fr]) {
                    puts("-1");
                    return 0;
                }
                by[G[fr][i].to] = fr;
                if (!inq[G[fr][i].to]) {
                    vis[G[fr][i].to]++;
                    if (vis[G[fr][i].to] > n) {
                        puts("-1");
                        return 0;
                    }
                    inq[G[fr][i].to] = true;
                    q.push(G[fr][i].to);
                }
            }
        }
    }
    // [/spfa]
    // [outpue answer]
    int64 dis_Tot = 0; int32 dis_Min = dis[1];
    for (int i = 1; i <= n; i++) {
        dis_Tot += dis[i];
        dis_Min = min(dis_Min, dis[i]);
    }
    // printf("dis_Min = %d.\n", dis_Min);
    write(dis_Tot - (dis_Min - 1) * n, 10);
    // [/outpue answer]
    return 0;
}