/*************************************
 * @problem:      P5960 【模板】差分约束算法.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-02-06.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

struct Edge {
    int to, v;
};
#define make_edge(u, v) ((Edge){(u), (v)})
vector<Edge> G[30007];
int n, h;
int dis[30007];
bool inq[30007];
// int vis[30007];

int main()
{
    // [read data]
    n = read<int>();
    h = read<int>();
    for (int i = 1, b, e, t; i <= h; i++) {
        b = read<int>();
        e = read<int>();
        t = read<int>();
        G[b - 1].push_back(make_edge(e, t)); // Sb-1 + t <= Se
    }
    for (int i = 0; i < n; i++) {
        G[i].push_back(make_edge(i + 1, 0)); // Si <= Si+1
        G[i + 1].push_back(make_edge(i, -1)); // Si+1 - 1 <= Sa
    }
    // [/read data] 
    // [spfa]
    queue<int> q;
    q.push(1);
    memset(dis, 0xcf, sizeof(dis));
    dis[0] = 0;
    while (!q.empty()) {
        int fr = q.front(); 
        q.pop();
        inq[fr] = 0;
        for (unsigned i = 0; i < G[fr].size(); i++) {
            if (dis[G[fr][i].to] < dis[fr] + G[fr][i].v) {
                dis[G[fr][i].to] = dis[fr] + G[fr][i].v;
                if (!inq[G[fr][i].to]) {
                    // vis[G[fr][i].to]++;
                    // if (vis[G[fr][i].to] > n) {
                    //     puts("NO");
                    //     return 0;
                    // }
                    inq[G[fr][i].to] = true;
                    q.push(G[fr][i].to);
                }
            }
        }
    }
    // [/spfa]
    // [outpue answer]
    write(dis[n], 10);
    // [/outpue answer]
    return 0;
}