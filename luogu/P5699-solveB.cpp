// run P5699 <P5699_addition_file\match1.in >P5699_addition_file\match1.usr
// P5699_addition_file\checker P5699_addition_file\match1.in P5699_addition_file\match1.usr EMPTY.txt
// run P5699 -f -q <P5699_addition_file\match10.in >P5699_addition_file\match10.usr & P5699_addition_file\checker P5699_addition_file\match10.in P5699_addition_file\match10.usr EMPTY.txt
#pragma GCC optimize("-O3")
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    register Int flag = 1;
    register char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    register Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    register Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    register Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;

int las[256 + 7], a[256 + 7], ans[256 + 7];
double now, vans;
double P[256 + 7][256 + 7];
int award[8 + 3];
double f[256 + 7][8 + 3];

#define nMx_StartT 20170933
#define nMx_EndT 0.001
#define nMx_LowerT 0.994
#define nMx_Kexp 3.12e5
#define nMx_ExchangeTimes 1 
#define nMx_SAtimes 18
#define nMx_accept(dif, t) (rand() > exp((dif) / (t)) * nMx_Kexp)
// 13589844.591100177

double check(int L, int R, int dep)
{
	if (L == R) {
        f[a[L]][dep] = 1;
	    if (L == 1) return (f[1][dep + 1] - f[1][dep]) * award[dep];
        else return 0;
    }
	register int mid = (L + R) >> 1;
    register double res = 0;
	res += check(L, mid, dep + 1);
	res += check(mid + 1, R, dep + 1);
	for (register int i = L; i <= R; i++)
		f[a[i]][dep] = 0;
	for (register int i = L; i <= mid; i++)
		for (int j = mid + 1; j <= R; j++) {
			f[a[i]][dep] += f[a[i]][dep + 1] * f[a[j]][dep + 1] * P[a[i]][a[j]];
			f[a[j]][dep] += f[a[i]][dep + 1] * f[a[j]][dep + 1] * P[a[j]][a[i]];
		}
	if (L == 1) res += (f[1][dep + 1] - f[1][dep]) * award[dep];
    return res;
}

#define get() (check(1, n, 1) + f[1][1] * award[0])
// #include<windows.h>
void MxSA()
{
    // if (Ha) random_shuffle(a + 2, a + n + 1);
    register double temperature = nMx_StartT;
    while (temperature >= nMx_EndT && clock() < CLOCKS_PER_SEC * 7) {
        for (register int i = 1; i <= n; i++) las[i] = a[i];
        swap(a[rand() % (n - 1) + 2], a[rand() % (n - 1) + 2]);
        swap(a[rand() % (n - 1) + 2], a[rand() % (n - 1) + 2]);
        swap(a[rand() % (n - 1) + 2], a[rand() % (n - 1) + 2]);
        swap(a[rand() % (n - 1) + 2], a[rand() % (n - 1) + 2]);
        swap(a[rand() % (n - 1) + 2], a[rand() % (n - 1) + 2]);
        // for (register int i = 0; i < nMx_ExchangeTimes; i++) swap(a[rand() % (n - 1) + 2], a[rand() % (n - 1) + 2]);
        now = get();
        // printf("now = %lf.\n", now);
        if (now > vans) {
            vans = now;
            for (int i = 1; i <= n; i++) ans[i] = a[i];
        } else if (!nMx_accept(abs(now - vans), temperature)) 
            for (register int i = 1; i <= n; i++) a[i] = las[i];
        temperature *= nMx_LowerT;
        // Sleep(100);
    }
}

int main()
{
    srand(time(0) ^ clock() ^ 20170933);
    n = read<int>();
    for (register int i = 1; i <= n; i++) a[i] = i;
    for (register int i = 1; i <= n; i++) 
        for (register int j = 1; j <= n; j++)
            scanf("%lf", P[i] + j);
    for (register int i = log2(n) + 0.1; i >= 0; i--) 
        scanf("%d", award + i);
    FILE * F = fopen("P5699_addition_file\\match9.usr", "r");
    for (register int i = 1; i <= n; i++) 
        fscanf(F, "%d", ans + i);
    fclose(F);
    for (register int i = 1; i <= n; i++) 
        a[i] = ans[i];
    vans = get();
    // printf("vans = %lf.\n", vans);
    // bool F = 0;
    while (clock() < CLOCKS_PER_SEC * 7) {
        MxSA();
    }
    // for (int i = 0; i < nMx_SAtimes; i++) SA();
    F = fopen("P5699_addition_file\\match9.usr", "w");
    for (register int i = 1; i <= n; i++) fprintf(F, "%d\n", ans[i]);
    fclose(F);
    return 0;
}