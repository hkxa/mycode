/*************************************
 * problem:      P5238 整数校验器.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-03-03.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 
/*************************************
 * record ID:    R16844607.
 * score:        10 pts 
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x, char nextch = ' ')
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10, 0);
    putchar((x % 10) | 48);
    if (nextch) putchar(nextch);
}  

long long l, r, T;
string s;
long long t;

int main()
{
	l = read<long long>();
	r = read<long long>();
	T = read<long long>();
	int nag;
	while (T--) {
		cin >> s;
		nag = 0;
		if (s[0] == '-') nag = 1;
		if (s[0] == '0' && s.length() != 1) {
			goto case_1;	
		}
		if (s[0] == '-' && s.length() == 1) {
			goto case_1;	
		}
		if (s[0] == '-' && s[1] == '0') {
			goto case_1;	
		}
		if (s.length() > 19 + nag) goto case_2;
		if (nag) {
//			if (l >= 0) goto case_2;
			if (s > "-9223372036854775808") goto case_2;
		} else {
//			if (r < 0) goto case_2;
			if (s > "9223372036854775807") goto case_2;
		}
		sscanf(s.c_str(), "%lld", &t);
		if (t < l || t > r) goto case_2;
		goto case_0;
		case_0:
			write(0, 10);
			goto case_break;
		case_1:
			write(1, 10);
			goto case_break;			
		case_2:
			write(2, 10);
			goto case_break;		
		case_break:
			;
	}
	return 0;
}

/*
#### Input #1
```
-9223372036854775808 9223372036854775807 9
-
-0
0123456789
-9223372036854775808
-92233720368547758081
92233720368547758083
9223372036854775808
9323372036854775807
-9243372036854775807
```
#### Output #1
```
1
1
1
0
2
2
2
2
2
```
*/
