/*************************************
 * problem:      P1039 侦探推理.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-27.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using std::string;
using std::cin;
#define puts_return(x) { puts(x); return 0; }
#define write_return(x) { write(x); return 0; }
 
typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;
 
template <typename Int>
inline Int read()      
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}
 
template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
} 
 
template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}
 
int n, m, p, fake[21], err, w[200], nx = 0;
string name[100], say[200];
const string day[10] = {
    "",
    "Today is Sunday.",
    "Today is Monday.",
    "Today is Tuesday.",
    "Today is Wednesday.",
    "Today is Thursday.",
    "Today is Friday.",
    "Today is Saturday."
};
 
void set(int who, int yx)
{
    if (fake[who] && fake[who] != yx) err = 1;
    else fake[who] = yx;
}
 
int main()
{
    m = read<int>();
    n = read<int>();
    p = read<int>();
    for (int i = 1; i <= m; i++)
        cin >> name[i];
    for (int i = 1; i <= p; i++) {
        string nm;
        cin >> nm;
        nm = nm.substr(0, nm.length() - 1);
        for (int j = 1; j <= m; j++)
            if (name[j] == nm) w[i] = j;
        getline(cin, say[i]);
        say[i] = say[i].substr(1, say[i].length() - 2);
        // printf("%s: %s\n", nm.c_str(), say[i].c_str());
    }
    for (int td = 1; td <= 7; td++) {
        for (int px = 1; px <= m; px++) {
            err = 0;
            memset(fake, 0, sizeof(fake));
            for (int i = 1; i <= p; i++) {
                int who = w[i];
                if (say[i] == "I am guilty.") set(who, px == who ? 1 : -1);
                if (say[i] == "I am not guilty.") set(who, px != who ? 1 : -1);
                for (int j = 1; j <= 7; j++)
                    if (say[i] == day[j]) set(who, j == td ? 1 : -1);
                for (int j = 1; j <= m; j++) {
                    if (say[i] == name[j] + " is guilty.") set(who, j == px ? 1 : -1);
                    if (say[i] == name[j] + " is not guilty.") set(who, j != px ? 1 : -1);
                }
            }
            int cnt = 0, ppp = 0;
            for (int i = 1; i <= m; i++) {
                if (fake[i] == -1) cnt++;
                if (fake[i] == 0) ppp++;
            }
            if (!err && cnt <= n && cnt + ppp >= n) {
                if (nx && nx != px) {
                    // printf("found %s with %s.\n", name[nx].c_str(), name[px].c_str());
                    // printf("day %d : ×%d, ?%d, √%d\n", td, cnt, ppp, m - cnt - ppp);
                    puts_return("Cannot Determine");
                } else nx = px;
            }
        }
    }
    if (!nx) puts_return("Impossible")
    else puts_return(name[nx].c_str());
    return 0;
}