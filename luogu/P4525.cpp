/*************************************
 * @problem:      【模板】自适应辛普森法1.
 * @author:       brealid.
 * @time:         2020-11-24.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

const double div6 = 1 / 6.;
double a, b, c, d, L, R;

inline double f(double x) {
    return (c * x + d) / (a * x + b);
}

double auto_simpson(double l, double r, double mid, double eps, double f0, double f2, double f4) {
    // printf("auto_simpson(%.6lf, %.6lf, %.6lf, %.6lf, %.6lf, %.6lf, %.6lf)\n", l, r, mid, eps, f0, f2, f4);
    double mid_l = (l + mid) * 0.5, mid_r = (mid + r) * 0.5;
    double f1 = f((l + mid) * 0.5), f3 = f((mid + r) * 0.5);
    double simpson_all = (f0 + 4 * f2 + f4) * (r - l) * div6;
    double simpson_l = (f0 + 4 * f1 + f2) * (mid - l) * div6;
    double simpson_r = (f2 + 4 * f3 + f4) * (r - mid) * div6;
    if (fabs(simpson_l + simpson_r - simpson_all) < eps) return simpson_all;
    return auto_simpson(l, mid, mid_l, eps, f0, f1, f2) + auto_simpson(mid, r, mid_r, eps, f2, f3, f4);
}

signed main() {
    cin >> a >> b >> c >> d >> L >> R;
    double mid = (L + R) * 0.5;
    cout << fixed << setprecision(6) << auto_simpson(L, R, mid, 1e-9, f(L), f(mid), f(R)) << endl;
    return 0;
}