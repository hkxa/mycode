/*************************************
 * problem:      P5431 【模板】乘法逆元2.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-09-29.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 
#pragma GCC optimize("Ofast")

#include <bits/stdc++.h>
using namespace std;

template <typename i = int>
struct io
{
    char ibuf[45 << 20] , * s;
    io()
    {
        fread( s = ibuf , 1 , 44 << 20 , stdin );
    }
    inline i read()
    {
        register i u = 0;
        while( * s < 48 ) s++;
        while( * s > 32 )
            u = u * 10 + * s++ - 48;
        return u;
    }
} ;
io<> ip;

// #define read ip.read
// template <typename Int>
// inline Int read()       
// {
//     Int flag = 1;
//     char c = getchar();
//     while ((!isdigit(c)) && c != '-') c = getchar();
//     if (c == '-') flag = -1, c = getchar();
//     Int init = c & 15;
//     while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
// 	return init * flag;
// }

template <typename Int>
inline void write(Int x)
{
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

int n, p, k;
int a[5000007] = {0};
long long ans = 0;
long long ki = 1;
int qz[5000007], hz[5000007];

inline long long ksm(long long a, long long mk)
{
    long long ret = 1;
    while (mk) {
        if (mk & 1) ret = (ret * a) % p;
        a = (a * a) % p;
        mk >>= 1;
    }
    return ret;
}

#define inv(x) ksm(x, p - 2)
#define dsum qz[n]

int main()
{
    n = ip.read();
    p = ip.read(); 
    k = ip.read() % p;
    for (int i = 1; i <= n; i++) {
        a[i] = ip.read();
    }
    qz[0] = hz[n + 1] = 1;
    for (int i = n; i >= 1; i--) {
        hz[i] = (long long)hz[i + 1] * a[i] % p;
    }
    for (int i = 1; i <= n; i++) {
        qz[i] = (long long)qz[i - 1] * a[i] % p;
        ki = k * ki % p;
        ans = (ans + (long long)ki * qz[i - 1] % p * hz[i + 1] % p) % p;
    }
    write(ans * inv(dsum) % p);
    return 0;
}