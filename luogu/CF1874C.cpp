/*************************************
 * @problem:      cf1874c.
 * @author:       brealid.
 * @time:         2023-09-30.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

// #define USE_FREAD  // 使用 fread  读入，去注释符号
// #define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 5000 + 7;

struct bigreal {
    double r;
    int th;
    bigreal(double _r = 0, int _th = 0): r(_r), th(_th) {
        if (r <= 1e-8) return;
        while (r >= 10) {
            r /= 10;
            ++th;
        }
        while (r < 1) {
            r *= 10;
            --th;
        }
    }
    operator double() const {
        double res = r;
        int _th = th;
        while (_th < 0) {
            res /= 10;
            ++_th;
        }
        while (_th > 0) {
            res *= 10;
            --_th;
        }
        return res;
    }
    bigreal operator * (const bigreal &b) const {
        // printf("%.6lfe%d * %.6lfe%d\n", r, th, b.r, b.th);
        return bigreal(r * b.r, th + b.th);
    }
    bigreal operator / (const bigreal &b) const {
        // printf("%.6lfe%d / %.6lfe%d\n", r, th, b.r, b.th);
        return bigreal(r / b.r, th - b.th);
    }
};


int T, n, m;
vector<int> G[N];
double f[N], tf[N];
bigreal fac[N];
// double C[N][N];
double psb[N][N];

void pre() {
    /*
    假设有优先级a1~an
    第 i+1 次:
    facts: a1~ai 已经被消耗
    消耗到 a_{i+x} 的概率为: C(n-(i+x)-1, i-x)/C(n-i,i)
    此时的贡献: f[a_{i+x+1}] * 概率 * 1/(n-2i)
    即 f[a_{i+x+1}] * C(n-(i+x)-1, i-x)/C(n-i,i) * 1/(n-2i)
    即              * (n-(i+x)-1)! / (i-x)! / (n-2i-1)! / (n-i)! * i! * (n-2i)! / (n-2i)
    即              * (n-(i+x)-1)! * i! / (2i-(i+x))! / (n-i)!
    记录  sum_{i,x}概率 * 1/(n-2i) 为 psb[n][i + x]
    */
    // for (int x = 1; x <= 5000; ++x) {
    //     C[x][1] = 1;
    //     for (int y = 1; y <= x; ++y)
    //         C[x][y] = C[x - 1][y - 1] + C[x - 1][y];
    // }
    fac[0] = 1;
    for (int i = 1; i <= 5000; ++i) fac[i] = fac[i - 1] * bigreal(i);
    for (int n = 1; n <= 5000; ++n) {
        // printf("%d\n", n);
        for (int p = 1; p < n; ++p) {
            for (int i = min(p, (n - 1) / 2); i >= (p + 1) / 2; --i) {
                // printf("aha %d %d %d\n", n, p, i); fflush(stdout);
                bigreal b = fac[n - p - 1] * fac[i] / fac[2 * i - p] / fac[n - i];
                // printf("%.6lfe%d\n", b.r, b.th);
                if (n == 1 && p == 1 && i == 0) return;
                double current = fac[n - p - 1] * fac[i] / fac[2 * i - p] / fac[n - i];
                psb[n][p] += current;
                if (current <= 1e-9) break;
                // printf("a???? %d %d %d %.6lf\n", n, p, i, current);
            }
        }
    }
}

void solve() {
    kin >> n >> m;
    for (int i = 1; i <= n; ++i) G[i].clear();
    for (int i = 1, u, v; i <= m; ++i) {
        kin >> u >> v;
        G[u].push_back(v);
    }
    f[n] = 1;
    for (int u = n - 1; u >= 1; --u) {
        int cnt = 0;
        for (int v: G[u]) tf[++cnt] = f[v];
        sort(tf + 1, tf + cnt + 1);
    }
}

signed main() {
    pre();
    // kin >> T;
    // while (T--) solve();
    return 0;
}