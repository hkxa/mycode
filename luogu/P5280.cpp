/*************************************
 * @problem:      P5280 [ZJOI2019]线段树.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-04-04. (7 days)
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int N = 100000 + 6, LinetreeSize = N * 8, P = 998244353;

#ifdef DEBUG
# define passing() cerr << "passing line [" << __LINE__ << "]." << endl
# define debug(...) printf(__VA_ARGS__)
# define show(x) cerr << #x << " = " << (x) << endl
#else
# define passing() do if (0) cerr << "passing line [" << __LINE__ << "]." << endl; while(0)
# define debug(...) do if (0) printf(__VA_ARGS__); while(0)
# define show(x) do if (0) cerr << #x << " = " << (x) << endl; while(0)
#endif

int n, m;
int f[LinetreeSize], g[LinetreeSize], tf[LinetreeSize], tg[LinetreeSize], sf[LinetreeSize];
int cntTree = 1;
#define UpdMul(_Dist, _Num) do { _Dist = (int64)_Dist * (_Num) % P; } while (0)
#define UpdAdd(_Dist, _Num) do { _Dist = (_Dist + (_Num)) % P; } while (0)
#define Mul(_Num1, _Num2) ((int64)_Num1 * (_Num2) % P)
#define Add(_Num1, _Num2) ((_Num1 + (_Num2)) % P)
#define Sub(_Num1, _Num2) (((_Num1) - (_Num2) + P) % P)

inline void pushUp(int u) { 
    sf[u] = Add(f[u], Add(sf[u << 1], sf[u << 1 | 1])); 
}

inline void pushTf(int u, int x) {
    UpdMul(f[u], x);
    UpdMul(tf[u], x);
    UpdMul(sf[u], x);
}

inline void pushTg(int u, int x) {
    UpdMul(g[u], x);
    UpdMul(tg[u], x);
}

inline void pushDown(int u) {
    if (tf[u] != 1) {
        pushTf(u << 1, tf[u]);
        pushTf(u << 1 | 1, tf[u]);
        tf[u] = 1;
    }
    if (tg[u] != 1) {
        pushTg(u << 1, tg[u]);
        pushTg(u << 1 | 1, tg[u]);
        tg[u] = 1;
    }
}

inline void pushOrange(int u)
{
    UpdAdd(f[u], Sub(cntTree, g[u]));
    UpdAdd(g[u], g[u]);
    pushTf(u << 1, 2);
    pushTf(u << 1 | 1, 2);
    pushTg(u << 1, 2);
    pushTg(u << 1 | 1, 2);
}

void modify(int u, int l, int r, int ml, int mr) {
    pushDown(u);
    if (l > mr || r < ml) {
        pushOrange(u);
        pushUp(u);
        return;
    }
    if (l >= ml && r <= mr) {
        UpdAdd(f[u], cntTree);
        pushTf(u << 1, 2);
        pushTf(u << 1 | 1, 2);
        pushUp(u);
        return;
    }
    UpdAdd(g[u], cntTree);
    int mid = (l + r) >> 1;
    modify(u << 1, l, mid, ml, mr);
    modify(u << 1 | 1, mid + 1, r, ml, mr);
    pushUp(u);
}

void PutTree(int u, int l, int r) {
    debug("%d : f = %d, sf = %d\n", u, f[u], sf[u]);
    if (l == r) return;
    int mid = (l + r) >> 1;
    PutTree(u << 1, l, mid);
    PutTree(u << 1 | 1, mid + 1, r);
}

int main()
{
    n = read<int>();
    m = read<int>();
    register int op, l, r;
    for (register int i = 1; i <= n * 4; i++) g[i] = tf[i] = tg[i] = 1;
    for (register int i = 1; i <= m; i++) {
        op = read<int>();
        if (op == 1) {
            l = read<int>();
            r = read<int>();
            modify(1, 1, n, l, r); 
            cntTree = (cntTree << 1) % P;
        } else {
            // PutTree(1, 1, n);
            write(sf[1], 10); 
        }
    }
    return 0;
}