/*************************************
 * problem:      P2243 电路维修.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-05-02.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

int n, m;
int dis[252525];
char buffer[507];

int diffX[4] = {1, 1, -1, -1}, diffY[4] = {1, -1, 1, -1};
struct pos {
    int x, y;

    vector<pos> exact()
    {
        vector<pos> vRet;
        pos t;
        for (int i = 0; i < 4; i++) {
            t.x = this->x + diffX[i];
            t.y = this->y + diffY[i];
            if (t.x < 1 || t.x > n + 1 || t.y < 1 || t.y > m + 1) continue;
            vRet.push_back(t);
        }
        return vRet;
    }

    operator int()
    {
        return x * 502 + y;
    }

    bool operator == (pos other) 
    {
        return x == other.x && y == other.y;
    }

    void turn_back(int ID)
    {
        x = ID / 502;
        y = ID % 502;
    }
};

struct orz_spfa {
    int to, val;
    orz_spfa(int TO = 0, int VAL = 0) : to(TO), val(VAL) {}
};

typedef vector<orz_spfa>::iterator voi;
// Vector Orz_spfa Iterator

vector<orz_spfa> G[252525];
bool instack[252525];

void BFS_SPFA_PLUS()
{
    memset(dis, 0x3f, sizeof(dis));
    deque <int> q;
    pos base_start, base_end;
    base_start.x = 1;
    base_start.y = 1;
    base_end.x = n + 1;
    base_end.y = m + 1;
    q.push_back(base_start);
    instack[base_start] = 1;
    dis[base_start] = 0;
    while (!q.empty()) {
        int m = q.front();
        q.pop_front();
        instack[m] = 0;
        for (voi it = G[m].begin(); it != G[m].end(); it++) {
            if (dis[it->to] <= dis[m] + it->val) continue;
            dis[it->to] = dis[m] + it->val;
            if (!instack[it->to]) {
                switch (it->val) {
                    case 0:
                        q.push_front(it->to);
                        break;
                    case 1:
                        q.push_back(it->to);
                        break;
                }
                instack[it->to] = 1;
            }
            // pos debug[2];
            // debug[0].turn_back(m);
            // debug[1].turn_back(it->to);
            // printf("EXACT : {%d, %d}[%d] to {%d, %d}[%d].\n", debug[0].x, debug[0].y, dis[m], debug[1].x, debug[1].y, dis[it->to]);
        }
    }
    write(dis[base_end], 10);
}

int playonce()
{
    n = read<int>();
    m = read<int>();
    for (int i = 0; i < 252525; i++) G[i].clear();
    if ((n + m) & 1) {
        puts("NO SOLUTION");
        return 0;
    }
    pos a, b, c, d;
    for (int i = 1; i <= n; i++) {
        scanf("%s", buffer + 1);
        for (int j = 1; j <= m; j++) {
            if (buffer[j] == '\\') {
                a.x = i;
                a.y = j;
                b.x = i + 1;
                b.y = j + 1;
                c.x = i + 1;
                c.y = j;
                d.x = i;
                d.y = j + 1;
            } else {
                a.x = i + 1;
                a.y = j;
                b.x = i;
                b.y = j + 1;
                c.x = i;
                c.y = j;
                d.x = i + 1;
                d.y = j + 1;
            }
            G[a].push_back(orz_spfa(b, 0));
            G[b].push_back(orz_spfa(a, 0));
            G[c].push_back(orz_spfa(d, 1));
            G[d].push_back(orz_spfa(c, 1));
        }
    }
    BFS_SPFA_PLUS();
    return 0;
}

int main()
{
    int T = read<int>();
    while (T--) playonce();
    return 0;
}
/*
SAMPLE
i 
3 7
\\//\\/
\\/\\\\
/\\/\\\
o
0
*/