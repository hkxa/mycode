/*************************************
 * @problem:      treas.
 * @author:       brealid.
 * @time:         2020-11-24.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define getchar() (i_P1 == i_P2 && (i_P2 = (i_P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), i_P1 == i_P2) ? EOF : *i_P1++)
char BUF[1 << 21], *i_P1 = BUF, *i_P2 = BUF;
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 2500 + 7;

int T, n;
int64 dis[N][N];
vector<int> G[N];
struct edge_info {
    int x, y;
    int64 d;
    void set_XYD(int X, int Y, int64 D) {
        x = X, y = Y, d = D;
    }
    bool operator < (const edge_info &rhs) const {
        return d < rhs.d;
    }
} e[N * N];
int ecnt;

struct UnionFindSet {
    int fa[N + 5];
    UnionFindSet() { memset(fa, -1, sizeof(fa)); }
    void reset() { memset(fa, -1, sizeof(fa)); }
    int find(int u) { return fa[u] < 0 ? u : fa[u] = find(fa[u]); }
    inline bool merge(int u, int v) {
        u = find(u), v = find(v);
        if (u == v) return false;
        if (fa[u] > fa[v]) swap(u, v);
        fa[u] += fa[v];
        fa[v] = u;
        return true;
    }
} ufs;


namespace tcs {
    int fa[N], dep[N], siz[N], wson[N], beg[N];
    int64 dis[N];
    namespace prework {
        void dfs1(int u, int ff) {
            fa[u] = ff;
            dep[u] = dep[ff] + 1;
            siz[u] = 1;
            wson[u] = 0;
            for (size_t i = 0; i < G[u].size(); ++i) {
                int v = G[u][i];
                if (v == ff) continue;
                dis[v] = dis[u] + ::dis[u][v];
                dfs1(v, u);
                siz[u] += siz[v];
                if (siz[v] > siz[wson[u]]) wson[u] = v;
            }
        }
        void dfs2(int u, int cbeg) {
            beg[u] = cbeg;
            if (!wson[u]) return;
            dfs2(wson[u], cbeg);
            for (size_t i = 0; i < G[u].size(); ++i) {
                int v = G[u][i];
                if (v != fa[u] && v != wson[u]) dfs2(v, v);
            }
        }
    }
    namespace query {
        int LCA(int u, int v) {
            while (beg[u] != beg[v]) {
                if (dep[beg[u]] < dep[beg[v]]) v = fa[beg[v]];
                else u = fa[beg[u]];
            }
            return dep[u] < dep[v] ? u : v;
        }
        int64 dist(int u, int v) {
            return dis[u] + dis[v] - (dis[LCA(u, v)] << 1);
        }
    }
}

signed main() {
    kin >> n;
    ecnt = 0;
    for (int i = 1; i <= n; ++i) {
        G[i].clear();
        for (int j = 1; j <= n; ++j) {
            kin >> dis[i][j];
            if (i < j) e[++ecnt].set_XYD(i, j, dis[i][j]);
        }
    }
    sort(e + 1, e + ecnt + 1);
    ufs.reset();
    for (int i = 1; i <= ecnt; ++i) {
        edge_info &now = e[i];
        if (now.d <= 0) {
            kout << "NO\n";
            return 0;
        }
        if (ufs.merge(now.x, now.y)) {
            G[now.x].push_back(now.y);
            G[now.y].push_back(now.x);
        }
    }
    tcs::prework::dfs1(1, 0);
    tcs::prework::dfs2(1, 1);
    for (int i = 1; i <= n; ++i)
        for (int j = 1; j <= n; ++j)
            if (dis[i][j] != tcs::query::dist(i, j)) {
                kout << "NO\n";
                return 0;
            }
    kout << "YES\n";
    return 0;
}