/*************************************
 * problem:      P4302 [SCOI2003]字符串折叠.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-04.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

char s[100 + 7];
string str;
int n;
int f[100 + 7][100 + 7];
string rec[100 + 7][100 + 7];

bool judge(int l, int r, int L, int R)
{
    int k = L - 1;
    while (true) {
        for (int i = l; i <= r; i++) {
            k++;
            if (s[k] != s[i]) return 0;
        }
        if (k == R) break; 
    }
    return 1;
}

string intToString(int ori)
{
    static char buf[17];
    sprintf(buf, "%d", ori);
    return string(buf);
}

#define lenDiff(n) (n < 10 ? 3 : n < 100 ? 4 : 5)

bool oneCase()
{
    if (scanf("%s", s + 1) == EOF) return 1;
    n = strlen(s + 1);
    s[0] = ' ';
    str = s;
    memset(f, 0x3f, sizeof(f));
    for (int i = 1; i <= n; i++) {
        f[i][i] = 1;
        rec[i][i] = s[i];
    }
    for (int l = 1; l < n; l++){
        for (int i = 1, j = l + 1; j <= n; i++, j++) {
            f[i][j] = l + 1;
            rec[i][j] = str.substr(i, l + 1);
            for (int k = i; k < j; k++) {
                if (judge(i, k, k + 1, j)) {
                    if (f[i][j] > f[i][k] + lenDiff((l + 1) / (k - i + 1))) {
                        f[i][j] = f[i][k] + lenDiff((l + 1) / (k - i + 1));
                        rec[i][j] = intToString((l + 1) / (k - i + 1)) + '(' + rec[i][k] + ')';
                    }
                }
                if (f[i][j] > f[i][k] + f[k + 1][j]) {
                    f[i][j] = f[i][k] + f[k + 1][j];
                    rec[i][j] = rec[i][k] + rec[k + 1][j];
                }
            }
        }
    }
    // write(f[1][n], 10);
    cout << rec[1][n] << endl;
    return 0;
}

int main()
{
    while (!oneCase());
    return 0;
}