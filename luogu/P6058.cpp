/*************************************
 * @contest:      【LGR-068】洛谷 2 月月赛 I & 加油武汉！.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-02-07.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#pragma GCC optimize("-O2")
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// #define DEBUG_ALL
// #define DEBUG_LCA
// #define DEBUG_CHK
#define int int64

#ifdef DEBUG_ALL
#define DEBUG_LCA
#define DEBUG_CHK
#endif

struct E {
    int to, val;
    E(int A = 0, int B = 0) : to(A), val(B) {}
    bool operator < (const E &other) const { return to < other.to; }
};

int n, k;
int dep[200006], f[200006][20];
int64 dis[200006];
vector<E> G[200006];
int leaf[200006], m = 0;
int64 cost[200006];

void dfs(int u, int fa)
{
    dep[u] = dep[fa] + 1;
    f[u][0] = fa;
    for (int k = 1; k <= 18 && f[u][k - 1]; k++) 
        f[u][k] = f[f[u][k - 1]][k - 1];
    if (G[u].size() == 1 && u != 1) {
        leaf[++m] = u;
        // printf("leaf[%d] = %d.\n", m, u);
        return;
    }
    for (unsigned e = 0; e < G[u].size(); e++) {
        if (G[u][e].to != fa) {
            dis[G[u][e].to] = dis[u] + G[u][e].val;
            dfs(G[u][e].to, u);
        }
    }
}

inline int LCA(int u, int v)
{
#   ifdef DEBUG_LCA
    printf("LCA(%d, %d) = ", u, v);
#   endif
    if (dep[u] < dep[v]) swap(u, v);
    int to_jump_dep = dep[u] - dep[v];
    for (int i = 0; i <= 18; i++)
        if (to_jump_dep & (1 << i)) u = f[u][i];
#   ifdef DEBUG_LCA
    if (u == v) printf("%d\n", u);
#   endif
    if (u == v) return u;
    for (int i = 18; i >= 0; i--)
        if (f[u][i] != f[v][i]) 
            u = f[u][i], v = f[v][i];
#   ifdef DEBUG_LCA
    printf("%d\n", f[u][0]);
#   endif
    return f[u][0];
}

inline bool check(int64 tim)
{
    if (dis[leaf[1]] * 2 > tim) return false;
#   ifdef DEBUG_CHK
    printf("check(%d) : \n", tim);
#   endif
    int cnt = 1;
    // int lca = leaf[1];
    int64 j = dis[leaf[1]];
    for (int i = 1; i < m && cnt <= k; i++) {
#       ifdef DEBUG_CHK
        printf("dis1 = %d, dis2 = %d.\n", dis[leaf[i + 1]], cost[i]);
#       endif
        if (j + cost[i] + dis[leaf[i + 1]] > tim) {
            if (dis[leaf[i + 1]] > tim) return false;
            j = dis[leaf[i + 1]];
            cnt++;
            // lca = leaf[i];
        } else {
            j += cost[i];
            // lca = LCA(lca, leaf[i]);
        }
#       ifdef DEBUG_CHK
        printf("i = %d, j = %d, cnt = %d.\n", i, j, cnt);
#       endif
    }
#   ifdef DEBUG_CHK
    printf("cnt = %d.\n", cnt);
#   endif
    return cnt <= k;
}

inline bool check2(int64 tim)
{
    tim /= 2;
    if (dis[leaf[1]] > tim) return false;
#   ifdef DEBUG_CHK
    printf("check(%d) : \n", tim);
#   endif
    int cnt = 1, lca = leaf[1];
    int64 j = dis[leaf[1]];
    for (int i = 2; i <= m && cnt <= k; i++) {
        j = j + dis[leaf[i]] - dis[LCA(lca, leaf[i])];
#       ifdef DEBUG_CHK
        printf("dis1 = %d, dis2 = %d.\n", dis[leaf[i]], dis[LCA(lca, leaf[i])]);
#       endif
        if (j > tim) {
            if (dis[leaf[i]] > tim) return false;
            j = dis[leaf[i]];
            cnt++;
            lca = leaf[i];
        } else lca = LCA(lca, leaf[i]);
#       ifdef DEBUG_CHK
        printf("i = %d, j = %d, cnt = %d.\n", i, j, cnt);
#       endif
    }
#   ifdef DEBUG_CHK
    printf("cnt = %d.\n", cnt);
#   endif
    return cnt <= k;
}

typedef bool Tp(int64);
Tp *checker;

signed main()
{
    n = read<int>();
    k = read<int>();
    if (n == 1) {
        puts("0");
        return 0;
    }
    int64 l = 0, r = 0, mid, ans;
    for (int i = 1, a, b, v; i < n; i++) {
        a = read<int>();
        b = read<int>();
        v = read<int>();
        G[a].push_back(E(b, v));
        G[b].push_back(E(a, v));
        r += v;
    }
    for (int i = 1; i <= n; i++) 
        sort(G[i].begin(), G[i].end());
    dfs(1, 0);
    for (int i = 1; i < m; i++) 
        cost[i] = dis[leaf[i]] + dis[leaf[i + 1]] - 2 * dis[LCA(leaf[i], leaf[i + 1])];
    r = r * 2 + 20;
    if (k == 1 || k == 2) checker = check;
    else checker = check2;
    while (l <= r) {
        mid = (l + r) >> 1;
        if (checker(mid)) {
            ans = mid;
            r = mid - 1;
        } else l = mid + 1;
    } 
    write(ans, 10);
    return 0;
}