/*************************************
 * problem:      P1122 最大子树和.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-03.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;
int beauty[16000 + 7];
int res[16000 + 7], ans = INT_MIN;
vector<int> G[16000 + 7];

void link(int u, int v) 
{
    G[u].push_back(v);
    G[v].push_back(u);
}

void solve(int u, int fa)
{
    res[u] = beauty[u];
    for (vector<int>::iterator it = G[u].begin(); it != G[u].end(); it++) {
        if (*it != fa) {
            solve(*it, u);
            res[u] += max(res[*it], 0);
        }
    }
    ans = max(ans, res[u]);
}

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        beauty[i] = read<int>();
    }
    for (int i = 1; i < n; i++) {
        link(read<int>(), read<int>());
    }
    solve(1, 0);
    write(ans, 0);
    return 0;
}