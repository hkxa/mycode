// luogu-judger-enable-o2
/*************************************
 * problem:      P4163 [SCOI2007]排列.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-03.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

char s[10 + 3];
int len, d, limit[10 + 3];
int cnt[10 + 3];

bool specialCase()
{
    for (int i = 1; i <= len; i++) {
        if (s[i] != '0') break;
        if (i == len) {
            puts("1");
            return true;
        }
    }
    if (d == 1) {
        int res(1);
        for (int i = 1; i <= len; i++) res *= i;
        for (int i = 0; i < 10; i++) {
            while (cnt[i] > 1) {
                res /= cnt[i];
                cnt[i]--;
            }
        }
        write(res, 10);
        return true;
    }
    if (d == 2) {
        int sum(0);
        for (int i = 0; i < 10; i++) {
            sum += cnt[i];
        }
        int even(0);
        for (int i = 0; i < 10; i += 2) {
            even += cnt[i];
        }
        int res(1);
        for (int i = 1; i <= len; i++) res *= i;
        for (int i = 0; i < 10; i++) {
            while (cnt[i] > 1) {
                res /= cnt[i];
                cnt[i]--;
            }
        }
        write(res * even / sum, 10);
        return true;
    }
    if (d == 5) {
        int sum(0);
        for (int i = 0; i < 10; i++) {
            sum += cnt[i];
        }
        int five_times(cnt[0] + cnt[5]);
        int res(1);
        for (int i = 1; i <= len; i++) res *= i;
        for (int i = 0; i < 10; i++) {
            while (cnt[i] > 1) {
                res /= cnt[i];
                cnt[i]--;
            }
        }
        write(res * five_times / sum, 10);
        return true;
    }
    if (d == 3 || d == 9) {
        int sum(0);
        for (int i = 0; i < 10; i++) {
            sum += cnt[i] * i;
        }
        if (sum % d != 0) {
            puts("0");
            return 0;
        }
        int res = 1;
        for (int i = 1; i <= len; i++) res *= i;
        for (int i = 0; i < 10; i++) {
            while (cnt[i] > 1) {
                res /= cnt[i];
                cnt[i]--;
            }
        }
        write(res, 10);
        return true;
    }
    return false;
}


int dfs(int p, long long n)
{
    if (p > len) return n % d == 0;
    if (n < limit[p]) return 0;
    int res(0);
    for (int i = 0; i < 10; i++) {
        if (cnt[i]) {
            cnt[i]--;
            res += dfs(p + 1, (((n << 2) + n) << 1) + i);
            cnt[i]++;
        }
    }
    return res;
}

void caseMain()
{
    scanf("%s%d", s + 1, &d);
    len = strlen(s + 1);
    memset(cnt, 0, sizeof(cnt));
    for (int i = 1; i <= len; i++) cnt[s[i] & 15]++;
    limit[len] = d / 10;
    for (int i = len - 1; i >= 1; i--) limit[i] = limit[i + 1] / 10;
    if (specialCase()) return;
    write(dfs(1, 0), 10);
}

int main()
{
    int Tcases = read<int>();
    while (Tcases--) caseMain();
    return 0;
}