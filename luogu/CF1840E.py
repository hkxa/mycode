T_cases = int(input())
for _ in range(T_cases):
    s1 = input()
    s2 = input()
    s = ['', list('@' + s1), list('@' + s2)] # to list, for better assignment
    t, q = map(int, input().split())
    release_queue = []
    release_queue_pos = 0
    differ = sum(c1 != c2 for c1, c2 in zip(s[1], s[2]))
    for i in range(q):
        if release_queue_pos < len(release_queue) and i == release_queue[release_queue_pos]:
            differ += 1
            release_queue_pos += 1
        query = list(map(int, input().split()))
        if query[0] == 1:
            pos = query[1]
            if s[1][pos] != s[2][pos]:
                differ -= 1
                release_queue.append(i + t)
        elif query[0] == 2:
            i1, p1, i2, p2 = query[1:]
            if p1 == p2:
                orig = s[1][p1] != s[2][p1]
                s[i1][p1], s[i2][p2] = s[i2][p2], s[i1][p1]
                differ += (s[1][p1] != s[2][p1]) - orig
            else:
                orig = (s[1][p1] != s[2][p1]) + (s[1][p2] != s[2][p2])
                # print((s[1][p1] != s[2][p1]), (s[1][p2] != s[2][p2]), orig)
                s[i1][p1], s[i2][p2] = s[i2][p2], s[i1][p1]
                differ += (s[1][p1] != s[2][p1]) + (s[1][p2] != s[2][p2]) - orig
        else:
            print('NO' if differ else 'YES')
        # print(''.join(s[1][1:]), ''.join(s[2][1:]), differ)

                

