/*************************************
 * @problem:      Tricky Function.
 * @user_name:    brealid.
 * @time:         2021-02-03.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 2e6 + 7;
const double alpha = 0.75;

struct position {
    int64 v[2];
} pos[N];

namespace kD_Tree {
    struct kD_Tree_Node {
        int64 v[2];
        int64 minv[2], maxv[2];
        int siz;
        int l, r;
        bool different_from(const int64 &x, const int64 &y) const {
            return x != v[0] || y != v[1];
        }
    } t[N];
    int kD_Tree_temp_node_cnt, kD_Tree_Node_cnt, root;
    inline bool cmp_v0_inc(const position &a, const position &b) {
        return a.v[0] != b.v[0] ? a.v[0] < b.v[0] : a.v[1] < b.v[1];
    }
    inline bool cmp_v1_inc(const position &a, const position &b) {
        return a.v[1] != b.v[1] ? a.v[1] < b.v[1] : a.v[0] < b.v[0];
    }
    typedef bool cmp_v_inc(const position &a, const position &b);
    cmp_v_inc *cmp_func[2] = { cmp_v0_inc, cmp_v1_inc };
    inline int64 pow2(const int64 rn) {
        return rn * rn;
    }
    inline int64 euclid_dist(const kD_Tree_Node &a, const int64 &x, const int64 &y) {
        return pow2(a.v[0] - x) + pow2(a.v[1] - y);
    }
    inline int64 nearest(const kD_Tree_Node &a, const int64 &x, const int64 &y) {
        return pow2(max(max(a.minv[0] - x, x - a.maxv[0]), 0ll)) + pow2(max(max(a.minv[1] - y, y - a.maxv[1]), 0ll));
    }
    inline void pushup(int u) {
        t[u].minv[0] = t[u].maxv[0] = t[u].v[0];
        t[u].minv[1] = t[u].maxv[1] = t[u].v[1];
        t[u].siz = 1;
        if (t[u].l) {
            t[u].minv[0] = min(t[u].minv[0], t[t[u].l].minv[0]);
            t[u].maxv[0] = max(t[u].maxv[0], t[t[u].l].maxv[0]);
            t[u].minv[1] = min(t[u].minv[1], t[t[u].l].minv[1]);
            t[u].maxv[1] = max(t[u].maxv[1], t[t[u].l].maxv[1]);
            t[u].siz += t[t[u].l].siz;
        }
        if (t[u].r) {
            t[u].minv[0] = min(t[u].minv[0], t[t[u].r].minv[0]);
            t[u].maxv[0] = max(t[u].maxv[0], t[t[u].r].maxv[0]);
            t[u].minv[1] = min(t[u].minv[1], t[t[u].r].minv[1]);
            t[u].maxv[1] = max(t[u].maxv[1], t[t[u].r].maxv[1]);
            t[u].siz += t[t[u].r].siz;
        }
    }
    void build(int &u, int l, int r, int D) {
        if (l > r) return;
        u = ++kD_Tree_Node_cnt;
        int mid = (l + r) >> 1;
        nth_element(pos + l, pos + mid, pos + r + 1, cmp_func[D]);
        t[u].v[0] = pos[mid].v[0];
        t[u].v[1] = pos[mid].v[1];
        build(t[u].l, l, mid - 1, !D);
        build(t[u].r, mid + 1, r, !D);
        pushup(u);
    }
    int64 ans;
    void find_nearest(const int &u, const int64 &x, const int64 &y) {
        if (t[u].different_from(x, y)) ans = min(ans, euclid_dist(t[u], x, y));
        register int64 dl = nearest(t[t[u].l], x, y), dr = nearest(t[t[u].r], x, y);
        if (dl < dr) {
            if (t[u].l && dl < ans) find_nearest(t[u].l, x, y);
            if (t[u].r && dr < ans) find_nearest(t[u].r, x, y);
        } else {
            if (t[u].r && dr < ans) find_nearest(t[u].r, x, y);
            if (t[u].l && dl < ans) find_nearest(t[u].l, x, y);
        }
    }
    inline int64 query_nearest(const int64 &x, const int64 &y) {
        ans = 1e12;
        find_nearest(root, x, y);
        return ans;
    }
}

int n;
int64 minn = 1e12;

signed main() {
    read >> n;
    for (register int i = 1; i <= n; i++) {
        pos[i].v[0] = i;
        pos[i].v[1] = pos[i - 1].v[1] + read.get_int<int>();
    }
    kD_Tree::build(kD_Tree::root, 1, n, 0);
    for (register int i = 1; i <= n; i++)
        minn = min(minn, kD_Tree::query_nearest(pos[i].v[0], pos[i].v[1]));
    write << minn << '\n';
    return 0;
}