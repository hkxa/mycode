/*************************************
 * @problem:      bubble.
 * @user_id:      ZJ-00625.
 * @time:         2020-03-07.
 * @language:     C++.
 * @upload_place: CCF - NOI Online.
*************************************/ 

/**
 * This is a wrong solution!
 * as once bubble could not only carry the MaxNum 
 * to the right side, but also carry 2ndMaxNum to 
 * right, left than the original place of MaxNum.
 */

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}
 
#define lowbit(x) ((x) & (-(x)))
#define int int64

int n, m;
int op, c;
int now[200007], t[200007];
int a[200007];
inline void upd(int x, int num)
{
    while (x <= n) {
        t[x] += num;
        x += lowbit(x);
    }
}
inline int query(int x, int base = 0)
{
    while (x) {
        base += t[x];
        x -= lowbit(x);
    }
    return base;
}
inline void updnow(int x, int nov)
{
    upd(x, nov);
    now[x] += nov;
}

/**
 * cmd code @1 : run P6186[way1] <"D:\赵奕\下载\P6186_1.in"
 * cmd code @2 : run P6186[way1] <"D:\赵奕\下载\P6186_1.in" >t.out -q -f & fc "D:\赵奕\下载\P6186_1.out" t.out & DEL t.out 
 * to run this program
 */

signed main()
{
    // freopen("bubble.in", "r", stdin);
    // freopen("bubble.out", "w", stdout);
    n = read<int>();
    m = read<int>();
    // for (register int i = 1; i <= n; i++) a[i] = read<int>();
    for (register int i = 1; i <= n; i++) { 
        a[i] = read<int>();
        now[a[i]] += a[i] - 1 - query(a[i] - 1);
        upd(a[i], 1);
    }
    memset(t, 0, sizeof(t));
    for (int i = 1; i <= n; i++) upd(i, now[i]);
    while (m--) {
        // printf("DEBUG : \n  a{} = ");
        // for (int i = 1; i <= n; i++) write(a[i], i == n ? 10 : 32);
        // printf("now{} = ");
        // for (int i = 1; i <= n; i++) write(now[i], i == n ? 10 : 32);
        op = read<int>();
        c = read<int>();
        if (op == 1) {
            if (a[c] > a[c + 1]) updnow(a[c], -1);
            else if (a[c] < a[c + 1]) updnow(a[c + 1], +1);
            swap(a[c], a[c + 1]);
            // swap(now[a[c]], now[a[c + 1]]);
        } else {
            if (c >= n - 1) puts("0");
            else write(query(n - c), 10);
        }
    }
    return 0;
}