/*************************************
 * @problem:      [WC2020]有根树.
 * @author:       brealid.
 * @time:         2021-01-31.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 5e5 + 7, Q = 1e6 + 7;

int n, q;
vector<int> G[N];
int fa[N], dep[N], siz[N], wson[N], beg[N], dfn[N];
int opt[Q], val[Q];

namespace graph {
    void dfs(int u, int father) {
        fa[u] = father;
        dep[u] = dep[father] + 1;
        siz[u] = 1;\
        for (unsigned i = 0; i < G[u].size(); ++i) {
            int v = G[u][i];
            if (v == father) continue;
            dfs(v, u);
            if (siz[v] > siz[wson[u]]) wson[u] = v;
            siz[u] += siz[v];
        }
    }

    void readln(vector<int> G[]) {
        for (int i = 1, u, v; i < n; ++i) {
            kin >> u >> v;
            G[u].push_back(v);
            G[v].push_back(u);
        }
    }

    int TCS_dft;
    void init_treeChainSplit(int u, int cbeg) {
        dfn[u] = ++TCS_dft;
        beg[u] = cbeg;
        if (!wson[u]) return;
        init_treeChainSplit(wson[u], cbeg);
        for (unsigned i = 0; i < G[u].size(); ++i) {
            int v = G[u][i];
            if (v == fa[u] || v == wson[u]) continue;
            init_treeChainSplit(v, v);
        }
    }
}

// namespace bitr {
//     int tr[N];
//     void add(int u) { for (; u <= n; u += u & -u) ++tr[u]; }
//     void del(int u) { for (; u <= n; u += u & -u) --tr[u]; }
//     int qry(int u) { int r = 0; for (; u; u ^= u & -u) r += tr[u]; return r; }
//     int qry(int l, int r) { return qry(r) - qry(l - 1); }
// }

namespace segt { // 标记持久化线段树
    int tr[N << 2], mx[N << 2], mn[N << 2];
    int cnt[N << 2];
    void range_add(int u, int l, int r, int ml, int mr, int dif) {
        if (l >= ml && r <= mr) {
            tr[u] += dif;
            mx[u] += dif;
            mn[u] += dif;
            return;
        }
        int mid = (l + r) >> 1, ls = u << 1, rs = ls ^ 1;
        if (mid >= ml) range_add(ls, l, mid, ml, mr, dif);
        if (mid < mr) range_add(rs, mid + 1, r, ml, mr, dif);
        mx[u] = max(mx[ls], mx[rs]) + tr[u];
        mn[u] = min(mn[ls], mn[rs]) + tr[u];
    }
    void upd_cnt(int u, int l, int r, int pos, int dif) {
        cnt[u] += dif;
        if (l == r) return;
        int mid = (l + r) >> 1;
        if (pos <= mid) upd_cnt(u << 1, l, mid, pos, dif);
        else upd_cnt(u << 1 | 1, mid + 1, r, pos, dif);
    }
    int query(int u, int l, int r, int limit) {
        if (!cnt[u] || mx[u] <= limit) return 0;
        if (mn[u] > limit) return cnt[u];
        if (l == r) return tr[u] > limit;
        int mid = (l + r) >> 1, ls = u << 1, rs = ls ^ 1;
        return query(ls, l, mid, limit - tr[u]) + query(rs, mid + 1, r, limit - tr[u]);
    }
    inline void add_w(int u, int dif) {
        upd_cnt(1, 1, n, dfn[u], dif);
        while (u) {
            range_add(1, 1, n, dfn[beg[u]], dfn[u], dif);
            u = fa[beg[u]];
        }
    }
}

namespace query {
    void readln(int t[], int v[]) {
        for (int i = 1; i <= q; ++i)
            kin >> t[i] >> v[i];
    }
}

namespace testcase {
    void solve() {
        for (int i = 1, ans = 0; i <= q; ++i) {
            if (opt[i] == 1) {
                segt::add_w(val[i], +1);
                if (segt::query(1, 1, n, ans) > ans) ++ans;
            } else {
                segt::add_w(val[i], -1);
                if (segt::query(1, 1, n, ans - 1) <= ans - 1) --ans;
            }
            kout << ans << '\n';
        }
    }
}

signed main() {
    kin >> n;
    graph::readln(G);
    graph::dfs(1, 0);
    graph::init_treeChainSplit(1, 1);
    kin >> q;
    query::readln(opt, val);
    testcase::solve();
    return 0;
}