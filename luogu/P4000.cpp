/**
 * Problem: P4000 斐波那契数列. 
 * Author:  航空信奥. 
 * Date:    2019-06-12. 
 * Upload:  Luogu. 
 */
#include <math.h>
#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <string.h>
#include <vector>
#include <queue> 
#include <map>
#include <set>
using namespace std;

template <typename _TpInt> inline _TpInt read();
template <typename _TpRealnumber> inline double readr();
template <typename _TpInt> inline void write(_TpInt x);

#define Max_N 100007
long long n;
long long q;  

struct martix {
    long long d[4][4];
    int n, m;
    void setNM(int N, int M)
    {
        n = N;
        m = M;
    }
    void Init_void()
    {
        memset(d, 0, sizeof(d));
    }
    void Init_std()
    {
        memset(d, 0, sizeof(d));
        for (int i = 1; i <= n; i++)
            d[i][i] = 1;
    }
    void Init_fib()
    {
        d[1][1] = 1; d[1][2] = 1;
        d[2][1] = 1; d[2][2] = 0;
    }
    martix() : n(2), m(2) 
    {
        Init_std();
    }
    void modM(long long M)
    {
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= m; j++) {
                d[i][j] %= M;
            }
        }
    }
    martix operator *(const martix &other)
    {
        martix res;
        res.Init_void();
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= m; j++) {
                for (int k = 1; k <= other.m; k++) {
                    res.d[i][k] += d[i][j] * other.d[j][k];
                }
            }
        }
        res.modM(q);
        return res;
    }
};

inline long long gcd(register long long n, register long long m)
{
    while (m ^= n ^= m ^= n %= m); return n;
}

inline long long lcm(register long long n, register long long m)
{
    return n / gcd(n , m) * m;
}

//    martix qpow(martix a, Big_int b)
//    {
//        martix res;
//        while (!b.is1n(0)) {
//            if (b.mod2() == 1) res = res * a;
//            a = a * a;
//            b = b / 2;
//        }
//        return res;
//    }

martix qpow(martix a, long long b)
{
    martix res;
    while (b) {
        if (b & 1) res = res * a;
        a = a * a;
        b = b >> 1;
    }
    return res;
}

#define elif(Is) else if(Is)

long long pi[10007], k[10007];

inline long long Get(register long long p)
{
    switch (p) {
        case 2 : return 3;
        case 3 : return 8;
        case 5 : return 20;
        default : break; // use regular way
    }
    register long long s = sqrt(p), tot = 0;
    for (register long long i = 2; i <= s; ++i)
        if (p % i == 0) {
            pi[++tot] = i;
            k[tot] = 1;
            while (p % i == 0) {
                p /= i, k[tot] *= i;
            }
        }
    for (register long long i = 1; i <= tot; ++i) k[i] /= pi[i];
    if (p ^ 1) k[++tot] = 1, pi[tot] = p;
    for (register long long i = 1; i <= tot; ++i) {
        if (pi[i] == 2) k[i]*=3;
        elif (pi[i] == 3) k[i] *= 5;
        elif (pi[i] == 5) k[i] *= 20;
        elif (pi[i] % 5 == 1|| pi[i] % 5 == 4) k[i] *= pi[i] - 1;
        else k[i] *= (pi[i] + 1) << 1;
    }
    register long long ans = k[1];
    for (register long long i = 2; i <= tot; ++i) ans = lcm(ans, k[i]);
    return ans;
}

void moderead(string s, long long &n, long long q)
{
    n = 0;
    for (size_t i = 0; i < s.length(); i++) {
        n = ((n * 10) + (s[i] & 15)) % q;
    }
}

int main() 
{
    // printf("Get(1) = %d.\n", Get(1));
    // printf("Get(3) = %d.\n", Get(3));
    string str;
    cin >> str;
    q = read<long long>();
    if (q == 1) {
        printf("0");
        return 0;
    }
    moderead(str, n, Get(q));
    if (n == 0) {
        printf("0");
        return 0;
    }
    if (n < 3) {
        printf("1");
        return 0;
    }
    martix fib, res;
    fib.Init_fib();
    n = n - 1;
    res = qpow(fib, n);
    martix fl;
    fl.setNM(1, 2);
    write((fl * res).d[1][1]);
    return 0;
}

#define Getchar() getchar()

template <typename _TpInt>
inline _TpInt read()       
{
    register int flag = 1;
    register char c = Getchar();
    while ((c > '9' || c < '0') && c != '-') 
        c = Getchar();
    if (c == '-') flag = -1, c = Getchar();
    register _TpInt init = (c & 15);
    while ((c = Getchar()) <= '9' && c >= '0') 
        init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename _TpRealnumber>
inline double readr()       
{
    register int flag = 1;
    register char c = Getchar();
    while ((c > '9' || c < '0') && c != '-') 
        c = Getchar();
    if (c == '-') flag = -1, c = Getchar();
    register _TpRealnumber init = (c & 15);
    while ((c = Getchar()) <= '9' && c >= '0') 
        init = init * 10 + (c & 15);
    if (c != '.') return init * flag;
    register _TpRealnumber l = 0.1;
    while ((c = Getchar()) <= '9' && c >= '0') 
        init = init + (c & 15) * l, l *= 0.1;
    return init * flag;
}

template <typename _TpInt>
inline void write(_TpInt x)
{
    if (x < 0) {
        putchar('-');
        write<_TpInt>(~x + 1);
    }
    else {
        if (x > 9) write<_TpInt>(x / 10);   
        putchar(x % 10 + '0');
    }
}