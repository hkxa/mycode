/*************************************
 * @problem:      P6066 [RC-02] 求和.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-02-05.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

char ch;
int now, sum;
bool neg, reading, havenum;

inline void cleanTag_Reading()
{
    if (reading) {
        sum += neg ? -now : now;
        // printf("push %d to sum (now sum = %d)\n", neg ? -now : now, sum);
        now = 0;
        havenum = 1;
        reading = 0;
        neg = 0;
    }
}

inline void cleanTag_Havenum()
{
    if (havenum) {
        write(sum, 10);
        sum = 0;
        havenum = 0;
    }
}

inline void addTag_Reading()
{
    if (!reading) now = 0;
    reading = havenum = 1;
}

inline void flush()
{
    cleanTag_Reading();
    cleanTag_Havenum();
}

int main()
{
    while (true) {
        ch = getchar();
        if (ch == -1) {
            flush();
            return 0;
        } else if (ch == '\n') {
            flush();
        } else if (isdigit(ch)) {
            addTag_Reading();
            now = (now * 10) + (ch & 15);
        } else if (ch == '-') {
            if (reading) cleanTag_Reading();
            else neg = 1;
        } else {
            cleanTag_Reading();
            if (neg) neg = 0;
        }
    }
    return 0;
}