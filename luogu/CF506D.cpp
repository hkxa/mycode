/*********************************************************
 * Do not mind how heavily it is raining,                *
 * as there's always sun shinning after the rain passed. *
 *********************************************************
 * @problem:      Mr. Kitayuta's Colorful Graph.         *
 * @author:       brealid.                               *
 * @time:         2020-01-23.                            *
 *********************************************************/
#include <bits/stdc++.h>

namespace fastio {
    namespace base {
        static const int BufferLen = 1e6;
        struct Getchar {
            char buf[BufferLen], *p1, *p2;
            Getchar() : p1(buf), p2(buf) {}
            char predict() {
                if (p1 == p2) p2 = buf + fread(p1 = buf, 1, BufferLen, stdin);
                return p1 == p2 ? EOF : *p1;
            }
            char operator() () {
                if (p1 == p2) p2 = buf + fread(p1 = buf, 1, BufferLen, stdin);
                return p1 == p2 ? EOF : *p1++;
            }
        } getchar;
        struct Putchar {
            char buf[BufferLen], *p1, *p2;
            Putchar() : p1(buf), p2(buf + BufferLen) {}
            ~Putchar() { fwrite(buf, 1, p1 - buf, stdout); }
            void flush() { fwrite(buf, 1, p1 - buf, stdout); p1 = buf; }
            void operator() (char ch) {
                if (p1 == p2) fwrite(p1 = buf, 1, BufferLen, stdout);
                *p1++ = ch;
            }
        } putchar;
        template<typename I> inline void get_int(I &x) { 
            static char ch = 0;
            bool negative = false;
            while (!isdigit(ch = getchar()) && ch != '-' && ch != EOF);
            if (ch == '-') {
                negative = true;
                x = getchar() & 15;
            } else x = ch & 15;
            while (isdigit(ch = getchar())) x = (((x << 2) + x) << 1) + (ch & 15);
            if (negative) x = -x;
        }
        template<typename I> inline void get_uint(I &x) { 
            static char ch = 0;
            while (!isdigit(ch = getchar()) && ch != EOF);
            x = ch & 15;
            while (isdigit(ch = getchar())) x = (((x << 2) + x) << 1) + (ch & 15);
        }
        inline void get_str(char *str) { 
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            if (*str != EOF)
                while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
        }
        inline void get_cpp_str(std::string &str) { 
            str.clear();
            char cc;
            while (((cc = getchar()) == ' ' || cc == '\n' || cc == '\r' || cc == '\t') && cc != EOF);
            while (cc != ' ' && cc != '\n' && cc != '\r' && cc != '\t' && cc != EOF) {
                str.push_back(cc);
                cc = getchar();
            }
        }
        inline void get_ch(char &ch) { 
            while ((ch = getchar()) == ' ' || ch == '\n' || ch == '\r' || ch == '\t');
        }
        template<typename I> inline void attach_int(I x) { 
            static char buf[23];
            static int top = 0;
            if (x < 0) putchar('-'), x = -x;
            do {
                buf[++top] = '0' | (x % 10);
                x /= 10;
            } while (x);
            while (top) putchar(buf[top--]);
        }
        template<typename I> inline void attach_uint(I x) { 
            static char buf[23];
            static int top = 0;
            do {
                buf[++top] = '0' | (x % 10);
                x /= 10;
            } while (x);
            while (top) putchar(buf[top--]);
        }
        inline void attach_str(const char *str) { 
            while (*str) putchar(*str++);
        }
    }
    struct InputStream {
        InputStream& operator >> (int &x) { base::get_int(x); return *this; }
        InputStream& operator >> (long long &x) { base::get_int(x); return *this; }
        InputStream& operator >> (unsigned &x) { base::get_uint(x); return *this; }
        InputStream& operator >> (unsigned long long &x) { base::get_uint(x); return *this; }
        InputStream& operator >> (char &x) { base::get_ch(x); return *this; }
        InputStream& operator >> (char *x) { base::get_str(x); return *this; }
        InputStream& operator >> (std::string &x) { base::get_cpp_str(x); return *this; }
        bool eof() const { return base::getchar.predict() == EOF; }
        char predict() const { return base::getchar.predict(); }
        operator bool() const { return eof(); }
    };
    struct OutputStream {
        OutputStream& operator << (const int &x) { base::attach_int(x); return *this; }
        OutputStream& operator << (const long long &x) { base::attach_int(x); return *this; }
        OutputStream& operator << (const unsigned &x) { base::attach_uint(x); return *this; }
        OutputStream& operator << (const unsigned long long &x) { base::attach_uint(x); return *this; }
        OutputStream& operator << (const char &x) { base::putchar(x); return *this; }
        OutputStream& operator << (const char *x) { base::attach_str(x); return *this; }
        OutputStream& operator << (const std::string &x) { base::attach_str(x.c_str()); return *this; }
        void put(const char &c) { base::putchar(c); }
        void flush() { base::putchar.flush(); }
    };
}
fastio::InputStream kin;
fastio::OutputStream kout;
template<typename T> T read() { printf("Error type for template-read: Not supportive.\n"); exit(1); }
template<> int read() { int x; kin >> x; return x; }
template<> long long read() { long long x; kin >> x; return x; }
template<> unsigned read() { unsigned x; kin >> x; return x; }
template<> unsigned long long read() { unsigned long long x; kin >> x; return x; }
template<> char read() { return fastio::base::getchar(); }
template<> std::string read() { std::string x; kin >> x; return x; }

const int N = 1e5 + 7;

int n, m, q;
struct Graph {
    std::unordered_map<int, int> fa;
    int find(int u) {
        std::unordered_map<int, int>::iterator it = fa.find(u);
        return it == fa.end() ? u : it->second = find(it->second);
    }
    void link(int x, int y) {
        x = find(x), y = find(y);
        if (x != y) fa[x] = y;
    }
    bool test(int x, int y) {
        return find(x) == find(y);
    }
} G[N];
std::unordered_set<int> connected_color[N];
std::unordered_map<int, int> ans[N];

signed main() {
    kin >> n >> m;
    for (int i = 1, x, y, c; i <= m; ++i) {
        kin >> x >> y >> c;
        G[c].link(x, y);
        connected_color[x].insert(c);
        connected_color[y].insert(c);
    }
    kin >> q;
    for (int i = 1, u, v; i <= q; ++i) {
        kin >> u >> v;
        if (u > v) std::swap(u, v);
        if (ans[u].count(v)) kout << ans[u][v] << '\n';
        else {
            int result = 0;
            if (connected_color[u].size() < connected_color[v].size()) {
                for (const int &col : connected_color[u])
                    if (G[col].test(u, v)) ++result;
            } else {
                for (const int &col : connected_color[v])
                    if (G[col].test(u, v)) ++result;
            }
            kout << (ans[u][v] = result) << '\n';
        }
    }
    return 0;
}