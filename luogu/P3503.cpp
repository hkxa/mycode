/*************************************
 * @problem:      [POI2010]KLO-Blocks.
 * @user_name:    brealid.
 * @time:         2020-11-05.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if true
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

const int N = 1e6 + 7, M = 50 + 3;

int n, m, a[N];
int64 sum[N];

int query(int HeightLimit) {
    static int sta[N] = {0}, top;
    for (int i = 1; i <= n; ++i) sum[i] = sum[i - 1] + a[i] - HeightLimit;
    sta[top = 1] = 0;
    for (int i = 1; i <= n; ++i)
        if (sum[i] < sum[sta[top]]) sta[++top] = i;
    int ans = 0;
    for (int i = n; i >= 1; --i) {
        if (sta[top] >= i) --top;
        while (top > 1 && sum[sta[top - 1]] <= sum[i]) --top;
        if (sum[sta[top]] <= sum[i]) ans = max(ans, i - sta[top]);
    }
    return ans;
}

signed main() {
    read >> n >> m;
    for (int i = 1; i <= n; ++i) read >> a[i];
    for (int i = 1; i <= m; ++i) write << query(read.get<int>()) << " \n"[i == m];
    return 0;
}