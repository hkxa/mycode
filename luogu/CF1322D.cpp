/*************************************
 * @problem:      Reality Show.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-27.
 * @language:     C++.
 * @fastio_ver:   20200820.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore space, \t, \r, \n
            ch = getchar();
            while (ch == ' ' || ch == '\t' || ch == '\r' || ch == '\n') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            Int flag = 1;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = -1, endch = getchar();
            i = endch & 15;
            while (isdigit(endch = getchar())) i = (i << 3) + (i << 1) + (endch & 15);
            if (flag == -1) i = -i;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            Int flag = 1;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = -1, endch = getchar();
            Int i = endch & 15;
            while (isdigit(endch = getchar())) i = (i << 3) + (i << 1) + (endch & 15);
            if (flag == -1) i = -i;
            return i;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64
// #define int128 int64
// #define int128 __int128

const int N = 2048;
int n, m, log2n, t, ans, l[N], s[N], c[N];
int dp[N][N], mxv[N][N * 2];

void update(int *num, int *val) {
    for (int i = 0; i < t; ++i) val[i | t] = num[i];
    for (int i = t - 1; i; --i) val[i] = max(val[i << 1], val[i << 1 | 1]);
}

int calc(int p, int mask) {
    int ans = c[p];
    while (mask & 1) ans += c[++p], mask >>= 1;
    return ans;
}

template<typename _T>
inline void chkmax(_T &x, _T y) { if(x < y) x = y; }

int main() {
    read >> n >> m;
    log2n = log2(n), t = 1 << (log2n + 1);
    for (int i = n; i; i--) read >> l[i];
    for (int i = n; i; i--) read >> s[i];
    for (int i = 1; i <= m + log2n; ++i) read >> c[i];
    memset(dp, 0xcf, sizeof(dp)), dp[0][1] = 0;
    for (int i = 0; i <= m; ++i) update(dp[i], mxv[i]);
    for (int i = 1; i <= n; ++i) {
        int val = l[i], w;
        for (int j = val; j >= 0; --j) {
            w = max(log2n + 1 - val + j, 0);
            for (int k = 0; k < (1 << w); ++k)
                chkmax(dp[val][k + 1], mxv[j][k | (1 << w)] + calc(val, k) - s[i]);
        }
        update(dp[val], mxv[val]);
    }
    for (int i = 0; i <= m; ++i) ans = max(ans, mxv[i][1]);
    write << ans << '\n';
}