/*************************************
 * problem:      P5019.
 * user ID:      85848.
 * user name:    hkxadpall.
 * time:         2019-08-29.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

int n, a[100000 + 7];

long long get(int l, int r, int delta)
{
    if (l > r) return 0;
    if (l == r) return a[l] - delta;
    int minnum = 10000 + 7, minpos = 0;
    for (int i = l; i <= r; i++) {
        if (a[i] < minnum) {
            minnum = a[i];
            minpos = i;
        }
    }
    return minnum - delta + get(l, minpos - 1, minnum) + get(minpos + 1, r, minnum);
}

int main()
{
    scanf("%d", &n);
    for (int i = 1; i <= n; i++) {
        scanf("%d", a + i);
    }
    printf("%lld", get(1, n, 0));
    return 0;
}