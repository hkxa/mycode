//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Broken BST.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-13.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

namespace against_cpp11 {
    const int N = 1e5 + 7;
    int n;
    int value_lsh[N];
    int v[N], l[N], r[N], fa[N];
    int f[N];
    int t[N];
    map<int, int> cnt;
    set<int> ans;
    void BST_DP(int u, int ml, int mr) {
        if (!~u || ml > mr) return;
        if (v[u] >= ml && v[u] <= mr) ans.insert(v[u]);
        BST_DP(l[u], ml, min(v[u] - 1, mr));
        BST_DP(r[u], max(ml, v[u] + 1), mr);
    }
    signed main() {
        read >> n;
        for (int i = 1; i <= n; i++) {
            read >> v[i] >> l[i] >> r[i];
            if (~l[i]) fa[l[i]] = i;
            if (~r[i]) fa[r[i]] = i;
            cnt[v[i]]++;
            // value_lsh[i] = v[i];
        }
        // sort(value_lsh + 1, value_lsh + n + 1);
        // for (int i = 1; i <= n; i++) v[i] = lower_bound(value_lsh + 1, value_lsh + n + 1, v[i]) - value_lsh;
        for (int i = 1; i <= n; i++)
            if (!fa[i]) {
                BST_DP(i, 0, 1e9);
                break;
            }
        int tot = 0;
        for (set<int>::iterator it = ans.begin(); it != ans.end(); it++)
            tot += cnt[*it];
        write << n - tot << '\n'; 
        return 0;
    }
}

signed main() { return against_cpp11::main(); }