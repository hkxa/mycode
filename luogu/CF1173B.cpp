#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

int n;
int m;

int main()
{
    n = read<int>();
    m = (n + 2) / 2;
    write(m, 10);
    for (int i = 1; i <= m; i++) {
        printf("%d %d\n", 1, i);
    }
    for (int i = m + 1; i <= n; i++) {
        printf("%d %d\n", i - m + 1, m);
    }
    return 0;
}