/*************************************
 * problem:      P5498 [LnOI2019]脸滚键盘.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-10.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int64 fexp(int64 a, int64 n, int64 p)
{
    int64 res(1);
    while (n) {
        if (n & 1) res = (res * a) % p;
        a = (a * a) % p;
        n >>= 1;
    }
    return res;
}

#define MaxN 1000007
#define P 100000007LL
#define ksm(x, Muiti) fexp(x, Muiti, P)
#define inv(x) ksm(x, P - 2)
// #define _sub_mul(arr, l, r) ((arr[r] * inv(arr[(l) - 1])) % P)
#define _sub_sum(arr, l, r) ((arr[r] - arr[max((l) - 1, 0)] + P) % P)
// #define SMmul(l, r) _sub_mul(_mul, l, r)
#define SSmul(l, r) _sub_sum(_mul_sum, l, r)
#define SSinvMul(l, r) (((l) - 1) < 0 ? _invMul_sum[r] : _sub_sum(_invMul_sum, l, r))
#define SSsdm(l, r) _sub_sum(_SdM_sum, l, r)
#define askTot(l, r) (((_mul_sum[r] * SSinvMul(l - 1, r - 1) % P) - SSsdm(l - 1, r - 1) + P) % P)
#define query(l, r) (askTot(l, r) * inv((r - l + 1) * (r - l + 2) / 2) % P)

int n, q;
int64 a[MaxN];
int64 _mul[MaxN]; // a 的前缀积
int64 _mul_sum[MaxN]; // _mul 的前缀和
int64 _invMul_sum[MaxN]; // _mul 的乘法逆元前缀和
int64 _SdM_sum[MaxN]; // _mul_Sum Div _Mul 的前缀和

int main()
{
    n = read<int>();
    q = read<int>();
    _mul[0] = 1;
    _invMul_sum[0] = 1;
    /**
     * _mul         6   72    432    1296     34992
     * _mul_sum     6   78    510    1806     36778
     * _invMul_sum  1/6 13/72 79/432 119/648  6427/34992
     * _SdM_sum     1   25/12 235/72 2051/108 350651/17496
     */
    for (int i = 1; i <= n; i++) {
        a[i] = read<int64>();
        _mul[i] = (_mul[i - 1] * a[i]) % P;
        _mul_sum[i] = (_mul_sum[i - 1] + _mul[i]) % P;
        _invMul_sum[i] = (_invMul_sum[i - 1] + inv(_mul[i])) % P;
        _SdM_sum[i] = (_SdM_sum[i - 1] + (_mul_sum[i] * inv(_mul[i]) % P)) % P;
    }
    int l, r;
    while (q--) {
        l = read<int>();
        r = read<int>();
        write(query(l, r), 10);
    }
    return 0;
}