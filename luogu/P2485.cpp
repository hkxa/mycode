/*************************************
 * @problem:      [SDOI2011]计算器.
 * @author:       brealid.
 * @time:         2021-01-24.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

namespace BSGS {
    typedef long long bsgs_int64;
    // Quick Power a^n in the meaning of mod p
    inline bsgs_int64 qpow(bsgs_int64 a, int n, bsgs_int64 p) {
        bsgs_int64 res = 1;
        for (; n; n >>= 1) {
            if (n & 1) res = res * a % p;
            a = a * a % p;
        }
        return res;
    }
    // BSGS work function, return -1 if no solution, a non-negative answer otherwise
    bsgs_int64 BSGS(bsgs_int64 a, bsgs_int64 k, bsgs_int64 p) {
        if (!(a %= p) && (k %= p)) return -1;
        bsgs_int64 sq = sqrt(p), inva = qpow(a, p - 2, p), now = k;
        map<bsgs_int64, bsgs_int64> mp;
        for (bsgs_int64 i = 0; i < sq; ++i) {
            mp[now] = i;
            now = now * inva % p;
        }
        bsgs_int64 a_psq = qpow(a, sq, p);
        now = 1;
        for (bsgs_int64 j = 0; j < p; j += sq) {
            if (mp.find(now) != mp.end()) return j + mp[now];
            now = now * a_psq % p;
        }
        return -1;
    }
}

signed main() {
    int T, K, y, z, p;
    for (kin >> T >> K; T--; ) {
        kin >> y >> z >> p;
        switch (K) {
            case 1:
                kout << BSGS::qpow(y, z, p) << '\n';
                break;
            case 2:
                if (y % p == 0) {
                    if (z) kout << "Orz, I cannot find x!\n";
                    else kout << "1\n";
                } else {
                    kout << z * BSGS::qpow(y, p - 2, p) % p << '\n';
                }
                break;
            case 3:
                int result = BSGS::BSGS(y, z, p);
                if (~result) kout << result << '\n';
                else kout << "Orz, I cannot find x!\n";
                break;
        }
    }
    return 0;
}

// 146324954 14948940 73162477
// 146324954^17105 == 14948940