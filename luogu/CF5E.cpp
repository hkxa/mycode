//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      watchman 守望者.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-30.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

/**
 * ### <Code-Backup> ###
    read >> n;
    for (int i = 1; i <= n; i++) {
        read >> h[i];
        if (h[i] == h[i - 1]) {
            cnt[i - 1]++;
            i--; n--;
        } else cnt[i] = 1;
    }
    if (n > 1 && h[1] == h[n]) {
        cnt[1] += cnt[n];
        n--;
    }
    for (int i = 1; i <= n; i++) 
        ans += (int64)cnt[i] * (cnt[i] - 1) / 2;
 * ### </Code-Backup> ###
 * 
 * 这是一道动态规划题

题意是有n座山组成一个环，两座山互相能看到的要求是相连的圆弧上没有任何其他的山高度比它们高，求能看到的山的组数。考虑一组山，我们让高度较小且的位置最靠左的那一座山负责贡献答案。

先把这个环变成一条链，把最高的山作为第一个山，按照顺序复制序列，再在最后接上一个最高的山。

设left[i]表示i左边第一个比i高的位置，right[i]表示i右边第一个比i高的位置，count[i]表示在i到right[i]的左开右闭区间内高度等于i的山的数目，那么每座山能看到的山的组数至少有count[i]组和(i,l[i])与(i,r[i])这两组，不过当l[x]=0且r[x]=n时其实是一组，注意判断。

于是问题就变成了求left数组、right数组和count数组，可以通过动态规划的思想来做。

注意数据范围，答案要用long\ longlong long，long\ longlong long在CodeForces要用"%I64d"输出。（当然如果你用cout另说）


 */

namespace against_cpp11 {
    const int N = 1e6 + 7;
    int n, p = 0;
    int64 ans = 0;
    int tH[N], h[N], l[N], r[N], cnt[N];
    int main() {
        read >> n;
        for (int i = 0; i < n; i++) read >> tH[i];
        for (int i = 1; i < n; i++)
            if (tH[i] > tH[p]) p = i;
        for (int i = 0; i <= n; i++)
            h[i] = tH[(i + p) % n]; 
        for (int i = 1; i <= n; i++) {
            l[i] = i - 1; 
            while (l[i] && h[i] >= h[l[i]])
                l[i] = l[l[i]]; 
        }
        for (int i = n - 1; i >= 0; i--) {
            r[i] = i + 1; 
            while (r[i] < n && h[i] > h[r[i]])
                r[i] = r[r[i]]; 
            if (r[i] < n && h[i] == h[r[i]]) {
                // For : situation of {h equal}
                cnt[i] = cnt[r[i]] + 1; 
                r[i] = r[r[i]];
            }
        }
        for (int i = 0; i < n; i++) {
            ans += cnt[i]; // For : situation of {h equal}
            if (h[i] < h[0]) { // not highest
                ans += 2; 
                if (!l[i] && r[i] == n) // shortest
                    ans--; 
            }
        }
        write << ans << '\n';
        return 0;
    }
}

signed main() { return against_cpp11::main(); }