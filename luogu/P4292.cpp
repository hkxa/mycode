/*************************************
 * @problem:      [WC2010]重建计划.
 * @user_name:    brealid/hkxadpall/zhaoyi20/j(o)moo/littleTortoise.
 * @time:         2020-09-20.
 * @language:     C++.
 * @fastio_ver:   20200913.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0,    // input
        flush_stdout = 1 << 1,  // output
        flush_stderr = 1 << 2,  // output
    };
    enum number_type_flags {
        output_double_stable = 1 << 0,  // output
        output_double_faster = 1 << 1   // output
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f & ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore character set = {' ', '\r', '\n', '\t'}
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (double &lf) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            lf = endch & 15;
            while (isdigit(endch = getchar())) lf = lf * 10 + (endch & 15);
            if (endch == '.') {
                double digit = 0.1;
                while (isdigit(endch = getchar())) {
                    lf += (endch & 15) * digit;
                    digit *= 0.1;
                }
            }
            if (flag) lf = -lf;
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            Int d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return d;
        }
    } read;

    struct Writer {
        Writer& operator << (io_flags f) {
            if (f & flush_stdout) fflush(stdout);
            if (f & flush_stderr) fflush(stderr);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
        inline void operator () (double val, int eps_digit = 6, char endch = '\0', number_type_flags flg = output_double_faster) {
            if (flg & output_double_stable) {
                static char output_format[10];
                sprintf(output_format, "%%.%dlf", eps_digit);
                printf(output_format, val);
            } else if (flg & output_double_faster) {
                if (val < 0) {
                    putchar('-');
                    val = -val;
                }
                double eps_number = 0.5;
                for (int i = 1; i <= eps_digit; i++) eps_number /= 10;
                val += eps_number;
                (*this) << (int64)val;
                val -= (int64)val;
                if (eps_digit) putchar('.');
                while (eps_digit--) {
                    val *= 10;
                    putchar((int)val | '0');
                    val -= (int)val;
                }
            } else {
               (*this) << '1';
                if (eps_digit) {
                    (*this) << ".E";
                    for (int i = 2; i <= eps_digit; i++) (*this) << '0';
                }
            }
            if (endch) putchar(endch);
        }
    } write;
}
using namespace Fastio;

namespace File_IO {
    void init_IO(const char *file_name) {
        char buff[107];
        sprintf(buff, "%s.in", file_name);
        freopen(buff, "r", stdin);
        sprintf(buff, "%s.out", file_name);
        freopen(buff, "w", stdout);
    }
}

// #define int int64

const int N = 3e5 + 20;
const double DB_inf_max = 1e13, DB_inf_min = -1e13;

int n, L_edgeCnt, R_edgeCnt;
vector<pair<int, int> > G[N];
int dep[N];         // 记录的是 u 到子树中最深的节点的距离
int wson[N];        // 长儿子
int wedge_val[N];   // 连接 u 到长儿子的边
int dfn[N], dft;    // dfs 序
double f[N], g[N];
double tr[N << 2];  // Maximum-线段树
double eval_dif;    // 全局记录 EdgeVal.Differ 值（该值由二分得到）
double maxPath;     // check 时全局记录答案

void attach_edge(int u, int v, double w, double &lBound, double &rBound) {
    G[u].push_back(make_pair(v, w));
    G[v].push_back(make_pair(u, w));
    if (w < lBound) lBound = w;
    if (w > rBound) rBound = w;
}

void tr_clear(int u, int l, int r) {
    tr[u] = DB_inf_min;
    if (l == r) return;
    int mid = (l + r) >> 1;
    tr_clear(u << 1, l, mid);
    tr_clear(u << 1 | 1, mid + 1, r);
}

void tr_update(int u, int l, int r, int index, double NewerVal) {
    if (NewerVal > tr[u]) tr[u] = NewerVal;
    if (l == r) return;
    int mid = (l + r) >> 1;
    if (index <= mid) tr_update(u << 1, l, mid, index, NewerVal);
    else tr_update(u << 1 | 1, mid + 1, r, index, NewerVal);
}

double tr_query(int u, int l, int r, int ml, int mr) {
    if (l >= ml && r <= mr) return tr[u];
    if (l > mr || r < ml) return DB_inf_min;
    int mid = (l + r) >> 1;
    return max(tr_query(u << 1, l, mid, ml, mr), tr_query(u << 1 | 1, mid + 1, r, ml, mr));
}

struct SegmentTree {
    inline void clear() {
        // printf("function(%s): enter...", __FUNCTION__);
        tr_clear(1, 1, n);
        // printf("exit...\n");
    }
    inline void update(int index, double Xval) {
        // printf("function(%s): enter...", __FUNCTION__);
        tr_update(1, 1, n, index, Xval);
        // printf("exit...\n");
    }
    inline double query(int l, int r) {
        // printf("function(%s): enter...", __FUNCTION__);
        return tr_query(1, 1, n, l, r);
        // printf("exit...\n");
    }
} segtree;

void pretreat_dfs(int u, int fa) {
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i].first, w = G[u][i].second;
        if (v == fa) continue;
        pretreat_dfs(v, u);
        if (dep[u] < dep[v] + 1) {
            // 更新长儿子
            dep[u] = dep[v] + 1;
            wson[u] = v;
            wedge_val[u] = w;
        }
    }
}

void init_dfn(int u, int fa) {
    dfn[u] = ++dft;
    if (!wson[u]) return; // 无长儿子，定然没有其他孩子
    // 长儿子单独处理
    init_dfn(wson[u], u);
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i].first;
        if (v == fa || v == wson[u]) continue;
        init_dfn(v, u);
    }
}

void dfs_solve(int u, int fa) {
    int u_dfn = dfn[u];
    if (!wson[u]) return; // 无长儿子，定然没有其他孩子
    // 长儿子单独处理
    dfs_solve(wson[u], u);
    g[u_dfn] += g[u_dfn + 1] + wedge_val[u] - eval_dif;
    f[u_dfn] = -g[u_dfn];
    segtree.update(u_dfn, f[u_dfn]); 
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i].first;
        if (v == fa || v == wson[u]) continue;
        dfs_solve(v, u);
        int v_dfn = dfn[v];
        double w = G[u][i].second - eval_dif;
        for (int j = 1; j <= dep[v] + 1; j++)
            if (L_edgeCnt - j <= dep[u]) {
                double xx = segtree.query(u_dfn + max(1, L_edgeCnt - j), u_dfn + min(R_edgeCnt - j, dep[u]));
                maxPath = max(maxPath, w + f[v_dfn + j - 1] + g[v_dfn] + g[u_dfn] + xx);
            }
        for (int j = 1; j <= dep[v] + 1; j++)
            if (f[u_dfn + j] < w + f[v_dfn + j - 1] + g[v_dfn] - g[u_dfn]) {
                f[u_dfn + j] = w + f[v_dfn + j - 1] + g[v_dfn] - g[u_dfn];
                segtree.update(u_dfn + j, f[u_dfn + j]);
            }
    }
    if (dep[u] >= L_edgeCnt) // 可以更新：达到最低要求（下界）
        maxPath = max(maxPath, g[u_dfn] + segtree.query(u_dfn + L_edgeCnt, u_dfn + min(R_edgeCnt, dep[u])));
}

bool judge(double x) {
    segtree.clear();
    memset(f, 0, sizeof(f));
    memset(g, 0, sizeof(g));
    eval_dif = x;
    maxPath = DB_inf_min;
    dfs_solve(1, 0);
    // printf("check(%.3lf) : maxPath = %.3lf\n", x, maxPath);
    return maxPath >= 0;
}

double BinarySearch(double l, double r) {
    double mid;
    // printf("BinarySearch in Range[%.3lf, %.3lf]\n", l, r);
    for (int i = 1; i <= 34; i++) {
        mid = (l + r) / 2;
        if (judge(mid)) l = mid;
        else r = mid;
    }
    return (l + r) / 2;
}

signed main() {
    // File_IO::init_IO("[WC2010]重建计划");
    read >> n >> L_edgeCnt >> R_edgeCnt;
    double MinEval = DB_inf_max, MaxEval = DB_inf_min;
    for (int i = 1, u, v, w; i < n; i++) {
        read >> u >> v >> w;
        attach_edge(u, v, w, MinEval, MaxEval);
    }
    pretreat_dfs(1, 0);
    init_dfn(1, 0);
    write(BinarySearch(MinEval, MaxEval), 3, '\n');
    return 0;
}