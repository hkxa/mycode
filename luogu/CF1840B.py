T = int(input())
for _ in range(T):
    n, k = map(int, input().split())
    print(min(n + 1, 2 ** min(k, 30)))