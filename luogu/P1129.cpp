/*************************************
 * @problem:      P1129 [ZJOI2007]矩阵游戏.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-01-22.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int N = 400 + 15, sqrN = N * N;

int head[sqrN], to[sqrN], next[sqrN], tot = 1, ans = 0;

int match[N], visit[N], n, T;

void add(int x, int y) 
{
    to[++tot] = y;
    next[tot] = head[x];
    head[x] = tot;
}

bool tryToMatch(int x) 
{
    for (int e = head[x]; e; e = next[e])
        if (!visit[to[e]]) {
            visit[to[e]] = 1;
            if (!match[to[e]] || tryToMatch(match[to[e]])) {
                match[to[e]] = x; 
                return true;
            }
        }
    return false;
}

int main()
{
    T = read<int>();
    while (T--) {
        ans = 0; 
        memset(head, 0, sizeof(head));
        memset(match, 0, sizeof(match));
        tot = 1;
        n = read<int>();
        for (int i = 1; i <= n; i++)
            for (int j = 1; j <= n; j++)
                if (read<int>() == 1) add(i, j + n);
        for (int i = 1; i <= n; i++) {
            memset(visit, 0, sizeof(visit));
            ans += tryToMatch(i);
        }
        if (ans >= n) puts("Yes");
        else puts("No");
    }
    return 0;
}