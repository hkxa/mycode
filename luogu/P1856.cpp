//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      扫描线求周长并.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jomoo/jmoo/航空信奥.
 * @time:         2020-06-09.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 988244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64
const int N = 2e5 + 7;

int n;
struct line {
    int x, y1, y2, k;
    bool operator < (const line &b) const {
        return x ^ b.x ? x < b.x : k > b.k;
    }
} l[N], h[N];

int lshL[N], lshH[N];
int64 ans = 0;
int64 lenL[N << 4], lenH[N << 4]; 
int cntL[N << 4], cntH[N << 4];

void UpdateL(int ml, int mr, int dif, int u = 1, int l = 0, int r = n - 1) {
    if (l >= ml && r <= mr) {
        cntL[u] += dif;
        if (cntL[u] == 1)  lenL[u] = lshL[r] - lshL[l];
        else if (cntL[u] == 0) lenL[u] = lenL[u << 1] + lenL[u << 1 | 1];
        // printf("lenL[%d (%d~%d)] = %lld\n", u, l, r, lenL[u]);
        return;
    }
    int mid = (l + r) >> 1;
    if (mid > ml) UpdateL(ml, mr, dif, u << 1, l, mid);
    if (mid < mr) UpdateL(ml, mr, dif, u << 1 | 1, mid, r);
    if (cntL[u] == 0) lenL[u] = lenL[u << 1] + lenL[u << 1 | 1];
    // printf("lenL[%d (%d~%d)] = %lld\n", u, l, r, lenL[u]);
}

void UpdateH(int ml, int mr, int dif, int u = 1, int l = 0, int r = n - 1) {
    if (l >= ml && r <= mr) {
        cntH[u] += dif;
        if (cntH[u] == 1)  lenH[u] = lshH[r] - lshH[l];
        else if (cntH[u] == 0) lenH[u] = lenH[u << 1] + lenH[u << 1 | 1];
        // printf("lenH[%d (%d~%d)] = %lld\n", u, l, r, lenH[u]);
        return;
    }
    int mid = (l + r) >> 1;
    if (mid > ml) UpdateH(ml, mr, dif, u << 1, l, mid);
    if (mid < mr) UpdateH(ml, mr, dif, u << 1 | 1, mid, r);
    if (cntH[u] == 0) lenH[u] = lenH[u << 1] + lenH[u << 1 | 1];
    // printf("lenH[%d (%d~%d)] = %lld\n", u, l, r, lenH[u]);
}

signed main() {
    n = read<int>();
    for (int i = 0, x1, y1, x2, y2; i < n; i++) {
        x1 = read<int>();
        y1 = read<int>();
        x2 = read<int>();
        y2 = read<int>();
        l[i << 1] = (line){x1, y1, y2, 1};
        l[i << 1 | 1] = (line){x2, y1, y2, -1};
        h[i << 1] = (line){y1, x1, x2, 1};
        h[i << 1 | 1] = (line){y2, x1, x2, -1};
        lshL[i << 1] = y1;
        lshL[i << 1 | 1] = y2;
        lshH[i << 1] = x1;
        lshH[i << 1 | 1] = x2;
    }
    n <<= 1;
    sort(lshL, lshL + n);
    sort(lshH, lshH + n);
    sort(l, l + n);
    sort(h, h + n);
    for (int i = 0; i < n; i++) {
        l[i].y1 = lower_bound(lshL, lshL + n, l[i].y1) - lshL;
        l[i].y2 = lower_bound(lshL, lshL + n, l[i].y2) - lshL;
        h[i].y1 = lower_bound(lshH, lshH + n, h[i].y1) - lshH;
        h[i].y2 = lower_bound(lshH, lshH + n, h[i].y2) - lshH;
    }
    // for (int i = 0; i < n; i++) {
    //     UpdateL(l[i].y1, l[i].y2, l[i].k);
    //     UpdateH(h[i].y1, h[i].y2, h[i].k);
    //     ans += lenL[1] + lenH[1];
    //     // printf("ans -> %lld\n", ans);
    // }
    for (int i = 0, t; i < n; i++) {
        t = lenL[1];
        UpdateL(l[i].y1, l[i].y2, l[i].k);
        // while (i < n - 1 && l[i].x == l[i + 1].x) {
        //     i++;
        //     UpdateL(l[i].y1, l[i].y2, l[i].k);
        // }
        // printf("lenL[1] = %lld\n", lenL[1]);
        ans += abs(t - lenL[1]);
        // ans += lenL[1];
    }
    // printf("ans = %d\n", ans);
    for (int i = 0, t; i < n; i++) {
        t = lenH[1];
        UpdateH(h[i].y1, h[i].y2, h[i].k);
        // while (i < n - 1 && h[i].x == h[i + 1].x) {
        //     i++;
        //     UpdateH(h[i].y1, h[i].y2, h[i].k);
        // }
        // printf("lenH[1] = %lld\n", lenH[1]);
        ans += abs(t - lenH[1]);
        // ans += lenH[1];
    }
    write(ans, 10);
    return 0;
}

// Create File Date : 2020-06-09