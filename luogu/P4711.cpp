/*************************************
 * @problem:      P4711 「化学」相对分子质量.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2019-12-28.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

char thing[107]; int p = 0;
#define nxt_ch() thing[p]

map<string, int> RelativeAtomicMass; // 已乘二
#define GetMass(element) RelativeAtomicMass[element]

inline void init_RelativeAtomicMass()
{
    RelativeAtomicMass["H"]  = 2;     RelativeAtomicMass["C"]  = 24;
    RelativeAtomicMass["N"]  = 28;    RelativeAtomicMass["O"]  = 32;
    RelativeAtomicMass["F"]  = 38;    RelativeAtomicMass["Na"] = 46;
    RelativeAtomicMass["Mg"] = 48;    RelativeAtomicMass["Al"] = 54;
    RelativeAtomicMass["Si"] = 56;    RelativeAtomicMass["P"]  = 62;
    RelativeAtomicMass["S"]  = 64;    RelativeAtomicMass["Cl"] = 71;
    RelativeAtomicMass["K"]  = 78;    RelativeAtomicMass["Ca"] = 80;
    RelativeAtomicMass["Mn"] = 110;   RelativeAtomicMass["Fe"] = 112;
    RelativeAtomicMass["Cu"] = 128;   RelativeAtomicMass["Zn"] = 130;
    RelativeAtomicMass["Ag"] = 216;   RelativeAtomicMass["I"]  = 254;
    RelativeAtomicMass["Ba"] = 274;   RelativeAtomicMass["Hf"] = 357;
    RelativeAtomicMass["Pt"] = 390;   RelativeAtomicMass["Au"] = 394;
    RelativeAtomicMass["Hg"] = 402;
}

inline string GetContent()
{
    if (isupper(thing[p]) && islower(thing[p + 1])) {
        p += 2;
        return (string)"" + thing[p - 2] + thing[p - 1];
    } else if (isdigit(thing[p])) {
        string ret;
        while (isdigit(thing[p])) ret += thing[p++];
        // printf("Get Content Number %s\n", ret.c_str());
        return ret;
    } else return (string)"" + thing[p++];
}

inline int strToNum(string num)
{
    int ret = 0;
    for (unsigned i = 0; i < num.size(); i++) ret = ret * 10 + (num[i] & 15);
    // printf("string %s to num %d\n", num.c_str(), ret);
    return ret;
}

int calc()
{
    int ret = 0, tmp;
    string t;
    while (thing[p]) {
        if (isupper(nxt_ch())) {
            // printf("Case Upper\n");
            t = GetContent();
            // printf("Get %s\n", t.c_str());
            tmp = GetMass(t);
            // printf("Mass %d\n", tmp);
            if (nxt_ch() == '_') {
                GetContent(); GetContent();
                tmp *= strToNum(GetContent());
                GetContent();
            }
            // printf("Total Mass %d\n", tmp);
            ret += tmp;
            // printf("Now Ret %d\n", ret);
        } else if (nxt_ch() == '~') {
            GetContent();
            if (isdigit(nxt_ch())) {
                tmp = strToNum(GetContent());
            } else tmp = 1;
            ret += tmp * calc();
        } else if (nxt_ch() == '(') {
            GetContent();
            tmp = calc();
            GetContent();
            if (nxt_ch() == '_') {
                GetContent(); GetContent();
                tmp *= strToNum(GetContent());
                GetContent();
            }
            ret += tmp;
        } else if (nxt_ch() == ')') break;
    }
    return ret;
}

int main()
{
    init_RelativeAtomicMass();
    scanf("%s", thing);
    int ans = calc();
    if (ans & 1) printf("%d.5", ans / 2);
    else printf("%d", ans / 2);
    return 0;
}