/*************************************
 * problem:      P3845 [TJOI2007]球赛.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-04-18.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

int n;
struct round {
    int x, y;
    void readIn()
    {
        x = read<int>();
        y = read<int>();
        if (x > y) swap(x, y);
    }
    bool operator < (const round &r) const
    {
        return x ^ r.x ? x < r.x : y < r.y;
    }
} a[1007] = {0};

void play()
{
    n = read<int>();
    memset(a, 0, sizeof(a));
    for (int i = 1; i <= n; i++) {
        a[i].readIn();
    }

}

int main()
{
    int nT = read<int>();
    while (nT--) play();
    return 0;
}