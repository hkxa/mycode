/*************************************
 * @problem:      P5629 【AFOI-19】区间与除法.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-01-29.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, m, d, q;
int64 a[500007], attack[63], t;
map<int64, int> attacks;
int _log[500007];
int64 st[19 + 3][500007];

inline int popcount64(int64 x)
{
    register int cnt(0);
    while (x) {
        cnt++;
        x -= x & (-x);
    }
    return cnt;
}

int main()
{
    n = read<int>();
    m = read<int>();
    d = read<int>();
    q = read<int>();
    _log[0] = -1;
    for (register int i = 1; i <= n; i++) _log[i] = _log[i >> 1] + 1;
    for (register int i = 1; i <= n; i++) a[i] = read<int64>();
    for (register int i = 1; i <= m; i++) attacks[attack[i] = read<int64>()] = i;
    for (register int i = 1, could; i <= n; i++) {
        t = a[i];
        could = 0;
        while (t) {
            if (attacks.find(t) != attacks.end()) could = attacks[t];
            t /= d;
        }
        if (could) st[0][i] = 1LL << (could - 1);
    }
    for (register int k = 1; k < 19; k++) {
        for (register int i = 1, lim = n - (1LL << k) + 1; i <= lim; i++) {
            st[k][i] = st[k - 1][i] | st[k - 1][i + (1LL << (k - 1))];
        }
    }
    for (register int i = 1, l, r, k, cnt; i <= q; i++) {
        l = read<int>();
        r = read<int>();
        k = _log[r - l + 1];
        t = st[k][l] | st[k][r - (1LL << k) + 1];
        cnt = 0;
        while (t) {
            cnt++;
            t -= t & (-t);
        }
        write(cnt, 10);
    }
    return 0;
}