T = int(input())
for _ in range(T):
    n = int(input())
    if n > 4:
        print('Alice')
    else:
        answer = {
            2: 'Bob',
            3: 'Bob', 
            4: 'Bob'
        }
        print(answer[n])