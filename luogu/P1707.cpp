/*************************************
 * problem:      P1707 刷题比赛.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-09.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

unsigned long long N, K;
unsigned long long p, q, r, t;
unsigned long long u, v, w;
unsigned long long x, y, z;

unsigned long long mul(unsigned long long x, unsigned long long y)
{
    unsigned long long res = 0;
    if (x < y) swap(x, y);
    if (!x || !y) return 0;
    while (y > 1) {
        if (y & 1) res = (x + res) % K;
        x = (x + x) % K;
        y >>= 1;
    }
    return (res + x) % K;
}

#define EMPTY_MATRIX 19260817
#define UNIT_MATRIX 114514
struct matrix {
    unsigned long long n, m;
    unsigned long long a[21][21];

    matrix(unsigned long long _n, unsigned long long _m, unsigned long long matrixType) : n(_n), m(_m)
    { 
        memset(a, 0, sizeof(a));
        if (matrixType == UNIT_MATRIX) for (unsigned long long i = 1; i <= _n; i++) a[i][i] = 1;
    }

    matrix operator * (const matrix &Timeser)
    {
        matrix res(n, Timeser.m, EMPTY_MATRIX);
        for (unsigned long long i = 1; i <= n; i++) {
            for (unsigned long long j = 1; j <= Timeser.m; j++) {
                for (unsigned long long k = 1; k <= m; k++) {
                    res.a[i][j] += mul(a[i][k], Timeser.a[k][j]) % K;
                    res.a[i][j] %= K;
                }
            }
        }
        return res;
    }
};

matrix fexp(matrix a, unsigned long long n)
{
    matrix res(a.n, a.m, UNIT_MATRIX);
    while (n) {
        if (n & 1) res = a * res;
        a = a * a;
        n >>= 1;
    }
    return res;
}

int main()
{
    N = read<unsigned long long>();
    K = read<unsigned long long>();
    // line 1
    p = read<unsigned long long>();
    q = read<unsigned long long>();
    r = read<unsigned long long>();
    t = read<unsigned long long>();
    // line 2
    u = read<unsigned long long>();
    v = read<unsigned long long>();
    w = read<unsigned long long>();
    // line 3
    x = read<unsigned long long>();
    y = read<unsigned long long>();
    z = read<unsigned long long>();
    // line 4
    matrix facer(11, 11, EMPTY_MATRIX), baser(11, 1, EMPTY_MATRIX), ans(11, 1, UNIT_MATRIX);
    // facer.n = 11;
    // facer.m = 11;
    facer.a[1][1] = p;
    facer.a[1][2] = 1;
    facer.a[1][3] = 1;
    facer.a[1][4] = q;
    facer.a[1][9] = r;
    facer.a[1][10] = t;
    facer.a[1][11] = 1;
    facer.a[2][1] = 1;
    facer.a[2][2] = u;
    facer.a[2][3] = 1;
    facer.a[2][5] = v;
    facer.a[2][7] = 1;
    facer.a[3][1] = 1;
    facer.a[3][2] = 1;
    facer.a[3][3] = x;
    facer.a[3][6] = y;
    facer.a[3][8] = 1;
    facer.a[3][10] = 1;
    facer.a[3][11] = 2;
    facer.a[4][1] = 1;
    facer.a[5][2] = 1;
    facer.a[6][3] = 1;
    facer.a[7][7] = w;
    facer.a[8][8] = z;
    facer.a[9][9] = 1;
    facer.a[9][10] = 2;
    facer.a[9][11] = 1;
    facer.a[10][10] = 1;
    facer.a[10][11] = 1;
    facer.a[11][11] = 1;
    // baser.n = 11;
    // baser.m = 1;
    baser.a[1][1] = 3;
    baser.a[2][1] = 3;
    baser.a[3][1] = 3;
    baser.a[4][1] = 1;
    baser.a[5][1] = 1;
    baser.a[6][1] = 1;
    baser.a[7][1] = w;
    baser.a[8][1] = z;
    baser.a[9][1] = 1;
    baser.a[10][1] = 1;
    baser.a[11][1] = 1;
    ans = fexp(facer, N - 2) * baser;
    printf("nodgd %llu\n", ans.a[1][1]);
    printf("Ciocio %llu\n", ans.a[2][1]);
    printf("Nicole %llu\n", ans.a[3][1]);
    return 0;
}