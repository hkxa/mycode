//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      E - Inversions After Shuffle.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-12.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

namespace against_cpp11 {
    const int N = 2e5 + 7;
    int n;
    int a[N], t[N];
    __float128 ans = 0;
    struct BinaryIndexTree {
        int64 val[N];
        BinaryIndexTree() {
            memset(val, 0, sizeof(val));
        }
        void add(int u, int dif) {
            while (u <= n) {
                val[u] += dif;
                u += u & -u;
            }
        }
        int64 query(int u) {
            int64 ret = 0;
            while (u) {
                ret += val[u];
                u -= u & -u;
            }
            return ret;
        }
    } c, s;
    // calc(s) = s*(s-1)/4
    // Q(i,j) = nxd(1,i-1)+nxd(j+1,n)+calc(j-i+1)
    // Ans = ∑_{1<=i<=j<=n}Q(i,j) / (n*(n-1)/2)
    signed main() {
        read >> n;
        for (int i = 1; i <= n; i++) read >> a[i];
        double ans = 0;
        for (int i = 1; i <= n; i++) ans += ((int64)i * (n - i) * (n - i + 1)); // contribute calc 
        for (int i = n; i >= 1; i--) { // Nxd Specific calc
            ans += 2ll * c.query(a[i]) * ((int64)n * (n + 1) - 2 * i - 2ll * n * i);
            ans += 4ll * s.query(a[i]) * i; // s has already * i
            c.add(a[i], 1);
            s.add(a[i], i);
        }
        ans /= ((int64)n * (n + 1)) * 2;
        write << (int64)(ans) << '.';
        ans -= (int64)(ans);
        for (int i = 1; i <= 23; i++) {
            ans = ans * 10;
            putchar('0' | (int)(ans));
            ans -= (int)(ans);
        }
        write << '\n';
        return 0;
    }
}

signed main() { return against_cpp11::main(); }