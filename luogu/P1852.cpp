/*************************************
 * @problem:      hop.
 * @user_name:    brealid/hkxadpall/zhaoyi20/j(o)moo/littleTortoise.
 * @time:         2020-09-14.
 * @language:     C++.
 * @fastio_ver:   20200827.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        // simple io flags
        ignore_int = 1 << 0,    // input
        char_enter = 1 << 1,    // output
        flush_stdout = 1 << 2,  // output
        flush_stderr = 1 << 3,  // output
        // combination io flags
        endline = char_enter | flush_stdout // output
    };
    enum number_type_flags {
        // simple io flags
        output_double_stable = 1 << 0,  // output
        output_double_faster = 1 << 1   // output
        // combination io flags
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f & ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore character set : ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (double &lf) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            lf = endch & 15;
            while (isdigit(endch = getchar())) lf = lf * 10 + (endch & 15);
            if (endch == '.') {
                double digit = 0.1;
                while (isdigit(endch = getchar())) {
                    lf += (endch & 15) * digit;
                    digit *= 0.1;
                }
            }
            if (flag) lf = -lf;
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            Int d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return d;
        }
    } read;

    struct Writer {
        Writer& operator << (io_flags f) {
            if (f & char_enter) putchar(10);
            if (f & flush_stdout) fflush(stdout);
            if (f & flush_stderr) fflush(stderr);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
        inline void operator () (double val, int eps_digit = 6, char endch = '\0', number_type_flags flg = output_double_faster) {
            if (flg & output_double_stable) {
                static char output_format[10];
                sprintf(output_format, "%%.%dlf", eps_digit);
                printf(output_format, val);
            } else if (flg & output_double_faster) {
                if (val < 0) {
                    putchar('-');
                    val = -val;
                }
                (*this) << (int64)val;
                val -= (int64)val;
                putchar('.');
                double eps_number = 0.5;
                for (int i = 1; i <= eps_digit; i++) eps_number /= 10;
                val += eps_number;
                while (eps_digit--) {
                    val *= 10;
                    putchar((int)val | '0');
                    val -= (int)val;
                }
            } else {
                (*this) << "1.#ERROR_NOT_IDENTIFIED_FLAG";
            }
            if (endch) putchar(endch);
        }
    } write;
}
using namespace Fastio;
namespace File_IO {
    void init_IO() {
        freopen("hop.in", "r", stdin);
        freopen("hop.out", "w", stdout);
    }
}

// #define int int64

int gcd(int x, int y) {
    return x ? gcd(y % x, x) : y;
}

namespace logicSort {
    void sort3(int &a, int &b, int &c) {
        if (a < b) {
            if (b < c) return;
            else if (a < c) swap(b, c);
            else { int t = c; c = b; b = a; a = t; }
        } else {
            if (c < b) swap(a, c);
            else if (a < c) swap(a, b);
            else { int t = a; a = b; b = c; c = t; }
        }
    }
}

void report_exit(const char *report_message) {
    puts(report_message);
    exit(0);
}

int a, b, c, x, y, z;

struct status {
    int a, b, c;
    bool operator < (const status &x) const {
        return (a ^ x.a) ? (a < x.a) : 
              ((b ^ x.b) ? (b < x.b) : c < x.c);
    }
    bool operator == (const status &x) const {
        return a == x.a && b == x.b && c == x.c;
    }
    bool operator != (const status &x) const {
        return a != x.a || b != x.b || c != x.c;
    }
} ST, ED;

pair<status, int> FindRoot(status x) {
    if (x.b * 2 == x.a + x.c) return make_pair(x, 0);
    if (x.b - x.a > x.c - x.b) {
        int dif = x.c - x.b, step = (x.b - x.a - 1) / (x.c - x.b);
        pair<status, int> ret = FindRoot((status){x.a, x.b - dif * step, x.c - dif * step});
        return make_pair(ret.first, ret.second + step);
    } else {
        int dif = x.b - x.a, step = (x.c - x.b - 1) / (x.b - x.a);
        pair<status, int> ret = FindRoot((status){x.a + dif * step, x.b + dif * step, x.c});
        return make_pair(ret.first, ret.second + step);
    }
}

status climb(status x, int remain_step) {
    // printf("climb({%d, %d, %d}, %d)\n", x.a, x.b, x.c, remain_step);
    if (!remain_step) return x;
    if (x.b - x.a > x.c - x.b) {
        int dif = x.c - x.b, step = (x.b - x.a - 1) / (x.c - x.b);
        step = min(step, remain_step);
        return climb((status){x.a, x.b - dif * step, x.c - dif * step}, remain_step - step);
    } else {
        int dif = x.b - x.a, step = (x.c - x.b - 1) / (x.b - x.a);
        step = min(step, remain_step);
        return climb((status){x.a + dif * step, x.b + dif * step, x.c}, remain_step - step);
    }
}

signed main() {
    // File_IO::init_IO();
    read >> a >> b >> c >> x >> y >> z;
    logicSort::sort3(a, b, c);
    logicSort::sort3(x, y, z);
    ST = (status){a, b, c};
    ED = (status){x, y, z};
    pair<status, int> Srt = FindRoot(ST);
    pair<status, int> Ert = FindRoot(ED);
    if (Srt.first != Ert.first) {
        puts("NO");
        return 0;
    } puts("YES");
    int l = 0, r, mid, ans, preJump;
    if (Srt.second > Ert.second) {
        ST = climb(ST, Srt.second - Ert.second);
        preJump = Srt.second - Ert.second;
        r = Ert.second;
    } else {
        ED = climb(ED, Ert.second - Srt.second);
        preJump = Ert.second - Srt.second;
        r = Srt.second;
    };
    while (l <= r) {
        mid = (l + r) >> 1;
        if (climb(ST, mid) == climb(ED, mid)) r = mid - 1, ans = mid;
        else l = mid + 1;
    }
    write << ans * 2 + preJump << '\n';
    return 0;
}