//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      [APIO2019] 路灯.
 * @user_id:      63720.
 * @user_name:    brealid.
 * @time:         2020-07-18.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 
// #pragma GCC optimize(3)
#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int N = 3e5 + 7, SIZ = 4e7 + 7;

int n, m;
struct interval {
    int l, r;
    interval() : l(0), r(0) {}
    interval(int L, int R) : l(L), r(R) {}
    inline int mid() const { return (l + r) >> 1; }
    inline interval get_L() const { return interval(l, mid()); }        // get left
    inline interval get_R() const { return interval(mid() + 1, r); }    // get right
    inline int size() const { return r - l + 1; }
    inline bool exist() const { return l <= r; }
    inline bool isInclude(int pos) const { return pos >= l && pos <= r; }
    inline bool isInclude(interval s) const { return s.l >= l && s.r <= r; }
    inline bool noUnion(interval s) const { return s.r < l || s.l > r; }
    inline bool only_point(int pos) const { return pos == l && pos == r; }
} full;

struct square {
    interval x, y;
    square() : x(), y() {}
    square(interval L, interval R) : x(L), y(R) {}
    square(int xl, int xr, int yl, int yr) : x(xl, xr), y(yl, yr) {}
    inline square get_L() const { return square(x, y.get_L()); }            // get left
    inline square get_R() const { return square(x, y.get_R()); }            // get right
    inline square get_U() const { return square(x.get_L(), y); }            // get up
    inline square get_D() const { return square(x.get_R(), y); }            // get down
    inline square get_LU() const { return square(x.get_L(), y.get_L()); }   // get left-up
    inline square get_RU() const { return square(x.get_L(), y.get_R()); }   // get right-up
    inline square get_LD() const { return square(x.get_R(), y.get_L()); }   // get left-down
    inline square get_RD() const { return square(x.get_R(), y.get_R()); }   // get right-down
    inline bool exist() const { return x.exist() && y.exist(); }
    inline bool isInclude(int X, int Y) const { return x.isInclude(X) && y.isInclude(Y); }
    inline bool isInclude(square s) const { return x.isInclude(s.x) && y.isInclude(s.y); }
    inline bool noUnion(square s) const { return x.noUnion(s.x) || y.noUnion(s.y); }
    inline bool only_point(int X, int Y) const { return x.only_point(X) && y.only_point(Y); }
};

struct LampExpander {
    int t[N];
    void add(int u, int dif) {
        while (u <= n) {
            t[u] += dif;
            u += u & -u;
        }
    }
    int sum(int u) {
        if (u < 0) return 0;
        int ret = 0;
        while (u) {
            ret += t[u];
            u -= u & -u;
        }
        return ret;
    }
    int findL(int u) {
        if (sum(u - 1) - sum(u - 2) == 0) return u;
        int l = 1, r = u - 1, mid, ret = u;
        while (l <= r) {
            mid = (l + r) >> 1;
            if (sum(u - 1) - sum(mid - 1) == u - mid) r = mid - 1, ret = mid;
            else l = mid + 1;
        }
        return ret;
    }
    int findR(int u) {
        if (sum(u + 1) - sum(u) == 0) return u;
        int l = u + 1, r = n, mid, ret = u;
        while (l <= r) {
            mid = (l + r) >> 1;
            if (sum(mid) - sum(u) == mid - u) l = mid + 1, ret = mid;
            else r = mid - 1;
        }
        return ret;
    }
} BIT;

int rt[N << 2], l[SIZ], r[SIZ], val[SIZ], cnt = 0;
bool status[N];

void BufferReadStatus() {
    char buffer[N];
    scanf("%s", buffer + 1);
    for (int i = 1; i <= n; i++) status[i] = buffer[i] & 1;
}

char GetOperation() {
    static char ch;
    ch = getchar();
    while (ch != 'q' && ch != 't') ch = getchar();
    getchar(); getchar(); getchar(); getchar();
    if (ch == 't') getchar();
    return ch;
}

void update_(int &u, interval now, interval upd, int dif) {
    if (!now.exist() || upd.noUnion(now)) return;
    if (!u) u = ++cnt;
    if (upd.isInclude(now)) {
        val[u] += dif;
        // printf("(found [%d, %d] in [%d, %d]) val set to %d\n", now.l, now.r, upd.l, upd.r, val[u]);
        return;  
    }
    update_(l[u], now.get_L(), upd, dif);
    update_(r[u], now.get_R(), upd, dif);
}

void update(int u, interval now, square upd, int dif) {
    if (!now.exist() || upd.x.noUnion(now)) return;
    if (upd.x.isInclude(now)) {
        // printf("found sub_interval[%d, %d] in [%d, %d] ↓update_info\n", now.x.l, now.x.r, upd.x.l, upd.x.r);
        update_(rt[u], full, upd.y, dif);
        return;  
    }
    update(u << 1, now.get_L(), upd, dif);
    update(u << 1 | 1, now.get_R(), upd, dif);
}

int query_(const int &u, interval now, int x) {
    if (!now.exist() || !now.isInclude(x) || !u) return 0;
    if (now.only_point(x)) return val[u];
    return query_(l[u], now.get_L(), x) + query_(r[u], now.get_R(), x) + val[u];
}

int query(int u, interval now, int x, int y) {
    if (!now.exist() || !now.isInclude(x)) return 1;
    if (now.only_point(x)) return query_(rt[u], full, y);
    if (x <= now.mid()) return query(u << 1, now.get_L(), x, y) + query_(rt[u], full, y);
    else return query(u << 1 | 1, now.get_R(), x, y) + query_(rt[u], full, y);
}

int main()
{
    n = read<int>();
    m = read<int>();
    full = interval(1, n);
    BufferReadStatus();
    for (int i = 1; i <= n; i++) {
        if (status[i]) BIT.add(i, 1);
    }
    // update(1, full, square(1, l - 1, l, r), add(P - InvP, 1));
    for (int i = 1, a, b, l, r; i <= m; i++) {
        if (GetOperation() == 't') {
            a = read<int>();
            l = BIT.findL(a);
            r = BIT.findR(a);
            if (status[a]) {
                update(1, full, square(l, a, a, r), i);
                BIT.add(a, -1);
                status[a] = 0;
            } else {
                update(1, full, square(l, a, a, r), -i);
                BIT.add(a, 1);
                status[a] = 1;
            }
        } else {
            a = read<int>();
            b = read<int>();
            // write(query(1, full, a, b - 1) + i, 10);
            if (BIT.sum(b - 1) - BIT.sum(a - 1) == b - a) {
                // printf("here ");
                write(query(1, full, a, b - 1) + i, 10);
            } else {
                write(query(1, full, a, b - 1), 10);
            }
        }
        // for (int l = 0; l < n; l++)
        //     for (int r = l + 1; r < n; r++)
        //         fprintf(stderr, "query(%d, %d) = %d\n", l, r, query(1, full, l, r));
    }
    return 0;
}