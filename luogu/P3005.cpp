/*************************************
 * @problem:      P3005 [USACO10DEC]槽的游戏The Trough Game.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-02-18.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define bitCount(x) __builtin_popcount(x)

int n, m;
char str[27];
int type[107], cnt[107];
bool fail, succeeded = false;
int success_status;

int main()
{
    n = read<int>();
    m = read<int>();
    for (int i = 1; i <= m; i++) {
        scanf("%s", str);
        cnt[i] = read<int>();
        for (int j = 0; j < n; j++) 
            if (str[j] & 1) type[i] |= (1 << j);
    }
    for (int i = 0; i < (1 << n); i++) {
        fail = false;
        for (int j = 1; j <= m; j++) {
            if (bitCount(i & type[j]) != cnt[j]) {
                fail = true;
                break;
            }
        }
        if (!fail) {
            if (succeeded) {
                puts("NOT UNIQUE");
                return 0;
            }
            succeeded = true;
            success_status = i;
        }
    }
    if (!succeeded) puts("IMPOSSIBLE");
    else for (int i = 0; i < n; i++) 
        putchar((success_status & (1 << i)) ? '1' : '0');
    putchar(10);
    return 0;
}