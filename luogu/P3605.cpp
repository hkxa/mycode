/*************************************
 * @problem:      P3605 [USACO17JAN]Promotion Counting P.
 * @user_id:      63720.
 * @user_name:    brealid.
 * @time:         2020-05-19.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{

    write(x);
    putchar(nextch);
}

// n, p : 与题目意思相仿
// tp : aka "temp p" 用于离散化
int n, p[100007], tp[100007];
// 存图
vector<int> G[100007];
// 存答案 Answers
int ans[100007];

// 树状数组 Tree array
int t[100007]; 
#define lowbit(x) ((x) & (-(x)))
int add(int x, int dif) {
    while (x <= n) {
        t[x] += dif;
        x += lowbit(x);
    }
}
int query(int x) {
    int res = 0;
    while (x) {
        res += t[x];
        x -= lowbit(x);
    }
    return res;
}

void dfs(int u) {
    ans[u] -= query(n) - query(p[u]);
    for (int v : G[u]) dfs(v);
    ans[u] += query(n) - query(p[u]);
    add(p[u], 1);
}

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) 
        tp[i] = p[i] = read<int>();
    for (int i = 2, fa; i <= n; i++) {
        fa = read<int>();
        G[fa].push_back(i);
    }
    // Read Data

    sort(tp + 1, tp + n + 1);
    for (int i = 1; i <= n; i++) 
        p[i] = lower_bound(tp + 1, tp + n + 1, p[i]) - tp;
    // Discretization 离散化

    dfs(1);
    // Dfs

    for (int i = 1; i <= n; i++) write(ans[i], 10);
    // Print Answers
    
    return 0;
}