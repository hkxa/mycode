//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      P2512 [HAOI2008]糖果传递.
 * @user_id:      63720.
 * @user_name:    brealid.
 * @time:         2020-06-02.
 * @language:     C++.
 * @upload_place: luogu.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// #define int int64

int n;
int a[1000007];
int64 sum[1000007];
int64 average, ans;

signed main() {
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        a[i] = read<int>();
        average += a[i];
    }
    average /= n;
    for (int i = 1; i <= n; i++)
        a[i] -= average;
    for (int i = 1; i <= n; i++) 
        sum[i] = sum[i - 1] + a[i];
    sort(sum + 1, sum + n + 1);
    int64 mid = sum[(n + 1) >> 1];
    for (int i = 1; i <= n; i++)
        ans += abs(sum[i] - mid);
    write(ans, 10);
    return 0;
}

// Create File Date : 2020-06-02