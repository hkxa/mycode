/*************************************
 * @problem:      [SDOI2016]齿轮.
 * @user_name:    brealid.
 * @time:         2020-11-14.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

const int primes[] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97}, pcnt = 25;

struct my_ratio {
    bool is_negative;
    int cnt[pcnt];
    my_ratio() : is_negative(false) {
        memset(cnt, 0, sizeof(cnt));
    }
    my_ratio(int val) : is_negative(false) {
        if (val < 0) {
            is_negative = true;
            val = -val;
        }
        memset(cnt, 0, sizeof(cnt));
        for (int i = 0; i < pcnt && val >= primes[i]; ++i)
            while (val % primes[i] == 0) {
                val /= primes[i];
                ++cnt[i];
            }
    }
    my_ratio operator * (const my_ratio &b) const {
        my_ratio ret;
        ret.is_negative = (is_negative != b.is_negative);
        for (int i = 0; i < pcnt; ++i)
            ret.cnt[i] = cnt[i] + b.cnt[i];
        return ret;
    }
    my_ratio operator / (const my_ratio &b) const {
        my_ratio ret;
        ret.is_negative = (is_negative != b.is_negative);
        for (int i = 0; i < pcnt; ++i)
            ret.cnt[i] = cnt[i] - b.cnt[i];
        return ret;
    }
    bool operator != (const my_ratio &b) const {
        if (is_negative != b.is_negative) return true;
        return memcmp(cnt, b.cnt, sizeof(cnt));
    }
};

struct edge {
    int to, nxt;
    my_ratio k;
    edge() {}
    edge(int To, int k1, int k2, int Nxt) : to(To), nxt(Nxt), k(my_ratio(k1) / my_ratio(k2)) {}    
};

const int N = 1000 + 7, M = 20000 + 7;

int n, m;
edge e[M];
int head[N], ecnt;
bool vis[N];
my_ratio rate[N];

void add_edge(int u, int v, int a, int b) {
    e[++ecnt] = edge(v, a, b, head[u]);
    head[u] = ecnt;
}

bool check(int u) {
    vis[u] = true;
    for (int a = head[u]; a; a = e[a].nxt) {
        int v = e[a].to;
        if (!vis[v]) {
            rate[v] = rate[u] * e[a].k;
            if (!check(v)) return false;
        } else if (rate[v] != rate[u] * e[a].k) return false;
    }
    return true;
}

signed main() {
    for (int T = read.get<int>(), case_id = 1; case_id <= T; ++case_id) {
        read >> n >> m;
        ecnt = 0;
        memset(head, 0, sizeof(head));
        memset(vis, 0, sizeof(vis));
        for (int i = 1, u, v, a, b; i <= m; ++i) {
            read >> u >> v >> a >> b;
            add_edge(u, v, a, b);
            add_edge(v, u, b, a);
        }
        bool fail = false;
        for (int i = 1; i <= n && !fail; ++i)
            if (!vis[i]) {
                rate[i] = my_ratio();
                if (!check(i)) fail = true;
            }
        write << "Case #" << case_id << (fail ? ": No\n" : ": Yes\n");
    }
    return 0;
}