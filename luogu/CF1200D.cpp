/*************************************
 * @problem:      White Lines.
 * @author:       brealid.
 * @time:         2021-02-22.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 2000 + 7;

int n, k;
char mp[N][N];
int start_count;   // 不擦的时候就有多少'白线'
bool if_ln[N][N];  // 如果从 (i, j) 向右擦 k 个格子，能否多获得这一行
bool if_col[N][N]; // 如果从 (i, j) 向下擦 k 个格子，能否多获得这一列
int tot[N][N];     // 如果从 (i, j) 向下向右擦 k*k 个格子，能获得多少'白线'

void init_if_ln() {
    for (int i = 1; i <= n; ++i) {
        int total = 0;
        for (int j = 1; j <= n; ++j)
            if (mp[i][j] == 'B') ++total;
        if (!total) {
            ++start_count;
            continue;
        }
        for (int j = 1; j < k; ++j)
            if (mp[i][j] == 'B') --total;
        for (int j = 1; j <= n - k + 1; ++j) {
            if (mp[i][j - 1] == 'B') ++total;
            if (mp[i][j + k - 1] == 'B') --total;
            if (!total) if_ln[i][j] = true;
        }
    }
}

void init_if_col() {
    for (int j = 1; j <= n; ++j) {
        int total = 0;
        for (int i = 1; i <= n; ++i)
            if (mp[i][j] == 'B') ++total;
        if (!total) {
            ++start_count;
            continue;
        }
        for (int i = 1; i < k; ++i)
            if (mp[i][j] == 'B') --total;
        for (int i = 1; i <= n - k + 1; ++i) {
            if (mp[i - 1][j] == 'B') ++total;
            if (mp[i + k - 1][j] == 'B') --total;
            if (!total) if_col[i][j] = true;
        }
    }
}

void init_tot_ln() {
    for (int j = 1; j <= n; ++j) {
        int total = 0;
        for (int i = 1; i < k; ++i)
            if (if_ln[i][j]) ++total;
        for (int i = 1; i <= n - k + 1; ++i) {
            if (if_ln[i - 1][j]) --total;
            if (if_ln[i + k - 1][j]) ++total;
            tot[i][j] += total;
        }
    }
}

void init_tot_col() {
    for (int i = 1; i <= n; ++i) {
        int total = 0;
        for (int j = 1; j < k; ++j)
            if (if_col[i][j]) ++total;
        for (int j = 1; j <= n - k + 1; ++j) {
            if (if_col[i][j - 1]) --total;
            if (if_col[i][j + k - 1]) ++total;
            tot[i][j] += total;
        }
    }
}

signed main() {
    kin >> n >> k;
    for (int i = 1; i <= n; ++i) kin >> (mp[i] + 1);
    init_if_ln();   // 预处理 if_ln
    init_if_col();  // 预处理 if_col
    init_tot_ln();  // 利用 if_ln 预处理 tot
    init_tot_col(); // 利用 if_col 预处理 tot
    int newly_gain(0);
    for (int i = 1; i <= n - k + 1; ++i)
        for (int j = 1; j <= n - k + 1; ++j)
            newly_gain = max(newly_gain, tot[i][j]);
    kout << start_count + newly_gain << '\n';
    return 0;
}