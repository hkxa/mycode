/*************************************
 * @problem:      P2466 [SDOI2008]Sue的小球.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-01-16.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int L = 0, R = 1;
int n, x0;
int x[1007], y[1007], v[1007];
long long _sumv[1007], sumy;
#define sum(i, j) (_sumv[j] - _sumv[(i) - 1])
#define except(i, j) (_sumv[n] - sum(i, j))
#define abs(num) ((num) < 0 ? -(num) : (num))
double f[2][1007][1007];

int main()
{
    n = read<int>();
    x0 = read<int>();
    for (int i = 1; i <= n; i++) x[i] = read<int>();
    for (int i = 1; i <= n; i++) y[i] = read<int>();
    for (int i = 1; i <= n; i++) v[i] = read<int>();
    for (int i = 1; i <= n; i++) {
        for (int j = n; j > i; j--) {
            if (x[i] > x[j]) {
                swap(x[i], x[j]);
                swap(y[i], y[j]);
                swap(v[i], v[j]);
            }
        }
    }
    for (int i = 1; i <= n; i++) {
        sumy += y[i];
        _sumv[i] = _sumv[i - 1] + v[i];
    }
    for (int i = 1; i <= n; i++) {
        f[L][i][i] = f[R][i][i] = abs(x0 - x[i]) * _sumv[n];
    }
    for (int l = 1; l < n; l++) {
        for (int i = 1, j = l + 1; j <= n; i++, j++) {
            f[L][i][j] = min(f[L][i + 1][j] + (x[i + 1] - x[i]) * except(i + 1, j), 
                             f[R][i + 1][j] + (x[j] - x[i]) * except(i + 1, j));
            f[R][i][j] = min(f[L][i][j - 1] + (x[j] - x[i]) * except(i, j - 1), 
                             f[R][i][j - 1] + (x[j] - x[j - 1]) * except(i, j - 1));
        }
    }
    printf("%.3lf", (sumy - min(f[L][1][n], f[R][1][n])) / 1000);
    return 0;
}