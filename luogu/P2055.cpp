 /*************************************
 * problem:      P2055 [ZJOI2009]���ڵ�����.
 * user ID:      63720.
 * user name:    �����Ű�.
 * time:         2019-03-19.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 
/*************************************
 * record ID:    R17377584.
 * time:         30 ms
 * memory:       968 KB
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

int head[107], next[20007], to[20007], top = 0;
int link[107];
bool used[107];
bool schooler[107];
bool isback[107];
int n, m, e;

void add_edge(int u, int v)
{
    // printf("add_edge person-%d with bed-%d.\n", u, v);
    // if (u > n || v > m) return;
    top++;
    to[top] = n + v;
    next[top] = head[u];
    head[u] = top;
    top++;
    to[top] = u;
    next[top] = head[n + v];
    head[n + v] = top;
}

bool dfs(int u) 
{
    for (int i = head[u]; i != -1; i = next[i]) {
        int &v = to[i];
        if (!used[v]) {
            used[v] = true;
            if (link[v] == -1 || dfs(link[v])) {
                link[v] = u;
                link[u] = v;
                return true;
            }
        }
    }
    return false;
}

int hungarian() 
{
    int res = 0;
    for (int u = 1; u <= n; u++) {
        memset(used, 0, sizeof(used));
        if (schooler[u] && isback[u]) continue;
        if (dfs(u)) {
            res++;
            // printf("link person[%d] with bed[%d].\n", u, link[u] - n);
        } else {
            // printf("There is no bed for person[%d] to live.\n", u);
        }
    }
    return res;
}

void markjuruo_AKed_IOI()
{
    // init
    memset(next, 0, sizeof(next));
    memset(to, 0, sizeof(to));
    memset(schooler, 0, sizeof(schooler));
    memset(isback, 0, sizeof(isback));
    top = 0;
}

int markjuruo_AK_IOI()
{
    markjuruo_AKed_IOI();
    memset(link, -1, sizeof(link));
    memset(head, -1, sizeof(head));
    n = read<int>();
    // m = read<int>();
    // e = read<int>();
    int opt;
    int want_bed_person;
    want_bed_person = n;
    for (int i = 1; i <= n; i++) {
        schooler[i] = read<int>();
    }
    for (int i = 1; i <= n; i++) {
        isback[i] = read<int>();
    }
    for (int i = 1; i <= n; i++) {
        if (schooler[i] && !isback[i]) {
            add_edge(i, i);
        }  
        if (schooler[i] && isback[i]) {
            want_bed_person--;
        }
    }
    for (int i = 1; i <= n; i++) {
        // printf("%d.\n", i);
        if (schooler[i] && isback[i]) {
            for (int j = 1; j <= n; j++) {
                opt = read<int>();
            }
            continue;
        }
        for (int j = 1; j <= n; j++) {
            opt = read<int>();
            // printf("/%d^%d\\\n", opt, schooler[j]);
            if (opt && schooler[j]) {
                add_edge(i, j);
            }
        }
    }
    puts(hungarian() == want_bed_person ? "^_^" : "T_T");
    return 0;
}

int main()
{
    int T = read<int>();
    while (T--) {
        markjuruo_AK_IOI();
    }
    return 0;
}
