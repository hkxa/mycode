//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      古代猪文.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-20.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define int int64

inline int64 fpow(int64 a, int64 n, int64 M) {
    if (!a) return 0;
    int64 ret = 1;
    while (n) {
        if (n & 1) ret = ret * a % M;
        a = a * a % M;
        n >>= 1;
    }
    return ret;
}

inline void exgcd(int64 m, int64 n, int64 &x, int64 &y) {
    if (!n) {
        x = 1;
        y = 0;
    } else {
        exgcd(n, m % n, y, x);
        y -= x * (m / n);
    }
}

inline int64 inv(int64 a, int64 M) {
    static int64 x, y;
    exgcd(a, M, x, y);
    return (x % M + M) % M;
}

int a[13], m[13] = {0, 2, 3, 4679, 35617};

int64 CRT(const int n) {
    int64 M = 1, x = 0;
    for (int i = 1; i <= n; i++) M *= m[i];
    for (int i = 1; i <= n; i++) {
        x = (x + a[i] * (M / m[i]) % M * inv(M / m[i], m[i]) % M) % M;
    }
    return x;
}

int64 fac[35617 + 7], ifac[35617 + 7];

void PrepareFac(int M) {
    fac[0] = fac[1] = ifac[0] = 1;
    for (int i = 2; i < M; i++) fac[i] = fac[i - 1] * i % M;
    ifac[M - 1] = inv(fac[M - 1], M);
    for (int i = M - 1; i >= 2; i--) ifac[i - 1] = ifac[i] * i % M;
    // for (int i = 0; i < M; i++) printf("fac[%d] = %d; ifac[%d] = %d; (mod %d)\n", i, fac[i], ifac[i], M);
}

int64 C(int n, int m, int P) {
    if (m < n) return 0;
    // printf("C(%d, %d, %d) = %lld\n", n, m, P, fac[m] * ifac[m - n] % P * ifac[n] % P);
    return fac[m] * ifac[m - n] % P * ifac[n] % P;
}

int64 Lucas(int n, int m, int P) {
    if (!n) return 1;
    return Lucas(n / P, m / P, P) * C(n % P, m % P, P) % P;
}

signed main() {
    int n = read<int>();
    for (int i = 1; i <= 4; i++) {
        PrepareFac(m[i]);
        for (int s = sqrt(n); s >= 1; s--) {
            if (n % s == 0) {
                a[i] = (a[i] + Lucas(s, n, m[i])) % m[i];
                // printf("Lucas(%d, %d, %d) = %lld\n", s, n, m[i], Lucas(s, n, m[i]));
                if (s * s != n) a[i] = (a[i] + Lucas(n / s, n, m[i])) % m[i];
                // printf("Lucas(%d, %d, %d) = %lld\n", n / s, n, m[i], Lucas(n / s, n, m[i]));
            }
        }
        // printf("a[%d] = %d (mod %d)\n", i, a[i], m[i]);
    }
    write(fpow(read<int>() % 999911659, CRT(4), 999911659), 10);
    return 0;
}

// Create File Date : 2020-06-20