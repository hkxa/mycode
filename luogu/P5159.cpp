/*************************************
 * problem:      P5159 WD与矩阵.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-22.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define puts_return(x) { puts(x); return 0; }
#define write_return(x) { write(x); return 0; }

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int64 fexp(int64 a, int64 n, int64 P = 998244353, int64 baseTimeser = 1)
{
    while (n) {
        if (n & 1) baseTimeser = (baseTimeser * a) % P;
        a = (a * a) % P;
        n >>= 1;
    }
    return baseTimeser;
}

int main()
{
    int T = read<int>(), n, m;
    while (T--) {
        n = read<int>();
        m = read<int>();
        write(fexp(fexp(2, n - 1), m - 1), 10);
    }
    return 0;
}