/*************************************
 * @problem:      【模板】AC自动机（简单版）[SAM-version].
 * @user_name:    brealid/hkxadpall/zhaoyi20/j(o)moo/littleTortoise.
 * @time:         2020-09-01.
 * @language:     C++.
 * @fastio_ver:   20200827.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        // simple io flags
        ignore_int = 1 << 0,    // input
        char_enter = 1 << 1,    // output
        flush_stdout = 1 << 2,  // output
        flush_stderr = 1 << 3,  // output
        // combination io flags
        endline = char_enter | flush_stdout // output
    };
    enum number_type_flags {
        // simple io flags
        output_double_stable = 1 << 0,  // output
        output_double_faster = 1 << 1   // output
        // combination io flags
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f & ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore character set : ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (double &lf) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            lf = endch & 15;
            while (isdigit(endch = getchar())) lf = lf * 10 + (endch & 15);
            if (endch == '.') {
                double digit = 0.1;
                while (isdigit(endch = getchar())) {
                    lf += (endch & 15) * digit;
                    digit *= 0.1;
                }
            }
            if (flag) lf = -lf;
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            Int d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return d;
        }
    } read;

    struct Writer {
        Writer& operator << (io_flags f) {
            if (f & char_enter) putchar(10);
            if (f & flush_stdout) fflush(stdout);
            if (f & flush_stderr) fflush(stderr);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
        inline void operator () (double val, int eps_digit = 6, char endch = '\0', number_type_flags flg = output_double_faster) {
            if (flg & output_double_stable) {
                static char output_format[10];
                sprintf(output_format, "%%.%dlf", eps_digit);
                printf(output_format, val);
            } else if (flg & output_double_faster) {
                if (val < 0) {
                    putchar('-');
                    val = -val;
                }
                double eps_number = 0.5;
                for (int i = 1; i <= eps_digit; i++) eps_number /= 10;
                val += eps_number;
                (*this) << (int64)val;
                val -= (int64)val;
                putchar('.');
                while (eps_digit--) {
                    val *= 10;
                    putchar((int)val | '0');
                    val -= (int)val;
                }
            } else {
                (*this) << "1.#ERROR_NOT_IDENTIFIED_FLAG";
            }
            if (endch) putchar(endch);
        }
    } write;
}
using namespace Fastio;
namespace File_IO {
    void init_IO() {
        freopen("【模板】AC自动机（简单版）[SAM-version].in", "r", stdin);
        freopen("【模板】AC自动机（简单版）[SAM-version].out", "w", stdout);
    }
}

// #define int int64

namespace std_assist {
    template <typename map_type>
    typename map_type::mapped_type visit(const map_type &mp, typename map_type::key_type QueryVal, typename map_type::mapped_type FailedRet = -1) {
        typename map_type::const_iterator it = mp.find(QueryVal);
        if (it == mp.end()) return FailedRet;
        return it->second;
    }
    template <typename ret_type>
    ret_type visit(const ret_type *array, int QueryVal, ret_type FailedRet = -1) {
        if (!array[QueryVal]) return FailedRet;
        return array[QueryVal];
    }
}

namespace data_structure {
    struct SAM_node {
        int len, link;
        int nxt[26];
    };
    template<size_t SIZE>
    struct SAM {
        SAM_node node[SIZE];
        int cnt, last_expand_node;
        void init_automaton() {
            node[1].len = 0;
            node[1].link = -1;
            memset(node[1].nxt, 0, 104);
            cnt = last_expand_node = 1;
        }
        void expand_next_char(char ch) {
            ch -= 'a';
            int cur = ++cnt;
            node[cur].len = node[last_expand_node].len + 1;
            memset(node[cur].nxt, 0, 104);
            int p = last_expand_node;
            while (~p && !node[p].nxt[ch]) {
                node[p].nxt[ch] = cur;
                p = node[p].link;
            }
            if (!~p) {
                node[cur].link = 1;
            } else {
                int q = node[p].nxt[ch];
                if (node[q].len == node[p].len + 1) {
                    node[cur].link = q;
                } else {
                    int clone = ++cnt;
                    node[clone].len = node[p].len + 1;
                    node[clone].link = node[q].link;
                    memcpy(node[clone].nxt, node[q].nxt, 104);
                    while (~p && std_assist::visit(node[p].nxt, ch) == q) {
                        node[p].nxt[ch] = clone;
                        p = node[p].link;
                    }
                    node[q].link = node[cur].link = clone;
                }
            }
            last_expand_node = cur;
        }
        bool match(const char *text) {
            int u = 1;
            while (*text != '\0') {
                u = std_assist::visit(node[u].nxt, *text - 'a');
                if (!~u) return false;
                ++text;
            }
            return true;
        }
    };
}

const int N = 1e6 + 7;

int n;
string str[N], mode_str;
int ans = 0;

signed main() {
    // File_IO::init_IO();
    data_structure::SAM<2000007> sam;
    sam.init_automaton();
    ios::sync_with_stdio(false);
    cin.tie(0);
    cin >> n;
    for (int i = 1; i <= n; i++) cin >> str[i];
    cin >> mode_str;
    for (size_t pos = 0; pos < mode_str.size(); pos++)
        sam.expand_next_char(mode_str[pos]);
    for (int i = 1; i <= n; i++) 
        if (sam.match(str[i].c_str())) ++ans;
    write << ans << '\n';
    return 0;
}