/*************************************
 * problem:      P2585 [ZJOI2006]三色二叉树.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-06-01.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

void backspace()
{
    printf("\b \b");
}

#ifdef DEBUG
#include <windows.h>
#endif

struct Tree_OP {
    struct Tro_tree {
        struct record_DP {
            int R, G, B;
            // object's name : my color
            // object's value : max/min count(color-green)
        } maxx, minn;
        Tro_tree *lc, *rc;

        Tro_tree() 
        {
            maxx.R = maxx.G = maxx.B = 0;
            minn.R = minn.G = minn.B = 0;
            lc = rc = NULL;
        }

        #ifdef DEBUG
        void pushup(int depth)
        #else
        void pushup()
        #endif
        {
            if (lc && rc) {
                #ifdef DEBUG
                lc->pushup(depth + 1);
                rc->pushup(depth + 1);
                #else
                lc->pushup();
                rc->pushup();
                #endif
                maxx.R = max(lc->maxx.G + rc->maxx.B, lc->maxx.B + rc->maxx.G);
                maxx.G = max(lc->maxx.R + rc->maxx.B, lc->maxx.B + rc->maxx.R) + 1;
                maxx.B = max(lc->maxx.R + rc->maxx.G, lc->maxx.G + rc->maxx.R);
                minn.R = min(lc->minn.G + rc->minn.B, lc->minn.B + rc->minn.G);
                minn.G = min(lc->minn.R + rc->minn.B, lc->minn.B + rc->minn.R) + 1;
                minn.B = min(lc->minn.R + rc->minn.G, lc->minn.G + rc->minn.R);
            } else if (lc) {
                #ifdef DEBUG
                lc->pushup(depth + 1);
                #else
                lc->pushup();
                #endif
                maxx.R = max(lc->maxx.G, lc->maxx.B);
                maxx.G = max(lc->maxx.R, lc->maxx.B) + 1;
                maxx.B = max(lc->maxx.R, lc->maxx.G);
                minn.R = min(lc->minn.G, lc->minn.B);
                minn.G = min(lc->minn.R, lc->minn.B) + 1;
                minn.B = min(lc->minn.R, lc->minn.G);
            } else {
                maxx.R = 0;
                maxx.G = 1;
                maxx.B = 0;
                minn.R = 0;
                minn.G = 1;
                minn.B = 0;
            }
            #ifdef DEBUG
            while (depth--) {
                if (depth) {
                    putchar(' ');
                    putchar(' ');
                } else {
                    putchar('|');
                    putchar('-');
                }
            }
            printf("max{%d, %d, %d}, min{%d, %d, %d}.\n", maxx.R, maxx.G, maxx.B, minn.R, minn.G, minn.B);
            #endif
        }

        int getMax()
        {
            return max(max(maxx.R, maxx.G), maxx.B);
        }

        int getMin()
        {
            return min(min(minn.R, minn.G), minn.B);
        }
    } *root;

    int buildPos;
    string tro;

    void basic_dfsBuild(Tro_tree *fa)
    {
        // printf("[dfs %c]", tro[buildPos]);
        // Sleep(800);
        switch (tro[buildPos]) {
            case '2' :
                buildPos++;
                fa->lc = new Tro_tree();
                basic_dfsBuild(fa->lc);
                buildPos++;
                fa->rc = new Tro_tree();
                basic_dfsBuild(fa->rc);
                break;
            case '1' :
                buildPos++;
                fa->lc = new Tro_tree();
                basic_dfsBuild(fa->lc);
                break;
            case '0' :
                break;
            default :
                printf("ERR!");
                exit(EXIT_FAILURE);
                break;
        }
        // backspace();
        // backspace();
        // backspace();
        // backspace();
        // backspace();
        // backspace();
        // backspace();
        // Sleep(800);
    }

    void readIn()
    {
        cin >> tro;
    }

    void build()
    {
        buildPos = 0;
        root = new Tro_tree();
        basic_dfsBuild(root);
        #ifdef DEBUG
        putchar(10);
        #endif
    }
    
    void solve()
    {
        #ifdef DEBUG
        root->pushup(0);
        #else
        root->pushup();
        #endif
        write(root->getMax(), 32);
        write(root->getMin(), 10);
    }
} td;

int main()
{
    td.readIn();
    td.build();
    td.solve();
    return EXIT_SUCCESS;
}