//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      T123926 重拳出击.
 * @user_id:      63720.
 * @user_name:    brealid.
 * @time:         2020-05-30.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
       return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
       return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// #define int int64

const int N = 400007;

int n, m;
int k, x;
vector<int> G[N];
bool yy[N];
int dep[N], maxDep[N], yyDep[N];

void dfs(int u, int fa) {
    maxDep[u] = dep[u] = dep[fa] + 1;
    if (yy[u]) yyDep[u] = 1;
    for (unsigned i = 0; i < G[u].size(); i++) {
        int v = G[u][i];
        if (v != fa) {
            dfs(v, u);
            maxDep[u] = max(maxDep[u], maxDep[v]);
            if (yyDep[v]) yyDep[u] = max(yyDep[u], yyDep[v] + 1);
        }
    }
}

int solve(int u, int fa, int already = 0) {
    // printf("solve(%d, %d, %d)\n", u, fa, already);
    int mx = 0, cnt = 0;
    for (unsigned i = 0; i < G[u].size(); i++) {
        int v = G[u][i];
        if (v != fa) {
            if (yyDep[v] > yyDep[mx]) {
                mx = v;
                cnt = 1;
            } else if (yyDep[v] == yyDep[mx]) {
                cnt++;
            }
        }
    }
    if (yyDep[mx] <= already) return 0;
    if (cnt != 1) return yyDep[mx] - already;
    int mx2 = 0;
    for (unsigned i = 0; i < G[u].size(); i++) {
        int v = G[u][i];
        if (v != fa && v != mx) {
            if (yyDep[v] > mx2) {
                mx2 = yyDep[v];
            }
        }
    }
    if (mx2) return max(mx2 - already, 0) + solve(mx, u, already + max(mx2 - already, 0) + 1) + 1;
    else return solve(mx, u, already + 1) + 1;
}

signed main() {
    n = read<int>();
    for (int i = 1, u, v; i < n; i++) {
        u = read<int>();
        v = read<int>();
        G[u].push_back(v);
        G[v].push_back(u);
    }
    m = read<int>();
    for (int i = 1; i <= m; i++)
        yy[read<int>()] = true;
    k = read<int>();
    x = read<int>();
    dfs(x, 0);
    write(solve(x, 0, k) + 1, 10);
    return 0;
}

// T123926?contestId=29966
// T123926?contestId=29965

// Create File Date : 2020-05-30

/*
5
1 2
1 3
2 4
2 5
5
1 2 3 4 5
1 5
*/