/*************************************
 * problem:      id_name.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-mm-dd.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define ForInVec(vectorType, vectorName, iteratorName) for (vector<vectorType>::iterator iteratorName = vectorName.begin(); iteratorName != vectorName.end(); iteratorName++)
#define ForInVI(vectorName, iteratorName) ForInVec(int, vectorName, iteratorName)
#define ForInVE(vectorName, iteratorName) ForInVec(Edge, vectorName, iteratorName)
#define MemWithNum(array, num) memset(array, num, sizeof(array))
#define Clear(array) MemWithNum(array, 0)
#define MemBint(array) MemWithNum(array, 0x3f)
#define MemInf(array) MemWithNum(array, 0x7f)
#define MemEof(array) MemWithNum(array, -1)
#define ensuref(condition) do { if (!(condition)) exit(0); } while(0)

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

struct FastIOer {
#   define createReadlnInt(type)            \
    FastIOer& operator >> (type &x)         \
    {                                       \
        x = read<type>();                   \
        return *this;                       \
    }
    createReadlnInt(short);
    createReadlnInt(unsigned short);
    createReadlnInt(int);
    createReadlnInt(unsigned);
    createReadlnInt(long long);
    createReadlnInt(unsigned long long);
#   undef createReadlnInt

#   define createReadlnReal(type)           \
    FastIOer& operator >> (type &x)         \
    {                                       \
        char c;                             \
        x = read<long long>(c);             \
        if (c != '.') return *this;         \
        type r = (x >= 0 ? 0.1 : -0.1);     \
        while (isdigit(c = getchar())) {    \
            x += r * (c & 15);              \
            r *= 0.1;                       \
        }                                   \
        return *this;                       \
    }
    createReadlnReal(double);
    createReadlnReal(float);
    createReadlnReal(long double);

#   define createWritelnInt(type)           \
    FastIOer& operator << (type &x)         \
    {                                       \
        write<type>(x);                     \
        return *this;                       \
    }
    createWritelnInt(short);
    createWritelnInt(unsigned short);
    createWritelnInt(int);
    createWritelnInt(unsigned);
    createWritelnInt(long long);
    createWritelnInt(unsigned long long);
#   undef createWritelnInt
    
    FastIOer& operator >> (char &x)
    {
        x = getchar();
        return *this;
    }
    
    FastIOer& operator << (char x)
    {
        putchar(x);
        return *this;
    }
    
    FastIOer& operator << (const char *x)
    {
        int __pos = 0;
        while (x[__pos]) {
            putchar(x[__pos++]);
        }
        return *this;
    }
} fast;

const int p10[] = {1, 10, 100, 1000, 10000, 100000, 1000000, 10000000};
bool inq[100007] = { false };
int dis[100007];
int n, t;
int numMx;

struct num {
    int v, l;
    num() {}
    num(int Val, int Len) : v(Val), l(Len) {}
    num(int Val) : v(Val) {
        l = 0;
        while (Val) {
            Val /= 10;
            l++;
        }
    }
    operator int () { return v; }
} s;
int bt[8];

int main()
{
    memset(dis, 0x3f, sizeof(dis));
    s = num(read<int>());
    dis[s] = 0;
    queue<int> q;
    inq[s] = 1;
    q.push(s);
    while (!q.empty()) {
        num f = q.front();
        q.pop();
        for (int i = 0, tmp = f; i < f.l; i++) {
            bt[i] = tmp % 10;
            tmp /= 10;
        }
        // printf("%d generate | ", f.v);
        for (int i = 0; i < f.l; i++) {
            for (int j = i + 1; j < f.l; j++) {
                t = f - (bt[j] - bt[i]) * (p10[j] - p10[i]);
                // printf("%d ", t);
                if (dis[t] > dis[f] + 1) {
                    dis[t] = dis[f] + 1;
                    if (!inq[t]) {
                        q.push(num(t, f.l));
                        inq[t] = true;
                    }
                }
            }
        }
        // printf("| ");
        for (int i = 0; i < f.l; i++) {
            t = 0;
            for (int j = f.l - 1; j >= 0; j--) {
                if (i != j) t = (t * 10) + bt[j];
            }
            // printf("%d ", t);
            if (dis[t] > dis[f] + 1) {
                dis[t] = dis[f] + 1;
                if (!inq[t]) {
                    q.push(num(t, f.l - 1));
                    inq[t] = true;
                }
            }
        }
        // printf("|%c", " \n"[f.l == s.l]);
        if (f.l == s.l) continue;
        for (int i = 1; i < f.l; i++) {
            for (int add = bt[i - 1] - 1; add > bt[i]; add--) {
                t = 0;
                for (int j = f.l - 1; j >= 0; j--) {
                    t = (t * 10) + bt[j];
                    if (i == j) t = (t * 10) + add;
                }
                // printf("%d ", t);
                if (dis[t] > dis[f] + 1) {
                    dis[t] = dis[f] + 1;
                    if (!inq[t]) {
                        q.push(num(t, f.l + 1));
                        inq[t] = true;
                    }
                }
            }
        }
        // printf("|\n");
    }
    n = read<int>();
    while (n--) {
        t = read<int>();
        write(dis[t] != 0x3f3f3f3f ? dis[t] : -1, 10);
    }
    return 0;
}