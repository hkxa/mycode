T = int(input())

while T:
    T -= 1
    x, y, k = map(int, input().split())
    if x > y:
        print(x)
    else:
        if y - x <= k:
            print(y)
        else:
            print(y + y - x - k)