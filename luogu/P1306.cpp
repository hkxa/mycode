#include <bits/stdc++.h>
#define P 100000000

struct FibMatrix {
    long long a[2][2], res[2][2];
    void Fib_init() {
        res[0][0] = res[1][1] = a[0][0] = a[0][1] = a[1][0] = 1;
        res[1][0] = res[0][1] = a[1][1] = 0;
    }
    FibMatrix() { Fib_init(); }
    void Mul_e(long long ar[2][2]) {
        long long original_ar[2][2] = {{ar[0][0], ar[0][1]}, {ar[1][0], ar[1][1]}};
        long long original_a[2][2] = {{a[0][0], a[0][1]}, {a[1][0], a[1][1]}};
        // printf("la\n%lld %lld\n%lld %lld\n", original_ar[0][0], original_ar[0][1], original_ar[1][0], original_ar[1][1]);
        ar[0][0] = (original_ar[0][0] * original_a[0][0] + original_ar[0][1] * original_a[1][0]) % P;
        ar[0][1] = (original_ar[0][0] * original_a[0][1] + original_ar[0][1] * original_a[1][1]) % P;
        ar[1][0] = (original_ar[1][0] * original_a[0][0] + original_ar[1][1] * original_a[1][0]) % P;
        ar[1][1] = (original_ar[1][0] * original_a[0][1] + original_ar[1][1] * original_a[1][1]) % P;
        // printf("lb\n%lld %lld\n%lld %lld\n", original_ar[0][0], original_ar[0][1], original_ar[1][0], original_ar[1][1]);
    }
    int fastPow(int k) {
        while (k) {
            if (k & 1) Mul_e(res);
            Mul_e(a);
            k >>= 1;
        }
        // printf("%lld %lld\n%lld %lld\n", res[0][0], res[0][1], res[1][0], res[1][1]);
        return res[0][1];
    }
} fib;

int n, m;

int main()
{
    scanf("%d%d", &n, &m);
    printf("%d", fib.fastPow(std::__gcd(n, m)));
    return 0;
}