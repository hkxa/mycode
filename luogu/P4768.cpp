//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      [NOI2018]归程.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-17.
 * @language:     C++.
*************************************/ 

#pragma GCC optimize(2)
#pragma GCC optimize(3)
#pragma GCC optimize("Ofast")
#pragma GCC optimize("inline")
#pragma GCC optimize("-fgcse")
#pragma GCC optimize("-fgcse-lm")
#pragma GCC optimize("inline-functions")
#pragma GCC optimize("no-stack-protector")
#pragma GCC optimize("inline-small-functions")
#pragma GCC optimize("-finline-small-functions")
#pragma GCC optimize("inline-functions-called-once")
#pragma GCC optimize("-fexpensive-optimizations")
#pragma GCC optimize("-fdelete-null-pointer-checks")

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

#ifdef DEBUG
# define passing() cerr << "passing line [" << __LINE__ << "] in No." << clock() << "ms" << endl
# define debug(...) fprintf(stderr, __VA_ARGS__)
# define show(x) cerr << #x << " = " << (x) << endl
#else
# define passing() do if (0) cerr << "passing line [" << __LINE__ << "] in No." << clock() << "ms" << endl; while(0)
# define debug(...) do if (0) fprintf(stderr, __VA_ARGS__); while(0)
# define show(x) do if (0) cerr << #x << " = " << (x) << endl; while(0)
#endif

// #define int int64

const int N = 2e5 + 7, SGT_SIZE = N * 80;

int n, m;

struct basicGraph {
    struct Edge { int to, val; };
    vector<Edge> G[N];
    int dest_dis[N];
    bool in_queue[N];
    struct DijNode {
        int u, dis;
        bool operator < (const DijNode &b) const {
            return dis > b.dis;
        }
    };

    void clear() {
        vector<Edge> *clear_pos = &G[1]; 
        while (!clear_pos->empty()) (clear_pos++)->clear();
    }
    void push_edge(int u, int v, int l) {
        G[u].push_back((Edge){v, l});
        G[v].push_back((Edge){u, l});
    }
    void dijkstra() {
        priority_queue<DijNode> q;
        q.push((DijNode){1, 0});
        in_queue[1] = 1;
        memset(dest_dis, 0x3f, sizeof(dest_dis));
        dest_dis[1] = 0;
        int u;
        while (!q.empty()) {
            u = q.top().u; q.pop();
            in_queue[u] = 0;
            for (size_t i = 0; i < G[u].size(); i++) {
                int v = G[u][i].to, w = G[u][i].val;
                if (dest_dis[v] > dest_dis[u] + w) {
                    dest_dis[v] = dest_dis[u] + w;
                    if (!in_queue[v]) {
                        q.push((DijNode){v, dest_dis[v]});
                        in_queue[v] = 1;
                    }
                }
            }
        }
    }
} Graph;

struct Durable_SGT_UFS{
    struct SGT_node {
        unsigned fa : 19;
        unsigned siz : 19;
        unsigned l : 25;
        unsigned r : 25;
        int dis;
    } t[SGT_SIZE];
    int cnt;
    int root[N << 1];
    void build(int u, int l, int r) {
        if (l == r) {
            t[u].fa = l;
            t[u].siz = 1;
            t[u].dis = Graph.dest_dis[l];
            return;
        }
        int mid = (l + r) >> 1;
        build(t[u].l = ++cnt, l, mid);
        build(t[u].r = ++cnt, mid + 1, r);
    }

    int query(int u, int l, int r, int pos) {
        if (l == r) return u;
        int mid = (l + r) >> 1;
        if (pos <= mid) return query(t[u].l, l, mid, pos);
        else return query(t[u].r, mid + 1, r, pos);
    }

    void modify_SizDis(int lst, int u, int l, int r, int pos, int Siz, int Dis) {
        if (l == r) {
            t[u].fa = l;
            t[u].siz = Siz;
            t[u].dis = Dis;
            return;
        }
        int mid = (l + r) >> 1;
        if (pos <= mid) {
            t[u].r = t[lst].r;
            int lst_l = t[lst].l;
            modify_SizDis(lst_l, t[u].l = ++cnt, l, mid, pos, Siz, Dis);
        } else {
            t[u].l = t[lst].l;
            int lst_r = t[lst].r;
            modify_SizDis(lst_r, t[u].r = ++cnt, mid + 1, r, pos, Siz, Dis);
        }
    }

    void modify_fa(int lst, int u, int l, int r, int pos, int new_val) {
        if (l == r) {
            t[u].fa = new_val;
            return;
        }
        int mid = (l + r) >> 1;
        if (pos <= mid) {
            t[u].r = t[lst].r;
            int lst_l = t[lst].l;
            modify_fa(lst_l, t[u].l = ++cnt, l, mid, pos, new_val);
        } else {
            t[u].l = t[lst].l;
            int lst_r = t[lst].r;
            modify_fa(lst_r, t[u].r = ++cnt, mid + 1, r, pos, new_val);
        }
    }

    void roll_back(int lst, int now) {
        root[now] = ++cnt;
        t[cnt].l = t[root[lst]].l;
        t[cnt].r = t[root[lst]].r;
    }

    int query_fa(int ed, int u) {
        int faId = query(root[ed], 1, n, u);
        return u == t[faId].fa ? faId : query_fa(ed, t[faId].fa);
    }

    void unify(int ed, int u, int v) {
        u = query_fa(ed, u);
        v = query_fa(ed, v);
        if (t[u].fa == t[v].fa) return;
        if (t[u].siz < t[v].siz) swap(u, v);
        modify_SizDis(root[ed], root[ed], 1, n, t[u].fa, t[u].siz + t[v].siz, min(t[u].dis, t[v].dis));
        modify_fa(root[ed], root[ed], 1, n, t[v].fa, t[u].fa);
    }

    int query_family(int ed, int u, int v) {
        u = query_fa(ed, u);
        v = query_fa(ed, v);
        return t[u].fa == t[v].fa;
    }
} DurableUFS;

struct GraphEdgeVal_Manager {
    struct E {
        int a, b, height;
        bool operator < (const E &b) const {
            return height > b.height;
        }
        bool operator == (const E &b) const {
            return height == b.height;
        }
    } EdgeValues[N << 1];
    int cntM;
    void clear() {
        cntM = 0;
    }
    void push_edge(int u, int v, int height) {
        EdgeValues[++cntM] = (E){u, v, height};
    }
    void discretization() {
        EdgeValues[0].height = 1.5e9;
        sort(EdgeValues + 1, EdgeValues + cntM + 1);
    }
    void generate_DurableUFS() {
        DurableUFS.build(DurableUFS.root[0] = DurableUFS.cnt = 1, 1, n);
        int edition = 0;
        for (int i = 1, j = 1; i <= cntM; i = j + 1) {
            while (j <= cntM && EdgeValues[j + 1].height == EdgeValues[i].height) j++;
            edition++;
            DurableUFS.roll_back(edition - 1, edition);
            // for (int k = i; k <= j; k++) printf("UFS version %d : unify %d & %d\n", edition, EdgeValues[k].a, EdgeValues[k].b);
            for (int k = i; k <= j; k++) DurableUFS.unify(edition, EdgeValues[k].a, EdgeValues[k].b);
        }
        cntM = unique(EdgeValues + 1, EdgeValues + cntM + 1) - EdgeValues - 1;
        // printf("Array EdgeValues :\n");
        // for (int i = 1; i <= cntM; i++)
        //     printf("%d%c", EdgeValues[i].height, " \n"[i == cntM]);
    }
    int query_edition(int water_height) {
        int l = 0, r = cntM, mid, ret;
        while (l <= r) {
            mid = (l + r) >> 1;
            if (EdgeValues[mid].height > water_height) l = mid + 1, ret = mid;
            else r = mid - 1;
        }
        return ret;
    }
} ufs_do;

struct Queries {
    bool online;
    struct online_decode {
        int last_ans, S;
        void init(int toSetS) { last_ans = 0; S = toSetS; }
        void recordAns(int toSetAns) { last_ans = toSetAns; }
        void operator () (int &v, int &p) {
            v = (v + last_ans - 1) % n + 1;
            p = (p + last_ans) % (S + 1);
        }
    } decode;
    
    int deal(int st, int P) {
        if (online) decode(st, P);
        int version = ufs_do.query_edition(P);
        int Id = DurableUFS.query_fa(version, st);
        int nRet = DurableUFS.t[Id].dis;
        // printf("Querying (%d, %d) : \n    UFS version = %d\n    Ancient = %d\n    Answer = ", st, P, version, DurableUFS.t[Id].fa);
        if (online) decode.recordAns(nRet);
        return nRet;
    }
} ask;


signed main() {
    // freopen("return.in", "r", stdin);
    // freopen("return.out", "w", stdout);
    int T; 
    read >> T;
    while (T--) {
        read >> n >> m;
        Graph.clear();
        ufs_do.clear();
        for (int i = 1, u, v, l, a; i <= m; i++) {
            read >> u >> v >> l >> a;
            Graph.push_edge(u, v, l);
            ufs_do.push_edge(u, v, a);
        }
        passing();
        Graph.dijkstra();
        passing();
        ufs_do.discretization();
        passing();
        ufs_do.generate_DurableUFS();
        passing();
        // for (int i = 0; i <= ufs_do.cntM; i++) {
        //     printf("UFS version %d - fathers[] = { ", i);
        //     for (int j = 1; j <= n; j++)
        //         printf("%d ", DurableUFS.t[DurableUFS.query_fa(i, j)].fa);
        //     printf("}\n");
        // }
        int Q, S, st, P;
        read >> Q >> ask.online >> S;
        ask.decode.init(S);
        while (Q--) {
            read >> st >> P;
            write << ask.deal(st, P) << '\n';
        }
        passing();
    }
    return 0;
}