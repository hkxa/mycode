/*************************************
 * @problem:      [WC2020]猜数游戏.
 * @author:       brealid.
 * @time:         2021-01-31.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 5e3 + 7, P = 998244353;

int n, p, phi;
int min_factor, factor[N], cnt;

namespace math {
    // Quick Power a^n in the meaning of mod p
    inline int64 qpow(int64 a, int n, int64 p) {
        int64 res = 1;
        for (; n; n >>= 1) {
            if (n & 1) res = res * a % p;
            a = a * a % p;
        }
        return res;
    }
    inline void get_factor(int n) {
        min_factor = 0;
        for (int i = 2; i * i <= n; ++i)
            if (n % i == 0) {
                min_factor = i;
                break;
            }
        if (!min_factor) min_factor = n;
        int now = phi = n - n / min_factor;
        for (int i = 2; i * i <= now; ++i)
            if (now % i == 0) {
                factor[++cnt] = i;
                while (now % i == 0) now /= i;
            }
        if (now != 1) factor[++cnt] = now;
    }
    inline int ord(int u, int p) {
        int res = phi;
        for (int i = 1; i <= cnt; ++i)
            while (res % factor[i] == 0 && qpow(u, res / factor[i], p) == 1)
                res /= factor[i];
        return res;
    }
    int power2[N];
    inline void power2_init(int n, int mod_P) {
        power2[0] = 1;
        for (int i = 1; i <= n; ++i)
            if ((power2[i] = power2[i - 1] << 1) >= mod_P) power2[i] -= mod_P;
    }
}

struct correlation_table {
    vector<int> v, cnt;
    map<int, int> rev;
    void insert(int number) {
        if (rev.count(number)) {
            ++cnt[rev[number]];
        } else {
            v.push_back(number);
            cnt.push_back(1);
            rev[number] = v.size() - 1;
        }
    }
} c1, c2;

int solve1() {
    // printf("hi 1 ");
    int *v = c1.v.data(), *c = c1.cnt.data(), siz = c1.v.size();
    int64 ans = 0;
    for (int i = 0; i < siz; ++i) {
        // printf("[hi ");
        int s = n;
        for (int j = 0; j < siz; ++j)
            if (v[j] % v[i] == 0) s -= c[j];
        // printf("%d] ", s);
        ans += int64(math::power2[c[i]] - 1) * math::power2[s] % P;
    }
    // printf("%d\n", ans % P);
    return ans % P;
}

int solve2() {
    // printf("hi 2 ");
    int c[N], siz = c2.v.size();
    int *v = c2.v.data();
    map<int, int> rev = c2.rev;
    int64 ans = 0;
    for (int i = 0; i < siz; i++) c[i] = n;
    for (int i = 0; i < siz; i++)
        for (int64 x = v[i]; x; x = x * v[i] % p)
            if (rev.count(x))
                --c[rev[x]];
    for (int i = 0; i < siz; i++)
        ans += math::power2[c[i]];
    // printf("%d\n", ans % P);
    return ans % P;
}

signed main() {
    kin >> n >> p;
    math::get_factor(p);
    math::power2_init(n, P);
    for (int i = 1, x; i <= n; ++i) {
        kin >> x;
        if (x % min_factor == 0) c2.insert(x);
        else c1.insert(math::ord(x, p));
    }
    kout << (solve1() + solve2()) % P << '\n';
    return 0;
}