/*************************************
 * problem:      P1186 玛丽卡.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-04-17.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 
/*************************************
 * record ID:    R18307386.
 * time:         1012 ms
 * memory:       6580 KB
*************************************/ 


#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

int n, m;

struct Edge {
    int b, v;
    int nxt;
    Edge() {}
    Edge(int B, int V, int Nxt) : b(B), v(V), nxt(Nxt) {}
} edges[500007];
int head[1007] = {0}, top = 0;

void addEdge(int a, int b, int v)
{
    edges[++top] = Edge(b, v, head[a]);
    head[a] = top;
}

void link(int a, int b, int v)
{
    addEdge(a, b, v);
    addEdge(b, a, v);
}

struct BasicEdgeIterator {
    int id;
    int from, len, to;
    bool error;

    BasicEdgeIterator() : error(false) {}
    
    BasicEdgeIterator operator ++ (int) 
    {
        id = edges[id].nxt;
        if (id != 0) {
            to = edges[id].b;
            len = edges[id].v;
        } else {
            error = true;
        }
        return *this;
    }

    bool isStop()
    {
        return error;
    }

    BasicEdgeIterator operator = (int startNode) 
    {
        id = head[startNode];
        from = startNode;
        if (id != 0) {
            to = edges[id].b;
            len = edges[id].v;
            error = false;
        } else {
            error = true;
        }
        return *this;
    }
};

typedef BasicEdgeIterator edgeIt;

int pre[1007] = {0};
int dis[1007];
bool inqueue[1007];
pair <int, int> cut;
int ans = 0;

void spfaFirst()
{
    memset(dis, 0x3f, sizeof(dis));
    memset(inqueue, 0, sizeof(inqueue));
    dis[1] = 0;
    queue<int> q;
    q.push(1);
    edgeIt e;
    inqueue[1] = true;
    while (!q.empty()) {
        e = q.front();
        q.pop();
        inqueue[e.from] = false;
        for (; !e.isStop(); e++) {
            if (dis[e.to] > dis[e.from] + e.len) {
                dis[e.to] = dis[e.from] + e.len;
                pre[e.to] = e.from;
                // printf("update : pre[%d] = %d.\n", e.to, e.from);
                if (!inqueue[e.to]) {
                    q.push(e.to);
                    inqueue[e.to] = true;
                }
            }
        }
    }
    // printf("spfaFirst : get %d.\n", dis[n]);
    ans = dis[n];
}

void spfaSecond()
{
    memset(dis, 0x3f, sizeof(dis));
    memset(inqueue, 0, sizeof(inqueue));
    dis[1] = 0;
    queue<int> q;
    q.push(1);
    edgeIt e;
    inqueue[1] = true;
    while (!q.empty()) {
        e = q.front();
        q.pop();
        inqueue[e.from] = false;
        for (; !e.isStop(); e++) {
            if (e.from == cut.first && e.to == cut.second) continue;
            if (e.to == cut.first && e.from == cut.second) continue;
            if (dis[e.to] > dis[e.from] + e.len) {
                dis[e.to] = dis[e.from] + e.len;
                if (!inqueue[e.to]) {
                    q.push(e.to);
                    inqueue[e.to] = true;
                }
            }
        }
    }
    // printf("spfaSecond : get %d.\n", dis[n]);
    if (dis[n] > ans) ans = dis[n];
}

int main()
{
    n = read<int>();
    m = read<int>();
    int a, b, v;
    for (int i = 1; i <= m; i++) {
        a = read<int>();
        b = read<int>();
        v = read<int>();
        link(a, b, v);
    }
    spfaFirst();
    for (int i = n; pre[i] != 0; i = pre[i]) {
        cut.first = i;
        cut.second = pre[i];
        // printf("cut %d with %d.\n", i, pre[i]);
        spfaSecond();
    }
    write(ans);
    return EXIT_SUCCESS;
}