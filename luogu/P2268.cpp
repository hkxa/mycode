/*************************************
 * @problem:      P2268 [HNOI2002]DNA分子的最佳比对.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2019-12-18.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int la, lb;
char a[1007], b[1007];
int f[1007][1007];

int main()
{
    scanf("%s%s", a + 1, b + 1);
    la = strlen(a + 1);
    lb = strlen(b + 1);
    for (int i = 1; i <= la; i++) a[i] = tolower(a[i]);
    for (int i = 1; i <= lb; i++) b[i] = tolower(b[i]);
    memset(f, 0xcf, sizeof(f));
    f[0][0] = 0;
    for (int i = 1; i <= la; i++) f[i][0] = -2 * i;
    for (int i = 1; i <= lb; i++) f[0][i] = -2 * i;
    for (int i = 1; i <= la; i++) {
        for (int j = 1; j <= lb; j++) {
            if (a[i] == b[j]) f[i][j] = max(f[i][j], f[i - 1][j - 1] + 1);
            else f[i][j] = max(f[i][j], f[i - 1][j - 1]);
            f[i][j] = max(f[i][j], f[i - 1][j] - 2);
            f[i][j] = max(f[i][j], f[i][j - 1] - 2);
        }
    }
    write(f[la][lb]);
    return 0;
}