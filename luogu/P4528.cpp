/*************************************
 * @problem:      totem
 * @author:       brealid.
 * @time:         2020-11-25.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#define getchar() (i_P1 == i_P2 && (i_P2 = (i_P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), i_P1 == i_P2) ? EOF : *i_P1++)
char BUF[1 << 21], *i_P1 = BUF, *i_P2 = BUF;
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

const int N = 2e5 + 7;

int n, h[N];

struct BinaryIndexTree {
    uint32 tr[N];
    BinaryIndexTree() {
        memset(tr, 0, sizeof(tr));
    }
    void clear() {
        memset(tr, 0, sizeof(tr));
    }
    void add(int u, uint32 dif) {
        for (; u <= n; u += u & -u) tr[u] += dif;
    }
    uint32 qry(int u) {
        uint32 ret = 0;
        for (; u; u ^= u & -u) ret += tr[u];
        return ret;
    }
};

struct segt {
    struct node {
        uint32 covered, add, sum;
    } tr[N << 2];
    segt() {
        memset(tr, 0, sizeof(tr));
    }
    void cover(int u, int l, int r, int pos, uint32 dif) {
        tr[u].covered += dif;
        if (l == r) return;
        int mid = (l + r) >> 1, ls = u << 1, rs = ls ^ 1;
        if (tr[u].add) {
            tr[ls].sum += tr[ls].covered * tr[u].add;
            tr[ls].add += tr[u].add;
            tr[rs].sum += tr[rs].covered * tr[u].add;
            tr[rs].add += tr[u].add;
            tr[u].add = 0;
        }
        if (pos <= mid) cover(ls, l, mid, pos, dif);
        else cover(rs, mid + 1, r, pos, dif);
    }
    void add(int u, int l, int r, int mr, uint32 dif) {
        if (r <= mr) {
            tr[u].sum += tr[u].covered * dif;
            tr[u].add += dif;
            return;
        }
        int mid = (l + r) >> 1, ls = u << 1, rs = ls ^ 1;
        if (tr[u].add) {
            tr[ls].sum += tr[ls].covered * tr[u].add;
            tr[ls].add += tr[u].add;
            tr[rs].sum += tr[rs].covered * tr[u].add;
            tr[rs].add += tr[u].add;
            tr[u].add = 0;
        }
        add(ls, l, mid, mr, dif);
        if (mid < mr) add(rs, mid + 1, r, mr, dif);
        tr[u].sum = tr[ls].sum + tr[rs].sum;
    }
    uint32 query(int u, int l, int r, int ml) {
        if (l >= ml) return tr[u].sum;
        int mid = (l + r) >> 1, ls = u << 1, rs = ls ^ 1;
        if (tr[u].add) {
            tr[ls].sum += tr[ls].covered * tr[u].add;
            tr[ls].add += tr[u].add;
            tr[rs].sum += tr[rs].covered * tr[u].add;
            tr[rs].add += tr[u].add;
            tr[u].add = 0;
        }
        uint32 res = 0;
        if (mid >= ml) res += query(ls, l, mid, ml);
        res += query(rs, mid + 1, r, ml);
        return res;
    }
};

uint32 after_greater[N], after_less[N], before_greater[N], before_less[N];
uint32 before_greater2[N];

void init() {
    BinaryIndexTree tr;
    for (int i = n; i >= 1; --i) {
        after_greater[i] = n - i - tr.qry(h[i]);
        tr.add(h[i], 1);
    }
    tr.clear();
    for (int i = 1; i <= n; ++i) {
        before_less[i] = tr.qry(h[i]);
        tr.add(h[i], 1);
    }
}

uint32 solve_1x2x() {
    segt s;
    uint32 ret = 0;
    for (int i = n; i >= 1; --i) {
        ret += s.query(1, 1, n, h[i]);
        s.add(1, 1, n, h[i], 1);
        s.cover(1, 1, n, h[i], after_greater[i]);
    }
    return ret;
}

uint32 solve_13xx() {
    segt s;
    uint32 ret = 0;
    for (int i = n; i >= 1; --i) {
        ret += s.query(1, 1, n, h[i]);
        s.add(1, 1, n, h[i], after_greater[i]);
        s.cover(1, 1, n, h[i], 1);
    }
    return ret;
}

uint32 solve_1234() {
    BinaryIndexTree tr;
    uint32 ret = 0;
    for (int i = 1; i <= n; ++i) {
        ret += tr.qry(h[i]) * after_greater[i];
        tr.add(h[i], before_less[i]);
    }
    return ret;
}

uint32 solve_1xxx() {
    uint32 ret = 0;
    for (int i = 1; i <= n; ++i) {
        uint32 x = after_greater[i];
        ret += (uint64)x * (x - 1) * (x - 2) / 6;
    }
    return ret;
}

signed main() {
    kin >> n;
    for (int i = 1; i <= n; ++i) kin >> h[i];
    init();
    uint32 ans = solve_1x2x() + solve_13xx() + solve_1234() - solve_1xxx();
    kout << (ans & 16777215) << '\n';
    return 0;
}