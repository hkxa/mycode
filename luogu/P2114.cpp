/*************************************
 * problem:      P2114 [NOI2014]起床困难综合症.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-03-06.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 
/*************************************
 * record ID:    R16951694.
 * time:         62 ms
 * memory:       816 KB
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

int bl_full = 0x7fffffff, bl_none = 0; // 真值表 
int n, m; // 如题意 
int ans = 0; // 答案记录 
int t; // 读入用临时变量 
char door_type; // 读入用记录门的类型 
    
char read_type()
{
	char char_tmp = getchar();
	while (!isalpha(char_tmp)) char_tmp = getchar();
	// 只要不是字母就一直读入
	return char_tmp;
	// 是字母了，返回结果 
}
    
int main()
{
    n = read<int>();
    m = read<int>();
    while (n--) {
        door_type = read_type();
        // read_type() 只读入第一个字母, 其余的字母由 read<int>() 自动忽略. 
        t = read<int>();
        if (door_type == 'A') {
            bl_full &= t;
            bl_none &= t;
        } else if (door_type == 'O') {
            bl_full |= t;
            bl_none |= t;
        } else {
            bl_full ^= t;
            bl_none ^= t;
        }
    }
    for (int i = 30; i >= 0; i--) {
    	// 为什么要倒叙？关于下面的 "1 -> 1" 可能性. 
        if (bl_none & (1 << i)) {
        	// 经过门的转化之后 : 0 -> 1 [选择该种]
            ans += 1 << i;
            // 初始 : 0
			// 答案 : 多加上 (1 << i) 
        } else if(m >= (1 << i) && (bl_full & (1 << i))) {
        	// 经过门的转化之后 : 1 -> 1 [若可以选择该种(所需的攻击力足够)]
            m -= 1 << i;
            ans += 1 << i;
        }
    }
    write(ans);
    return 0;
}
