//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      [Violet 6]蒲公英.
 * @user_name:    brealid/hkxadpall/jmoo/jomoo/zhaoyi20/航空信奥/littleTortoise.
 * @time:         2020-06-10.
 * @language:     C++.
*************************************/ 
#pragma GCC optimize(2)
#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// #define int int64

const int N = 100000 + 7, SqrtN = 507;

int n, c, Q;
int bl, blc;
int a[N];
int even[SqrtN][SqrtN];
int occur[SqrtN][N];
int cnt[N];

#define block(v) ((v) / bl)
#define st(v)    max((v) * bl, 1)
#define ed(v)    min(((v) + 1) * bl - 1, n)
#define len(v)   (ed(v) - st(v) + 1)

signed main() {
    n = read<int>();
    c = read<int>();
    Q = read<int>();
    // bl = sqrt(n * log2(n));
    // bl = sqrt(n / log2(n));
    bl = sqrt(n);
    // bl = SqrtN;
    blc = block(n);
    // printf("bl = %d\n", bl);
    for (int i = 1; i <= n; i++) {
        a[i] = read<int>();
    }
    for (register int i = 0; i <= blc; i++) {
        for (register int j = i; j <= blc; j++) {
            if (j != i) even[i][j] = even[i][j - 1];
            for (register int k = st(j), ek = ed(j); k <= ek; k++) {
                cnt[a[k]]++;
                // occur[j][a[k]]++;
                if (cnt[a[k]] != 1 && (cnt[a[k]] & 1)) even[i][j]--;
                else if (!(cnt[a[k]] & 1)) even[i][j]++;
            }
        }
        memset(cnt, 0, sizeof(cnt));
    }
    for (register int j = 0; j <= blc; j++) 
        for (register int k = st(j), ek = ed(j); k <= ek; k++) 
            occur[j][a[k]]++;
    for (int i = 1; i <= blc; i++) 
        for (int j = 1; j <= c; j++)
            occur[i][j] += occur[i - 1][j];
    // for (int i = 0; i <= blc; i++) 
    //     for (int j = 1; j <= c; j++) printf("occur[%d][%d] = %d\n", i, j, occur[i][j]);
    // fprintf(stderr, "Pretreat ended in %ums\n", clock());
    // unsigned c1 = 0, c2 = 0, c3 = 0, ct;
    for (int i = 1, l, r, b1, b2, x = 0, oc1, oc2; i <= Q; i++) {
        // if (!(i & 2047)) fprintf(stderr, "Solve %d case in %ums (%u, %u, %u)\n", i, clock(), c1, c2, c3);
        // l = (read<int>() + x) % n + 1;
        // r = (read<int>() + x) % n + 1;
        scanf("%d%d", &l, &r);
        l = (l + x) % n + 1;
        r = (r + x) % n + 1;
        // l = read<int>();
        // r = read<int>();
        if (l > r) swap(l, r);
        b1 = block(l);
        b2 = block(r);
        if (b1 + 1 >= b2) {
            x = 0;
            // ct = clock();
            for (register int i = l; i <= r; i++) cnt[a[i]]++;
            for (register int i = l; i <= r; i++) {
                if (!cnt[a[i]]) continue;
                if (!(cnt[a[i]] & 1)) x++;
                cnt[a[i]] = 0;
            }
            // c1 += clock() - ct;
        } else {
            x = even[b1 + 1][b2 - 1];
            // printf("b1~b2 : %d\n", x);
            // ct = clock();
            for (register int i = l, ek = ed(b1); i <= ek; i++) cnt[a[i]]++;
            for (register int i = st(b2); i <= r; i++) cnt[a[i]]++;
            for (register int i = l, ek = ed(b1); i <= ek; i++) {
                if (!cnt[a[i]]) continue;
                oc1 = occur[b2 - 1][a[i]] - occur[b1][a[i]];
                oc2 = cnt[a[i]] + oc1;
                // printf("num %d : oc1 = %d, oc2 = %d\n", a[i], oc1, oc2);
                if (!(oc2 & 1) && (!oc1 || (oc1 & 1))) x++;
                else if ((oc2 & 1) && (oc1 && !(oc1 & 1))) x--;
                cnt[a[i]] = 0;
            }
            for (register int i = st(b2); i <= r; i++) {
                if (!cnt[a[i]]) continue;
                oc1 = occur[b2 - 1][a[i]] - occur[b1][a[i]];
                oc2 = cnt[a[i]] + oc1;
                // printf("num %d : oc1 = %d, oc2 = %d\n", a[i], oc1, oc2);
                if (!(oc2 & 1) && (!oc1 || (oc1 & 1))) x++;
                else if ((oc2 & 1) && (oc1 && !(oc1 & 1))) x--;
                cnt[a[i]] = 0;
            }
            // c2 += clock() - ct;
        }
        // ct = clock();
        // printf("%d\n", x);
        write(x, 10);
        // c3 += clock() - ct;
    }
    return EXIT_SUCCESS;
}

// Create File Date : 2020-06-10