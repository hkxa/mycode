/*************************************
 * @problem:      [CQOI2018]异或序列.
 * @author:       brealid.
 * @time:         2020-12-01.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 1e5 + 7, B = 1 << 20;

int n, m, k;
int s[N];

struct query {
    int l, r, block, id;
    bool operator < (const query &b) const {
        return block ^ b.block ? block < b.block : ((block & 1) ? r < b.r : r > b.r);
    }
} q[N];

int cnt[B];
int64 ans[N], now;

inline void inc(const int &val) {
    now += cnt[val];
    ++cnt[val ^ k];
}

inline void dec(const int &val) {
    --cnt[val ^ k];
    now -= cnt[val];
}

signed main() {
    kin >> n >> m >> k;
    int block_size = sqrt(n);
    for (int i = 1; i <= n; ++i)
        s[i] = s[i - 1] ^ kin.get<int>();
    for (int i = 1; i <= m; ++i) {
        kin >> q[i].l >> q[i].r;
        q[i].block = --q[i].l / block_size;
        q[i].id = i;
    }
    sort(q + 1, q + m + 1);
    for (int i = 1, l = 0, r = -1; i <= m; ++i) {
        while (r < q[i].r) now += cnt[s[++r]], ++cnt[s[r] ^ k];
        while (l > q[i].l) now += cnt[s[--l]], ++cnt[s[l] ^ k];
        while (r > q[i].r) --cnt[s[r] ^ k], now -= cnt[s[r--]];
        while (l < q[i].l) --cnt[s[l] ^ k], now -= cnt[s[l++]];
        ans[q[i].id] = now;
    }
    for (int i = 1; i <= m; ++i)
        kout << ans[i] << '\n';
    return 0;
}