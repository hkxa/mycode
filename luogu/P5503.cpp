/*************************************
 * @problem:      [JSOI2016]灯塔.
 * @author:       brealid.
 * @time:         2020-12-03.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define getchar() (i_P1 == i_P2 && (i_P2 = (i_P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), i_P1 == i_P2) ? EOF : *i_P1++)
char BUF[1 << 21], *i_P1 = BUF, *i_P2 = BUF;
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int tail = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++tail] = '0' | (x % 10);
                x /= 10;
            }
            while (tail) putchar(buffer[tail--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 5e5 + 7;

int n, h[N], ans[N];
int q[N], intersect[N], head, tail;

inline double calc(int x, int id) {
    return h[x] + sqrt(abs(x - id));
}

inline double intersect_L(int x, int y) {
    double l = max(x, y), r = n, mid;
    for (int i = 15; --i;) {
        mid = (l + r) / 2;
        if (calc(x, mid) > calc(y, mid)) l = mid;
        else r = mid;
    }
    return (l + r) / 2;
}

inline double intersect_R(int x, int y) {
    double l = 0, r = min(x, y), mid;
    for (int i = 15; --i;) {
        mid = (l + r) / 2;
        if (calc(x, mid) < calc(y, mid)) l = mid;
        else r = mid;
    }
    return (l + r) / 2;
}

signed main() {
    kin >> n;
    for (int i = 1; i <= n; ++i) kin >> h[i];
    q[head = tail = 1] = 1;
    for (int i = 2; i <= n; ++i) {
        while (head < tail && calc(q[head], i) < calc(q[head + 1], i)) ++head;
        ans[i] = ceil(calc(q[head], i));
        while (head < tail && intersect[tail] >= intersect_L(q[tail], i)) --tail;
        q[++tail] = i;
        intersect[tail] = intersect_L(q[tail - 1], i);
    }
    q[head = tail = 1] = n;
    for (int i = n - 1; i >= 1; --i) {
        while (head < tail && calc(q[head], i) < calc(q[head + 1], i)) ++head;
        ans[i] = max(ans[i], (int)ceil(calc(q[head], i)));
        while (head < tail && intersect[tail] <= intersect_R(q[tail], i)) --tail;
        q[++tail] = i;
        intersect[tail] = intersect_R(q[tail - 1], i);
    }
    for (int i = 1; i <= n; ++i)
        kout << max(ans[i] - h[i], 0) << '\n';
    return 0;
}