/*************************************
 * @problem:      AtCoder - tokiomarine2020_e - O(rand).
 * @author:       brealid, Jomoo(Vjudge).
 * @time:         2021-03-07.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

// #define is_on(x, b) ((x) >> (b) & 1)
// #define is_off(x, b) (!is_on(x, b))
// #define turn_on(x, b) ((x) |= (decltype(x)(1) << b))
// #define turn_off(x, b) ((x) &= ~(decltype(x)(1) << b))

int n, K, S, T, bit, mask;
int a[53];

int count_bit(int n) {
    int res = 0;
    while (n) {
        ++res;
        n ^= n & -n;
    }
    return res;
}

int compress(int num, int s) {
    int nRet = 0, b = 0;
    while (s && num) {
        if (s & 1) {
            if (num & 1) nRet |= 1 << b;
            ++b;
        }
        s >>= 1;
        num >>= 1;
    }
    return nRet;
}

int64 cnt_include[263000], C[53][53], F[53];
bool is_popcount_odd[263000];

// inline void ArrayAdd(int64 *to, const int64 *from, int nK) {
//     while (nK--)
//         *(to++) += *(from++);
// }

signed main() {
    kin >> n >> K >> S >> T;
    if ((S | T) != T) {
        kout <<"0\n";
        return 0;
    }
    C[0][0] = 1;
    for (int i = 1; i <= n; ++i) {
        C[i][0] = 1;
        for (int j = 1; j <= i; ++j)
            C[i][j] = C[i - 1][j - 1] + C[i - 1][j];
    }
    for (int x = 0; x <= n; ++x)
        for (int i = 0; i < K; ++i)
            F[x] += C[x][i];
    // for (int x = 0; x <= n; ++x) printf("F(%d) = %d\n", x, F[x]);
    int Diff_ST = T & ~S;
    for (int i = 1; i <= n; ++i) {
        kin >> a[i];
        if ((a[i] & S) != S || (a[i] | T) != T) --n, --i;
        else a[i] = compress(a[i], Diff_ST);
    }
    bit = count_bit(Diff_ST);
    // T = compress(T, Diff_ST);
    // S = 0;
    mask = (1 << bit) - 1;
    for (int i = 1; i <= mask; ++i) is_popcount_odd[i] = (is_popcount_odd[i >> 1] != (i & 1));
    // for (int i = 0; i <= mask; ++i) printf("%d: %s\n", i, is_popcount_odd[i] ? "True" : "False");
    // printf("K, S, T = %d, %d, %d\na = [", K, S, T);
    // for (int i = 1; i <= n; ++i)
    //     printf(" %d%c", a[i], ", "[i == n]);
    // printf("]\n");
    // printf("bit, mask = %d, %d\n", bit, mask);
    int64 ans = 0;
    for (int s = 1; s <= n; ++s) {
        memset(cnt_include, 0, sizeof(cnt_include));
        for (int i = s + 1; i <= n; ++i) {
            int num = a[i] ^ (~a[s] & mask);
            // printf("{ s: %d, i: %d }\n", s, i);
            // for (int b = num; b; b = (b - 1) & num)
            //     ++cnt_include[b];
            for (int s = 0; s <= mask; ++s)
                if ((num & s) == s) ++cnt_include[s];
            // printf("num = %d, b = %d\n", num, b);
        }
        for (int b = 0; b <= mask; ++b)
            if (is_popcount_odd[b]) ans -= F[cnt_include[b]];
            else ans += F[cnt_include[b]];
    }
    kout << ans << '\n';
    return 0;
}