//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Little Pony and Summer Sun Celebration.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-14.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

namespace against_cpp11 {
    const int N = 1e5 + 7;
    int n, m, x[N];
    vector<int> G[N];
    int seq[N * 4], seq_cnt;
#   define mark_traveled(node) ((x[node] = !x[node]), (seq[++seq_cnt] = (node)))
    int vis[N], have_1_x;
    void dfs(int u) {
        // printf("dfs(%d)\n", u);
        if (x[u]) have_1_x = true;
        vis[u] = 1;
        mark_traveled(u);
        for (size_t i = 0; i < G[u].size(); i++) {
            int v = G[u][i];
            if (vis[v]) continue;
            // printf("Tree structure : %d -> %d\n", u, v);
            dfs(v);
            mark_traveled(u);
            if (x[v]) {
                mark_traveled(v);
                mark_traveled(u);
            }
        }
    }
    void set_x_to_0(int u) {
        // printf("set_x_to_0(%d)\n", u);
        x[u] = 0;
        vis[u] = 2;
        for (size_t i = 0; i < G[u].size(); i++) {
            int v = G[u][i];
            if (vis[v] == 2) continue;
            set_x_to_0(v);
        }
    }
    signed main() {
        read >> n >> m;
        for (int i = 1, u, v; i <= m; i++) {
            read >> u >> v;
            G[u].push_back(v);
            G[v].push_back(u);
        }
        for (int i = 1; i <= n; i++) read >> x[i];
        // make all x(=1) to x(=0) [1=>0]
        // dfs(1=>0)
        // 0=>1? 
        // 0 also can be 1
        // 0=>1 must back
        // dfs first go then back
        // make a dfs tree
        int root = 1;
        have_1_x = 0;
        while (root <= n) {
            // printf("check ing root %d\n", root);
            if (!vis[root]) {
                dfs(root);
                if (have_1_x) break;
                set_x_to_0(root);
                seq_cnt = 0;
            }
            root++;
        }
        for (int i = 1; i <= n; i++)
            if (x[i] && i != root) {
                puts("-1");
                return 0;
            }
        int l = 1, r = seq_cnt;
        if (x[root] == 1) l = 2;
        while (l < r && seq[l] == seq[r]) l++, r--;
        write << r - l + 1 << '\n';
        for (int i = l; i <= r; i++)
            write << seq[i] << " \n"[i == r];
        return 0;
    }
}

signed main() { return against_cpp11::main(); }