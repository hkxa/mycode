/*************************************
 * problem:      P2882 [USACO07MAR]面对正确的方式Face The Right Way.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-07-24.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

int n;
int m = 5007, k = 5007;
int original[5007];
int operate[5007];

void check(int machineLevel)
{
    // printf("check(%d)\n", machineLevel);
    int sum = 0, cnt = 0;
    for (int i = 1; i <= n - machineLevel + 1; i++) {
        if ((sum + original[i]) & 1) operate[i] = 0;
        else {
            operate[i] = 1;
            sum++;
            cnt++;
            // printf("operate %d~%d\n", i, i + machineLevel - 1);
        }
        if (i >= machineLevel) sum -= operate[i - machineLevel + 1];
    }
    for (int i = n - machineLevel + 2; i <= n; i++) {
        if (((sum + original[i]) & 1) == 0) return;
        if (i >= machineLevel) sum -= operate[i - machineLevel + 1];
    }
    // printf("Succeed with k = %d, m = %d.\n", machineLevel, cnt);
    if (cnt < m) {
        m = cnt;
        k = machineLevel;
    } else if (cnt == m) {
        k = min(k, machineLevel);
    }
}

char buf[3];

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        scanf("%s", buf);
        original[i] = (buf[0] == 'F');
    }
    for (int testK = 1; testK <= n; testK++) {
        check(testK);
    }
    write(k, 32), write(m, 10);
    return 0;
}