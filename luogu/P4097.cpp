/*************************************
 * @problem:      [HEOI2013]Segment.
 * @author:       brealid.
 * @time:         2020-11-17.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#if true
#define getchar() (i_P1 == i_P2 && (i_P2 = (i_P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), i_P1 == i_P2) ? EOF : *i_P1++)
char BUF[1 << 21], *i_P1 = BUF, *i_P2 = BUF;
#endif

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        Reader& operator >> (double &lf) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF && endch != '.') endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            if (endch != '.') {
                lf = (endch & 15);
                while (isdigit(endch = getchar())) lf = lf * 10 + (endch & 15);
            } else lf = 0;
            if (endch == '.') {
                double len = 1;
                while (isdigit(endch = getchar())) lf += (endch & 15) * (len *= 0.1);
            }
            if (flag) lf = -lf;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 1e5 + 7, TrMax = 39989 + 7, MaxY = 1e9;

int n, m;
template <typename T>
struct funct {
    T k, b;
    funct() : k(0), b(0) {}
    funct(const T &K, const T &B) : k(K), b(B) {}
    T operator () (int val) const {
        return k * val + b;
    }
};
funct<double> f[N];
int tr[TrMax << 2];

void segt_insert(const int &u, const int &l, const int &r, const int &ml, const int &mr, const int &id) {
    if (l > mr || r < ml) return;
    int mid = (l + r) >> 1;
    if (l >= ml && r <= mr) {
        if (!tr[u]) {
            tr[u] = id;
            return;
        }
        if (l == r) {
            if (f[id](l) > f[tr[u]](l)) tr[u] = id;
            return;
        }
        if (f[id].k > f[tr[u]].k) {
            if (f[id](mid) > f[tr[u]](mid)) segt_insert(u << 1, l, mid, ml, mr, tr[u]), tr[u] = id;
            else segt_insert(u << 1 | 1, mid + 1, r, ml, mr, id);
        } else {
            if (f[id](mid) > f[tr[u]](mid)) segt_insert(u << 1 | 1, mid + 1, r, ml, mr, tr[u]), tr[u] = id;
            else segt_insert(u << 1, l, mid, ml, mr, id);
        }
    } else {
        segt_insert(u << 1, l, mid, ml, mr, id);
        segt_insert(u << 1 | 1, mid + 1, r, ml, mr, id);
    }
}

int mx_id;
double mx_val;

inline void update(int id, double v) {
    (v > mx_val || (v == mx_val && id < mx_id)) && (mx_id = id, mx_val = v);
}

void segt_query(const int &u, const int &l, const int &r, const int &pos) {
    if (tr[u]) update(tr[u], f[tr[u]](pos));
    if (l == r) return;
    int mid = (l + r) >> 1;
    pos <= mid ? segt_query(u << 1, l, mid, pos) : segt_query(u << 1 | 1, mid + 1, r, pos);
}

signed main() {
    n = 39989;
    kin >> m;
    for (int i = 1, id = 0, opt, x1, y1, x2, y2; i <= m; ++i) {
        kin >> opt;
        if (opt == 0) {
            kin >> x1;
            x1 = (x1 + mx_id - 1) % n + 1;
            mx_val = mx_id = 0;
            segt_query(1, 1, n, x1);
            kout << mx_id << '\n';
        } else {
            kin >> x1 >> y1 >> x2 >> y2;
            x1 = (x1 + mx_id - 1) % n + 1;
            y1 = (y1 + mx_id - 1) % MaxY + 1;
            x2 = (x2 + mx_id - 1) % n + 1;
            y2 = (y2 + mx_id - 1) % MaxY + 1;
            if (x1 > x2) swap(x1, x2), swap(y1, y2);
            if (x1 == x2) {
                f[++id].k = 0;
                f[id].b = max(y1, y2);
            } else {
                f[++id].k = (double)(y2 - y1) / (x2 - x1);
                f[id].b = y1 - (double)(y2 - y1) / (x2 - x1) * x1;
            }
            segt_insert(1, 1, n, x1, x2, id);
        }
    }
    return 0;
}