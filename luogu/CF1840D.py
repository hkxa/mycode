T = int(input())
for _ in range(T):
    n = int(input())
    a = sorted(list(map(int, input().split())))
    if n <= 3:
        print(0)
        continue
    def upper_bound(v, l, r):
        while l < r:
            m = (l + r) // 2
            if a[m] <= v:
                l = m + 1
            else:
                r = m
        return l
    def check(x):
        period2_start = upper_bound(a[0] + x * 2, 0, n)
        if period2_start == n:
            return True
        period3_start = upper_bound(a[period2_start] + x * 2, period2_start, n)
        if period3_start == n:
            return True
        return a[-1] - a[period3_start] <= x * 2
        
    l, r = 0, a[-1] + 1
    while l < r:
        m = (l + r) // 2
        if check(m):
            r = m
        else:
            l = m + 1
    print(l)

    # sum = [a[0]]
    # for i in range(1, n):
    #     sum.append(sum[-1] + a[i])
    # s = lambda i, j: sum[j] - sum[i - 1] if i > 0 else sum[j]
    # cost = lambda i, j: s((i+j+1)//2, j) - s(i, (i+j)//2)
    # ans = n * 1000000000
    # i, j = 0, 1
    # # [0,i] [i+1,j] [j+1,n-1]
    # while i < n - 2:
    #     if i == j:
    #         j += 1
    #     while j < n - 1 and cost(i, j) + cost(j + 1, n - 1) > cost(i, j + 1) + cost(j + 2, n - 1):
    #         j += 1
    #     # print(i, j)
    #     ans = min(ans, cost(0, i) + cost(i + 1, j) + cost(j + 1, n - 1))
    #     i += 1
    # print(ans)
