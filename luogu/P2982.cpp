/*************************************
 * @problem:      P2982 [USACO10FEB]Slowing down G.
 * @user_id:      63720.
 * @user_name:    brealid.
 * @time:         2020-05-19.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int N = 100000 + 7;

int n;
int p[N];
vector<int> G[N];
int dfn[N], dft;
int f[N], beg[N], wson[N], siz[N];

// 树状数组 Tree array
int t[100007]; 
#define lowbit(x) ((x) & (-(x)))
void add(int x, int dif) {
    // printf("add %d\n", x);
    while (x <= n) {
        t[x] += dif;
        x += lowbit(x);
    }
}
int sum(int x) {
    int res = 0;
    while (x) {
        res += t[x];
        x -= lowbit(x);
    }
    return res;
}
#define query(l, r) (sum(r) - sum((l) - 1))

void dfs1(int u, int fa)
{
    siz[u] = 1;
    f[u] = fa;
    for (int v : G[u]) {
        if (v != fa) {
            dfs1(v, u);
            siz[u] += siz[v];
            if (siz[v] > siz[wson[u]]) 
                wson[u] = v;
        }
    }
}

void dfs2(int u, int chainBegin)
{
    dfn[u] = ++dft;
    beg[u] = chainBegin;
    if (wson[u]) dfs2(wson[u], chainBegin);
    for (int v : G[u]) {
        if (v != f[u] && v != wson[u]) {
            dfs2(v, v);
        }
    }
}

int main()
{
    n = read<int>();
    for (int i = 1, a, b; i < n; i++) {
        a = read<int>();
        b = read<int>();
        G[a].push_back(b);
        G[b].push_back(a);
    }
    for (int i = 1; i <= n; i++) p[i] = read<int>();
    dfs1(1, 0);
    dfs2(1, 1);
    int64 ans = 0;
    for (int i = 1, u; i <= n; i++) {
        u = p[i];
        ans = 0;
        while (u) {
            // printf("query(%d, %d)\n", dfn[beg[u]], dfn[u]);
            ans += query(dfn[beg[u]], dfn[u]);
            u = f[beg[u]];
        }
        add(dfn[p[i]], 1);
        write(ans, 10);
    }
    return 0;
}

/*
简单梳理一下这道题可以用的两种方法  

首先基础是：**dfs序+区间数据结构(线段树，树状数组)**

## 1. 树链剖分（对于每个节点考虑“从哪里来”）
对于每个点，查询从这个节点到根节点经过了多少个有奶牛的点，作为其 $ans$  
可以处理一个点就输出这个点的 $ans$  
因此需要 **树链剖分**   
时间复杂度：$n$ 个节点 $\times$ 树链剖分的 $\log n$ 段链 $\times$ 区间查询(区间加)的 $\log n$，总时间复杂度 $O(n \log^2 n)$  

优点是可以**在线查询**  
缺点是**时间复杂度较大，常数相对较大**，不过足已通过本题  

代码: [传送门](https://gitee.com/hkxa/mycode/blob/master/luogu/P2982.cpp)

## 2. dfs 序（对于每个节点考虑“到哪里去”）
对于每个点，使其所有子树的 $ans = ans + 1$  
最后处理完每个点后再输出每个点的 $ans$  
不需要 **树链剖分**，仅需要 **dfs序**  
时间复杂度：$n$ 个节点 $\times$ 区间查询(区间加)的 $\log n$，总时间复杂度 $O(n \log n)$  

优点是**时间复杂度较小，常数相对较小**，可以应对时间卡的比较严格的题目   
缺点是不可以**在线查询** 

这种代码就不贴了，大部分题解都是这种

*/