/*************************************
 * @problem:      [TJOI2008]Binary Land.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2019-12-24.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int MaxRC = 30 + 7;

int r, c;
char mp[MaxRC][MaxRC];
bool vis[1000000];
#define ForInMap(i, j) for (int i = 1; i <= r; i++) for (int j = 1; j <= c; j++)
int xx[4] = {0, 0, 1, -1}, yy[4] = {1, -1, 0, 0};

struct pos { int x, y; inline bool operator==(pos); } ans;
inline bool pos::operator == (pos b) { return x == b.x && y == b.y; }
struct status { pos G, M; int step; inline status operator+(int); } start, t, m, m_true;
inline status status::operator + (int op) { 
    return (status){(pos){G.x + xx[op], G.y - yy[op]}, (pos){M.x + xx[op], M.y + yy[op]}, step + 1};
}
inline int zip(status s) { return s.G.x * 27000 + s.G.y * 900 + s.M.x * 30 + s.M.y; }
inline bool isAnswer(status s) { return ans == s.G && ans == s.M; }

inline bool could_go(status s) { 
    return (mp[s.G.x][s.G.y] == '.' || mp[s.M.x][s.M.y] == '.') && 
           (mp[s.G.x][s.G.y] != 'X' && mp[s.M.x][s.M.y] != 'X'); 
}

int main()
{
    r = read<int>();
    c = read<int>();
    for (int i = 1; i <= r; i++) scanf("%s", mp[i] + 1);
    ForInMap(i, j) {
        if (mp[i][j] == 'G') start.G = (pos){i, j}, mp[i][j] = '.';
        if (mp[i][j] == 'M') start.M = (pos){i, j}, mp[i][j] = '.';
        if (mp[i][j] == 'T') ans = (pos){i, j}, mp[i][j] = '.';
    }
    start.step = 0;
    queue<status> q;
    q.push(start);
    while (!q.empty()) {
        t = q.front(); q.pop();
        for (int op = 0; op < 4; op++) {
            m_true = m = t + op;
            if (mp[m.G.x][m.G.y] != '.') m_true.G = t.G;
            if (mp[m.M.x][m.M.y] != '.') m_true.M = t.M;
            if (!vis[zip(m_true)] && could_go(m)) {
                vis[zip(m_true)] = true;
                if (isAnswer(m_true)) {
                    printf("%d", m.step);
                    return 0;
                } else q.push(m_true);
            }
        }
    }
    printf("no");
    return 0;
}

/*
10 15
.......T.......
.###.###.###.##
##.#.#.###.#.#.
..X....#.......
.#####.#.#####.
.......#....X..
##.#.#.#.#.#.##
.......#.......
.#############.
......G#M......

12 17
#################
#.......T.......#
#.###.###.###.###
###.#.#.###.#.#.#
#..X....#.......#
#.#####.#.#####.#
#.......#....X..#
###.#.#.#.#.#.###
#.......#.......#
#.#############.#
#......G#M......#
#################
*/