/*************************************
 * @problem:      sotomon.
 * @author:       brealid.
 * @time:         2020-11-27.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;
#define getchar() (i_P1 == i_P2 && (i_P2 = (i_P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), i_P1 == i_P2) ? EOF : *i_P1++)
char BUF[1 << 21], *i_P1 = BUF, *i_P2 = BUF;
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;
namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

const int N = 1e5 + 7, RC = 1e6 + 7, Node = 3e5 + 5, Edge = 1e6 + 7;

struct door {
    int x, y, type;
} d[N];
int n, r, c;
int ln[RC], col[RC];
map<int, int> id[RC];
int node_total;
struct graph {
    struct edge {
        int to, nxt_edge;
    } e[Edge];
    int head[Node], ecnt;
    graph() : ecnt(1) {}
    inline void add_edge(const int &from, const int &to) {
        e[++ecnt].to = to;
        e[ecnt].nxt_edge = head[from];
        head[from] = ecnt;
    }
} origin, tarjaned;
int ans[Node];

namespace tarjan {
    int dfn[Node], low[Node], dft;
    int fam[Node], siz[Node], fam_cnt;
    int sta[Node], top;
    bool ins[Node];
    inline void solve(int u) {
        sta[++top] = u;
        ins[u] = true;
        dfn[u] = low[u] = ++dft;
        for (int i = origin.head[u]; i; i = origin.e[i].nxt_edge) {
            int v = origin.e[i].to;
            if (!dfn[v]) {
                solve(v);
                low[u] = min(low[u], low[v]);
            } else if (ins[v]) {
                low[u] = min(low[u], dfn[v]);
            }
        }
        if (low[u] == dfn[u]) {
            ++fam_cnt;
            while (low[sta[top]] >= low[u]) {
                ins[sta[top]] = false;
                fam[sta[top]] = fam_cnt;
                if (sta[top--] <= n) ++siz[fam_cnt];
            }
        }
    }
}

int solve(int u) {
    if (ans[u]) return ans[u];
    for (int i = tarjaned.head[u]; i; i = tarjaned.e[i].nxt_edge)
        ans[u] = max(ans[u], solve(tarjaned.e[i].to));
    return ans[u] += tarjan::siz[u];
}

signed main() {
    // file_io::set_to_file("sotomon");
    kin >> n >> r >> c;
    node_total = n;
    for (int i = 1; i <= n; ++i) {
        kin >> d[i].x >> d[i].y >> d[i].type;
        id[d[i].x][d[i].y] = i;
        if (!ln[d[i].x]) ln[d[i].x] = ++node_total;
        if (!col[d[i].y]) col[d[i].y] = ++node_total;
    }
    for (int i = 1; i <= n; ++i) {
        origin.add_edge(ln[d[i].x], i);
        origin.add_edge(col[d[i].y], i);
        if (d[i].type == 1) origin.add_edge(i, ln[d[i].x]);
        else if (d[i].type == 2) origin.add_edge(i, col[d[i].y]);
        else if (d[i].type == 3) {
            if (id[d[i].x - 1].count(d[i].y - 1)) origin.add_edge(i, id[d[i].x - 1][d[i].y - 1]);
            if (id[d[i].x - 1].count(d[i].y    )) origin.add_edge(i, id[d[i].x - 1][d[i].y    ]);
            if (id[d[i].x - 1].count(d[i].y + 1)) origin.add_edge(i, id[d[i].x - 1][d[i].y + 1]);
            if (id[d[i].x    ].count(d[i].y - 1)) origin.add_edge(i, id[d[i].x    ][d[i].y - 1]);
            if (id[d[i].x    ].count(d[i].y + 1)) origin.add_edge(i, id[d[i].x    ][d[i].y + 1]);
            if (id[d[i].x + 1].count(d[i].y - 1)) origin.add_edge(i, id[d[i].x + 1][d[i].y - 1]);
            if (id[d[i].x + 1].count(d[i].y    )) origin.add_edge(i, id[d[i].x + 1][d[i].y    ]);
            if (id[d[i].x + 1].count(d[i].y + 1)) origin.add_edge(i, id[d[i].x + 1][d[i].y + 1]);
        }
    }
    for (int i = 1; i <= node_total; ++i)
        if (!tarjan::dfn[i]) tarjan::solve(i);
    for (int u = 1; u <= node_total; ++u)
        for (int i = origin.head[u]; i; i = origin.e[i].nxt_edge) {
            int v = origin.e[i].to;
            if (tarjan::fam[u] != tarjan::fam[v]) 
                tarjaned.add_edge(tarjan::fam[u], tarjan::fam[v]);
        }
    int ans(0);
    for (int i = 1; i <= n; ++i)
        ans = max(ans, solve(i));
    kout << ans << '\n';
    return 0;
}