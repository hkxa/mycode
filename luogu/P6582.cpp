//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      T131085 座位调查.
 * @user_id:      63720.
 * @user_name:    brealid.
 * @time:         2020-05-30.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
       return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
       return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// #define int int64

const int N = 1003, P = 998244353;

inline int64 kpow(int64 a, int n) {
    int64 r = 1;
    while (n) {
        if (n & 1) r = r * a % P;
        a = a * a % P;
        n >>= 1;
    }
    return r;
}

inline int64 calc(int k, int cnt) {
    return k * kpow(k - 1, cnt - 1) % P;
}

int n, m, k;
char a[N][N];
int64 ans = 1;

int xx[4] = {0, 0, 1, -1}, yy[4] = {1, -1, 0, 0};

bool vis[N][N];

#define Seat(i, j) (!vis[i][j] && a[i][j] == 'O')
#define Near(i, j) (Seat(i, j + 1) + Seat(i, j - 1) + Seat(i + 1, j) + Seat(i - 1, j))

int check(pair<int, int> pos) {
    int x = pos.first, y = pos.second, nRet;
    vis[x][y] = 1;
    // printf("Near(%d, %d) = %d\n", x, y, Near(x, y));
    if ((nRet = Near(x, y)) > 1) return -1;
    if (!nRet) return 1;
    for (int o = 0; o < 4; o++)
        if (!vis[x + xx[o]][y + yy[o]] && Seat(x + xx[o], y + yy[o])) {
            if (~(nRet = check(make_pair(x + xx[o], y + yy[o])))) return nRet + 1;
            else return -1;
        }
}

void color(int x, int y) {
    a[x][y] = '-';
    for (int o = 0; o < 4; o++)
        if (a[x + xx[o]][y + yy[o]] == 'O') 
            color(x + xx[o], y + yy[o]);
}

pair<int, int> Find_Source(int i, int j) {
    vis[i][j] = 1;
    pair<int, int> piiRet;
    for (int o = 0; o < 4; o++)
        if (!vis[i + xx[o]][j + yy[o]] && a[i + xx[o]][j + yy[o]] == 'O') {
            piiRet = Find_Source(i + xx[o], j + yy[o]);
            vis[i][j] = 0;
            return piiRet;
        }
    // printf("Source : (%d, %d)\n", i, j);
    return (vis[i][j] = 0), make_pair(i, j);
}

signed main() {
    n = read<int>();
    m = read<int>();
    k = read<int>();
    for (int i = 1; i <= n; i++)
        scanf("%s", a[i] + 1);
    for (int i = 1, nRet; i <= n; i++) 
        for (int j = 1; j <= m; j++)
            if (a[i][j] == 'O') {
                if (~(nRet = check(Find_Source(i, j)))){
                    color(i, j);
                    ans = ans * calc(k, nRet) % P;
                } else {
                    write(0, 10);
                    return 0;
                }
            }
    write(ans, 10);
    return 0;
}

// Create File Date : 2020-05-30