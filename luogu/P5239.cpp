/*************************************
 * problem:      P5239 ���侩��.
 * user ID:      63720.
 * user name:    �����Ű�.
 * time:         2019-03-02.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 
/*************************************
 * record ID:    R16846397.
 * time:         197 ms
 * memory:       16652 KB
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x, char nextch = ' ')
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10, 0);
    putchar((x % 10) | 48);
    if (nextch) putchar(nextch);
}  

#define N 1003
long long q, n, m;
long long C[N + 3][N + 3];
long long ans[N + 3][N + 3] = {0};

int main()
{
	q = read<long long>();
	C[1][0] = 1;
	C[1][1] = 1;
	for (int i = 2; i < N; i++){
		C[i][0] = 1;
		for (int j = 1; j <= i; j++)
			C[i][j] = (C[i - 1][j] + C[i - 1][j - 1]) % 19260817;
		for (int j = i + 1; j < N; j++)
			C[i][j] = 0;
	}
//	ans[1][0] = 0;
//	ans[1][1] = 0;
//	for (int i = 2; i < N; i++){
//		ans[i][0] = 0;
//	}
	for (int i = 1; i < N; i++){
		for (int j = 1; j < N; j++) {
			ans[i][j] = (ans[i - 1][j] + ans[i][j - 1] - ans[i - 1][j - 1] + C[i][j] + 19260817) % 19260817;
		}
	}
//	for (int i = 1; i < 7; i++){
//		for (int j = 1; j < 7; j++) {
//			printf("[%-3lld, %-3lld] ", C[i][j], ans[i][j]);
//		}
//		puts("");
//	}
	while (q--) {
		n = read<long long>();
		m = read<long long>();
		printf("%lld\n", ans[m][n]);
	}
	return 0;
}
