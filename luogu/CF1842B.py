T = int(input())
for _ in range(T):
    n, x = map(int, input().split())
    u = 0
    for v in map(int, input().split()):
        if v & x != v:
            break
        u |= v
    for v in map(int, input().split()):
        if v & x != v:
            break
        u |= v
    for v in map(int, input().split()):
        if v & x != v:
            break
        u |= v
    print('YES' if u == x else 'NO')