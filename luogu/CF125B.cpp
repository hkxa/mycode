/*************************************
 * problem:      CF125B Simple XML.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-04-11.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 
/*************************************
 * record ID:    R18122825.
 * time:         754 ms
 * memory:       28 KB
*************************************/ 


#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

enum XMLflagType {
    XMLstart = 1, XMLend = 2
};

struct flag {
    string name;
    XMLflagType type;
} a[400];
int n = 0, pos = 1;

void printTab(int len)
{
    for (int i = 1; i <= len; i++) {
        putchar(' ');
        putchar(' ');
    }
}

void dfs(int h)
{
    while (a[pos].type == XMLstart) {
        printTab(h);
        printf("<%s>\n", a[pos].name.c_str());
        pos++;
        dfs(h + 1);
        printTab(h);
        printf("</%s>\n", a[pos].name.c_str());
        pos++;
        if (pos > n) return;
    }
}

int main()
{
    char str[503];
    while (getchar() == '<') {
        scanf("%[^>]>", str);
        n++;
        if (str[0] == '/') {
            a[n].type = XMLend;
            a[n].name = str + 1;
        } else {
            a[n].type = XMLstart;
            a[n].name = str;
        }
    }
    // printf("function 'read' is ok.\n");
    dfs(0);
}
/*
intput1

<a><b><c></c></b></a>
output1

<a>
  <b>
    <c>
    </c>
  </b>
</a>
input2

<a><b></b><d><c></c></d></a>
output2

<a>
  <b>
  </b>
  <d>
    <c>
    </c>
  </d>
</a>
*/