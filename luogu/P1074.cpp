// O2 needed
//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      sudoku.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-07.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

struct Reader {
    char endch;
    Reader() { endch = '\0'; }
    template <typename Int>
    Reader& operator >> (Int &i) {
        i = baseFastio::read<Int>(endch);
        return *this;
    }
    template <typename Int>
    inline Int get_int() {
        return baseFastio::read<Int>();
    }
    inline char get_nxt() {
        return endch;
    }
} read;

struct Writer {
    Writer& operator << (const char *ch) {
        // char *p = ch;
        while (*ch) putchar(*(ch++));
        return *this;
    }
    Writer& operator << (const char ch) {
        putchar(ch);
        return *this;
    }
    template <typename Int>
    Writer& operator << (const Int i) {
        baseFastio::write(i);
        return *this;
    }
} write;

// #define int int64

const int score[9][9] = {
    {6, 6, 6, 6, 6, 6, 6, 6, 6}, 
    {6, 7, 7, 7, 7, 7, 7, 7, 6}, 
    {6, 7, 8, 8, 8, 8, 8, 7, 6}, 
    {6, 7, 8, 9, 9, 9, 8, 7, 6}, 
    {6, 7, 8, 9,10, 9, 8, 7, 6}, 
    {6, 7, 8, 9, 9, 9, 8, 7, 6}, 
    {6, 7, 8, 8, 8, 8, 8, 7, 6}, 
    {6, 7, 7, 7, 7, 7, 7, 7, 6}, 
    {6, 6, 6, 6, 6, 6, 6, 6, 6}, 
};

int a[9][9];
// int may[9][9];

int cnt0() {
    int cnt = 0;
    for (int i = 0; i < 9; i++)
        for (int j = 0; j < 9; j++)
            if (!a[i][j]) cnt++;
    return cnt;
}

int cntScore() {
    int ret = 0;
    for (int i = 0; i < 9; i++)
        for (int j = 0; j < 9; j++)
            ret += score[i][j] * a[i][j];
    return ret;
}

int ans = -1;

bool logic(int &cnt0, int &now) {
    static bool could[10], updated;
    updated = true;
    while (updated) {
        updated = false;
        for (int i = 0; i < 9; i++)
            for (int j = 0; j < 9; j++)
                if (!a[i][j]) {
                    for (int k = 1; k <= 9; k++) could[k] = true;  
                    for (int k = 0; k < 9; k++) {
                        could[a[k][j]] = false;
                        could[a[i][k]] = false;
                    }
                    int b1 = i / 3 * 3, b2 = j / 3 * 3;
                    for (int x = b1; x < b1 + 3; x++)
                        for (int y = b2; y < b2 + 3; y++) {
                            could[a[x][y]] = false;
                        }
                    int ok = 0;
                    for (int k = 1; k <= 9; k++) {
                        if (could[k]) {
                            if (ok) ok = 10;
                            else ok = k;
                        }
                    }
                    if (!ok) return 0;
                    else if (ok < 10) {
                        a[i][j] = ok;
                        now += ok * score[i][j];
                        cnt0--;
                        updated = true;
                    }
                }
    }
    return 1;
}

void dfs(int remain, int now) {
    if (now + 90 * remain <= ans) return;
    int backup[9][9];
    for (int i = 0; i < 9; i++)
        for (int j = 0; j < 9; j++)
            backup[i][j] = a[i][j];
    if (!logic(remain, now)) {
        for (int x = 0; x < 9; x++)
            for (int y = 0; y < 9; y++)
                a[x][y] = backup[x][y];
        return;
    }
    if (!remain) {
        ans = max(ans, now);
        for (int x = 0; x < 9; x++)
            for (int y = 0; y < 9; y++)
                a[x][y] = backup[x][y];
        return;
    }
    bool could[10];
    for (int i = 0; i < 9; i++)
        for (int j = 0; j < 9; j++)
            if (!a[i][j]) {
                for (int k = 1; k <= 9; k++) could[k] = true;
                for (int k = 0; k < 9; k++) {
                    could[a[k][j]] = false;
                    could[a[i][k]] = false;
                }
                int b1 = i / 3 * 3, b2 = j / 3 * 3;
                for (int x = b1; x < b1 + 3; x++)
                    for (int y = b2; y < b2 + 3; y++)
                        could[a[x][y]] = false;
                for (int fi = 1; fi <= 9; fi++) {
                    if (could[fi]) {
                        a[i][j] = fi;
                        dfs(remain - 1, now + score[i][j] * fi);
                        a[i][j] = 0;
                    }
                }
                for (int x = 0; x < 9; x++)
                    for (int y = 0; y < 9; y++)
                        a[x][y] = backup[x][y];
                return;
            }
    for (int x = 0; x < 9; x++)
        for (int y = 0; y < 9; y++)
            a[x][y] = backup[x][y];
}

signed main()
{
    // freopen("sudoku.in", "r", stdin);
    // freopen("sudoku.out", "w", stdout);
    for (int i = 0; i < 9; i++)
        for (int j = 0; j < 9; j++)
            read >> a[i][j];
    dfs(cnt0(), cntScore());
    write << ans << '\n';
    return 0;
}