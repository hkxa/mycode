//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Arpa and a game with Mojtaba.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-08.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int eps = 1e-8;

namespace against_cpp11 {
    const int N = 100 + 7, M = 1000 + 7;
    int n;
    vector<pair<int, int> > a[N];
    map<int, set<int> > m;
    typedef map<int, set<int> >::iterator misi_it;
    typedef set<int>::iterator si_it;
    int l[M], r[M], k;
    template <typename Iter> Iter nxt(Iter it) { return ++it; }
    template <typename Iter> Iter pre(Iter it) { return --it; }
    int value, ans;
    int popcount(int val) {
        int ret = 0;
        while (val) {
            ret++;
            val -= (val & -val); 
        }
        return ret;
    }
    int highbit(int val) {
        int ret = 0;
        while (val) {
            ret++;
            val >>= 1; 
        }
        return ret;
    }
    map<int, int> mex_value;
    int mex(int u) {
        if (!u) return 0;
        if (mex_value.count(u)) return mex_value[u];
        int h = highbit(u);
        set<int> vis;
        for (int i = 0; i < h; i++)
            vis.insert(mex((u >> (i + 1)) | (u & ((1 << i) - 1))));
        for (int i = 0; ; i++)
            if (!vis.count(i)) return mex_value[u] = i;
    }
    signed main() {
        read >> n;
        for (int i = 1, num, sqr_number, cnt; i <= n; i++) {
            read >> num; 
            if (num == 1) { continue; }
            sqr_number = sqrt(num);
            for (int j = 2; j <= sqr_number; j++) {
                if (num % j == 0) {
                    cnt = 0;
                    while (num % j == 0) {
                        cnt++;
                        num /= j;
                    }
                    m[j].insert(cnt);
                }
            }
            if (num != 1) m[num].insert(1);
        }
        k = m.size();
        for (misi_it i = m.begin(); i != m.end(); ++i) {
            value = 0;
            for (si_it j = i->second.begin(), j_end = i->second.end(); j != j_end; ++j) {
                value |= (1 << (*j - 1));
            }
            ans ^= mex(value);
            // printf("%d, %d : %d\n", i->first, value, mex(value));
        }
        // fprintf(stderr, "mex = %d\n", ans);
        if (ans) puts("Mojtaba");
        else puts("Arpa");
        return 0;
    }
}

signed main() { return against_cpp11::main(); }