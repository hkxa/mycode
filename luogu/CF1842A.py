T = int(input())
for _ in range(T):
    n, m = map(int, input().split())
    a = list(map(int, input().split()))
    b = list(map(int, input().split()))
    diff = sum(a) - sum(b)
    if diff > 0:
        print('Tsondu')
    elif diff < 0:
        print('Tenzing')
    else:
        print('Draw')