/*************************************
 * @problem:      【模板】树上 k 级祖先.
 * @user_name:    brealid/hkxadpall/zhaoyi20/j(o)moo/littleTortoise.
 * @time:         2020-09-20.
 * @language:     C++.
 * @fastio_ver:   20200913.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0,    // input
        flush_stdout = 1 << 1,  // output
        flush_stderr = 1 << 2,  // output
    };
    enum number_type_flags {
        output_double_stable = 1 << 0,  // output
        output_double_faster = 1 << 1   // output
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f & ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore character set = {' ', '\r', '\n', '\t'}
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (double &lf) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            lf = endch & 15;
            while (isdigit(endch = getchar())) lf = lf * 10 + (endch & 15);
            if (endch == '.') {
                double digit = 0.1;
                while (isdigit(endch = getchar())) {
                    lf += (endch & 15) * digit;
                    digit *= 0.1;
                }
            }
            if (flag) lf = -lf;
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            Int d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return d;
        }
    } read;

    struct Writer {
        Writer& operator << (io_flags f) {
            if (f & flush_stdout) fflush(stdout);
            if (f & flush_stderr) fflush(stderr);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
        inline void operator () (double val, int eps_digit = 6, char endch = '\0', number_type_flags flg = output_double_faster) {
            if (flg & output_double_stable) {
                static char output_format[10];
                sprintf(output_format, "%%.%dlf", eps_digit);
                printf(output_format, val);
            } else if (flg & output_double_faster) {
                if (val < 0) {
                    putchar('-');
                    val = -val;
                }
                double eps_number = 0.5;
                for (int i = 1; i <= eps_digit; i++) eps_number /= 10;
                val += eps_number;
                (*this) << (int64)val;
                val -= (int64)val;
                if (eps_digit) putchar('.');
                while (eps_digit--) {
                    val *= 10;
                    putchar((int)val | '0');
                    val -= (int)val;
                }
            } else {
               (*this) << '1';
                if (eps_digit) {
                    (*this) << ".E";
                    for (int i = 2; i <= eps_digit; i++) (*this) << '0';
                }
            }
            if (endch) putchar(endch);
        }
    } write;
}
using namespace Fastio;

namespace File_IO {
    void init_IO(const char *file_name) {
        char buff[107];
        sprintf(buff, "%s.in", file_name);
        freopen(buff, "r", stdin);
        sprintf(buff, "%s.out", file_name);
        freopen(buff, "w", stdout);
    }
}

// #define int int64

struct DATA_GEN {
    uint32 x;
    inline uint32 nxt() {
        x ^= x << 13;
        x ^= x >> 17;
        x ^= x << 5;
        return x; 
    }
} gen;

const int N = 5e5 + 7, LogN = 20;

int n, q, root;
int fa[N][LogN + 2];
vector<int> sons[N], up[N], dn[N];

int lg2[N];

int dep[N], maxdep[N], wson[N];
int beg[N], chain_len[N];

void dfs1(int u) {
    maxdep[u] = dep[u] = dep[*fa[u]] + 1;
    for (int i = 0; i < LogN; i++)
        fa[u][i + 1] = fa[fa[u][i]][i];
    for (size_t i = 0; i < sons[u].size(); i++) {
        int v = sons[u][i];
        dfs1(v);
        if (maxdep[u] < maxdep[v]) {
            maxdep[u] = maxdep[v];
            wson[u] = v;
        }
    }
}

void dfs2(int u) {
    if (wson[*fa[u]] == u) beg[u] = beg[*fa[u]];
    else {
        beg[u] = u;
        int len = chain_len[u] = maxdep[u] - dep[u];
        up[u].resize(len);
        dn[u].resize(len);
        for (int x = *fa[u], cnt = 0; cnt < len; cnt++, x = *fa[x]) up[u][cnt] = x;
        for (int x = wson[u], cnt = 0; cnt < len; cnt++, x = wson[x]) dn[u][cnt] = x;
    }
    for (size_t i = 0; i < sons[u].size(); i++) {
        int v = sons[u][i];
        dfs2(v);
    }
}

int query(int u, int kth) {
    if (!kth) return u;
    u = fa[u][lg2[kth]];
    kth -= (1 << lg2[kth]);
    int dis = dep[u] - dep[beg[u]];
    if (kth == dis) return beg[u];
    else if (kth < dis) return dn[beg[u]][dis - kth - 1];
    else return up[beg[u]][kth - dis - 1];
}

signed main() {
    // File_IO::init_IO("【模板】树上 k 级祖先");
    read >> n >> q >> gen.x;
    lg2[0] = -1;
    for (int i = 1; i <= n; i++) lg2[i] = lg2[i >> 1] + 1;
    for (int i = 1; i <= n; i++) {
        int &f = *fa[i];
        read >> f;
        if (f) sons[f].push_back(i);
        else root = i;
    }
    dfs1(root);
    dfs2(root);
    int lastans = 0;
    int64 ans = 0;
    for (int i = 1, x, d; i <= q; i++) {
        x = (gen.nxt() ^ lastans) % n + 1;
        d = (gen.nxt() ^ lastans) % dep[x];
        // printf("query(%d, %d) = ", x, d);
        lastans = query(x, d);
        ans ^= (int64)i * lastans;
        // printf("%d\n", lastans);
    }
    write << ans << '\n';
    return 0;
}