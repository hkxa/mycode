/*************************************
 * @problem:      Welfare State.
 * @author:       brealid.
 * @time:         2021-02-21.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 2e5 + 7;

int n, q;
int a[N], tr[N << 2];

void build(int u, int l, int r) {
    if (l == r) {
        tr[u] = a[l];
        return;
    }
    tr[u] = -1;
    int mid = (l + r) >> 1, ls = u << 1, rs = ls ^ 1;
    build(ls, l, mid);
    build(rs, mid + 1, r);
}

void modify(int u, int l, int r, int p, int new_v) {
    if (l == r) {
        tr[u] = new_v;
        return;
    }
    int mid = (l + r) >> 1, ls = u << 1, rs = ls ^ 1;
    if (tr[u] > tr[ls]) tr[ls] = tr[u];
    if (tr[u] > tr[rs]) tr[rs] = tr[u];
    tr[u] = -1;
    if (p <= mid) modify(ls, l, mid, p, new_v);
    else modify(rs, mid + 1, r, p, new_v);
}

void print_tree(int u, int l, int r) {
    if (l == r) {
        kout << tr[u] << " \n"[r == n];
        return;
    }
    int mid = (l + r) >> 1, ls = u << 1, rs = ls ^ 1;
    if (tr[u] > tr[ls]) tr[ls] = tr[u];
    if (tr[u] > tr[rs]) tr[rs] = tr[u];
    tr[u] = -1;
    print_tree(ls, l, mid);
    print_tree(rs, mid + 1, r);
}

signed main() {
    kin >> n;
    for (int i = 1; i <= n; ++i) kin >> a[i];
    build(1, 1, n);
    kin >> q;
    for (int i = 1, t, x; i <= q; ++i) {
        kin >> t >> x;
        if (t == 1) modify(1, 1, n, x, kin.get<int>());
        else if (tr[1] < x) tr[1] = x;
    }
    print_tree(1, 1, n);
    return 0;
}