#include <cstdio>
#include <iostream>
#include <algorithm>
using namespace std;

int n;
long long ans = 0;
long long num[10001];

bool comp(long long a, long long b)
{
    return a > b;
}

void js(int n)
{	
    int k = n; 
    for (int i = n-1; i >= 0; i--) {		
        if (num[i] >= num[n] || i == 0) {
            if (i == 0) i--;
            int temp = num[n];
            for (int j = n; j > i+1; j--) {
                num[j] = num[j-1];
            }
            num[i+1] = temp;
            return;
        }
    }
}
    
int main()
{
    scanf("%d", &n);
    for (int i = 0; i < n; i++)
        scanf("%lld", &num[i]);
    sort(num, num+n, comp);
    for (int i = n-1; i > 0; i--) {
        ans += (num[i-1] += num[i]);
        js(i-1);
    }
    printf("%lld", ans);
}
