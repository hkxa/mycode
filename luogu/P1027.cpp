//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Car的旅行路线.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-20.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 998244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? Module(w - P) : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

int n;
int S, t, A, B;
int T[107];
double dist[407][407];

// <MODULE>

struct point {
    int x, y;
    inline void readln() {
        x = read<int>();
        y = read<int>();
    }
    inline point operator + (point b) {
        return (point){x + b.x, y + b.y};
    }
    inline point operator - (point b) {
        return (point){x - b.x, y - b.y};
    }
};

inline int square(int x) {
    return x * x;
}

inline int square_distance(point a, point b) {
    return square(a.x - b.x) + square(a.y - b.y);
}

inline double distance(point a, point b) {
    return sqrt(square_distance(a, b));
}

struct airport {
    point pos;
    int city_id;
    inline void readln(int i) {
        pos.readln();
        city_id = i;
    }
    inline void construct_4thPoint(int i, airport a, airport b, airport c) {
        city_id = i;
        int ab = square_distance(a.pos, b.pos), 
            ac = square_distance(a.pos, c.pos), 
            bc = square_distance(b.pos, c.pos);
        if (ab + ac == bc) pos = b.pos + c.pos - a.pos;
        else if (ab + bc == ac) pos = a.pos + c.pos - b.pos;
        else if (ac + bc == ab) pos = a.pos + b.pos - c.pos;
    }
} a[407];

inline double cost(airport a, airport b) {
    if (a.city_id ^ b.city_id) return distance(a.pos, b.pos) * t;
    else return distance(a.pos, b.pos) * T[a.city_id];
}

// </MODULE>

inline void construct_graph() {
    int airport_count = S * 4;
    for (int i = 1; i <= airport_count; i++) 
        for (int j = i + 1; j <= airport_count; j++) 
            dist[i][j] = dist[j][i] = cost(a[i], a[j]);
}

// SPP : aka short-path problem
inline void SPP_floyed() {
    int airport_count = S * 4;
    for (int k = 1; k <= airport_count; k++)
        for (int i = 1; i <= airport_count; i++) if (i != k)
            for (int j = 1; j <= airport_count; j++) if (i != j && j != k)
                if (dist[i][j] > dist[i][k] + dist[k][j])
                    dist[i][j] = dist[i][k] + dist[k][j];
}

inline double get_minimum_cost() {
    double lfRet = 1e18; // double (aka long float) return value
    for (int i = A * 4 - 3; i <= A * 4; i++)
        for (int j = B * 4 - 3; j <= B * 4; j++)
            if (lfRet > dist[i][j])
                lfRet = dist[i][j];
    return lfRet;
}

signed main() {
    n = read<int>();
    while (n--) {
        // n cases
        S = read<int>();
        t = read<int>();
        A = read<int>();
        B = read<int>();
        for (int i = 1; i <= S; i++) {
            a[i * 4 - 3].readln(i);
            a[i * 4 - 2].readln(i);
            a[i * 4 - 1].readln(i);
            a[i * 4].construct_4thPoint(i, a[i * 4 - 3], a[i * 4 - 2], a[i * 4 - 1]);
            T[i] = read<int>();
        }
        construct_graph();
        SPP_floyed();
        printf("%.1lf\n", get_minimum_cost());
    }
    return 0;
}

// Create File Date : 2020-06-20