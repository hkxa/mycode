/*************************************
 * @problem:      SUM and REPLACE.
 * @user_name:    brealid.
 * @time:         2020-11-09.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

#ifdef DEBUG
# define passing() cerr << "passing line [" << __LINE__ << "] in No." << clock() << "ms" << endl
# define debug(...) fprintf(stderr, __VA_ARGS__)
# define show(x) cerr << #x << " = " << (x) << endl
#else
# define passing() do if (0) cerr << "passing line [" << __LINE__ << "] in No." << clock() << "ms" << endl; while(0)
# define debug(...) do if (0) fprintf(stderr, __VA_ARGS__); while(0)
# define show(x) do if (0) cerr << #x << " = " << (x) << endl; while(0)
#endif

const int N = 1e6 + 7;

bool isnp[N];
int primes[N], pcnt;

void init_primes(int n) {
    for (int i = 2; i <= n; ++i)
        if (!isnp[i]) {
            primes[++pcnt] = i;
            for (int j = (i << 1); j <= n; j += i)
                isnp[j] = true;
        }
}

int calc_d(int n) {
    int d = 1;
    for (int i = 1, cnt; primes[i] * primes[i] <= n; ++i) {
        for (cnt = 0; n % primes[i] == 0; ++cnt) n /= primes[i];
        d *= (cnt + 1);
    }
    if (n != 1) d <<= 1;
    return d;
}

int d0[N];

int n, m;
int a[N];
bool ended[N << 2];
int64 sum[N << 2];

void build(int u, int l, int r) {
    if (l == r) {
        sum[u] = a[l];
        if (sum[u] <= 2) ended[u] = true;
        return;
    }
    int mid = (l + r) >> 1, ls = u << 1, rs = u << 1 | 1;
    build(ls, l, mid);
    build(rs, mid + 1, r);
    sum[u] = sum[ls] + sum[rs];
    ended[u] = ended[ls] && ended[rs];
}

void modify(int u, int l, int r, int ml, int mr) {
    if (ended[u]) return;
    if (l == r) {
        sum[u] = d0[sum[u]];
        if (sum[u] <= 2) ended[u] = true;
        return;
    }
    int mid = (l + r) >> 1, ls = u << 1, rs = u << 1 | 1;
    if (ml <= mid) modify(ls, l, mid, ml, mr);
    if (mr > mid) modify(rs, mid + 1, r, ml, mr);
    sum[u] = sum[ls] + sum[rs];
    ended[u] = ended[ls] && ended[rs];
}

int64 query(int u, int l, int r, int ml, int mr) {
    if (l >= ml && r <= mr) return sum[u];
    int mid = (l + r) >> 1;
    int64 result = 0;
    if (ml <= mid) result = query(u << 1, l, mid, ml, mr);
    if (mr > mid) result += query(u << 1 | 1, mid + 1, r, ml, mr);
    return result;
}

signed main() {
    init_primes(1e4);
    for (int i = 1; i < N; ++i) d0[i] = calc_d(i);
    read >> n >> m;
    for (int i = 1; i <= n; ++i) read >> a[i];
    build(1, 1, n);
    for (int i = 1, opt, l, r; i <= m; ++i) {
        read >> opt >> l >> r;
        if (opt == 1) modify(1, 1, n, l, r);
        else write << query(1, 1, n, l, r) << '\n';
    }
    return 0;
}