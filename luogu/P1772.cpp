/*************************************
 * problem:      P1772 [ZJOI2006]��������.
 * user ID:      63720.
 * user name:    �����Ű�.
 * time:         2019-03-18.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 
/*************************************
 * record ID:    R17361384.
 * time:         52 ms
 * memory:       804 KB
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

int n, m, K, e, d;
bool couldReach[21][101] = {0};
struct Edge {
	int toNode, cost;
	Edge *nextEdge;
	Edge() : nextEdge(NULL) {}
	Edge(int t, int c, Edge *n) : toNode(t), cost(c), nextEdge(n) {}
} *head[21] = {NULL};
bool instack[21] = {0};
int dis[21] = {0};
int f[101] = {0}; 
bool now[21];

void addEdge(int from, int to, int cost)
{
	Edge *tmp = new Edge(to, cost, head[from]);
	head[from] = tmp;
}

int spfa()
{
	memset(instack, 0, sizeof(instack));
	memset(dis, 0x3f, sizeof(dis));
	queue <int> q;
	q.push(1);
	dis[1] = 0;
	instack[1] = 1;
	while (!q.empty()) {
		int top = q.front();
		q.pop();
		instack[top] = 0;
		for (Edge *it = head[top]; it; it = it->nextEdge) {
			if (now[it->toNode] && dis[top] + it->cost < dis[it->toNode]) {
				dis[it->toNode] = dis[top] + it->cost;
				if (!instack[it->toNode]) {
					q.push(it->toNode);
					instack[it->toNode] = 1;
				}
			} 
		}
	}
	return dis[m];
} 

int main()
{
	memset(couldReach, 1, sizeof(couldReach));
	n = read<int>();
	m = read<int>();
	K = read<int>();
	e = read<int>();
	int u, v, c;
	for (int i = 1; i <= e; i++) {
		u = read<int>();
		v = read<int>();
		c = read<int>();
		addEdge(u, v, c);
		addEdge(v, u, c);
	}
	d = read<int>();
	int p, a, b;
	for (int i = 1; i <= d; i++) {
		p = read<int>();
		a = read<int>();
		b = read<int>();
		for (int i = a; i <= b; i++) {
			couldReach[p][i] = 0;
//			printf("set wharf No.%d in day %d : broken.\n", p, i);
		}
	}
	memset(f, 0x3f, sizeof(f));
	f[0] = -K;
	for (int i = 1; i <= n; i++) {
		memset(now, 1, sizeof(now));
		for (int j = i; j >= 1; j--) {
			for (int k = 1; k <= m; k++) {
				now[k] &= couldReach[k][j];
			}
			int nRet = spfa();
			if (nRet == 0x3f3f3f3f) break;
			if (f[i] > f[j - 1] + nRet * (i - j + 1) + K) {
				f[i] = f[j - 1] + nRet * (i - j + 1) + K;
			}
		}
//		printf("f[%d] = %d.\n", i, f[i]);
	}
	write(f[n]);
	return 0;
}
