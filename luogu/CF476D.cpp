/*************************************
 * @problem:      Dreamoon and Sets.
 * @author:       brealid(赵奕).
 * @time:         2021-02-05.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

int n, k;

// template <typename OutputStream>
// void put_vector(OutputStream &out, const vector<int> &b) {
//     for (unsigned i = 0; i < b.size(); ++i)
//         out << b[i] << ' ';
//     out << '\n';
// }

// vector<int> construct(int T) {
//     vector<int> g(4);
//     g[0] = T * 6 - 5;
//     g[1] = T * 6 - 4;
//     g[2] = T * 6 - 3;
//     g[3] = T * 6 - 1;
//     return g;
// }

// vector<int> operator * (const vector<int> &v, int K) {
//     vector<int> g(v.size());
//     for (unsigned i = 0; i < v.size(); ++i)
//         g[i] = v[i] * K;
//     return g;
// }

signed main() {
    kin >> n >> k;
    // kout << (construct(n) * k).back() << '\n';
    // for (int i = 1; i <= n; ++i)
    //     put_vector(kout, construct(i) * k);
    kout << (n * 6 - 1) * k << '\n';
    for (int i = 1; i <= n; ++i)
        kout << (i * 6 - 5) * k << ' ' << (i * 6 - 4) * k << ' ' << (i * 6 - 3) * k << ' ' << (i * 6 - 1) * k << '\n';
    return 0;
}