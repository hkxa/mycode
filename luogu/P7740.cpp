/*************************************
 * @problem:      robot.
 * @author:       brealid.
 * @time:         2021-07-28.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

namespace kmath/* 数学资源库的名字 */ {
    typedef unsigned uint32;
    typedef long long int64;
    typedef unsigned long long uint64;

    template <typename T>
    inline T gcd_BitOptimize_ull(T a, T b) {
        if (!a || !b) return a | b;
        int t = __builtin_ctzll(a | b);
        a >>= __builtin_ctzll(a);
        do {
            b >>= __builtin_ctzll(b);
            if (a > b) swap(a, b);
            b -= a;
        } while(b);
        return a << t;
    }
    template <typename T>
    inline T gcd_BitOptimize_ui(T a, T b) {
        if (!a || !b) return a | b;
        int t = __builtin_ctz(a | b);
        a >>= __builtin_ctz(a);
        do {
            b >>= __builtin_ctz(b);
            if (a > b) swap(a, b);
            b -= a;
        } while(b);
        return a << t;
    }
    template <typename T>
    T gcd_EuclidMethod(T a, T b) {
        return a ? gcd_EuclidMethod(b % a, a) : b;
    }

    template <typename T>
    inline T fmul(T a, T b, T p) {
        T ret = 0;
        while (b) {
            if (b & 1) if ((ret += a) >= p) ret -= p;
            if ((a <<= 1) >= p) a -= p; 
            b >>= 1;
        }
        return ret;
    }
    template <typename T>
    inline T fpow_FmulMethod(T a, T b, T p) {
        T ret = 1;
        while (b) {
            if (b & 1) ret = fmul(ret, a, p);
            a = fmul(a, a, p);
            b >>= 1;
        }
        return ret;
    }
    template <typename T, typename UpTurn = int64>
    inline T fpow(T a, T b, T p) {
        T ret = 1;
        while (b) {
            if (b & 1) ret = (UpTurn)ret * a % p;
            a = (UpTurn)a * a % p;
            b >>= 1;
        }
        return ret;
    }
    template <typename T> inline T gcd(T a, T b) { return gcd_EuclidMethod(a, b); }
    template <> inline int64 gcd(int64 a, int64 b) { return gcd_BitOptimize_ull(a, b); }
    template <> inline uint64 gcd(uint64 a, uint64 b) { return gcd_BitOptimize_ull(a, b); }
    template <> inline int gcd(int a, int b) { return gcd_BitOptimize_ui(a, b); }
    template <> inline uint32 gcd(uint32 a, uint32 b) { return gcd_BitOptimize_ui(a, b); }

    template <typename T>
    inline T lcm(T n, T m) {
        return n / gcd(n, m) * m;
    }

    template<typename Ta, typename Tb, typename ...Targ>
    inline Ta gcd(Ta a, Tb b, Targ ...arg) {
        return gcd(gcd(a, b), arg...);
    }

    template<typename Ta, typename Tb, typename ...Targ>
    inline Ta lcm(Ta a, Tb b, Targ ...arg) {
        return lcm(lcm(a, b), arg...);
    }

    template <typename T>
    void exgcd(T n, T m, T &x, T &y) {
        if (n == 1) x = 1, y = 0;
        else exgcd(m, n % m, y, x), y -= n / m * x;
    }

    template <typename T>
    T inv(T a, T m) {
        T x, y;
        exgcd(a, m, x, y);
        return (x % m + m) % m;
    }

    template <typename T>
    inline T inv_FimaMethod(T a, T m) {
        return fpow(a, m - 2, m);
    }

    template<int N, int64 ModP>
    struct CombinationNumber {
        int64 fac[N + 2], ifac[N + 2];
        CombinationNumber() {
            fac[0] = 1;
            for (int i = 1; i <= N; ++i) fac[i] = fac[i - 1] * i % ModP;
            ifac[N] = inv(fac[N], ModP);
            for (int i = N; i >= 1; --i) ifac[i - 1] = ifac[i] * i % ModP;
        }
        inline int64 C(int n, int m) {
            return fac[n] * ifac[n - m] % ModP * ifac[m] % ModP;
        }
        inline int64 P(int n, int m) {
            return fac[n] * ifac[n - m] % ModP;
        }
    };
};

const int N = 32 + 2, M = 1000 + 5, S = 100 + 5, P = 1e9 + 7;

int n, m, len[M];
char s[M][S];
int status[M][N];
int R[M];
const int popcnt[] = {0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4};

namespace TEST_nLeq16 {
    bool on[16];
    int ok[N];
    bool check() { return n <= 16; }
    void solve() {
        // fprintf(stderr, "solve!\n");
        int mask = (1 << n) - 1;
        int64 ans = 0;
        // fprintf(stderr, "maxR = %d\n", maxR);
        for (int i = 1; i <= mask; ++i) {
            int l(-1), r(0), neg(-1);
            for (int j = 0; j < n; ++j) {
                on[j] = (i >> j) & 1;
                if (on[j]) {
                    neg = -neg;
                    if (!~l) l = j;
                    r = j;
                }
            }
            // fprintf(stderr, "i = %d\n", i);
            int64 current = neg < 0 ? P - 1 : 1;
            for (int rob = 1; rob <= m; ++rob) {
                if (r + R[rob] > n) continue;
                // fprintf(stderr, "rob = %d\n", rob);
                for (int j = 1; j <= r - l; ++j) ok[j] = 9;
                for (int j = r - l + 1; j <= R[rob]; ++j) ok[j] = 15;
                for (int j = R[rob] + 1; j <= R[rob] + r - l; ++j) ok[j] = 9;
                for (int shift = 0; shift <= r - l; ++shift) {
                    if (!on[l + shift]) continue;
                    // for (int j = 1; j <= R[rob]; ++j)
                    //     printf("<= %d\n", status[rob][j]);
                    for (int j = 1; j <= R[rob]; ++j)
                        ok[j + shift] &= status[rob][j];
                }
                for (int j = 1; j <= R[rob] + r - l; ++j)
                    current = current * (popcnt[ok[j]] + 1) % P;
                current = current * kmath::fpow<int64>(3, n - (R[rob] + r - l), P) % P;
            }
            ans = (ans + current) % P;
            // fprintf(stderr, "%d: %lld * %d\n", i, current * (neg + P) % P, neg);
            // fprintf(stdout, "%d: %lld * %d\n", i, current * (neg + P) % P, neg);
        }
        kout << ans << '\n';
    }
}

namespace TEST_noR {
    bool check() {
        for (int i = 1; i <= m; ++i)
            if (R[i] != 1) return false;
        return true;
    }
    void solve() {
        kmath::CombinationNumber<50, P> kc;
        int64 ans = 0;
        for (int have = 1; have <= n; ++have) {
            int64 current = kc.C(n, have);
            if (!(have & 1)) current = P - current;
            for (int i = 1; i <= m; ++i)
                current = current *
                          kmath::fpow<int64>(popcnt[status[i][1] & (have == 1 ? 15 : 9)] + 1, have, P) % P *
                          kmath::fpow<int64>(3, n - have, P) % P;
            ans = (ans + current) % P;
            // printf("%d: %lld\n", have, current * ((have & 1) ? 1 : P - 1) % P);
        }
        // 225 - 15 + 1
        kout << ans << '\n';
    }
}

signed main() {
    kin >> n >> m;
    for (int i = 1; i <= m; ++i) {
        kin >> (s[i] + 1);
        len[i] = strlen(s[i] + 1);
        char now_status = '-';
        for (int j = 1; j <= len[i]; ++j)
            if (s[i][j] == 'R') {
                if (now_status == '-') status[i][++R[i]] = 9;
                else if (now_status == '*') status[i][++R[i]] = 6;
                else if (now_status == '0') status[i][++R[i]] = 10;
                else status[i][++R[i]] = 5;
                if (R[i] >= 32) {
                    --i;
                    goto MainFor_End;
                }
                now_status = '-';
            } else if (s[i][j] != '*') now_status = s[i][j];
            else if (now_status == '-') now_status = '*';
            else if (now_status == '*') now_status = '-';
            else now_status ^= 1;
        if (now_status == '-') status[i][++R[i]] = 9;
        else if (now_status == '*') status[i][++R[i]] = 6;
        else if (now_status == '0') status[i][++R[i]] = 10;
        else status[i][++R[i]] = 5;
        MainFor_End:;
    }
#ifdef Force_TEST_noR
    if (TEST_noR::check()) TEST_noR::solve();
    return 0;
#endif
    if (TEST_nLeq16::check()) TEST_nLeq16::solve();
    else if (TEST_noR::check()) TEST_noR::solve();
    return 0;
}

/*
   --  00  01  10  11
-   1   1   0   0   1   [9]
*   1   0   1   1   0   [6]
0   1   1   0   1   0   [10]
1   1   0   1   0   1   [5]
*/