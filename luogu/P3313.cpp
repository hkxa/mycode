/*************************************
 * @problem:      [SDOI2014]旅行.
 * @user_name:    brealid.
 * @time:         2020-11-13.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

const int N = 1e5 + 7;

int n, q;
int c[N], w[N];

enum OptionType { CC, CW, QS, QM };
OptionType get_option() {
    static char buffer[3];
    scanf("%s", buffer);
    if (buffer[0] == 'C') return buffer[1] == 'C' ? CC : CW;
    else return buffer[1] == 'S' ? QS : QM;
}

struct segtree_node {
    int ls, rs;
    int sum, mx;
    void clear() { ls = rs = sum = mx = 0; }
} tr[N << 5];

void pushup(int u) {
    tr[u].sum = tr[tr[u].ls].sum + tr[tr[u].rs].sum;
    tr[u].mx = max(tr[tr[u].ls].mx, tr[tr[u].rs].mx);
}

class MemoryPool {
private:
    int pool_stack[N << 3], stack_top, unused_node;
public:
    MemoryPool() : stack_top(0), unused_node(0) {}
    void recycle(int node) {
        pool_stack[++stack_top] = node;
        tr[node].clear();
    }
    int get() {
        return stack_top ? pool_stack[stack_top--] : ++unused_node;
    }
} pool;

class segment_tree {
private:
    int root;
    void BASE_insert_or_modify(int &u, int l, int r, int pos, int val) {
        if (!u) u = pool.get();
        if (l == r) {
            tr[u].mx = tr[u].sum = val;
            return;
        }
        int mid = (l + r) >> 1;
        if (pos <= mid) BASE_insert_or_modify(tr[u].ls, l, mid, pos, val);
        else BASE_insert_or_modify(tr[u].rs, mid + 1, r, pos, val);
        pushup(u);
    }
    void BASE_erase(int &u, int l, int r, int pos) {
        if (l == r) {
            pool.recycle(u);
            u = 0;
            return;
        }
        int mid = (l + r) >> 1;
        if (pos <= mid) BASE_erase(tr[u].ls, l, mid, pos);
        else BASE_erase(tr[u].rs, mid + 1, r, pos);
        if (!tr[u].ls && !tr[u].rs) {
            pool.recycle(u);
            u = 0;
        } else pushup(u);
    }
    int BASE_query_sum(int u, int l, int r, int ml, int mr) {
        if (!u || l > mr || r < ml) return 0;
        if (l >= ml && r <= mr) return tr[u].sum;
        int mid = (l + r) >> 1;
        return BASE_query_sum(tr[u].ls, l, mid, ml, mr) + BASE_query_sum(tr[u].rs, mid + 1, r, ml, mr);
    }
    int BASE_query_max(int u, int l, int r, int ml, int mr) {
        if (!u || l > mr || r < ml) return 0;
        if (l >= ml && r <= mr) return tr[u].mx;
        int mid = (l + r) >> 1;
        return max(BASE_query_max(tr[u].ls, l, mid, ml, mr), BASE_query_max(tr[u].rs, mid + 1, r, ml, mr));
    }
public:
    segment_tree() : root(0) {}
    void insert(int pos, int val) { BASE_insert_or_modify(root, 1, n, pos, val); }
    void modify(int pos, int val) { BASE_insert_or_modify(root, 1, n, pos, val); }
    void erase(int pos) { BASE_erase(root, 1, n, pos); }
    int query_sum(int l, int r) { return BASE_query_sum(root, 1, n, l, r); }
    int query_max(int l, int r) { return BASE_query_max(root, 1, n, l, r); }
} segt[N];

class Graph {
private:
    vector<int> adj[N];
    int fa[N], siz[N], wson[N], dep[N], beg[N], dfn[N], dft;
    void Initial_Dfs(int u, int father) {
        fa[u] = father;
        dep[u] = dep[father] + 1;
        siz[u] = 1;
        for (size_t i = 0; i < adj[u].size(); ++i) {
            int &v = adj[u][i];
            if (v != father) {
                Initial_Dfs(v, u);
                siz[u] += siz[v];
                if (siz[v] > siz[wson[u]]) wson[u] = v;
            }
        }
    }
    void TreeChainSplit_Dfs(int u, int chain_beg) {
        dfn[u] = ++dft;
        beg[u] = chain_beg;
        segt[c[u]].insert(dfn[u], w[u]);
        if (!wson[u]) return;
        TreeChainSplit_Dfs(wson[u], chain_beg);
        for (size_t i = 0; i < adj[u].size(); ++i) {
            int &v = adj[u][i];
            if (v != fa[u] && v != wson[u]) TreeChainSplit_Dfs(v, v);
        }
    }
public:
    Graph() : dft(0) {}
    void link(int u, int v) {
        adj[u].push_back(v);
        adj[v].push_back(u);
    }
    void init() {
        Initial_Dfs(1, 0);
        TreeChainSplit_Dfs(1, 1);
    }
    void modify_religion(int u, int new_c) {
        segt[c[u]].erase(dfn[u]);
        segt[new_c].insert(dfn[u], w[u]);
        c[u] = new_c;
    }
    void modify_rating(int u, int new_w) {
        segt[c[u]].modify(dfn[u], new_w);
        w[u] = new_w;
    }
    int query_sum(int u, int v) {
        int religion = c[u], sum = 0;
        while (beg[u] != beg[v]) {
            if (dep[beg[u]] < dep[beg[v]]) swap(u, v);
            sum += segt[religion].query_sum(dfn[beg[u]], dfn[u]);
            u = fa[beg[u]];
        }
        if (dep[u] < dep[v]) swap(u, v);
        sum += segt[religion].query_sum(dfn[v], dfn[u]);
        return sum;
    }
    int query_max(int u, int v) {
        int religion = c[u], maxv = 0;
        while (beg[u] != beg[v]) {
            if (dep[beg[u]] < dep[beg[v]]) swap(u, v);
            maxv = max(maxv, segt[religion].query_max(dfn[beg[u]], dfn[u]));
            u = fa[beg[u]];
        }
        if (dep[u] < dep[v]) swap(u, v);
        maxv = max(maxv, segt[religion].query_max(dfn[v], dfn[u]));
        return maxv;
    }
} G;

signed main() {
    read >> n >> q;
    for (int i = 1; i <= n; ++i) read >> w[i] >> c[i];
    for (int i = 1, x, y; i < n; ++i) {
        read >> x >> y;
        G.link(x, y);
    }
    G.init();
    for (int i = 1, opt, x, y; i <= q; ++i) {
        opt = get_option();
        read >> x >> y;
        switch (opt) {
            case CC : G.modify_religion(x, y); break;
            case CW : G.modify_rating(x, y); break;
            case QS : write << G.query_sum(x, y) << '\n'; break;
            case QM : write << G.query_max(x, y) << '\n'; break;
        }
    }
    return 0;
}