//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      【模板】插头dp.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-22.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// #define int int64

int n, m, EndX, EndY;
int status[13][13];
int64 f[2][41835 + 7], *las, *now;
int state[41835 + 7], nxt[13][41835 + 7][4], cnt, mid;
map<int, int> revState;

const int LefPlug = 1, RigPlug = 2, NonePlug = 0;

string get_status(int k) { // For Debug
    string ret = "";
    for (int i = m; i >= 0; i--) 
        switch ((k >> (i << 1)) & 3) {
            case 3 : ret += '3'; break;
            case 2 : ret += '2'; break;
            case 1 : ret += '1'; break;
            case 0 : ret += '0'; break;
        }
    return ret;
}

inline void GetEndXY() {
    for (int i = 1; i <= n; i++)
        for (int j = 1; j <= m; j++)
            if (status[i][j]) {
                EndX = i;
                EndY = j;
            }
}

void PretreatExist(int m, int s) {
    bool flag;
    revState.clear();
    cnt = 0;
    for (int i = 0, j, c; i < s; i++) {
        if (i == (1 << m)) mid = cnt;
        c = 0;
        j = m;
        flag = 1;
        while (j >= 0 && flag) {
            switch ((i >> j) & 3) {
                case 1 : 
                    c++;
                    break;
                case 2 : 
                    c--;
                    if (c < 0) flag = 0;
                    break;
                case 3 :
                    flag = 0;
                    break;
            }
            j -= 2;
        }
        if (c) flag = 0;
        if (flag) {
            state[++cnt] = i;
            revState[i] = cnt;
        }
    }
}

inline void AddNxt(int &pos, const int conti) {
    if (revState.find(conti) != revState.end())
        pos = revState[conti];
}

inline void PretreatNxt() {
    int up, lef, remain, pos, CntPlug;
    memset(nxt, 0, sizeof(nxt));
    for (int j = 1; j <= m; j++) {
        for (int _k = 1, k; _k <= cnt; _k++) {
            k = state[_k];
            remain = k & ~(15 << ((m - j) << 1));
            up = (k >> ((m - j) << 1)) & 3;
            lef = (k >> ((m - j + 1) << 1)) & 3;
            // [不可铺线]格
            if (up == NonePlug && lef == NonePlug)
                AddNxt(nxt[j][_k][2], k);
            // [一定铺线]格
            switch (lef * 3 + up) {
                case 0 : // lef : NonePlug; up : NonePlug
                    AddNxt(nxt[j][_k][0], remain | (6 << ((m - j) << 1)));
                    break;
                case 1 : // lef : NonePlug; up : LefPlug
                case 2 : // lef : NonePlug; up : RigPlug
                    AddNxt(nxt[j][_k][0], remain | (up << ((m - j) << 1)));
                    AddNxt(nxt[j][_k][1], remain | (up << ((m - j + 1) << 1)));
                    break;
                case 3 : // lef : LefPlug; up : NonePlug
                case 6 : // lef : RigPlug; up : NonePlug
                    AddNxt(nxt[j][_k][0], remain | (lef << ((m - j) << 1)));
                    AddNxt(nxt[j][_k][1], remain | (lef << ((m - j + 1) << 1)));
                    break;
                case 4 : // lef : LefPlug; up : LefPlug
                    pos = (m - j) << 1;
                    CntPlug = 0;
                    do {
                        pos -= 2;
                        if (((k >> pos) & 3) == LefPlug) CntPlug++;
                        else if (((k >> pos) & 3) == RigPlug) CntPlug--;
                    } while (~CntPlug);
                    AddNxt(nxt[j][_k][0], (remain & ~(3 << pos)) | (LefPlug << pos));
                    break;
                case 8 : // lef : RigPlug; up : RigPlug
                    pos = (m - j + 1) << 1;
                    CntPlug = 0;
                    do {
                        pos += 2;
                        if (((k >> pos) & 3) == LefPlug) CntPlug--;
                        else if (((k >> pos) & 3) == RigPlug) CntPlug++;
                    } while (~CntPlug);
                    AddNxt(nxt[j][_k][0], (remain & ~(3 << pos)) | (RigPlug << pos));
                    break;
                case 5 : // lef : LefPlug; up : RigPlug
                    AddNxt(nxt[j][_k][0], remain);
                    break;
                case 7 : // lef : RigPlug; up : LefPlug
                    AddNxt(nxt[j][_k][0], remain);
                    break;
            }
        }
    }
}

inline void AddVal(const int state, const int64 &difVal) {
    if (revState.find(state) != revState.end())
        now[revState[state]] += difVal;
}

signed _main() {
    memset(f, 0, sizeof(f));
    n = read<int>();
    m = read<int>();
    for (int i = 1; i <= n; i++) 
        for (int j = 1; j <= m; j++)
            status[i][j] = read<int>();
    GetEndXY();
    las = f[0];
    now = f[1];
    int statusAll = 1 << ((m + 1) << 1);
    PretreatExist(m << 1, statusAll);
    PretreatNxt();
    las[1] = 1;
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++) {
            for (int k = 1; k <= cnt; k++) {
                if (!status[i][j]) {
                    if (nxt[j][k][2]) now[nxt[j][k][2]] += las[k];
                } else {
                    if (nxt[j][k][0]) now[nxt[j][k][0]] += las[k];
                    if (nxt[j][k][1]) now[nxt[j][k][1]] += las[k];
                }
            }
            swap(las, now);
            if (i == EndX && j == EndY) {
                write(las[1], 10);
                return 0;
            }
            for (int k = 1; k <= cnt; k++) now[k] = 0;
        }
        for (int k = 1; k <= mid; k++) las[k] = las[revState[state[k] << 2]];
        for (int k = mid + 1; k <= cnt; k++) las[k] = 0;
    }
    return 0;
}

signed main() {
    int T = read<int>();
    while (T--) _main();
    return 0;
}

// Create File Date : 2020-06-22