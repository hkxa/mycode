/*************************************
 * @problem:      Tree.
 * @user_name:    brealid/hkxadpall/zhaoyi20/j(o)moo/littleTortoise.
 * @time:         2020-09-22.
 * @language:     C++.
 * @fastio_ver:   20200913.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0,    // input
        flush_stdout = 1 << 1,  // output
        flush_stderr = 1 << 2,  // output
    };
    enum number_type_flags {
        output_double_stable = 1 << 0,  // output
        output_double_faster = 1 << 1   // output
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f & ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore character set = {' ', '\r', '\n', '\t'}
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (double &lf) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            lf = endch & 15;
            while (isdigit(endch = getchar())) lf = lf * 10 + (endch & 15);
            if (endch == '.') {
                double digit = 0.1;
                while (isdigit(endch = getchar())) {
                    lf += (endch & 15) * digit;
                    digit *= 0.1;
                }
            }
            if (flag) lf = -lf;
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            Int d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return d;
        }
    } read;

    struct Writer {
        Writer& operator << (io_flags f) {
            if (f & flush_stdout) fflush(stdout);
            if (f & flush_stderr) fflush(stderr);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
        inline void operator () (double val, int eps_digit = 6, char endch = '\0', number_type_flags flg = output_double_faster) {
            if (flg & output_double_stable) {
                static char output_format[10];
                sprintf(output_format, "%%.%dlf", eps_digit);
                printf(output_format, val);
            } else if (flg & output_double_faster) {
                if (val < 0) {
                    putchar('-');
                    val = -val;
                }
                double eps_number = 0.5;
                for (int i = 1; i <= eps_digit; i++) eps_number /= 10;
                val += eps_number;
                (*this) << (int64)val;
                val -= (int64)val;
                if (eps_digit) putchar('.');
                while (eps_digit--) {
                    val *= 10;
                    putchar((int)val | '0');
                    val -= (int)val;
                }
            } else {
               (*this) << '1';
                if (eps_digit) {
                    (*this) << ".E";
                    for (int i = 2; i <= eps_digit; i++) (*this) << '0';
                }
            }
            if (endch) putchar(endch);
        }
    } write;
}
using namespace Fastio;

namespace File_IO {
    void init_IO(const char *file_name) {
        char buff[107];
        sprintf(buff, "%s.in", file_name);
        freopen(buff, "r", stdin);
        sprintf(buff, "%s.out", file_name);
        freopen(buff, "w", stdout);
    }
}

// #define int int64

const int N = 700 + 3, DigByte = 100000000;

struct Bint {
    uint64 atom[17];
    int len;
    Bint(int number = 0) : len(0) {
        while (number) {
            atom[len++] = number % DigByte;
            number /= DigByte;
        }
    }
    Bint operator * (const Bint &b) const {
        // print(); printf(" * "); b.print('\n');
        Bint ret;
        memset(ret.atom, 0, sizeof(ret.atom));
        ret.len = len + b.len;
        for (int i = 0; i < len; i++)
            for (int j = 0; j < b.len; j++)
                ret.atom[i + j] += atom[i] * b.atom[j];
        for (int i = 1; i < ret.len; i++) {
            ret.atom[i] += ret.atom[i - 1] / DigByte;
            ret.atom[i - 1] %= DigByte;
        }
        if (!ret.atom[ret.len - 1]) --ret.len;
        return ret;
    }
    bool operator < (const Bint &b) const {
        if (len ^ b.len) return len < b.len;
        for (int i = len - 1; i >= 0; i--)
            if (atom[i] ^ b.atom[i]) return atom[i] < b.atom[i];
        return false;
    }
    void print(char endch = '\0') const {
        if (len <= 0) {
            putchar('0');
            if (endch) putchar(endch);
            return;
        }
        printf("%llu", atom[len - 1]);
        int i = len - 2;
        while (i >= 0) printf("%08llu", atom[i--]);
        if (endch) putchar(endch);
    }
};

int n;
Bint f[N][N], g[N];
vector<int> G[N];
int siz[N];

void sol(int u, int fa) {
    f[u][1] = f[u][0] = siz[u] = 1;
    for (size_t eid = 0; eid < G[u].size(); eid++) {
        int v = G[u][eid];
        if (v == fa) continue;
        sol(v, u);
        for (int i = 0; i <= siz[u] + siz[v]; i++)
            g[i] = 0;
        for (int i = 0; i <= siz[v]; i++)
            for (int j = 0; j <= siz[u]; j++)
                g[i + j] = max(g[i + j], f[u][j] * f[v][i]);
        siz[u] += siz[v];
        for (int i = 0; i <= siz[u]; i++)
            f[u][i] = g[i];
    }
    for (int i = 1; i <= siz[u]; i++)
        f[u][0] = max(f[u][0], f[u][i] * i);
}

signed main() {
    // File_IO::init_IO("Tree");
    read >> n;
    for (int i = 1, u, v; i < n; i++) {
        read >> u >> v;
        G[u].push_back(v);
        G[v].push_back(u);
    }
    sol(1, -1);
    f[1][0].print('\n');
    return 0;
}