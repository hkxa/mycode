/*************************************
 * problem:      P1484 ����.
 * user ID:      63720.
 * user name:    �����Ű�.
 * time:         2019-02-24.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 
/*************************************
 * record ID:    R16623668 .
 * status:       AC 100 pts.
 * time:         709 ms
 * memory:       8332 KB
 * ststus - AC:  10 blocks, 100 pts.
 * ststus - PC:  0 blocks, 0 pts.
 * ststus - WA:  0 blocks, 0 pts.
 * ststus - RE:  0 blocks, 0 pts. 
 * ststus - TLE: 0 blocks, 0 pts. 
 * ststus - MLE: 0 blocks, 0 pts. 
 * ststus - OLE: 0 blocks, 0 pts. 
*************************************/ 
#include <bits/stdc++.h>
#define swap(a, b) { typeof(a) t = a; a = b; b = t; }
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
//    printf("I-R*BT = %d*%d(equal %d)\n", init, flag, init * flag);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

int n, k;
int a[500007] = {0};
bool vis[500007] = {0};
int l[500007], r[500007];

struct cmp {
	bool operator () (int x, int y) 
	{
		return a[x] < a[y];
	}
};

priority_queue <int, vector<int>, cmp> q;

int main()
{
    n = read<int>();
    k = read<int>();
    for (int i = 1; i <= n; i++) {
    	a[i] = read<int>();
    	l[i] = i - 1;
    	r[i] = i + 1;
    	q.push(i);
	}
	long long ans = 0;
	while (k--) {
		while (vis[q.top()]) {
			q.pop();
		}
		int x = q.top();
		q.pop();
		if (a[x] <= 0) {
			break;
		}
		ans += a[x];
		vis[l[x]] = vis[r[x]] = 1;
		a[x] = a[l[x]] + a[r[x]] - a[x];
		q.push(x);
		l[x] = l[l[x]];
		r[x] = r[r[x]];
		l[r[x]] = x;
		r[l[x]] = x;
	}
	write<long long>(ans);
	putchar(10);
	return 0;
}
