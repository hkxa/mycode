/*************************************
 * @problem:      P5889 跳树.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-01-04.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, m, q;
int a[500007];
int op[500007], s[500007], x[500007], y[500007];

int GoCase1()
{
    for (int i = 1; i <= q; i++) {
        if (op[i] == 1) {
            for (int j = x[i]; j <= y[i]; j++) {
                switch (a[j]) {
                    case 1 : s[i] = s[i] * 2; break;
                    case 2 : s[i] = s[i] * 2 + 1; break;
                    case 3 : s[i] = max(s[i] / 2, 1); break;
                }
            }
            write(s[i], 10);
        } else {
            a[x[i]] = y[i];
        }
    }
    return 0;
}

int ans[500007];

int GoCase2()
{
    for (int i = 1; i <= m; i++) {
        switch (a[i]) {
            case 1 : ans[i] = ans[i - 1] * 2; break;
            case 2 : ans[i] = ans[i - 1] * 2 + 1; break;
            case 3 : ans[i] = max(ans[i - 1] / 2, 1); break;
        }
    }
    for (int i = 1; i <= q; i++) {
        write(ans[y[i]], 10);
    }
    return 0;
}

int main()
{
    n = read<int>();
    m = read<int>();
    q = read<int>();
    for (int i = 1; i <= m; i++) a[i] = read<int>();
    bool AllType1 = true, Alls1 = true, Allx1 = true;
    for (int i = 1; i <= q; i++) {
        op[i] = read<int>();
        if (op[i] == 1) {
            s[i] = read<int>();
            x[i] = read<int>();
            y[i] = read<int>();
            if (s[i] != 1) Alls1 = false;
            if (x[i] != 1) Allx1 = false;
        } else {
            x[i] = read<int>();
            y[i] = read<int>();
            AllType1 = false;
        }
    }
    if (m <= 2000 && q <= 2000) return GoCase1();
    if (AllType1 && Alls1 && Allx1) return GoCase2();
    GoCase1();
    return 0;
}