/*************************************
 * problem:      P1433 吃奶酪.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-28.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define puts_return(x) { puts(x); return 0; }
#define write_return(x) { write(x); return 0; }

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;
bool vis[1001]; 
double x[15 + 3], y[15 + 3];
double ans = 1e10; 

#define p2(x) ((x) * (x))
#define dis(i, j) sqrt(p2(x[i] - x[j]) + p2(y[i] - y[j]))

void dfs(int step, int now, double cnt)
{
    if (cnt > ans) return; 
    if (step == n) {
        ans = cnt;
        return;
    }
    for (int i = 1; i <= n; i++) {
        if (!vis[i]) {
            vis[i] = 1;
            dfs(step + 1, i, cnt + dis(now, i));  
            vis[i] = 0; 
        }
    }
}

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        scanf("%lf%lf", x + i, y + i);
    }
    x[0] = y[0] = 0; 
    dfs(0, 0, 0);  
    printf("%.2f", ans);
    return 0;
}