/*************************************
 * @problem:      CF1303C Perfect Keyboard.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-02-16.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}
// #define int long long
int n, len;
char S[207];
bool m[26][26];
int cnt;
int status[26]; // 0 for not vis, 1 for [count = 1] (start point), 2 for vis, 3 for no edge
bool fail;

void dfs(int u)
{
    putchar(u + 'a');
    status[u] = 2;
    for (int i = 0; i < 26; i++)
        if (m[u][i] && status[i] < 2) {
            dfs(i);
        }
}

signed main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        scanf("%s", S);
        len = strlen(S);
        memset(m, 0, sizeof(m));
        memset(status, 0, sizeof(status));
        for (int i = 1; i < len; i++) {
            m[S[i - 1] - 'a'][S[i] - 'a'] = true;
            m[S[i] - 'a'][S[i - 1] - 'a'] = true;
        }
        for (int i = 0; i < 26; i++) {
            cnt = 0;
            for (int j = 0; j < 26; j++) {
                if (m[i][j]) cnt++;
            }
            if (cnt > 2) {
                puts("NO");
                fail = true;
                break;
            }
            if (cnt == 1) {
                status[i] = 1;
            }
            if (cnt == 0) {
                status[i] = 3;
            }
            // printf("%c : status = %d.\n", i + 'a', status[i]);
        }
        // if it a 闭环
        if (fail) {
            fail = false;
            continue;
        }
        puts("YES");
        for (int i = 0; i < 26; i++) {
            if (status[i] == 1) dfs(i);
            if (status[i] == 3) putchar(i + 'a');
        }
        puts("");
    }
    return 0;
}

// a--c--o--d--e