//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Mice and Holes.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-13.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

namespace against_cpp11 {
    const int N = 5e3 + 7;
    int n, m;
    struct mouse_and_hole {
        int x, y;
        bool operator < (const mouse_and_hole &b) const {
            return x < b.x;
        }
    } p[N * 2];
    vector<int> hole[N];
    struct st {
        int64 w;
        int id;
        bool operator < (const st &b) const {
            return w > b.w;
        }
    };
    priority_queue<int64, vector<int64>, greater<int64> > q1;
    priority_queue<st> q2;
    int64 w, ans;
    int count_hole_room = 0;
    signed main() {
        read >> n >> m;
        for (int i = 1; i <= n; i++) {
            read >> p[i].x;
            p[i].y = -1;
        }
        for (int i = 1; i <= m; i++) {
            read >> p[i + n].x >> p[i + n].y;
            count_hole_room += p[i + n].y;
        }
        if (count_hole_room < n) {
            puts("-1");
            return 0;
        }
        n += m;
        sort(p + 1, p + n + 1);
        for (int i = 1, k; i <= n; i++)
            if (p[i].y == -1) {
                w = INT_MAX;
                if (!q2.empty()) {
                    k = q2.top().id;
                    w = p[i].x + q2.top().w; 
                    q2.pop();
                    if (p[k].y)
                        p[k].y--, q2.push((st){-p[k].x, k});
                }
                ans += w;
                q1.push(-p[i].x - w);
            } else {
                while (p[i].y && !q1.empty() && p[i].x + q1.top() < 0) {
                    p[i].y--;
                    w = p[i].x + q1.top();
                    q1.pop();
                    ans += w;
                    q2.push((st){-p[i].x - w, 0});
                }
                if (p[i].y)
                    q2.push((st){-p[i].x, i}), p[i].y--;
            }
        write << ans << '\n';
        return 0;
    }
}

signed main() { return against_cpp11::main(); }