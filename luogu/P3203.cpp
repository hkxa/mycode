//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      P3690 【模板】Link Cut Tree （动态树）.
 * @user_id:      63720.
 * @user_name:    brealid.
 * @time:         2020-05-27.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;
// #define int int64

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// #define INFO_PUSHDOWN_ALL
// #define INFO_ROTATE
// #define INFO_SPLAY
// #define INFO_ACCESS
// #define INFO_FINDROOT
// #define INFO_MAKEROOT
// #define INFO_PREFER
// #define INFO_LINK
// #define INFO_CUT
// #define INFO_OPTION
// #define INFO_AFTER_DEALING

const int N = 200000 + 7;

#define lc son[u][0]
#define rc son[u][1]

int f[N];
int son[N][2];
bool rev[N];

inline bool isroot(int);        // [splay 中] 判断一个节点是否为 root
inline bool which(int);         // [splay 中] 判断一个节点是父亲节点的哪个孩子
inline void reverse(int);       // [splay 中] 懒标记地翻转左右孩子
inline void pushdown(int);      // [splay 中] 下传翻转子树的懒标记
inline void pushdown_all(int);  // [splay 中] 对 splay 的根节点到 u 的所有节点进行 pushdown 操作
inline void pushup(int);        // [splay 中] 更新 s[u] (splay 子树的 xor 和)
inline void rotate(int);        // [splay 中] 旋转操作
inline void splay(int);         // [splay 中] 使节点成为所在 splay 的根
inline void access(int);        // [原树 中] 使原树根节点到 u 的路径成为 Preferred Path
inline void makeroot(int);      // [原树 中] 使节点成为所在原树的根
inline int findroot(int);       // [原树 中] 返回所在原树的根
inline void prefer(int, int);   // [原树 中] 使原树 u 到 v 的路径成为 Preferred Path
inline void link(int, int);     // [原树 中] 若 u, v 尚未连接，连接 u, v
inline void cut(int, int);      // [原树 中] 若 u, v 已有连接，断开 u, v

int n, m;
int k[N];
int opt, x, siz[N];

inline bool isroot(int u) { return !f[u] || (u != son[f[u]][0] && u != son[f[u]][1]); }
inline bool which(int u) { return u == son[f[u]][1]; }
inline void reverse(int u) { swap(lc, rc); rev[u] ^= 1; }
inline void pushup(int u) { siz[u] = siz[lc] + siz[rc] + 1; }
inline void pushdown(int u) { if (rev[u]) { reverse(lc); reverse(rc); rev[u] = 0; } }

// inline void pushdown_all(int u) { if (!isroot(u)) { pushdown_all(f[u]); } pushdown(u); }
inline void pushdown_all(int u) { 
    static int s[N], cnt;
    cnt = 0;
#ifdef INFO_PUSHDOWN_ALL
    printf("a. ");
#endif
    while (!isroot(u)) { 
        s[++cnt] = u;
        u = f[u];
    } 
    s[++cnt] = u;
#ifdef INFO_PUSHDOWN_ALL
    printf("p.\n");
#endif
    while (cnt) pushdown(s[cnt--]);
}

inline void rotate(int u) {
#ifdef INFO_ROTATE
    printf("rotate(%d)\n", u);
#endif
    int v = f[u], w = f[v], d = which(u);
    if (!isroot(v)) son[w][which(v)] = u;
    f[son[u][!d]] = v;
    son[v][d] = son[u][!d];
    son[u][!d] = v;
    f[u] = w;
    f[v] = u;
    pushup(v);
    pushup(u);
}

inline void splay(int u) {
#ifdef INFO_SPLAY
    printf("splay(%d)\n", u);
#endif
    pushdown_all(u);
    for (int v = f[u]; !isroot(u); v = f[u]) {
        if (!isroot(v)) rotate(which(u) ^ which(v) ? u : v);
        rotate(u);
    }
    pushup(u);
}
inline void access(int u) { 
#ifdef INFO_ACCESS
    printf("access(%d)\n", u);
#endif
    for (int v = 0; u; u = f[v = u]) { 
        splay(u); 
        rc = v; 
        pushup(u); 
    } 
}

inline int findroot(int u) { 
#ifdef INFO_FINDROOT
    printf("findroot(%d)\n", u);
#endif
    access(u); 
    splay(u); 
    while (lc) { 
        pushdown(u); 
        u = lc; 
    } 
    splay(u); 
    return u; 
}

inline void makeroot(int u) { 
#ifdef INFO_MAKEROOT
    printf("makeroot(%d)\n", u); 
#endif
    access(u); 
    splay(u); 
    reverse(u); 
}

inline void prefer(int u, int v) { 
#ifdef INFO_PREFER
    printf("prefer(%d, %d)\n", u, v); 
#endif
    makeroot(u); 
    access(v); 
    splay(v);
}

inline void link(int u, int v) {
#ifdef INFO_LINK
    printf("link(%d, %d)\n", u, v); 
#endif
    makeroot(u); 
    if (findroot(v) != u) f[u] = v; 
}

inline void cut(int u, int v) { 
#ifdef INFO_CUT
    printf("cut(%d, %d)\n", u, v);
#endif
    makeroot(u); 
    if (findroot(v) == u && f[v] == u && !lc) { 
        f[v] = rc = 0; 
        pushup(u); 
    } 
}

signed main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        k[i] = min(i + read<int>(), n + 1);
        // printf("treating edge %d -> %d\n", i, k[i]);
        link(i, k[i]);
        siz[i] = 1;
    }
    siz[n + 1] = 1;
    m = read<int>();
    for (int i = 1; i <= m; i++) {
        // printf("treating question %d\n", i);
        opt = read<int>();
        x = read<int>() + 1;
        if (opt == 1) {
            prefer(x, n + 1);
            write(siz[n + 1] - 1, 10);
        } else {
            // printf("cutting edge %d -> %d\n", x, k[x]);
            cut(x, k[x]);
            k[x] = min(x + read<int>(), n + 1);
            // printf("linking edge %d -> %d\n", x, k[x]);
            link(x, k[x]);
        }
    }
    return 0;
}