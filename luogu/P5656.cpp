//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      【模板】二元一次不定方程(exgcd).
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-23.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

struct Reader {
    template <typename Int>
    Reader& operator >> (Int &i) {
        i = read<int>();
        return *this;
    }
    Reader& operator << (const char ch) {
        putchar(ch);
        return *this;
    }
    template <typename Int>
    Reader& operator << (const Int i) {
        write(i);
        return *this;
    }
} fastio;

int64 gcd(int64 a, int64 b) {
    return b ? gcd(b, a % b) : a;
}

void exgcd(int64 a, int64 b, int64 &x, int64 &y) {
    if (!b) {
        x = 1;
        y = 0;
        return;
    }
    exgcd(b, a % b, y, x);
    y -= x * (a / b);
}

signed main() {
    int T;
    int64 a, b, c, g;
    int64 x, y, x_min, x_max, y_max, y_min, sol_cnt;
    fastio >> T;
    while (T--) {
        fastio >> a >> b >> c;
        g = gcd(a, b);
        if (c % g) {
            // No solution
            fastio << -1 << '\n';
        } else {
            a /= g; b /= g; c /= g;
            exgcd(a, b, x, y);
            x *= c;
            y *= c;
            x_min = (x % b + b - 1) % b + 1;
            y_max = y + (x - x_min) / b * a;
            if (y_max > 0) {
                y_min = (y_max - 1) % a + 1;
                sol_cnt = (y_max - y_min) / a;
                x_max = x_min + sol_cnt * b;
                fastio << sol_cnt + 1 << ' ' << x_min << ' ' << y_min << ' ' << x_max << ' ' << y_max << '\n';
            } else {
                y_min = (y_max % a + a - 1) % a + 1;
                fastio << x_min << ' ' << y_min << '\n';
            }
        }
    }
    return 0;
}

// Create File Date : 2020-06-23