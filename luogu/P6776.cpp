//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      surreal.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-09-10.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

template<typename T>
struct dynamic_array {
    typedef T                                       value_type;
    typedef size_t                                  size_type;
    typedef value_type*                             iterator;
    typedef const value_type*                       const_iterator;
    typedef value_type&                             reference;
    typedef const value_type&                       const_reference;
    typedef std::reverse_iterator<iterator>         reverse_iterator;
    typedef const std::reverse_iterator<iterator>   const_reverse_iterator;
    value_type *pStart, *pFinish, *pEnd_of_Storage;
    dynamic_array() {
        pFinish = pStart = new value_type[4];
        pEnd_of_Storage = pStart + 4;
    }
    template<typename Init_Iterator>
    dynamic_array(Init_Iterator x, Init_Iterator y) {
        pFinish = pStart = new value_type[4];
        pEnd_of_Storage = pStart + 4;
        while (x != y) {
            if (pFinish == pEnd_of_Storage) resize_memory((pEnd_of_Storage - pStart) << 1);
            *(pFinish++) = *(x++);
        }
    }  
    dynamic_array(const dynamic_array<value_type> &from) {
        pFinish = pStart = new value_type[from.max_size()];
        pEnd_of_Storage = pStart + from.max_size();
        while (pFinish - pStart < from.size()) {
            *pFinish = from[pFinish - pStart];
            ++pFinish;
        }
    }
    dynamic_array<value_type> operator = (dynamic_array<value_type> &from) { 
        delete[] pStart;
        pFinish = pStart = new value_type[from.max_size()];
        pEnd_of_Storage = pStart + from.max_size();
        while (pFinish - pStart < from.size()) {
            *pFinish = *from[pFinish - pStart];
            ++pFinish;
        }
    }
    dynamic_array<value_type> operator = (dynamic_array<value_type> from) { *this = from; }
    ~dynamic_array() { delete[] pStart; }
    iterator begin() const { return pStart; }
    iterator end() const { return pFinish; }
    reverse_iterator rbegin() const { return reverse_iterator(pFinish); }
    reverse_iterator rend() const { return reverse_iterator(pStart); }
    bool empty() const { return pStart == pFinish; }
    size_t size() const { return pFinish - pStart; }
    size_t max_size() const { return pEnd_of_Storage - pStart; }
    reference front() { return *pStart; }
    reference back() { return *(pFinish - 1); }
    reference operator [] (int pos) { return *(pStart + pos); }
    const_reference front() const { return *pStart; }
    const_reference back() const { return *(pFinish - 1); }
    const_reference operator [] (int pos) const { return *(pStart + pos); }
    void clear() {
        delete[] pStart;
        pFinish = pStart = new value_type[4];
        pEnd_of_Storage = pStart + 4;
    }
    void resize_memory(size_t new_size) {
        iterator neo = new value_type[new_size];
        iterator neo_ed = neo + new_size, neo_t = neo;
        iterator orig = pStart;
        while (orig != pFinish && neo_t != neo_ed)
            *(neo_t++) = *(orig++);
        delete[] pStart;
        pStart = neo;
        pFinish = neo_t;
        pEnd_of_Storage = neo_ed;
    }
    void resize(size_t new_size) {
        resize_memory(new_size);
        while (pFinish != pEnd_of_Storage) *(pFinish++) = value_type();
    }
    void push_back(const value_type val) {
        if (pFinish == pEnd_of_Storage) resize_memory((pEnd_of_Storage - pStart) << 1);
        *(pFinish++) = val;
    }
    void pop_back() {
        --pFinish;
        if ((pFinish - pStart) < ((pEnd_of_Storage - pStart) >> 1) && ((pEnd_of_Storage - pStart) >> 1) >= (size_t)4) 
            resize_memory((pEnd_of_Storage - pStart) >> 1);
    }
    void swap(dynamic_array<value_type> &b) {
        swap(pStart, b.pStart);
        swap(pFinish, b.pFinish);
        swap(pEnd_of_Storage, b.pEnd_of_Storage);
    }
};

// #define int int64

const int N = 2e6 + 7, P = 998244353;

int T, n, m;
int l[N], r[N], cnt;

#define leaf(x) (!l[x] && !r[x])

bool check(dynamic_array<int> &v) {
    // printf("check : { ");
    // for (int &x : v) printf("%d, ", x);
    // printf("\b\b }\n");
    if (v.empty()) return false;
    for (int &x : v) if (leaf(x)) return true;
    dynamic_array<int> a, b, c, d;
    // a : left = null
    // b : right = null
    // c : left = leaf
    // d : right = leaf
    for (int &x : v) {
        int &xl = l[x], &xr = r[x];
        if (!xl && xr) a.push_back(xr);
        if (xl && !xr) b.push_back(xl);
        if (xl && xr) {
            if (leaf(xl)) c.push_back(xr);
            if (leaf(xr)) d.push_back(xl);
        }
    }
    return check(a) && check(b) && check(c) && check(d);
}

signed main() {
    read >> T;
    dynamic_array<int> t;
    while (T--) {
        cnt = 0;
        read >> m;
        t.clear();
        for (int i = 1; i <= m; i++) {
            read >> n;
            t.push_back(cnt + 1);
            for (int i = 1; i <= n; i++) {
                int u = cnt + i;
                read >> l[u] >> r[u];
                if (l[u]) l[u] += cnt;
                if (r[u]) r[u] += cnt;
            }
            cnt += n;
        }
        if (check(t)) puts("Almost Complete");
        else puts("No");
    }
    return 0;
}