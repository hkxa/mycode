/*************************************
 * @problem:      【模板】笛卡尔树.
 * @author:       brealid.
 * @time:         2020-12-01.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#define USE_FREAD  // 使用 fread  读入，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) { // 由于这道题的特殊性, 本函数读入 unsigned
            endch = getchar();
            while ((!isdigit(endch)) && endch != EOF) endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };
}
Fastio::Reader kin;

const int N = 1e7 + 7, B = 1 << 20;

int n, v[N];
int s[N], top;
int l[N], r[N];

signed main() {
    kin >> n;
    for (int i = 1; i <= n; ++i) {
        kin >> v[i];
        while (top && v[i] < v[s[top]]) l[i] = s[top--];
        r[s[top]] = i; // 如果 top = 0 则 s[top] = 0 对答案无影响(不特判)
        s[++top] = i;
    }
    int64 sl(0), sr(0);
    for (int i = 1; i <= n; ++i)
        sl ^= i * int64(l[i] + 1), sr ^= i * int64(r[i] + 1);
    cout << sl << ' ' << sr << endl;
    return 0;
}