#include <bits/stdc++.h>
#include "eval.hpp"
using namespace std;
// https://www.luogu.org/blog/your-alpha1022/testdata

const int n = 20;
const int sizeData[21] = {0, 15, 20, 30, 45, 60, 80, 100, 300, 600, 1000, 3000, 6000, 10000, 30000, 60000, 100000, 300000, 500000, 800000, 1000000};

int main()
{
    for (int i = 1; i <= n; i++) {
        eval testdata;
        testdata.open("P1252_data\\P1252_data", i);
        for (int j = 1; j <= 5; j++) {
            int costTime = 0, minn = 1;
            for (int k = 1; k <= 10; k++) {
                minn = randint(minn, sizeData[i] - (10 - k));
                costTime += minn;
                testdata << costTime << ' ';
            }
            testdata << '\n';
        }
        testdata.generate("P1252.exe");
        testdata.clear();
    }
    return 0;
}