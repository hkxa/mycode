/*************************************
 * @problem:      [JSOI2008]Blue Mary开公司.
 * @author:       brealid.
 * @time:         2020-11-17.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#if true
#define getchar() (i_P1 == i_P2 && (i_P2 = (i_P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), i_P1 == i_P2) ? EOF : *i_P1++)
char BUF[1 << 21], *i_P1 = BUF, *i_P2 = BUF;
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        Reader& operator >> (double &lf) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF && endch != '.') endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            if (endch != '.') {
                lf = (endch & 15);
                while (isdigit(endch = getchar())) lf = lf * 10 + (endch & 15);
            } else lf = 0;
            if (endch == '.') {
                double len = 1;
                while (isdigit(endch = getchar())) lf += (endch & 15) * (len *= 0.1);
            }
            if (flag) lf = -lf;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 100000 + 7;

int n, m;
template <typename T>
struct funct {
    T k, b;
    funct() : k(0), b(0) {}
    funct(const T &K, const T &B) : k(K), b(B) {}
    T operator () (int val) const {
        return k * val + b;
    }
};
funct<double> tr[N << 2];

void segt_insert(const int &u, const int &l, const int &r, const funct<double> &f) {
    if (tr[u].k == 0 && tr[u].b == 0) {
        tr[u] = f;
        return;
    }
    if (l == r) {
        if (f(l) > tr[u](l)) tr[u] = f;
        return;
    }
    int mid = (l + r) >> 1;
    if (f.k > tr[u].k) {
        if (f(mid) > tr[u](mid)) segt_insert(u << 1, l, mid, tr[u]), tr[u] = f;
        else segt_insert(u << 1 | 1, mid + 1, r, f);
    } else {
        if (f(mid) > tr[u](mid)) segt_insert(u << 1 | 1, mid + 1, r, tr[u]), tr[u] = f;
        else segt_insert(u << 1, l, mid, f);
    }
}

double segt_query(const int &u, const int &l, const int &r, const int &pos) {
    if (l == r) return tr[u](pos);
    int mid = (l + r) >> 1;
    return pos <= mid ? max(tr[u](pos), segt_query(u << 1, l, mid, pos))
                      : max(tr[u](pos), segt_query(u << 1 | 1, mid + 1, r, pos));
}

signed main() {
    n = 5e4;
    char option[20];
    double s, p;
    int xday;
    kin >> m;
    for (int i = 1; i <= m; ++i) {
        kin >> option;
        if (option[0] == 'P') {
            kin >> s >> p;
            segt_insert(1, 1, n, funct<double>(p, s - p));
        } else {
            kin >> xday;
            // printf("[%.5lf] ", segt_query(1, 1, n, xday));
            kout << int(segt_query(1, 1, n, xday) / 100) << '\n';
        }
    }
    return 0;
}