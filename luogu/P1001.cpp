/*************************************
 * problem:      P1001 A+B Problem.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2017-10-19.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 
/*************************************
 * record ID:    R3804031.
 * time:         0 ms
 * memory:       7859 KB
*************************************/ 

#include <iostream>
using namespace std;

int main()
{
    long long a, b;
    cin >> a >> b;
    cout << a+b;
    return 0;
}
