/*************************************
 * problem:      P2574 XOR的艺术.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-10-09.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 
#pragma GCC optimize(1)
#pragma GCC optimize(2)
#pragma GCC optimize(3)
#pragma GCC optimize("Ofast")
#include <bits/stdc++.h>
using namespace std;

int n, m;
char s[200007];
int op, a, b;

struct node {
    node *l, *r;
    int ml, mr;
    int cnt, size;
    bool val, lazy;
    node() : l(NULL), r(NULL), ml(0), mr(0), cnt(0), size(0), val(false), lazy(false) {}

    void change() 
    { 
        val ^= 1; 
        lazy ^= 1; 
        cnt = size - cnt; 
    }

    void pushdown() 
    {  
        if (lazy) {
            // printf("pushdown ing...\n");
            if (l) l->change();
            if (r) r->change();
            lazy = false;
        }
    }

    void pushup() 
    {  
        val = l->val ^ r->val;
        cnt = l->cnt + r->cnt;
        lazy = false;
    }
};
node *root = new node;

void build(int l, int r, node *p)
{
    p->ml = l;
    p->mr = r;
    if (l == r) {
        p->val = s[l] & 1;
        if (p->val) p->cnt = 1; 
        p->size = 1;
        return;
    }
    int mid = (l + r) >> 1;
    if (l <= mid) {
        p->l = new node;
        build(l, mid, p->l);
        p->val ^= p->l->val;
        p->size += p->l->size;
        p->cnt += p->l->cnt;
    }
    if (mid + 1 <= r) {
        p->r = new node;
        build(mid + 1, r, p->r);
        p->val ^= p->r->val;
        p->size += p->r->size;
        p->cnt += p->r->cnt;
    }
}

void change(int l, int r, node *p)
{
    if (p->ml > r || p->mr < l) return;
    if (p->ml >= l && p->mr <= r) { p->change(); return; }
    p->pushdown();
    if (p->l) change(l, r, p->l);
    if (p->r) change(l, r, p->r);
    p->pushup();
}

int query(int l, int r, node *p)
{
    if (p->ml > r || p->mr < l) return 0;
    if (p->ml >= l && p->mr <= r) { 
        // printf("query ret [%d, %d] = %d\n", p->ml, p->mr, p->cnt);
        return p->cnt; 
    }
    int res = 0;
    p->pushdown();
    if (p->l) res += query(l, r, p->l);
    if (p->r) res += query(l, r, p->r);
    // printf("query ret [%d, %d] = %d\n", p->ml, p->mr, res);
    return res;
}

int main()
{
    scanf("%d%d", &n, &m);
    scanf("%s", s + 1);
    build(1, n, root);
    while (m--) {
        scanf("%d%d%d", &op, &a, &b);
        if (op == 0) change(a, b, root);
        else printf("%d\n", query(a, b, root));
    }
    return 0;
}