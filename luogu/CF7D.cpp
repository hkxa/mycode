/*************************************
 * @problem:      lizard.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-22.
 * @language:     C++.
 * @fastio_ver:   20200820.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore space, \t, \r, \n
            ch = getchar();
            while (ch != ' ' && ch != '\t' && ch != '\r' && ch != '\n') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            Int flag = 1;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = -1, endch = getchar();
            i = endch & 15;
            while (isdigit(endch = getchar())) i = (i << 3) + (i << 1) + (endch & 15);
            if (flag == -1) i = -i;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            Int flag = 1;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = -1, endch = getchar();
            Int i = endch & 15;
            while (isdigit(endch = getchar())) i = (i << 3) + (i << 1) + (endch & 15);
            if (flag == -1) i = -i;
            return i;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64
// #define int128 int64
// #define int128 __int128

char a[5000010], s[10000010];
int f[10000010];
int n;

inline void transfer() {
    s[0] = s[1] = '#';
    for (int i = 0; i < n; i++)
        s[(i << 1) + 2] = a[i], s[(i << 1) + 3] = '#';
    n = n * 2 + 2;
    s[n] = '\0';
}

inline void manacher() {
    int MaxRight = 0, mid;
    for (int i = 1; i < n; i++) {
        if (i < MaxRight)f[i] = min(f[(mid << 1) - i], f[mid] + mid - i);
        else f[i] = 1;
        while (s[i + f[i]] == s[i - f[i]]) {
            if (f[i] + i > MaxRight) {
                MaxRight = f[i] + i;
                mid = i;
            }
            f[i]++;
        }
    }
}

int g[5000010];

int main() {
    scanf("%s", a);
    n = strlen(a);
    transfer();
    manacher();

    int ans = 1;
    g[2] = 1;
    for (int i = 3; i <= (n >> 1); i++) {
        if (f[i] == i) g[i] = g[(i + 1) >> 1] + 1;
        else g[i] = 0;
        ans += g[i];
    }
    write << ans <<'\n';
    return 0;
}