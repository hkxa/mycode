//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      P4556 [Vani有约会]雨天的尾巴 /【模板】线段树合并.
 * @user_id:      63720.
 * @user_name:    brealid.
 * @time:         2020-05-19.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}
// #define int int64
const int N = 100000 + 7, NlogN = 100000 * 20 + 7, MaxCol = 100000;
int n, m;
vector<int> G[N];
queue<pair<int, int> > upd[N]; // 需要 upd 的组合
int root[N];            // 每个节点对应的线段树 root（其实就是其本身）
int l[NlogN], r[NlogN]; // 存储左右子节点
int sum[NlogN];         // 结点掌管区间内的 最多的救济粮 的个数
int64 ans[NlogN];       // 结点掌管区间区间内的 最小的 且 最多的 救济粮的编号
int cnt = 0;            // 动态开点，当前开的最大点编号
int64 NodeAns[N];       // 每个真实树上节点的 Answer

// 树链剖分预备
int dfn[N], dft;
int f[N], beg[N], wson[N], siz[N], dep[N];

// 树链剖分 dfs - 1 : get siz, f
void dfs1(int u, int fa) {
    siz[u] = 1;
    f[u] = fa;
    for (int v : G[u]) {
        if (v != fa) {
            dfs1(v, u);
            siz[u] += siz[v];
            if (siz[v] > siz[wson[u]]) 
                wson[u] = v;
        }
    }
}

// 树链剖分 dfs - 2 : get dfn, beg, dep
void dfs2(int u, int chainBegin) {
    dfn[u] = ++dft;
    beg[u] = chainBegin;
    dep[u] = dep[f[u]] + 1;
    if (wson[u]) dfs2(wson[u], chainBegin);
    for (int v : G[u]) {
        if (v != f[u] && v != wson[u]) {
            dfs2(v, v);
        }
    }
}

int LCA(int u, int v) {
    while (beg[u] != beg[v]) {
        if (dep[beg[u]] < dep[beg[v]]) swap(u, v);
        u = f[beg[u]];
    }
    if (dep[u] < dep[v]) return u;
    else return v;
}

void pushUp(int u) {
    if (ans[l[u]] != ans[r[u]]) {
        sum[u] = max(sum[l[u]], sum[r[u]]);
        if (sum[l[u]] >= sum[r[u]]) ans[u] = ans[l[u]]; // 取 l[u], r[u] 中 sum 较大的一者记录 ans (相等取较小)
        else ans[u] = ans[r[u]]; // sum[l[u]] < sum[r[u]] 
    } else {
        sum[u] = sum[l[u]] + sum[r[u]];
        ans[u] = ans[l[u]]; 
    }
}

/**
 * u : 线段树节点编号
 * co : 将要修改的 color 值，为这个 color 值加上 dif
 * dif : △修改值
 * L,R : 当前线段树节点所掌管的区间(不会用数组记录，随 dfs 而计算)(使用大写是为了与 记录线段树节点左右儿子的数组 区分)
 */
void update(int &u, int co, int dif, int L, int R) {
    if (!u) {
        // 访问到空节点(NULL)，但还没完成修改操作，应当新建节点
        // 一个小 trick : u 设为 '&'(引用)，在 update 里修改 u 即可以修改真实的 l[Fa_of_u] / r[Fa_of_u]
        u = ++cnt;
    }
    if (co == L && co == R) {
        // 找到要修改的节点并修改
        sum[u] += dif;
        ans[u] = sum[u] ? co : 0;
        return;
    }
    int mid = (L + R) >> 1;
    if (co <= mid) update(l[u], co, dif, L, mid);
    else update(r[u], co, dif, mid + 1, R);
    pushUp(u);
}

/**
 * u,v : 将要合并的两棵线段树节点编号(v 合并到 u)
 * l,r : 当前线段树节点所掌管的区间(不会用数组记录，随 dfs 而计算)
 */
int merge(int u, int v, int L, int R) {
    // 如果两棵线段树中，只有 1 棵有掌管 [L, R] 的节点，那么合并后该节点肯定保持不变
    if (!u) return v; 
    if (!v) return u;

    // 叶子节点的合并
    if (L == R) {
        // 由于两个节点都属于叶子节点，所以无需考虑 ans 不同的情况
        sum[u] += sum[v];
        ans[u] = sum[u] ? L : 0;
        // fprintf(stderr, "u : {.sum = %d, .ans = %d}\n", sum[u], ans[u]);
        // fprintf(stderr, "v : {.sum = %d, .ans = %d}\n", sum[v], ans[v]);
        return u;
    }

    // 非叶子结点的合并
    int mid = (L + R) >> 1;
    l[u] = merge(l[u], l[v], L, mid);     // 左子树的合并
    r[u] = merge(r[u], r[v], mid + 1, R); // 右子树的合并
    pushUp(u); // 本节点的更新
    return u;
}

void dfs(int u, int fa)
{
    for (int v : G[u]) {
        if (v != fa) {
            dfs(v, u);
            merge(root[u], root[v], 1, MaxCol);
        }
    }
    while (!upd[u].empty()) {
        update(root[u], upd[u].front().first, upd[u].front().second, 1, MaxCol);
        upd[u].pop();
    }
    NodeAns[u] = ans[root[u]];
}

int main()
{
    n = read<int>();
    m = read<int>();
    for (int i = 1; i <= n; i++) 
        root[i] = i; // 尚未初始化的开点；初始化会在 update 内一并完成
    cnt = n; // 已经开了 n 个 空的 root 节点
    for (int i = 1, x, y; i < n; i++) {
        x = read<int>();
        y = read<int>();
        G[x].push_back(y);
        G[y].push_back(x);
    }
    dfs1(1, 0);
    dfs2(1, 1);
    // printf("LCA(%d, %d) = %d\n", 4, 5, LCA(4, 5));
    // printf("LCA(%d, %d) = %d\n", 2, 4, LCA(2, 4));
    // printf("LCA(%d, %d) = %d\n", 2, 5, LCA(2, 5));
    for (int i = 1, x, y, z, l; i <= m; i++) {
        x = read<int>();
        y = read<int>();
        z = read<int>();
        l = LCA(x, y);
        upd[x].push(make_pair(z, 1));
        upd[y].push(make_pair(z, 1));
        upd[l].push(make_pair(z, -1));
        upd[f[l]].push(make_pair(z, -1));
    }
    dfs(1, 0);
    for (int i = 1; i <= n; i++) {
        write(NodeAns[i], '\n');
    }
    return 0;
}