/*************************************
 * problem:      P2145 [JSOI2007]祖码.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-17.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define puts_return(x) { puts(x); return 0; }
#define write_return(x) { write(x); return 0; }

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

struct near {
    int id, cnt;
    near() : id(-1), cnt(0) {}
    near(int i, int c) : id(i), cnt(c) {}
} a[501];

int n, f[501][501];

int main()
{
    int TempN = read<int>() - 1, tmp = read<int>();
    a[n = 1] = near(tmp, 1);
    while (TempN--) {
        tmp = read<int>();
        if (a[n].id == tmp) a[n].cnt++;
        else a[++n] = near(tmp, 1);
    }
    memset(f, 0x3f, sizeof(f));
    for (int i = 1; i <= n; i++) {
        f[i][i] = (a[i].cnt == 1) + 1;
    }
    for (int l = 2; l <= n; l++) {
        for (int i = 1, j = l; j <= n; i++, j++) {
            for (int k = i; k < j; k++) {
                f[i][j] = min(f[i][j], f[i][k] + f[k + 1][j]);
            }
            if (a[i].id == a[j].id) {
                f[i][j] = min(f[i][j], f[i + 1][j - 1] + (a[i].cnt + a[j].cnt < 3));
                for (int k = i + 2; k <= j - 2; k++) {
                    if (a[i].id == a[k].id && a[i].cnt + a[j].cnt <= 3 && a[k].cnt == 1) {
                        f[i][j] = min(f[i][j], f[i + 1][k - 1] + f[k + 1][j - 1]);
                        for (int m = k + 2; m <= j - 2; m++) {
                            if (a[i].id == a[m].id && a[i].cnt == 1 && a[j].cnt == 1 && a[m].cnt == 1)
                                f[i][j] = min(f[i][j], f[i + 1][k - 1] + f[k + 1][m - 1] + f[m + 1][j - 1]);
                        }
                    }
                    if (a[i].id == a[k].id && a[k].cnt > 1) { // divide 
                        for (int m = k + 2; m <= j - 2; m++) {
                            if (a[i].id == a[m].id && a[i].cnt == 1 && a[j].cnt == 1 && a[m].cnt == 1)
                                f[i][j] = min(f[i][j], f[i + 1][k - 1] + f[k + 1][m - 1] + f[m + 1][j - 1]);
                        }
                    }
                }
            }
        }
    }
    write(f[1][n], 10);
    return 0;
}