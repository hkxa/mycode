/*************************************
 * @problem:      beads.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-22.
 * @language:     C++.
 * @fastio_ver:   20200820.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore space, \t, \r, \n
            ch = getchar();
            while (ch != ' ' && ch != '\t' && ch != '\r' && ch != '\n') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            Int flag = 1;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = -1, endch = getchar();
            i = endch & 15;
            while (isdigit(endch = getchar())) i = (i << 3) + (i << 1) + (endch & 15);
            if (flag == -1) i = -i;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            Int flag = 1;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = -1, endch = getchar();
            Int i = endch & 15;
            while (isdigit(endch = getchar())) i = (i << 3) + (i << 1) + (endch & 15);
            if (flag == -1) i = -i;
            return i;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64
// #define int128 int64
// #define int128 __int128

int n, f1; 
int64 k; 
bool flag;

void Print(int x) {
    for(int i = n / 2 - 1; ~i; --i)
        printf("%d", (x >> i) & 1);
}

inline int Rev(int x) {
    int ret = 0;
    for(int i = 0; i < n / 2; ++i)
        ret |= ((x >> i) & 1) << (n / 2 - 1 - i);
    return ret;
}

int main() { 
    read >> n >> k;
    f1 = (1 << (n / 2)) - 1;
    for (int i = 0; i < 1 << (n / 2 - 1); ++i) {
        int siz = ((1 << (n / 2 - 1)) - i) << (1 + (n & 1));
        siz -= !i + (n & 1);
        if (k > siz) k -= siz;
        else {
            flag = true;
            for (int j = 1; j < 1 << (n / 2); ++j) {
                if (Rev(j) >= i && Rev((j ^ f1)) >= i) --k;
                if (!k) {
                    Print(i);
                    if (n & 1) putchar('0');
                    Print(j);
                    puts("");
                    break;
                }
            }
            if ((n & 1) && k) {
                for (int j = 0; j < 1 << (n / 2); ++j) {
                    if (Rev(j) >= i && Rev((j ^ f1)) > i) --k;
                    if (!k) {
                        Print(i);
                        printf("1");
                        Print(j);
                        printf("\n");
                        break;
                    }
                }
            }
        }
        if (flag) break;
    }
    if (!flag) puts("-1");
    return 0;
}