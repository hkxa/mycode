/*************************************
 * @problem:      CF1895C.
 * @author:       brealid.
 * @time:         2023-11-03.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

// #define USE_FREAD  // 使用 fread  读入，去注释符号
// #define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 3e5 + 7;

int T, n, m;
struct card {
    int a, b, id;
} x[N], y[N];
int a_beaten_maxdefid[N], b_beaten_maxdefid[N];
int winstate[N];

bool cmp_by_atk(const card &p, const card &q) { return p.a < q.a; }
bool cmp_by_def(const card &p, const card &q) { return p.b < q.b; }
bool cmp_by_id(const card &p, const card &q) { return p.id < q.id; }

void get_winstate(int i) {
    // 3 for win, 2 for draw, 1 for lose
    if (a_beaten_maxdefid[i] == -1) {
        winstate[i] = 3;
        return;
    }
    int nextcard = b_beaten_maxdefid[a_beaten_maxdefid[i]];
    if (nextcard == -1) {
        winstate[i] = 1;
        return;
    }
    if (nextcard == i) {
        winstate[i] = 2;
        return;
    }
    if (winstate[nextcard] == 0) get_winstate(nextcard);
    winstate[i] = winstate[nextcard];
}

void once() {
    kin >> n;
    for (int i = 1; i <= n; ++i) x[i].id = i;
    for (int i = 1; i <= n; ++i) kin >> x[i].a;
    for (int i = 1; i <= n; ++i) kin >> x[i].b;
    kin >> m;
    for (int i = 1; i <= m; ++i) y[i].id = i;
    for (int i = 1; i <= m; ++i) kin >> y[i].a;
    for (int i = 1; i <= m; ++i) kin >> y[i].b;
    // a_beaten by b
    sort(x + 1, x + n + 1, cmp_by_def);
    sort(y + 1, y + m + 1, cmp_by_atk);
    int i = n, j = m, curmaxdefid = -1;
    while (i >= 1) {
        while (j >= 1 && y[j].a > x[i].b) {
            if (curmaxdefid == -1 || y[j].b > y[curmaxdefid].b)
                curmaxdefid = j;
            --j;
        }
        // printf("a %d: j+1=%d\n", x[i].id, y[j+1].id);
        a_beaten_maxdefid[x[i].id] = curmaxdefid == -1 ? -1 : y[curmaxdefid].id;
        --i;
    }
    // b_beaten by a
    sort(x + 1, x + n + 1, cmp_by_atk);
    sort(y + 1, y + m + 1, cmp_by_def);
    i = n, j = m, curmaxdefid = -1;
    while (j >= 1) {
        while (i >= 1 && x[i].a > y[j].b) {
            if (curmaxdefid == -1 || x[i].b > x[curmaxdefid].b)
                curmaxdefid = i;
            --i;
        }
        b_beaten_maxdefid[y[j].id] = curmaxdefid == -1 ? -1 : x[curmaxdefid].id;
        --j;
    }
    // solve
    // sort(x + 1, x + n + 1, cmp_by_id);
    // sort(y + 1, y + m + 1, cmp_by_id);
    for (int i = 1; i <= n; ++i) winstate[i] = 0;
    int ans[4] = {0};
    for (int i = 1; i <= n; ++i) {
        get_winstate(i);
        ++ans[winstate[i]];
        // printf("winstate[%d] = %d (a_beaten_maxdefid = %d)\n", i, winstate[i], a_beaten_maxdefid[i]);
    }
    kout << ans[3] << ' ' << ans[2] << ' ' << ans[1] << '\n';
}

signed main() {
    kin >> T;
    while (T--) once();
    return 0;
}