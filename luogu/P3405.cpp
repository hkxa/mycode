/*************************************
 * @problem:      P3405 [USACO16DEC]Cities and States省市.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-02-04.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;
char name[10 + 3], code[2 + 3];
int cnt[26 * 26 * 26 * 26 + 3];
int64 ans = 0;

#define transOne(str) ((str[0] - 'A') * 26 + (str[1] - 'A'))
#define translate(str1, str2) (transOne(str1) * 676 + transOne(str2))

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        scanf("%s%s", name, code);
        // printf("%d %d\n", transOne(name), transOne(code));
        // int f = translate(name, code);
        // printf("%c%c %c%c.\n", f / 26 / 26 / 26 + 'A', f / 26 / 26 % 26 + 'A', f / 26 % 26 + 'A', f % 26 + 'A');
        if (transOne(name) != transOne(code)) {
            ans += cnt[translate(name, code)];
            cnt[translate(code, name)]++;
        }
    }
    write(ans, 10);
    return 0;
}