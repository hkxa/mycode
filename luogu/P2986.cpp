/*************************************
 * problem:      P2986 [USACO10MAR]伟大的奶牛聚集Great Cow Gat….
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-20.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define puts_return(x) { puts(x); return 0; }
#define write_return(x) { write(x); return 0; }

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

struct road {
    int nxt, val;
};

int n, c[100007], sumCow = 0;
int size[100007];
vector<road> G[100007];
int64 ans[100007], minAns = 0x3f3f3f3f3f3f3f3f;

int64 getRootAns(int u, int fa)
{
    int64 ret(0);
    size[u] = c[u];
    for (road it : G[u]) {
        if (it.nxt != fa) {
            ret += getRootAns(it.nxt, u) + (int64)size[it.nxt] * it.val;
            size[u] += size[it.nxt];
        }
    }
    return ret;
}

/*
   1(1)
   | 
  1|
   3(0)
 2/ \3
 /   \
2(1)  4(0)
      |3
      |
      3(2)
*/

void getOtherAns(int u, int fa)
{
    minAns = min(minAns, ans[u]);
    for (road it : G[u]) {
        if (it.nxt != fa) {
            ans[it.nxt] = ans[u] + (int64)(sumCow - (size[it.nxt] << 1)) * it.val;
            getOtherAns(it.nxt, u);
        }
    }
}

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        c[i] = read<int>();
        sumCow += c[i];
    }
    int a, b, l;
    for (int i = 1; i < n; i++) {
        a = read<int>();
        b = read<int>();
        l = read<int>();
        G[a].push_back((road){b, l});
        G[b].push_back((road){a, l});
    }
    ans[1] = getRootAns(1, 0);
    // write(ans[1], '`');
    getOtherAns(1, 0);
    write(minAns, 10);
    return 0;
}