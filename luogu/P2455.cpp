//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      [SDOI2006]线性方程组.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-15.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 998244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? Module(w - P) : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

const int N = 100 + 7;

int n;
double a[N][N];

bool TestNoSolution(int i) {
    for (int j = 1; j <= n; j++) if (abs(a[i][j]) >= 1e-6) return false;
    return abs(a[i][n + 1]) >= 1e-6;
}

void NoSolution() {
    puts("-1");
    exit(0);
}

void ManySolution() {
    puts("0");
    exit(0);
}

void debug(int u) {
    printf("Id = %d : \n", u);
    for (int i = 1; i <= n; i++)
        for (int j = 1; j <= n + 1; j++)
            printf("%.2lf%c", a[i][j], " \n"[j == n + 1]);
}

bool better(int u, int v, int p) {
    if (abs(a[u][p]) - abs(a[v][p]) > 1e-6) return true;
    if (abs(a[v][p]) - abs(a[u][p]) > 1e-6) return false;
    p++;
    while (p <= n) {
        // printf("p = %d\n", p);
        if (abs(a[v][p]) - abs(a[u][p]) > 1e-6) return true;
        if (abs(a[u][p]) - abs(a[v][p]) > 1e-6) return false;
        p++;
    }
    return false;
}

void Guass() {
    double m;
    bool Many = false;
    for (int i = 1; i <= n; i++) {
        int Swap = i;
        for (int j = i + 1; j <= n; j++)
            if (better(j, Swap, i)) Swap = j;
        // printf("Swap = %d\n", Swap);
        for (int j = 1; j <= n + 1; j++) swap(a[i][j], a[Swap][j]);
        if (abs(a[i][i]) >= 1e-6) {
            m = a[i][i];
            for (int j = i; j <= n + 1; j++) a[i][j] /= m;
        }
        for (int j = 1; j <= n; j++) if (i != j) {
            m = a[j][i];
            for (int k = i; k <= n + 1; k++) {
                a[j][k] -= a[i][k] * m;
            }
        }
        // debug(i);
    }
}

double Turn(double x) {
    if (abs(x) < 1e-6) return 0;
    return x;
}

signed main() {
    n = read<int>();
    for (int i = 1; i <= n; i++) 
        for (int j = 1; j <= n + 1; j++)
            a[i][j] = read<int>();
    Guass();
    for (int i = 1; i <= n; i++)
        if (abs(a[i][i]) <= 1e-6 && abs(a[i][n + 1]) >= 1e-6) NoSolution();
    for (int i = 1; i <= n; i++)
        if (abs(a[i][i]) <= 1e-6 && abs(a[i][n + 1]) <= 1e-6) ManySolution();
    for (int i = 1; i <= n; i++)
        printf("x%d=%.2lf\n", i, Turn(a[i][n + 1] / a[i][i]), " \n"[i == n]);
    return 0;
}

// Create File Date : 2020-06-15

/*
3
1 0 1 1
1 1 0 1
0 1 1 1
*/