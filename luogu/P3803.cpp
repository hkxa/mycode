// luogu-judger-enable-o2
#include <bits/stdc++.h>
#define Maxn 5000007
#define Pi M_PI // import M_PI from math.h
using namespace std;

template <typename Int>
Int read() {
	char ch = getchar();
	while (!isdigit(ch)) ch = getchar();
	Int a = (ch & 15);
	ch = getchar();
	while (isdigit(ch)) {
		a = (a + (a << 2) << 1) + (ch ^ 48);
		ch = getchar();
	}
	return a;
}  

struct Complex {
	double x, y;
	Complex(double xx = 0, double yy = 0) : x(xx), y(yy) {}
	
	Complex operator + (Complex b)
	{
		return Complex(x + b.x, y + b.y);
	}
	
	Complex operator - (Complex b)
	{
		return Complex(x - b.x, y - b.y);
	}
	
	Complex operator * (Complex b)
	{
		return Complex(x * b.x - y * b.y, x *b.y + y * b.x);
	}
} b[Maxn], c[Maxn];
// struct_Complex and operations
int n, m, r[Maxn];

void fft(Complex *f, int op)
{
	for (int i = 0; i < n; i++) {
		if (i < r[i]) {
			Complex tmp = f[i];
			f[i] = f[r[i]];
			f[r[i]] = tmp;
		}
	}
	for (int p = 2; p <= n; p <<= 1) { 
	    // $ O(log_n) $
		int len = p / 2;
		Complex tmp(cos(Pi / len), op * sin(Pi / len));
		for (register int k = 0; k < n; k += p) { 
		    // $ O(log_n) $
			Complex buf(1, 0);
			for (register int l = k; l < k + len; l++) { 
			    // $ O(log_n) $
				Complex tt = buf * f[len + l];
				f[len + l] = f[l] - tt;
				f[l] = f[l] + tt;
				buf = buf * tmp;
			}
		}
	}
}

char num[1000007];
int ans[2000007];

int main()
{
    scanf("%s", num);
	n = strlen(num) - 1;
	for (register int i = 0; i <= n; i++) {
		b[i].x = num[i] & 15;
	}
    scanf("%s", num);
	m = strlen(num) - 1;
	for (register int i = 0; i <= m; i++) {
		c[i].x = num[i] & 15;
	}
	m += n; 
	n = 1;
	while (n <= m) {
		n <<= 1;
	}
	for (int i = 0; i < n; i++) {
		r[i] = (r[i >> 1] >> 1) | ((i & 1) ? n >> 1 : 0);
	}
	fft(b, 1);
	fft(c, 1);
	// DFT
	for (register int i = 0; i < n; i++) b[i] = b[i] * c[i];
	fft(b, -1);
	// IDFT
	for (register int i = m; i > 0; i--) {
		ans[i] += (int)(fabs(b[i].x) / n + 0.25);
        if (ans[i] > 9) {
            ans[i - 1] = ans[i] / 10;
            ans[i] %= 10;
        }
	}
    printf("%d", ans[0] + (int)(fabs(b[0].x) / n + 0.25));
	for (register int i = 1; i <= m; i++) {
		printf("%d", ans[i]);
	}
	return 0;
}