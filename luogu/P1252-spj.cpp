#include "testlib.h"

int player[6][11];
int needToRun[6], sum = 0, cnt = 0;

int main(int argc, char* argv[]) 
{
    registerTestlibCmd(argc, argv);
    int std_ans = ans.readLong();
    int usr_ans = ouf.readLong();
    if (std_ans != usr_ans) quitf(_wa, "The answer is wrong: expect min_distance = %lld, found = %lld", std_ans, usr_ans);
    for (int i = 1; i <= 5; i++) {
        for (int j = 1; j <= 10; j++) {
            player[i][j] = inf.readLong();
        }
    }
    for (int i = 1; i <= 5; i++) {
        needToRun[i] = ouf.readLong();
        if (needToRun[i] < 1 || needToRun[i] > 10) quitf(_wa, "The min_distance is correct, but in second line, No.%d number is Invaild", i);
        sum += player[i][needToRun[i]];
        cnt += needToRun[i];
    }
    if (sum == std_ans && cnt == 25) quitf(_ok, "The answer is correct.");
    else if (cnt != 25) quitf(_wa, "The min_distance is correct, but in second line, sum of 5 numbers is not equal to 25");
    else quitf(_wa, "The min_distance is correct, but in second line, sum of 5 runner's cost_time is not correct");
    return 0;

}