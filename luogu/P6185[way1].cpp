/*************************************
 * @problem:      sequence.
 * @user_id:      ZJ-00625.
 * @time:         2020-03-07.
 * @language:     C++.
 * @upload_place: CCF - NOI Online.
*************************************/ 
// WA(?) T(?) M(↓memory)
#pragma GCC optimize("-O2")
#pragma GCC optimize("-Ofast")
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// #define int int64
// #define unsigned uint64
#define MaxN 100007
#define MaxM 100007
#define MaxEdge 200007

int T, n, m;
int a[MaxN];
int op[MaxM], u[MaxM], v[MaxM];
int head[MaxN], to[MaxEdge], nxt[MaxEdge], edge_cnt;
int belong[MaxN], block_cnt;
// bool getRidOf[MaxN];
int64 sum[MaxN];
int CountNode;
int64 NodesSum[3];
int type[MaxN];
bool NoAnswer;

inline void clearEdges()
{
    memset(head, -1, sizeof(head));
    edge_cnt = 0;
}

inline void clearBlocks()
{
    memset(belong, 0, sizeof(belong));
    memset(sum, 0, sizeof(sum));
    block_cnt = 0;
}

inline void addEdge(int u, int v)
{
    // Add u->v
    to[++edge_cnt] = v;
    nxt[edge_cnt] = head[u];
    head[u] = edge_cnt;
    // Add v->u
    to[++edge_cnt] = u;
    nxt[edge_cnt] = head[v];
    head[v] = edge_cnt;
}


void dfs(int u)
{
    // get arr 'belong'
    belong[u] = block_cnt;
    sum[block_cnt] += a[u];
    for (int e = head[u]; ~e; e = nxt[e]) 
        if (!belong[to[e]]) dfs(to[e]);
}

bool ErFenTu(int u, int num) 
{
    if (type[u]) return type[u] == num;
    type[u] = num;
    CountNode++;
    NodesSum[num] += sum[u];
    int RetTag = true;
    for (int e = head[u]; ~e; e = nxt[e]) 
        if (!ErFenTu(to[e], 3 - num)) RetTag = false;
    return RetTag;
}

// bool GetSumVis[MaxN];
// int64 GetSum(int u) 
// {
//     GetSumVis[u] = true;
//     int64 RetTag = sum[u];
//     for (int e = head[u]; ~e; e = nxt[e]) 
//         if (!GetSumVis[to[e]]) RetTag += GetSum(to[e]);
//     return RetTag;
// }

// bool NotCorrect;

// char T_message[] = "true", F_message[] = "false";

signed main()
{
    // freopen("sequence.in", "r", stdin);
    // freopen("sequence.out", "w", stdout);
    T = read<int>();
    while (T--) {
        // printf("--->>> Case Remain %d <<<---\n", T + 1);
        NoAnswer = false;
        n = read<int>();
        m = read<int>();
        for (register int i = 1; i <= n; i++) a[i] = read<int>();
        for (register int i = 1; i <= n; i++) a[i] -= read<int>();
        clearEdges();
        for (register int i = 1; i <= m; i++) {
            op[i] = read<int>();
            u[i] = read<int>();
            v[i] = read<int>();
            if (op[i] == 2) addEdge(u[i], v[i]);
        }
        clearBlocks();
        for (register int i = 1; i <= n; i++) {
            if (!belong[i]) {
                ++block_cnt;
                dfs(i);
            }
        }
        clearEdges();
        // memset(getRidOf, 0, sizeof(getRidOf));
        for (register int i = 1; i <= m; i++) {
            if (op[i] == 1) {
                addEdge(belong[u[i]], belong[v[i]]);
                // if (belong[u[i]] == belong[v[i]]) getRidOf[belong[u[i]]] = true;
            }
        }
        NoAnswer = false;
        memset(type, 0, sizeof(type));
        for (register int i = 1; i <= block_cnt; i++) {
            if (!type[i]) {
                CountNode = NodesSum[1] = NodesSum[2] = 0;
                // printf("block %d :\n    CountNode = %d\n    NodesSum = %lld\n    getRidOf = %s\n", 
                //        i, CountNode, NodesSum, getRidOf[i] ? T_message : F_message);
                // if (CountNode == 1 && getRidOf[i]) NodesSum &= 1;
                if (ErFenTu(i, 1)) {
                    if (NodesSum[1] != NodesSum[2]) {
                        puts("NO");
                        NoAnswer = true;
                        break;
                    }
                } else {
                    // printf("debug : GetSum() = %lld; NodesSum[1] = %lld; NodesSum[2] = %lld;\n", GetSum(i), NodesSum[1], NodesSum[2]);
                    if ((NodesSum[1] + NodesSum[2]) & 1) {
                        puts("NO");
                        NoAnswer = true;
                        break;
                    }
                }
            }
        }
        if (!NoAnswer) puts("YES");
    }
    return 0;
}