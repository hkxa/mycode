/*************************************
 * problem:      CF88B Keyboard.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-04-05.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

int n, m, x;
enum Type {
    no = 0, 
    lower = 1,
    upper = 2
} could[26] = {no};
char keyboard[32][52];
int cnt = 0, Sx[1501], Sy[1501];
int q;
char ques[500002];

void readData()
{
    n = read<int>();
    m = read<int>();
    x = read<int>();
    for (int i = 1; i <= n; i++) {
        scanf("%s", keyboard[i] + 1);
    }
}

void prepareShift()
{
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++) {
            if (keyboard[i][j] == 'S') {
                cnt++;
                Sx[cnt] = i;
                Sy[cnt] = j;
            }
        }
    }
}

bool testDist(int x1, int y1, int x2, int y2)
{
    return sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)) <= x;
}

void dealShift()
{
    for (int k = 1; k <= cnt; k++) {
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= m; j++) {
                if (keyboard[i][j] == 'S' || could[keyboard[i][j] - 'a'] == upper) continue;
                // printf("[deal %c] \t", keyboard[i][j]);
                if (testDist(Sx[k], Sy[k], i, j)) {
                    could[keyboard[i][j] - 'a'] = upper;
                }
            }
        }
    }
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++) {
            if (could[keyboard[i][j] - 'a'] == no) { 
                could[keyboard[i][j] - 'a'] = lower; 
            }
        }
    }
}

void readQues()
{
    q = read<int>();
    scanf("%s", ques + 1);
}

bool check()
{
    bool y = 1;
    for (int i = 1; i <= q; i++) {
        if (ques[i] >= 'A' && ques[i] <= 'Z' && could[ques[i] - 'A'] == no) y = 0;
        if (ques[i] >= 'a' && ques[i] <= 'z' && could[ques[i] - 'a'] == no) y = 0;
        if (ques[i] >= 'A' && ques[i] <= 'Z' && cnt == 0) y = 0;
        if (y == 0) {
            puts("-1");
            return 0;
        }
    }
    return 1;
}

void answerQues()
{
    int ans = 0;
    for (int i = 1; i <= q; i++) {
        if (ques[i] >= 'A' && ques[i] <= 'Z' && could[ques[i] - 'A'] == lower) ans++;
    }
    write(ans);
}

int main()
{
    readData();
    prepareShift();
    dealShift();
    readQues();
    if (check()) answerQues();
    return 0;
}