/*************************************
 * @problem:      [WC2001]高性能计算机.
 * @author:       brealid.
 * @time:         2021-02-01.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;
#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int NodeMax = 20 + 3, TaskMax = 60 + 3;

int nA, nB, p;
int h[TaskMax][TaskMax][2]; // h[i][j][k] 表示计算 i 个 A 类任务与 j 个 B 类任务, 最后一个任务是 k 所需要的最少时间
int f[TaskMax][TaskMax]; // f[i][j] 表示计算 i 个 A 类任务与 j 个 B 类任务所需要的最少时间
int g[NodeMax][TaskMax][TaskMax]; // g[i][j][k] 表示使用前 i 个计算节点计算 j 个 A 类任务与 k 个 B 类任务所需要的最少时间

// 传入某一个计算节点相关的属性
void calc(int tA, int tB, int kA, int kB) {
    memset(h, 0x3f, sizeof(h));
    memset(f, 0x3f, sizeof(f));
    h[0][0][0] = h[0][0][1] = 0;
    for (int i = 0; i <= nA; ++i)
        for (int j = 0; j <= nB; ++j) {
            f[i][j] = min(h[i][j][0], h[i][j][1]);
            for (int nxt_i = i + 1; nxt_i <= nA; ++nxt_i)
                h[nxt_i][j][0] = min(h[nxt_i][j][0], h[i][j][1] + tA + kA * (nxt_i - i) * (nxt_i - i));
            for (int nxt_j = j + 1; nxt_j <= nB; ++nxt_j)
                h[i][nxt_j][1] = min(h[i][nxt_j][1], h[i][j][0] + tB + kB * (nxt_j - j) * (nxt_j - j));
        }
}

void contribute(int now[TaskMax][TaskMax], int lst[TaskMax][TaskMax]) {
    for (int i = 0; i <= nA; ++i)
        for (int j = 0; j <= nB; ++j)
            for (int nxt_i = i; nxt_i <= nA; ++nxt_i)
                for (int nxt_j = j; nxt_j <= nB; ++nxt_j)
                    now[nxt_i][nxt_j] = min(now[nxt_i][nxt_j], max(lst[i][j], f[nxt_i - i][nxt_j - j]));
}

signed main() {
    memset(g, 0x3f, sizeof(g));
    g[0][0][0] = 0;
    kin >> nA >> nB >> p;
    for (int i = 1, tA, tB, kA, kB; i <= p; ++i) {
        kin >> tA >> tB >> kA >> kB;
        calc(tA, tB, kA, kB);
        contribute(g[i], g[i - 1]);
    }
    kout << g[p][nA][nB] << '\n';
    return 0;
}