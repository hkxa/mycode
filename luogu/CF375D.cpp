/*************************************
 * @problem:      Tree and Queries.
 * @author:       brealid.
 * @time:         2021-01-09.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;
#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 1e5 + 7;

int n, m, col[N], ans[N];
int cnt[N], tr[N];
vector<int> G[N];
vector<pair<int, int> > queries[N];
int siz[N], wson[N];

void add(int u) { if (u) for (; u <= n; u += u & -u) ++tr[u]; }
void del(int u) { if (u) for (; u <= n; u += u & -u) --tr[u]; }
int qry(int u) { int r(0); for (; u; u ^= u & -u) r += tr[u]; return r;}

void col_add(int c) { del(cnt[c]); add(++cnt[c]); }
void col_del(int c) { del(cnt[c]); add(--cnt[c]); }

void tree_add(int u, int father) {
    col_add(col[u]);
    for (size_t i = 0; i < G[u].size(); ++i) {
        int v = G[u][i];
        if (v == father) continue;
        tree_add(v, u);
    }
}
void tree_del(int u, int father) {
    col_del(col[u]);
    for (size_t i = 0; i < G[u].size(); ++i) {
        int v = G[u][i];
        if (v == father) continue;
        tree_del(v, u);
    }
}

void pre_dfs(int u, int father) {
    siz[u] = 1;
    for (size_t i = 0; i < G[u].size(); ++i) {
        int v = G[u][i];
        if (v == father) continue;
        pre_dfs(v, u);
        siz[u] += siz[v];
        if (siz[v] > siz[wson[u]]) wson[u] = v;
    }
}

void solve(int u, int father, bool is_need_to_remove) {
    for (size_t i = 0; i < G[u].size(); ++i) {
        int v = G[u][i];
        if (v == father || v == wson[u]) continue;
        solve(v, u, 1);
    }
    if (wson[u]) solve(wson[u], u, 0);
    col_add(col[u]);
    for (size_t i = 0; i < G[u].size(); ++i) {
        int v = G[u][i];
        if (v == father || v == wson[u]) continue;
        tree_add(v, u);
    }
    int total_color_type = qry(n);
    for (size_t i = 0; i < queries[u].size(); ++i)
        ans[queries[u][i].second] = total_color_type - qry(min(queries[u][i].first - 1, n));
    if (is_need_to_remove) tree_del(u, father);
}

signed main() {
    kin >> n >> m;
    for (int i = 1; i <= n; ++i) kin >> col[i];
    for (int i = 1, u, v; i < n; ++i) {
        kin >> u >> v;
        G[u].push_back(v);
        G[v].push_back(u);
    }
    for (int i = 1, u, k; i <= m; ++i) {
        kin >> u >> k;
        queries[u].push_back(make_pair(k, i));
    }
    pre_dfs(1, 0);
    solve(1, 0, 0);
    for (int i = 1; i <= m; ++i)
        kout << ans[i] << '\n';
    return 0;
}