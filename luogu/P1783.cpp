/*************************************
 * problem:      P1783.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-07-25.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

struct UFS {
    int fa[807];
    int familyCnt;

    UFS() : familyCnt(0)
    {
        memset(fa, -1, sizeof(fa));
    }

    int find(int u)
    {
        return fa[u] < 0 ? u : fa[u] = find(fa[u]);
    }

    void connect(int x, int y)
    {
        int xx = find(x), yy = find(y);
        if (xx == yy) return;
        if (fa[xx] > fa[yy]) swap(xx, yy);
        fa[xx] += fa[yy];
        fa[yy] = xx;
        familyCnt--;
    }

    bool isFamily(int u, int v)
    {
        return find(u) == find(v);
    }
};

int n, m;
double dis[807][807] = {0};
int x[807], y[807];

double calcDist(int i, int j)
{
    return sqrt((x[i] - x[j]) * (x[i] - x[j]) + (y[i] - y[j]) * (y[i] - y[j]));
}

bool check(double k)
{
    // printf("check %.2lf\n", k);
    UFS ufs;
    for (int i = 1; i <= m; i++) {
        for (int j = 1; j < i; j++) {
            if (dis[i][j] <= k) {
                ufs.connect(i, j);
            }
        }
    }
    for (int i = 1; i <= m; i++) {
        for (int j = 1; j <= m; j++) {
            if (ufs.isFamily(i, j) && x[i] <= k && x[j] >= n - k) {
                return true;
            }
        }
    }
    return false;
}

int main()
{
    n = read<int>();
    m = read<int>();
    for (int i = 1; i <= m; i++) {
        x[i] = read<int>();
        y[i] = read<int>();
        for (int j = 1; j < i; j++) {
            dis[i][j] = dis[j][i] = calcDist(i, j) / 2;
        }
    }
    double l = 1, r = sqrt((double)100001 * 100001 + 1001 * 1001), mid;
    while (r - l > 0.00063720) {
        mid = (l + r) / 2;
        if (check(mid)) r = mid;
        else l = mid;
    }
    printf("%.2lf", (l + r) / 2);
    return 0;
}