//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Roads and Planes 道路和航线.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-11.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 988244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

const int N = 25007;

int t, r, p, s;
vector<pair<int, int> > G[N], F[N];
vector<int> Fam[N];
int dis[N];
int fa[N], ind[N];
int find(int x) { return x == fa[x] ? x : fa[x] = find(fa[x]); }

struct Node {
    int u, dist;
    inline bool operator < (const Node &b) const {
        return dist > b.dist;
    }
    inline bool latest() const {
        return dis[u] == dist;
    }
};

void getReachable(int u) {
    dis[u] = 1;
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i].first;
        if (!dis[v]) getReachable(v);
    }
    for (size_t i = 0; i < F[u].size(); i++) {
        int v = F[u][i].first;
        if (!dis[v]) getReachable(v);
    }
}

void Dij(int x) {
    // printf("FAMILY : %d\n", x);
    priority_queue<Node> q;
    for (size_t i = 0; i < Fam[x].size(); i++) {
        int u = Fam[x][i];
        if (dis[u] ^ 0x3fffffff)
            q.push((Node){u, dis[u]});
    }
    while (!q.empty()) {
        while (!q.empty() && !q.top().latest()) q.pop();
        if (q.empty()) break;
        int u = q.top().u; q.pop();
        for (size_t i = 0; i < G[u].size(); i++) {
            int v = G[u][i].first, w = G[u][i].second;
            if (dis[v] > dis[u] + w) {
                dis[v] = dis[u] + w;
                q.push((Node){v, dis[v]});
            }
        }
    }
}

void SPFA(int x) {
    memarr(t, 0x3fffffff, dis);
    dis[x] = 0;
    queue<int> q;
    q.push(fa[x]);
    while (!q.empty()) {
        int Family = q.front(); q.pop();
        Dij(Family);
        for (size_t i = 0; i < Fam[Family].size(); i++) {
            int u = Fam[Family][i];
            for (size_t j = 0; j < F[u].size(); j++) {
                int v = F[u][j].first, w = F[u][j].second;
                if (dis[v] > dis[u] + w) {
                    dis[v] = dis[u] + w;
                }
                ind[fa[v]]--;
                if (!ind[fa[v]]) q.push(fa[v]);
            }
        }
    }
}

signed main() {
    t = read<int>();
    r = read<int>();
    p = read<int>();
    s = read<int>();
    for (int i = 1; i <= t; i++) fa[i] = i;
    { // <Read>
        register int a, b, c;
        for (int i = 1; i <= r; i++) {
            a = read<int>();
            b = read<int>();
            c = read<int>();
            G[a].push_back(make_pair(b, c));
            G[b].push_back(make_pair(a, c));
            fa[find(a)] = find(b);
        }
        for (int i = 1; i <= t; i++) 
            Fam[find(i)].push_back(i);
        for (int i = 1; i <= p; i++) {
            a = read<int>();
            b = read<int>();
            c = read<int>();
            F[a].push_back(make_pair(b, c));
            ind[fa[b]]++;
        }
    } // </Read>
    getReachable(s);
    for (int i = 1; i <= t; i++) {
        if (!dis[i]) {
            for (size_t j = 0; j < F[i].size(); j++) {
                ind[fa[F[i][j].first]]--;
            }
        }
    }
    SPFA(s);
    for (int i = 1; i <= t; i++)
        if (dis[i] ^ 0x3fffffff) write(dis[i], 10);
        else puts("NO PATH");
    return 0;
}

// Create File Date : 2020-06-11

/*
6 3 3 4
1 2 5
3 4 5
5 6 10
3 5 -100
4 6 -100
1 3 -10

6 3 3 4
2 1 5
4 3 5
6 5 10
3 5 -100
4 6 -100
1 3 -10

10 7 5 8
1 2 2
1 3 1
2 3 2
5 6 4
7 8 2
8 9 3
7 9 3
5 3 -2
8 2 0
10 9 -100
7 6 -3
10 3 -200
*/