/*************************************
 * @problem:      edge.
 * @author:       brealid.
 * @time:         2021-07-26.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 1e5 + 7;
int T, n, m;
struct edge {
    int v, id;
    // edge() {}
    edge(int V, int D) : v(V), id(D) {}
};
vector<edge> G[N];

namespace TEST_5000 {
    int path[5007], cnt;
    bool marked[5007];
    bool access_path(int u, int v, int from = 0) {
        if (u == v) {
            path[++cnt] = v;
            return true;
        }
        for (size_t i = 0; i < G[u].size(); ++i) {
            int to = G[u][i].v;
            if (to == from) continue;
            if (access_path(to, v, u)) {
                path[++cnt] = u;
                return true;
            }
        }
        return false;
    }
    void solve() {
        memset(marked, 0, n * sizeof(bool));
        for (int i = 1, op, u, v; i <= m; ++i) {
            kin >> op >> u >> v;
            cnt = 0;
            access_path(u, v);
            path[cnt + 1] = 0;
            if (op == 1) {
                for (int i = 1; i <= cnt; ++i) {
                    int now = path[i];
                    for (edge x: G[now])
                        if (x.v == path[i + 1] || x.v == path[i - 1]) marked[x.id] = true;
                        else marked[x.id] = false;
                }
            } else {
                int ans = 0;
                for (int i = 1; i <= cnt; ++i) {
                    int now = path[i];
                    for (edge x: G[now])
                        if (x.v == path[i + 1] && marked[x.id]) ++ans;
                }
                kout << ans << '\n';
            }
        }
    }
}

namespace TEST_chain {
    int nth[N], cnt;
    namespace segt {
        int tr[N << 2];
        void modify_0(int u, int l, int r, int p) {
            if (l == r) tr[u] = 0;
            else {
                int mid = (l + r) >> 1, ls = u << 1, rs = ls | 1;
                if (tr[u] != tr[ls] + tr[rs]) {
                    tr[ls] = mid - l + 1;
                    tr[rs] = r - mid;
                }
                if (p <= mid) modify_0(ls, l, mid, p);
                else modify_0(rs, mid + 1, r, p);
                tr[u] = tr[ls] + tr[rs];
            }
        }
        void modify_1(int u, int l, int r, int ml, int mr) {
            if (l >= ml && r <= mr) {
                tr[u] = r - l + 1;
            } else {
                int mid = (l + r) >> 1, ls = u << 1, rs = ls | 1;
                if (tr[u] != tr[ls] + tr[rs]) {
                    tr[ls] = mid - l + 1;
                    tr[rs] = r - mid;
                }
                if (ml <= mid) modify_1(ls, l, mid, ml, mr);
                if (mr > mid) modify_1(rs, mid + 1, r, ml, mr);
                tr[u] = tr[ls] + tr[rs];
            }
        }
        int query(int u, int l, int r, int ml, int mr) {
            if (l >= ml && r <= mr) return tr[u];
            int mid = (l + r) >> 1, ls = u << 1, rs = ls | 1, nRet = 0;
            if (tr[u] != tr[ls] + tr[rs]) {
                tr[ls] = mid - l + 1;
                tr[rs] = r - mid;
            }
            if (ml <= mid) nRet = query(ls, l, mid, ml, mr);
            if (mr > mid) nRet += query(rs, mid + 1, r, ml, mr);
            return nRet;
        }
    }
    bool check() {
        for (int i = 1; i <= n; ++i)
            if (G[i].size() > 2) return false;
        return true;
    }
    void flatten_chain_dfs(int u, int fr = 0) {
        nth[++cnt] = u;
        for (edge x: G[u])
            if (x.v != fr) flatten_chain_dfs(x.v, u);
    }
    void solve() {
        cnt = 0;
        for (int i = 1; i <= n; ++i)
            if (G[i].size() <= 1) {
                flatten_chain_dfs(i);
                break;
            }
        memset(segt::tr, 0, sizeof(int) * ((n << 2) + 7));
        for (int i = 1, op, u, v; i <= m; ++i) {
            kin >> op >> u >> v;
            if (nth[u] > nth[v]) swap(u, v);
            if (op == 1) {
                segt::modify_1(1, 1, n, nth[u], nth[v] - 1);
                if (nth[u] > 1) segt::modify_0(1, 1, n, nth[u] - 1);
                if (nth[v] < n) segt::modify_0(1, 1, n, nth[v]);
            } else {
                kout << segt::query(1, 1, n, nth[u], nth[v] - 1) << '\n';
            }
        }
    }
}
namespace TEST_B {
    int fa[N], siz[N], wson[N], dep[N], beg[N], dfn[N], dft;
    void dfs1(int u, int ff) {
        fa[u] = ff;
        dep[u] = dep[ff] + 1;
        siz[u] = 1;
        wson[u] = 0;
        for (edge x: G[u])
            if (x.v != ff) {
                dfs1(x.v, u);
                siz[u] += siz[x.v];
                if (siz[x.v] > siz[wson[u]]) wson[u] = x.v;
            }
    }
    void dfs2(int u, int cbg) {
        beg[u] = cbg;
        dfn[u] = ++dft;
        if (!wson[u]) return;
        dfs2(wson[u], cbg);
        for (edge x: G[u])
            if (x.v != fa[u] && x.v != wson[u])
                dfs2(x.v, x.v);
    }
    namespace segt {
        int tr[N << 2];
        void modify(int u, int l, int r, int ml, int mr, int v) {
            if (l >= ml && r <= mr) {
                tr[u] = v;
            } else {
                int mid = (l + r) >> 1, ls = u << 1, rs = ls | 1;
                if (tr[u]) {
                    tr[ls] = tr[rs] = tr[u];
                    tr[u] = 0;
                }
                if (ml <= mid) modify(ls, l, mid, ml, mr, v);
                if (mr > mid) modify(rs, mid + 1, r, ml, mr, v);
            }
        }
        int query(int u, int l, int r, int p) {
            if (l == r) return tr[u];
            int mid = (l + r) >> 1, ls = u << 1, rs = ls | 1;
            if (tr[u]) {
                tr[ls] = tr[rs] = tr[u];
                tr[u] = 0;
            }
            if (p <= mid) return query(ls, l, mid, p);
            else return query(rs, mid + 1, r, p);
        }
    }
    void solve() {
        // fprintf(stderr, "case B\n");
        dft = 0;
        dfs1(1, 0), dfs2(1, 1);
        memset(segt::tr, 0, sizeof(int) * ((n << 2) + 5));
        for (int i = 1, op, u, v; i <= m; ++i) {
            kin >> op >> u >> v;
            // fprintf(stderr, "op %d: %d %d %d\n", i, op, u, v);
            if (op == 1) {
                while (beg[u] != beg[v]) {
                    if (dep[beg[u]] < dep[beg[v]]) swap(u, v);
                    // fprintf(stderr, "[%d, %d] => %d\n", dfn[beg[u]], dfn[u], i);
                    segt::modify(1, 1, n, dfn[beg[u]], dfn[u], i);
                    u = fa[beg[u]];
                }
                if (dep[u] < dep[v]) swap(u, v);
                segt::modify(1, 1, n, dfn[v] + 1, dfn[u], i);
                segt::modify(1, 1, n, dfn[v], dfn[v], -i);
            } else {
                if (dep[u] < dep[v]) swap(u, v);
                int tu = segt::query(1, 1, n, dfn[u]), tv = segt::query(1, 1, n, dfn[v]);
                if (tv >= 0) kout << int(tu > 0 && tu >= tv) << '\n'; 
                else kout << int(tu + tv == 0) << '\n';
            }
        }
    }
}

signed main() {
    // file_io::set_to_file("edge");
    kin >> T;
    while (T--) {
        kin >> n >> m;
        for (int i = 1; i <= n; ++i) G[i].clear();
        for (int i = 1, u, v; i < n; ++i) {
            kin >> u >> v;
            G[u].push_back(edge(v, i)), G[v].push_back(edge(u, i));
        }
        if (n <= 5000) {
            TEST_5000::solve();
            continue;
        }
        if (TEST_chain::check()) {
            TEST_chain::solve();
            continue;
        }
        TEST_B::solve();
    }
    return 0;
}