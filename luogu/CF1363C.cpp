//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      Codeforces - Codeforces Round #646 (Div. 2).
 * @user_name:    hkxadpall.
 * @time:         2020-05-31.
 * @language:     C++.
 * @upload_place: Codeforces.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// #define int int64

int t;
int n, x;
vector<int> G[1007];

bool SG(int u, int fa) {
    if (G[u].size() == 1) return true;
    unsigned ans = 0;
    for (unsigned i = 0; i < G[u].size(); i++) {
        int v = G[u][i];
        if (v != fa) {
            ans += SG(v, u);
        }
    }
    return ~(ans & 1);
    // return ans < G[u].size();
}

signed main() {
    t = read<int>();
    while (t--) {
        n = read<int>();
        x = read<int>();
        for (int i = 1; i <= n; i++) G[i].clear();
        for (int i = 1, u, v; i < n; i++) {
            u = read<int>();
            v = read<int>();
            G[u].push_back(v);
            G[v].push_back(u);
        }
        if (n == 1) {
            puts("Ayush");
            continue;
        }
        if (G[x].size() == 1) {
            puts("Ayush");
            continue;
        }
        if (n & 1) puts("Ashish");
        else puts("Ayush");
        // if (SG(x, 0)) puts("Ayush");
        // else puts("Ashish");
    }
    return 0;
}