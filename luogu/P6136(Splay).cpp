// WA

/*
      Unfinished
      Status : 20/100 pts
      AC  : 2
      TLE : 1
      RE  : 7
*/

//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      P6136 【模板】普通平衡树（数据加强版） (Splay).
 * @user_id:      63720.
 * @user_name:    brealid.
 * @time:         2020-05-26.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int N = 1100007;

struct node {
    node *fa, *son[2];
    int val, siz, cnt;
    inline void clear() {
        fa = son[0] = son[1] = NULL;
        val = siz = cnt = 0;
    }
    node() {
        clear();
    }
    inline bool which(node *UnrecognizeNode) {
        return son[1] == UnrecognizeNode;
    }
    inline void pushUp() {
        siz = cnt + son[0]->siz + son[1]->siz;
    }
    int getSiz() {
        if (this == NULL) return 0;
        else return siz;
    }
};

struct MemoryPool {
    node UnusedNode[N];
    node *RecyclePool[N]; 
    int cntU, cntR;
    MemoryPool() : cntU(0), cntR(0) {}
    node *GetMem() {
        if (cntR) return RecyclePool[cntR--];
        else return &UnusedNode[++cntU];
    }
    node *Get() {
        node *ret = GetMem();
        ret->fa = ret->son[0] = ret->son[1] = &UnusedNode[0];
        return ret;
    }
    void Recycle(node *trash) {
        trash->clear();
        RecyclePool[++cntR] = trash;
    }
} pool;

node *nullNode = &pool.UnusedNode[0], *root = nullNode;

void Rotate(node *a, node *&k = nullNode) {
    // printf("Rotate(%d, %d)...\n", a, k);
    node *f = a->fa, *g = f->fa; 
    int d = f->which(a);
    // printf("S1... ");
    a->son[d ^ 1]->fa = f;      // Step 1
    f->son[d] = a->son[d ^ 1];  // Step 1
    // printf("S2... ");
    a->fa = g;                  // Step 2
    g->son[g->which(f)] = a;    // Step 2
    // printf("S3... ");
    f->fa = a;                  // Step 3
    a->son[d ^ 1] = f;          // Step 3
    // printf("pushUp... ");
    f->pushUp();
    a->pushUp();
    if (f == k) k = a; 
    // printf("Finished!\n");
    /// @b OriginalNode:a reach @b GoalNode:k
}

// let the node a Rotate to k
void Splay(node *a, node *&k) {
    // printf("Splay...\n");
    for (node *f = a->fa; a != k; f = a->fa) {
        // printf("Splay %d %d\n", a, k);
        if (f != k) Rotate(f->which(a) ^ f->fa->which(f) ? a : f, k);
        Rotate(a, k);
    }
}

node *find(int v) {
    node *t = root;
    while (t->val != v) {
        if (v < t->val) t = t->son[0];
        else t = t->son[1];
    }
    return t;
}

// Find the node which val = v and Splay it
void Splay(int v) {
    Splay(find(v), root);
}

void Insert(int v, node *&u, node *fa = nullNode) {
    // printf("Ins(%d)\n", v);
    if (u != nullNode) u->siz++;
    if (u == nullNode) {
        // printf("Find Node *\n");
        u = pool.Get();
        u->fa = fa;
        u->val = v;
        u->cnt = 1;
        u->siz = 1;
        // printf("Ready to Splay...\n");
        Splay(u, root);
        // printf("Splay Succeed!\n");
    } else if (v == u->val) {
        u->cnt++;
        Splay(u, root);
    } else if (v < u->val) {
        Insert(v, u->son[0], u);
    } else {
        Insert(v, u->son[1], u);
    }
}

void PreorderTraversal(node* u, int dep = 0) {
    if (u == nullNode) return;
    printf("| ");
    for (int i = 1; i <= dep; i++) printf("  ");
    printf("v(%d) siz(%d) cnt(%d)\n", u->val, u->getSiz(), u->cnt);
    PreorderTraversal(u->son[0], dep + 1);
    PreorderTraversal(u->son[1], dep + 1);
}

void Erase(int v) {
    // printf("Del(%d)\n", v);
    Splay(v);
    if (root->cnt > 1) {
        root->cnt--;
        // printf("Case 1 : cnt--\n");
    } else if (root->son[0] == nullNode || root->son[1] == nullNode) {
        node *son = (root->son[0] != nullNode ? root->son[0] : root->son[1]);
        son->fa = nullNode;
        pool.Recycle(root);
        root = son;
    } else {
        node *u = root->son[0];
        while (u->son[1] != nullNode) u = u->son[1];
        node *t = root;
        Splay(u, root);
        // PreorderTraversal(root);
        u->son[1] = t->son[1];
        t->son[1]->fa = u;
    }
}

int GetRank(int v) {
    Insert(v, root);
    Splay(v);
    int ans = root->son[0]->siz + 1;
    Insert(v, root);
    return ans;
}

int GetVal(int rank) {
    node *u = root;
    while (true) {
        // printf("v(%d) l(%d) r(%d)\n", u->val, u->son[0], u->son[1]);
        if (u->son[0]->getSiz() < rank && rank <= u->son[0]->getSiz() + u->cnt) return u->val;
        else if (rank <= u->son[0]->getSiz()) u = u->son[0];
        else rank -= u->son[0]->getSiz() + u->cnt, u = u->son[1];
    }
}

int GetPre(int v) {
    Insert(v, root);
    // PreorderTraversal(root);
    node *u = root->son[0];
    while (u->son[1] != nullNode) u = u->son[1];
    Erase(v);
    return u->val;
}

int GetNxt(int v) {
    Insert(v, root);
    // PreorderTraversal(root);
    node *u = root->son[1];
    while (u->son[0] != nullNode) u = u->son[0];
    Erase(v);
    return u->val;
}

int n, m;
int opt, x, lastans = 0, totAns = 0;

int main()
{
    n = read<int>();
    m = read<int>();
    for (int i = 1; i <= n; i++) Insert(read<int>(), root);
    for (int i = 1; i <= m; i++) {
        opt = read<int>();
        x = lastans ^ read<int>();
        // printf("Receive Option %d (argument = %d)\n", opt, x);
        printf("\rReceive Option %d (argument = %d)\n", opt, x);
        printf("deaing operation %d / %d", i, m);
        switch (opt) {
            case 1: Insert(x, root); break;  
            case 2: Erase(x); break;  
            case 3: totAns ^= (lastans = GetRank(x)); break;  
            case 4: totAns ^= (lastans = GetVal(x)); break;  
            case 5: totAns ^= (lastans = GetPre(x)); break;  
            case 6: totAns ^= (lastans = GetNxt(x)); break;  
            default: break;
        }
        // PreorderTraversal(root);
    }
    write(totAns, 10);
    return 0;
}