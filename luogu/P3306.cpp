/*************************************
 * @problem:      [SDOI2013] 随机数生成器.
 * @author:       brealid.
 * @time:         2021-01-07.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;
// #define USE_FREAD  // 使用 fread  读入，去注释符号
// #define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

namespace BSGS_func {
    inline int64 fpow(int64 a, int n, int64 p) {
        int64 res = 1;
        for (; n; n >>= 1) {
            if (n & 1) res = res * a % p;
            a = a * a % p;
        }
        return res;
    }
}

int64 BSGS(int64 a, int64 k, int64 p) {
    int64 sq = sqrt(p), inva = BSGS_func::fpow(a, p - 2, p), now = k;
    map<int64, int> mp;
    for (int64 i = 0; i < sq; ++i) {
        mp[now] = i;
        now = now * inva % p;
    }
    int64 a_psq = BSGS_func::fpow(a, sq, p);
    now = 1;
    for (int64 j = 0; j < p; j += sq) {
        if (mp.find(now) != mp.end()) return j + mp[now];
        now = now * a_psq % p;
    }
    return -1;
}

signed main() {
    for (int CASES = kin.get<int>(), p, a, b, x1, t; CASES --> 0; ) {
        kin >> p >> a >> b >> x1 >> t;
        if (x1 == t) kout << "1\n";
        else if (a == 0) {
            if (b == t) kout << "2\n";
            else kout << "-1\n";
            continue;
        } else if (a == 1) {
            if (b == 0) kout << "-1\n";
            else kout << (int64(t - x1) * BSGS_func::fpow(b, p - 2, p) % p + p) % p + 1 << '\n';
        } else {
            int dif = b * BSGS_func::fpow(a - 1, p - 2, p) % p;
            int res = BSGS(a, int64(t + dif) * BSGS_func::fpow(x1 + dif, p - 2, p) % p, p);
            if (~res) kout << res + 1 << '\n';
            else kout << "-1\n";
        }
    }
    return 0;
}