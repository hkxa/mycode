/*************************************
 * @problem:      Fairy.
 * @user_name:    brealid/hkxadpall/zhaoyi20/j(o)moo/littleTortoise.
 * @time:         2020-09-15.
 * @language:     C++.
 * @fastio_ver:   20200827.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        // simple io flags
        ignore_int = 1 << 0,    // input
        char_enter = 1 << 1,    // output
        flush_stdout = 1 << 2,  // output
        flush_stderr = 1 << 3,  // output
        // combination io flags
        endline = char_enter | flush_stdout // output
    };
    enum number_type_flags {
        // simple io flags
        output_double_stable = 1 << 0,  // output
        output_double_faster = 1 << 1   // output
        // combination io flags
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f & ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore character set : ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (double &lf) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            lf = endch & 15;
            while (isdigit(endch = getchar())) lf = lf * 10 + (endch & 15);
            if (endch == '.') {
                double digit = 0.1;
                while (isdigit(endch = getchar())) {
                    lf += (endch & 15) * digit;
                    digit *= 0.1;
                }
            }
            if (flag) lf = -lf;
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            Int d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return d;
        }
    } read;

    struct Writer {
        Writer& operator << (io_flags f) {
            if (f & char_enter) putchar(10);
            if (f & flush_stdout) fflush(stdout);
            if (f & flush_stderr) fflush(stderr);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
        inline void operator () (double val, int eps_digit = 6, char endch = '\0', number_type_flags flg = output_double_faster) {
            if (flg & output_double_stable) {
                static char output_format[10];
                sprintf(output_format, "%%.%dlf", eps_digit);
                printf(output_format, val);
            } else if (flg & output_double_faster) {
                if (val < 0) {
                    putchar('-');
                    val = -val;
                }
                (*this) << (int64)val;
                val -= (int64)val;
                putchar('.');
                double eps_number = 0.5;
                for (int i = 1; i <= eps_digit; i++) eps_number /= 10;
                val += eps_number;
                while (eps_digit--) {
                    val *= 10;
                    putchar((int)val | '0');
                    val -= (int)val;
                }
            } else {
                (*this) << "1.#ERROR_NOT_IDENTIFIED_FLAG";
            }
            if (endch) putchar(endch);
        }
    } write;
}
using namespace Fastio;
namespace File_IO {
    void init_IO() {
        freopen("Fairy.in", "r", stdin);
        freopen("Fairy.out", "w", stdout);
    }
}

// #define int int64

const int N = 10000 + 7;

int n, m, k, cnt, tot, thm, flag;
int num[N], c[N], f[N], p[N];
int dep[N], ans[N], fa[N][20];
bool vis[N];

struct node {
    int u, v, id;
} a[N], b[N];

struct edge {
    int v, next, id;
} e[N * 2];

int findSet(int x) {
    return p[x] == x ? x : p[x] = findSet(p[x]);
}

void dfs(int u, int par) {
    fa[u][0] = par;
    dep[u] = dep[par] + 1;
    for (int i = 1; i < 20; i++)
        fa[u][i] = fa[fa[u][i - 1]][i - 1];
    for (int i = f[u]; i; i = e[i].next)
        if (e[i].v ^ par) {
            num[e[i].v] = e[i].id;
            dfs(e[i].v, u);
        }
}

int Lca(int u, int v) {
    for (int i = 19; i >= 0; i--) {
        if (dep[fa[u][i]] >= dep[v])
            u = fa[u][i];
        if (dep[fa[v][i]] >= dep[u])
            v = fa[v][i];
    }
    if (u == v) return u;
    for (int i = 19; i >= 0; i--)
        if (fa[u][i] ^ fa[v][i])
            u = fa[u][i], v = fa[v][i];
    return fa[u][0];
}

void make(int u, int par) {
    vis[u] = 1;
    for (int i = f[u]; i; i = e[i].next) {
        int v = e[i].v;
        if (v == par)
            continue;
        make(v, u);
        c[u] += c[v];
    }
}

int main() {
    read >> n >> m;
    for (int i = 1; i <= n; i++) p[i] = i;
    for (int i = 1; i <= m; i++) a[i] = (node){read.get<int>(), read.get<int>(), i};
    for (int i = 1; i <= m; i++) {
        int x = findSet(a[i].u), y = findSet(a[i].v);
        if (x ^ y) {
            p[x] = y;
            e[++tot] = edge{a[i].u, f[a[i].v], a[i].id}, f[a[i].v] = tot;
            e[++tot] = edge{a[i].v, f[a[i].u], a[i].id}, f[a[i].u] = tot;
        } else b[++k] = a[i];
    }
    for (int i = 1; i <= n; i++)
        if (!dep[i]) dfs(i, 0);
    for (int i = 1; i <= k; i++) {
        int u = b[i].u, v = b[i].v, lca = Lca(u, v);
        int len = dep[u] + dep[v] - dep[lca] * 2, delta = 1;
        if (len & 1) delta = -1;
        else flag++, thm = b[i].id;
        c[u] += delta, c[v] += delta, c[lca] -= 2 * delta;
    }
    for (int i = 1; i <= n; i++)
        if (!vis[i]) make(i, 0);
    if (!flag)
        for (int i = 1; i <= m; i++) ans[++cnt] = i;
    else
        for (int i = 1; i <= n; i++)
            if (c[i] == flag)
                ans[++cnt] = num[i];
    if (flag == 1) ans[++cnt] = thm;
    sort(ans + 1, ans + 1 + cnt);
    write << cnt << '\n';
    for (int i = 1; i <= cnt; i++)
        write << ans[i] << " \n"[i == cnt];
    return 0;
}