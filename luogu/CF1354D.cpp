// #pragma GCC optimize(1)
// #pragma GCC optimize(2)
// #pragma GCC optimize(3)

// #include
#include <bits/stdc++.h>
using namespace std;
 
typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;
 
template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}
 
template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}
 
template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  
 
template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, q;

int t[1000007];
int qsum(int x) {
    int ret = 0;
    while (x) {
        ret += t[x];
        x -= (x & (-x));
    }
    return ret;
}
void upd(int x, int dif) {
    while (x <= n) {
        t[x] += dif;
        x += (x & (-x));
    }
}
 
int main(){
    n = read<int>();
    q = read<int>();
    for (int i = 1; i <= n; i ++) upd(read<int>(), 1);
    int Z = n;
    int l, r, mid, ans;
    for(int i = 1;i <= q;i++){
        // printf("Q %d\n", i);
        int cmd = read<int>();
        if(cmd > 0)upd(cmd, 1), Z++;
        else {
            l = 1; r = n; ans = n;
            while (l <= r) {
                // printf("l %d, r %d\n", l, r);
                mid = (l + r) >> 1;
                if (qsum(mid) >= -cmd) {
                    r = mid - 1;
                    ans = mid;
                } else l = mid + 1;
            }
            // printf ("^&");
            upd(ans, -1);
            Z--;
        }
    }
    if (Z) {
        l = 1; r = n; ans = n;
        while (l <= r) {
            mid = (l + r) >> 1;
            if (qsum(mid) >= 1) {
                r = mid - 1;
                ans = mid;
            } else l = mid + 1;
        }
        write(ans, 10);
    } else puts("0");
    return 0;
}