/*************************************
 * problem:      P2395 BBCode转换Markdown.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-26.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define puts_return(x) { puts(x); return 0; }
#define write_return(x) { write(x); return 0; }

#ifdef DEBUG
# define passing() cerr << "passing line [" << __LINE__ << "]." << endl
# define debug(...) printf(__VA_ARGS__)
# define show(x) cerr << #x << " = " << x << endl
#else
# define passing() do if (0) cerr << "passing line [" << __LINE__ << "]." << endl;  while(0)
# define debug(...) do if (0) printf(__VA_ARGS__); while(0)
# define show(x) do if (0) cerr << #x << " = " << x << endl; while(0)
#endif

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

typedef int32 BBcode_resolver_retVal;

class BBcode_resolver {
  private:
    string code;
    int len, pos;
    stack<string> tag;
    bool quote;
  public:
    BBcode_resolver() : len(0), pos(0), quote(0) {}
    BBcode_resolver(istream in) : len(0), pos(0), quote(0)
    {
        in >> code;
        len = code.length();
    }
    BBcode_resolver(FILE *in) : len(0), pos(0), quote(0)
    {
        char ch;
        while (fscanf(in, "%c", &ch) != EOF) code += ch;
        len = code.length();
    }
    BBcode_resolver(char *copy) : len(0), pos(0), quote(0)
    {
        code = copy;
        len = code.length();
    }
    string get_type()
    {
        static string ret;
        debug("[info] get from with pos *%d\n", pos);
        while (code[pos] != '[' && pos < len) pos++;
        if (pos >= len) return "get nothing";
        pos += 3;
        if (code[pos - 2] == '/')  
            ret = string() + code[pos - 1] + code[pos] + '*';
        else 
            ret = string() + code[pos - 2] + code[pos - 1];
        if (quote) {
            if (ret != "qu*") return get_type();
            else quote = false;
        }
        return ret;
    }
    string get_tag()
    {
        debug("[info] get from with pos *%d\n", pos);
        while (code[pos] != '[' && pos < len) {
            answer += code[pos];
            if (code[pos] == '\n' && quote) answer += "> ";
            pos++;
        }
        string ret;
        while (code[pos] != ']' && pos < len) ret += code[pos++];
        return ret + code[pos++];
    }

    bool check()
    {
        string t;
        while (true) {
            t = get_type();
            if (pos >= len) break;
            if (t.length() == 3) {
                if (tag.empty()) puts_return("Match Error");
                if (tag.top() != t.substr(0, 2)) puts_return("Match Error");
                // printf("pop %s\n", tag.top().c_str());
                tag.pop();
            } else {
                tag.push(t);
                // printf("push %s\n", t.c_str());
                if (t == "qu") quote = true;
            }
        }
        if (tag.empty()) return 1;
        else puts_return("Unclosed Mark");
    }

    bool isEmptyLine(char ch)
    {
        return ch == ' ' || ch == '>' || ch == '\n';
    }

    void solve()
    {
        pos = 0;
        string all, t;
        while (true) {
            all = get_tag();
            if (pos >= len) break;
            if (quote && all != "[/quote]") {
                answer += all;
                continue;
            }
            if (all[1] == '/') {
                t = all.substr(2, 2);
                if (t == "h1") answer += " #";
                else if (t == "h2") answer += " ##";
                else if (t == "i]") answer += '*';
                else if (t == "b]") answer += "__";
                else if (t == "ur") answer += string() + "](" + tag.top() + ')';
                else if (t == "im") answer += string() + "](" + tag.top() + ')';
                else {
                    quote = false;
                    while (isEmptyLine(answer[answer.length() - 1])) answer = answer.substr(0, answer.length() - 1);
                    if (code[pos] != '\n') answer += '\n';
                }
            } else {
                t = all.substr(1, 2);
                if (t == "h1") answer += "# ";
                else if (t == "h2") answer += "## ";
                else if (t == "i]") answer += '*';
                else if (t == "b]") answer += "__";
                else if (t == "ur") answer += '[', tag.push(all.substr(5, all.length() - 6));
                else if (t == "im") answer += "![", tag.push(all.substr(5, all.length() - 6));
                else {
                    quote = true;
                    while (code[pos] == '\n') pos++;
                    answer += "\n> ";
                }
            }
        }
        cout << answer;
    }
};

int main()
{
    BBcode_resolver br(stdin);
    if (br.check()) {
        br.solve();
    }
    return 0;
}