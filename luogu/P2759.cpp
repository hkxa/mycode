/*************************************
 * problem:      P2759 奇怪的函数.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-05.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

long long n, l = 1, r = 238723448, mid, ans;

#define powerNumLen(x) ((long long)((x) * log10(x) + 1))

int main()
{
    n = read<long long>();
    while (l <= r) {
        mid = (l + r) >> 1;
        if (powerNumLen(mid) >= n) {
            r = mid - 1;
            ans = mid;
        } else {
            l = mid + 1;
        }
    }
    write(ans, 10);
    return 0;
}