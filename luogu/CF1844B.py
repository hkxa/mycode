T = int(input())
for _ in range(T):
    n = int(input())
    if n == 1:
        print(1)
    elif n == 2:
        print(1, 2)
    else:
        num = 4
        print(2, end = ' ')
        for i in range((n - 3) // 2):
            print(num, end = ' ')
            num += 1
        print(1, end = ' ')
        for i in range((n - 3 + 1) // 2):
            print(num, end = ' ')
            num += 1
        print(3)