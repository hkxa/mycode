/*************************************
 * problem:      P2327 [SCOI2005]ɨ��.
 * user ID:      63720.
 * user name:    �����Ű�.
 * time:         2019-03-19.
 * language:     C++.
 * upload place: Luogu.
*************************************/
/*************************************
 * record ID:    R17389240.
 * time:         29 ms
 * memory:       792 KB
*************************************/  

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

int n;
int cnt[10007];
int ans = 2;

int main()
{
	n = read<int>();
	for (int i = 1; i <= n; i++) {
		cnt[i] = read<int>();
	}
	int l1, l2, l3;
	l1 = 0;
	l2 = 0;
	for (int i = 1; i <= n; i++) {
		l3 = cnt[i] - l1 - l2;
		if ((l3 != 1 && l3 != 0) || (i == n && l3 != 0)) {
			ans--;
			break;
		} 
		l1 = l2;
		l2 = l3;
	}
	l1 = 0;
	l2 = 1;
	for (int i = 1; i <= n; i++) {
		l3 = cnt[i] - l1 - l2;
		if ((l3 != 1 && l3 != 0) || (i == n && l3 != 0)) {
			ans--;
			break;
		}
		l1 = l2;
		l2 = l3;
	}
	write(ans);
	return 0;
}
