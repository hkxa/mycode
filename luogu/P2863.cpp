/*************************************
 * problem:      P2863 [USACO06JAN]牛的舞会The Cow Prom.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-07-12.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

int n, m;
vector<int> G[100007];
vector<int> S[200007];
typedef vector<int>::iterator Vec_it;
int dfn[100007] = {0}, low[100007] = {0}, cnt = 0;
bool ins[100007] = {0};
int s[100007] = {0}, top = 0;
int kind[100007] = {0}, kinds = 0;
int kindCnt[100007] = {0};
int in[100007] = {0};
int out[100007] = {0};

void tarjan(int u)
{
    low[u] = dfn[u] = ++cnt;
    ins[u] = true;
    s[++top] = u;
    for (Vec_it it = G[u].begin(); it != G[u].end(); it++) {
        if (!dfn[*it]) {
            tarjan(*it);
            low[u] = min(low[u], low[*it]);
        } else if (ins[*it]) {
            low[u] = min(low[u], low[*it]);
        }
    }
    if (dfn[u] == low[u]) {
        kind[u] = ++kinds;
        ins[u] = 0;
        kindCnt[kinds] = 1;
        while (s[top] != u) {
            kind[s[top]] = kinds;
            ins[s[top--]] = 0;
            kindCnt[kinds]++;
        }
        top--;
    }
}

void tarjan_all()
{
    for(int i = 1; i <= n; i++) {
        if (!dfn[i]) tarjan(i);
    }
}

#define deal(id) ((id) > kinds ? ((id) - kinds) : (id))
#define start kind[1]

/* DAG-longest-road-spfa DLRS */
int DLRS()
{
    queue<int> q;
    int dis[200007];
    memset(dis, 0, sizeof(dis));
    q.push(start);
    while (!q.empty()) {
        int fr = q.front();
        q.pop();
        for (Vec_it it = S[fr].begin(); it != S[fr].end(); it++) {
            if (dis[*it] < dis[fr] + kindCnt[deal(*it)]) {
                dis[*it] = dis[fr] + kindCnt[deal(*it)];
                q.push(*it);
            }
        }
    }
    return dis[start + kinds];
}

void P3119()
{
    for (int i = 1; i <= n; i++) {
        for (Vec_it it = G[i].begin(); it != G[i].end(); it++) {
            if (kind[i] == kind[*it]) continue;
            S[kind[i]].push_back(kind[*it]);
            S[kind[i] + kinds].push_back(kind[*it] + kinds);
            S[kind[*it]].push_back(kind[i] + kinds);
        }
    }
    S[start].push_back(start + kinds);
    int ans = DLRS(); // DAG-longest-road-spfa DLRS
    write(ans, 0);
}

void P2863()
{
    int ans(0);
    for (int i = 1; i <= kinds; i++) {
        if (kindCnt[i] > 1) ans++;
    }
    write(ans, 0);
}

int main()
{
    n = read<int>();
    m = read<int>();
    int u, v;
    for (int i = 1; i <= m; i++) {
        u = read<int>();
        v = read<int>();
        G[u].push_back(v);
    }
    tarjan_all();
    P2863();
    return 0;
}