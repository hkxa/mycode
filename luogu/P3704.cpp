/*************************************
 * @problem:      [SDOI2017]数字表格.
 * @author:       brealid.
 * @time:         2021-03-15.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 1e6 + 7, P = 1e9 + 7;

inline int64 fpow(int64 a, int64 b) {
    int64 ret = 1;
    while (b) {
        if (b & 1) ret = ret * a % P;
        a = a * a % P;
        b >>= 1;
    }
    return ret;
}
#define inv(x) fpow(x, P - 2)

namespace euler {
    bool IsntPrime[N];
    int prs[N], pcnt, mu[N];
    unsigned a[N];

    void init_prs(int n) {
        mu[1] = 1;
        for (int i = 2; i <= n; ++i) {
            if (!IsntPrime[i]) {
                prs[++pcnt] = i;
                mu[i] = -1;
            }
            for (int j = 1; j <= pcnt && prs[j] * i <= n; ++j) {
                IsntPrime[prs[j] * i] = true;
                if (i % prs[j] == 0) break;
                mu[prs[j] * i] = -mu[i];
            }
        }
    }
}

namespace fib {
    int f[N], invf[N];
    void init(int n) {
        invf[1] = f[1] = 1;
        for (int i = 2; i <= n; ++i) {
            f[i] = (f[i - 1] + f[i - 2]) % P;
            invf[i] = inv(f[i]);
        }
    }
}

namespace F {
    int64 f[N], sum[N];
    void init(int n) {
        for (int g = 0; g <= n; ++g) f[g] = 1;
        sum[0] = 1;
        for (int g = 1; g <= n; ++g) {
            for (int d = 1; d * g <= n; ++d) // T = d * g
                switch (euler::mu[d]) {
                    case 1: f[g * d] = f[g * d] * fib::f[g] % P; break;
                    case -1: f[g * d] = f[g * d] * fib::invf[g] % P; break;
                }
            sum[g] = sum[g - 1] * f[g] % P;
            // printf("f[%d] = %lld\n", g, f[g]);
        }
    }
    int64 prod(int l, int r) {
        return sum[r] * inv(sum[l - 1]) % P;
    }
}

signed main() {
    euler::init_prs(1e6);
    fib::init(1e6);
    F::init(1e6);
    int T, n, m;
    for (kin >> T; T--;) {
        kin >> n >> m;
        int64 ans = 1;
        for (int l = 1, r, bound = min(n, m); l <= bound; l = r + 1) {
            r = min(n / (n / l), m / (m / l));
            ans = ans * fpow(F::prod(l, r), int64(n / l) * (m / l) % (P - 1)) % P;
        }
        kout << ans << '\n';
    }
    return 0;
}