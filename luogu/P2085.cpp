/*************************************
 * problem:      P2085 最小函数值.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-03-31.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

struct function {
    int a, b, c;
    int x;
    int value;

    int getv()
    {
        return a * x * x + b * x + c;
    }

    void resetx(int xNum)
    {
        x = xNum;
        value = getv();
    }

    void readIn()
    {
        a = read<int>();
        b = read<int>();
        c = read<int>();
        resetx(1);
    }

    void updatex(int diff)
    {
        x += diff;
        value = getv();
    }

    bool operator < (const function &b) const 
    {
        return value > b.value;
    }
} t;

priority_queue <function> q;
int n, m;

int main()
{
    n = read<int>();
    m = read<int>();
    for (int i = 1; i <= n; i++) {
        t.readIn();
        q.push(t);
    }
    while (m--) {
        t = q.top();
        q.pop();
        write(t.value, ' ');
        t.updatex(+1);
        q.push(t);
    }
    return 0;
}