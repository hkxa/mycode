#include <bits/stdc++.h>
using namespace std;
#define Inf 0x3f3f3f3f

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

bool found = false;
long long minn = Inf, h[1010] = {0}, ans[1010] = {0};
bool mp[1000 + 7];

long long gcd(long long a, long long b)
{
    return b ? gcd(b, a % b) : a;
}

struct Fraction {
    long long son, ma;

    Fraction(int s, int m)
    {
        son = s;
        ma = m;
    }

    void reduction()
    {
        long long g = gcd(son, ma);
        son /= g;
        ma /= g;
    } 

    Fraction operator + (const Fraction &b) 
    {
        Fraction cnt(0, 1);
        cnt.son = son * b.ma + ma * b.son;
        cnt.ma = ma * b.ma;
        cnt.reduction();
        return cnt;
    }
    Fraction operator - (const Fraction &b) 
    {
        Fraction cnt(0, 1);
        cnt.son = son * b.ma - ma * b.son;
        cnt.ma = ma * b.ma;
        cnt.reduction();
        return cnt;
    }
    Fraction operator * (Fraction b){
        Fraction cnt(son * b.son, ma * b.ma);
        cnt.reduction();
        return cnt;
    }
    Fraction operator / (Fraction b){
        Fraction cnt(son * b.ma, ma * b.son);
        cnt.reduction();
        return cnt;
    }
    bool operator > (Fraction b)
    {
        return ((son * b.ma) > (ma * b.son));
    }
    bool operator < (Fraction b)
    {
        return ((son * b.ma) < (ma * b.son));
    }
};

Fraction make(long long a, long long b)
{
    Fraction z(a, b);
    return z;
}

void dfs(long long depth, long long step, Fraction w)
{
    w.reduction();
    if (step == depth) {
        if(w.son == 1 && w.ma < minn && w.ma > h[step - 1])
        {
            if (w.ma <= 1000 && mp[w.ma]) return;
            found = 1;
            minn = w.ma;
            for (long long i = 1; i < step; i++)
            {
                ans[i] = h[i];//copy answer
            }
            ans[step] = w.ma;//copy last answer
        }
        return;//found answer
    }
    long long j = h[step - 1] + 1;//bigger than last one's denominator
    Fraction smt = w * make(1, depth - step + 1);//smallest
    while (w < make(1, j)) j++;
    while (make(1, j) > smt){
        if (mp[j]) {
            j++;
            continue;
        }
        h[step] = j;
        // printf("try:%lld/%lld - 1/%lld = %lld/%lld.\n", w.son, w.ma, j, (w - make(1, j)).son, (w - make(1, j)).ma);
        dfs(depth, step + 1, w - make(1, j));//DFS continue
        j++;
    }
} 

int solveCase(int caseId)
{
    found = false;
    minn = Inf;
    memset(mp, 0, sizeof(mp));
    int a = read<int>(), b = read<int>();
    printf("Case %d: %d/%d=", caseId, a, b);
    Fraction n(a, b);
    // n.reduction();
    // if (a == 1) {
    //     printf("%d", b);
    //     return 0;
    // }
    int k = read<int>();
    while (k--) mp[read<int>()] = true;
    long long depth = 1;
    while (true) {
        h[0] = 1;
        // printf("visit depth %d : ", depth);
        dfs(depth, 1, n);
        // printf("end.\n");
        if (found) {
//          printf("depth = %d\n", depth);
            for (long long i = 1; i <= depth; i++) {
                printf("1/%lld%c", ans[i], "+\n"[i == depth]);
            }  
            return 0;
        }
        depth++;
    }
}

int main()
{
    int Tc = read<int>();
    for (int id = 1; id <= Tc; id++) solveCase(id);
}