//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      P4196 [CQOI2006]凸多边形 /【模板】半平面交.
 * @user_id:      63720.
 * @user_name:    brealid.
 * @time:         2020-05-21.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

template <typename T>
int compare(T x, T eps = 1e-8) {
    if (abs(x) < eps) return 0;
    else return x > 0 ? 1 : -1;
}

template <typename T>
struct Vec {
    T x, y;
    Vec() {}
    Vec(T X, T Y) : x(X), y(Y) {}

    template <typename MultipleType>
    Vec<T> operator * (const MultipleType k) {
        return Vec(x * k, y * k);
    }

    template <typename MultipleType>
    Vec<T> operator / (const MultipleType k) {
        if (k) return Vec(x / k, y / k);
        else return Vec(NAN, NAN);
    }

    Vec<T> operator + (const Vec<T> b) const {
        return Vec(x + b.x, y + b.y);
    }

    Vec<T> operator - (const Vec<T> b) const {
        return Vec(x - b.x, y - b.y);
    }
    
    T length() {
        return sqrt(x * x + y * y);
    }
    
    T angle() {
        return atan2(y, x);
    }
};

// template <typename T>
// struct Point : Vec<T> {};
#define Point Vec

template <typename T>
T metrix(Vec<T> a, Vec<T> b) {
    return a.x * b.x + a.y * b.y;
}

template <typename T>
T cross(Vec<T> a, Vec<T> b) {
    // printf("cross (%.3lf, %.3lf) & (%.3lf, %.3lf)\n", a.x, a.y, b.x, b.y);
    return a.x * b.y - a.y * b.x;
}

template <typename T>
T dist(Vec<T> a, Vec<T> b) {
    return (a - b).length();
}

template <typename T>
struct Line {
    Point<T> x;
    Vec<T> v;
    T angle;
    Line() {}
    Line(Point<T> X, Vec<T> V) : x(X), v(V) { angle = atan2(v.y, v.x); }
    bool operator < (const Line<T> &b) const {
        return compare(angle - b.angle) ? 
               compare(angle - b.angle) < 0 : 
               compare(cross(v, b.x + b.v - x)) > 0;
    }
};

template <typename T>
Point<T> interPoint(Line<T> a, Line<T> b) {
    Vec<T> u(a.x - b.x);
    T k = cross(u, b.v) / cross(b.v, a.v);
    return a.x + a.v * k;
}

template <typename T>
bool check(Line<T> i, Line<T> j, Line<T> k) {
    Point<T> p = interPoint(i, j);
    // printf("interPoint() = (%.3lf, %.3lf)\n", p.x, p.y);
    return compare(cross(k.v, p - k.x)) < 0;
}

const int N = 10 + 3, M = 50 + 3, NM = 500 + 3;
int n, m, cnt;
Line<double> a[NM], q[NM];
Point<double> t[NM];
int l = 1, r = 0;

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        m = read<int>();
        for (int i = 1; i <= m; i++) {
            scanf("%lf%lf", &t[i].x, &t[i].y);
        }
        for (int i = 1; i < m; i++) a[++cnt] = Line<double>(t[i], t[i + 1] - t[i]);
        a[++cnt] = Line<double>(t[m], t[1] - t[m]);
    }
    sort(a + 1, a + cnt + 1);
    // for (int i = 1; i <= cnt; i++) 
    //     printf("(%.3lf, %.3lf) ->(%.3lf, %.3lf)\n", a[i].x.x, a[i].x.y, a[i].v.x, a[i].v.y);
    int tmpCnt = cnt;
    cnt = 0;
	for (int i = 1; i <= tmpCnt; i++) {
		if (compare(a[i].angle - a[i - 1].angle)) cnt++;
		a[cnt] = a[i];
	}
    // printf("-----------------------------------\n");
    for (int i = 1; i <= cnt; i++) 
        printf("(%.3lf, %.3lf) ->(%.3lf, %.3lf)\n", a[i].x.x, a[i].x.y, a[i].v.x, a[i].v.y);
    q[++r] = a[1]; q[++r] = a[2];
    for (int i = 3; i <= cnt; i++) {
		while (l < r && check(q[r - 1], q[r], a[i])) r--;
		while (l < r && check(q[l + 1], q[l], a[i])) l++;
		q[++r] = a[i];
        // printf("l = %d, r = %d\n", l, r);
	}
	while (l < r && check(q[r - 1], q[r], q[l])) r--;
	while (l < r && check(q[l + 1], q[l], q[r])) l++;
	q[r + 1] = q[l];
    // printf("l = %d, r = %d\n", l, r);
    for (int i = l; i <= r; i++) t[i] = interPoint(q[i], q[i + 1]);
    double S = cross(t[r], t[l]);
    for (int i = l; i < r; i++) S += cross(t[i], t[i + 1]);
    printf("%.3lf\n", S / 2);
    return 0;
}