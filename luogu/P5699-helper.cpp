// run P5699 <P5699_addition_file\match1.in >P5699_addition_file\match1.usr
// P5699_addition_file\checker P5699_addition_file\match1.in P5699_addition_file\match1.usr EMPTY.txt
// run P5699 -f -q <P5699_addition_file\match10.in >P5699_addition_file\match10.usr & P5699_addition_file\checker P5699_addition_file\match10.in P5699_addition_file\match10.usr EMPTY.txt
#pragma GCC optimize("-O3")
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    register Int flag = 1;
    register char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    register Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    register Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    register Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;
char Path[1007], Path2[1007];

int main()
{
    fprintf(stderr, "enter program.\n");
    freopen("OptArr.P5699-Ans", "w", stdout);
    fprintf(stderr, "opened file.\n");
    printf("int ans6_10[11][512 + 7] = {\n");
    for (int i = 6; i <= 10; i++) {
        fprintf(stderr, "writing %d.\n", i);
        sprintf(Path, "P5699_addition_file\\match%d.usr", i);
        sprintf(Path2, "P5699_addition_file\\match%d.in", i);
        int n;
        FILE * F = fopen(Path2, "r");
        fscanf(F, "%d", &n);
        freopen(Path, "r", stdin);
        printf("    { ");
        for (int j = 1; j <= n; j++) {
            printf("%d%c ", read<int>(), ", "[j == n]);
        }
        printf("}%c\n", ", "[i == 10]);
    }
    printf("};");
    return 0;
}