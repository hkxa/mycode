//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Bear and Prime Numbers.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-06.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

namespace against_cpp11 {
    const int N = 1e7 + 7;
    int n, q;
    bool isnp[N];
    int primes[N], cnt;
    int64 mark[N];
    void pretreat_prime_table(int maxn = 1e7) {
        for (int i = 2; i <= maxn; i++) {
            if (!isnp[i]) {
                primes[++cnt] = i;
                for (int j = i + i; j <= maxn; j += i)
                    isnp[j] = true;
            }
        }
    }
    signed main() {
        pretreat_prime_table();
        primes[++cnt] = 1e9 + 7;
        // printf("Primes from %d to %d : total count = %d\n", 1, (int)1e7, cnt);
        read >> n;
        for (int i = 1, x, j; i <= n; i++) {
            read >> x;
            j = 1;
            while (x != 1 && primes[j] * primes[j] <= x) {
                if (x % primes[j] == 0) {
                    mark[j]++;
                    while (x % primes[j] == 0) x /= primes[j];
                }
                j++;
            }
            if (x != 1) {
                j = lower_bound(primes + 1, primes + cnt + 1, x) - primes;
                mark[j]++;
            }
        }
        for (int i = 2; i <= cnt; i++) mark[i] += mark[i - 1];
        read >> q;
        for (int i = 1, l, r; i <= q; i++) {
            read >> l >> r;
            l = lower_bound(primes + 1, primes + cnt + 1, l) - primes;
            r = upper_bound(primes + 1, primes + cnt + 1, r) - primes - 1;
            write << mark[r] - mark[l - 1] << '\n';
        }
        return 0;
    }
}

signed main() { return against_cpp11::main(); }