//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      P1717 钓鱼.
 * @user_id:      63720.
 * @user_name:    brealid.
 * @time:         2020-06-01.
 * @language:     C++.
 * @upload_place: luogu.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// #define int int64
const int N = 25 + 4, HT = 16 * 60 + 7;

int H, n;
int f[N], d[N], t[N];
int fish[N][HT];
int dp[N][HT];
#define dist(l, r) (t[r] - t[l])

signed main() {
    n = read<int>();
    H = read<int>() * 12;
    for (int i = 1; i <= n; i++) f[i] = read<int>();
    for (int i = 1; i <= n; i++) d[i] = read<int>();
    for (int i = 2; i <= n; i++) t[i] = t[i - 1] + read<int>();
    for (int i = 1; i <= n; i++) {
        int now = f[i];
        for (int j = 1; j <= H; j++) {
            fish[i][j] = fish[i][j - 1] + now;
            now = max(now - d[i], 0);
        }
    }
    memset(dp, 0xcf, sizeof(dp));
    dp[0][0] = 0;
    for (int i = 1; i <= n; i++) {
        for (int j = 0; j < i; j++) {
            for (int k = dist(j, i); k <= H; k++) {
                for (int p = k; p <= H; p++) {
                    dp[i][p] = max(dp[i][p], dp[j][k - dist(j, i)] + fish[i][p - k]);
                }
            }
        }
    }
    // for (int i = 1; i <= n; i++)
    //     for (int j = 0; j <= H; j++)
    //         printf("%-3d%c", dp[i][j], " \n"[j == H]);
    int ans = 0;
    for (int i = 1; i <= n; i++) ans = max(ans, dp[i][H]);
    write(ans, 10);
    return 0;
}

// Create File Date : 2020-06-01

/*
2 1
10 1
2 5
2
*/