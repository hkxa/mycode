def fpow(x, n, P):
    ret = 1
    while n != 0:
        if n & 1:
            ret = ret * x % P
        x = x * x % P
        n >>= 1
    return ret

def inv(x, P):
    return fpow(x, P - 2, P)

invP_P = 998244353

def invP(x):
    return inv(x, invP_P)

def fac(n):
    nRet = 1
    for i in range(1, n + 1):
        nRet *= i
    return nRet

def C(n, m):
    return fac(n) // fac(m) // fac(n - m)

def P(n, m):
    return fac(n) // fac(n - m)

class mod_calc:
    def __init__(self, n = 1000000, P = invP_P):
        self.n = n
        self.modP = P
        self.fac = [1]
        for i in range(1, n + 1):
            self.fac.append(self.fac[-1] * i % P)
        self.ifac = [0 for i in range(n + 1)]
        self.ifac[n] = inv(self.fac[-1], P)
        for i in range(n, 0, -1):
            self.ifac[i - 1] = self.ifac[i] * i % P
    
    def C(self, n, m):
        if n < m or n < 0:
            return 0
        print(self.fac[n], self.ifac[m], self.ifac[n - m])
        return self.fac[n] * self.ifac[m] * self.ifac[n - m] % self.modP
    
    def P(self, n, m):
        if n < m or n < 0:
            return 0
        return self.fac[n] * self.ifac[n - m] % self.modP

def guess_real_fraction(value, P, lim = 1000000):
    if value == 1:
        return (1, 1)
    for fa in range(1, min(lim, P)):
        son = value * fa % P
        if son < fa:
            return (son, fa)
    return (-1, -1)

def guess_fraction(value, P, lim = 1000000):
    for fa in range(1, min(lim, P)):
        son = value * fa % P
        if son <= max(fa ** 1.5, fa + 10):
            return (son, fa)
    return (-1, -1)

for n in range(1, 100):
    for i in range((n + 1) // 2):
        sum = 0
        for x in range(i + 1):
            sum += C(n-(i+x)-1, i-x)
        assert sum == C(n-i,i), f'{n},{i},{sum},{C(n-i,i)}'