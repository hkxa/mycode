/*************************************
 * problem:      P2949 [USACO09OPEN]工作调度Work Scheduling.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-04-25.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 
/*************************************
 * record ID:    R18531668.
 * time:         414 ms
 * memory:       2568 KB
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

struct game {
    int end, worth;
    
    void input()
    {
        end = read<int>();
        worth = read<int>();
    }

    bool operator < (const game &t) const
    {
        return worth < t.worth;
    }
} a[100010];

bool cmp_endTime(game &a, game &b) 
{   
    return a.end < b.end;
}

priority_queue<game> q;
int n;
long long ans = 0;

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        a[i].input();
    }
    sort(a + 1, a + n + 1, cmp_endTime);
    for (int tim = a[n].end, pos = n; tim >= 1; tim--)
    {
        while (pos && a[pos].end >= tim) {
            q.push(a[pos--]);
        }
        if (!q.empty()) {
            ans += q.top().worth;
            q.pop();
        }
        if (q.empty()) {
            tim = a[pos].end + 1;
        }
    }
    write(ans);
    return 0;
}