/*************************************
 * @problem:      Check Transcription.
 * @author:       brealid.
 * @time:         2021-02-03.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N_T = 1e5 + 7, N_S = 1e6 + 7, P = 1e9 + 7;

int nT, nS, cnt0, cnt1;
char t[N_T], s[N_S];
int64 HashValue2017[N_S], power2017[N_S];

inline int64 HashS(int l, int r) {
    return (HashValue2017[r] - HashValue2017[l - 1] * power2017[r - l + 1] % P + P) % P;
}

signed main() {
    kin >> (t + 1) >> (s + 1);
    nT = strlen(t + 1);
    nS = strlen(s + 1);
    HashValue2017[0] = 0;
    power2017[0] = 1;
    for (int i = 1; i <= nT; ++i)
        if (t[i] == '0') ++cnt0;
        else ++cnt1;
    for (int i = 1; i <= nS; ++i) {
        power2017[i] = (power2017[i - 1] * 2017) % P;
        HashValue2017[i] = (HashValue2017[i - 1] * 2017 + (s[i] & 31)) % P;
    }
    int ans = 0;
    for (int len0 = 1; len0 * cnt0 + cnt1 <= nS; ++len0) {
        if ((nS - len0 * cnt0) % cnt1 != 0) continue;
        int len1 = (nS - len0 * cnt0) / cnt1;
        int64 r0 = 0, r1 = 0;
        bool success = true;
        for (int p = 1, where = 1; p <= nT; ++p) {
            if (t[p] == '0') {
                if (!r0) r0 = HashS(where, where + len0 - 1);
                else if (r0 != HashS(where, where + len0 - 1)) {
                    success = false;
                    break;
                }
                where += len0;
            } else {
                if (!r1) r1 = HashS(where, where + len1 - 1);
                else if (r1 != HashS(where, where + len1 - 1)) {
                    success = false;
                    break;
                }
                where += len1;
            }
        }
        if (success && r0 != r1) ++ans;
        // printf("[%I64d, %I64d]\n", r0, r1);
    }
    kout << ans << '\n';
    return 0;
}