/*************************************
 * @problem:      最长k可重区间集问题.
 * @author:       brealid.
 * @time:         2021-01-30.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

namespace Network_MinCostMaxFlow {
    typedef long long int64;
    const int Net_Node = 1003, Net_Edge = 5000;
    const int64 inf = 0x3f3f3f3f3f3f3f3f; // 当 int64 为 int 的时候，自动类型强转为 0x3f3f3f3f
    struct edge {
        int to, nxt_edge;
        int64 flow, fee;
    } e[Net_Edge * 2 + 5];
    int head[Net_Node + 5], ecnt = 1;
    int node_total, st, ed;
    int64 flow[Net_Node + 5], dis[Net_Node + 5];
    int from[Net_Node + 5];
    bool inq[Net_Node + 5];
    // 清零，此函数适用于多组数据
    inline void clear() {
        memset(head, 0, sizeof(head));
        ecnt = 1;
        st = ed = 0;
    }
    // 添加边（正向边和反向边均会自动添加）
    inline void add_edge(const int &from, const int &to, const int64 &flow, const int64 &fee) {
        // Add "positive going edge"
        e[++ecnt].to = to;
        e[ecnt].flow = flow;
        e[ecnt].fee = fee;
        e[ecnt].nxt_edge = head[from];
        head[from] = ecnt;
        // Add "reversed going edge"
        e[++ecnt].to = from;
        e[ecnt].flow = 0;
        e[ecnt].fee = -fee; // 反边费用相反
        e[ecnt].nxt_edge = head[to];
        head[to] = ecnt;
    }
    // SPFA 算法
    inline bool spfa() {
        memset(dis, 0x3f, sizeof(int64) * (node_total + 1));
        memset(flow, 0, sizeof(int64) * (node_total + 1));
        memset(inq, 0, sizeof(bool) * (node_total + 1));
        from[st] = -1;
        dis[st] = 0;
        flow[st] = inf;
        std::queue<int> q;
        q.push(st);
        inq[st] = true;
        while (!q.empty()) {
            int u = q.front(); q.pop();
            for (int i = head[u]; i; i = e[i].nxt_edge)
                if (dis[e[i].to] > dis[u] + e[i].fee && e[i].flow) {
                    dis[e[i].to] = dis[u] + e[i].fee;
                    flow[e[i].to] = std::min(flow[u], e[i].flow);
                    from[e[i].to] = i;
                    if (!inq[e[i].to]) {
                        q.push(e[i].to);
                        inq[e[i].to] = true;
                    }
                }
            inq[u] = false;
        }
        return dis[ed] != inf;
    }
    // 记录 MCMF(Min Cost Max Flow) 的结果
    struct MCMF_result {
        int64 max_flow, min_cost;
        MCMF_result(int64 MF, int64 MC) : max_flow(MF), min_cost(MC) {}
    };
    // MCMF(Edmond_Karp + SPFA) 总工作函数，需要提供节点数，起始点（默认 1），结束点（默认 node_count)
    MCMF_result work(int node_count, int start_node = 1, int finish_node = -1) {
        fprintf(stderr, "NODE %d, EDGE %d\n", node_count, ecnt);
        node_total = node_count;
        st = start_node;
        ed = ~finish_node ? finish_node : node_count;
        int64 max_flow = 0, min_cost = 0;
        while (spfa()) {
            int64 now_flow = flow[ed];
            fprintf(stderr, "[STATUS %lld %lld] Archeve %lld\n", max_flow, min_cost, now_flow);
            max_flow += now_flow;
            for (int i = from[ed]; ~i; i = from[e[i ^ 1].to]) {
                e[i].flow -= now_flow;
                e[i ^ 1].flow += now_flow;
                min_cost += e[i].fee * now_flow;
            }
        }
        return MCMF_result(max_flow, min_cost);
    }
}

const int N = 500 + 5;

int n, k, l[N], r[N];
int val[N << 1], m;

signed main() {
    kin >> n >> k;
    for (int i = 1; i <= n; ++i) {
        kin >> l[i] >> r[i];
        if (l[i] > r[i]) swap(l[i], r[i]);
        val[++m] = l[i], val[++m] = r[i];
    }
    sort(val + 1, val + m + 1);
    m = unique(val + 1, val + m + 1) - val - 1;
    for (int i = 2; i <= m; ++i) Network_MinCostMaxFlow::add_edge(i - 1, i, 100, 0);
    int forward_S = m + 1, S = m + 2, T = m + 3;
    for (int i = 1; i <= n; ++i) {
        int len = r[i] - l[i];
        l[i] = lower_bound(val + 1, val + m + 1, l[i]) - val;
        r[i] = lower_bound(val + 1, val + m + 1, r[i]) - val;
        // Network_MinCostMaxFlow::add_edge(S, l[i], 1, 0);
        Network_MinCostMaxFlow::add_edge(l[i], r[i], 1, -len);
        // Network_MinCostMaxFlow::add_edge(r[i], T, 1, 0);
    }
    Network_MinCostMaxFlow::add_edge(forward_S, S, k, 0);
    Network_MinCostMaxFlow::add_edge(S, 1, 100, 0);
    Network_MinCostMaxFlow::add_edge(m, T, 100, 0);
    fprintf(stderr, "Note that add-edge-complete\n");
    kout << -Network_MinCostMaxFlow::work(m + 3, forward_S, T).min_cost << '\n';
    return 0;
}