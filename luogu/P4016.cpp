/*************************************
 * problem:      P4016 负载平衡问题.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-25.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define puts_return(x) { puts(x); return 0; }
#define write_return(x) { write(x); return 0; }

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;
int64 a[5000007];
int64 average, tmp[5000007] = {0}, ansSum = 0;
// int64 ans[5000007] = {0}

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) average += (a[i] = read<int>());
    average /= n;
    for (int i = 1; i <= n; i++) tmp[i] = (a[i] += a[i - 1] - average); 
    nth_element(tmp + 1, tmp + ((n + 1) >> 1), tmp + n + 1);
    int64 mid = tmp[(n + 1) >> 1];
    for (int i = 1; i <= n; i++) ansSum += abs(ans[i] = a[i] - mid);
    write(ansSum, 10);
    // write(-ans[n], 32);
    // write(ans[1], 10);
    // for (int i = 2; i <= n; i++) {
    //     write(-ans[i - 1], 32);
    //     write(ans[i], 10);
    // }
    return 0;
}