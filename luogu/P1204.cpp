/*************************************
 * problem:      P1204 [USACO1.2]挤牛奶Milking Cows.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-06-02.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

int n;
int diff[1000007] = {0};
int beg, end;
bool situation;
int cnt = 0, mx[2] = {0}, now;
int L = 1000008, R = 0;

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        beg = read<int>();
        end = read<int>();
        diff[beg]++;
        diff[end]--;
        L = min(L, beg);
        R = max(R, end);
    }
    if (diff[L]) situation = 1;
    else situation = 0;
    for (int i = L; i <= R; i++) {
        now += diff[i];
        if ((now != 0 && situation) || !(now != 0 || situation)) {
            cnt++;
        } else {
            mx[situation] = max(mx[situation], cnt);
            situation = !situation;
            cnt = 1;
        }
        // printf("pos %d : now = %d, situation = %d, cnt = %d, mx[1] = %d, mx[0] = %d.\n", i, now, situation, cnt, mx[1], mx[0]);
    }
    write(mx[1], 32);
    write(mx[0], 10);
    return 0;
}