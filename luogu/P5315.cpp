/*************************************
 * problem:      P5315 头像上传.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-04-20.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 
/*************************************
 * record ID:    R18417799.
 * time:         31 ms
 * memory:       804 KB
*************************************/ 


#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

int n, L, G;
int w, h;

int main()
{
    n = read<int>();
    L = read<int>();
    G = read<int>();
    while (n--) {
        w = read<int>();
        h = read<int>();
        while (w > G || h > G) {
            w >>= 1;
            h >>= 1;
        }
        if (w < L || h < L) {
            puts("Too Young");
        } else if (w != h) {
            puts("Too Simple");
        } else {
            puts("Sometimes Naive");
        }
    }
    return 0;
}