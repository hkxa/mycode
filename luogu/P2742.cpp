//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      P2742 [USACO5.1]圈奶牛Fencing the Cows /【模板】二维凸包.
 * @user_id:      63720.
 * @user_name:    brealid.
 * @time:         2020-05-20.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;
struct pos {
    double x, y;
    bool operator < (const pos &b) const {
        return x != b.x ? x < b.x : y < b.y;
    }
    pos operator - (const pos &b) const {
        return (pos){x - b.x, y - b.y};
    }
} a[100007];
int s[100007], top = -1; // stack

inline double dis(pos a, pos b) {
    return sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
}

inline double Cross(pos a, pos b) {
    return a.x * b.y - a.y * b.x;
}

inline bool Compare(int i, int j, int k) {
    return Cross(a[i] - a[j], a[i] - a[k]) >= 0;
}

void Andrew() {
    sort(a + 1, a + n + 1);
    s[++top] = 1;
    s[++top] = 2;
    for (int i = 3; i <= n; i++) {
        while (top && Compare(s[top - 1], s[top], i)) top--;
        s[++top] = i; 
    }
    // node n has been push to the stack
    s[++top] = n - 1;
    for (int i = n - 2; i >= 1; i--) {
        while (top && Compare(s[top - 1], s[top], i)) top--;
        s[++top] = i; 
    }
}

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        scanf("%lf%lf", &a[i].x, &a[i].y);
    }
    Andrew();
    double ans = 0;
    // for (int i = 0; i <= top; i++) write(s[i], i == top ? 10 : 32);
    for (int i = 0; i < top; i++) {
        ans += dis(a[s[i]], a[s[i + 1]]);
    }
    printf("%.2lf\n", ans);
    return 0;
}