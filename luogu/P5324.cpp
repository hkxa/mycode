/*************************************
 * @problem:      「BJOI2019」删数.
 * @author:       brealid.
 * @time:         2020-11-19.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#if true
#define getchar() (i_P1 == i_P2 && (i_P2 = (i_P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), i_P1 == i_P2) ? EOF : *i_P1++)
char BUF[1 << 21], *i_P1 = BUF, *i_P2 = BUF;
#endif

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 1.5e5 + 7, M = 4.5e5 + 7;

int n, m, a[N], occured[M];
int mx, L, R, ok;

struct node {
    int minv, cnt, tag;
} tr[M << 2];

inline void pushdown(int u) {
    if (tr[u].tag) {
        int ls = u << 1, rs = ls | 1, &now = tr[u].tag;
        tr[ls].minv += now;
        tr[rs].minv += now;
        tr[ls].tag += now;
        tr[rs].tag += now;
        now = 0;
    }
}

inline void pushup(int u) {
    int ls = u << 1, rs = ls | 1;
    if (tr[ls].minv == tr[rs].minv) {
        tr[u].minv = tr[ls].minv;
        tr[u].cnt = tr[ls].cnt + tr[rs].cnt;
    } else if (tr[ls].minv < tr[rs].minv) {
        tr[u].minv = tr[ls].minv;
        tr[u].cnt = tr[ls].cnt;
    } else {
        tr[u].minv = tr[rs].minv;
        tr[u].cnt = tr[rs].cnt;
    }
}

void build(int u, int l, int r) {
    tr[u].cnt = r - l + 1;
    if (l == r) return;
    int mid = (l + r) >> 1, ls = u << 1, rs = ls | 1;
    build(ls, l, mid), build(rs, mid + 1, r);
}

void segt_modify(int u, int l, int r, int ml, int mr, int dif) {
    if (l >= ml && r <= mr) {
        tr[u].minv += dif;
        tr[u].tag += dif;
        return;
    }
    pushdown(u);
    int mid = (l + r) >> 1, ls = u << 1, rs = ls | 1;
    if (mid >= ml) segt_modify(ls, l, mid, ml, mr, dif);
    if (mid < mr) segt_modify(rs, mid + 1, r, ml, mr, dif);
    pushup(u);
}

inline void insert(int x) {
    int p = x - occured[x]++;
    if (x >= L && x <= R && p >= 1) segt_modify(1, 1, mx, p, p, 1);
}

inline void erase(int x) {
    int p = x - --occured[x];
    if (x >= L && x <= R && p >= 1) segt_modify(1, 1, mx, p, p, -1);
}

int segt_query(int u, int l, int r, int ml, int mr) {
    if (l >= ml && r <= mr) return tr[u].minv == 0 ? tr[u].cnt : 0;
    pushdown(u);
    int mid = (l + r) >> 1, ls = u << 1, rs = ls | 1, res = 0;
    if (mid >= ml) res += segt_query(ls, l, mid, ml, mr);
    if (mid < mr) res += segt_query(rs, mid + 1, r, ml, mr);
    return res;
}

signed main() {
    kin >> n >> m;
    mx = m + n + m;
    L = m + 1;
    R = m + n;
    build(1, 1, mx);
    for (int i = 1; i <= n; ++i) {
        kin >> a[i];
        insert(a[i] += L - 1);
    }
    for (int i = 1, p, x; i <= m; ++i) {
        kin >> p >> x;
        if (p) {
            erase(a[p]);
            insert(a[p] = x + L - 1);
        } else if (x == -1) {
            if (occured[L]) segt_modify(1, 1, mx, L - occured[L] + 1, L, -1);
            ++L;
            if (occured[++R]) segt_modify(1, 1, mx, R - occured[R] + 1, R, +1);
        } else {
            if (occured[R]) segt_modify(1, 1, mx, R - occured[R] + 1, R, -1);
            --R;
            if (occured[--L]) segt_modify(1, 1, mx, L - occured[L] + 1, L, +1);
        }
        kout << segt_query(1, 1, mx, L, R) << '\n';
    }
    return 0;
}