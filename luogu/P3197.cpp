/*************************************
 * problem:      P3197.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-05.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define P 100003

long long fexp(long long a, long long n)
{
    long long res(1);
    while (n) {
        if (n & 1) res = (res * a) % P;
        a = (a * a) % P;
        n >>= 1;
    }
    return res;
}

#define tot fexp(m, n)
#define not_breakout (m * fexp(m - 1, n - 1)) % P

int main()
{
    long long m = read<long long>(), n = read<long long>();
    write((tot - not_breakout + P) % P);
    return 0;
}