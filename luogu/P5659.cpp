#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read() {
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    Int n = c & 15;
    while (isdigit(c = getchar())) n = (((n << 2) + n) << 1) + (c & 15);
    return n;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

template <typename T>
inline T ChkMin(T &a, const T b) { return a > b ? a = b : a; }

// value Definition
const bitset<2048> empty;
int n, pos[2007];;
int head[4007], to[4007], nxt[4007], edges_cnt = 0;
int Degree[2007];

struct node {
    // id : 节点编号
    int id;
    // fst : 最先删去的边的编号
    int fst;
    // lst : 最后删去的边的编号
    int lst;
    // fa : 这个节点用来防止提前自闭用的并查集数组
    int fa[4093];
    // beg[i] : 以边 i 为起点的边是否可用
    bitset<2048> beg;
    // end[i] : 以边 i 为终点的边是否可用
    bitset<2048> end; 
    void init()
    {
        beg.set(); end.set(); 
        fst = lst = 0;
        for (int i = 0; i < n; i++) fa[i] = i;
    }
    int find(int x) { return fa[x] == x ? x : (fa[x] = find(fa[x])); }
    inline bool family(int u, int v) { return find(u) == find(v); }
    inline void merge(int u, int v) { fa[find(u)] = find(v); }
} t[2048];

void add(int u, int v)
{
    // add edge u->v
    to[edges_cnt] = v;
    nxt[edges_cnt] = head[u];
    head[u] = edges_cnt++;
    // add edge v->u
    to[edges_cnt] = u;
    nxt[edges_cnt] = head[v];
    head[v] = edges_cnt++;
    // maintain Degree
    Degree[u]++;
    Degree[v]++;
}

void init()
{
    memset(head, -1, sizeof(head));
    memset(Degree, 0, sizeof(Degree));
    edges_cnt = 2;
    for (register int i = 1; i <= n; i++) t[i].init();
}

int GetAns(int u, int elas)
{
    int res = n + 1;
    if (elas /* 这不是起飞点 */ && (!t[u].lst || t[u].lst == elas) /* 这个节点 u 可以作为终点 */ 
        && t[u].end[elas] /* 这条边不会使这个点提前自闭(以这条边作为结束边) */  
        && !(t[u].fst && Degree[u] > 1 && t[u].family(elas, t[u].fst)) /* 这条边不会使这个点提前自闭 */) res = u;
    for (register int i = head[u], enxt = (i >> 1); ~i; i = nxt[i], enxt = (i >> 1)) {
        if (elas == enxt) continue;
        if (!elas) {
            // 这是起飞点
            if (t[u].fst && t[u].fst != enxt) continue; // 如果这个点不能作为满足要求的起飞点, continue
            if (!t[u].beg[enxt]) continue; // 如果这个点已经用过这条边作为中转边, continue 
            if (t[u].lst && Degree[u] > 1 && t[u].family(enxt, t[u].lst)) continue; // 防止提前自闭
            ChkMin(res, GetAns(to[i], enxt));
        } else {
            // 这是中转点
            if (elas == t[u].lst) continue;  // 上条边已被用作结束边
            if (enxt == t[u].fst) continue;  // 这条边已被用作起始边
            if (t[u].family(elas, enxt)) continue; // 防止提前自闭 
            if (!t[u].end[elas] || !t[u].beg[enxt]) continue;  // 如果这个点已经用过这两边作为中转边, continue 
            if (t[u].fst && t[u].lst && Degree[u] > 2 && t[u].family(elas, t[u].fst) && t[u].family(enxt, t[u].lst)) continue; // 防止提前自闭
            ChkMin(res, GetAns(to[i], enxt));
        }
    }
    return res;
}

bool UpdateData(int u, int elas, int p)
{
    if (u == p) return t[u].lst = elas, 1;
    for (register int i = head[u], enxt = (i >> 1); ~i; i = nxt[i], enxt = (i >> 1)) {
        if (elas != enxt) {
            if (UpdateData(to[i], enxt, p)) {
                if (!elas) t[u].fst = enxt; // 这是起飞点
                else {
                    t[u].end[elas] = t[u].beg[enxt] = 0; 
                    Degree[u]--;
                    t[u].merge(elas, enxt);
                    // 打上标记：用过这条边
                }
                return true;
            }
        }
    }
    return false;
}

int main()
{
    int T = read<int>();
    while (T--) {
        n = read<int>();
        init();
        for (register int i = 1; i <= n; i++) pos[i] = read<int>();
        for (register int u, v, i = 1; i < n; i++) {
            u = read<int>();
            v = read<int>();
            add(u, v);
        }
        for (register int i = 1, p; i <= n; i++) {
            p = GetAns(pos[i], 0);
            UpdateData(pos[i], 0, p);
            write(p, 32);
        }
        putchar(10);
    }
    return 0;
}