T = int(input())
for _ in range(T):
    n, k, q = map(int, input().split())
    a = list(map(int, input().split())) + [q + 1]
    current = 1 - k
    ans = 0
    for day in a:
        if day > q:
            if current >= 0:
                ans += current * (current + 1) // 2
            current = 1 - k
        else:
            current += 1
    print(ans)