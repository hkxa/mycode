/*************************************
 * @problem:      id_name.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2019-mm-dd.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int64 n, k;
struct Matrix {
    int64 a[103][103];
    Matrix() { memset(a, 0, sizeof(a)); }
    void init() { for (int64 i = 1; i <= n; i++) a[i][i] = 1; }
    Matrix operator * (const Matrix &b) {
        Matrix res;
        for (int64 i = 1; i <= n; i++) {
            for (int64 j = 1; j <= n; j++) {
                for (int64 k = 1; k <= n; k++) {
                    res.a[i][j] = (res.a[i][j] + a[i][k] * b.a[k][j]) % 1000000007;
                }
            }
        }
        return res;
    }
    Matrix operator ^ (int64 n) {
        Matrix res, now = *this;
        res.init();
        while (n) {
            if (n & 1) res = res * now; 
            now = now * now;
            n >>= 1;
        }
        return res;
    }
} a;

int main()
{
    n = read<int64>();
    k = read<int64>();
    for (int64 i = 1; i <= n; i++) {
        for (int64 j = 1; j <= n; j++) {
            a.a[i][j] = read<int64>();
        }
    }
    a = a ^ k;
    for (int64 i = 1; i <= n; i++) {
        for (int64 j = 1; j <= n; j++) {
            write(a.a[i][j], j == n ? 10 : 32);
        }
    }
    return 0;
}