/*************************************
 * @problem:      [USACO08NOV]Toys G.
 * @author:       brealid.
 * @time:         2021-02-02.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

int N, Price_Clean, DayCost_Fast, Price_Fast, DayCost_Slow, Price_Slow;
vector<int> r;

int calc(int Clean_Count) {
    // printf("calc(%d)\n", Clean_Count);
    int Money_Used = Clean_Count * Price_Clean;
    deque<pair<int, int> > Dirty, Fast_Washed, Slow_Washed;
    for (int i = 0; i < N; ++i) {
        // printf("When day %d, 'Clean' remains %d\n", i + 1, Clean_Count);
        while (!Dirty.empty() && Dirty.front().first <= i - DayCost_Fast) {
            Fast_Washed.push_back(Dirty.front());
            Dirty.pop_front();
        }
        while (!Fast_Washed.empty() && Fast_Washed.front().first <= i - DayCost_Slow) {
            Slow_Washed.push_back(Fast_Washed.front());
            Fast_Washed.pop_front();
        }
        int Use_Count = min(r[i], Clean_Count);
        Clean_Count -= Use_Count;
        int Require = r[i] - Use_Count;
        while (Require && !Slow_Washed.empty()) {
            Use_Count = min(Require, Slow_Washed.back().second);
            Money_Used += Use_Count * Price_Slow;
            Require -= Use_Count;
            if (Use_Count == Slow_Washed.back().second) Slow_Washed.pop_back();
            else Slow_Washed.back().second -= Use_Count;
        }
        while (Require && !Fast_Washed.empty()) {
            Use_Count = min(Require, Fast_Washed.back().second);
            Money_Used += Use_Count * Price_Fast;
            Require -= Use_Count;
            if (Use_Count == Fast_Washed.back().second) Fast_Washed.pop_back();
            else Fast_Washed.back().second -= Use_Count;
        }
        if (Require) return 2e9;
        Dirty.push_back(make_pair(i, r[i]));
    }
    return Money_Used;
}

signed main() {
    kin >> N >> DayCost_Fast >> DayCost_Slow >> Price_Fast >> Price_Slow >> Price_Clean;
    // ++DayCost_Fast, ++DayCost_Slow;
    if (DayCost_Fast > DayCost_Slow) {
        swap(DayCost_Fast, DayCost_Slow);
        swap(Price_Fast, Price_Slow);
    }
    if (Price_Fast < Price_Slow)
        DayCost_Slow = DayCost_Fast, Price_Slow = Price_Fast;
    r.resize(N);
    int sum(0);
    for (int i = 0; i < N; ++i) {
        kin >> r[i];
        sum += r[i];
    }
    int l = 1, r = sum, m1, m2;
    while (r - l > 10) {
        m1 = (l * 2 + r) / 3;
        m2 = (l + r * 2) / 3;
        if (calc(m1) < calc(m2)) r = m2;
        else l = m1;
    }
    int ans = 2e9;
    for (int i = l; i <= r; ++i) ans = min(ans, calc(i));
    kout << ans << '\n';
    return 0;
}