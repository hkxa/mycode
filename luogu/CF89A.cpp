/*************************************
 * problem:      CF89A Robbery.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-04-06.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 
/*************************************
 * record ID:    R17968464.
 * time:         677 ms
 * memory:       100 KB
*************************************/ 


#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

long long n, m, k;
long long a[10007] = {0};
long long minSteal = 100000 + 1, cntNum = 0;

int main()
{
    n = read<long long>();
    m = read<long long>();
    k = read<long long>();
    for (int i = 1; i <= n; i++) {
        a[i] = read<long long>();
        if (a[i] < minSteal && (i & 1)) {
            minSteal = a[i];
        }
    }
    cntNum = (n >> 1) + 1;
    if ((n & 1) && cntNum <= m) {
        write(min(m / cntNum * k, minSteal));
    } else {
        write(0);
    }
    return 0;
}