/*************************************
 * @problem:      【模板】树同构（[BJOI2015]树的同构）.
 * @user_name:    brealid.
 * @time:         2020-11-14.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

bool operator == (const vector<uint64> &a, const vector<uint64> &b) {
    if (a.size() ^ b.size()) return false;
    for (size_t i = 0; i < a.size(); ++i)
        if (a[i] ^ b[i]) return false;
    return true;
}

const int N = 50 + 3;
const uint64 K = 20170933, P = 446186599297ull;

int m, n;
vector<int> G[N];
vector<uint64> hash_value[N];

uint64 TreeHash(int u, int fa) {
    vector<int> hash_son;
    for (size_t i = 0; i < G[u].size(); ++i) {
        int &v = G[u][i];
        if (v != fa) hash_son.push_back(TreeHash(v, u));
    }
    sort(hash_son.begin(), hash_son.end());
    uint64 ret = 1;
    for (size_t i = 0; i < hash_son.size(); ++i)
        ret = (ret * K + hash_son[i]) % P;
    return ret;
}

signed main() {
    read >> m;
    for (int i = 1; i <= m; ++i) {
        read >> n;
        for (int j = 1; j <= n; ++j) G[j].clear();
        for (int j = 1, fa; j <= n; ++j) {
            read >> fa;
            if (fa) {
                G[j].push_back(fa);
                G[fa].push_back(j);
            }
        }
        hash_value[i].clear();
        for (int j = 1; j <= n; ++j) hash_value[i].push_back(TreeHash(j, 0));
        sort(hash_value[i].begin(), hash_value[i].end());
        for (int j = 1; j <= i; ++j)
            if (hash_value[j] == hash_value[i]) {
                write << j << '\n';
                break;
            }
    }
    return 0;
}