//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      P2571 [SCOI2010]传送带.
 * @user_id:      63720.
 * @user_name:    brealid.
 * @time:         2020-06-01.
 * @language:     C++.
 * @upload_place: luogu.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// #define int int64
int p, q, R;

struct point {
    double x, y;
    void readln() {
        x = read<int>();
        y = read<int>();
    }
    point operator + (const point ot) {
        return (point){x + ot.x, y + ot.y};
    }
    point operator / (const double k) {
        return (point){x / k, y / k};
    }
    bool operator == (const point ot) {
        return abs(x - ot.x) < 1e-6 && abs(y - ot.y) < 1e-6;
    }
} A, B, C, D;

bool cmp(point a, point b) {
    return abs(a.x - b.x) < 1e-4 && abs(a.y - b.y) < 1e-4;
}

double dist(point p, point q, int speed) {
    return sqrt((p.x - q.x) * (p.x - q.x) + (p.y - q.y) * (p.y - q.y)) / speed;
}

double sol(point a, point mid, point b, int am, int mb) {
    // printf("sol({%.2lf, %.2lf}, {%.2lf, %.2lf}, {%.2lf, %.2lf}, %d, %d) = %.2lf\n", 
    //         a.x, a.y, mid.x, mid.y, b.x, b.y, am, mb, dist(a, mid, am) + dist(mid, b, mb));
    return dist(a, mid, am) + dist(mid, b, mb);
}

double solve(point a, point b, point t, int ab) {
    point m1, m2, l = a, r = b;
    double ans = 1e9;
    while (!cmp(l, r)) {
        m1 = (l + l + r) / 3;
        m2 = (l + r + r) / 3;
        if (sol(a, m1, t, ab, R) < sol(a, m2, t, ab, R)) r = m2, ans = min(ans, sol(a, m1, t, ab, R));
        else l = m1, ans = min(ans, sol(a, m2, t, ab, R));
    }
    return ans;
}

double work(point m) {
    return dist(A, m, p) + solve(D, C, m, q);
}

#define min4(a, b, c, d) min(min(a, b), min(c, d))

signed main() {
    A.readln(); B.readln();
    C.readln(); D.readln();
    p = read<int>();
    q = read<int>();
    R = read<int>();
    if (A == B && C == D)  {
        printf("%.2lf\n", dist(A, D, R));
        return 0;
    }
    if (A == B) {
        printf("%.2lf\n", solve(D, C, A, q));
        return 0;
    }
    if (C == D) {
        printf("%.2lf\n", solve(A, B, D, p));
        return 0;
    }
    point m1, m2, l = A, r = B;
    double ans = 1e9, r1, r2;
    while (!cmp(l, r)) {
        m1 = (l + l + r) / 3;
        m2 = (l + r + r) / 3;
        r1 = work(m1); r2 = work(m2);
        if (r1 < r2) r = m2, ans = min(ans, r1);
        else l = m1, ans = min(ans, r2);
    }
    printf("%.2lf\n", ans);
    return 0;
}

// Create File Date : 2020-06-01