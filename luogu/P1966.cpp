/*************************************
 * problem:      P1966 火柴排队.
 * user ID:      85848.
 * user name:    hkxadpall.
 * time:         2019-10-02.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define ForInVec(vectorType, vectorName, iteratorName) for (vector<vectorType>::iterator iteratorName = vectorName.begin(); iteratorName != vectorName.end(); iteratorName++)
#define ForInVI(vectorName, iteratorName) ForInVec(int, vectorName, iteratorName)
#define ForInVE(vectorName, iteratorName) ForInVec(Edge, vectorName, iteratorName)
#define MemWithNum(array, num) memset(array, num, sizeof(array))
#define Clear(array) MemWithNum(array, 0)
#define MemBint(array) MemWithNum(array, 0x3f)
#define MemInf(array) MemWithNum(array, 0x7f)
#define MemEof(array) MemWithNum(array, -1)
#define ensuref(condition) do { if (!(condition)) exit(0); } while(0)

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

struct FastIOer {
#   define createReadlnInt(type)            \
    FastIOer& operator >> (type &x)         \
    {                                       \
        x = read<type>();                   \
        return *this;                       \
    }
    createReadlnInt(short);
    createReadlnInt(unsigned short);
    createReadlnInt(int);
    createReadlnInt(unsigned);
    createReadlnInt(long long);
    createReadlnInt(unsigned long long);
#   undef createReadlnInt

#   define createReadlnReal(type)           \
    FastIOer& operator >> (type &x)         \
    {                                       \
        char c;                             \
        x = read<long long>(c);             \
        if (c != '.') return *this;         \
        type r = (x >= 0 ? 0.1 : -0.1);     \
        while (isdigit(c = getchar())) {    \
            x += r * (c & 15);              \
            r *= 0.1;                       \
        }                                   \
        return *this;                       \
    }
    createReadlnReal(double);
    createReadlnReal(float);
    createReadlnReal(long double);

#   define createWritelnInt(type)           \
    FastIOer& operator << (type x)          \
    {                                       \
        write<type>(x);                     \
        return *this;                       \
    }
    createWritelnInt(short);
    createWritelnInt(unsigned short);
    createWritelnInt(int);
    createWritelnInt(unsigned);
    createWritelnInt(long long);
    createWritelnInt(unsigned long long);
#   undef createWritelnInt
    
    FastIOer& operator >> (char &x)
    {
        x = getchar();
        return *this;
    }
    
    FastIOer& operator << (char x)
    {
        putchar(x);
        return *this;
    }
    
    FastIOer& operator << (const char *x)
    {
        int __pos = 0;
        while (x[__pos]) {
            putchar(x[__pos++]);
        }
        return *this;
    }
} fast;

int n;
// bool compType;
struct ele {
    int a, b;
    bool operator < (const ele &other) const 
    { 
        // return compType ? b < other.b : a < other.a; 
        return a < other.a; 
        // return a == other.a ? b < other.b : a < other.a; 
    }
} e[100007];
int tmp[100007]; 

long long ans = 0;
void mergeSort(int l, int r)
{
#   ifdef DEBUG
    if (l == r) {
        printf("mergeSort(%d, %d) : %d\n", l, r, e[l].b);
        puts("contribution : 0");
    }
#   endif
    if (l >= r) return;
    int mid = (l + r) >> 1, p1 = l, p2 = mid + 1, p = l;
    mergeSort(p1, mid);
    mergeSort(p2, r);
#   ifdef DEBUG
    long long before = ans;
#   endif
    while (p1 <= mid && p2 <= r) {
        if (e[p1].b <= e[p2].b) tmp[p++] = e[p2++].b;
        else {
            ans = (ans + r - p2 + 1) % 99999997;
            tmp[p++] = e[p1++].b;
        }
    }
    while (p1 <= mid) {
        tmp[p++] = e[p1++].b;
    }
    while (p2 <= r) {
        tmp[p++] = e[p2++].b;
    }
    for (p = l; p <= r; p++) e[p].b = tmp[p];
#   ifdef DEBUG
    printf("mergeSort(%d, %d) : ", l, r);
    for (p = l; p <= r; p++) {
        write(e[p].b, p == r ? 10 : 32);
    }
    printf("contribution : %lld\n", ans - before);
#   endif
}

int main()
{
    fast >> n;
    for (int i = 1; i <= n; i++) {
        fast >> e[i].a;
    }
    for (int i = 1; i <= n; i++) {
        fast >> e[i].b;
    }
    // compType = 1;
    // sort(e + 1, e + n + 1);
    // compType = 0;
    sort(e + 1, e + n + 1);
#   ifdef DEBUG
    puts("sorted : ");
    for (int i = 1; i <= n; i++) {
        write(e[i].a, i == n ? 10 : 32);
    }
    for (int i = 1; i <= n; i++) {
        write(e[i].b, i == n ? 10 : 32);
    }
#   endif
    mergeSort(1, n);
    write(ans);
    return 0;
}