/*************************************
 * problem:      P4779 【模板】单源最短路径（标准版）.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-08.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

struct Graph {
    struct Node {
        int u, val;
        bool operator < (const Node &other) const
        {
            return val > other.val;
        }
    };
    struct Edge {
        int v, w;
    } e[200007];
    int head[100007], next[200007], edgeCnt;

    Graph() : edgeCnt(0)
    {
        memset(head, -1, sizeof(head));
    }

    void addEdge(int u, int v, int w)
    {
        e[++edgeCnt] = (Edge){v, w};
        next[edgeCnt] = head[u];
        head[u] = edgeCnt;
    }

    void dij(int n, int s, int *dis)
    {
        memset(dis, 0x3f, sizeof(int) * (n + 1));
        dis[s] = 0;
        priority_queue<Node> q;
        q.push((Node){s, 0});
        while (!q.empty()) {
            while (q.top().val != dis[q.top().u] && !q.empty()) q.pop();
            if (q.empty()) break;
            int fr = q.top().u;
            q.pop();
            #define to e[i].v
            #define va e[i].w
            for (int i = head[fr]; ~i; i = next[i]) {
                if (dis[to] - va > dis[fr]) {
                    dis[to] = dis[fr] + va;
                    q.push((Node){to, dis[to]});
                }
            }
            #undef to
            #undef va
        }
    }
} G;

int main()
{
    int n, m, s;
    int u, v, w;
    n = read<int>();
    m = read<int>();
    s = read<int>();
    while (m--) {
        u = read<int>();
        v = read<int>();
        w = read<int>();
        G.addEdge(u, v, w);
    }
    int *dis = new int[n + 1];
    G.dij(n, s, dis);
    for (int i = 1; i <= n; i++) {
        write(dis[i], 32);
    }
    return 0;
}