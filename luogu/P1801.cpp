//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      普通平衡树.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-15.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 200007;
const double alpha = 0.75;

struct ScapegoatNode {
    int v, tot, siz, l, r;
} t[N];
int root;
int temp[N];
int TempCnt, NodeCnt;

bool Balance(int x) {
    return t[t[x].l].siz < t[x].siz * alpha && 
           t[t[x].r].siz < t[x].siz * alpha;
}

void Flatten(int x) {
    if (!x) return;
    Flatten(t[x].l);
    if (t[x].tot) temp[++TempCnt] = x;
    Flatten(t[x].r);
}

void Build(int &x, int l, int r) {
    if (l > r) return void(x = 0);
    if (l == r) {
        x = temp[l];
        t[x].l = 0;
        t[x].r = 0;
        t[x].siz = t[x].tot;
        return;
    }
    int mid = (l + r) >> 1;
    x = temp[mid];
    Build(t[x].l, l, mid - 1);
    Build(t[x].r, mid + 1, r);
    t[x].siz = t[t[x].l].siz + t[t[x].r].siz + t[x].tot;
}

void ReBuild(int &x) {
    TempCnt = 0;
    Flatten(x);
    Build(x, 1, TempCnt);
}

void Check(int &a) {
    if (!a) return;
    if (!Balance(a)) ReBuild(a);
    else if (t[t[a].l].siz > t[t[a].r].siz) Check(t[a].l);
    else Check(t[a].r);
}

void Insert(int &a, int v) {
    if (!a) {
        a = ++NodeCnt;
        t[a].v = v;
        t[a].tot = 1;
    } else if (v == t[a].v) {
        t[a].tot++;
    } else if (v < t[a].v) {
        Insert(t[a].l, v);
    } else {
        Insert(t[a].r, v);
    }
    t[a].siz++;
}

void Delete(int a, int v) {
    assert(a);
    if (v == t[a].v) {
        t[a].tot--;
    } else if (v < t[a].v) {
        Delete(t[a].l, v);
    } else {
        Delete(t[a].r, v);
    }
    t[a].siz--;
}

int GetVal(int rank) {
    int a = root;
    while (true) {
        if (t[t[a].l].siz < rank && t[t[a].l].siz + t[a].tot >= rank) return t[a].v;
        else if (t[t[a].l].siz >= rank) a = t[a].l;
        else rank -= t[t[a].l].siz + t[a].tot, a = t[a].r;
    }
}

int GetRank(int val) {
    int a = root, rank = 1;
    while (a) {
        if (t[a].v >= val) a = t[a].l;     
        else rank += t[t[a].l].siz + t[a].tot, a = t[a].r;     
    }
    return rank;
}

int m, n, A[N], u[N];

int main()
{
    read >> m >> n;;
    for (int i = 1; i <= m; i++) read >> A[i];
    for (int i = 1; i <= n; i++) read >> u[i];
    for (int i = 1, j = 1, query_id = 0; i <= m; i++) {
        Insert(root, A[i]);
        Check(root);
        while (j <= n && u[j] == i) {
            j++;
            write << GetVal(++query_id) << '\n';
        }
    }
    return 0;
}