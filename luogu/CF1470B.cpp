/*************************************
 * @problem:      Strange Definition.
 * @author:       hkxadpall.
 * @time:         2021-02-02.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

int T, n, q;
vector<int> a;

vector<int> get_primes(int N) {
    vector<bool> IsPrime;
    vector<int> p;
    IsPrime.assign(N + 1, true);
    for (int i = 2; i <= N; ++i)
        if (IsPrime[i]) {
            p.push_back(i);
            for (int j = i << 1; j <= N; j += i)
                IsPrime[j] = false;
        }
    return p;
}

signed main() {
    kin >> T;
    vector<int> p = get_primes(1e3);
    vector<int> p2(p.size());
    for (int i = 0; i < (int)p.size(); ++i) p2[i] = p[i] * p[i];
    while (T --> 0) {
        kin >> n;
        a.resize(n);
        map<int, int> m;
        for (int i = 0; i < n; ++i) {
            kin >> a[i];
            for (int j = 0; j < (int)p.size() && p2[j] <= a[i]; ++j)
                while (a[i] % p2[j] == 0) a[i] /= p2[j];
            ++m[a[i]];
        }
        int d0 = 0;
        for (const auto &x : m) d0 = max(d0, x.second);
        int d1 = 0;
        for (const auto &x : m)
            if (x.first == 1 || x.second % 2 == 0) d1 += x.second;
        d1 = max(d0, d1);
        kin >> q;
        int64 x;
        for (int i = 1; i <= q; ++i) {
            kin >> x;
            if (x == 0) kout << d0 << '\n';
            else kout << d1 << '\n';
        }
    }
    return 0;
}