/*************************************
 * @problem:      CF372D Choosing Subtree is Fun.
 * @user_name:    brealid.
 * @time:         2020-10-30.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    enum i_flags { ignore_int = 1 };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

const int N = 1e5 + 7;
int n, k;
vector<int> G[N];

int fa[N], dep[N], siz[N], wson[N];
int beg[N], dfn[N], dft;
void dfs1(int u, int fat) {
    fa[u] = fat;
    dep[u] = dep[fat] + 1;
    siz[u] = 1;
    for (size_t i = 0; i < G[u].size(); ++i) {
        int v = G[u][i];
        if (v != fat) {
            dfs1(v, u);
            siz[u] += siz[v];
            if (siz[v] > siz[wson[u]]) wson[u] = v;
        }
    }
}
void dfs2(int u, int cbeg) {
    beg[u] = cbeg;
    dfn[u] = ++dft;
    if (!wson[u]) return;
    dfs2(wson[u], cbeg);
    for (size_t i = 0; i < G[u].size(); ++i) {
        int v = G[u][i];
        if (v != fa[u] && v != wson[u]) dfs2(v, v);
    }
}
int qLCA(int u, int v) {
    if (u < 0 || v < 0 || u > n || v > n) return 0;
    while (beg[u] != beg[v]) {
        if (dep[beg[u]] < dep[beg[v]]) v = fa[beg[v]];
        else u = fa[beg[u]];
    }
    if (dep[u] < dep[v]) return u;
    else return v;
}
int query(int x, int y, int z) {
    return dep[x] + dep[qLCA(y, z)] - dep[qLCA(x, y)] - dep[qLCA(x, z)];
}
struct comp_dfn {
    bool operator () (const int &a, const int &b) const {
        return dfn[a] < dfn[b];
    }
};

set<int, comp_dfn> s;
typedef set<int, comp_dfn>::iterator set_iter;
set_iter prev_iter(set_iter it) { return it == s.begin() ? --s.end() : --it; }
set_iter next_iter(set_iter it) { return it == --s.end() ? s.begin() : ++it; }

signed main() {
    read >> n >> k;
    for (int i = 1, u, v; i < n; ++i) {
        read >> u >> v;
        G[u].push_back(v);
        G[v].push_back(u);
    }
    dfs1(1, 0);
    dfs2(1, 1);
    int ans = 1, now = 0;
    dfn[n + 1] = n + 1;
    for (int i = 1, j = 0; i <= n; ++i) {
        while (now <= k) {
            ans = max(ans, j - i + 1);
            if (j >= n) break;
            set_iter it = s.insert(++j).first;
            if (s.size() == 1) ++now;
            now += query(j, *prev_iter(it), *next_iter(it));
        }
        set_iter it = s.find(i);
        now -= query(i, *prev_iter(it), *next_iter(it));
        if (s.size() == 1) --now;
        s.erase(it);
    }
    write << ans << '\n';
    return 0;
}