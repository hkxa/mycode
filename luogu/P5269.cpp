/*************************************
 * problem:      P5269 欧稳欧再次学车.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-03-23.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 
/*************************************
 * record ID:    R17548581.
 * time:         534 ms
 * memory:       904 KB
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

int T, N, L, R, X, K;
int x, y;
unsigned long long ans = 0;
int gear, rev;
int keep;

int error()
{
    puts("-1");
    return 0;
}

int main()
{
    T = read<int>();
    N = read<int>();
    L = read<int>();
    R = read<int>();
    X = read<int>();
    K = read<int>();
    gear = 1;
    rev = L;
    keep = 0;
    for (int i = 1; i <= T; i++) {
        x = read<int>();
        y = read<int>();
        switch (x) {
            case 0:
                gear++;
                if (gear > N) {
                    return error();
                }
                rev = L;
                break;
            case 1:
                gear--;
                if (gear < 1) {
                    return error();
                }
                rev = R;
                break;
            case 2:
                break;
            default:
                return error();
        }
        switch (y) {
            case 0:
                break;
            case 1:
                rev += X;
                if (rev > R) {
                    rev = R;
                }
                break;
            default:
                return error();
        }
        if (rev == R) keep++;
        else keep = 0;
        ans += (long long)gear * rev;
        if (keep >= K) {
            write(ans);
            return 0;
        }
    }
    write(ans);
    return 0;
}
