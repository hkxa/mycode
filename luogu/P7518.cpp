// Liu Xinnan AK IOI!
// xaero give me strength!!
// Towards your dream, no matter meet what!!!
#define SX2021_ProblemName "gem"
#include <bits/stdc++.h>
using namespace std;
#ifdef Test_TimMem
bool MemTester1;
#endif

inline void FastIn(int &val) {
//	cin >> val;return;
    char ch = getchar();
    while (ch < '0' || ch > '9') ch = getchar();
    val = ch & 15;
    while ((ch = getchar()) >= '0' && ch <= '9') val = (((val << 2) + val) << 1) + (ch & 15);
}

inline void PrintInt(int val) {
    if (val > 9) PrintInt(val / 10);
    putchar('0' | (val % 10));
}

inline void UpdMax(int &a, int b) {
    if (a < b) a = b;
}

const int N = 2e5 + 7, M = 5e4 + 7;
int n, m, c, q;
int p[M], p_id[M], w[N];
struct ToEdge {
    int v, nxt;
} e[N << 1];
int fa[N], dep[N], siz[N], wson[N], beg[N], dfn[N], dfn_w[N], dft; // , id[N]
int head[N], ecnt;
#define AddE(fr, to) e[++ecnt].v = to, e[ecnt].nxt = head[fr], head[fr] = ecnt

namespace segt {
    // struct range {
    //     int l;
    //     mutable int r;
    //     range(int L, int R) : l(L), r(R) {}
    //     bool operator < (const range &b) const {
    //         return l < b.l;
    //     }
    // };
    // set<range> l_to_r[N << 2], r_to_l[N << 2];
    map<int, int> l_to_r[N << 2], r_to_l[N << 2];
    void build(int u, int l, int r) {
        map<int, int> &nowl = l_to_r[u], &nowr = r_to_l[u];
        // map<int, int>::iterator it;
        // map<int, list<int*>* > upd;
        // for (int i = l; i <= r; ++i) {
        //     int w = dfn_w[i];
        //     if (!w) continue;
        //     list<int*>* &where = upd[w];
        //     if (where == NULL) where = new list<int*>();
        //     it = nowl.find(w);
        //     if (it == nowl.end())
        //         where->push_back(&nowl.insert(make_pair(w, w)).first->second);
        //     if (upd.find(w - 1) != upd.end()) upd[w]->splice(upd[w]->end(), *upd[w - 1]);
        // }
        // for (map<int, list<int*>* >::iterator it = upd.begin(); it != upd.end(); ++it)
        //     for (list<int*>::iterator j = it->second->begin(); j != it->second->end(); ++j)
        //         **j = it->first;
        // upd.clear();
        // for (int i = r; i >= l; --i) {
        //     int w = dfn_w[i];
        //     if (!w) continue;
        //     list<int*>* &where = upd[w];
        //     if (where == NULL) where = new list<int*>();
        //     it = nowr.find(w);
        //     if (it == nowr.end())
        //         where->push_back(&nowr.insert(make_pair(w, w)).first->second);
        //     if (upd.find(w - 1) != upd.end()) upd[w]->splice(upd[w]->end(), *upd[w - 1]);
        // }
        // for (map<int, list<int*>* >::iterator it = upd.begin(); it != upd.end(); ++it)
        //     for (list<int*>::iterator j = it->second->begin(); j != it->second->end(); ++j)
        //         **j = it->first;
        // if (l == r) return;
        if (l == r) {
//            int w = dfn_w[l];
            int w = dfn_w[l];
            if (w) nowl[w] = nowr[w] = w;
            return;
        }
        int mid = (l + r) >> 1, ls = u << 1, rs = ls | 1;
        build(ls, l, mid), build(rs, mid + 1, r);
        nowl = l_to_r[ls], nowr = r_to_l[rs];
        map<int, int> &rs_LtoR = l_to_r[rs], &ls_RtoL = r_to_l[ls];
        for (map<int, int>::iterator it = nowl.begin(); it != nowl.end(); ++it) {
            map<int, int>::iterator jt = rs_LtoR.find(it->second + 1);
            if (jt != rs_LtoR.end()) UpdMax(it->second, jt->second);
        }
        for (map<int, int>::iterator it = rs_LtoR.begin(); it != rs_LtoR.end(); ++it)
            UpdMax(nowl[it->first], it->second);
        for (map<int, int>::iterator it = nowr.begin(); it != nowr.end(); ++it) {
            map<int, int>::iterator jt = ls_RtoL.find(it->second + 1);
            if (jt != ls_RtoL.end()) UpdMax(it->second, jt->second);
        }
        for (map<int, int>::iterator it = ls_RtoL.begin(); it != ls_RtoL.end(); ++it)
            UpdMax(nowr[it->first], it->second);
    }
    int pos;
    void queryLtoR(int u, int l, int r, int ml, int mr) {
        if (l >= ml && r <= mr) {
            // map<int, int> *now = l_to_r + u;
            map<int, int>::iterator it = l_to_r[u].find(pos + 1);
            if (it != l_to_r[u].end()) UpdMax(pos, it->second);
            return;
        }
        int mid = (l + r) >> 1, ls = u << 1, rs = ls | 1;
        if (mid >= ml) queryLtoR(ls, l, mid, ml, mr);
        if (mid < mr) queryLtoR(rs, mid + 1, r, ml, mr);
    }
    void queryRtoL(int u, int l, int r, int ml, int mr) {
        if (l >= ml && r <= mr) {
            // map<int, int> *now = r_to_l + u;
            map<int, int>::iterator it = r_to_l[u].find(pos + 1);
            if (it != r_to_l[u].end()) UpdMax(pos, it->second);
            return;
        }
        int mid = (l + r) >> 1, ls = u << 1, rs = ls | 1;
        if (mid < mr) queryRtoL(rs, mid + 1, r, ml, mr);
        if (mid >= ml) queryRtoL(ls, l, mid, ml, mr);
    }
}

void dfs1(int u, int ff) {
    // cerr << "dfs1: " << u << ' ' << ff << endl;
    fa[u] = ff;
    dep[u] = dep[ff] + 1;
    siz[u] = 1;
    for (int i = head[u]; i; i = e[i].nxt) {
        int v = e[i].v;
        if (v != ff) {
            dfs1(v, u);
            siz[u] += siz[v];
            if (siz[v] > siz[wson[u]]) wson[u] = v;
        }
    }
}

void dfs2(int u, int bg) {
    // cerr << "dfs2: " << u << ' ' << bg << endl;
    beg[u] = bg;
    dfn[u] = ++dft;
//    id[dft] = u;
    dfn_w[dft] = w[u];
    if (!wson[u]) return;
    dfs2(wson[u], bg);
    for (int i = head[u]; i; i = e[i].nxt) {
        int v = e[i].v;
        if (v != fa[u] && v != wson[u]) dfs2(v, v);
    }
}

int query(int u, int v) {
    static int s1[47], s2[47];
    int top = 0;
    segt::pos = 0;
    while (beg[u] != beg[v]) {
        if (dep[beg[u]] < dep[beg[v]]) {
            // v jump-up
            s1[++top] = dfn[beg[v]];
            s2[top] = dfn[v];
            v = fa[beg[v]];
        } else {
            // u jump-up
            segt::queryRtoL(1, 1, n, dfn[beg[u]], dfn[u]);
            // printf("[#1] from %d to %d: Collection Increased to %d\n", u, beg[u], segt::pos);
            u = fa[beg[u]];
        }
    }
    if (dep[u] < dep[v]) {
        // v jump-up
        segt::queryLtoR(1, 1, n, dfn[u], dfn[v]);
        // printf("[#2] from %d to %d: Collection Increased to %d\n", u, v, segt::pos);
    } else {
        // u jump-up
        segt::queryRtoL(1, 1, n, dfn[v], dfn[u]);
        // printf("[#3] from %d to %d: Collection Increased to %d\n", u, v, segt::pos);
    }
    while (top) {
        segt::queryLtoR(1, 1, n, s1[top], s2[top]);
        // printf("[#4] from %d to %d: Collection Increased to %d\n", id[s1[top]], id[s2[top]], segt::pos);
        --top;
    }
    return segt::pos;
}

#ifdef Test_TimMem
bool MemTester2;
#endif
int main() {
#ifndef zy_NoFreopen_
    // freopen(SX2021_ProblemName ".in", "r", stdin);
    // freopen(SX2021_ProblemName ".out", "w", stdout);
#endif
//    ios::sync_with_stdio(false);
#ifdef Test_TimMem
    // cerr << "Line [" << __LINE__ << "] Tim Used: " << clock() << "ms" << endl;
#endif
    FastIn(n), FastIn(m), FastIn(c);
    for (int i = 1; i <= c; ++i) {
//        cin >> p[i];
        FastIn(p[i]);
        p_id[p[i]] = i;
    }
    for (int i = 1; i <= n; ++i) {
//        cin >> w[i];
        FastIn(w[i]);
        w[i] = p_id[w[i]];
    }
    for (int i = 1, u, v; i < n; ++i) {
//        cin >> u >> v;
        FastIn(u), FastIn(v);
        AddE(u, v), AddE(v, u);
    }
#ifdef Test_TimMem
    // cerr << "Line [" << __LINE__ << "] Tim Used: " << clock() << "ms" << endl;
#endif
    dfs1(1, 0), dfs2(1, 1);
#ifdef Test_TimMem
    // cerr << "Line [" << __LINE__ << "] Tim Used: " << clock() << "ms" << endl;
#endif
    segt::build(1, 1, n);
#ifdef Test_TimMem
    // cerr << "Line [" << __LINE__ << "] Tim Used: " << clock() << "ms" << endl;
#endif
    FastIn(q);
    for (int i = 1, u, v; i <= q; ++i) {
        FastIn(u), FastIn(v);
        PrintInt(query(u, v)), putchar('\n');
    }
    cout << flush;
#ifdef Test_TimMem
    // cerr << "Tim Used: " << clock() << "ms" << endl;
    // cerr << "Mem Used: " << (&MemTester2 - &MemTester1) / 1048576.0 << "mb" << endl;
#endif
    return 0;
}
//cd gem
//g++ .\gem.cpp -o .\gem.exe -Wall -Wextra -Wl,--stack=12345678 -O2; .\gem

// -Wall -Wextra -Wl,--stack=12345678 -DTest_TimMem -Dzy_NoFreopen -O2

/*

    void build(int u, int l, int r) {
        set<range> &nowl = l_to_r[u], &nowr = r_to_l[u];
        set<range>::iterator it;
        map<int, list<set<range>::iterator> > upd;
        for (int i = l; i <= r; ++i) {
            int w = dfn_w[i];
            if (!w) continue;
            it = nowl.upper_bound(range(w, 0));
            if (it == nowl.begin() || (--it)->r + 1 < w) {
                upd[w].push_back(nowl.insert(range(w, w)).second);
            } else if (it->r + 1 == w) {
                it->r = w;
            }
        }
        for (int i = r; i >= l; --i) {
            int w = dfn_w[i];
            if (!w) continue;
            it = nowr.upper_bound(range(w, 0));
            if (it == nowr.begin() || (--(pre = it))->r + 1 < w) {
                nowr.insert(range(w, w));
            } else if (pre->r + 1 == w) {
                pre->r = w;
                if (it != nowr.end() && w + 1 == it->l) {
                    pre->r = it->r;
                    nowr.erase(it);
                }
            }
        }
        if (l == r) return;
        int mid = (l + r) >> 1, ls = u << 1, rs = ls | 1;
        build(ls, l, mid), build(rs, mid + 1, r);
    }
    int pos;
    void queryLtoR(int u, int l, int r, int ml, int mr) {
        if (l >= ml && r <= mr) {
            set<range> *now = l_to_r + u;
            set<range>::iterator it = now->upper_bound(range(pos + 1, 0));
            if (it != now->begin()) pos = max(pos, (--it)->r);
            return;
        }
        int mid = (l + r) >> 1, ls = u << 1, rs = ls | 1;
        if (mid >= ml) queryLtoR(ls, l, mid, ml, mr);
        if (mid < mr) queryLtoR(rs, mid + 1, r, ml, mr);
    }
    void queryRtoL(int u, int l, int r, int ml, int mr) {
        if (l >= ml && r <= mr) {
            set<range> *now = r_to_l + u;
            set<range>::iterator it = now->upper_bound(range(pos + 1, 0));
            if (it != now->begin()) pos = max(pos, (--it)->r);
            return;
        }
        int mid = (l + r) >> 1, ls = u << 1, rs = ls | 1;
        if (mid < mr) queryRtoL(rs, mid + 1, r, ml, mr);
        if (mid >= ml) queryRtoL(ls, l, mid, ml, mr);
    }
*/
