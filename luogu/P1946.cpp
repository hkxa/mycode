/*************************************
 * problem:      P1946 Olympic_NOI����2009��ߣ�1��.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-13.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define pib pair<int, bool>
int au[15 + 3], ag[15 + 3], cu[15 + 3];
int tot[15 + 3];
pib temp[15 + 3];

int n;

int ans = 0, ansx = 1, ansy = 1, ansz = 1;

inline bool Compare(pib a, pib b)
{
    return a.first < b.first;
}

inline void Solve(int x, int y)
{
    int num = 0, tail = 0;
    for (int i = 1; i <= n; i++)
        tot[i] = au[i] * x + ag[i] * y;
    for (int i = 2; i <= n; i++) {
        double delta = tot[i] - tot[1], k = cu[1] - cu[i];
        if (!k) {
            num += (delta <= 0);
            continue;
        }
        if (k < 0 && delta > 0)
            continue;
        if (k > 0 && delta < 0) {
            num++;
            continue;
        }
        if (k > 0)
            temp[++tail] = make_pair(ceil(delta / k), true);
        else
            temp[++tail] = make_pair(floor(delta / k), false);
    }
    sort(temp + 1, temp + tail + 1, Compare);
    for (int i = 1; i <= tail; i++)
        if (!temp[i].second)
            num++;
    /**
     * 2
     * 0 2 2
     * 2 0 1
     * -----
     * delta = 0
     * k = 1
     * temp[1] = {0, true}
     */
    for (int i = 1; i <= tail; i++) {
        int j = i, cnt = 0;
        while (temp[j + 1].first == temp[i].first && j + 1 <= tail)
            j++;
        for (int k = i; k <= j; k++)
            if (temp[k].second)
                num++/* , x + y == 2 && printf("temp[%d].second = true, num = %d + 1.\n", k, num - 1) */;
            else
                cnt++;
        if (num > ans && (temp[i].first > 0 || (temp[i].first == 0 && j == tail)) && temp[i].first <= y) {
            ans = num;
            ansx = x, ansy = y, ansz = temp[i].first + (temp[i].first == 0 && j == tail);
            // write(ansx, 32);
            // write(ansy, 32);
            // write(ansz, 10);
            // exit(0);
        }
        i = j;
        num -= cnt;
    }

    return;
}

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        au[i] = read<int>();
        ag[i] = read<int>();
        cu[i] = read<int>();
    }
    // Solve(1, 1);
    // Solve(1, 1);
    // Solve(1, 1);
    for (int i = 1; i <= 1000; i++)
        for (int j = 1; j <= i; j++)
            Solve(i, j);
    write(ansx, 32);
    write(ansy, 32);
    write(ansz, 10);
    return 0;
}