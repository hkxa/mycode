/*************************************
 * @problem:      CF1895C.
 * @author:       brealid.
 * @time:         2023-11-03.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

// #define USE_FREAD  // 使用 fread  读入，去注释符号
// #define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 2e5 + 7;

int n;
string a;

int as_lef[6][6][100]; // as_lef[half_len][left_len][cursum + 50]
int as_rig[6][6][100]; // as_rig[half_len][left_len][cursum + 50]

signed main() {
    ios::sync_with_stdio(false);
    cin >> n;
    int64 ans = 0;
    for (int th = 1; th <= n; ++th) {
        cin >> a;
        int cur_len = a.length();
        for (int len = cur_len + 1 + !(cur_len & 1); len <= min(10, cur_len + 5); len += 2) {
            int half_len = len / 2;
            // As LEFT
            int asleft_sum = 0;
            for (int i = 0; i < min(cur_len, half_len); ++i)
                asleft_sum += a[i] & 15;
            for (int i = half_len; i < cur_len; ++i)
                asleft_sum -= a[i] & 15;
            // As RIGHT
            int asright_sum = 0;
            for (int i = 0; i < min(cur_len, half_len); ++i)
                asright_sum += a[cur_len - 1 - i] & 15;
            for (int i = half_len; i < cur_len; ++i)
                asright_sum -= a[cur_len - 1 - i] & 15;
            // get ans
            ans += as_lef[half_len][len - cur_len][asright_sum + 50]
                +  as_rig[half_len][len - cur_len][asleft_sum + 50];
            if (half_len == cur_len) ++ans;
            // sum up
            ++as_lef[half_len][cur_len][asleft_sum + 50];
            ++as_rig[half_len][cur_len][asright_sum + 50];
            // printf("%d %d %d (%d %d)\n", half_len, cur_len, len - cur_len, asleft_sum, asright_sum);
        }
    }
    kout << ans << '\n';
    return 0;
}