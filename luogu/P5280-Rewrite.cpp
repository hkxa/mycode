/*************************************
 * @problem:      Rewrite P5280.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read() {
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c) {
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x) {
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch) {
    write(x);
    putchar(nextch);
}

#define N 100007
#define sizeSGT (N * 8)
#define P 998244353
int n, m;
int f[sizeSGT], g[sizeSGT], tf[sizeSGT], tg[sizeSGT], sf[sizeSGT];
int treeCnt = 1;

#define Mul(a, b) (((int64)(a) * (b)) % P)
#define Mul2(a) (((a) << 1) % P)
#define Add(a, b) (((a) + (b)) % P)
#define Sub(a, b) (((a) - (b) + P) % P)

void pushF(int u, int x) {
    f[u] = Mul(f[u], x);
    tf[u] = Mul(tf[u], x);
    sf[u] = Mul(sf[u], x);
}

void pushG(int u, int x) {
    g[u] = Mul(g[u], x);
    tg[u] = Mul(tg[u], x);
}

void pushDown(int u) {
    if (tf[u] != 1) {
        pushF(u << 1, tf[u]);
        pushF(u << 1 | 1, tf[u]);
        tf[u] = 1;
    }
    if (tg[u] != 1) {
        pushG(u << 1, tg[u]);
        pushG(u << 1 | 1, tg[u]);
        tg[u] = 1;
    }
}

void pushUp(int u) {
    sf[u] = Add(f[u], Add(sf[u << 1], sf[u << 1 | 1]));
} 

void modify(int u, int l, int r, int ml, int mr) {
    pushDown(u);
    if (l > mr || r < ml) {
        // NotInRange
        f[u] = Add(f[u], Sub(treeCnt, g[u]));
        g[u] = Mul2(g[u]);
        pushF(u << 1, 2);
        pushG(u << 1, 2);
        pushF(u << 1 | 1, 2);
        pushG(u << 1 | 1, 2);
    } else if (l >= ml && r <= mr) {
        // InRange
        f[u] = Add(f[u], treeCnt);
        pushF(u << 1, 2);
        pushF(u << 1 | 1, 2);
    } else {
        int mid = (l + r) >> 1;
        g[u] = Add(g[u], treeCnt);
        modify(u << 1, l, mid, ml, mr);
        modify(u << 1 | 1, mid + 1, r, ml, mr);
    }
    pushUp(u);
}

int main() {
    n = read<int>();
    m = read<int>();
    for (int i = 1; i <= n * 4; i++) g[i] = tf[i] = tg[i] = 1;
    for (int i = 1, l, r; i <= m; i++) {
        if (read<int>() == 1) {
            l = read<int>();
            r = read<int>();
            modify(1, 1, n, l, r);
            treeCnt = Mul2(treeCnt);
        } else {
            write(sf[1], 10);
        }
    }
    return 0;
}
