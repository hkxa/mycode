/*************************************
 * @problem:      [PKUWC2018]Slay the Spire.
 * @author:       brealid.
 * @time:         2021-05-12.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 6e3 + 7, P = 998244353;

int n, m, k, a[N], b[N], C[N][N], f[N][N][2], g[N][N][2];

signed main() {
    for (int i = C[0][0] = 1; i < N; ++i)
        for (int j = C[i][0] = 1; j <= i; ++j)
            C[i][j] = (C[i - 1][j - 1] + C[i - 1][j]) % P;
    int T;
    kin >> T;
    while (T--) {
        kin >> n >> m >> k;
        for (int i = 1; i <= n; ++i) kin >> a[i];
        for (int i = 1; i <= n; ++i) kin >> b[i];
        sort(a + 1, a + n + 1, greater<int>());
        sort(b + 1, b + n + 1, greater<int>());
        for (int i = f[0][0][0] = f[0][0][1] = 1; i <= n; ++i)
            for (int j = f[i][0][1] = 1; j <= i; ++j) {
                f[i][j][0] = (int64)a[i] * f[i - 1][j - 1][1] % P;
                f[i][j][1] = (f[i][j][0] + f[i - 1][j][1]) % P;
            }
        for (int i = 1; i <= n; ++i)
            for (int j = 1; j <= i; ++j) {
                g[i][j][0] = ((int64)b[i] * C[i - 1][j - 1] + g[i - 1][j - 1][1]) % P;
                g[i][j][1] = (g[i][j][0] + g[i - 1][j][1]) % P;
            }
        int ans = 0;
        for (int i = 0; i < k - 1; ++i)
            for (int j = 1; j <= n; ++j)
                ans = (ans + (int64)f[n][i][1] * g[j][k - i][0] % P * C[n - j][m - k]) % P;
        for (int i = 0; i <= n; ++i)
            for (int j = 1; j <= n; ++j)
                ans = (ans + (int64)f[i][k - 1][0] * b[j] % P * C[2 * n - i - j][m - k]) % P;
        kout << ans << '\n';
    }
    return 0;
}