//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      [NOI2014]随机数生成器.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-21.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 998244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? Module(w - P) : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

struct point {
    short x, y;
} pos[25000007];

int xr = read<int>(), a, b, c, d;
int n, m, q;
inline int _My_Rand() {
    return xr = ((int64)a * xr * xr + (int64)b * xr + c) % d;
}
int l[5007], r[5007];
int num[25000007];

signed main() {
    a = read<int>();
    b = read<int>();
    c = read<int>();
    d = read<int>();
    n = read<int>();
    m = read<int>();
    q = read<int>();
    for (register int i = 1; i <= n * m; i++) {
        num[i] = i;
        swap(num[i], num[_My_Rand() % i + 1]);
    }
    while (q--) {
        swap(num[read<int>()], num[read<int>()]);
    }
    for (register int i = 1; i <= n * m; i++) {
        pos[num[i]] = (point){(i + m - 1) / m, (i - 1) % m + 1};
    }
    for (register int i = 1; i <= n; i++) {
        l[i] = 1;
        r[i] = m;
    }
    // fprintf(stderr, "C = %u\n", clock());
    for (register int i = 1, tx, ty; i <= n * m; i++) {
        tx = pos[i].x;
        ty = pos[i].y;
        if (ty < l[tx] || ty > r[tx]) continue;
        for (int i = 1; i < tx; i++) r[i] = min(r[i], ty);
        for (int i = tx + 1; i <= n; i++) l[i] = max(l[i], ty);
        write(i, 32);
    }
    putchar(10);
    return 0;
}

// Create File Date : 2020-06-21