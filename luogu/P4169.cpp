//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      [Violet]天使玩偶/SJY摆棋子.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-07.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 2e6 + 7;
const double alpha = 0.75;

struct position {
    int v[2];
} pos[N];

namespace kD_Tree {
    struct kD_Tree_Node {
        int v[2];
        int siz;
        int minv[2], maxv[2];
        int l, r;
        bool different_from(const int &x, const int &y) const {
            return x != v[0] || y != v[1];
        }
    } t[N];
    int kD_Tree_temp_node_cnt, kD_Tree_Node_cnt, root;
    inline bool cmp_v0_inc(const position &a, const position &b) {
        return a.v[0] ^ b.v[0] ? a.v[0] < b.v[0] : a.v[1] < b.v[1];
    }
    inline bool cmp_v1_inc(const position &a, const position &b) {
        return a.v[1] ^ b.v[1] ? a.v[1] < b.v[1] : a.v[0] < b.v[0];
    }
    typedef bool cmp_v_inc(const position &a, const position &b);
    cmp_v_inc *cmp_func[2] = { cmp_v0_inc, cmp_v1_inc };
    inline int euclid_dist(const kD_Tree_Node &a, const int &x, const int &y) {
        return abs(a.v[0] - x) + abs(a.v[1] - y);
    }
    inline int farthest(const kD_Tree_Node &a, const int &x, const int &y) {
        return max(abs(a.minv[0] - x), abs(a.maxv[0] - x)) + max(abs(a.minv[1] - y), abs(a.maxv[1] - y));
    }
    inline int nearest(const kD_Tree_Node &a, const int &x, const int &y) {
        return max(max(a.minv[0] - x, x - a.maxv[0]), 0) + max(max(a.minv[1] - y, y - a.maxv[1]), 0);
    }
    inline void pushup(int u) {
        t[u].minv[0] = t[u].maxv[0] = t[u].v[0];
        t[u].minv[1] = t[u].maxv[1] = t[u].v[1];
        t[u].siz = 1;
        if (t[u].l) {
            t[u].minv[0] = min(t[u].minv[0], t[t[u].l].minv[0]);
            t[u].maxv[0] = max(t[u].maxv[0], t[t[u].l].maxv[0]);
            t[u].minv[1] = min(t[u].minv[1], t[t[u].l].minv[1]);
            t[u].maxv[1] = max(t[u].maxv[1], t[t[u].l].maxv[1]);
            t[u].siz += t[t[u].l].siz;
        }
        if (t[u].r) {
            t[u].minv[0] = min(t[u].minv[0], t[t[u].r].minv[0]);
            t[u].maxv[0] = max(t[u].maxv[0], t[t[u].r].maxv[0]);
            t[u].minv[1] = min(t[u].minv[1], t[t[u].r].minv[1]);
            t[u].maxv[1] = max(t[u].maxv[1], t[t[u].r].maxv[1]);
            t[u].siz += t[t[u].r].siz;
        }
    }
    void build(int &u, int l, int r, int D) {
        if (l > r) return;
        u = ++kD_Tree_Node_cnt;
        int mid = (l + r) >> 1;
        nth_element(pos + l, pos + mid, pos + r + 1, cmp_func[D]);
        t[u].v[0] = pos[mid].v[0];
        t[u].v[1] = pos[mid].v[1];
        build(t[u].l, l, mid - 1, !D);
        build(t[u].r, mid + 1, r, !D);
        pushup(u);
    }
    void flatten(int u) {
        if (t[u].l) flatten(t[u].l);
        pos[++kD_Tree_temp_node_cnt].v[0] = t[u].v[0];
        pos[kD_Tree_temp_node_cnt].v[1] = t[u].v[1];
        if (t[u].r) flatten(t[u].r);
    }
    inline void rebuild(int &u, int D) {
        kD_Tree_temp_node_cnt = 0;
        flatten(u);
        build(u, 1, kD_Tree_temp_node_cnt, D);
    }
    void maintain_balance(int &u, int D) {
        if (!u) return;
        else if (t[t[u].l].siz >= t[u].siz * alpha || t[t[u].r].siz >= t[u].siz * alpha) rebuild(u, D);
        else if (t[t[u].l].siz > t[t[u].r].siz) maintain_balance(t[u].l, !D);
        else maintain_balance(t[u].r, !D);
    }
    void insert(int &u, const position &a, int D) {
        if (!u) {
            u = ++kD_Tree_Node_cnt;
            t[u].v[0] = a.v[0];
            t[u].v[1] = a.v[1];
        } else if (a.v[D] < t[u].v[D]) {
            insert(t[u].l, a, !D);
        } else {
            insert(t[u].r, a, !D);
        }
        pushup(u);
    }
    void insert(const int &x, const int &y) {
        static position ForInsertTempPosition;
        ForInsertTempPosition.v[0] = x;
        ForInsertTempPosition.v[1] = y;
        insert(root, ForInsertTempPosition, 0);
        maintain_balance(root, 0);
    }
    int ans;
    void find_nearest(const int &u, const int &x, const int &y) {
        ans = min(ans, euclid_dist(t[u], x, y));
        register int dl = nearest(t[t[u].l], x, y), dr = nearest(t[t[u].r], x, y);
        if (dl < dr) {
            if (t[u].l && dl < ans) find_nearest(t[u].l, x, y);
            if (t[u].r && dr < ans) find_nearest(t[u].r, x, y);
        } else {
            if (t[u].r && dr < ans) find_nearest(t[u].r, x, y);
            if (t[u].l && dl < ans) find_nearest(t[u].l, x, y);
        }
    }
    void find_farthest(const int &u, const int &x, const int &y) {
        ans = max(ans, euclid_dist(t[u], x, y));
        int dl = farthest(t[t[u].l], x, y), dr = farthest(t[t[u].r], x, y);
        if (dl > dr) {
            if (t[u].l && dl > ans) find_farthest(t[u].l, x, y);
            if (t[u].r && dr > ans) find_farthest(t[u].r, x, y);
        } else {
            if (t[u].r && dr > ans) find_farthest(t[u].r, x, y);
            if (t[u].l && dl > ans) find_farthest(t[u].l, x, y);
        }
    }
    inline int query_diff(const int &x, const int &y) {
        ans = -1e9;
        find_farthest(root, x, y);
        int nRet = ans;
        ans = 1e9;
        find_nearest(root, x, y);
        return nRet - ans;
    }
    inline int query_farthest(const int &x, const int &y) {
        ans = -1e9;
        find_farthest(root, x, y);
        return ans;
    }
    inline int query_nearest(const int &x, const int &y) {
        ans = 1e9;
        find_nearest(root, x, y);
        return ans;
    }
}

namespace against_cpp11 {
    int n, m, ans = 1e9;
    signed main() {
        read >> n >> m;
        for (register int i = 1; i <= n; i++)
            read >> pos[i].v[0] >> pos[i].v[1];
        kD_Tree::build(kD_Tree::root, 1, n, 0);
        for (register int i = 1, opt, x, y; i <= m; i++) {
            // fprintf(stderr, "dealing option %d\n", i);
            read >> opt >> x >> y;
            if (opt == 1) kD_Tree::insert(x, y);
            else write << kD_Tree::query_nearest(x, y) << '\n';
        }
        return 0;
    }
}

signed main() { return against_cpp11::main(); }