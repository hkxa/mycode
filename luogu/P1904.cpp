/*************************************
 * problem:      P1904 天际线.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-04.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n = 0;
#define NEW_BUILDING 1
#define END_BUILDING 0
struct Line {
    int buildingId;
    bool type;
    int pos, high;
    bool operator < (const Line &other) const
    {
        return pos != other.pos ? pos < other.pos : type > other.type;
    }
} b[100000 + 7];

struct PQ_Node {
    int buildingId;
    int pos, high;
    PQ_Node(const Line other) {
        buildingId = other.buildingId;
        pos = other.pos;
        high = other.high;
    }
    bool operator < (const PQ_Node &other) const
    {
        return high < other.high;
    }
};

bool readln()
{
    static int l, h, r;
    if (scanf("%d%d%d", &l, &h, &r) == EOF) return false;
    b[n] = (Line){n >> 1, NEW_BUILDING, l, h};
    n++;
    b[n] = (Line){n >> 1, END_BUILDING, r, h};
    n++;
    return true;
}

bool ended[50000 + 7] = {false};

priority_queue<PQ_Node> pq;

int main()
{
    while (readln());
    sort(b, b + n);
    int y = 0;
    for (int i = 0; i < n; i++) {
        if (b[i].type == NEW_BUILDING) {
            pq.push(PQ_Node(b[i]));
            if (pq.top().high != y) {
                y = pq.top().high;
                write(b[i].pos, 32);
                write(y, 32);
            }
        } else {
            ended[b[i].buildingId] = true;
            while (!pq.empty() && ended[pq.top().buildingId]) pq.pop();
            if (pq.empty() || pq.top().high != y) {
                y = pq.empty() ? 0 : pq.top().high;
                write(b[i].pos, 32);
                write(y, 32);
            }
        }

    }
    return 0;
}