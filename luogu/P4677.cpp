/*************************************
 * @problem:      P4677 山区建小学.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-02-17.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, m;
int d[507];
int PreSum[507], mid;
int f[507][507];
int cost[507][507];

int main()
{
    n = read<int>();
    m = read<int>();
    for (int i = 2; i <= n; i++) d[i] = d[i - 1] + read<int>();
    for (int i = 1; i <= n; i++) {
        for (int j = i; j <= n; j++) {
            mid = (i + j) >> 1;
            for (int k = i; k <= j; k++) {
                cost[i][j] += abs(d[mid] - d[k]);
            }
            // printf("cost[%d][%d] = %d.\n", i, j, cost[i][j]);
        }
    }
    memset(f, 0x3f, sizeof(f));
    f[0][0] = 0;
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= min(i, m); j++) {
            for (int k = j - 1; k <= i; k++) 
                f[i][j] = min(f[i][j], f[k][j - 1] + cost[k + 1][i]);
        } 
    }
    write(f[n][m], 10);
    return 0;
}