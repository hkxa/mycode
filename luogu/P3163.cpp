//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      [CQOI2014]危桥.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-14.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }

// #define int int64

const int N = 10007, M = 200007;

int n, m, s, t;
int head[N], to[M], nxt[M], val[M], cnt;
int flow[N], h[N], CntH[N];
bool inQ[N];

struct myCmp {
    bool operator () (const int &a, const int &b) const {
        return h[a] < h[b];
    }
};

#define ADD_EDGE(a, b, v) to[cnt] = (b); val[cnt] = (v); nxt[cnt] = head[a]; head[a] = cnt++;
#define notST(u) ((u) != s && (u) != t)
#define ableToPush(u) (!inQ[u] && notST(u) && TryToRelabel(u))

inline bool PushFlow(int u, int e) {
    int v = to[e], delta = std::min(flow[u], val[e]);
    // printf("e %d->%d v(%d) : delta = %d\n", u, to[e], val[e], delta);
    flow[u] -= delta;
    val[e] -= delta;
    flow[v] += delta;
    val[e ^ 1] += delta;
    return delta;
}

inline bool TryToRelabel(int u) {
    int MinH = n + 5;
    for (int e = head[u]; ~e; e = nxt[e]) {
        if (val[e]) {
            MinH = std::min(MinH, h[to[e]]);
        }
    }
    if (!--CntH[h[u]])
        for (int i = 1; i <= n; i++)
            if (notST(i) && h[i] > h[u]) {
                CntH[h[i]]--;
                h[i] = n + 1;
            }
    h[u] = MinH + 1;
    CntH[h[u]]++;
    if (MinH == n + 5) return false;
    return true;
}

bool PreBfs() {
    memarr(n, n + 5, h);
    h[t] = 0;
    std::queue<int> q;
    q.push(t);
    inQ[t] = true;
    while (!q.empty()) {
        int u = q.front(); q.pop();
        for (int e = head[u]; ~e; e = nxt[e]) {
            if (!val[e] && h[to[e]] > h[u] + 1) {
                h[to[e]] = h[u] + 1;
                if (!inQ[to[e]]) {
                    q.push(to[e]);
                    inQ[to[e]] = true;
                }
            }
        }
        inQ[u] = 0;
    }
    return h[s] != n + 5;
}

void PFAA(bool flag = 0) {
    // Pre-Flow Advancing Algorithm 预流推进算法
    // HLPP
    std::priority_queue<int, std::vector<int>, myCmp> q;
    h[s] = n + 1;
    memarr(n, 0, CntH);
    if (!flag) memarr(n, 0, flow);
    for (int i = 1; i <= n; i++) CntH[h[i]]++;
    for (int e = head[s]; ~e; e = nxt[e]) {
        flow[s] -= val[e];
        flow[to[e]] += val[e];
        val[e ^ 1] = val[e];
        val[e] = 0;
        if (!inQ[to[e]] && notST(to[e])) {
            q.push(to[e]);
            inQ[to[e]] = true;
        }
    }
    // int Ct = 0;
    while (!q.empty()) {
        // Ct++;
        int u = q.top(); q.pop();
        // printf("Node %d : flow = %d, h = %d\n", u, flow[u], h[u]);
        for (int e = head[u]; ~e && flow[u]; e = nxt[e]) {
            if (h[u] == h[to[e]] + 1 && PushFlow(u, e) && !inQ[to[e]] && notST(to[e])) {
                q.push(to[e]);
                inQ[to[e]] = true;
            }
        }
        if (flow[u] && TryToRelabel(u)) q.push(u);
        else inQ[u] = false;
        // for (int i = 1; i <= n; i++) printf("flow[%d] = %d;   ", i, flow[i]);
        // printf("\n");
    }
    // printf("Ct = %lld\n", Ct);
}

signed main() {
    while (scanf("%d", &n) != EOF) {
        s = n;
        t = n + 1;
        memarr(n, -1, head);
        int a1, a2, an, b1, b2, bn, lim;
        std::cin >> a1 >> a2 >> an >> b1 >> b2 >> bn;
        lim = (an + bn) << 1;
        char buf[53][53];
        cnt = 0;
        for (int i = 0; i < n; i++) {
            scanf("%s", buf[i]);
            for (int j = 0; j < n; j++){
                switch (buf[i][j]) {
                    case 'N' :
                        ADD_EDGE(i, j, lim);
                        ADD_EDGE(j, i, 0);
                        break;
                    case 'O' :
                        ADD_EDGE(i, j, 2);
                        ADD_EDGE(j, i, 0);
                        break;
                }
            }
        }
        // std::swap(b1, b2);
        ADD_EDGE(s, a1, an << 1);
        ADD_EDGE(a1, s, 0);
        ADD_EDGE(a2, t, an << 2);
        ADD_EDGE(t, a2, 0);
        ADD_EDGE(s, b1, bn << 1);
        ADD_EDGE(b1, s, 0);
        ADD_EDGE(b2, t, bn << 2);
        ADD_EDGE(t, b2, 0);
        if (!PreBfs()) { puts("No"); continue; }
        PFAA();
        if (flow[t] ^ lim) { puts("No"); continue; }
        // printf("CONT } ");
        memarr(n, -1, head);
        cnt = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++){
                switch (buf[i][j]) {
                    case 'N' :
                        ADD_EDGE(i, j, lim);
                        ADD_EDGE(j, i, 0);
                        break;
                    case 'O' :
                        ADD_EDGE(i, j, 2);
                        ADD_EDGE(j, i, 0);
                        break;
                }
            }
        }
        ADD_EDGE(s, a1, an << 1);
        ADD_EDGE(a1, s, 0);
        ADD_EDGE(a2, t, an << 2);
        ADD_EDGE(t, a2, 0);
        ADD_EDGE(s, b2, bn << 1);
        ADD_EDGE(b2, s, 0);
        ADD_EDGE(b1, t, bn << 2);
        ADD_EDGE(t, b1, 0);
        if (!PreBfs()) { puts("No"); continue; }
        PFAA();
        if (flow[t] ^ lim) { puts("No"); /*printf("%d\n", flow[t]);*/ continue; }
        puts("Yes");
    }
    return 0;
}

// Create File Date : 2020-06-13