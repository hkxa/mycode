/*************************************
 * @problem:      [CTSC1998]算法复杂度.
 * @author:       brealid.
 * @time:         2021-01-31.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

int strToInt(string ss) {
    if (ss == "n") return -1;
    stringstream turner(ss);
    int number;
    turner >> number;
    return number;
}

void readUntilEnd() {
    string s;
    for (int top = 1; top;) {
        cin >> s;
        if (s == "loop") ++top;
        else if (s == "end") --top;
    }
}

struct node {
    int val;
    bool isOperation;
    node *fa;
    vector<node> child;
    node(int V, bool Cond, node *F) : val(V), isOperation(Cond), fa(F) {}
} root(1, false, NULL);

int ans[23];

void solve(node *p, int exponent, int coefficient) {
    if (p->val != -1) coefficient *= p->val;
    else ++exponent;
    if (p->isOperation) ans[exponent] += coefficient;
    for (size_t i = 0; i < p->child.size(); ++i)
        solve(&p->child[i], exponent, coefficient);
}

signed main() {
    node *p = &root;
    string cmd, x;
    while (true) {
        cin >> cmd;
        if (cmd == "loop") {
            cin >> x;
            if (x == "0") x = "n";
            p->child.push_back(node(strToInt(x), false, p));
            p = &p->child.back();
        } else if (cmd == "op") {
            cin >> x;
            p->child.push_back(node(strToInt(x), true, p));
        } else if (cmd == "break") {
            if (p == &root) continue;
            p->val = 1;
            p = p->fa;
            readUntilEnd();
        } else if (cmd == "continue") {
            if (p == &root) continue;
            p = p->fa;
            readUntilEnd();
        } else if (cmd == "end") {
            if (p == &root) break;
            p = p->fa;
        }
    }
    solve(&root, 0, 1);
    bool first = true;
    for (int i = 22; i >= 0; --i) {
        if (!ans[i]) continue;
        if (!first) cout << '+';
        else first = false;
        if (ans[i] != 1 || !i) cout << ans[i];
        if (i >= 1) cout << 'n';
        if (i >= 2) cout << '^' << i;
    }
    if (first) cout << "0";
    cout << endl;
    return 0;
}