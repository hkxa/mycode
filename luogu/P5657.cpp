// 100pts
#include <bits/stdc++.h>
using namespace std;
#define openFile(x) freopen(x".in","r",stdin);freopen(x".out","w",stdout)

string solve(long long n, long long k)
{
    // printf("solve(%d, %d)\n", n, k);
    if (!n) return "";
    return k < (1ll << (n - 1)) ? '0' + solve(n - 1, k) : '1' + solve(n - 1, (1ll << n) - k - 1);
}

int main()
{
    openFile("code");
    long long n, k;
    scanf("%lld%lld", &n, &k);
    cout << solve(n, k);
    return 0;
}