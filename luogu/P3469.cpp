/*************************************
 * problem:      P3469 [POI2008]BLO-Blockade.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-07-13.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

const int N = 100000 + 100;
vector<long long> e[N], nd_size[N];
int n, m;
int dfn[N], low[N], IsGD[N], nd_to, size[N];
void Tarjan(int now, int fa) {
    dfn[now] = low[now] = ++nd_to;
    size[now]++;
    int temp = 0;
    for (int i = 0; i < int(e[now].size()); i++)
        if (dfn[e[now][i]] == 0) {
            Tarjan(e[now][i], now);
            size[now] += size[e[now][i]];
            low[now] = min(low[now], low[e[now][i]]);
            if (low[e[now][i]] >= dfn[now]) {
                temp += size[e[now][i]];
                IsGD[now] = true;
                nd_size[now].push_back(size[e[now][i]]);
            }
        } else if (e[now][i] != fa)
            low[now] = min(low[now], low[e[now][i]]);
    if (IsGD[now] == true and n - temp - 1 != 0)
        nd_size[now].push_back(n - temp - 1);
}
int main() {
    n = read<int>(), m = read<int>();
    for (int i = 1; i <= n; i++) {
        e[i].reserve(4);
        nd_size[i].reserve(4);
    }
    for (int i = 1; i <= m; i++) {
        int s = read<int>(), t = read<int>();
        e[s].push_back(t);
        e[t].push_back(s);
    }

    Tarjan(1, 0);

    for (int i = 1; i <= n; i++) {
        long long ans = 2 * (n - 1);
        if (nd_size[i].size() != 0 and nd_size[i].size() != 1) {
            for (int j = 0; j < int(nd_size[i].size()); j++)
                for (int k = j + 1; k < int(nd_size[i].size()); k++) ans += 2 * nd_size[i][j] * nd_size[i][k];
        }
        write(ans, 10);
    }
    return 0;
}