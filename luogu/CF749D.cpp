//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      D - Leaving Auction.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-12.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

namespace against_cpp11 {
    const int N = 2e5 + 7;
    int n, q;
    int l[N];
    vector<int> G[N];
    set<pair<int, int> > s;
    template <typename Iter> Iter pre(Iter it) { return --it; }
    template <typename Iter> Iter nxt(Iter it) { return ++it; }
    signed main() {
        read >> n;
        for (int i = 1, a, b; i <= n; i++) {
            read >> a >> b;
            G[a].push_back(b);
        }
        for (int i = 1; i <= n; i++) 
            if (!G[i].empty()) s.insert(make_pair(G[i].back(), i));
        read >> q;
        for (int i = 1, k; i <= q; i++) {
            read >> k;
            for (int j = 1; j <= k; j++) {
                read >> l[j];
                if (G[l[j]].empty()) l[j] = 0;
                else s.erase(make_pair(G[l[j]].back(), l[j]));
            }
            if (s.size() == 0) printf("0 0\n"); 
            else if (s.size() == 1) printf("%d %d\n", s.begin()->second, *G[s.begin()->second].begin());
            else {
                int num = pre(s.end())->second; 
                vector<int>::iterator it = upper_bound(G[num].begin(), G[num].end(), pre(pre(s.end()))->first);
                printf("%d %d\n", num, *it);
            }
            for (int j = 1; j <= k; j++)
                if (l[j]) s.insert(make_pair(G[l[j]].back(), l[j]));
        }
        return 0;
    }
}

signed main() { return against_cpp11::main(); }