// P2860 [USACO06JAN]冗余路径Redundant Paths
#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

struct Edge {
    int to, nxt;
    bool vis;
    Edge() {}
    Edge(int t, int n) : to(t), nxt(n) {}
} gra[20005];

int ecnt = 0, head[5005];
bool gra1[5005][5005];
int f, r;
int dfn[5005], low[5005], par[5005], step = 1;
int lable[5005];
int du[5005];

void conn(int u, int v) {
    gra[ecnt] = Edge(v, head[u]);
    head[u] = ecnt;
    gra[ecnt + 1] = Edge(u, head[v]);
    head[v] = ecnt + 1;
    gra[ecnt].vis = gra[ecnt + 1].vis = false;
    ecnt += 2;
}

void dfs(int u) 
{
    dfn[u] = low[u] = step++;
    int e = head[u];
    while(e != -1) {
        if(gra[e].vis) {
            e = gra[e].nxt;
            continue;
        }
        gra[e].vis = gra[e ^ 1].vis = true;
        if(dfn[gra[e].to] == 0) {
            par[gra[e].to] = e;
            dfs(gra[e].to);
            low[u] = min(low[gra[e].to], low[u]);
        } else {
            low[u] = min(dfn[gra[e].to], low[u]);
        }
        e = gra[e].nxt;
    }
}

void sd(int u, int num) 
{
    lable[u] = num;
    int e = head[u];
    while(e != -1) {
        if(gra[e].vis && lable[gra[e].to] == 0) {
            sd(gra[e].to, num);
        }
        e = gra[e].nxt;
    }
}

int main() 
{
    memset(head, -1, sizeof(head));
    f = read<int>();
    r = read<int>();
    for (int i = 0; i < r; i++) {
        conn(read<int>(), read<int>());
    }
    dfs(1);
    for (int i = 1; i <= f; i++) {
        int opp = par[i] ^ 1;
        int oppp = gra[opp].to;
        if (low[i] > dfn[oppp]) {
            gra[par[i]].vis = false;
            gra[opp].vis = false;
        }
    }
    int num = 1;
    for (int i = 1; i <= f; i++) {
        if (lable[i] == 0) {
            sd(i, num++);
        }
    }
    for (int i = 1; i <= f; i++) {
        int e = head[i];
        while (e != -1) {
            gra1[lable[gra[e].to]][lable[i]] = 
                gra1[lable[i]][lable[gra[e].to]] = true;
            e = gra[e].nxt;
        }
    } 
    for (int i = 1; i < num; i++) {
        for (int j = 1; j < num; j++) {
            if (i != j && gra1[i][j]) du[i]++;
        }
    }
    int ans = 0;
    for (int i = 1; i < num; i++) {
        if (du[i] == 1) ans++;
    } 
    write((ans + 1) / 2);
    return 0;
}