/*************************************
 * problem:      UVA1108, SP16185, P3225.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-07-21.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

int head[50007], ver[100007], nex[100007], tot;
vector<int> dcc[50007];
typedef vector<int>::iterator Vec_it;
int dfn[50007], low[50007], cut[50007], myStack[50007], st, root, dfs_clock = 1, cc;

#define addedge(u, v) { ver[tot] = v; nex[tot] = head[u]; head[u] = tot++; }

inline void tarjan(int cur) 
{
    myStack[st++] = cur;
    dfn[cur] = low[cur] = dfs_clock++;
    if (cur == root && head[cur] == -1) {
        dcc[cc++].push_back(cur);
        return;
    }
    int flag = 0;
    for (int i = head[cur]; i != -1; i = nex[i]) {
        if (!dfn[ver[i]]) {
            tarjan(ver[i]);
            low[cur] = min(low[cur], low[ver[i]]);
            if(dfn[cur] <= low[ver[i]]) {
                flag++;
                if(cur != root || flag == 2) cut[cur] = 1;
                int t;
                do {
                    t = myStack[(st--)-1];
                    dcc[cc].push_back(t);
                } while(t != ver[i]);
                dcc[cc++].push_back(cur);
            }
        } else {
            low[cur] = min(low[cur], dfn[ver[i]]);
        }
    }
}

int main() {
    int n, m, u, v, cases = 1;
    while (scanf("%d", &m) != EOF && m) {
        memset(head, -1, sizeof(head));
        memset(dfn, 0, sizeof(dfn));
        memset(cut, 0, sizeof(cut)); 
        n = cc = tot = 0;
        while (m--) {
            u = read<int>();
            v = read<int>();
            addedge(u, v);
            addedge(v, u);
        }
        n = max(max(n, u), v); 
        for (int i = 1; i <= n; i++) {
            if (!dfn[i]) {
                st = 0;
                root = i;
                tarjan(i);
            }
        }
        if (cc == 1) {
            printf("Case %d: 2 %lld\n", cases++, (long long)dcc[0].size() * (dcc[0].size() - 1) / 2);
            dcc[0].clear();
            continue;
        }
        long long num = 0, sor = 1;
        for (int i = 0; i < cc; i++) {
            int ncut = 0;
            for (Vec_it it = dcc[i].begin(); it != dcc[i].end(); it++)
                if(cut[*it]) ncut++;
            if (ncut == 1) {
                num++; 
                sor *= (dcc[i].size() - ncut);
            }
            dcc[i].clear();
        }
        printf("Case %d: %lld %lld\n", cases++, num, sor);
    }   
    return 0;
}