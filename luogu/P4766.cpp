/*************************************
 * @problem:      P4766 [CERC2014]Outer space invaders.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-01-19.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

template <typename I, typename J> I UpdMin(I &S, J F) 
{ return S > F ? S = F : S; }

int T = -1;
int n;
int lshval[10007], lshcnt = 0;
int a[307], b[307], d[307];
int f[607][607];

int main()
{
	if (T == -1) T = read<int>();
	if (T == 0) return 0;
	T--;
	// ====================== start dealing ======================
	// ---------------------------- [MEMSET]
	memset(lshval, 0, sizeof(lshval));
	memset(f, 0, sizeof(f));
	lshcnt = 0;
	// ---------------------------- [READLN]
	n = read<int>();
	for (int i = 1; i <= n; i++) {
		a[i] = read<int>();
		b[i] = read<int>();
		d[i] = read<int>();
		lshval[a[i]] = 1;
		lshval[b[i]] = 1;
	}
	for (int i = 1; i <= 10000; i++) {
		if (lshval[i]) lshval[i] = ++lshcnt;
	}
	for (int i = 1; i <= n; i++) {
		a[i] = lshval[a[i]];
		b[i] = lshval[b[i]];
	}
	// ---------------------------- [DP]
	for (int l = 2; l <= lshcnt; l++) {
		for (int i = 1, j = l, pos; j <= lshcnt; i++, j++) {
			pos = 0;
			for (int k = 1; k <= n; k++)
				if (a[k] >= i && b[k] <= j && (!pos || d[k] > d[pos])) pos = k;
			if (!pos) continue;
			f[i][j] = 0x3fffffff;
			for (int k = a[pos]; k <= b[pos]; k++) {
				UpdMin(f[i][j], f[i][k - 1] + f[k + 1][j] + d[pos]);
			}
		}
	}
	write(f[1][lshcnt], 10);
	// ====================== end dealing ========================
	main();
    return 0;
}
