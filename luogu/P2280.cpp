//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      P2280 [HNOI2003]激光炸弹.
 * @user_id:      63720.
 * @user_name:    brealid.
 * @time:         2020-06-01.
 * @language:     C++.
 * @upload_place: luogu.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// #define int int64
const int N = 5007;
int n, r;
int a[N][N];

#define S(x1, y1, x2, y2) (a[x2][y2] - a[x1 - 1][y2] - a[x2][y1 - 1] + a[x1 - 1][y1 - 1])

signed main() {
    n = read<int>();
    r = read<int>();
    for (int i = 1, x, y; i <= n; i++) {
        x = read<int>() + 1;
        y = read<int>() + 1;
        a[x][y] += read<int>();
    }
    for (int i = 1; i <= 5001; i++)
        for (int j = 1; j <= 5001; j++)
            a[i][j] += a[i - 1][j] + a[i][j - 1] - a[i - 1][j - 1];
    int ans = 0;
    for (int i = r; i <= 5001; i++)
        for (int j = r; j <= 5001; j++)
            ans = max(ans, S(i - r + 1, j - r + 1, i, j));
    // write(S(1, 1, 1, 1), 10);
    // write(S(2, 2, 2, 2), 10);
    // write(S(3, 3, 3, 3), 10);
    write(ans, 10);
    return 0;
}

// Create File Date : 2020-06-01