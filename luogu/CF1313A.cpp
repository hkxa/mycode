/*************************************
 * @contest:      Codeforces Round #622 (Div. 2).
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-02-23.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int T;
int a, b, c;
int cnt;
bool AC, BC, AB, ABC;

int main()
{
    T = read<int>();
    while (T--) {
        a = read<int>();
        b = read<int>();
        c = read<int>();
        cnt = 0;
        AC = BC = AB = 0;
        if (a) { a--; cnt++; }
        if (b) { b--; cnt++; }
        if (c) { c--; cnt++; }
        if (a >= 1 && b >= 1 && c >= 2) { a--; b--; c -= 2; cnt += 2; AC = BC = 1; }
        if (a >= 1 && b >= 1 && !AB) { a--; b--; cnt++; AB = true; }
        if (a >= 1 && c >= 1 && !AC) { a--; c--; cnt++; AC = true; }
        if (c >= 1 && b >= 1 && !BC) { c--; b--; cnt++; BC = true; }
        if (a && b && c) { a++; b++; c++; cnt++; ABC = true; }
        write(cnt, 10);
    }
    return 0;
}