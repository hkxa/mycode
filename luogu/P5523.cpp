/*************************************
 * @problem:      [yLOI2019] 珍珠.
 * @author:       brealid.
 * @time:         2021-01-03.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#include <cstdio>

namespace Maker {

typedef unsigned int uit;

bool __sp;
uit __x, __y, __z;
int __type, __k, __m;

const int L = 1 << 21;
char buf[L], *front=buf, *end=buf;
char GetChar() {
  if (front == end) {
    end = buf + fread(front = buf, 1, L, stdin);
    if (front == end) return -1;
  }
  return *(front++);
}

template <typename T>
inline void qr(T &x) {
  char ch = GetChar(), lst = ' ';
  while ((ch > '9') || (ch < '0')) lst = ch, ch = GetChar();
  while ((ch >= '0') && (ch <= '9')) x = (x << 1) + (x << 3) + (ch ^ 48), ch = GetChar();
  if (lst == '-') x = -x;
}

template <typename T>
inline void Begin(const T &x) {
  __type = x % 10;
  qr(__x); qr(__y); qr(__z); qr(__m);
  __sp = (__type == 3) || (__type == 4); __type &= 1;
}

inline uit __Next_Integer() {
  __x ^= __y << (__z & 31);
  __y ^= __z >> (__x & 31);
  __z ^= __x << (__y & 31);
  __x ^= __x >> 5; __y ^= __y << 13; __z ^= __z >> 6;
  return __x;
}

inline uit Rand() { return __Next_Integer(); }

template <typename Tx, typename Ty, typename Tz>
inline void Get_Nextline(Tx &x, Ty &y, Tz &z) {
  if (__m) {
    --__m;
    x = 0; y = 0; z = 0;
    qr(x); qr(y); qr(z);
    if (x == 0) {
      ++__k;
    }
  } else {
    x = Rand() & 1; y = Rand() & 1;
    if (__k == 0) { x = 0; }
    if (x == 0) {
      ++__k;
      if (__sp) {
        z = __type;
      } else {
        z = Rand() & 1;
      }
    } else {
      int dk = __k >> 1;
      if (dk == 0) {
        z = 1;
      } else {
        z = Rand() % dk + dk;
      }
    }
  }
}

} // namespace Maker

struct solver {
    int type[20000010], nearest[20000010];
    int l, r;
    solver() : l(10000010), r(10000009) { memset(nearest, -1, sizeof(nearest)); }
    void add_pre(int id) {
        type[--l] = id;
        if (id == 0) {
            nearest[l] = l;
            for (int p = l + 1; p <= r && type[p]; ++p)
                nearest[p] = l;
        }
    }
    void add_suf(int id) {
        type[++r] = id;
        if (id == 0) nearest[r] = r;
        else nearest[r] = nearest[r - 1];
    }
    bool query(int Distance) {
        // if (Distance == 1) return type[l];
        int id = l + Distance - 1;
        int pre0 = nearest[id];
        if (pre0 < l) return Distance & 1;
        if (pre0 == l) return !(Distance & 1);
        return !((id - pre0) & 1);
    }
} pre, suf;

int ans0, ans1, ans2, ans3;

signed main() {
    int n;
    scanf("%d", &n);
    Maker::Begin(n);
    for (int i = 1, x, y, z; i <= n; ++i) {
        Maker::Get_Nextline(x, y, z);
        if (x == 0) {
            if (y) {
                pre.add_suf(z);
                suf.add_pre(z);
            } else {
                pre.add_pre(z);
                suf.add_suf(z);
            }
        } else {
            if ((y == 0 ? pre : suf).query(z)) {
                ++ans0;
                if (!(i & 1)) ++ans2;
                // printf("Query #%d: 1\n", i);
            } else {
                if (i & 1) ++ans1;
                if (!(i & 1023)) ++ans3;
                // printf("Query #%d: 0\n", i);
            }
        }
    }
    printf("%d %d %d %d\n", ans0, ans1, ans2, ans3);
    return 0;
}