/*************************************
 * problem:      id_name.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-mm-dd.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

long long phi[10000000 + 7];

void getPhi(long long *phi, long long n)
{
    if (n <= 1) {
        puts("We can\'t afford this n : n too small.\n");
    }
    phi[0] = 0;
    phi[1] = 1;
    bool *notPrime = new bool[n + 1];
    long long *p = new long long[n + 1];
    long long cntP = 0;
    memset(notPrime, 0, sizeof(bool) * (n + 1));
    for (long long i = 2; i <= n; i++) {
        if (!notPrime[i]) {
            p[++cntP] = i;
            phi[i] = i - 1;    
        }    
        for (long long j = 1; j <= cntP && p[j] * i <= n; j++) {
            notPrime[p[j] * i]=1;
            if (i % p[j]) {
                phi[p[j] * i] = phi[i] * phi[p[j]]; 
            } else {
                phi[p[j] * i] = phi[i] * p[j]; 
                break;
            }  
        }
    }
}

long long fexp(long long a, long long n, long long p)
{
    long long res(1);
    while (n) {
        if (n & 1) res = res * a % p;
        a = a * a % p;
        n >>= 1;
    }
    return res;
}

long long getAns(long long p)
{
    return p == 1 ? 0 : fexp(2, getAns(phi[p]) + phi[p], p);
}

void solveCase()
{
    long long p = read<long long>();
    write(getAns(p), 10);
}

int main()
{
    getPhi(phi, 10000000);
    long long Tcases = read<long long>();
    while (Tcases--) solveCase();
    return 0;
}
