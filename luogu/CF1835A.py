T_cases = int(input())
for _ in range(T_cases):
    A, B, C, k = map(int, input().split())
    A_l, A_r = 10 ** (A - 1), 10 ** A
    B_l, B_r = 10 ** (B - 1), 10 ** B
    C_l, C_r = 10 ** (C - 1), 10 ** C
    for i in range(A_l, A_r):
        rg_l = max(B_l, C_l - i)
        rg_r = min(B_r, C_r - i)
        if rg_l > rg_r:
            continue
        if k <= rg_r - rg_l:
            print(i, '+', rg_l + k - 1, '=', i + rg_l + k - 1)
            break
        k -= rg_r - rg_l
    else:
        print(-1)
