/*************************************
 * @problem:      [WC2019]远古计算机.
 * @author:       brealid.
 * @time:         2021-01-31.
*************************************/

#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace subtask_1 {
    void main() {
        printf("node 1\n"
               "read 0 a\n"
               "write a 0\n");
    }
}

namespace subtask_2 {
    void main() {
        printf("node 1\n"
               "read 0 a\n"
               "add a 4\n"
               "jmp a\n");
        int fib[45];
        fib[0] = 0;
        fib[1] = 1;
        for (int i = 2; i < 45; ++i) fib[i] = fib[i - 2] + fib[i - 1];
        for (int i = 0; i < 45; ++i)
            printf("write %d 0\n", fib[i]);
    }
}

namespace subtask_3 {
    int n, m;
    vector<int> G[104];
    int dis[104], from[104];
    void spfa() {
        queue<int> q;
        q.push(1);
        memset(dis, 0x3f, sizeof(dis));
        dis[1] = 0;
        while (!q.empty()) {
            int u = q.front();
            q.pop();
            for (size_t i = 0; i < G[u].size(); ++i) {
                int v = G[u][i];
                if (dis[v] > dis[u] + 1) {
                    dis[v] = dis[u] + 1;
                    q.push(v);
                    from[v] = u;
                }
            }
        }
    }
    void main() {
        cin >> n >> m;
        for (int i = 1, u, v; i <= m; ++i) {
            cin >> u >> v;
            G[u].push_back(v);
            G[v].push_back(u);
        }
        spfa();
        for (int u = n; from[u]; u = from[u]) {
            printf("node %d\n"
                   "read %d a\n"
                   "write a %d\n", from[u], from[from[u]], u);
        }
        printf("node %d\n"
               "read %d a\n"
               "write a 0\n", n, from[n]);
    }
}

namespace subtask_4 {
    int n, m;
    vector<int> G[1004];
    int dis[1004];
    vector<pair<int, int> > message[1004];
    void spfa() {
        queue<int> q;
        memset(dis, 0x3f, sizeof(dis));
        for (int i = 51; i <= 100; ++i) {
            q.push(i);
            dis[i] = 0;
        }
        while (!q.empty()) {
            int u = q.front();
            q.pop();
            for (size_t i = 0; i < G[u].size(); ++i) {
                int v = G[u][i];
                if (dis[v] > dis[u] + 1) {
                    dis[v] = dis[u] + 1;
                    q.push(v);
                }
            }
        }
    }
    void main() {
        cin >> n >> m;
        for (int i = 1, u, v; i <= m; ++i) {
            cin >> u >> v;
            G[u].push_back(v);
            G[v].push_back(u);
        }
        spfa();
        queue<int> q;
        for (int i = 1; i <= 50; ++i) {
            q.push(i);
            message[i].push_back(make_pair(0, 0));
        }
        while (!q.empty()) {
            int u = q.front(), goal = 0;
            q.pop();
            for (size_t i = 0; i < G[u].size(); ++i) {
                int v = G[u][i];
                if (dis[v] + 1 == dis[u]) goal = v;
            }
            printf("node %d\n", u);
            sort(message[u].begin(), message[u].end());
            for (size_t i = 0; i < message[u].size(); ++i) {
                int timx = message[u][i].first, from = message[u][i].second;
                printf("read %d a\n"
                        "write a %d\n", from, goal);
                message[goal].push_back(make_pair(timx + 1, u));
            }
            if (goal) q.push(goal);
        }
    }
}

namespace subtask_5 {
    int n, m;
    vector<int> G[104];
    int dis[104], from[104], used[104][104];
    struct info {
        int timx, from, to;
        info(int X, int F, int T) : timx(X), from(F), to(T) {}
        bool operator < (const info &b) const {
            return timx < b.timx;
        }
    };
    vector<info> ans[104];
    void spfa(int st, int ed) {
        queue<int> q;
        memset(dis, 0x3f, sizeof(dis));
        memset(from, 0, sizeof(from));
        q.push(st);
        dis[st] = 0;
        while (!q.empty()) {
            int u = q.front();
            q.pop();
            for (size_t i = 0; i < G[u].size(); ++i) {
                int v = G[u][i], w = dis[u] + 1;
                while (used[v][w] || used[v][w + 1]) ++w;
                if (dis[v] > w) {
                    dis[v] = w;
                    from[v] = u;
                    q.push(v);
                }
            }
        }
        for (int u = ed; from[u]; u = from[u]) {
            int midp = from[u];
            used[midp][dis[midp]] = used[midp][dis[midp] + 1] = true;
            ans[midp].push_back(info(dis[midp], from[midp], u));
        }
        used[ed][dis[ed]] = used[ed][dis[ed] + 1] = true;
        ans[ed].push_back(info(dis[ed], from[ed], 0));
    }
    void main() {
        cin >> n >> m;
        for (int i = 1, u, v; i <= m; ++i) {
            cin >> u >> v;
            G[u].push_back(v);
            G[v].push_back(u);
        }
        for (int i = 1; i <= 10; ++i) used[i][0] = used[i][1] = true;
        for (int i = 1; i <= 10; ++i) spfa(i, 101 - i);
        for (int i = 1; i <= n; ++i)
            if (!ans[i].empty()) {
                printf("node %d\n", i);
                sort(ans[i].begin(), ans[i].end());
                for (size_t j = 0; j < ans[i].size(); ++j)
                    printf("read %d a\n"
                           "write a %d\n", ans[i][j].from, ans[i][j].to);
            }
    }
}

signed main() {
    int id;
    cin >> id;
    switch (id) {
        case 1: subtask_1::main(); break;
        case 2: subtask_2::main(); break;
        case 3: subtask_3::main(); break;
        case 4: subtask_4::main(); break;
        case 5: subtask_5::main(); break;
    }
    return 0;
}