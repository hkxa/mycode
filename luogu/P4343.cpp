/*************************************
 * problem:      P4343 [SHOI2015]自动刷题机.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-06.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define XiMax 1000000007LL
#define LMax 100007LL
#define AnsMax (XiMax * LMax)

int l, k;
int x[LMax];

long long check(long long n) // 返回值 : 写的代码数
{
    long long code(0), task(0);
    for (int i = 1; i <= l; i++) {
        code += x[i];
        if (code < 0) code = 0;
        if (code >= n) {
            code = 0;
            task++;
        }
    }
    return task;
}

int main()
{
    l = read<int>();
    k = read<int>();
    for (int i = 1; i <= l; i++) {
        x[i] = read<int>();
    }
    long long L = 1, R = AnsMax << 1, mid, ans = -1;
    while (L <= R) { // get n_min
        mid = (L + R) >> 1;
        if (check(mid) <= k) {
            ans = mid;
            R = mid - 1;
        } else {
            L = mid + 1;
        }
    } 
    if (ans == -1) {
        puts("-1");
        return 0;
    }
    // if (ans == 1 && check(1) < k) {
    //     puts("-1");
    //     return 0;
    // }
    if (check(ans) < k) {
        puts("-1");
        return 0;
    }
    write(ans, 32);
    L = 1;
    R = AnsMax;
    ans = -1;
    while (L <= R) { // get n_max
        mid = (L + R) >> 1; 
        if (check(mid) >= k) {
            ans = mid;
            L = mid + 1;
        } else {
            R = mid - 1;
        }
    } 
    write(ans, 10);
    return 0;
}