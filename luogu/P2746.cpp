/*************************************
 * problem:      P2746 [USACO5.3]校园网Network of Schools.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-mm-dd.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

int n, m;
vector<int> G[10007];
typedef vector<int>::iterator Vec_it;
int dfn[10007] = {0}, low[10007] = {0}, cnt = 0;
bool ins[10007] = {0};
int s[10007] = {0}, top = 0;
int kind[10007] = {0}, kinds = 0;
int kindCnt[10007] = {0};
int in[10007] = {0}, out[10007] = {0};

void tarjan(int u)
{
    low[u] = dfn[u] = ++cnt;
    ins[u] = true;
    s[++top] = u;
    for (Vec_it it = G[u].begin(); it != G[u].end(); it++) {
        if (!dfn[*it]) {
            tarjan(*it);
            low[u] = min(low[u], low[*it]);
        } else if (ins[*it]) {
            low[u] = min(low[u], low[*it]);
        }
    }
    if (dfn[u] == low[u]) {
        kind[u] = ++kinds;
        ins[u] = 0;
        kindCnt[kinds] = 1;
        while (s[top] != u) {
            kind[s[top]] = kinds;
            ins[s[top--]] = 0;
            kindCnt[kinds]++;
        }
        top--;
    }
}

void tarjan_all()
{
    for(int i = 1; i <= n; i++) {
        if (!dfn[i]) tarjan(i);
    }
}

int main()
{
    n = read<int>();
    int v;
    for (int i = 1; i <= n; i++) {
        v = read<int>();
        while (v) {
            G[i].push_back(v);
            v = read<int>();
        }
    }
    tarjan_all();
    for (int i = 1; i <= n; i++) {
        // printf("point #%d : kind<%d>\n", i, kind[i]);
        for (Vec_it it = G[i].begin(); it != G[i].end(); it++) {
            // printf("[%d -> %d]:", i, *it);
            if (kind[i] != kind[*it]) {
                in[kind[*it]]++;
                out[kind[i]]++;
                // printf("chk edge %d --> %d\n", i, *it);
            } //else putchar(10);
        }
    }
    int inC(0), outC(0);
    for (int i = 1; i <= kinds; i++) {
        if (in[i] == 0) inC++;
        if (out[i] == 0) outC++;
    }
    // printf("kinds = %d.\n", kinds);
    write(inC, 10);
    if (kinds != 1) write(max(inC, outC), 10);
    else write(0, 10);
    return 0;
}