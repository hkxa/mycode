/*************************************
 * problem:      P4159 [SCOI2009]迷路.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-09.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int N, T;
char buf[13];

#define EMPTY_MATRIX 0
#define UNIT_MATRIX 1
struct matrix {
    int m[103][103];
    matrix(int matrixType = EMPTY_MATRIX) 
    {
        memset(m, 0, sizeof(m));
        if (matrixType == UNIT_MATRIX) for (int i = 1; i <= 100; i++) m[i][i] = 1;
    }
    matrix operator * (const matrix &Timeser)
    {
        matrix res;
        for (int i = 1; i <= N * 9; i++) {
            for (int k = 1; k <= N * 9; k++) {
                for (int j = 1; j <= N * 9; j++) {
                    res.m[i][j] += m[i][k] * Timeser.m[k][j];
                }
            }
        }
        for (int i = 1; i <= N * 9; i++) {
            for (int j = 1; j <= N * 9; j++) {
                res.m[i][j] %= 2009;
            }
        }
        return res;
    }
};

matrix fexp(matrix a, int n)
{
    matrix res(UNIT_MATRIX);
    while (n) {
        if (n & 1) res = res * a;
        a = a * a;
        n >>= 1;
    }
    return res;
}

int main()
{
    N = read<int>();
    T = read<int>();
    matrix orig, facer, ans;
    for (int i = 1; i <= N; i++) {
        scanf("%s", buf + 1);
        for (int j = 1; j <= N; j++) {
            orig.m[i][j] = buf[j] & 15;
        }
    }
    for (int i = 1; i <= N; i++) {
        for (int j = 1; j <= N; j++) {
            if (orig.m[i][j]) {
                facer.m[i * 9][j * 9 + 1 - orig.m[i][j]] = 1;
            }
        }
        for (int k = 1; k < 9; k++) {
            facer.m[i * 9 - k][i * 9 - k + 1] = 1;
        }
    }
    ans = fexp(facer, T);
    write(ans.m[9][N * 9], 10);
    return 0;
}