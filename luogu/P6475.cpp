/*************************************
 * @problem:      [NOI Online #2 入门组]建设城市.
 * @user_name:    brealid.
 * @time:         2020-11-10.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

const int N = 2e5 + 7, P = 998244353;

int64 fpow(int64 a, int n) {
    int64 ret = 1;
    while (n) {
        if (n & 1) ret = ret * a % P;
        a = a * a % P;
        n >>= 1;
    }
    return ret;
}

struct MathCombinationNumber {
    int64 fac[N], ifac[N];
    MathCombinationNumber() {
        fac[0] = ifac[0] = 1;
        for (int i = 1; i < N; ++i) fac[i] = fac[i - 1] * i % P;
        ifac[N - 1] = fpow(fac[N - 1], P - 2);
        for (int i = N - 1; i > 1; --i) ifac[i - 1] = ifac[i] * i % P;
    }
    int64 Comb(int n, int m) {
        if (n < m) return 0;
        return fac[n] * ifac[m] % P * ifac[n - m] % P;
    }
    int64 ChaBan_boxNotEmpty(int n, int m) {
        return Comb(n - 1, m - 1);
    }
    int64 ChaBan_boxCountBeEmpty(int n, int m) {
        return ChaBan_boxNotEmpty(n + m, m);
    }
    int64 Query_building(int HeightDiffLimit, int BuildingCount) {
        return ChaBan_boxCountBeEmpty(HeightDiffLimit, BuildingCount + 1);
    }
} zy_math;

int m, n, x, y;

void solve_XY_one_side(int m, int n, int x, int y) {
    int64 ans = zy_math.Query_building(m - 1, n);
    int64 cnt = 0;
    for (int XY_height = 1; XY_height <= m; ++XY_height)
        cnt += zy_math.Query_building(XY_height - 1, x - 1) *
               zy_math.Query_building(m - XY_height, n - y) % P;
    write << ans * (cnt % P) % P << '\n';
}

void solve_XY_differ_side(int m, int n, int x, int y) {
    int64 ans = 0;
    for (int XY_height = 1; XY_height <= m; ++XY_height)
        ans += zy_math.Query_building(XY_height - 1, x - 1) *
               zy_math.Query_building(XY_height - 1, 2 * n - y) % P *
               zy_math.Query_building(m - XY_height, n - x) % P *
               zy_math.Query_building(m - XY_height, y - n - 1) % P;
    write << ans % P << '\n';
}

signed main() {
    read >> m >> n >> x >> y;
    if (x > n) solve_XY_one_side(m, n, 2 * n - y + 1, 2 * n - x + 1);
    else if (y <= n) solve_XY_one_side(m, n, x, y);
    else solve_XY_differ_side(m, n, x, y);
    return 0;
}