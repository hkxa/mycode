//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Jury Meeting.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-10.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

namespace against_cpp11 {
    const int N = 1e6 + 7;
    const int64 inf = 1e14;
    int n, m, k;
    vector<pair<int, int> > go[N], back[N];
    int flight[N], cnt;
    int64 go_mincost[N], back_mincost[N];
    int64 tot, ans = inf;
    signed main() {
        read >> n >> m >> k;
        for (int i = 1, day, a, b, c; i <= m; i++) {
            read >> day >> a >> b >> c;
            if (!b) go[day].push_back(make_pair(a, c));
            else back[day].push_back(make_pair(b, c));
        }
        go_mincost[0] = inf;
        memset(flight, -1, sizeof(flight));
        for (int i = 1; i < N; i++) {
            go_mincost[i] = go_mincost[i - 1];
            for (size_t j = 0; j < go[i].size(); j++) {
                int u = go[i][j].first, c = go[i][j].second;
                if (!~flight[u]) {
                    cnt++;
                    tot += c;
                    flight[u] = c;
                } else if (c < flight[u]) {
                    tot += c - flight[u];
                    flight[u] = c;
                }
            }
            if (cnt == n) go_mincost[i] = min(go_mincost[i], tot);
            // if (i < 15) printf("go_mincost[%d] = %lld\n", i, go_mincost[i]);
        }
        back_mincost[N - 1] = inf;
        memset(flight, -1, sizeof(flight));
        cnt = tot = 0;
        for (int i = N - 2; i >= k; i--) {
            back_mincost[i] = back_mincost[i + 1];
            for (size_t j = 0; j < back[i].size(); j++) {
                int u = back[i][j].first, c = back[i][j].second;
                if (!~flight[u]) {
                    cnt++;
                    tot += c;
                    flight[u] = c;
                } else if (c < flight[u]) {
                    tot += c - flight[u];
                    flight[u] = c;
                }
            }
            if (cnt == n) back_mincost[i] = min(back_mincost[i], tot);
            // // if (i < 15) printf("back_mincost[%d] = %lld\n", i, back_mincost[i]);
        }
        for (int i = 1; i < N - k - 1; i++)
            ans = min(ans, go_mincost[i] + back_mincost[i + k + 1]);
        if (ans == inf) puts("-1");
        else write << ans << '\n';
        return 0;
    }
}

signed main() { return against_cpp11::main(); }