/*************************************
 * problem:      P1189 `SEARCH`.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-07-25.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

struct TownTool {
    int R, C;
    char final[53][53];
    char statu[53][53];

    TownTool()
    {
        memset(final, 0, sizeof(final));
    }
    
    void readln()
    {
        R = read<int>();
        C = read<int>();
        for (int i = 1; i <= R; i++) {
            scanf("%s", statu[i] + 1);
        }
    }

    void copy()
    {
        for (int i = 1; i <= R; i++) {
            for (int j = 1; j <= C; j++) {
                statu[i][j] = final[i][j];
            }
        }
        memset(final, 0, sizeof(final));
    }

#   define lineInRange(pos, range) (pos >= 1 && pos <= range)
#   define inRange(X, Y) (lineInRange(X, R) && lineInRange(Y, C))
    void trans(int deltaX, int deltaY)
    {
        for (int i = 1; i <= R; i++) {
            for (int j = 1; j <= C; j++) {
                if (statu[i][j] != '*') continue;
                statu[i][j] = '.';
                int x = i + deltaX, y = j + deltaY;
                while (inRange(x, y) && statu[x][y] != 'X') {
                    final[x][y] = '*';
                    x += deltaX;
                    y += deltaY;
                }
            }
        }
        for (int i = 1; i <= R; i++) {
            for (int j = 1; j <= C; j++) {
                if (!final[i][j]) final[i][j] = statu[i][j];
            }
        }
    }

    void deal(string operation)
    {
        if (operation == "NORTH") trans(-1, 0);
        else if (operation == "SOUTH") trans(1, 0);
        else if (operation == "WEST") trans(0, -1);
        else trans(0, 1);
        copy();
    }

    void outputTownMap()
    {
        for (int i = 1; i <= R; i++) {
            printf("%s\n", statu[i] + 1);
        }
    }
} town;

int main()
{
    town.readln();
    int n = read<int>();
    string op;
    while (n--) {
        cin >> op;
        town.deal(op);
    }
    town.outputTownMap();
    return 0;
}