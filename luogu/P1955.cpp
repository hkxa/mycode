/*************************************
 * @problem:      P1955 [NOI2015]程序自动分析.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-01-21.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define P 20170933
#define N 2000000

int n;
int head[P + 3], num[N + 3], nxt[N + 3], cnt = 0;
int fa[N + 3];
int u[N + 3], v[N + 3], chk = 0;

int HashPush(int a)
{
    num[++cnt] = a;
    int pos = a % P;
    nxt[cnt] = head[pos];
    head[pos] = cnt;
    fa[cnt] = cnt;
    return cnt;
}

int HashGet(int a)
{
    for (int e = head[a % P]; e; e = nxt[e]) {
        if (num[e] == a) return e;
    }
    return HashPush(a);
}

int find(int a)
{
    return a == fa[a] ? a : fa[a] = find(fa[a]);
}

int T = -1;
int main()
{
    if (T == -1) T = read<int>();
    if (T <= 0) return 0;
    T--;
    // ======================== memset
    cnt = chk = 0;
    memset(head, 0, sizeof(head));
    // ======================== start algorithm
    n = read<int>();
    // printf("n = %d.\n", n);
    for (int i, j, e, k = 1; k <= n; k++) {
        i = HashGet(read<int>());
        j = HashGet(read<int>());
        e = read<int>();
        if (e == 1 && find(i) != find(j)) fa[find(i)] = find(j);
        else if (e == 0) {
            chk++;
            u[chk] = i;
            v[chk] = j;
        }
    }
    for (int i = 1; i <= chk; i++) {
        if (find(u[i]) == find(v[i])) {
            puts("NO");
            return main();
        }
    }
    puts("YES");
    // ======================== end algorithm
    return main();
}