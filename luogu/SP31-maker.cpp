#include <bits/stdc++.h>
using namespace std;
#define random(x) (rand() % (x) + 1)
#define randLR(l, r) (random((r) - (l) + 1) + (l) - 1)

int main() 
{
    freopen("SP31.in", "w", stdout);
    srand(time(0) * clock());
    int n = 10000;
    printf("2\n");
    for (int _count = 1; _count <= 2; _count++) {
        printf("%d", randLR(1, 9));
        for (int i = 1; i < n; i++) {
            printf("%d", randLR(0, 9));
        }
        printf(" ");
        printf("%d", randLR(1, 9));
        for (int i = 1; i < n; i++) {
            printf("%d", randLR(0, 9));
        }
        printf("\n");
    }
    return 0;
}