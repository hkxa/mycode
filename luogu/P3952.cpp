/*************************************
 * problem:      P3952 时间复杂度.
 * user ID:      85848.
 * user name:    hkxadpall.
 * time:         2019-09-11.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
char buf[1 << 6];

struct line {
    enum LineType { F = 1, E = 2 } lt;
    char i;
    struct range {
        enum RangeType { NUM = 3, N = 4 } rt;
        int num;
    } s, e;
};

int toNum(const char *nm)
{
    int res(0), pos(0);
    while (nm[pos]) res = ((res + (res << 2)) << 1) + (nm[pos++] & 15);
    return res;
}

line readLine()
{
    line res;
    scanf("%s", buf);
    if (buf[0] == 'F') {
        res.lt = line::F;
        scanf("%s", buf);
        res.i = buf[0];
        scanf("%s", buf);
        if (buf[0] == 'n') res.s.rt = line::range::N;
        else {
            res.s.rt = line::range::NUM;
            res.s.num = toNum(buf);
        }
        scanf("%s", buf);
        if (buf[0] == 'n') res.e.rt = line::range::N;
        else {
            res.e.rt = line::range::NUM;
            res.e.num = toNum(buf);
        }
    } else {
        res.lt = line::E;
    }
    return res;
}

class AplusplusDealer {
  private:
    bool programErr;
    int l, w; // l = 代码行数; w = 时间复杂度
    int mx, my; // mx = 自己计算的时间复杂度, my = 当前循环的时间复杂度
    int pos;
    line code[107];
    bool have[107];
    int wontDo;
    stack<line> s;

  public:
    void clear()
    {
        while (!s.empty()) s.pop();
        memset(have, false, sizeof(have));
        programErr = false;
        l = w = 0;
        mx = my = 0;
        pos = 0;
        wontDo = 0;
    }

    void readln()
    {
        scanf("%d%s", &l, buf);
        if (buf[2] == 'n') {
            buf[strlen(buf) - 1] = '\0';
            w = toNum(buf + 4);
        } else w = 0;
        for (int i = 0; i < l; i++) {
            code[i] = readLine();
        }
    }

    void work()
    {
        if (l & 1) {
            programErr = 1;
            return;
        }
        while (pos < l) {
            line &now = code[pos];
            if (now.lt == line::F) {
                if (have[now.i - 'a']) {
                    programErr = 1;
                    return;
                }
                have[now.i - 'a'] = true;
                if (now.s.rt == line::range::NUM && now.e.rt == line::range::N) {
                    if (!wontDo) my++;
                    if (my > mx) mx = my;
                } else if ((now.s.rt == line::range::N && now.e.rt == line::range::NUM) ||
                           (now.s.rt == line::range::NUM && now.e.rt == line::range::NUM && now.s.num > now.e.num)) {
                    wontDo++;
                }
                s.push(now);
            } else {
                if (s.empty()) {
                    programErr = 1;
                    return;
                }
                have[s.top().i - 'a'] = 0;
                if ((s.top().s.rt == line::range::N && s.top().e.rt == line::range::NUM) ||
                    (s.top().s.rt == line::range::NUM && s.top().e.rt == line::range::NUM && s.top().s.num > s.top().e.num)) {
                    wontDo--;
                }
                if (s.top().s.rt == line::range::NUM && s.top().e.rt == line::range::N && !wontDo) my--;
                s.pop();
            }
            // printf("line %d : my = %d, mx = %d.\n", pos + 1, my, mx);
            pos++;
        }
    }

    void output()
    {
        if (programErr || !s.empty()) {
            puts("ERR");
        } else if (mx == w) {
            puts("Yes");
        } else {
            puts("No");
        }
    }
};

int main()
{
    int T;
    scanf("%d", &T);
    AplusplusDealer appd;
    while (T--) {
        appd.clear();
        appd.readln();
        appd.work();
        appd.output();
    }
    return 0;
}

/*
2
6 O(n^1)
F a 1 n
F b 78 69
F c 9 n
E
E
E
6 O(n^1)
F a 1 n
F b n 2
F a 1 1
E
E
E
*/