/*************************************
 * problem:      P3373 【模板】线段树 2. 
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-11-07.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, m, P;
int op, x, y; int64 k;
int64 a[100007];

struct data {
    int size;
    int64 val, add, mul;
    data() : size(0), val(0), add(0), mul(1) {}
} t[100007 << 4];  

inline void pushdown(int p)
{
    t[p << 1].val = (t[p].mul * t[p << 1].val + t[p].add * t[p << 1].size) % P;
    t[p << 1].mul = t[p].mul * t[p << 1].mul % P;
    t[p << 1].add = (t[p].mul * t[p << 1].add + t[p].add) % P;
    t[p << 1 | 1].val = (t[p].mul * t[p << 1 | 1].val + t[p].add * t[p << 1 | 1].size) % P;
    t[p << 1 | 1].mul = t[p].mul * t[p << 1 | 1].mul % P;
    t[p << 1 | 1].add = (t[p].mul * t[p << 1 | 1].add + t[p].add) % P;
    t[p].mul = 1;
    t[p].add = 0;
}

inline void pushup(int p)
{
    t[p].val = t[p << 1].val + t[p << 1 | 1].val;
    t[p].mul = 1;
    t[p].add = 0;
}

inline void build(int p, int l, int r)
{
    // printf("build point %d seq [%d, %d]\n", p, l, r);
    if (l > r) return;
    if (l == r) {
        t[p].val = a[l];
        t[p].size = 1;
        return;
    }
    int mid = (l + r) >> 1;
    build(p << 1, l, mid);
    build(p << 1 | 1, mid + 1, r);
    t[p].size = t[p << 1].size + t[p << 1 | 1].size;
    t[p].val = t[p << 1].val + t[p << 1 | 1].val;
}

inline void modify_mul(int p, int l, int r, int ml, int mr, int64 k)
{
    // printf("modify_mul point %d seq [%d, %d]\tgoal_seq [%d, %d] diff %lld\n", p, l, r, ml, mr, k);
    // printf("node info {size = %d, val = %lld, add = %lld, mul = %lld}\n", t[p].size, t[p].val, t[p].add, t[p].mul);
    if (l > mr || r < ml) return;
    if (l >= ml && r <= mr) {
        t[p].val = t[p].val * k % P;
        t[p].mul = t[p].mul * k % P;
        t[p].add = t[p].add * k % P;
        return;
    }
    int mid = (l + r) >> 1;
    pushdown(p);
    modify_mul(p << 1, l, mid, ml, mr, k);
    modify_mul(p << 1 | 1, mid + 1, r, ml, mr, k);
    pushup(p);
}

inline void modify_add(int p, int l, int r, int ml, int mr, int64 k)
{
    // printf("modify_add point %d seq [%d, %d]\tgoal_seq [%d, %d] diff %lld\n", p, l, r, ml, mr, k);
    // printf("node info {size = %d, val = %lld, add = %lld, mul = %lld}\n", t[p].size, t[p].val, t[p].add, t[p].mul);
    if (l > mr || r < ml) return;
    if (l >= ml && r <= mr) {
        t[p].val = (t[p].val + k * t[p].size) % P;
        t[p].add = (t[p].add + k) % P;
        return;
    }
    int mid = (l + r) >> 1;
    pushdown(p);
    modify_add(p << 1, l, mid, ml, mr, k);
    modify_add(p << 1 | 1, mid + 1, r, ml, mr, k);
    pushup(p);
}

inline int64 query(int p, int l, int r, int ml, int mr)
{
    // printf("query point %d seq [%d, %d]\tgoal_seq [%d, %d]\n", p, l, r, ml, mr);
    // printf("node info {size = %d, val = %lld, add = %lld, mul = %lld}\n", t[p].size, t[p].val, t[p].add, t[p].mul);
    if (l > mr || r < ml) return 0;
    // if (l >= ml && r <= mr) printf("matched! val = %lld\n", t[p].val);
    if (l >= ml && r <= mr) return t[p].val;
    int mid = (l + r) >> 1;
    pushdown(p);
    return (query(p << 1, l, mid, ml, mr) + query(p << 1 | 1, mid + 1, r, ml, mr)) % P;
}

int main()
{
    n = read<int>();
    m = read<int>();
    P = read<int>();
    // write(P, 10);
    for (int i = 1; i <= n; i++) a[i] = read<int>();
    build(1, 1, n);
    while (m--) {
        op = read<int>();
        x = read<int>();
        y = read<int>();
        if (op == 1) {
            k = read<int>();
            modify_mul(1, 1, n, x, y, k);
            // printf("DEBUG : ");
            // for (int i = 1; i <= n; i++) write(query(1, 1, n, i, i), i == n ? 10 : 32);
        } else if (op == 2) {
            k = read<int>();
            modify_add(1, 1, n, x, y, k);
            // printf("DEBUG : ");
            // for (int i = 1; i <= n; i++) write(query(1, 1, n, i, i), i == n ? 10 : 32);
        } else {
            write(query(1, 1, n, x, y), 10);
        }
    }
    return 0;
}