/*************************************
 * @problem:      id_name.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-mm-dd.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int N = 100000 + 7, M = 200000 + 7, T = 1000 + 7;
 
int n, m, A, B, C;
inline int f(int x) {
    return A * x * x + B * x + C;
}

struct bus {
    int y, p, q;
    bus(int Y = 0, int P = 0, int Q = 0) : y(Y), p(P), q(Q) {}
    bool operator < (const bus &other) const {
        return p > other.p;
    }
};
vector<bus> G[N];
int S[N][T];
void dfs(int tim, int u, int anger) {
    if (S[u][tim] <= anger) return;
    S[u][tim] = anger;
    // printf("S[%d][%d] = %d\n", u, tim, anger);
    for (register unsigned i = 0; i < G[u].size(); i++) {
        if (G[u][i].p >= tim) dfs(G[u][i].q, G[u][i].y, anger + f(G[u][i].p - tim));
        else return;
    }
}

int main()
{
    n = read<int>();
    m = read<int>();
    A = read<int>();
    B = read<int>();
    C = read<int>();
    for (register int i = 1, x, y, p, q; i <= m; i++) {
        x = read<int>();
        y = read<int>();
        p = read<int>();
        q = read<int>();
        G[x].push_back(bus(y, p, q));
    }
    for (register int i = 1; i <= n; i++) sort(G[i].begin(), G[i].end());
    memset(S, 0x7f, sizeof(S));
    dfs(0, 1, 0);
    register int ans = 0x7fffffff;
    for (register int i = 1; i <= 1000; i++) {
        ans = min(ans, i + S[n][i]);
    }
    write(ans, 10);
    return 0;
}