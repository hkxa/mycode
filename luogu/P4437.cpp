/*************************************
 * @problem:      [HNOI/AHOI2018]排列.
 * @user_name:    brealid/hkxadpall/zhaoyi20/j(o)moo/littleTortoise.
 * @time:         2020-08-30.
 * @language:     C++.
 * @fastio_ver:   20200827.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        // simple io flags
        ignore_int = 1 << 0,    // input
        char_enter = 1 << 1,    // output
        flush_stdout = 1 << 2,  // output
        flush_stderr = 1 << 3,  // output
        // combination io flags
        endline = char_enter | flush_stdout // output
    };
    enum number_type_flags {
        // simple io flags
        output_double_stable = 1 << 0,  // output
        output_double_faster = 1 << 1   // output
        // combination io flags
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f & ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore character set : ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (double &lf) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            lf = endch & 15;
            while (isdigit(endch = getchar())) lf = lf * 10 + (endch & 15);
            if (endch == '.') {
                double digit = 0.1;
                while (isdigit(endch = getchar())) {
                    lf += (endch & 15) * digit;
                    digit *= 0.1;
                }
            }
            if (flag) lf = -lf;
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            Int d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return d;
        }
    } read;

    struct Writer {
        Writer& operator << (io_flags f) {
            if (f & char_enter) putchar(10);
            if (f & flush_stdout) fflush(stdout);
            if (f & flush_stderr) fflush(stderr);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
        inline void operator () (double val, int eps_digit = 6, char endch = '\0', number_type_flags flg = output_double_faster) {
            if (flg & output_double_stable) {
                static char output_format[10];
                sprintf(output_format, "%%.%dlf", eps_digit);
                printf(output_format, val);
            } else if (flg & output_double_faster) {
                if (val < 0) {
                    putchar('-');
                    val = -val;
                }
                double eps_number = 0.5;
                for (int i = 1; i <= eps_digit; i++) eps_number /= 10;
                val += eps_number;
                (*this) << (int64)val;
                val -= (int64)val;
                putchar('.');
                while (eps_digit--) {
                    val *= 10;
                    putchar((int)val | '0');
                    val -= (int)val;
                }
            } else {
                (*this) << "1.#ERROR_NOT_IDENTIFIED_FLAG";
            }
            if (endch) putchar(endch);
        }
    } write;
}
using namespace Fastio;
namespace File_IO {
    void init_IO() {
        freopen("[HNOI/AHOI2018]排列.in", "r", stdin);
        freopen("[HNOI/AHOI2018]排列.out", "w", stdout);
    }
}

// #define int int64

template <typename T, typename Container = vector<T>, typename Comparer = less<T> >
class heap {
  private:
    priority_queue<T, Container, Comparer> que, del; 
    inline void push_del_tags() {
        while (!del.empty() && !que.empty() && que.top() == del.top())
            del.pop(), que.pop();
    }
  public:
    inline size_t size() {
        return que.size() - del.size();
    }
    inline bool empty() {
        return que.size() == del.size();
    }
    inline T top() {
        push_del_tags();
        return que.top();
    }
    inline T top_pop() {
        push_del_tags();
        T x = que.top();
        que.pop();
        return x;
    }
    inline void pop() {
        push_del_tags();
        que.pop();
    }
    inline void insert(T x) { que.push(x); }
    inline void erase(T x) { del.push(x); }
    inline void swap(heap &x) {
        que.swap(x.que);
        del.swap(x.del);
    }
};

const int N = 5e5 + 7;

struct status {
    int id;
    int64 w;
    int64 cnt;
    status() {}
    status(int ID, int64 W, int64 CNT) : id(ID), w(W), cnt(CNT) {}
    bool operator < (const status &b) const {
        if (w * b.cnt != b.w * cnt) return w * b.cnt > b.w * cnt;
        if (id != b.id) return id < b.id;
        return w < b.w;
    }
    bool operator == (const status &b) const {
        return id == b.id && w == b.w && cnt == b.cnt;
    }
};

int n;
int a[N];
int64 w[N], cnt[N], preCalcAns[N];
int dead[N];
heap<status> hp;
int64 ans;

bool found_circle(int u) {
    static int8 visiting_status[N];
    visiting_status[u] = 1;
    bool bRet;
    if (!a[u]) bRet = false;
    else if (visiting_status[a[u]] == 2) bRet = false;
    else if (visiting_status[a[u]] == 1) bRet = true;
    else bRet = found_circle(a[u]);
    visiting_status[u] = 2;
    return bRet; 
}

int find(int u) {
    if (!a[u] || dead[a[u]] == 1) return a[u] = 0;
    if (!dead[a[u]]) return a[u];
    return a[u] = find(a[u]);
}

signed main() {
    read >> n;
    for (int i = 1; i <= n; i++) read >> a[i];
    for (int i = 1; i <= n; i++) read >> w[i];
    for (int i = 1; i <= n; i++) 
        if (found_circle(i)) {
            puts("-1");
            return 0;
        }
    for (int i = 1; i <= n; i++) {
        hp.insert(status(i, w[i], cnt[i] = 1));
    }
    int timx = 1;
    while (!hp.empty()) {
        int u = hp.top().id; hp.pop();
        int fa = find(u);
        // printf("node %d (fa = %d) w = %lld, cnt = %lld (ans = %lld) [%lld]\n", u, fa, w[u], cnt[u], ans, preCalcAns[u]);
        if (!fa) {
            dead[u] = 1;
            ans += w[u] * timx + preCalcAns[u];
            timx += cnt[u];
            continue;
        }
        // if (dead[u]) fprintf(stderr, "ERROR: dead[u] = true [u = %d, fa = %d]\n", u, fa);
        // if (dead[fa]) fprintf(stderr, "ERROR: dead[fa] = true [u = %d, fa = %d]\n", u, fa);
        hp.erase(status(fa, w[fa], cnt[fa]));
        preCalcAns[fa] += cnt[fa] * w[u] + preCalcAns[u];
        cnt[fa] += cnt[u];
        w[fa] += w[u];
        hp.insert(status(fa, w[fa], cnt[fa]));
        dead[u] = 2;
    }
    write << ans << '\n';
    return 0;
}