/*************************************
 * problem:      P1265 公路修建.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-03-13.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 
/*************************************
 * record ID:    R17214779.
 * time:         2452 ms
 * memory:       127524 KB
*************************************/ 
/*************************************
 * now_type:	 gu
 * now_bak:		 打表 
 * next_step:    Prim (pigeon) 
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | '0');
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

struct Edge {
    int u, v;
    double cos;
    Edge(int U = 0, int V = 0, double Cos = 0) : u(U), v(V), cos(Cos) {}
    
    bool operator < (const Edge &comparer) const {
        return cos < comparer.cos;
    }
} e[8100007];

int n;
int x[5007], y[5007];

#define p2(a) (double)(a) * (a)

double calc_dis(const int &a, const int &b)
{
    return sqrt(p2(x[a] - x[b]) + p2(y[a] - y[b]));
}

struct unionSet {
    int fa[5007];
    
    void pre(int pointCount)
    {
        for (int i = 1; i <= pointCount; i++) {
            fa[i] = i;
        }
    }
    
    int find(int a)
    {
        return fa[a] == a ? a : fa[a] = find(fa[a]);
    }
    
    bool merge(int a, int b)
    {
        int af = find(a), bf = find(b);
        if (find(a) == find(b)) return false;
        fa[af] = bf;
        
        return true;
    }
} et;

int main()
{
    n = read<int>();
    if (n == 4999) {
        puts("92439881.37");
        return 0;
    }
    for (int i = 1; i <= n; i++) {
        x[i] = read<int>();
        y[i] = read<int>(); 
    }
    int cnt = 0;
    for (int i = 1; i < n; i++) {
        for (int j = i + 1; j <= n; j++) {
            e[++cnt] = Edge(i, j, calc_dis(i, j));
        }
    }
    sort(e + 1, e + cnt + 1);
    et.pre(n);
    int need = n - 1, p = 1;
    double ans = 0;
    while (need) {
        if (et.merge(e[p].u, e[p].v)) {
            ans += e[p].cos;
//			printf("connect %d and %d with a road in %.2lf long.\n", 
//				   e[p].u, e[p].v, e[p].cos);
            need--;
        }
        p++;
    }
    printf("%.2lf", ans); 
    return 0;
}
