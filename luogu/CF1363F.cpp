//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
#pragma GCC optimize(2)
/*************************************
 * @problem:      CF1363F Rotating Substrings.
 * @user_id:      63720.
 * @user_name:    hkxadpall.
 * @time:         2020-06-01.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define int int64
const int N = 2000 + 7, LETTERS = 26 + 3;

int n;
int DpCache[N][N];
int sufs[N][LETTERS], suft[N][LETTERS];
char s[N], t[N];

int dp(int i, int j) {
    // printf("dp(%d, %d)\n", i, j);
    if (!j) return 0;
    int &ans = DpCache[i][j];
    if (~ans) return ans;
    ans = 2e9;
    if (i) {
		ans = dp(i - 1, j) + 1;
		if (s[i] == t[j])
			ans = min(ans, dp(i - 1, j - 1));
	}
	int ch = t[j] & 31;
	if (sufs[i + 1][ch] > suft[j + 1][ch])
		ans = min(ans, dp(i, j - 1));
    return ans;
}

int32 main() {
    int T = read<int>();
    while (T--) {
        n = read<int>();
        scanf("%s%s", s + 1, t + 1);
        memset(sufs[n + 1], 0, sizeof(sufs[n + 1]));
        memset(suft[n + 1], 0, sizeof(suft[n + 1]));
        for (int i = 0; i <= n; i++)
            for (int j = 0; j <= n; j++)
                DpCache[i][j] = -1;
        for (int i = n; i >= 1; i--) {
            memcpy(sufs[i], sufs[i + 1], sizeof(sufs[i]));
            memcpy(suft[i], suft[i + 1], sizeof(suft[i]));
            sufs[i][s[i] & 31]++;
            suft[i][t[i] & 31]++;
            // printf("sufs[%d][%d] = %d\n", i, s[i] & 31, sufs[i][s[i] & 31]);
            // printf("suft[%d][%d] = %d\n", i, t[i] & 31, suft[i][t[i] & 31]);
        }
        int ans = dp(n, n);
        // for (int i = 0; i <= n; i++)
        //     for (int j = 0; j <= n; j++)
        //         printf("%d%c", DpCache[i][j], " \n"[j == n]);
        if (ans >= 1e9) puts("-1");
        else write(ans, 10);
    }
    return 0;
}

// Create File Date : 2020-06-01

/*
2
10
qwertuiopy qywertuiop
10
weturiyopq qywertuiop
*/