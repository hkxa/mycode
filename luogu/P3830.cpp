/*************************************
 * problem:      P3830.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-07-18.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

double solve_treeLeavesDepthAverage(int n)
{
    double f[107];
    f[1] = 0; //根节点深度定义为 0
    for (int i = 2; i <= n; i++) {
        f[i] = f[i - 1] + 2.0 / i;
        // 增加的深度   = 增加二个节点的深度和 - 原节点深度
        // depthAdd[i] = (f[i - 1] + 1) * 2 - f[i - 1]
        //             = f[i - 1] + 2

        // 答案 = (     原深度总和     +   增加的深度 ) / 节点个数
        // f[i] = (f[i - 1] * (i - 1) + f[i - 1] + 2) / i
        //      = (f[i - 1] * i + 2) / i
        //      = f[i - 1] + 2 / i;
    }
    return f[n];
}

double solve_treeTotalDepthAverage(int n)
{
    double f[107][107] = {0}, ans = 0;
    for (int i = 1; i <= n; i++) {
        f[i][0] = 1;
    }
    for (int i = 2; i <= n; i++) {
        for (int j = 1; j < i; j++) {
            for (int k = 1; k < i; k++) {
                f[i][j] += f[k][j - 1] + f[i - k][j - 1] - f[k][j - 1] * f[i - k][j - 1];
            }
            f[i][j] /= i - 1;
        }
    }
    for (int i = 1; i <= n; i++) {
        ans += f[n][i];
    }
    return ans;
}

#define Case1 solve_treeLeavesDepthAverage
#define Case2 solve_treeTotalDepthAverage
typedef double func_lf_i(int);

func_lf_i *solve[3] = {NULL, Case1, Case2};

int main()
{
    int q, n;
    q = read<int>();
    n = read<int>();
    printf("%.6lf", solve[q](n));
    return 0;
}