/*************************************
 * @problem:      Xenia and Tree.
 * @author:       brealid.
 * @time:         2021-01-16.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 1e5 + 7;

int n, q;
vector<int> G[N];
int fa[N];

// Tree-Chain Split
namespace tcs {
    int siz[N], dep[N], wson[N], beg[N];
    void InitDfs1(int u, int father) {
        fa[u] = father;
        dep[u] = dep[father] + 1;
        siz[u] = 1;
        for (size_t i = 0; i < G[u].size(); ++i) {
            int v = G[u][i];
            if (v == father) continue;
            InitDfs1(v, u);
            siz[u] += siz[v];
            if (siz[v] > siz[wson[u]]) wson[u] = v;
        }
    }
    void InitDfs2(int u, int chain_begin) {
        beg[u] = chain_begin;
        if (!wson[u]) return;
        InitDfs2(wson[u], chain_begin);
        for (size_t i = 0; i < G[u].size(); ++i) {
            int v = G[u][i];
            if (v == fa[u] || v == wson[u]) continue;
            InitDfs2(v, v);
        }
    }
    int GetLCA(int u, int v) {
        while (beg[u] != beg[v])
            if (dep[beg[u]] < dep[beg[v]]) v = fa[beg[v]];
            else u = fa[beg[u]];
        return dep[u] < dep[v] ? u : v;
    }
    int GetDist(int u, int v) {
        return dep[u] + dep[v] - (dep[GetLCA(u, v)] << 1);
    }
}

// Point-Divide Tree
namespace pdt {
    int fa[N];      // Node's father in the Point-Divide Tree
    int nearest[N]; // Nearest Red-Node in the subtree of the Point-Divide Tree
    bool vis[N];
    int siz[N];
    void InitSize(int u) {
        vis[u] = true;
        siz[u] = 1;
        for (size_t i = 0; i < G[u].size(); ++i) {
            int v = G[u][i];
            if (vis[v]) continue;
            InitSize(v);
            siz[u] += siz[v];
        }
        vis[u] = false;
    }
    pair<int, int> GetWeightHeart(int u, int siz_all) {
        pair<int, int> ret(0x3f3f3f3f, 0), self(siz_all - siz[u], u);
        vis[u] = true;
        for (size_t i = 0; i < G[u].size(); ++i) {
            int v = G[u][i];
            if (vis[v]) continue;
            self.first = max(self.first, siz[v]);
            ret = min(ret, GetWeightHeart(v, siz_all));
        }
        vis[u] = false;
        return min(ret, self);
    }
    void InitPDT(int u) {
        vis[u] = true;
        for (size_t i = 0; i < G[u].size(); ++i) {
            int v = G[u][i];
            if (vis[v]) continue;
            InitSize(v);
            int root = GetWeightHeart(v, siz[v]).second;
            fa[root] = u;
            InitPDT(root);
        }
        // vis[u] = false;
    }
    void Init() {
        memset(nearest, 0x3f, sizeof(nearest));
        InitSize(1);
        int root = GetWeightHeart(1, n).second;
        InitPDT(root);
    }
    void MarkRed(int u) {
        for (int now = u; now; now = fa[now])
            nearest[now] = min(nearest[now], tcs::GetDist(u, now));
    }
    int QueryMin(int u) {
        int res = 0x3f3f3f3f;
        for (int now = u; now; now = fa[now])
            res = min(res, nearest[now] + tcs::GetDist(u, now));
        return res;
    }
}

signed main() {
    kin >> n >> q;
    for (int i = 1, u, v; i < n; ++i) {
        kin >> u >> v;
        G[u].push_back(v);
        G[v].push_back(u);
    }
    tcs::InitDfs1(1, 0);
    tcs::InitDfs2(1, 1);
    pdt::Init();
    pdt::MarkRed(1);
    for (int i = 1, t, v; i <= q; ++i) {
        kin >> t >> v;
        if (t == 1) pdt::MarkRed(v);
        else kout << pdt::QueryMin(v) << '\n';
    }
    return 0;
}
