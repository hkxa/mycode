/*************************************
 * @problem:      电视网络 Cable TV Network.
 * @author:       brealid.
 * @time:         2021-02-02.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace fastio {
    namespace base {
        static const int BufferLen = 1e6;
        struct Getchar {
            char buf[BufferLen], *p1, *p2;
            Getchar() : p1(buf), p2(buf) {}
            char predict() {
                if (p1 == p2) p2 = buf + fread(p1 = buf, 1, BufferLen, stdin);
                return p1 == p2 ? EOF : *p1;
            }
            char operator() () {
                if (p1 == p2) p2 = buf + fread(p1 = buf, 1, BufferLen, stdin);
                return p1 == p2 ? EOF : *p1++;
            }
        } getchar;
        struct Putchar {
            char buf[BufferLen], *p1, *p2;
            Putchar() : p1(buf), p2(buf + BufferLen) {}
            ~Putchar() { fwrite(buf, 1, p1 - buf, stdout); }
            void flush() { fwrite(buf, 1, p1 - buf, stdout); p1 = buf; }
            void operator() (char ch) {
                if (p1 == p2) fwrite(p1 = buf, 1, BufferLen, stdout);
                *p1++ = ch;
            }
        } putchar;
        template<typename I> inline void get_int(I &x) { 
            static char ch = 0;
            bool negative = false;
            while (!isdigit(ch = getchar()) && ch != '-' && ch != EOF);
            if (ch == '-') {
                negative = true;
                x = getchar() & 15;
            } else x = ch & 15;
            while (isdigit(ch = getchar())) x = (((x << 2) + x) << 1) + (ch & 15);
            if (negative) x = -x;
        }
        template<typename I> inline void get_uint(I &x) { 
            static char ch = 0;
            while (!isdigit(ch = getchar()) && ch != EOF);
            x = ch & 15;
            while (isdigit(ch = getchar())) x = (((x << 2) + x) << 1) + (ch & 15);
        }
        inline void get_str(char *str) { 
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            if (*str != EOF)
                while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
        }
        inline void get_cpp_str(std::string &str) { 
            str.clear();
            char cc;
            while (((cc = getchar()) == ' ' || cc == '\n' || cc == '\r' || cc == '\t') && cc != EOF);
            while (cc != ' ' && cc != '\n' && cc != '\r' && cc != '\t' && cc != EOF) {
                str.push_back(cc);
                cc = getchar();
            }
        }
        inline void get_ch(char &ch) { 
            while ((ch = getchar()) == ' ' || ch == '\n' || ch == '\r' || ch == '\t');
        }
        template<typename I> inline void attach_int(I x) { 
            static char buf[23];
            static int top = 0;
            if (x < 0) putchar('-'), x = -x;
            do {
                buf[++top] = '0' | (x % 10);
                x /= 10;
            } while (x);
            while (top) putchar(buf[top--]);
        }
        template<typename I> inline void attach_uint(I x) { 
            static char buf[23];
            static int top = 0;
            do {
                buf[++top] = '0' | (x % 10);
                x /= 10;
            } while (x);
            while (top) putchar(buf[top--]);
        }
        inline void attach_str(const char *str) { 
            while (*str) putchar(*str++);
        }
    }
    struct InputStream {
        InputStream& operator >> (int &x) { base::get_int(x); return *this; }
        InputStream& operator >> (long long &x) { base::get_int(x); return *this; }
        InputStream& operator >> (unsigned &x) { base::get_uint(x); return *this; }
        InputStream& operator >> (unsigned long long &x) { base::get_uint(x); return *this; }
        InputStream& operator >> (char &x) { base::get_ch(x); return *this; }
        InputStream& operator >> (char *x) { base::get_str(x); return *this; }
        InputStream& operator >> (std::string &x) { base::get_cpp_str(x); return *this; }
        bool eof() const { return base::getchar.predict() == EOF; }
        char predict() const { return base::getchar.predict(); }
        operator bool() const { return !eof(); }
    };
    struct OutputStream {
        OutputStream& operator << (const int &x) { base::attach_int(x); return *this; }
        OutputStream& operator << (const long long &x) { base::attach_int(x); return *this; }
        OutputStream& operator << (const unsigned &x) { base::attach_uint(x); return *this; }
        OutputStream& operator << (const unsigned long long &x) { base::attach_uint(x); return *this; }
        OutputStream& operator << (const char &x) { base::putchar(x); return *this; }
        OutputStream& operator << (const char *x) { base::attach_str(x); return *this; }
        OutputStream& operator << (const std::string &x) { base::attach_str(x.c_str()); return *this; }
        void put(const char &c) { base::putchar(c); }
        void flush() { base::putchar.flush(); }
    };
}
fastio::InputStream kin;
fastio::OutputStream kout;
template<typename T> T read() { printf("Error type for template-read: Not supportive.\n"); exit(1); }
template<> int read() { int x; kin >> x; return x; }
template<> long long read() { long long x; kin >> x; return x; }
template<> unsigned read() { unsigned x; kin >> x; return x; }
template<> unsigned long long read() { unsigned long long x; kin >> x; return x; }
template<> char read() { return fastio::base::getchar(); }
template<> std::string read() { std::string x; kin >> x; return x; }

namespace Network_MaxFlow {
    typedef long long int64;
    const int Net_Node = 500/*最大节点数*/, Net_Edge = 5000/*最大边数(无需开两倍)*/;
    const int64 inf = 0x3f3f3f3f3f3f3f3f;
    struct edge {
        int to, nxt_edge;
        int64 flow;
    } e[Net_Edge * 2 + 5];
    int depth[Net_Node + 5], head[Net_Node + 5], cur[Net_Node + 5], ecnt = 1;
    int node_total, st, ed;
    // 清零，此函数适用于多组数据
    void clear() {
        memset(head, 0, sizeof(int) * (node_total + 1));
        ecnt = 1;
        st = ed = 0;
    }
    // 添加边（正向边和反向边均会自动添加）
    inline void add_edge(const int &from, const int &to, const int64 &flow) {
        // Add "positive going edge"
        e[++ecnt].to = to;
        e[ecnt].flow = flow;
        e[ecnt].nxt_edge = head[from];
        head[from] = ecnt;
        // Add "reversed going edge"
        e[++ecnt].to = from;
        e[ecnt].flow = 0;
        e[ecnt].nxt_edge = head[to];
        head[to] = ecnt;
    }
    // Dinic 算法 bfs 函数
    inline bool dinic_bfs() {
        memset(depth, 0x3f, sizeof(int) * (node_total + 1));
        memcpy(cur, head, sizeof(int) * (node_total + 1));
        std::queue<int> q;
        q.push(st);
        depth[st] = 0;
        while (!q.empty()) {
            int u = q.front(); q.pop();
            for (int i = head[u]; i; i = e[i].nxt_edge)
                if (depth[e[i].to] > depth[u] + 1 && e[i].flow) {
                    depth[e[i].to] = depth[u] + 1;
                    if (e[i].to == ed) return ed; // 后续 bfs 到的节点, depth 一定大于 ed, 没有丝毫用处
                    q.push(e[i].to);
                }
        }
        return false;
    }
    // Dinic 算法 dfs 函数
    int64 dinic_dfs(int u, int64 now) {
        if (u == ed) return now;
        int64 max_flow = 0, nRet;
        for (int &i = cur[u]; i && now; i = e[i].nxt_edge)
            if (depth[e[i].to] == depth[u] + 1 && (nRet = dinic_dfs(e[i].to, std::min(now, e[i].flow)))) {
                now -= nRet;
                max_flow += nRet;
                e[i].flow -= nRet;
                e[i ^ 1].flow += nRet;
            }
        return max_flow;
    }
    // Dinic 算法总工作函数，需要提供节点数，起始点（默认 1），结束点（默认 node_count）
    int64 dinic_work(int node_count, int start_node = 1, int finish_node = -1) {
        node_total = node_count;
        st = start_node;
        ed = ~finish_node ? finish_node : node_count;
        int64 max_flow = 0;
        while (dinic_bfs())
            max_flow += dinic_dfs(st, inf);
        return max_flow;
    }
}

int n, m, x[3000], y[3000];

signed main() {
    while (kin >> n >> m) {
        for (int i = 1; i <= m; ++i) {
            kin >> x[i] >> y[i];
            ++x[i], ++y[i];
        }
        int ans = n;
        for (int s = 1; s <= n; ++s) {
            for (int t = 1; t <= n; ++t) {
                if (s == t) continue;
                Network_MaxFlow::clear();
                for (int i = 1; i <= n; ++i)
                    Network_MaxFlow::add_edge(i, i + n, (i == s || i == t) ? 1e6 : 1);
                for (int i = 1; i <= m; ++i) {
                    Network_MaxFlow::add_edge(x[i] + n, y[i], 1e6);
                    Network_MaxFlow::add_edge(y[i] + n, x[i], 1e6);
                }
                ans = min(ans, (int)Network_MaxFlow::dinic_work(2 * n, s, t));
            }
        }
        kout << ans << '\n';
    }
    return 0;
}