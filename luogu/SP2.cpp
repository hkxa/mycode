/*************************************
 * problem:      SP2 PRIME1.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-07-29.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define maxn 35000

bool isNotP[maxn + 7];
int pri[4000 + 7], cnt = 0;

void prepare_sqrtN_prime()
{
    isNotP[1] = true;
    for (int i = 2; i <= maxn; i++) {
        if (!isNotP[i]) {
            pri[++cnt] = i;
            for (int j = i + i; j <= maxn; j += i) {
                isNotP[j] = true;
            }
        }
    }
}

bool isprime(int n)
{
    if (n <= maxn) return !isNotP[n];
    else for (int i = 1, limit = sqrt(n) + 1; pri[i] < limit; i++) {
        if (n % pri[i] == 0) return false;
    }
    return true;
}

void solveCase()
{
    int l = read<int>(), r = read<int>();
    while (l <= r) {
        if (isprime(l)) write(l, 10);
        l++;
    }
    puts("");
}

int main()
{
    prepare_sqrtN_prime();
    int Tcases = read<int>();
    while (Tcases--) solveCase();
    return 0;
}