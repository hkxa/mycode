//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      【模板】原根.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-13.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

namespace against_cpp11 {
    const int N = 1e6 + 7, M = 100 + 7;
    int prime[N], cnt_prime;
    int phi[N];
    bool have_g[N];
    int fpow(int a, int n, int P) {
        int ret = 1;
        while (n) {
            if (n & 1) ret = (int64)ret * a % P;
            a = (int64)a * a % P;
            n >>= 1;
        }
        return ret;
    }
    void init_phi() {
        for (int i = 1; i < N; i++) phi[i] = i;
        for (int i = 2; i < N; i++) {
            if (phi[i] == i) {
                prime[++cnt_prime] = i;
                for (int j = i; j < N; j += i) 
                    phi[j] = (int64)phi[j] * (i - 1) / i;
            }
        }
    }
    void init_have_g() {
        have_g[1] = have_g[2] = have_g[4] = 1;
        for (int i = 2; i <= cnt_prime; i++) {
            for (int64 num = prime[i]; num < N; num *= prime[i]) {
                have_g[num] = 1;
                if ((num << 1) < N) have_g[num << 1] = 1;
                // printf("set %lld\n", num);
            }
        }
    }
    int T, n, output_range;
    int factor[M], cnt_factor;
    int ans[N], cnt_ans;
    void resolveInteger_into_factors_of_PrimeNumber(int value) {
        cnt_factor = 0;
        for (int i = 1; (int64)prime[i] * prime[i] <= value; i++)
            if (value % prime[i] == 0) {
                factor[++cnt_factor] = prime[i];
                while (value % prime[i] == 0) value /= prime[i];
            }
        if (value != 1) factor[++cnt_factor] = value;
    }
    bool check_g(int x, int value) {
        // printf("check_g(%d, %d) phi[val] = %d\n", x, value, phi[value]);
        if (fpow(x, phi[value], value) != 1) return false;
        for (int i = 1; i <= cnt_factor; i++)  
            if (fpow(x, phi[value] / factor[i], value) == 1) return false;
        return true;
    }
    int find_minist_g(int value) {
        for (int i = 1; i < value; i++) 
            if (check_g(i, value)) return i;
        return 0;
    }
    void resolve_answer(int g, int value) {
        // fprintf(stderr, "passing line[%d] in function %s\n", __LINE__, __FUNCTION__);
        cnt_ans = 0;
        int num = g;
        for (int i = 1; i <= phi[value]; i++, num = (int64)num * g % value)
            if (__gcd(i, phi[value]) == 1) ans[++cnt_ans] = num;
        sort(ans + 1, ans + cnt_ans + 1);
        // fprintf(stderr, "passing line[%d] in function %s\n", __LINE__, __FUNCTION__);
    }
    signed main() {
        init_phi();
        init_have_g();
        read >> T;
        while (T--) {
            read >> n >> output_range;
            if (!have_g[n]) {
                write << "0\n\n";
                continue;
            }
            // fprintf(stderr, "passing line[%d] in function %s\n", __LINE__, __FUNCTION__);
            resolveInteger_into_factors_of_PrimeNumber(phi[n]);
            // fprintf(stderr, "passing line[%d] in function %s\n", __LINE__, __FUNCTION__);
            resolve_answer(find_minist_g(n), n);
            // fprintf(stderr, "passing line[%d] in function %s\n", __LINE__, __FUNCTION__);
            write << cnt_ans << '\n';
            for (int i = output_range; i <= cnt_ans; i += output_range)
                write << ans[i] << ' ';
            write << '\n';
        }
        return 0;
    }
}

signed main() { return against_cpp11::main(); }