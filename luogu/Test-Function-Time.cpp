#include <bits/stdc++.h>
using namespace std;
typedef long long int64;
#define TestFunc(f) (printf("Time Used of %-16s: %ld(ms)\n", #f, measure_time(f)))
#define CompareFunc(f1, f2) compare_function_BASE(f1, f2, #f1, #f2)

clock_t measure_time(void (*func)()) {
    clock_t pre = clock();
    func();
    return clock() - pre;
}

void compare_function_BASE(void (*f1)(), void (*f2)(), const char *f1s, const char *f2s) {
    printf("compare_function_BASE: %s, %s\n", f1s, f2s);
    long c1 = measure_time(f1), c2 = measure_time(f2);
    printf("- %-17s: %ld(ms)\n", f1s, c1);
    printf("- %-17s: %ld(ms)\n", f2s, c2);
    if ((c1 == 0 && c2 == 0) || abs(c1 - c2) <= 0.01 * max(c1, c2)) printf("- Almost Same(diff <= 1%%)\n");
    else if (c1 < c2) printf("- Faster: %.3lf%%\n", 100. * (c2 - c1) / c2);
    else printf("- Slower: %.3lf%%\n", 100. * (c1 - c2) / c2);
}

void EmptyFor_21e8() {
    for (int i = 1; i <= 2100000000; ++i);
}

void AssignValue_21e8() {
    unsigned x = 19260817;
    for (int i = 1; i <= 2100000000; ++i) x = x;
}

void IntAdd_21e8() {
    unsigned x = 19260817;
    for (int i = 1; i <= 2100000000; ++i) x = x + x;
}

void Int64Add_21e8() {
    unsigned long long x = 19260817;
    for (int i = 1; i <= 2100000000; ++i) x = x + x;
}

void IntMulMod_21e8() {
    int x = 19260817;
    for (int i = 1; i <= 2100000000; ++i) x = ((int64)x * (int64)x) % 998244353;
}

void Int64MulMod_21e8() {
    int64 x = 19260817;
    for (int i = 1; i <= 2100000000; ++i) x = (x * x) % 998244353;
}

void IntDiv_21e8() {
    int x = 19260817;
    for (int i = 1; i <= 2100000000; ++i) x = 998244353l / x;
}

void UintDiv_21e8() {
    unsigned x = 19260817;
    for (int i = 1; i <= 2100000000; ++i) x = 998244353u / x;
}

int main() {
    // TestFunc(EmptyFor_21e8);
    // TestFunc(AssignValue_21e8);
    // TestFunc(IntAdd_21e8);
    // TestFunc(Int64Add_21e8);
    // TestFunc(IntMulMod_21e8);
    // TestFunc(Int64MulMod_21e8);
    CompareFunc(EmptyFor_21e8, AssignValue_21e8);
    CompareFunc(IntAdd_21e8, Int64Add_21e8);
    CompareFunc(IntMulMod_21e8, Int64MulMod_21e8);
    CompareFunc(IntDiv_21e8, UintDiv_21e8);
    return 0;
}