//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Diversity.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-17.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

int n, m;
uint64 C[53][53], ycl[53];
int a[53][53];
uint64 ans;

signed main() {
    C[0][0] = 1;
    for (int i = 1; i <= 50; i++)
        for (int j = 0; j <= i; j++)
            C[i][j] = C[i - 1][j] + C[i - 1][j - 1];
    for (int i = 2; i <= 50; i++) 
        for (int j = 2; j <= i; j++)
            ycl[i] += C[i][j];
    // for (int i = 2; i <= 50; i++) 
    //     write << "ycl[" << i << "] = " << ycl[i] << '\n';
    read >> n >> m;
    for (int i = 1; i <= n; i++)
        for (int j = 1; j <= m; j++)
            read >> a[i][j];
    for (int i = 1, cnt; i <= n; i++) {
        cnt = 0;
        for (int j = 1; j <= m; j++)
            cnt += a[i][j];
        ans += ycl[cnt] + ycl[m - cnt];
    }
    for (int j = 1, cnt; j <= m; j++) {
        cnt = 0;
        for (int i = 1; i <= n; i++)
            cnt += a[i][j];
        ans += ycl[cnt] + ycl[n - cnt];
    }
    write << ans + n * m << '\n';
    return 0;
}