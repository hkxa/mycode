#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, qCnt;
map<string, string> prob;
string q, ans, t[4];

int main()
{
    n = read<int>();
    qCnt = read<int>();
    for (int i = 1; i <= n; i++) {
        cin >> q >> ans;
        prob[q] = ans;
    }
    for (int i = 1; i <= qCnt; i++) {
        cin >> q >> t[0] >> t[1] >> t[2] >> t[3];
        ans = prob[q];
        for (int j = 0; j < 4; j++) {
            if (ans == t[j]) {
                putchar(j + 'A');
                putchar(10);
                break;
            }
        }
    }
    return 0;
}