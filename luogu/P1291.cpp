/*************************************
 * @problem:      P1291 [SHOI2002]百事世界杯之旅.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-01-22.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;

template <typename I>
I gcd(I a, I b)
{
    return a == 0 ? b : gcd(b % a, a);
} 

template <typename I>
struct fraction {
    I d, son, prt;
    fraction() : d(0), son(0), prt(1) {}
    fraction(I S, I P) : d(0), son(S), prt(P) { reducation(); }
    fraction(I D, I S, I P) : d(D), son(S), prt(P) { reducation(); }
    inline void reducation()
    {
        I g = gcd(son, prt);
        son /= g;
        prt /= g;
        d += son / prt;
        son %= prt;
    }
    inline fraction<I> operator + (fraction<I> F)
    {
        fraction<I> ret(d + F.d, son * F.prt + F.son * prt, prt * F.prt);
        ret.reducation();
        return ret;
    }
};

fraction<int64> f[103 + 3];

template <typename I>
inline fraction<I> make_frac(I a, I b) { return fraction<I>(a, b); }

template <typename I>
inline fraction<I> make_frac(I d, I a, I b) { return fraction<I>(d, a, b); }

template <typename I>
int count_digit(I num)
{
    int ret = 1;
    while (num > 9) {
        num /= 10;
        ret++;
    }
    return ret;
}

int main()
{
    n = read<int>();
    f[1].son = 1;
    for (int i = 2; i <= n; i++) {
        f[i] = f[i - 1] + make_frac<int64>(n, n - i + 1);
    }
    if (f[n].son == 0) write(f[n].d, 10);
    else {
        int c1 = count_digit(f[n].d), c2 = count_digit(f[n].prt);
        for (int i = 1; i <= c1; i++) putchar(' ');
        write(f[n].son, 10);
        write(f[n].d);
        for (int i = 1; i <= c2; i++) putchar('-');
        putchar(10);
        for (int i = 1; i <= c1; i++) putchar(' ');
        write(f[n].prt, 10);
    }
    return 0;
}