/*************************************
 * problem:      id_name.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-mm-dd.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define ForInVec(vectorType, vectorName, iteratorName) for (vector<vectorType>::iterator iteratorName = vectorName.begin(); iteratorName != vectorName.end(); iteratorName++)
#define ForInVI(vectorName, iteratorName) ForInVec(int, vectorName, iteratorName)
#define ForInVE(vectorName, iteratorName) ForInVec(Edge, vectorName, iteratorName)
#define MemWithNum(array, num) memset(array, num, sizeof(array))
#define Clear(array) MemWithNum(array, 0)
#define MemBint(array) MemWithNum(array, 0x3f)
#define MemInf(array) MemWithNum(array, 0x7f)
#define MemEof(array) MemWithNum(array, -1)
#define ensuref(condition) do { if (!(condition)) exit(0); } while(0)

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

struct FastIOer {
#   define createReadlnInt(type)            \
    FastIOer& operator >> (type &x)         \
    {                                       \
        x = read<type>();                   \
        return *this;                       \
    }
    createReadlnInt(short);
    createReadlnInt(unsigned short);
    createReadlnInt(int);
    createReadlnInt(unsigned);
    createReadlnInt(long long);
    createReadlnInt(unsigned long long);
#   undef createReadlnInt

#   define createReadlnReal(type)           \
    FastIOer& operator >> (type &x)         \
    {                                       \
        char c;                             \
        x = read<long long>(c);             \
        if (c != '.') return *this;         \
        type r = (x >= 0 ? 0.1 : -0.1);     \
        while (isdigit(c = getchar())) {    \
            x += r * (c & 15);              \
            r *= 0.1;                       \
        }                                   \
        return *this;                       \
    }
    createReadlnReal(double);
    createReadlnReal(float);
    createReadlnReal(long double);

#   define createWritelnInt(type)           \
    FastIOer& operator << (type &x)         \
    {                                       \
        write<type>(x);                     \
        return *this;                       \
    }
    createWritelnInt(short);
    createWritelnInt(unsigned short);
    createWritelnInt(int);
    createWritelnInt(unsigned);
    createWritelnInt(long long);
    createWritelnInt(unsigned long long);
#   undef createWritelnInt
    
    FastIOer& operator >> (char &x)
    {
        x = getchar();
        return *this;
    }
    
    FastIOer& operator << (char x)
    {
        putchar(x);
        return *this;
    }
    
    FastIOer& operator << (const char *x)
    {
        int __pos = 0;
        while (x[__pos]) {
            putchar(x[__pos++]);
        }
        return *this;
    }
} fast;

int n;
int val[100002][3];
const int LOW_0 = 0;
const int LOW_1 = 1;
const int HIGH_1 = 2;
const int HIGH_2 = 3;
/**
 * @b pos-1 this position : lower or higher than beside & tree high
 * LOW_x means tree high x, lower than beside
 * HIGH_x means tree high x, higher than beside
 */
int f[4][100007] = {0};

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        val[i][0] = read<int>();
        val[i][1] = read<int>();
        val[i][2] = read<int>();
    }
    for (int i = 2; i <= n; i++) {
        f[LOW_0][i] = max(f[HIGH_1][i - 1], f[HIGH_2][i - 1]) + val[i][0];
        f[LOW_1][i] = f[HIGH_2][i - 1] + val[i][1];
        f[HIGH_1][i] = f[LOW_0][i - 1] + val[i][1];
        f[HIGH_2][i] = max(f[LOW_0][i - 1], f[LOW_1][i - 1]) + val[i][2];
        // printf("%d %d %d %d\n", f[LOW_0][i], f[LOW_1][i], f[HIGH_1][i], f[HIGH_2][i]);
    }
    f[LOW_0][1] = max(f[HIGH_1][n], f[HIGH_2][n]) + val[1][0]; 
    f[LOW_1][1] = f[HIGH_2][n] + val[1][1];
    f[HIGH_1][1] = f[LOW_0][n] + val[1][1];
    f[HIGH_2][1] = max(f[LOW_0][n], f[LOW_1][n]) + val[1][2];
    // printf("%d %d %d %d\n", f[LOW_0][1], f[LOW_1][1], f[HIGH_1][1], f[HIGH_2][1]);
    write(max(max(f[LOW_0][1], f[LOW_1][1]), max(f[HIGH_1][1], f[HIGH_2][1])));
    return 0;
}