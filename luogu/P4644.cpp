//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      [USACO05DEC]Cleaning Shifts S.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-22.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 998244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? Module(w - P) : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

const int N = 10007, TIM = 86407;

int n, M, E;
tuple<int, int, int> cow[N];

int64 t[TIM << 2];

int64 query(const int ml, const int mr, int u = 1, int l = M - 1, int r = E) {
    if (l > mr || r < ml) return LONG_LONG_MAX;
    if (l >= ml && r <= mr) return t[u];
    int mid = (l + r) >> 1;
    return min(query(ml, mr, u << 1, l, mid), query(ml, mr, u << 1 | 1, mid + 1, r));
}

void update(const int pos, const int64 newval, int u = 1, int l = M - 1, int r = E) {
    if (l == r) {
        t[u] = min(t[u], newval);
        return;
    }
    int mid = (l + r) >> 1;
    if (pos <= mid) update(pos, newval, u << 1, l, mid);
    else update(pos, newval, u << 1 | 1, mid + 1, r);
    t[u] = min(t[u << 1], t[u << 1 | 1]);
}

signed main() {
    n = read<int>();
    M = read<int>() + 1;
    E = read<int>() + 1;
    memset(t, 0x3f, sizeof(t));
    for (int i = 1, t1, t2, s; i <= n; i++) {
        t1 = max(M, read<int>() + 1);
        t2 = min(E, read<int>() + 1);
        s = read<int>();
        cow[i] = make_tuple(t1, t2, s);
    }
    sort(cow + 1, cow + n + 1);
    update(M - 1, 0);
    for (int i = 1; i <= n; i++)
        update(get<1>(cow[i]), query(get<0>(cow[i]) - 1, get<1>(cow[i])) + get<2>(cow[i]));
    int64 ans = query(E, E);
    write(ans ^ 4557430888798830399LL ? ans : -1, 10);
    return 0;
}

// Create File Date : 2020-06-22

/*
3 0 4
1 2 3
3 4 2
0 0 1
*/