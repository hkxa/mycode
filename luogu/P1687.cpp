/*************************************
 * problem:      P1687 机器人小Q.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-28.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define puts_return(x) { puts(x); return 0; }
#define write_return(x) { write(x); return 0; }

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, m, ans = 0x3f3f3f3f;
int w[3010];
int day[3007][3007], tim[3007][3007];

int main()
{
    n = read<int>();
    m = read<int>();
    for (int i = 1;i <= n;i++){
        w[i] = read<int>();
        if (w[i] > 119) i--, n--;
    }
    if (n < m) puts_return("You can\'t do it.");
    memset(day, 0x3f, sizeof(day));
    memset(tim, 0x3f, sizeof(tim));
    day[0][0] = tim[0][0] = 0;
    int x, y;
    for (int i = 1; i <= n; i++) 
        for (int j = 1; j <= min(i ,m); j++){
            day[i][j] = day[i - 1][j];
            tim[i][j] = tim[i - 1][j];
            x = day[i - 1][j - 1];
            y = tim[i - 1][j - 1] + w[i];
            if (y > 119) {
                x++;
                y = w[i];
            }
            if (x < day[i - 1][j]) {
                day[i][j] = x;
                tim[i][j] = y;
            }
            else if (x == day[i - 1][j])
                if (y < tim[i - 1][j]) {
                    day[i][j] = x;
                    tim[i][j] = y;
                }
        }
    for (int i = m; i <= n; i++) {
        if (tim[i][m] > 0) day[i][m]++;
        ans = min(ans, day[i][m]);      
    }
    write(ans);
    return 0;   
}