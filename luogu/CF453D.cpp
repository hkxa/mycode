//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Little Pony and Elements of Harmony.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-14.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

// inline int64 mul(int64 a, int64 b, int64 P) {
//     int64 ret = 0;
//     while (b) {
//         ret = (ret + a * (b & 4095) % P) % P;
//         a = (a << 12) % P;
//         b >>= 12;
//     }
//     return ret;
// }

int64 mul(int64 a, int64 b, int64 P) {
    // return a * b % P;
    return ((a * b - ((int64)((long double)a / P * b) * P)) % P + P) % P;
}

int64 fpow(int64 a, int64 n, const int64 P) {
    int64 ret = 1;
    while (n) {
        if (n & 1) ret = mul(ret, a, P);
        a = mul(a, a, P);
        n >>= 1;
    }
    return ret;
}

namespace FWT {
    void FWT_XOR(int64 *a, int n, int64 MUL = 1, const int64 P = 998244353) {
        for (int s = 2, t = 1; s <= n; s <<= 1, t <<= 1)
            for (int i = 0; i < n; i += s)
                for (int j = 0; j < t; j++) {
                    int64 a0 = a[i + j], a1 = a[i + j + t];
                    a[i + j] = (a0 + a1) % P;
                    a[i + j + t] = (a0 - a1 + P) % P;
                    a[i + j] = mul(a[i + j], MUL, P);
                    a[i + j + t] = mul(a[i + j + t], MUL, P); 
                }
    }
}

namespace against_cpp11 {
    const int N = 1.2e6 + 7, M = 20 + 7;
    int n, m;
    int64 t, p;
    int64 e0[N], b[N];
    int64 a[N], buf[N];
    const int popcount_table[] = {0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4};
    int popcount(int64 u) {
        int ret = 0;
        while (u) {
            ret += popcount_table[u & 15];
            u >>= 4;
        }
        return ret;
    }
    signed main() {
        read >> m >> t >> p;
        n = (1 << m);
        p *= n;
        for (int i = 0; i < n; i++) read >> e0[i];
        for (int i = 0; i <= m; i++) read >> buf[i];
        for (int i = 0; i < n; i++) b[i] = buf[popcount(i)];
        for (int i = 0; i < n; i++) a[i] = e0[i];
        FWT::FWT_XOR(a, n, 1, p);
        FWT::FWT_XOR(b, n, 1, p);
        // <fpow>
        while (t) {
            if (t & 1) for (int i = 0; i < n; i++) a[i] = mul(a[i], b[i], p);
            for (int i = 0; i < n; i++) b[i] = mul(b[i], b[i], p);
            t >>= 1;
        }
        // </fpow>
        // printf("1/2 = %d\n", fpow(2, p - 2, p));
        FWT::FWT_XOR(a, n, 1, p);
        for (int i = 0; i < n; i++) a[i] /= n;
        for (int i = 0; i < n; i++)
            write << a[i] << '\n';
        return 0;
    }
}

signed main() { return against_cpp11::main(); }