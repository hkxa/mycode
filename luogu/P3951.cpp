/*************************************
 * problem:      P3951 小凯的疑惑.
 * user ID:      85848.
 * user name:    hkxadpall.
 * time:         2019-09-02.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define ForInVec(vectorType, vectorName, iteratorName) for (vector<vectorType>::iterator iteratorName = vectorName.begin(); iteratorName != vectorName.end(); iteratorName++)
#define ForInVI(vectorName, iteratorName) ForInVec(int, vectorName, iteratorName)
#define ForInVE(vectorName, iteratorName) ForInVec(Edge, vectorName, iteratorName)
#define MemWithNum(array, num) memset(array, num, sizeof(array))
#define Clear(array) MemWithNum(array, 0)
#define MemBint(array) MemWithNum(array, 0x3f)
#define MemInf(array) MemWithNum(array, 0x7f)
#define MemEof(array) MemWithNum(array, -1)
#define ensuref(condition) do { if (!(condition)) exit(0); } while(0)

typedef long long int64;
#define P 1000000007
#define modp(x, p) (((x) % p + p) % p)

int64 n, m;

int main()
{
    scanf("%lld%lld", &n, &m);
    printf("%lld", n * m - n - m);
    /*
    3 2
    1
    3 1
    2 2
    0
    */
    return 0;
}