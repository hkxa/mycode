/*************************************
 * @problem:      P2587 [ZJOI2008]泡泡堂.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-01-05.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;
int ZJ[100007], other[100007];

int beat(int *a, int *b)
{
    int ai = 1, aj = n, bi = 1, bj = n, cnt = 0;
    while (ai <= aj && bi <= bj) {
        if (a[ai] > b[bi]) ai++, bi++, cnt += 2;
        else if (a[aj] > b[bj]) aj--, bj--, cnt += 2;
        else {
            cnt += a[ai] == b[bj];
            ai++;
            bj--;
        }
    }
    return cnt;
}

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) ZJ[i] = read<int>();
    for (int i = 1; i <= n; i++) other[i] = read<int>();
    sort(ZJ + 1, ZJ + n + 1, less<int>());
    sort(other + 1, other + n + 1, less<int>());
    // for (int i = 1; i <= n; i++) write(ZJ[i], i == n ? 10 : 32);
    // for (int i = 1; i <= n; i++) write(other[i], i == n ? 10 : 32);
    write(beat(ZJ, other), 32);
    write(n * 2 - beat(other, ZJ));
    return 0;
}