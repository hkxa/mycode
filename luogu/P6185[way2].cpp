/*************************************
 * @problem:      sequence.
 * @user_id:      ZJ-00625.
 * @time:         2020-03-07.
 * @language:     C++.
 * @upload_place: CCF - NOI Online.
*************************************/ 
#pragma GCC optimize("-O2")
#pragma GCC optimize("-Ofast")
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// #define int int64
// #define unsigned uint64

int T, n, m;
int a[100007];
bool chk[100007];
struct Operation {
    int t, u, v;
    bool operator < (const Operation b) const { return u > b.u; }
    // bool operator < (const Operation b) const { return u != b.u ? u > b.u : (v != b.v ? v > b.v : t > b.t); }
    bool operator == (const Operation b) const { return u == b.u && v == b.v && t == b.t; }
} op[100007];
priority_queue<Operation> q;

bool check()
{
    return false;
    register bool now = 0;
    for (int i = 1; i <= n; i++) {
        now ^= (a[i] & 1);
    }
    // printf("now = %d.\n", now);
    if (now) return true;
    memset(chk, 0, sizeof(chk));
    for (int i = 1; i <= m; i++) {
        chk[op[i].u] = true;
        chk[op[i].v] = true;
    }
    for (int i = 1; i <= n; i++) {
        if (a[i] && !chk[i]) return true;
    }
    return false;
}

bool CheckSpecial()
{
    for (int i = 1; i <= m; i++) if (op[i].t == 1) return false;
    memset(chk, 0, sizeof(chk));
    for (int i = 1; i <= m; i++) {
        chk[op[i].u] = true;
        chk[op[i].v] = true;
    }
    for (int i = 1; i <= n; i++) {
        if (a[i] && !chk[i]) {
            puts("NO");
            return true;
        }
    }
    int64 sum = 0;
    for (int i = 1; i <= n; i++) sum += a[i];
    puts(sum == 0 ? "YES" : "NO");
    return true;
}

signed main()
{
    // freopen("sequence.in", "r", stdin);
    // freopen("sequence.out", "w", stdout);
    T = read<int>();
    register Operation f, x;
    register bool Could;
    while (T--) {
        // while (!q.empty()) q.pop();
        n = read<int>();
        m = read<int>();
        for (register int i = 1; i <= n; i++) a[i] = read<int>();
        for (register int i = 1; i <= n; i++) a[i] -= read<int>();
        // printf("case = {%lld, %lld}\n", a[1], a[2]);
        for (register int i = 1; i <= m; i++) {
            op[i].t = read<int>();
            op[i].u = read<int>();
            op[i].v = read<int>();
        }
        if (CheckSpecial()) continue;
        for (register int i = 1; i <= m; i++) {
            if (op[i].t == 2) op[i].t = -1;
            if (op[i].u > op[i].v) swap(op[i].u, op[i].v);
            q.push(op[i]);
        }
        if (check()) {
            puts("NO");
            continue;
        }
        // register bool Has;
        while (!q.empty()) {
            f = q.top(); q.pop(); 
            // if (f.u == f.v) printf("Now the case is!\n");
            // printf("deal {t = %lld, u = %lld, v = %lld}\n", f.t, f.u, f.v);
            while (!q.empty() && f == q.top()) q.pop(); 
            // Has = a[f.u];
            // Has = 0;
            if (f.u == f.v) {
                a[f.u] = (a[f.u] & 1); 
            } else {
                a[f.v] -= a[f.u] * f.t;
                a[f.u] = 0;
            }
            // if (!q.empty()) {
            //     x = q.top();
            //     if (x.u == f.u && (x.v != f.v || x.t != f.t)) 
            //         q.push((Operation){x.t == f.t ? -1 : 1, min(x.v, f.v), max(x.v, f.v)});
            // }
            if (!q.empty()) {
                x = q.top();
                if (x.u == f.u) {
                    if (x.v != f.v) 
                        q.push((Operation){x.t == f.t ? -1 : 1, min(x.v, f.v), max(x.v, f.v)});
                    if (x.v == f.v && x.t != f.t) 
                        q.push((Operation){x.t == f.t ? -1 : 1, min(x.v, f.v), max(x.v, f.v)});
                }
                // if (x.v == f.u) q.push((Operation){x.t == f.t ? -1 : 1, x.u, f.v});
            }
        }
        Could = true;
        for (register int i = 1; i <= n; i++) 
            if (a[i]) {
                Could = false;
                break;
            }
        puts(Could ? "YES" : "NO");
    }
    return 0;
}