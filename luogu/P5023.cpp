/*************************************
 * problem:      P5023 填数游戏.
 * user ID:      85848.
 * user name:    hkxadpall.
 * time:         2019-08-31.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define ForInVec(vectorType, vectorName, iteratorName) for (vector<vectorType>::iterator iteratorName = vectorName.begin(); iteratorName != vectorName.end(); iteratorName++)
#define ForInVI(vectorName, iteratorName) ForInVec(int, vectorName, iteratorName)
#define ForInVE(vectorName, iteratorName) ForInVec(Edge, vectorName, iteratorName)
#define MemWithNum(array, num) memset(array, num, sizeof(array))
#define Clear(array) MemWithNum(array, 0)
#define MemBint(array) MemWithNum(array, 0x3f)
#define MemInf(array) MemWithNum(array, 0x7f)
#define MemEof(array) MemWithNum(array, -1)
#define ensuref(condition) do { if (!(condition)) exit(0); } while(0)

typedef long long int64;
#define P 1000000007
#define modp(x, p) (((x) % p + p) % p)

int64 n, m;

int64 fexp(int64 a, int64 n, int64 p = P, int64 timesBase = 1)
{
    while (n) {
        if (n & 1) timesBase = (timesBase * a) % p;
        a = (a * a) % p;
        n >>= 1;
    }
    return timesBase;
}

bool solve1()
{
    if (n != 1) return false;
    printf("%lld", fexp(2, m));
    return true;
}

bool solve2()
{
    if (n != 2) return false;
    printf("%lld", fexp(3, m - 1) * 4 % P);
    return true;
}

int64 solve(int64 n, int64 m)
{
    if (n == 3 && m == 3) return 112;
    if (n == 3) return 112 * fexp(3, m - 3) % P;
    if (n == 4 && m == 4) return 400 + 512;
    if (n == m) return modp(8 * solve(n - 1, m - 1) - 5 * (1 << n), P);
    if (n > 3 && m > n + 1) return 3 * solve(n, m - 1) % 1000000007;
    if (n > 3 && m > n) return modp(3 * solve(n, m - 1) - 3 * (1 << n), P);
}

int main()
{
    scanf("%lld%lld", &n, &m);
    if (n > m) swap(n, m);
    ensuref(!solve1());
    ensuref(!solve2());
    printf("%lld", solve(n, m));
    return 0;
}