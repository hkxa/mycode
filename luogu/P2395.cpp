/*************************************
 * problem:      P2395 BBCode转换Markdown.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-26.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define puts_return(x) { puts(x); return 0; }
#define write_return(x) { write(x); return 0; }

#ifdef DEBUG
# define passing() cerr << "passing line [" << __LINE__ << "]." << endl
# define debug(...) printf(__VA_ARGS__)
# define show(x) cerr << #x << " = " << x << endl
#else
# define passing() do if (0) cerr << "passing line [" << __LINE__ << "]." << endl;  while(0)
# define debug(...) do if (0) printf(__VA_ARGS__); while(0)
# define show(x) do if (0) cerr << #x << " = " << x << endl; while(0)
#endif

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

typedef int32 BBcode_resolver_retVal;
#define bit(x)          (1 << (x))
#define ok               bit(0)
#define ReturnNow        bit(1)
#define fail            (bit(2) | ReturnNow)
#define MatchError      (bit(3) | fail)
#define UnclosedMark    (bit(4) | fail)
#define UnknownError    (bit(5) | fail)
#define TagEnd          (bit(6) | ok)
#define AllEnd          (bit(7) | ok | ReturnNow)


class BBcode_tester {
  private:
    string code;
    int len, pos;
    stack<string> tag;
    bool quote;
  public:
    BBcode_tester() : len(0), pos(0), quote(0) {}
    BBcode_tester(istream in) : len(0), pos(0), quote(0)
    {
        in >> code;
        len = code.length();
    }
    BBcode_tester(FILE *in) : len(0), pos(0), quote(0)
    {
        char ch;
        while (fscanf(in, "%c", &ch) != EOF) code += ch;
        len = code.length();
    }
    BBcode_tester(char *copy) : len(0), pos(0), quote(0)
    {
        code = copy;
        len = code.length();
    }
    string get_type()
    {
        static string ret;
        debug("[info] get from with pos *%d\n", pos);
        while (code[pos] != '[' && pos < len) pos++;
        if (pos >= len) return "get nothing";
        pos += 3;
        if (code[pos - 2] == '/')  
            ret = string() + code[pos - 1] + code[pos] + '*';
        else 
            ret = string() + code[pos - 2] + code[pos - 1];
        if (quote) {
            if (ret != "qu*") return get_type();
            else quote = false;
        }
        return ret;
    }

    bool check()
    {
        string t;
        while (true) {
            t = get_type();
            if (pos >= len) break;
            if (t.length() == 3) {
                if (tag.empty()) puts_return("Match Error");
                if (tag.top() != t.substr(0, 2)) puts_return("Match Error");
                // printf("pop %s\n", tag.top().c_str());
                tag.pop();
            } else {
                tag.push(t);
                // printf("push %s\n", t.c_str());
                if (t == "qu") quote = true;
            }
        }
        if (tag.empty()) return 1;
        else puts_return("Unclosed Mark");
    }
};

class BBcode_resolver {
  private:
    BBcode_tester tester;
    char code[1 << 23];
    char md[1 << 23];
    int len, pos;
    int mdPos;
    int tagCount;
  public:
    BBcode_resolver() : len(0), pos(0), mdPos(0), tagCount(0) {}
    BBcode_resolver(FILE *in) : len(0), pos(0), mdPos(0), tagCount(0)
    {
        while (fscanf(in, "%c", code + len) != EOF) len++;
        code[len] = '\0';
    }
    BBcode_resolver(istream in) : len(0), pos(0), mdPos(0), tagCount(0)
    {
        while (in >> code[len]) len++;
        code[len] = '\0';
    }
    BBcode_resolver(char *copy) : len(0), pos(0), mdPos(0), tagCount(0)
    {
        while (code[len] = copy[len]) len++;
        code[len] = '\0';
    }
    BBcode_resolver_retVal get(char *save, int &pos)
    {
        debug("[info] get from with pos *%d\n", pos);
        while (code[pos] != '[') {
            md[mdPos++] = code[pos];
            pos++;
            if (pos == len) return AllEnd;
        }
        pos--;
        for (int i = -1; i == -1 || code[pos] != ']' ? 1 : (save[++i] = 0); ) save[++i] = code[++pos];
        pos++;
        debug("[info] get tag %s\n", save);
        debug("[info] get end with pos *%d\n", pos);
        if (!strcmp(save, "[h1]") || !strcmp(save, "[h2]") || !strcmp(save, "[i]") || 
            !strcmp(save, "[b]") || !strncmp(save, "[url=", 5) || !strncmp(save, "[img=", 5) ||
            !strcmp(save, "[quote]")) {
            tagCount++;
            return ok;
        }
        if (save[1] == '/') {
            // tagCount--;
            return TagEnd;
        }
        return UnknownError;
    }
    BBcode_resolver_retVal until(const char *need)
    {
        bool foundAns = 0;
        for (const int needLen = strlen(need); pos + needLen <= len; pos++) {
            for (int i = 0; i < needLen; i++) 
                if (code[pos + i] != need[i]) goto continuePos;
            debug("founded in pos %c%c%c...\n", code[pos - 2], code[pos - 1], code[pos]);
            foundAns = 1;
            break;
            continuePos:;
        }
        pos += strlen(need);
        if (foundAns) {
            tagCount--;
            return ok;
        } else return UnclosedMark;
    }
    BBcode_resolver_retVal deal()
    {
        debug("[info] deal from pos *%d\n", pos);
        int &l = pos, sl, sr;
        if (l >= len) return AllEnd;
        char tag[1 << 8];
        BBcode_resolver_retVal ret = get(tag, l), ret2 = ok;
        debug("nRet := %d\n", ret);
        if (ret != ok) {
            if (ret == TagEnd) pos -= strlen(tag);
            debug("dealRet := %d\n", ret);
            return ret;
        }
        switch (tag[1]) {
            case 'h' : 
                if (tag[2] == '1') {
                    md[mdPos++] = '#';
                    md[mdPos++] = ' ';
                    // l++;
                    while (ret2 != TagEnd) if ((ret2 = deal()) & ReturnNow) return ret2;
                    ret2 = until("[/h1]");
                    if (ret2 & ReturnNow) return ret2;
                    // tagCount--;
                    md[mdPos++] = ' ';
                    md[mdPos++] = '#';
                    // l++;
                    // l += 5;
                } else {
                    md[mdPos++] = '#';
                    md[mdPos++] = '#';
                    md[mdPos++] = ' ';
                    // l++;
                    while (ret2 != TagEnd) if ((ret2 = deal()) & ReturnNow) return ret2;
                    ret2 = until("[/h2]");
                    if (ret2 & ReturnNow) return ret2;
                    // tagCount--;
                    md[mdPos++] = ' ';
                    md[mdPos++] = '#';
                    md[mdPos++] = '#';
                    // l++;
                    // l += 5;
                }
                break;
            case 'i' :
                if (tag[2] == ']') {
                    md[mdPos++] = '*';
                    // l++;
                    while (ret2 != TagEnd) if ((ret2 = deal()) & ReturnNow) return ret2;
                    ret2 = until("[/i]");
                    if (ret2 & ReturnNow) return ret2;
                    // tagCount--;
                    md[mdPos++] = '*';
                    // l++;
                    // l += 4;
                } else {
                    md[mdPos++] = '!';
                    md[mdPos++] = '[';
                    // l++;
                    while (ret2 != TagEnd) if ((ret2 = deal()) & ReturnNow) return ret2;
                    ret2 = until("[/img]");
                    if (ret2 & ReturnNow) return ret2;
                    // tagCount--;
                    md[mdPos++] = ']';
                    md[mdPos++] = '(';
                    for (int i = 5; i < strlen(tag) - 1; i++) md[mdPos++] = tag[i];
                    md[mdPos++] = ')';
                    // l++;
                    // l += 6;
                }
                break;
            case 'b' :
                md[mdPos++] = '_';
                md[mdPos++] = '_';
                // l++;
                while (ret2 != TagEnd) if ((ret2 = deal()) & ReturnNow) return ret2;
                ret2 = until("[/b]");
                if (ret2 & ReturnNow) return ret2;
                // tagCount--;
                md[mdPos++] = '_';
                md[mdPos++] = '_';
                // l++;
                // l += 4;
                break;
            case 'u' :
                md[mdPos++] = '[';
                // l++;
                while (ret2 != TagEnd) if ((ret2 = deal()) & ReturnNow) return ret2;
                ret2 = until("[/url]");
                if (ret2 & ReturnNow) return ret2;
                // tagCount--;
                md[mdPos++] = ']';
                md[mdPos++] = '(';
                for (int i = 5; i < strlen(tag) - 1; i++) md[mdPos++] = tag[i];
                md[mdPos++] = ')';
                // l++;
                // l += 6;
                break;
            case 'q' :
                sl = l;
                ret2 = until("[/quote]");
                if (ret2 & ReturnNow) return ret2;
                // tagCount--;
                sr = l - 1 - 8;
                // l += 8;
                while (code[sl] == '\n') sl++;
                while (code[sr] == '\n') sr--;
                debug("%c%c...%c%c\n", code[sl], code[sl + 1], code[sr - 1], code[sr]);
                if (mdPos && md[mdPos - 1] != '\n') md[mdPos++] = '\n';
                md[mdPos++] = '>';
                md[mdPos++] = ' ';
                while (sl <= sr) {
                    md[mdPos++] = code[sl];
                    if (code[sl] == '\n') {
                        md[mdPos++] = '>';
                        md[mdPos++] = ' ';
                    }
                    sl++;
                }
                md[mdPos++] = '\n';
                break;
            default :
                return UnknownError;
        }
        debug("dealRet := %d\n", ok);
        return ok;
    }

    void solve()
    {
        tester = BBcode_tester(code);
        if (!tester.check()) return;
        pos = 0;
        BBcode_resolver_retVal ret = ok;
        while (pos < len && ret != AllEnd) {
            debug("new Round : \n");
            ret = deal();
            // debug("dealRet := %d\n", ret);
            if (ret != ok) {
                if (ret == UnclosedMark) {
                    puts("Unclosed Mark");
                    return;
                } else if (ret == MatchError || ret == TagEnd) {
                    puts("Match Error");
                    return;
                }
            }
            // pos++;
        }
        if (tagCount) {
            puts("Unclosed Mark");
            return;
        }
        md[mdPos] = 0;
        puts(md);
    }
};

int main()
{
    BBcode_resolver turn(stdin);
    turn.solve();
    return 0;
}