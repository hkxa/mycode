T = int(input())
for _ in range(T):
    n = int(input())
    a = list(map(int, input().split()))
    have0 = 0 in a
    count_negative = sum([1 for i in a if i < 0])
    if count_negative % 2 == 1 or have0:
        print("0")
    else:
        print('1')
        print('1 0')
