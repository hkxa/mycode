/*************************************
 * @problem:      【模板】二逼平衡树（树套树）.
 * @user_name:    brealid.
 * @time:         2020-11-14.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

const int N = 5e4 + 7;

int n, m;

struct segtree_node {
    int ls, rs;
    int siz;
    void clear() { ls = rs = siz = 0; }
} tr[N << 9];

inline void pushup(int u) {
    tr[u].siz = tr[tr[u].ls].siz + tr[tr[u].rs].siz;
}

class MemoryPool {
private:
    int pool_stack[N << 9], stack_top, unused_node;
public:
    MemoryPool() : stack_top(0), unused_node(0) {}
    void recycle(int node) {
        pool_stack[++stack_top] = node;
        tr[node].clear();
    }
    int get() {
        return stack_top ? pool_stack[stack_top--] : ++unused_node;
    }
} pool;

class segment_tree {
private:
    void BASE_insert(int &u, int l, int r, int val) {
        if (!u) u = pool.get();
        if (l == r) {
            ++tr[u].siz;
            return;
        }
        int mid = (l + r) >> 1;
        if (val <= mid) BASE_insert(tr[u].ls, l, mid, val);
        else BASE_insert(tr[u].rs, mid + 1, r, val);
        pushup(u);
    }
    void BASE_erase(int &u, int l, int r, int val) {
        if (l == r) {
            if (!--tr[u].siz) {
                pool.recycle(u);
                u = 0;
            }
            return;
        }
        int mid = (l + r) >> 1;
        if (val <= mid) BASE_erase(tr[u].ls, l, mid, val);
        else BASE_erase(tr[u].rs, mid + 1, r, val);
        pushup(u);
        if (!tr[u].siz) {
            pool.recycle(u);
            u = 0;
        }
    }
public:
    int root;
    segment_tree() : root(0) {}
    void insert(int val) { BASE_insert(root, -100000000, 100000000, val); }
    void erase(int val) { BASE_erase(root, -100000000, 100000000, val); }
};

class bit_include_segt {
private:
    segment_tree segt[N];
    int add[40], nadd;
    int del[40], ndel;
    void ADD_DEL_init_query(int l, int r) {
        nadd = ndel = 0;
        for (; r; r ^= r & -r) add[++nadd] = segt[r].root;
        for (--l; l; l ^= l & -l) del[++ndel] = segt[l].root;
    }
    inline void ADD_DEL_move_to_ls() {
        for (int i = 1; i <= nadd; ++i) {
            add[i] = tr[add[i]].ls;
            if (!add[i]) add[i--] = add[nadd--];
        }
        for (int i = 1; i <= ndel; ++i) {
            del[i] = tr[del[i]].ls;
            if (!del[i]) del[i--] = del[ndel--];
        }
    }
    inline void ADD_DEL_move_to_rs() {
        for (int i = 1; i <= nadd; ++i) {
            add[i] = tr[add[i]].rs;
            if (!add[i]) add[i--] = add[nadd--];
        }
        for (int i = 1; i <= ndel; ++i) {
            del[i] = tr[del[i]].rs;
            if (!del[i]) del[i--] = del[ndel--];
        }
    }
    int BASE_query_rank_result;
    void BASE_query_rank(int l, int r, int value) {
        if (l == r) return;
        int mid = (l + r) >> 1;
        if (value <= mid) {
            ADD_DEL_move_to_ls();
            BASE_query_rank(l, mid, value);
        } else {
            for (int i = 1; i <= nadd; ++i) BASE_query_rank_result += tr[tr[add[i]].ls].siz;
            for (int i = 1; i <= ndel; ++i) BASE_query_rank_result -= tr[tr[del[i]].ls].siz;
            ADD_DEL_move_to_rs();
            BASE_query_rank(mid + 1, r, value);
        }
    }
    int BASE_query_kth(int l, int r, int k) {
        if (l == r) return l;
        int mid = (l + r) >> 1, l_siz = 0;
        for (int i = 1; i <= nadd; ++i) l_siz += tr[tr[add[i]].ls].siz;
        for (int i = 1; i <= ndel; ++i) l_siz -= tr[tr[del[i]].ls].siz;
        if (k <= l_siz) {
            ADD_DEL_move_to_ls();
            return BASE_query_kth(l, mid, k);
        } else {
            ADD_DEL_move_to_rs();
            return BASE_query_kth(mid + 1, r, k - l_siz);
        }
    }
public:
    bit_include_segt() {}
    int query_rank(int l, int r, int val) {
        ADD_DEL_init_query(l, r);
        BASE_query_rank_result = 1;
        BASE_query_rank(-100000000, 100000000, val);
        return BASE_query_rank_result;
    }
    int query_kth(int l, int r, int k) {
        ADD_DEL_init_query(l, r);
        return BASE_query_kth(-100000000, 100000000, k);
    }
    void insert(int position, int value) {
        while (position <= n) {
            segt[position].insert(value);
            position += position & -position;
        }
    }
    void erase(int position, int value) {
        while (position <= n) {
            segt[position].erase(value);
            position += position & -position;
        }
    }
    int query_size(int l, int r) {
        ADD_DEL_init_query(l, r);
        int result = 0;
        for (; r; r ^= r & -r) result += tr[segt[r].root].siz;
        for (--l; l; l ^= l & -l) result -= tr[segt[l].root].siz;
        return result;
    }
    int query_prev(int l, int r, int val) {
        int ret = query_rank(l, r, val);
        if (ret <= 1) return -2147483647;
        return query_kth(l, r, ret - 1);
    }
    int query_next(int l, int r, int val) {
        int ret = query_rank(l, r, val + 1);
        if (ret > query_size(l, r)) return 2147483647;
        return query_kth(l, r, ret);
    }
} bitr;

int a[N];

signed main() {
    read >> n >> m;
    for (int i = 1; i <= n; ++i) {
        read >> a[i];
        bitr.insert(i, a[i]);
    }
    for (int i = 1, opt, x, y, z; i <= m; ++i) {
        read >> opt >> x >> y;
        if (opt != 3) read >> z;
        switch (opt) {
            case 1: write << bitr.query_rank(x, y, z) << '\n'; break;
            case 2: write << bitr.query_kth(x, y, z) << '\n'; break;
            case 3: bitr.erase(x, a[x]); bitr.insert(x, a[x] = y); break;
            case 4: write << bitr.query_prev(x, y, z) << '\n'; break;
            case 5: write << bitr.query_next(x, y, z) << '\n'; break;
        }
    }
    return 0;
}