//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      [SDOI2010]外星千足虫.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-15.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 998244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? Module(w - P) : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

const int N = 2000 + 7;

void CannotDetermine() {
    puts("Cannot Determine");
    exit(0);
}

int n, m;
char s[N];
bitset<N> a[N], t;
int ans = 0;

void debug(int u) {
    printf("--- debug (%d) ---\n", u);
    for (int i = 1; i <= m; i++) {
        for (int j = 1; j <= n; j++) putchar(a[i].test(j) | 48);
        printf(" -> %d\n", a[i].test(n + 1));
    }
    printf("-----------------\n");
}

bool Guass() {
    for (int i = 1; i <= n; i++) {
        int Will = i;
        while (Will <= m && !a[Will][i]) Will++;
        if (Will > m) return false;
        ans = max(ans, Will);
        swap(a[i], a[Will]);
        t.reset();
        for (int j = i; j <= n + 1; j++) t.set(j);
        if (!a[i][i]) a[i] ^= t;
        t.reset();
        for (int j = i; j <= n + 1; j++) if (a[i].test(j)) t.set(j);
        for (int j = 1; j <= m; j++) if (i != j) {
            if (a[j][i]) a[j] ^= t;            
        }
        // debug(i);
    }
    return true;
}

signed main() {
    n = read<int>();
    m = read<int>();
    for (int i = 1; i <= m; i++) {
        scanf("%s", s + 1);
        for (int j = 1; j <= n; j++) 
            if (s[j] & 1) a[i].set(j);
        if (read<int>()) a[i].set(n + 1);
    }
    if (!Guass()) CannotDetermine();
    else {
        write(ans, 10);
        for (int i = 1; i <= n; i++)
            if (a[i][n + 1]) puts("?y7M#");
            else puts("Earth");
    }
    return 0;
}

// Create File Date : 2020-06-15

/*
3 5
011 1
101 1
110 0
111 1
001 1
*/