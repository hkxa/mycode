/*************************************
 * problem:      P1277 拼字游戏.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-08.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
#include <windows.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int row[4], col[4], dia[2];
int rn[4], cn[4], dn[2];
int rrest[4], crest[4], drest[2];
int mat[4][4] = {0}, now[4][4] = {0};
int x, y;

bool chk()
{
    int t = 0;
    for (int i = 0; i < 4; i++) {
        t = 0;
        for (int j = 0; j < 4; j++) {
            t += now[i][j];
        }
        if (t != row[i]) return false;
    }
    for (int j = 0; j < 4; j++) {
        t = 0;
        for (int i = 0; i < 4; i++) {
            t += now[i][j];
        }
        if (t != col[j]) return false;
    }
    t = 0;
    for (int i = 0; i < 4; i++) {
        t += now[i][i];
    }
    if (t != dia[0]) return false;
    t = 0;
    for (int i = 0; i < 4; i++) {
        t += now[i][3 - i];
    }
    if (t != dia[1]) return false;
    return true;
}
    
bool cut()
{
    int t;
    for (int i = 0; i < 4; i++) {
        if (rn[i] == 4) {
            t = 0;
            for (int j = 0; j < 4; j++) {
                t += now[i][j];
            }
            if (t != row[i]) return true;
        } else break;
    }

    
    for (int i = 0; i < 4; i++) {
        if (cn[i] == 4) {
            t = 0;
            for (int j = 0; j < 4; j++) {
                t += now[j][i];
            }
            if (t != col[i]) return true;
        } else break;
    }
    if (dn[0] == 4) {
        t = 0;
        for (int i = 0; i < 4; i++) {
            t += now[i][i];
        }
        if (t != dia[0]) return true;
    }
    if (dn[1] == 4) {
        t = 0;
        for (int i = 0; i < 4; i++) {
            t += now[i][3 - i];
        }
        if (t != dia[1]) return true;
    }
    return false;
}

// #include <time.h>
// clock_t startTime;

void dfs(int x, int y)
{
    if (cut()) return;
    // printf("[%d, %d].\n", x, y);
    // for (int i = 0; i < 4; i++) {
    //     for (int j = 0; j < 4; j++) {
    //         write(now[i][j], " \n"[j == 3]);
    //     }
    // }
    // Sleep(100);
    if (y == 4) {
        dfs(x + 1, 0);
        return;
    }
    if (x == 4) {
        // todo : del this func 'chk()'
        if (chk()) {
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    write(now[i][j], " \n"[j == 3]);
                }
            }
            // long int cost = clock() - startTime;
            // printf("\nThe program costs %ld.%03lds.\n", cost / 1000, cost % 1000);
            exit(0);
        }
        return;
    }
    if (mat[x][y]) {
        now[x][y] = mat[x][y];
        // fprintf(stderr, "(%d, %d) = %d.\n", x, y, now[x][y]);
        dfs(x, y + 1);
        return;
    }
    if (rn[x] == 3) {
        if (rrest[x] < 1) return;
        now[x][y] = rrest[x];
        crest[y] -= now[x][y];
        if (x == y) drest[0] -= now[x][y];
        if (x + y == 3) drest[1] -= now[x][y];
        // fprintf(stderr, "(%d, %d) = %d.\n", x, y, now[x][y]);
        dfs(x, y + 1);
        crest[y] += now[x][y];
        if (x == y) drest[0] += now[x][y];
        if (x + y == 3) drest[1] += now[x][y];
        return;
    }
    if (cn[y] == 3) {
        if (crest[y] < 1) return;
        now[x][y] = crest[y];
        rrest[x] -= now[x][y];
        if (x == y) drest[0] -= now[x][y];
        if (x + y == 3) drest[1] -= now[x][y];
        // fprintf(stderr, "(%d, %d) = %d.\n", x, y, now[x][y]);
        dfs(x, y + 1);
        rrest[x] += now[x][y];
        if (x == y) drest[0] += now[x][y];
        if (x + y == 3) drest[1] += now[x][y];
        return;
    }
    if (x == 3 && y == 0) {
        if (drest[1] < 1) return;
        now[x][y] = drest[1];
        rrest[x] -= now[x][y];
        crest[y] -= now[x][y];
        // fprintf(stderr, "(%d, %d) = %d.\n", x, y, now[x][y]);
        dfs(x, y + 1);
        rrest[x] += now[x][y];
        crest[y] += now[x][y];
        return;
    }
    if (x == 3 && y == 3) {
        if (drest[0] < 1) return;
        now[x][y] = drest[0];
        rrest[x] -= now[x][y];
        crest[y] -= now[x][y];
        // fprintf(stderr, "(%d, %d) = %d.\n", x, y, now[x][y]);
        dfs(x, y + 1);
        rrest[x] += now[x][y];
        crest[y] += now[x][y];
        return;
    }
    int mx = min(rrest[x] + rn[x] - 3, crest[y] + cn[y] - 3);
    if (x == y) mx = min(mx, drest[0] + dn[0] - 3);
    if (x + y == 3) mx = min(mx, drest[1] + dn[1] - 3);
    // if (mx > 0) 
    //     fprintf(stderr, "(%d, %d) : mx = %d.\n", x, y, mx);
    // Sleep(1200);
    for (int i = 1; i <= mx; i++) {
        // fprintf(stderr, "(%d, %d) = %d.\n", x, y, i);
        // Sleep(1200);
        rrest[x] -= i, rn[x]++;
        crest[y] -= i, cn[y]++;
        if (x == y) drest[0] -= i, dn[0]++;
        if (x + y == 3) drest[1] -= i, dn[1]++;
        now[x][y] = i;
        dfs(x, y + 1);
        rrest[x] += i, rn[x]--;
        crest[y] += i, cn[y]--;
        if (x == y) drest[0] += i, dn[0]--;
        if (x + y == 3) drest[1] += i, dn[1]--;
    }
}

int main()
{
    for (int i = 0; i < 4; i++) rrest[i] = row[i] = read<int>();
    for (int i = 0; i < 4; i++) crest[i] = col[i] = read<int>();
    for (int i = 0; i < 2; i++) drest[i] = dia[i] = read<int>();
    for (int i = 0; i < 4; i++) {
        x = read<int>();
        y = read<int>();
        mat[x][y] = now[x][y] = read<int>();
        rrest[x] -= mat[x][y];
        crest[y] -= mat[x][y];
        if (x == y) drest[0] -= mat[x][y];
        if (x + y == 3) drest[1] -= mat[x][y];
        rn[x]++;
        cn[y]++;
        if (x == y) dn[0]++;
        if (x + y == 3) dn[1]++;
    }
    // startTime = clock();
    dfs(0, 0);
    // long int cost = clock() - startTime;
    // printf("\nThe program costs %ld.%03lds.\n", cost / 1000, cost % 1000);
    return 0;
}

/*

11 16 13 4 9 9 11 15 8 15 3 2 1 1 3 4 3 1 1 2 0 2 

 * * * *
 * * * 4
 2 * * *
 * 1 1 *

 */