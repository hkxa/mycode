/*************************************
 * @problem:      P2233 [HNOI2002]公交车路线.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-01-10.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;
int _f[2][7];
#define f _f[i & 1]
#define g _f[(i & 1) ^ 1]
#define ans _f[(n & 1) ^ 1]

int main()
{
    _f[0][3] = 1;
    n = read<int>();
    for (int i = 1; i < n; i++) {
        f[0] = g[1];
        f[1] = (g[0] + g[2]) % 1000;
        f[2] = (g[1] + g[3]) % 1000;
        f[3] = (g[2] + g[4]) % 1000;
        f[4] = (g[3] + g[5]) % 1000;
        f[5] = (g[4] + g[6]) % 1000;
        f[6] = g[5];
    }
    write((ans[0] + ans[6]) % 1000, 10);
    return 0;
}
/*
0  0  0  1  0  0  0
0  0  1  0  1  0  0
0  1  0  2  0  1  0
1  0  3  0  3  0  1
0  4  0  6  0  4  0
4  0  10 0  10 0  4
*/