/*************************************
 * problem:      P1144 最短路计数.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-06.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define P 100003

int n, m;
int inQueue[1000010], dis[1000010], NearestRoadCnt[1000010]; 
vector<int> G[1000010];
queue<int> q;

void spfa()
{
    queue<int> q;
    memset(dis, 0x7f, sizeof(dis));
    q.push(1);
    inQueue[1] = 1;
    NearestRoadCnt[1] = 1;
    dis[1] = 0;
    while (!q.empty()) {
        int u = q.front();
        q.pop();
        inQueue[u] = 0;
        for (int i = 0; i < G[u].size(); i++) {
            int v = G[u][i];
            if (dis[v] > dis[u] + 1) {
                dis[v] = dis[u] + 1;
                NearestRoadCnt[v] = NearestRoadCnt[u];
                if (!inQueue[v]) {
                    inQueue[v] = 1;
                    q.push(v);
                }
            } else if (dis[v] == dis[u] + 1) {
                NearestRoadCnt[v] = (NearestRoadCnt[v] + NearestRoadCnt[u]) % P; 
            }
        }
        inQueue[u] = 0;
    }
}
int main()
{
    n = read<int>();
    m = read<int>();
    int u, v;
    while (m--) {
        u = read<int>();
        v = read<int>();
        G[u].push_back(v);
        G[v].push_back(u);
    }
    spfa();  
    for (int i = 1; i <= n; i++) {
        write(NearestRoadCnt[i], 10);
    }
    return 0;
}