/*************************************
 * problem:      P2602 [ZJOI2010]数字计数.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-10.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int64 a, b;
int64 power10[21], f[21];

#define FOR_IN_DIGIT(_ItName) for (int _ItName = 0; _ItName < 10; _ItName++)

vector<int64> calc(int64 x)
{
    vector<int64> ans;
    ans.resize(10);
    long long num[20] = {0};
    int len = 0;
    while(x) {
        num[++len] = x % 10;
        x /= 10;
    } 
    for (int i = len; i; i--) {
        FOR_IN_DIGIT(j) {
            ans[j] += f[i - 1] *num[i];
        }
        for (int j = 0; j < num[i]; j++) {
            ans[j] += power10[i-1];
        }
        int64 restNum = 0;
        for (int j = i - 1; j >= 1; j--) {
            restNum = restNum * 10 + num[j];
        }
        ans[num[i]] += restNum + 1;
        ans[0] -= power10[i-1];
    } 
    return ans;
}

int main()
{
    a = read<int64>();
    b = read<int64>();
    power10[0] = 1;
    for (int i = 1; i <= 13; i++) {
        f[i] = i * power10[i - 1];
        power10[i] = power10[i - 1] * 10;
    }
    vector<int64> aCnt, bCnt;
    aCnt = calc(a - 1);
    bCnt = calc(b);
    FOR_IN_DIGIT(i) {
        write(bCnt[i] - aCnt[i], 32);
    }
    return 0;
}