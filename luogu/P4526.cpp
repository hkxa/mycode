/*************************************
 * @problem:      【模板】自适应辛普森法2.
 * @author:       brealid.
 * @time:         2020-11-24.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

const double div6 = 1 / 6., eps = 1e-8;
double a;

inline double f(double x) {
    return pow(x, a / x - x);
}

double auto_simpson(double l, double r, double mid, double f0, double f2, double f4) {
    // printf("auto_simpson(%.6lf, %.6lf, %.6lf, %.6lf, %.6lf, %.6lf, %.6lf)\n", l, r, mid, eps, f0, f2, f4);
    double mid_l = (l + mid) * 0.5, mid_r = (mid + r) * 0.5;
    double f1 = f((l + mid) * 0.5), f3 = f((mid + r) * 0.5);
    double simpson_all = (f0 + 4 * f2 + f4) * (r - l) * div6;
    double simpson_l = (f0 + 4 * f1 + f2) * (mid - l) * div6;
    double simpson_r = (f2 + 4 * f3 + f4) * (r - mid) * div6;
    if (fabs(simpson_l + simpson_r - simpson_all) < 15 * eps) return simpson_l + simpson_r + (simpson_l + simpson_r - simpson_all) / 15;
    return auto_simpson(l, mid, mid_l, f0, f1, f2) + auto_simpson(mid, r, mid_r, f2, f3, f4);
}

signed main() {
    cin >> a;
    if (a < 0) {
        cout << "orz" << endl;
        return 0;
    }
    double L = eps, R = 30, mid = (L + R) * 0.5;
    cout << fixed << setprecision(5) << auto_simpson(L, R, mid, f(L), f(mid), f(R)) << endl;
    return 0;
}