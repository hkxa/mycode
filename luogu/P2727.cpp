/*************************************
 * problem:      P2727 01串 Stringsobits.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-11-09.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int64 n, l, k;
int64 C(int64 n, int64 m)
{
    int64 res = 1;
    for (int i = 1; i <= m; i++) {
        res *= (n - i + 1);
        res /= i;
    }
    return res;
}

int64 get(int64 n, int64 m) {
    int64 res = 0;
    for (int i = 0; i <= m; i++) res += C(n, i);
    return res;
}

int main()
{
    n = read<int64>();
    l = read<int64>();
    k = read<int64>() - 1;
    for (int i = n, t; i >= 1; i--) {
        t = get(i - 1, l);
        if (k >= t) {
            k -= t;
            l--;
            putchar(49);
        } else putchar(48);
    }
    return 0;
}