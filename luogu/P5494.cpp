/*************************************
 * @problem:      【模板】线段树分裂.
 * @author:       brealid.
 * @time:         2021-03-21.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 2e5 + 7, SIZ = 4e7 + 7;

int n, m, root[N], id;
int a[N];

namespace segt {
    struct node {
        int l, r;
        int64 tot;
    } tr[SIZ];
    int tr_cnt;

    void build(int &u, int l, int r) {
        if (!u) u = ++tr_cnt;
        if (l == r) {
            tr[u].tot = a[l];
            return;
        }
        int mid = (l + r) >> 1;
        build(tr[u].l, l, mid);
        build(tr[u].r, mid + 1, r);
        tr[u].tot = tr[tr[u].l].tot + tr[tr[u].r].tot;
    }

    void insert(int &u, int l, int r, int pos, int xcnt) {
        if (!u) u = ++tr_cnt;
        tr[u].tot += xcnt;
        if (l == r) return;
        int mid = (l + r) >> 1;
        if (pos <= mid) insert(tr[u].l, l, mid, pos, xcnt);
        else insert(tr[u].r, mid + 1, r, pos, xcnt);
    }

    int split(int &u, int l, int r, int x) {
        if (r <= x) return 0;
        if (l > x) {
            int nRet(u);
            u = 0;
            return nRet;
        }
        int mid = (l + r) >> 1;
        int vl = split(tr[u].l, l, mid, x), vr = split(tr[u].r, mid + 1, r, x);
        tr[u].tot = tr[tr[u].l].tot + tr[tr[u].r].tot;
        if (!vl && !vr) return 0;
        tr[++tr_cnt].l = vl, tr[tr_cnt].r = vr;
        tr[tr_cnt].tot = tr[vl].tot + tr[vr].tot;
        return tr_cnt;
    }

    void merge(int &u, int v, int l, int r) {
        if (!u) {
            u = v;
            return;
        }
        if (!v) return;
        tr[u].tot += tr[v].tot;
        if (l == r) return;
        int mid = (l + r) >> 1;
        merge(tr[u].l, tr[v].l, l, mid);
        merge(tr[u].r, tr[v].r, mid + 1, r);
    }

    int query_kth(int u, int64 k) {
        if (k > tr[u].tot) return -1;
        int l = 1, r = n;
        while (l < r) {
            int mid = (l + r) >> 1;
            if (k <= tr[tr[u].l].tot) r = mid, u = tr[u].l;
            else k -= tr[tr[u].l].tot, l = mid + 1, u = tr[u].r;
        }
        return l;
    }

    int64 query_inRange(int u, int l, int r, int ml, int mr) {
        if (l >= ml && r <= mr) return tr[u].tot;
        int mid = (l + r) >> 1;
        int64 nRet = 0;
        if (ml <= mid && tr[u].l) nRet = query_inRange(tr[u].l, l, mid, ml, mr);
        if (mr > mid && tr[u].r) nRet += query_inRange(tr[u].r, mid + 1, r, ml, mr);
        return nRet;
    }
}

void DebugTree(int tr) {
    printf("tr %d: { ", tr);
    if (!root[tr]) {
        printf("\b\b[NULL]\n");
        return;
    }
    for (int i = 1; i <= n; ++i) printf("%d, ", segt::query_inRange(root[tr], 1, n, i, i));
    printf("\b\b }\n");
}

signed main() {
    kin >> n >> m;
    for (int i = 1; i <= n; ++i) kin >> a[i];
    segt::build(root[++id], 1, n);
    for (int i = 1, opt, p, x, y; i <= m; ++i) {
        // printf("[prompt] (0=split 1=merge 2=insert 3=count 4=kth) ");
        kin >> opt >> p;
        if (opt == 0) {
            kin >> x >> y;
            root[++id] = segt::split(root[p], 1, n, x - 1);
            int right_part = segt::split(root[id], 1, n, y);
            segt::merge(root[p], right_part, 1, n);
        } else if (opt == 1) {
            kin >> x;
            segt::merge(root[p], root[x], 1, n);
            root[x] = 0;
        } else if (opt == 2) {
            kin >> x >> y;
            segt::insert(root[p], 1, n, y, x);
        } else if (opt == 3) {
            kin >> x >> y;
            kout << segt::query_inRange(root[p], 1, n, x, y) << '\n';
        } else {
            kout << segt::query_kth(root[p], kin.get<int64>()) << '\n';
        }
        // printf("============= Operation %d finished =============\n", i);
        // for (int now = 1; now <= id; ++now) DebugTree(now);
    }
    return 0;
}

/*
## Generator
```python
import os, random, sys

def print_seq(seq, sep = ' ', end = '\n'):
    for x in range(len(seq)):
        print(seq[x], end = sep)
    print(end, end = '')

n, m, cnt_max = 1000, 1000, 100

print(f'[Generator for P5494] n = {n}, m = {m}, cnt_max = {cnt_max}', file = sys.stderr)
tree_alive = [1]
total = 1

print(n, m)

print_seq([random.randint(0, cnt_max) for i in range(n)])

for i in range(m):
    if len(tree_alive) > 1:
        opt = random.randint(0, 4)
    else:
        opt = random.randint(1, 4)
        if opt == 1:
            opt = 0
    p = random.choice(tree_alive)
    if opt == 0 or opt == 3:
        x, y = random.randint(1, n), random.randint(1, n)
        print(opt, p, min(x, y), max(x, y))
        if opt == 0:
            total += 1
            tree_alive.append(total)
    elif opt == 1:
        another = random.choice(tree_alive)
        while another == p:
            another = random.choice(tree_alive)
        if random.randint(0, 1):
            print(opt, p, another)
        else:
            print(opt, p, another)
    elif opt == 2:
        print(opt, p, random.randint(0, cnt_max), random.randint(1, n))
    elif opt == 4:
        print(opt, p, random.randint(1, cnt_max // 3))
```
## Checker
```python
import os, random, sys

CompileCommand = ['g++ -std=c++11 P5494.cpp -o user', 
                  'g++ -std=c++11 std.cpp -o std']
CaseCommand = ['C:\\Users\\xyz\\AppData\\Local\\Programs\\Python\\Python38\\python.exe gen.py > in',
               'user < in > out', 
               'std < in > ans']

for cmd in CompileCommand:
    os.system(cmd)

while True:
    for cmd in CaseCommand:
        os.system(cmd)
    if os.system('fc out ans'):
        os.system('pause')
```
*/