#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, maxh;
int rec[20];

bool dfs(int x, int cur)
{
    if ((x << (maxh - cur) < n) || cur > maxh) return false;  
    if (x == n) return true;
    rec[cur] = x;

    for (int i = 0; i <= cur; i++)
        if (dfs(x + rec[i], cur + 1) || dfs(x > rec[i] ? x - rec[i] : rec[i] - x, cur + 1)) 
            return true;
    return false;
}

int main()
{
    n = read<int>();
    while (n) {
        maxh = 0;
        while (!dfs(1, 0)) maxh++;
        write(maxh, 10);
        n = read<int>();
    }
    return 0;
}