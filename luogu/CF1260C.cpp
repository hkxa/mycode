/*************************************
 * @problem:      CF1260C Infinite Fence.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-04-25.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int64 gcd(int64 n, int64 m) {
    return m ? gcd(m, n % m) : n;
}

int64 n, m, k, g;

void Solve()
{
    n = read<int64>();
    m = read<int64>();
    k = read<int64>();
    if (k == 1) {
        puts("REBEL");
        return;
    }
    if (n == m) {
        puts("OBEY");
        return;
    }
    if (n < m) swap(n, m);
    g = gcd(n, m);
    n /= g;
    m /= g;
    if (n - 1 >= m * (k - 1) + 1) {
        puts("REBEL");
        return;
    } 
    puts("OBEY");
}

int main()
{
    int T = read<int>();
    while (T--) Solve();
    return 0;
}

/*
12
2 10 4
2 3 6
1 4 7
1 1 2
370359350 416913505 3
761592061 153246036 6
262185277 924417743 5
668232501 586472717 2
891054824 169842323 6
629603359 397927152 2
2614104 175031972 68
924509243 421614240 4

No
Yes
Yes
Yes
Yes
Yes
Yes
No
No
No
Yes
Yes
*/