//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      聪明的打字员.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-07.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

struct Reader {
    char endch;
    Reader() { endch = '\0'; }
    template <typename Int>
    Reader& operator >> (Int &i) {
        i = baseFastio::read<Int>(endch);
        return *this;
    }
    template <typename Int>
    inline Int get_int() {
        return baseFastio::read<Int>();
    }
    inline char get_nxt() {
        return endch;
    }
} read;

struct Writer {
    Writer& operator << (const char *ch) {
        // char *p = ch;
        while (*ch) putchar(*(ch++));
        return *this;
    }
    Writer& operator << (const char ch) {
        putchar(ch);
        return *this;
    }
    template <typename Int>
    Writer& operator << (const Int i) {
        baseFastio::write(i);
        return *this;
    }
} write;

// #define int int64

struct status {
    int16 a[6];
    int16 pos;
    operator int () {
        int res = 0;
        for (int i = 0; i < 6; i++) res = (((res << 2) + res) << 1) + a[i];
        return res * 6 + pos;
    }
    int16& now() {
        return a[pos];
    }
} st, ed;

int f[6000000 + 7];
queue<status> q;

void read_status(status &s) {
    int r = read.get_int<int>();
    for (int i = 5; i >= 0; i--) {
        s.a[i] = r % 10;
        r /= 10;
    }
    s.pos = 0;
}

bool is_end(status chk) {
    for (int i = 0; i < 6; i++)
        if (ed.a[i] != chk.a[i]) return false;
    return true;
}

void swap0(status s) {
    if (s.now() == s.a[0]) return;
    int fval = f[s];
    swap(s.now(), s.a[0]);
    if (!~f[s]) {
        f[s] = fval + 1;
        q.push(s);
    }
}

void swap1(status s) {
    if (s.now() == s.a[5]) return;
    int fval = f[s];
    swap(s.now(), s.a[5]);
    if (!~f[s]) {
        f[s] = fval + 1;
        q.push(s);
    }
}

void Up(status s) {
    if (s.now() == 9) return;
    int fval = f[s];
    s.now()++;
    if (!~f[s]) {
        f[s] = fval + 1;
        q.push(s);
    }
}

void Down(status s) {
    if (s.now() == 0) return;
    int fval = f[s];
    s.now()--;
    if (!~f[s]) {
        f[s] = fval + 1;
        q.push(s);
    }
}

void Left(status s) {
    if (s.pos == 0) return;
    int fval = f[s];
    s.pos--;
    if (!~f[s]) {
        f[s] = fval + 1;
        q.push(s);
    }
}

void Right(status s) {
    if (s.pos == 5) return;
    int fval = f[s];
    s.pos++;
    if (!~f[s]) {
        f[s] = fval + 1;
        q.push(s);
    }
}

signed main()
{
    memset(f, -1, sizeof(f));
    read_status(st);
    read_status(ed);
    q.push(st);
    f[st] = 0;
    while (!q.empty()) {
        st = q.front(); q.pop();
        if (is_end(st)) break;
        swap0(st);
        swap1(st);
        Up(st);
        Down(st);
        Left(st);
        Right(st);
    }
    write << f[st] << '\n';
    return 0;
}