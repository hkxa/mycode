/*************************************
 * problem:      P1993 小K的农场.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-03-10.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 
/*************************************
 * now_type:	 loading
 * now_bak:		 写了一半 
 * next_step:    鸽子 
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

struct Edge {
	int to, v;
	Edge(int To = 0, int V = 0) : to(To), v(V) {}
};

vector <Edge> G[10007];
int n, m;

int main()
{
	n = read<int>();
	m = read<int>();
	int type, a, b, c;
	while (m--) {
		type = read<int>();
		a = read<int>();
		b = read<int>();
		if (type != 3) c = read<int>();
		switch (type) {
			case 1:
				G[v].push_back(Edge(u, -w));
			case 2:
				G[u].push_back(Edge(v, w));
			case 3:
				G[u].push_back(Edge(v, 0));
				G[v].push_back(Edge(u, 0));
		}
	}
	SPFA();
	return 0;
}
