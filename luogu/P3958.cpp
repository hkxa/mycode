/*************************************
 * problem:      P3958 奶酪.
 * user ID:      85848.
 * user name:    hkxadpall.
 * time:         2019-09-09.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define ForInVec(vectorType, vectorName, iteratorName) for (vector<vectorType>::iterator iteratorName = vectorName.begin(); iteratorName != vectorName.end(); iteratorName++)
#define ForInVI(vectorName, iteratorName) ForInVec(int, vectorName, iteratorName)
#define ForInVE(vectorName, iteratorName) ForInVec(Edge, vectorName, iteratorName)
#define MemWithNum(array, num) memset(array, num, sizeof(array))
#define Clear(array) MemWithNum(array, 0)
#define MemBint(array) MemWithNum(array, 0x3f)
#define MemInf(array) MemWithNum(array, 0x7f)
#define MemEof(array) MemWithNum(array, -1)
#define ensuref(condition) do { if (!(condition)) exit(0); } while(0)

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

struct FastIOer {
#   define createReadlnInt(type)            \
    FastIOer& operator >> (type &x)         \
    {                                       \
        x = read<type>();                   \
        return *this;                       \
    }
    createReadlnInt(short);
    createReadlnInt(unsigned short);
    createReadlnInt(int);
    createReadlnInt(unsigned);
    createReadlnInt(long long);
    createReadlnInt(unsigned long long);
#   undef createReadlnInt

#   define createWritelnInt(type)           \
    FastIOer& operator << (type x)          \
    {                                       \
        write<type>(x);                     \
        return *this;                       \
    }
    createWritelnInt(short);
    createWritelnInt(unsigned short);
    createWritelnInt(int);
    createWritelnInt(unsigned);
    createWritelnInt(long long);
    createWritelnInt(unsigned long long);
#   undef createWritelnInt
    
    FastIOer& operator >> (char &x)
    {
        x = getchar();
        return *this;
    }
    
    FastIOer& operator << (char x)
    {
        putchar(x);
        return *this;
    }
    
    FastIOer& operator << (const char *x)
    {
        int __pos = 0;
        while (x[__pos]) {
            putchar(x[__pos++]);
        }
        return *this;
    }
} fast;

int64 n, h, r;
struct pos {
    int64 x, y, z;
} a[1007];

#define pow2(exp) ((exp) * (exp))
#define calc_dist(u, v) sqrt(pow2(a[u].x - a[v].x) + pow2(a[u].y - a[v].y) + pow2(a[u].z - a[v].z))
bool check(int u, int v) { return u > v ? check(v, u) : (u == 0 || v == n + 1); }
bool special(int u, int v) { return u > v ? special(v, u) : (u == 0 ? a[v].z - r <= 0 : a[u].z + r >= h); }
#define isnear(u, v) (check(u, v) ? special(u, v) : calc_dist(u, v) <= r * 2)

struct UnionFindSet {
    int fa[1007];
    UnionFindSet() { MemEof(fa); }
    int find(int u) { return fa[u] < 0 ? u : fa[u] = find(fa[u]); }
    bool connect(int x, int y)
    {
        int fx = find(x), fy = find(y);
        if (fx != fy) {
            if (fa[fx] > fa[fy]) swap(fx, fy);
            fa[fx] += fa[fy];
            fa[fy] = fx;
            return true;
        }
        return false;
    }
};

bool solve()
{
    UnionFindSet ufs;
    for (int i = 1; i <= n + 1; i++) {
        for (int j = (i == n + 1); j < i; j++) {
            if (i != j && isnear(i, j)) {
                // printf("(%d, %d)\n", i, j);
                ufs.connect(i, j);
                if (ufs.find(0) == ufs.find(n + 1)) return true;
            }
        }
    }
    return false;
}

int main()
{
    int Tcases = read<int>();
    while (Tcases--) {
        fast >> n >> h >> r;
        for (int i = 1; i <= n; i++) {
            fast >> a[i].x >> a[i].y >> a[i].z;
        }
        fast << (solve() ? "Yes\n" : "No\n");
    }
    return 0;
}