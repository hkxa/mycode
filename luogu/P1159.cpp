/*************************************
 * problem:      id_name.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-mm-dd.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define puts_return(x) { puts(x); return 0; }
#define write_return(x) { write(x); return 0; }

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;
struct song {
    char name[101];
    int l, r;
    bool operator < (const song &b) const { return r > b.r; }
} a[100];
priority_queue<song> pqs;
char buf[5];

bool song_sortCmp(const song &a, const song &b) 
{
    return a.l < b.l;
}

int main()
{
    n = read<int>();
    for (int i = 0; i < n; i++) {
        scanf("%s%s", a[i].name, buf);
        switch (buf[0]) {
            case 'U' :
                // UP
                a[i].l = i + 1;
                a[i].r = n - 1;
                break;
            case 'D' :
                // DOWN
                a[i].l = 0;
                a[i].r = i - 1;
                break;
            case 'S' :
                // SAME
                a[i].l = i;
                a[i].r = i;
                break;
        }
        // printf("last range is [%d, %d]\n", a[i].l, a[i].r);
    }
    sort(a, a + n, song_sortCmp);
    for (int i = 0, pos = 0; i < n; i++) {
        while (pos < n && a[pos].l <= i) pqs.push(a[pos++]);
        puts(pqs.top().name);
        pqs.pop();
    }
    return 0;
}