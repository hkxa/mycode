/*************************************
 * @contest:      【LGR-069】洛谷 2 月月赛 II & EE Round 2 Div 2.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-02-15.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

uint64 a;

/*
给定一个整数 nn，问有多少个长度为 nn 的字符串，满足这个字符串是一个“程序片段”。
具体定义如下：
单个分号 ; 是一个“语句”。
空串 是一个“程序片段”。
如果字符串 A 是“程序片段”，字符串 B 是“语句”，则 AB 是“程序片段”。
如果字符串 A 是“程序片段”，则 {A} 是“语句块”。
如果字符串 A 是“语句块”，则 A 是“语句”，[]A 和 []()A 都是“函数”。
如果字符串 A 是“函数”，则 (A) 是“函数”，A 和 A() 都是“值”。
如果字符串 A 是“值”，则 (A) 是“值”，A; 是“语句”。
注意：A 是 B 并不代表 B 是 A。
*/
int n;
uint64 yj[10007], cxpd[10007], yjk[10007], val[10007], fun[10007];

int main()
{
    n = read<int>();
    yj[1] = 1;
    cxpd[0] = cxpd[1] = 1;
    // printf("%d : yj : %llu; cxpd : %llu; yjk : %llu; val : %llu; fun : %llu.\n", 0, yj[0], cxpd[0], yjk[0], val[0], fun[0]);
    // printf("%d : yj : %llu; cxpd : %llu; yjk : %llu; val : %llu; fun : %llu.\n", 1, yj[1], cxpd[1], yjk[1], val[1], fun[1]);
    for (int i = 2; i <= n; i++) {
        yjk[i] = cxpd[i - 2];
        fun[i] = fun[i - 2] + yjk[i - 2]; // 改成这一句加上下一个“注释句”也可AC
        fun[i] = fun[i - 2] + yjk[i - 2] + yjk[i - 4];
        // val[i] = fun[i] + val[i - 2]; // 改成这一句加上上一个“注释句”也可AC
        val[i] = fun[i] + fun[i - 2] + val[i - 2];
        yj[i] = yjk[i] + val[i - 1];
        for (int j = 0; j < i; j++) {
            cxpd[i] += cxpd[j] * yj[i - j];
        }
        // printf("%d : yj : %llu; cxpd : %llu; yjk : %llu; val : %llu; fun : %llu.\n", i, yj[i], cxpd[i], yjk[i], val[i], fun[i]);
    }
    write(cxpd[n], 10);
    return 0;
}

/*
[](){}
[]{;;}
[]{{}}
([]{})
*/