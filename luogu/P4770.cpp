/*************************************
 * @problem:      [NOI2018]你的名字.
 * @user_name:    brealid/hkxadpall/zhaoyi20/j(o)moo/littleTortoise.
 * @time:         2020-09-13.
 * @language:     C++.
 * @fastio_ver:   20200827.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        // simple io flags
        ignore_int = 1 << 0,    // input
        char_enter = 1 << 1,    // output
        flush_stdout = 1 << 2,  // output
        flush_stderr = 1 << 3,  // output
        // combination io flags
        endline = char_enter | flush_stdout // output
    };
    enum number_type_flags {
        // simple io flags
        output_double_stable = 1 << 0,  // output
        output_double_faster = 1 << 1   // output
        // combination io flags
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f & ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore character set : ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (double &lf) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            lf = endch & 15;
            while (isdigit(endch = getchar())) lf = lf * 10 + (endch & 15);
            if (endch == '.') {
                double digit = 0.1;
                while (isdigit(endch = getchar())) {
                    lf += (endch & 15) * digit;
                    digit *= 0.1;
                }
            }
            if (flag) lf = -lf;
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            Int d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return d;
        }
    } read;

    struct Writer {
        Writer& operator << (io_flags f) {
            if (f & char_enter) putchar(10);
            if (f & flush_stdout) fflush(stdout);
            if (f & flush_stderr) fflush(stderr);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
        inline void operator () (double val, int eps_digit = 6, char endch = '\0', number_type_flags flg = output_double_faster) {
            if (flg & output_double_stable) {
                static char output_format[10];
                sprintf(output_format, "%%.%dlf", eps_digit);
                printf(output_format, val);
            } else if (flg & output_double_faster) {
                if (val < 0) {
                    putchar('-');
                    val = -val;
                }
                double eps_number = 0.5;
                for (int i = 1; i <= eps_digit; i++) eps_number /= 10;
                val += eps_number;
                (*this) << (int64)val;
                val -= (int64)val;
                putchar('.');
                while (eps_digit--) {
                    val *= 10;
                    putchar((int)val | '0');
                    val -= (int)val;
                }
            } else {
                (*this) << "1.#ERROR_NOT_IDENTIFIED_FLAG";
            }
            if (endch) putchar(endch);
        }
    } write;
}
using namespace Fastio;
namespace File_IO {
    void init_IO() {
        freopen("name.in", "r", stdin);
        freopen("name.out", "w", stdout);
    }
}

// #define int int64
#define idx(ch) ((ch) - 'a')

const int N = 1e6 + 7;
int m, n;
int book[N], rt[N];
char str[N];
int64 ans;
struct tr_node {
    int l, r, sumv;
} t[N << 6];
int tr_node_cnt;

size_t getStrLen(const char *st) {
    const char *ed = st;
    while (*ed) ++ed;
    return ed - st;
}

void modify(int &u, int p, int l, int r) {
    t[u = ++tr_node_cnt].sumv = 1;
    if (l == r) return;
    int mid = (l + r) >> 1;
    if (p <= mid) modify(t[u].l, p, l, mid);
    else modify(t[u].r, p, mid + 1, r);
}

bool query(int u, int l, int r, int ml, int mr) {
    if (!u || l > mr || r < ml) return 0;
    if (l >= ml && r <= mr) return t[u].sumv;
    int mid = (l + r) >> 1;
    return query(t[u].l, l, mid, ml, mr) || query(t[u].r, mid + 1, r, ml, mr);
}

void merge(int &u, int p1, int p2) {
    if (!p1 || !p2) u = p1 | p2;
    else {
        u = ++tr_node_cnt;
        t[u].sumv = t[p1].sumv + t[p2].sumv;
        merge(t[u].l, t[p1].l, t[p2].l);
        merge(t[u].r, t[p1].r, t[p2].r);
    }
}

struct SAM {
    int ch[N][26], len[N], parent_link[N], sam_node_cnt;
    // int fa[N][20];
    int f[N], where[N];
    int last_expand_node;
    int alloc_node(int x) {
        len[++sam_node_cnt] = x;
        memset(ch[sam_node_cnt], 0, sizeof(ch[sam_node_cnt]));
        parent_link[sam_node_cnt] = f[sam_node_cnt] = 0;
        return sam_node_cnt;
    }
    void clear() {
        sam_node_cnt = 0;
        last_expand_node = alloc_node(0);
    }
    void expand_next_char(int c, int p) {
        int v = last_expand_node, u = alloc_node(len[v] + 1);
        last_expand_node = u;
        while (!ch[v][c]) {
            ch[v][c] = u;
            v = parent_link[v];
        }
        if (!v) parent_link[u] = 1;
        else {
            int x = ch[v][c];
            if (len[v] + 1 == len[x])
                parent_link[u] = x;
            else {
                int y = alloc_node(len[v] + 1);
                memcpy(ch[y], ch[x], sizeof(ch[y]));
                parent_link[y] = parent_link[x];
                parent_link[x] = parent_link[u] = y;
                while (ch[v][c] == x) {
                    ch[v][c] = y;
                    v = parent_link[v];
                }
            }
        }
        if (p) modify(rt[u], where[u] = p, 1, n);
    }
    void treat() {
        static int count_occur[N] = {0}, STRrank[N] = {0};
        for (int i = 1; i <= sam_node_cnt; i++) count_occur[len[i]]++;
        for (int i = 1; i <= n; i++) count_occur[i] += count_occur[i - 1];
        for (int i = 1; i <= sam_node_cnt; i++) STRrank[count_occur[len[i]]--] = i;
        for (int i = sam_node_cnt; i >= 1; i--) {
            int x = STRrank[i];
            merge(rt[parent_link[x]], rt[x], rt[parent_link[x]]);
        }
    }
    // vector<int> parent_tree_sons[N];
    // void Tdfs(int u) {
    //     for (int i = 0; i < 19; i++) fa[u][i + 1] = fa[fa[u][i]][i];
    //     for (size_t i = 0; i < parent_tree_sons[u].size(); i++)
    //         Tdfs(parent_tree_sons[u][i]);
    // }
    void initT() {
        for (int i = 1; i <= sam_node_cnt; i++) {
            f[i] = len[i];
            // fa[i][0] = parent_link[i];
            // parent_tree_sons[parent_link[i]].push_back(i);
        }
        // Tdfs(0);
    }
} S, T;

signed main() {
    S.clear();
    scanf("%s", str + 1);
    n = getStrLen(str + 1);
    for (int i = 1; i <= n; i++) S.expand_next_char(idx(str[i]), i);
    S.treat();
    read >> m;
    for (int i = 1, l, r; i <= m; i++) {
        scanf("%s", str + 1);
        read >> l >> r;
        ans = 0;
        int len = getStrLen(str + 1), tot = 0;
        T.clear();
        for (int j = 1; j <= len; j++)
            T.expand_next_char(idx(str[j]), 0);
        // printf("FILE %s Passing line %d\n", __FILE__, __LINE__);
        T.initT();
        // printf("FILE %s Passing line %d\n", __FILE__, __LINE__);
        for (int j = 1, u = 1, v = 1; j <= len; j++) {
            int c = idx(str[j]);
            v = T.ch[v][c];
            if (S.ch[u][c] && query(rt[S.ch[u][c]], 1, n, l + tot, r)) {
                u = S.ch[u][c];
                ++tot;
            } else {
                while (u && !(S.ch[u][c] && query(rt[S.ch[u][c]], 1, n, l + tot, r))) {
                    if (!tot) {
                        u = 0;
                        break;
                    }
                    if (--tot == S.len[S.parent_link[u]])
                        u = S.parent_link[u];
                }
                if (!u) tot = 0, u = 1;
                else ++tot, u = S.ch[u][c];
            }
            for (int x = v; x; x = T.parent_link[x]) 
                if (tot <= T.len[T.parent_link[x]]) {
                    ans += T.f[x] - T.len[T.parent_link[x]];
                    T.f[x] = T.len[T.parent_link[x]];
                } else {
                    ans += T.f[x] - tot;
                    T.f[x] = tot;
                    break;
                }
        }
        write << ans << '\n';
    }
    return 0;
}