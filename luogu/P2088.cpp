// luogu-judger-enable-o2
/*************************************
 * problem:      P2088 果汁店的难题.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-21.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define puts_return(x) { puts(x); return 0; }
#define write_return(x) { write(x); return 0; }

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

struct Toy {
    int ToyId, nextPlayTime;
    Toy(int mi, int nqi) : ToyId(mi), nextPlayTime(nqi) {}
    bool operator < (const Toy &otherMemory) const
    {
        return nextPlayTime < otherMemory.nextPlayTime;
    }
};


priority_queue<Toy> floorToy;
int catchToyTimes = 0, floorToyCount = 0;
const int m = read<int>(), n = read<int>();
map<int, int> nextCatchToy_Map;
map<int, int> onFloor;
int nextQuery[500001], query[500001];

int main()
{
    for (int i = 1; i <= n; i++) {
        query[i] = read<int>();
    }
    for (int i = n; i > 0; i--) {
        if (nextCatchToy_Map[query[i]] != 0) nextQuery[i] = nextCatchToy_Map[query[i]];
        else nextQuery[i] = n + 1;
        nextCatchToy_Map[query[i]] = i;
    }
    for (int i = 1; i <= n; i++) {
        if (onFloor[query[i]] != i) {
            // printf("[Cache Miss] query #%d : ", i);
            if (floorToyCount == m) {
                while (onFloor[floorToy.top().ToyId] != floorToy.top().nextPlayTime) floorToy.pop();
                onFloor[floorToy.top().ToyId] = 0;
                // printf("pop %d ", floorToy.top().ToyId);
                floorToy.pop();
                floorToyCount--;
                catchToyTimes++;
            } 
            // printf("push %d\n", query[i]);
            floorToyCount++;
        }
        floorToy.push(Toy(query[i], nextQuery[i]));
        onFloor[query[i]] = nextQuery[i];
    }
    write(catchToyTimes, 10);
    return 0;
}