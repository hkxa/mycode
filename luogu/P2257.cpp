/*************************************
 * @problem:      YY的GCD.
 * @user_name:    brealid.
 * @time:         2020-11-11.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

const int N = 1e7 + 7;

bool isnp[N];
int primes[N], pcnt;
int mu[N], g[N];
int64 sum[N];

void init(int n) {
    mu[1] = 1;
    for (int i = 2; i <= n; ++i) {
        if (!isnp[i]) primes[++pcnt] = i, mu[i] = -1;
        for (int j = 1; j <= pcnt && i * primes[j] <= n; ++j) {
            isnp[i * primes[j]] = true;
            if (i % primes[j] == 0) break;
            mu[i * primes[j]] = -mu[i];
        }
    }
    for (int i = 1; i <= pcnt; ++i)
        for (int j = 1, p = primes[i]; j * p <= n; ++j)
            g[j * p] += mu[j];
    for (int i = 1; i <= n; ++i) sum[i] = sum[i - 1] + g[i];
}

int T, n, m;

signed main() {
    init(1e7);
    for (scanf("%d", &T); T --> 0;) {
        scanf("%d%d", &n, &m);
        int64 ans = 0;
        for (int l = 1, r; l <= min(n, m); l = r + 1) {
            r = min(n / (n / l), m / (m / l));
            ans += (int64)(sum[r] - sum[l - 1]) * (n / l) * (m / l);
        }
        printf("%lld\n", ans);
    }
    return 0;
}