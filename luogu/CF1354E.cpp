// #pragma GCC optimize(1)
// #pragma GCC optimize(2)
// #pragma GCC optimize(3)

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, m, n1, n2, n3;
vector<int> G[5007];
int vis[5007];

int belong[5007];
int odd[5007], even[5007], tot = 0;
int f[5007][5007];
bool rev[5007];

bool dfs(int u, int col) 
{
    vis[u] = col;
    if (col & 1) odd[tot]++;
    else even[tot]++;
    belong[u] = tot;
    for (unsigned i = 0; i < G[u].size(); i++) {
        if (!vis[G[u][i]]) {
            if (dfs(G[u][i], 3 - col)) return true;
        } else if (vis[G[u][i]] == vis[u]) return true;
    }
    return false;
}

inline int calc_base(int u) {
    if (rev[belong[u]]) return 3 - vis[u];
    else return vis[u];
}

inline int calc(int u) {
    if (calc_base(u) == 1) {
        if (n1) {
            n1--;
            return 1;
        } else return 3;
    } else return 2;
}

int main() {
    cin >> n >> m >> n1 >> n2 >> n3;
    for (int i = 1, u, v; i <= m; i++) {
        u = read<int>();
        v = read<int>();
        G[u].push_back(v);
        G[v].push_back(u);
    }
    for (int i = 1; i <= n; i++) {
        if (!vis[i]) {
            // odd = even = 0;
            tot++;
            if (dfs(i, 1)) {
                puts("NO");
                return 0;
            }
        }
    }
    f[0][0] = 1;
    for (int i = 1; i <= tot; i++) {
        for (int j = 0; j <= n2; j++) {
            if (f[i - 1][j]) {
                f[i][j + odd[i]] = 1;
                f[i][j + even[i]] = 2;
            }
        }
    }
    if (!f[tot][n2]) {
        puts("NO");
        return 0;
    }
    puts("YES");
    for (int i = tot, p = n2; i >= 1; i--) {
        if (f[i][p] == 1) {
            rev[i] = true;
            p -= odd[i];
        } else {
            p -= even[i];
        }
    }
    for (int i = 1; i <= n; i++)
    putchar(calc(i) + '0');
    putchar(10);
    return 0;
}