#include <stdio.h>
using namespace std;

double a, b, f;

void input()
{
    scanf("%lf%lf", &a, &b);
}

void printAns(double ans)
{
    printf("%.10lf\n", ans);
}

void play()
{
    input();
    f = b * 4;
    if (b == 0) printAns(1);
    else if (a == 0) printAns(0.5);
    else if (a >= f) printAns((a - b) / a);
    else printAns((a * 0.25 + f * 0.5) / f);
}

int main()
{
    int test;
    scanf("%d", &test);
    while (test--) play();
    return 0;
}