/*************************************
 * @contest:      Graph Subset Problem.
 * @author:       hkxadpall.
 * @time:         2020-11-17.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#if true /* 需要使用 fread 优化，改此参数为 true */
#define getchar() (i_P1 == i_P2 && (i_P2 = (i_P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), i_P1 == i_P2) ? EOF : *i_P1++)
char BUF[1 << 21], *i_P1 = BUF, *i_P2 = BUF;
#endif

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 1e5 + 7;

int T, n, m, k;
unordered_set<int> G[N];

template <typename T, typename Container = vector<T>, typename Comparer = less<T> >
class heap {
  private:
    priority_queue<T, Container, Comparer> que, del; 
    inline void push_del_tags() {
        while (!del.empty() && !que.empty() && del.top() == que.top())
            del.pop(), que.pop();
    }
  public:
    inline size_t size() {
        return que.size() - del.size();
    }
    inline bool empty() {
        return que.size() == del.size();
    }
    inline T top() {
        push_del_tags();
        return que.top();
    }
    inline void pop() {
        push_del_tags();
        que.pop();
    }
    inline void insert(const T &x) { que.push(x); }
    inline void erase(const T &x) { del.push(x); }
    inline void swap(heap &x) {
        que.swap(x.que);
        del.swap(x.del);
    }
};

signed main() {
    for (kin >> T; T--;) {
        kin >> n >> m >> k;
        if (((int64)k * (k - 1) >> 1) > m) {
            while (m--) kin.get<int>(), kin.get<int>();
            kout << "-1\n";
            continue;
        }
        for (int i = 1; i <= n; ++i) G[i].clear();
        for (int i = 1, u, v; i <= m; ++i) {
            kin >> u >> v;
            G[u].insert(v);
            G[v].insert(u);
        }
        heap<pair<int, int>, vector<pair<int, int> >, greater<pair<int, int> > > q;
        for (int i = 1; i <= n; ++i) q.insert(make_pair(G[i].size(), i));
        bool has_ans = 0;
        while (!q.empty()) {
            int u = q.top().second;
            q.pop();
            if (!G[u].size()) continue;
            if ((int)G[u].size() >= k) {
                kout << "1 " << q.size() + 1 << '\n' << u << ' ';
                while (!q.empty()) {
                    kout << q.top().second << ' ';
                    q.pop();
                }
                kout << '\n';
                has_ans = 1;
                break;
            }
            if ((int)G[u].size() == k - 1) {
                bool failed = false;
                for (const int &v : G[u]) {
                    for (const int &p : G[u])
                        if (v != p && G[v].find(p) == G[v].end()) {
                            failed = true;
                            break;
                        }
                    if (failed) break;
                }
                if (!failed) {
                    has_ans = 1;
                    kout << "2\n" << u << ' ';
                    for (const int &v : G[u]) 
                        kout << v << ' ';
                    kout << '\n';
                    has_ans = 1;
                    break;
                }
            }
            for (const int &v : G[u]) {
                q.erase(make_pair(G[v].size(), v));
                G[v].erase(u);
                q.insert(make_pair(G[v].size(), v));
            }
        }
        if (!has_ans) kout << "-1\n";
    }
    return 0;
}