/*************************************
 * @problem:      [AGC027D] Modulo Matrix.
 * @author:       brealid.
 * @time:         2021-02-01.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

int isnp[7500];

vector<int> get_primes(int require) {
    vector<int> v;
    for (int i = 2; (int)v.size() < require; ++i) {
        if (!isnp[i]) {
            v.push_back(i);
            for (int j = i << 1; j < 7500; j += i)
                isnp[j] = true;
        }
    }
    return v;
}

int n;
vector<int> p;

int64 get(int x, int y) {
    if ((x + y) & 1) {
        if (y > 1 && y < n) return get(x, y - 1) * get(x, y + 1) + 1;
        if (x > 1 && x < n) return get(x - 1, y) * get(x + 1, y) + 1;
        if (x == 1 && y == n) return get(1, n - 1) * get(2, n) + 1;
        if (x == n && y == 1) return get(n - 1, 1) * get(n, 2) + 1;
        return -1;
    } else {
        int lu_rd_id = (n + x - y + 1) >> 1;
        int ru_ld_id = ((n - 1) | 1) + ((x + y) >> 1);
        // printf("get(%d, %d) need: p[%d] & p[%d]\n", x, y, lu_rd_id - 1, ru_ld_id - 1);
        return (int64)p[lu_rd_id - 1] * p[ru_ld_id - 1];
    }
}

signed main() {
    kin >> n;
    if (n == 2) {
        kout << "4 7\n23 10\n";
        return 0;
    }
    p = get_primes(2 * n + 1);
    int64 mzx = 0;
    for (int i = 1; i <= n; ++i)
        for (int j = 1; j <= n; ++j)
            mzx = max(mzx, get(i, j));
    for (int i = 1; i <= n; ++i)
        for (int j = 1; j <= n; ++j)
            kout << get(i, j) << " \n"[j == n];
    return 0;
}