/*************************************
 * @problem:      CF1917D.
 * @author:       CharmingLakesideJewel.
 * @time:         2023-12-24.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

// #define USE_FREAD  // 使用 fread  读入，去注释符号
// #define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 2e5 + 5, P = 998244353;

int T, n, k;

namespace nxd {
    int binarytr[N], _n;
    void init(int n) {
        _n = n;
        for (int i = 1; i <= n; ++i) binarytr[i] = 0;
    }
    void add(int u, int v) {
        for (; u <= _n; u += u & -u) binarytr[u] += v;
    }
    int query(int u) {
        int res = 0;
        for (; u; u -= u & -u) res += binarytr[u];
        return res;
    }
    int64 query(int n, int *a) {
        init(n);
        int64 res = 0;
        for (int i = n; i; --i) {
            res += query(a[i]);
            add(a[i] + 1, 1);
        }
        return res % P;
    }
}

namespace supernxd {
    int binarytr[N * 2], _n, _k;
    void init(int n, int k) {
        _n = n * 2;
        _k = k;
        for (int i = 1; i <= _n; ++i) binarytr[i] = 0;
    }
    void add(int u, int v) {
        for (; u <= _n; u += u & -u) binarytr[u] += v;
    }
    int query(int u) {
        int res = 0;
        for (; u; u -= u & -u) res += binarytr[u];
        return res;
    }
    int query(int l, int r) {
        return query(r) - query(l - 1);
    }
    int64 calc_offset(int64 offset) {
        if (offset < 0) {
            if (offset < -_k) return 0;
            return (_k + offset) * (_k + offset + 1) / 2 % P;
        } else {
            if (offset > _k) return (int64)_k * _k % P;
            return ((int64)_k * _k - (_k - offset) * (_k - offset + 1) / 2) % P;
        }
    }
    int64 query(int n, int k, int *a) {
        init(n, k);
        int64 res = 0;
        for (int i = 1; i <= n; ++i) {
            // lower part
            int rig = a[i], lef = rig / 2, step = -1;
            while (lef < rig) {
                int count = query(lef + 1, rig);
                res += calc_offset(step) * count % P;
                rig = lef;
                lef = lef / 2;
                --step;
            }
            // upper part
            rig = a[i] * 2, lef = a[i], step = 1;
            while (lef < _n) {
                int count = query(lef + 1, min(rig, _n));
                res += calc_offset(step) * count % P;
                lef = rig;
                rig = rig * 2;
                ++step;
            }
            // add
            add(a[i], 1);
        }
        return res % P;
    }

}


int p[N], q[N];

signed main() {
    kin >> T;
    while (T--) {
        kin >> n >> k;
        for (int i = 1; i <= n; ++i) kin >> p[i];
        for (int i = 1; i <= k; ++i) kin >> q[i];
        int64 ans = (nxd::query(k, q) * n + supernxd::query(n, k, p)) % P;
        kout << ans << '\n';
    }
    return 0;
}