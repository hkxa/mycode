#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

#define isInt(a) ((a) == (int)(a))

int low(double f)
{
    int ret = f;
    return f < 0 ? ret - !isInt(f) : ret;
}

int mor(double f)
{
    int l = low(f);
    return l + !isInt(f);
}

int n;
double a[100007];
int lSum;

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        scanf("%lf", a + i);
        lSum += low(a[i]);
    }
    int toAdd = -lSum;
    for (int i = 1; i <= n; i++) {
        if (!toAdd) write(low(a[i]), 10);
        else write(mor(a[i]), 10), toAdd -= !isInt(a[i]);
    }
    return 0;
}