/*************************************
 * @problem:      P5675 [GZOI2017]取石子游戏.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-01-30.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;
int a[200 + 3];
int f[200 + 3][256 + 3];
int ans = 0;

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) a[i] = read<int>();
    for (int i = 1; i <= n; i++) {
        memset(f, 0, sizeof(f));
        f[0][0] = 1;
        for (int j = 1; j <= n; j++) {
            for (int k = 0; k < 256; k++) {
                if (i == j) f[j][k] = f[j - 1][k];
                else f[j][k] = (f[j - 1][k] + f[j - 1][k ^ a[j]]) % 1000000007;
                if (j == n && k >= a[i]) ans = (ans + f[n][k]) % 1000000007;
                // if (j == n && k >= a[i] && f[n][k]) printf("first get heap %d, with probem = %d : cases_count = %d\n", i, k, f[n][k]); 
            }
        }
    }
    write(ans, 10);
    return 0;
}