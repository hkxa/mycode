/*************************************
 * @problem:      P5851 [USACO19DEC]Greedy Pie Eaters.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-01-19.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define N_max 303
#define M_max 45003

int n, m;
int w[M_max], l[M_max], r[M_max];
int mx[N_max][N_max][N_max], f[N_max][N_max];

template <typename I, typename J> I UpdMax(I &S, J F) 
{ return S < F ? S = F : S; }

int main()
{
	n = read<int>();
	m = read<int>();
	for (int i = 1; i <= m; i++) {
		w[i] = read<int>();
		l[i] = read<int>();
		r[i] = read<int>(); 
		for (int j = l[i]; j <= r[i]; j++) UpdMax(mx[j][l[i]][r[i]], w[i]);
	}
	for (int i = 1; i <= n; i++) {
		for (int j = n; j >= 1; j--) {
			for (int k = i; k <= n; k++)  {
				if (j > 1) UpdMax(mx[i][j - 1][k], mx[i][j][k]);
				if (k < n) UpdMax(mx[i][j][k + 1], mx[i][j][k]);
			}
		} 
	}
	for (int i = n; i >= 1; i--) {
		for (int j = i; j <= n; j++) {
			for (int k = i; k <= j - 1; k++) {
				UpdMax(f[i][j], f[i][k] + f[k + 1][j]);
				UpdMax(f[i][j], mx[k][i][j] + (k > i ? f[i][k - 1] : 0)
											+ (k < j ? f[k + 1][j] : 0));
			}
			UpdMax(f[i][j], mx[j][i][j] + f[i][j - 1]);
		}
	}
	write(f[1][n]);
    return 0;
}
