def solve():
    n = int(input())
    a = list(map(int, input().split()))
    b = list(map(int, input().split()))
    a[0] = -1
    a_p1 = [a[i] + i for i in range(n)] + [-2000000000]
    a_m1 = [a[i] - i for i in range(n)] + [-2000000000]
    b_p1 = [b[i] + i for i in range(n)] + [-2000000000]
    b_m1 = [b[i] - i for i in range(n)] + [-2000000000]
    for i in range(n - 2, -1, -1):
        a_p1[i] = max(a_p1[i], a_p1[i + 1])
        a_m1[i] = max(a_m1[i], a_m1[i + 1])
        b_p1[i] = max(b_p1[i], b_p1[i + 1])
        b_m1[i] = max(b_m1[i], b_m1[i + 1])
    preCost = 0
    ans = max(b_p1[0], a_m1[0] + 2 * n - 1)
    # print(-1, ans)
    for i in range(0, n, 2):
        preCost = max(preCost, a[i] + (n - i) * 2 - 1, b[i] + (n - i) * 2 - 2)
        ans = min(ans, max(preCost, a_p1[i + 1] - (i + 1), b_m1[i + 1] + 2 * n - i - 2))
        # print(i, max(preCost, a_p1[i + 1] - (i + 1), b_m1[i + 1] + 2 * n - i - 2))
        if i == n - 1:
            break
        preCost = max(preCost, a[i + 1] + (n - i) * 2 - 4, b[i + 1] + (n - i) * 2 - 3)
        ans = min(ans, max(preCost, b_p1[i + 2] - (i + 2), a_m1[i + 2] + 2 * n - i - 3))
        # print(i + 1, max(preCost, b_p1[i + 2] - (i + 2), a_m1[i + 2] + 2 * n - i - 3))
    print(ans + 1)

T = int(input())
while T:
    solve()
    T -= 1