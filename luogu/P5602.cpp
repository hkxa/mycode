#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;
int a[300007];
long long tot = 0;
double ans = 0;

bool cmpBigger(int a, int b) { return a > b; }

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        a[i] = read<int>();
    }
    sort(a + 1, a + n + 1, cmpBigger);
    tot = a[1];
    ans = (long long)a[1] * a[1];
    for (int i = 2; i <= n; i++) {
        tot += a[i];
        if ((double)tot / i * tot > ans) {
            ans = (double)tot / i * tot;
        }
    }
    printf("%.8lf", ans);
    return 0;
}