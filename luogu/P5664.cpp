#include <bits/stdc++.h>
using namespace std; 
const int Mx_n = 100 + 3, Mx_m = 2000 + 3, P = 998244353;

template <typename ret_type> ret_type read() {
    bool flg = 0; ret_type ret_num = 0; char ch = getchar();
    while (!isdigit(ch) && ch != '-') ch = getchar(); 
    if (ch == '-') { flg = 1; ch = getchar(); }
    while (isdigit(ch)) { ret_num = (((ret_num << 2) + ret_num) << 1) + (ch & 15); ch = getchar(); } 
    return flg ? (~ret_num + 1) : ret_num;
}

int n, m, a[Mx_n][Mx_m];
int sum[Mx_n] = {0};
long long f[Mx_n][Mx_m] = {0}, g[Mx_n][Mx_n] = {0};

int all_situation()
{
    int res = 0;
    g[0][0] = 1;
    for (int i = 1; i <= n; i++) {
        g[i][0] = 1;
        for (int j = 1; j <= i; j++) {
            g[i][j] = (g[i - 1][j] + g[i - 1][j - 1] * sum[i]) % P;
            // printf("g[%d][%d] = %d\n", i, j, g[i][j]);
        }
    }
    // printf("------------------------------\n");
    for (int i = 1; i <= n; i++) {
        res = (res + g[n][i]) % P;
        // printf("g[n][%d] = %d\n", i, g[n][i]);
    }
    // printf("all_situation : res = %d\n", res);
    return res;
}

int work_column(int col)
{
    int res = 0;
    f[0][n] = 1;
    for (int i = 1; i <= n; i++) {
        for (int j = n - i; j <= n + i; j++) {
            f[i][j] = (f[i - 1][j] + a[i][col] * f[i - 1][j - 1] + (sum[i] - a[i][col]) * f[i - 1][j + 1]) % P;
        }
    }
    for (int i = 1; i <= n; i++) res = (res + f[n][n + i]) % P;
    // printf("work_column(column = %d) : res = %d\n", col, res);
    return res;
}

int incorrect_range()
{
    int res = 0;
    for (int col = 1; col <= m; col++) {
        res = (res + work_column(col)) % P;
    }
    // printf("incorrect_range : res = %d\n", res);
    return res;
}

int main()
{
    n = read<int>();
    m = read<int>();
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++) {
            a[i][j] = read<int>();
            sum[i] = (sum[i] + a[i][j]) % P;
        }
    }
    printf("%d", (all_situation() - incorrect_range() + P) % P);
    return 0;
}