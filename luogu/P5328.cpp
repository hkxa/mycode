//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      P5328 [ZJOI2019]浙江省选.
 * @user_id:      63720.
 * @user_name:    brealid.
 * @time:         2020-05-22.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;
typedef __int128             int128;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int N = 1e5 + 5;
const int64 int64_Max = 0x3f3f3f3f3f3f;

struct Fraction {
    int64 a, b;

    Fraction() {}
    Fraction(int64 x, int64 y) : a(x), b(y) {
        if (b < 0) {
            b = -b;
            a = -a;
        }
    }

    bool operator < (Fraction x) const { 
        return ((int128)a) * x.b < ((int128)b) * x.a; 
    }
    bool operator <= (Fraction x) const { 
        return ((int128)a) * x.b <= ((int128)b) * x.a; 
    }
    bool operator == (Fraction x) const { 
        return ((int128)a) * x.b == ((int128)b) * x.a; 
    }

    int64 floor() { 
        return a < 0 ? ((a - b + 1) / b) : (a / b); 
    }
    int64 ceil() { 
        return a < 0 ? (a / b) : ((a + b - 1) / b); 
    }
};

struct Line {
    int64 a, b; int id;
    Fraction intersect(Line x) {
        return Fraction(x.b - b, a - x.a);
    }
};

struct Event {
    int type, v; 
    Fraction p;
    Event() {}
    Event(int type, int v, Fraction p) : type(type), v(v), p(p) {}
    bool operator < (Event b) const { 
        return p == b.p ? (type == b.type ? v > b.v : type < b.type) : p < b.p; 
    }
};

int n, m;
Line li[N];
Fraction fl[N], fr[N];

bool ban[N];
int ans[N];
Line stk[N];
vector<Event> E;
vector<int> ncand;
vector<int> candidate;

const Fraction fZero(0, 1);

int main() {
    n = read<int>();
    m = read<int>();
    for (int i = 1; i <= n; i++) {
        li[i].a = read<int>();
        li[i].b = read<int64>();
        li[i].id = i;
        ans[i] = -1;
    }
    sort(li + 1, li + n + 1, [&](Line a, Line b) { return a.a == b.a ? a.b > b.b : a.a < b.a; });
    for (int r = 1; r <= m; r++) {
        int tp = 0;
        int64 lsa = 0;
        fill(ban + 1, ban + n + 1, false);
        for (int i = 1; i <= n; i++) {
            if (ans[li[i].id] == -1 && li[i].a != lsa) {
                lsa = li[i].a;
                while (tp >= 2 && stk[tp].intersect(li[i]) < stk[tp - 1].intersect(stk[tp])) tp--;
                stk[++tp] = li[i];
            } else if (li[i].a == lsa) {
                ban[i] = true;
            }
        }
        if (!tp) break;
        E.clear();
        int cov = 0;
        for (int i = 1; i <= n; i++) {
            if (ban[i]) continue;
            int l = 2, r = tp, mid;
            Line &lc = li[i];
            int id = li[i].id;
            while (l <= r) {
                mid = (l + r) >> 1;
                if (stk[mid].a < lc.a && stk[mid].intersect(stk[mid - 1]) < lc.intersect(stk[mid])) {
                    l = mid + 1;
                } else {
                    r = mid - 1;
                }
            }
            l--;
            if (stk[l].a >= lc.a) {
                fl[i] = Fraction(-int64_Max, 1);
            } else {
                fl[i] = stk[l].intersect(lc);
            }

            l = 1, r = tp - 1;
            while (l <= r) {
                mid = (l + r) >> 1;
                if (stk[mid].a > lc.a && lc.intersect(stk[mid]) < stk[mid].intersect(stk[mid + 1])) {
                    r = mid - 1;
                } else {
                    l = mid + 1;
                }
            }
            r++;
            if (stk[r].a <= lc.a) {
                fr[i] = Fraction(int64_Max, 1);
            } else {
                fr[i] = lc.intersect(stk[r]);
            }
            if (fr[i] < fl[i] || fr[i] < fZero) {
                continue;
            }
            if (ans[id] != -1) {
                if (fl[i] < fZero) {
                    cov++;
                } else {
                    E.emplace_back(1, 1, fl[i]);
                }
                if (fr[i].a != int64_Max) {
                    E.emplace_back(1, -1, fr[i]);
                }
            } else {
                E.emplace_back(0, i, fl[i]);
            }
        }
        sort(E.begin(), E.end());
        Fraction lsf = fZero;
        bool have_first = cov < r;
        for (auto it = E.begin(), nit = it, _it = E.end(); it != _it; it = nit) {
            while (nit != _it && (*nit).p == (*it).p) {
                auto e = *nit;
                if (!e.type) {
                    candidate.push_back(e.v);
                } else {
                    cov += e.v;
                }
                nit++;
            }
            auto p = (*it).p;
            if (cov < r) {
                if (!have_first) {
                    have_first = true;
                    lsf = p;
                }
            } else if (have_first) {
                if (p.floor() >= lsf.ceil()) {
                    ncand.clear();
                    for (auto x : candidate) {
                        auto L = max(fl[x], lsf);
                        auto R = min(fr[x], p);
                        if (L.ceil() <= R.floor()) {
                            ans[li[x].id] = r;
                        } else if (lsf < fl[x] && p <= fr[x]) {
                            ncand.push_back(x);
                        }
                    }
                    candidate = ncand;
                }
                have_first = false;
            }
        }
        if (cov < r) {
            for (auto x : candidate) {
                auto L = max(fl[x], lsf);
                auto R = fr[x];
                if (L.ceil() <= R.floor()) {
                    ans[li[x].id] = r;
                }
            }
        }
        candidate.clear();
    }
    for (int i = 1; i <= n; i++) {
        write(ans[i], i == n ? 10 : 32);
    }
    return 0;
}