/*************************************
 * @problem:      基于值域预处理的快速 GCD.
 * @author:       brealid.
 * @time:         2021-01-07.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;
#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 1e6 + 7, M = 1e3 + 5, P = 998244353;

int primes[N], pcnt;
bool isnp[N];
struct Tuple3 {
    int a, b, c;
    inline void set(const Tuple3 &f, const int &minp) {
        int t = f.a * minp;
        if (t < f.b) a = t, b = f.b, c = f.c;
        else if (t < f.c) a = f.b, b = t, c = f.c;
        else a = f.b, b = f.c, c = t;
    }
} factor[N];

void seive_primes(int Mx) {
    factor[1].a = factor[1].b = factor[1].c = 1;
    for (int i = 2; i <= Mx; ++i) {
        if (!isnp[i]) {
            primes[++pcnt] = i;
            factor[i].a = factor[i].b = 1;
            factor[i].c = i;
        }
        // if (!factor[i].a || !factor[i].b || !factor[i].c)
        //     printf("WARN 1: factor[%d] = {%d, %d, %d}\n", i, factor[i].a, factor[i].b, factor[i].c);
        // if (factor[i].a > factor[i].b || factor[i].b > factor[i].c)
        //     printf("WARN 2: factor[%d] = {%d, %d, %d}\n", i, factor[i].a, factor[i].b, factor[i].c);
        // if (factor[i].a * factor[i].b * factor[i].c != i)
        //     printf("WARN 3: factor[%d] = {%d, %d, %d}\n", i, factor[i].a, factor[i].b, factor[i].c);
        for (int j = 1; j <= pcnt && i * primes[j] <= Mx; ++j) {
            isnp[i * primes[j]] = true;
            factor[i * primes[j]].set(factor[i], primes[j]);
            if (i % primes[j] == 0) break;
        }
    }
}

int gcd_pre[M][M], Limit;
void seive_gcd(int Mx) {
    Limit = sqrt(Mx);
    for (int i = 0; i <= Limit; ++i) gcd_pre[i][0] = i;
    for (int i = 1; i <= Limit; ++i)
        for (int j = 1; j <= i; ++j)
            gcd_pre[i][j] = gcd_pre[j][i % j];
}

int gcd(int x, int y) {
    // fprintf(stderr, "Query gcd(%d, %d): Division of %d is %d * %d * %d\n", x, y, x, factor[x].a, factor[x].b, factor[x].c);
    int result_a = gcd_pre[factor[x].a][y % factor[x].a];
    y /= result_a;
    int result_b = gcd_pre[factor[x].b][y % factor[x].b];
    y /= result_b;
    if (factor[x].c <= Limit) return result_a * result_b * gcd_pre[factor[x].c][y % factor[x].c];
    else return result_a * result_b * (y % factor[x].c == 0 ? factor[x].c : 1);
}

int n, a[5007], b[5007];

int64 query(int i) {
    int64 prod = 1, sum = 0;
    for (int j = 1; j <= n; ++j)
        sum += (prod = prod * i % P) * gcd(a[i], b[j]) % P;
    return sum % P;
}

int main() {
    kin >> n;
    int MXV = 1;
    for (int i = 1; i <= n; ++i) {
        kin >> a[i];
        MXV = max(MXV, a[i]);
    }
    for (int i = 1; i <= n; ++i) {
        kin >> b[i];
        MXV = max(MXV, b[i]);
    }
    seive_primes(MXV);
    seive_gcd(MXV);
    for (int i = 1; i <= n; ++i) kout << query(i) << '\n';
    return 0;
}