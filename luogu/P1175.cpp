/*************************************
 * @problem:      P1175 表达式的转换.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-01-26.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

inline int priority(const char ch) 
{
    switch (ch) {
        case '+': case '-': return 1;
        case '*': case '/': return 2;
        case '^': return 3;
        case '(': case ')': return 0;
    }
    return EOF;
}

inline string toSuffix(const string& s) 
{
    string ret = "";  
    stack<char> st;   
    for (register unsigned i = 0; i < s.size(); i++) {  
        if (isdigit(s[i])) ret += s[i];
        else if (s[i] == '(') st.push(s[i]);
        else if (s[i] == ')') {         
            while (st.top() != '(')     
                ret += st.top(), st.pop(); 
            st.pop();
        } else {                   
            while (!st.empty() && priority(st.top()) >= priority(s[i])) { 
                ret += st.top();
                st.pop();
            }
            st.push(s[i]);              
        }
    }
    while (!st.empty()) { 
        ret += st.top();
        st.pop();
    }
    return ret;              
}

inline int calcOper(const int a, const int b, const int symbol) 
{
    switch (symbol) {
        case '+': return a + b;
        case '-': return a - b;
        case '*': return a * b;
        case '/': return a / b;
        case '^': return (int)pow(a, b);
    }
    return EOF;
}

inline void calc(const string& s) 
{
    for (register unsigned i = 0; i < s.size(); i ++)
        printf("%c%c", s[i], " \n"[i == s.size() - 1]);
    list<int> st;
    register int a, b;
    for (register unsigned i = 0; i < s.size(); i++) {
        if (isdigit(s[i])) st.push_back(s[i] - '0');
        else {                
            a = st.back(); st.pop_back();
            b = st.back(); st.pop_back();
            st.push_back(calcOper(b, a, s[i])); 
            for (list<int>::iterator it = st.begin(); it != st.end(); it++) write(*it, 32);      
            for (register unsigned j = i + 1; j < s.size(); j++) printf("%c ", s[j]);      
            putchar(10);
        }
    }
}

int main() 
{
    string s;
    cin >> s;
    calc(toSuffix(s));
    return 0;
}