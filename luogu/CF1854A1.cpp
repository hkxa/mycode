/*************************************
 * @problem:      cf1854B.
 * @author:       brealid.
 * @time:         2023-07-29.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

// #define USE_FREAD  // 使用 fread  读入，去注释符号
// #define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 20 + 7;

int T, n, a[N];

void solve_positive(int pos) {
    int cnt = 0;
    for (int i = 1; i <= n; ++i)
        if (a[i] < 0) ++cnt;
    kout << n + cnt + 1 << '\n';
    kout << pos << ' ' << pos << '\n';
    kout << pos << ' ' << pos << '\n';
    kout << pos << ' ' << pos << '\n';
    kout << pos << ' ' << pos << '\n';
    kout << pos << ' ' << pos << '\n';
    kout << 1 << ' ' << pos << '\n';
    kout << 1 << ' ' << pos << '\n';
    kout << 1 << ' ' << pos << '\n';
    kout << 1 << ' ' << 1 << '\n';
    kout << 1 << ' ' << 1 << '\n';
    kout << 1 << ' ' << 1 << '\n';
    kout << 1 << ' ' << 1 << '\n';
    for (int i = 2; i <= n; ++i) {
        kout << i << ' ' << i - 1 << '\n';
        kout << i << ' ' << i - 1 << '\n';
    }
}

void solve_negative(int pos) {
    kout << 2 * n + 10 << '\n';
    kout << pos << ' ' << pos << '\n';
    kout << pos << ' ' << pos << '\n';
    kout << pos << ' ' << pos << '\n';
    kout << pos << ' ' << pos << '\n';
    kout << pos << ' ' << pos << '\n';
    kout << n << ' ' << pos << '\n';
    kout << n << ' ' << pos << '\n';
    kout << n << ' ' << pos << '\n';
    kout << n << ' ' << n << '\n';
    kout << n << ' ' << n << '\n';
    kout << n << ' ' << n << '\n';
    kout << n << ' ' << n << '\n';
    for (int i = n - 1; i >= 1; --i) {
        kout << i << ' ' << i + 1 << '\n';
        kout << i << ' ' << i + 1 << '\n';
    }
}

signed main() {
    for (kin >> T; T--;) {
        kin >> n;
        for (int i = 1; i <= n; ++i) {
            kin >> a[i];
        }
        int p_positive = -1, p_negatvie = -1;
        for (int i = 1; i <= n; ++i) {
            if (a[i] > 0 && (p_positive == -1 || a[i] > a[p_positive])) p_positive = i;
            if (a[i] < 0 && (p_negatvie == -1 || a[i] < a[p_negatvie])) p_negatvie = i;
        }
        if (p_positive != -1) {
            if (p_negatvie == -1 || a[p_negatvie] > -a[p_negatvie]) solve_positive(p_positive);
            else solve_negative(p_negatvie);
        } else {
            if (p_negatvie != -1) solve_negative(p_negatvie);
            else kout << "0\n";
        }
    }
    return 0;
}