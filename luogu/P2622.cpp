/*************************************
 * problem:      P2622 �ص�����II.
 * user ID:      63720.
 * user name:    �����Ű�.
 * time:         2019-02-24.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 
/*************************************
 * record ID:    R16627182.
 * status:       AC 100 pts.
 * time:         43 ms
 * memory:       932 KB
 * ststus - AC:  10 blocks, 100 pts.
 * ststus - PC:  0 blocks, 0 pts.
 * ststus - WA:  0 blocks, 0 pts.
 * ststus - RE:  0 blocks, 0 pts. 
 * ststus - TLE: 0 blocks, 0 pts. 
 * ststus - MLE: 0 blocks, 0 pts. 
 * ststus - OLE: 0 blocks, 0 pts. 
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

int n, m;
int a[108][18] = {0}, f[2048] = {0};

int main()
{
    n = read<int>();
    m = read<int>();
    memset(f, 0x3f, sizeof(f));
    for (int i = 1; i <= m; i++)
        for (int j = 1; j <= n; j++)
            a[i][j] = read<int>();
    f[(1 << n) - 1] = 0;
    for (int i = (1 << n) - 1; i >= 0; i--) {
        for (int j = 1; j <= m; j++) {
            int now = i;
            for (int l = 1; l <= n; l++) {
                if (a[j][l] == 0) continue;
                if (a[j][l] == 1 && (i & (1 << (l - 1)))) now ^= (1 << (l - 1));
                if (a[j][l] == -1 && !(i & (1 << (l - 1)))) now ^= (1 << (l - 1));
            }
            f[now] = min(f[now], f[i] + 1);
        }
    }
    write<int>(f[0] != 0x3f3f3f3f ? f[0] : -1);
    return 0;
}
