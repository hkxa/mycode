/*************************************
 * @user_id:      ZJ-00071.
 * @time:         2020-04-25.
 * @language:     C++.
 * @upload_place: NOI Online.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int N = 1000007, P = 1000000007;

int n, a[N], book[N];
int64 add[N * 4], sum[N * 4], sum2[N * 4];
int l[N * 4], r[N * 4];
int64 ans = 0;

void pushAdd(int u, int64 dif) {
    sum2[u] = (sum2[u] + 2 * sum[u] * dif % P + dif * dif % P * (r[u] - l[u] + 1) % P) % P;
    sum[u] = (sum[u] + dif * (r[u] - l[u] + 1)) % P;
    add[u] = (add[u] + dif) % P;
}

void pushdown(int u) {
    if (add[u]) {
        pushAdd(u << 1, add[u]);
        pushAdd(u << 1 | 1, add[u]);
        add[u] = 0;
    }
}

void pushup(int u) {
    sum[u] = (sum[u << 1] + sum[u << 1 | 1]) % P;
    sum2[u] = (sum2[u << 1] + sum2[u << 1 | 1]) % P;
}

void build(int u, int ml, int mr) {
    if (ml > mr) return;
    l[u] = ml;
    r[u] = mr;
    if (ml == mr) {
        // sum2[u] = sum[u] = 1;
        return;
    }
    int mid = (ml + mr) >> 1;
    build(u << 1, ml, mid);
    build(u << 1 | 1, mid + 1, mr);
    // pushup(u);
}

void update(int u, int ml, int mr, int dif) {
    if (l[u] > mr || r[u] < ml) return;
    if (l[u] >= ml && r[u] <= mr) {
        pushAdd(u, dif);
        return;
    }
    pushdown(u);
    update(u << 1, ml, mr, dif);
    update(u << 1 | 1, ml, mr, dif);
    pushup(u);
}

int64 query(int u, int ml, int mr) {
    if (l[u] > mr || r[u] < ml) return 0;
    if (l[u] >= ml && r[u] <= mr) return sum2[u];
    pushdown(u);
    return (query(u << 1, ml, mr) + query(u << 1 | 1, ml, mr)) % P;
}

int main()
{
    // freopen("sequence.in", "r", stdin);
    // freopen("sequence.out", "w", stdout);
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        book[i] = a[i] = read<int>();
    }
    sort(book + 1, book + n + 1);
    for (int i = 1; i <= n; i++) {
        a[i] = lower_bound(book + 1, book + n + 1, a[i]) - book;
    }
    memset(book, 0, sizeof(book));
    build(1, 1, n);
    for (int i = 1; i <= n; i++) {
        if (!book[a[i]]) update(1, 1, i, 1);
        else update(1, book[a[i]] + 1, i, 1);
        ans = (ans + query(1, 1, i)) % P;
        // for (int j = 1; j <= i; j++) {
        //     write(query(1, j, j), i == j ? 10 : 32);
        // }
        book[a[i]] = i;
    }
    write(ans, 10);
    return 0;
}