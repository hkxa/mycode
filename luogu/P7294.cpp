/*************************************
 * @problem:      [USACO21JAN] Minimum Cost Paths P.
 * @author:       brealid.
 * @time:         2021-03-14.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 2e5 + 7;

int n, m, q;
int64 c[N], ans[N];
int stk[N], top;
int64 num[N], val[N];

struct m_Query {
    int x, y, id;
    bool operator < (const m_Query &b) const {
        return y < b.y;
    }
} qry[N];

double aver(int l, int r) {
    return .5 * (c[r + 1] - c[l]) / (r - l + 1);
}

int64 calc(int l, int r, int64 x) {
    if (l > r) return 0;
    return x * x * (r - l + 1) - x * (c[r + 1] - c[l]);
}

void Insert(int p) {
    // printf("Insert %d\n", p);
    while (top && aver(stk[top - 1] + 1, stk[top]) + 1e-6 > aver(stk[top] + 1, p)) --top;
    stk[++top] = p;
    num[top] = round(aver(stk[top - 1] + 1, stk[top]));
    val[top] = val[top - 1] + calc(stk[top - 1] + 1, stk[top], num[top]);
}


signed main() {
    kin >> n >> m;
    for (int i = 1; i <= m; ++i) kin >> c[i];
    kin >> q;
    for (int i = 1; i <= q; ++i) {
        kin >> qry[i].x >> qry[i].y;
        qry[i].id = i;
    }
    sort(qry + 1, qry + q + 1);
    qry[0].y = 1;
    for (int p = 1; p <= q; ++p) {
        if (qry[p].y == 1) {
            ans[qry[p].id] = (qry[p].x - 1) * c[1];
            continue;
        }
        ans[qry[p].id] = qry[p].x * c[qry[p].y] - c[1];
        for (int i = qry[p - 1].y; i < qry[p].y; ++i) Insert(i);
        int L = 0, R = top, mid;
        while (L < R) {
            mid = (L + R + 1) >> 1;
            if (num[mid] > qry[p].x) R = mid - 1;
            else L = mid;
        }
        ans[qry[p].id] += calc(stk[L] + 1, qry[p].y - 1, qry[p].x) + val[L];
        L = 1, R = top + 1;
        while (L < R) {
            mid = (L + R) >> 1;
            if (num[mid] <= 0) L = mid + 1;
            else R = mid;
        }
        ans[qry[p].id] += calc(1, stk[L - 1], 1) - val[L - 1];
    }
    for (int i = 1; i <= q; ++i) kout << ans[i] << '\n';
    return 0;
}