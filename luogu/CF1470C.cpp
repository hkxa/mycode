/*************************************
 * @problem:      Strange Shuffle.
 * @author:       hkxadpall.
 * @time:         2021-02-02.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

int n, k;

int ask(int number) {
    static int x;
    cout << "? " << number << endl;
    cin >> x;
    return x;
}

void answer(int number) {
    cout << "! " << number << endl;
    exit(0);
}

int dec(int x) {
    return (x + n - 2) % n + 1;
}

int inc(int x) {
    return x % n + 1;
}

int solve(int p, int res) {
    if (res > k) {
        for (int i = 1; i <= 3; ++i)
            if (ask(p = dec(p)) == k && ask(p) == k) return p;
        int step = 1;
        while (step) {
            int nxt = (p + n - step - 1) % n + 1;
            int qRet = ask(nxt);
            if (qRet <= k) step >>= 1;
            else p = nxt, step <<= 1;
        }
        if (ask(p) == k) return p;
        if (ask(p == 1 ? n : p - 1) == k) return p == 1 ? n : p - 1;
    } else {
        for (int i = 1; i <= 3; ++i)
            if (ask(p = inc(p)) == k && ask(p) == k) return p;
        int step = 1;
        while (step) {
            int nxt = (p + step - 1) % n + 1;
            int qRet = ask(nxt);
            if (qRet >= k) step >>= 1;
            else p = nxt, step <<= 1;
        }
        if (ask(p) == k) return p;
        if (ask(p == n ? 1 : p + 1) == k) return p == n ? 1 : p + 1;
    }
}

void try_to_startUp(int id) {
    int nRet = ask(id);
    if (nRet != k) answer(solve(id, nRet));
}

signed main() {
    cin >> n >> k;
    ask(1);
    int nRet;
    for (int t = 1, id = 2; t <= 450; id = min(id + ++t, n)) {
        try_to_startUp(id);
    }
    try_to_startUp(1), try_to_startUp(1);
    try_to_startUp(n - 1), try_to_startUp(n - 1);
    try_to_startUp(2), try_to_startUp(2);
    try_to_startUp(n - 2), try_to_startUp(n - 2);
    try_to_startUp(3), try_to_startUp(3);
    try_to_startUp(n - 3), try_to_startUp(n - 3);
    try_to_startUp(4), try_to_startUp(4);
    try_to_startUp(n - 4), try_to_startUp(n - 4);
    return 0;
}

/*
2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2
*/