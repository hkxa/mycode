/*************************************
 * @problem:      [POI2007]POW-The Flood.
 * @user_name:    brealid/hkxadpall/zhaoyi20/j(o)moo/littleTortoise.
 * @time:         2020-08-30.
 * @language:     C++.
 * @fastio_ver:   20200827.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        // simple io flags
        ignore_int = 1 << 0,    // input
        char_enter = 1 << 1,    // output
        flush_stdout = 1 << 2,  // output
        flush_stderr = 1 << 3,  // output
        // combination io flags
        endline = char_enter | flush_stdout // output
    };
    enum number_type_flags {
        // simple io flags
        output_double_stable = 1 << 0,  // output
        output_double_faster = 1 << 1   // output
        // combination io flags
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f & ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore character set : ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (double &lf) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            lf = endch & 15;
            while (isdigit(endch = getchar())) lf = lf * 10 + (endch & 15);
            if (endch == '.') {
                double digit = 0.1;
                while (isdigit(endch = getchar())) {
                    lf += (endch & 15) * digit;
                    digit *= 0.1;
                }
            }
            if (flag) lf = -lf;
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            Int d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return d;
        }
    } read;

    struct Writer {
        Writer& operator << (io_flags f) {
            if (f & char_enter) putchar(10);
            if (f & flush_stdout) fflush(stdout);
            if (f & flush_stderr) fflush(stderr);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
        inline void operator () (double val, int eps_digit = 6, char endch = '\0', number_type_flags flg = output_double_faster) {
            if (flg & output_double_stable) {
                static char output_format[10];
                sprintf(output_format, "%%.%dlf", eps_digit);
                printf(output_format, val);
            } else if (flg & output_double_faster) {
                if (val < 0) {
                    putchar('-');
                    val = -val;
                }
                double eps_number = 0.5;
                for (int i = 1; i <= eps_digit; i++) eps_number /= 10;
                val += eps_number;
                (*this) << (int64)val;
                val -= (int64)val;
                putchar('.');
                while (eps_digit--) {
                    val *= 10;
                    putchar((int)val | '0');
                    val -= (int)val;
                }
            } else {
                (*this) << "1.#ERROR_NOT_IDENTIFIED_FLAG";
            }
            if (endch) putchar(endch);
        }
    } write;
}
using namespace Fastio;
namespace File_IO {
    void init_IO() {
        freopen("[POI2007]POW-The Flood.in", "r", stdin);
        freopen("[POI2007]POW-The Flood.out", "w", stdout);
    }
}

// #define int int64

const int N = 1000 + 7;
int n, m, h[N][N];

#define ID(x, y) (((x) - 1) * m + (y))
int fa[N * N];
struct PLACE {
    int h;
    bool type;
    int x, y;
    bool operator < (const PLACE &b) const {
        return h < b.h;
    }
} pl[N * N];

int find(int u) {
    return u == fa[u] ? u : fa[u] = find(fa[u]);
}

inline void unify(int u, int v) {
    fa[find(u)] = find(v);
}

inline bool in_same_fam(int u, int v) {
    return find(u) == find(v);
}

const int dir_x[] = {1, -1, 0, 0}, dir_y[] = {0, 0, 1, -1};
int ans = 0;

signed main() {
    // File_IO::init_IO();
    read >> n >> m;
    int have_machine = n * m + 1;
    fa[have_machine] = have_machine;
    for (int i = 1; i <= n; i++)
        for (int j = 1; j <= m; j++) {
            read >> h[i][j];
            int id = ID(i, j);
            fa[id] = id;
            pl[id].type = (h[i][j] > 0);
            pl[id].h = h[i][j] = abs(h[i][j]);
            pl[id].x = i;
            pl[id].y = j;
        }
    sort(pl + 1, pl + n * m + 1);
    for (int i = 1, j; i <= n * m; i = j + 1) {
        j = i;
        int height = pl[i].h;
        while (j < n * m && pl[j + 1].h == height) ++j;
        for (int k = i; k <= j; k++) {
            int u = ID(pl[k].x, pl[k].y);
            for (int dir = 0; dir < 4; dir++) {
                int mx = pl[k].x + dir_x[dir], my = pl[k].y + dir_y[dir];
                if (mx >= 1 && mx <= n && my >= 1 && my <= m && h[mx][my] <= height) {
                    unify(u, ID(mx, my));
                    // printf("Flood unify : (%d, %d) & (%d, %d)\n", pl[k].x, pl[k].y, mx, my);
                }
            }
        }
        for (int k = i; k <= j; k++) {
            int u = ID(pl[k].x, pl[k].y);
            if (pl[k].type && !in_same_fam(u, have_machine)) {
                unify(u, have_machine);
                ++ans;
            }
        }
    }
    write << ans << '\n';
    return 0;
}