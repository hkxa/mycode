/*************************************
 * @problem:      Jog Around The Graph.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-23.
 * @language:     C++.
 * @fastio_ver:   20200820.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore space, \t, \r, \n
            ch = getchar();
            while (ch != ' ' && ch != '\t' && ch != '\r' && ch != '\n') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            Int flag = 1;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = -1, endch = getchar();
            i = endch & 15;
            while (isdigit(endch = getchar())) i = (i << 3) + (i << 1) + (endch & 15);
            if (flag == -1) i = -i;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            Int flag = 1;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = -1, endch = getchar();
            Int i = endch & 15;
            while (isdigit(endch = getchar())) i = (i << 3) + (i << 1) + (endch & 15);
            if (flag == -1) i = -i;
            return i;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64
// #define int128 int64
// #define int128 __int128

const int N = 2e3 + 7, P = 1e9 + 7, inv2 = 500000004;

int n, m;
int64 T;
int64 f[N][N];
int u[N], v[N], w[N];

int64 ans;

inline void upd_max(int64 &pos, int64 val) { if (pos < val) pos = val; }
inline void upd_sum(int64 &pos, int64 val) { pos = ((int64)(pos + val)) % P; }

map<int, int64> s;
map<int, int64>::iterator it;

template<typename Iter> Iter pre(Iter it) { return --it; }
template<typename Iter> Iter nxt(Iter it) { return ++it; }

bool could_be_removed(const map<int, int64>::iterator &it) {
    double cross_1 = (double)(it->second - pre(it)->second) / (pre(it)->first - it->first);
    double cross_2 = (double)(nxt(it)->second - it->second) / (it->first - nxt(it)->first);
    // printf("y = %dx + %d : left_cross = %.3lf, right_cross = %.3lf\n", it->first, it->second, cross_1, cross_2);
    return (int64)ceil(cross_1) > (int64)floor(cross_2);
}

int get_sum(int64 l, int64 r, int64 k, int64 b) {
    return (l * k + b + r * k + b) % P * (r - l + 1) % P * inv2 % P;
}

signed main() {
    read >> n >> m >> T;
    for (int i = 1; i <= m; i++) read >> u[i] >> v[i] >> w[i];
    memset(f, 0xcf, sizeof(f));
    f[0][1] = 0;
    for (int tim = 1; tim <= min((int64)n, T); tim++) {
        for (int i = 1; i <= m; i++) {
            upd_max(f[tim][v[i]], f[tim - 1][u[i]] + w[i]);
            upd_max(f[tim][u[i]], f[tim - 1][v[i]] + w[i]);
        }
        upd_sum(ans, *max_element(f[tim] + 1, f[tim] + n + 1));
    }
    if (T <= n) {
        write << ans << '\n';
        return 0;
    }
    // for (int i = 1; i <= n; i++) 
    //     printf("f[n][%d] = %d\n", i, f[n][i]);
    for (int i = 1; i <= m; i++)
        upd_max(s[w[i]], max(f[n][u[i]], f[n][v[i]]));
    // for (it = s.begin(); it != s.end(); ++it)
    //     printf("y = %dx + %d\n", it->first, it->second); 
    if (s.size() > 2)
        for (it = nxt(s.begin()); nxt(it) != s.end(); ) {
            if (could_be_removed(it)) {
                s.erase(it++);
                while (pre(it) != s.begin() && could_be_removed(pre(it))) s.erase(pre(it));
            } else ++it;
        }
    // for (it = s.begin(); it != s.end(); ++it)
    //     printf("y = %dx + %d\n", it->first, it->second); 
    int lst = 1;
    for (it = s.begin(); it != s.end(); ++it) {
        if (nxt(it) != s.end()) {
            int right_most = min((it->second - nxt(it)->second) / (nxt(it)->first - it->first), T - n);
            // printf("right_most = %d\n", right_most);
            if (right_most < lst) continue;
            upd_sum(ans, get_sum(lst, right_most, it->first, it->second));
            // printf("%d~%d : y = %dx + %d\n", lst, right_most, it->first, it->second);
            lst = right_most + 1;
        } else {
            if (T - n < lst) continue;
            upd_sum(ans, get_sum(lst, T - n, it->first, it->second));
            // printf("%d~%d : y = %dx + %d\n", lst, T - n, it->first, it->second);
        }
    }
    write << ans << '\n';
    return 0;
}

/*
12x + 176 : x = 1 => 188
14x + 170 : x = 1 => 184

intersect1 : -8
intersect2 : 1.6
intersect3 : -1
*/