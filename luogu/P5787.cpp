//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      二分图 /【模板】线段树分治.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-18.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 1e5 + 7, M = 2e5 + 7;

int fa[N], siz[N], d[N];
int find(int u, int &dSav) {
    dSav += d[u];
    return u == fa[u] ? u : find(fa[u], dSav);
}
int connect(int u, int v) {
    int du = 0, dv = 0;
    int fu = find(u, du), fv = find(v, dv);
    if (fu == fv) {
        if (!((du + dv) & 1)) return -1;
        else return 0;
    }
    if (siz[fu] < siz[fv]) {
        swap(fu, fv);
        swap(u, v);
    }
    fa[fv] = fu;
    siz[fu] += siz[fv];
    d[fv] = ((du + dv) & 1) ^ 1;
    return fv;
}

int n, m, k;
int e_a[M], e_b[M];
vector<int> query[N << 2];

void insert_edge(int u, int l, int r, int ql, int qr, int eid) {
    if (l > qr || r < ql) return void();
    if (l >= ql && r <= qr) return query[u].push_back(eid);
    int mid = (l + r) >> 1;
    insert_edge(u << 1, l, mid, ql, qr, eid);
    insert_edge(u << 1 | 1, mid + 1, r, ql, qr, eid);
}

int not_suitable = 0;

void preorder_travel(int u, int l, int r) {
    if (l > r) return;
    for (size_t i = 0; i < query[u].size(); i++) {
        int &id = query[u][i];
        id = connect(e_a[id], e_b[id]);
        if (!~id) not_suitable++;
    }
    if (l == r) puts(not_suitable ? "No" : "Yes");
    else {
        int mid = (l + r) >> 1;
        preorder_travel(u << 1, l, mid);
        preorder_travel(u << 1 | 1, mid + 1, r);
    }
    for (size_t i = 0; i < query[u].size(); i++) {
        int &id = query[u][i];
        if (!~id) not_suitable--;
        else if (id) {
            siz[fa[id]] -= siz[id];
            fa[id] = id;
            d[id] = 0;
        }
    }
}

signed main() {
    read >> n >> m >> k;
    for (int i = 1; i <= n; i++) {
        siz[i] = 1;
        fa[i] = i;
    }
    for (int i = 1, l, r; i <= m; i++) {
        read >> e_a[i] >> e_b[i] >> l >> r;
        insert_edge(1, 1, k, l + 1, r, i);
    }
    preorder_travel(1, 1, k);
    return 0;
}