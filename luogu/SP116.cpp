/*************************************
 * @problem:      SP116 INTERVAL - intervals.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-02-12.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

// twice EXP with UVA1723

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

struct Edge {
    int to, v;
};
#define make_edge(u, v) ((Edge){(u), (v)})
vector<Edge> G[50007];
int n;
int dis[50007];
bool inq[50007];
// int vis[50007];

int do_case()
{
    for (int i = 0; i <= 50000; i++) G[i].clear();
    // [read data]
    n = read<int>();
    for (int i = 1, a, b, c; i <= n; i++) {
        a = read<int>();
        b = read<int>();
        c = read<int>();
        G[a - 1].push_back(make_edge(b, c));
    }
    for (int i = 0; i <= 50000; i++) {
        G[i].push_back(make_edge(i + 1, 0));
        G[i + 1].push_back(make_edge(i, -1));
    }
    // [/read data] 
    // [spfa]
    queue<int> q;
    q.push(0);
    memset(dis, -1, sizeof(dis));
    // memset(vis, 0, sizeof(vis));
    memset(inq, 0, sizeof(inq));
    dis[0] = 0;
    while (!q.empty()) {
        int fr = q.front(); 
        q.pop();
        inq[fr] = 0;
        for (register unsigned i = 0; i < G[fr].size(); i++) {
            if (dis[G[fr][i].to] < dis[fr] + G[fr][i].v) {
                dis[G[fr][i].to] = dis[fr] + G[fr][i].v;
                if (!inq[G[fr][i].to]) {
                    // vis[G[fr][i].to] = vis[fr] + 1;
                    // if (vis[G[fr][i].to] > 50000) {
                    //     printf("circle!\n");
                    //     return 0;
                    // }
                    inq[G[fr][i].to] = true;
                    q.push(G[fr][i].to);
                }
            }
        }
    }
    // [/spfa]
    // [outpue answer]
    // for (int i = 0; i < 15; i++)
    //     printf("%-2d%c", i, " \n"[i == 14]);
    // for (int i = 0; i < 15; i++)
    //     printf("%-2d%c", dis[i], " \n"[i == 14]);
    write(dis[50000], 10);
    // [/outpue answer]
    return 0;
}

int main()
{
    int T = read<int>();
    while (T--) do_case();
    return 0;
}