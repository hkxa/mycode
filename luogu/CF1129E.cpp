/*************************************
 * @problem:      Legendary Tree.
 * @author:       brealid.
 * @time:         2021-01-16.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

const int N = 500 + 6;

int n, siz[N], fa[N];
int order[N];
set<int> s;
int erased[N];

bool comp_size(int u, int v) {
    return siz[u] > siz[v];
}

bool check(int u, int l, int r) {
    set<int>::iterator it = s.begin();
    for (int i = 1; i < l; ++i) ++it;
    cout << "1\n1\n" << r - l + 1 << '\n';
    for (int i = l; i <= r; ++i) cout << *(it++) << ' ';
    cout << '\n' << u << endl;
    int ans;
    cin >> ans;
    return ans;
}

signed main() {
    cin >> n;
    siz[1] = n;
    order[1] = 1;
    for (int i = 2; i <= n; ++i) { // Using n-1 queries
        order[i] = i;
        cout << "1\n1\n" << n - 1 << '\n';
        for (int j = 2; j <= n; ++j) cout << j << ' ';
        cout << '\n' << i << endl;
        cin >> siz[i];
    }
    sort(order + 1, order + n + 1, comp_size);
    for (int _i = n; _i >= 1; --_i) {
        int u = order[_i];
        if (siz[u] != 1) {
            while (!s.empty() && check(u, 1, s.size())) {
                int l = 1, r = s.size(), mid;
                while (l < r) {
                    mid = (l + r) >> 1;
                    if (check(u, l, mid)) r = mid;
                    else l = mid + 1;
                }
                set<int>::iterator it = s.begin();
                for (int i = 1; i < l; ++i) ++it;
                fa[*it] = u;
                s.erase(it);
            }
        }
        s.insert(u);
    }
    assert(s.size() == 1);
    cout << "ANSWER\n";
    for (int i = n; i > 1; --i)
        cout << i << ' ' << fa[i] << '\n';
    return 0;
}