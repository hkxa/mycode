/*************************************
 * problem:      contest 17734 A - P5461 赦免战俘.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-07-14.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

int n;

int a[1024 + 10][1024 + 10] = {0}, p;

void create()
{
    for (int i = 1; i <= p; i++) {
        for (int j = 1; j <= p; j++) {
            a[i + p][j] = a[i][j];
            a[i][j + p] = a[i][j];
            a[i + p][j + p] = a[i][j];
        }
    }
}

void clean()
{
    for (int i = 1; i <= p; i++) {
        for (int j = 1; j <= p; j++) {
            a[i][j] = 0;
        }
    }
}

int main()
{
    n = read<int>();
    p = 1;
    a[1][1] = 1;
    for (int i = 1; i <= n; i++) {
        create();
        clean();
        p <<= 1;
    }
    for (int i = 1; i <= p; i++) {
        for (int j = 1; j <= p; j++) {
            printf("%d ", a[i][j]);
        }
        putchar(10);
    }
    return 0;
}