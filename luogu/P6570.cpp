//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      P6570 [NOI Online #3 提高组]优秀子序列（民间数据）.
 * @user_id:      63720.
 * @user_name:    brealid.
 * @time:         2020-05-26.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;
typedef long long int64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

const int N = 1000000 + 7, P = 1000000000 + 7, MaxAi = 262144 + 7;

int n, cnt[N];
int64 fpow(int64 x, int64 p) {
    int64 rwt = 1;
    while (p) {
        if (p & 1) rwt = rwt * x % P;
        x = x * x % P;
        p >>= 1;
    }
    return rwt;
}

bool m[MaxAi];
int phi[MaxAi], p[MaxAi], nump;
void getPhi() {
    phi[1] = 1;
    for (int i = 2; i < MaxAi; i++) {
        if (!m[i]) {
            p[++nump] = i;
            phi[i] = i - 1;
        }
        for (int j = 1; j <= nump && p[j] * i < MaxAi; j++) {
            m[p[j] * i] = 1;
            if (i % p[j] == 0) {
                phi[p[j] * i] = phi[i] * p[j];
                break;
            } else phi[p[j] * i] = phi[i] * (p[j] - 1);
        }
    }
}

int64 f[MaxAi], ans;
int T[MaxAi];

void getT()
{
    for (int i = 0; i <= 18; i++) T[1 << i] = (1 << i) - 1;
    for (int i = (1 << 18) - 1; i; i--) {
        if (!T[i]) T[i] = T[i + 1];
    }
    // T[i] = (1 << (log_2 highbit(i))) - 1
}
#define lowbit(x) ((x) & (-(x)))

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) cnt[read<int>()]++;
    getPhi();
    getT();
    f[0] = fpow(2, cnt[0]);
    for (int i = 1; i < 262144; i++) {
        f[i] = (f[i] + f[0] * cnt[i]) % P;
        int R = T[i + 1] ^ i;
        // 枚举缺失位
        for (int j = R; j; j = (j - 1) & R) {
            // if (cnt[i] && f[j]) printf("cnt[%d] = %d, j = %d\n", i, cnt[i], j);
            f[i | j] = (f[i | j] + f[j] * cnt[i]) % P;
        }
    }
    for (int i = 0; i < 262144; i++) {
        // if (f[i]) printf("f[%d] = %lld\n", i, f[i]);
        ans = (ans + f[i] * phi[i + 1]) % P;
    }
    printf("%lld\n", ans);
    return 0;
}

/*
20
0 0 1 2 3 5 5 6 8 9 11 13 13 13 15 16 18 21 24 30
answer = 4088
*/