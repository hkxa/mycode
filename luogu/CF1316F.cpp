//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Battalion Strength.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-31.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

#define int int64
const int N = 3e5 + 7, P = 1e9 + 7; 
int n, q;
int a[N];

int64 fpow(int64 a, int b) {
    int64 ret = 1;
    while (b) {
        if (b & 1) ret = ret * a % P;
        a = a * a % P;
        b >>= 1;
    }
    return ret;
}
#define inv(x) fpow(x, P - 2)

namespace MathVal {
    int pow2[N << 1], ipow2[N << 1];
    void init(int n) {
        pow2[0] = ipow2[0] = 1;
        for (int i = 1; i <= n; i++) pow2[i] = (pow2[i - 1] << 1) % P;
        ipow2[n] = inv(pow2[n]);
        for (int i = n - 1; i >= 1; i--) ipow2[i] = (ipow2[i + 1] << 1) % P;
    }
}

namespace SegmentTree {
    using MathVal::pow2;
    using MathVal::ipow2;
    struct treeNode { 
        int l, r;
        int tot;
        int64 val, val1, val2; 
    } t[N << 3];
    void build(int u, int l, int r){
        t[u].l = l;
        t[u].r = r;
        t[u].tot = t[u].val = t[u].val1 = t[u].val2 = 0;
        if (l == r) return;
        int mid = (l + r) >> 1;
        build(u << 1, l, mid);
        build(u << 1 | 1, mid + 1, r);
    }
    void pushup(int u) {
        t[u].tot = t[u << 1].tot + t[u << 1 | 1].tot;
        t[u].val1 = (t[u << 1].val1 + t[u << 1 | 1].val1 * pow2[t[u << 1].tot]) % P;
        t[u].val2 = (t[u << 1].val2 + t[u << 1 | 1].val2 * ipow2[t[u << 1].tot]) % P;
        t[u].val = (t[u << 1].val + t[u << 1 | 1].val + t[u << 1].val1 * t[u << 1 | 1].val2 % P * ipow2[t[u << 1].tot]) % P;
    }
    void update(int u, int pos, int ActualValue) {
        if (t[u].l == t[u].r) {
            if (ActualValue > 0) {
                t[u].tot = 1;
                t[u].val1 = ActualValue;
                t[u].val2 = (int64)ActualValue * ipow2[1] % P;
                t[u].val = 0;
            } else {
                t[u].tot = t[u].val1 = t[u].val2 = t[u].val = 0;
            }
            return;
        }
        int mid = (t[u].l + t[u].r) >> 1;
        if (pos <= mid) update(u << 1, pos, ActualValue);
        else update(u << 1 | 1, pos, ActualValue);
        pushup(u);
    }
}

namespace against_cpp11 {   
    int wh[N], val[N];
    struct LiSanHua_node {
        int Value, *pos;
        bool operator < (const LiSanHua_node &b) const {
            return Value < b.Value;
        }
    } LiSanHua[N << 1];
    signed main() {
        read >> n;
        for (int i = 1; i <= n; i++) {
            read >> a[i];
            LiSanHua[i] = (LiSanHua_node){a[i], a + i};
        }
        read >> q;
        for (int i = 1; i <= q; i++) {
            read >> wh[i] >> val[i];
            LiSanHua[i + n] = (LiSanHua_node){val[i], val + i};
        }
        sort(LiSanHua + 1, LiSanHua + n + q + 1);
        for (int i = 1; i <= n + q; i++) *LiSanHua[i].pos = i;
        MathVal::init(n + q);
        SegmentTree::build(1, 1, n + q);
        // for (int i = 1; i <= n; i++) printf("%d\n", a[i]);
        for (int i = 1; i <= n; i++) SegmentTree::update(1, a[i], LiSanHua[a[i]].Value);
        write << SegmentTree::t[1].val << '\n';
        for (int i = 1; i <= q; i++) {
            SegmentTree::update(1, a[wh[i]], -LiSanHua[a[wh[i]]].Value);
            a[wh[i]] = val[i];
            SegmentTree::update(1, a[wh[i]], LiSanHua[a[wh[i]]].Value);
            write << SegmentTree::t[1].val << '\n';
        }
        return 0;
    }
}

signed main() { return against_cpp11::main(); }