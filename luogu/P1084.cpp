// luogu-judger-enable-o2
#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cmath>
#define LL long long
using namespace std;
int read()
{
    int q = 0;
    char ch = ' ';
    while (ch < '0' || ch > '9')
        ch =
            getchar();
    while (ch >= '0' && ch <= '9')
        q = q * 10 + ch - '0', ch = getchar();
    return q;
}
