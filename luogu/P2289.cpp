//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      [HNOI2004]邮递员.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-22.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// #define int int64
const int P = 49999;

int n, m;
__int128 f[2][P + 7];
int head[2][P + 7], val[2][P + 7], nxt[2][P + 7], las = 0, now = 1;
int hashCnt[2];

const int LefPlug = 1, RigPlug = 2, NonePlug = 0;

string get_status(int k) { // For Debug
    string ret = "";
    for (int i = 0; i <= m; i++) 
        switch ((k >> (i << 1)) & 3) {
            case 3 : ret += '3'; break;
            case 2 : ret += '2'; break;
            case 1 : ret += '1'; break;
            case 0 : ret += '0'; break;
        }
    return ret;
}

inline void InsHash(int zt, const __int128 dif) {
    int pos = zt % P;
    for (int u = head[now][pos]; ~u; u = nxt[now][u]) 
        if (val[now][u] == zt) {
            f[now][u] += dif;
            return;
        }
    f[now][++hashCnt[now]] = dif;
    val[now][hashCnt[now]] = zt;
    nxt[now][hashCnt[now]] = head[now][pos];
    head[now][pos] = hashCnt[now];
}

inline __int128 search(int zt) {
    int pos = zt % P;
    for (int u = head[las][pos]; ~u; u = nxt[las][u]) 
        if (val[las][u] == zt)
            return f[las][u];
    return 0;
}

signed main() {
    m = read<int>();
    n = read<int>();
    if (m == 1 || n == 1) {
        puts("1");
        return 0;
    }
    int statusAll = 1 << ((m + 1) << 1);
    int up, lef, remain, pos, CntPlug;
    memset(head[las], -1, sizeof(head[las]));
    hashCnt[las] = 1;
    f[las][1] = 1;
    val[las][1] = 0;
    nxt[las][1] = -1;
    head[las][0] = 1;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            memset(head[now], -1, sizeof(head[now]));
            for (int id = 1, k; id <= hashCnt[las]; id++) {
                k = val[las][id];
                if (k >= statusAll) continue;
                remain = k & ~(15 << (j << 1));
                up = (k >> ((j + 1) << 1)) & 3;
                lef = (k >> (j << 1)) & 3;
                switch (lef * 3 + up) { // [一定铺线]格
                    case 0 : // lef : NonePlug; up : NonePlug
                        InsHash(remain | (9 << (j << 1)), f[las][id]);
                        break;
                    case 1 : // lef : NonePlug; up : LefPlug
                    case 2 : // lef : NonePlug; up : RigPlug
                        InsHash(remain | (up << (j << 1)), f[las][id]);
                        InsHash(remain | (up << ((j + 1) << 1)), f[las][id]);
                        break;
                    case 3 : // lef : LefPlug; up : NonePlug
                    case 6 : // lef : RigPlug; up : NonePlug
                        InsHash(remain | (lef << (j << 1)), f[las][id]);
                        InsHash(remain | (lef << ((j + 1) << 1)), f[las][id]);
                        break;
                    case 4 : // lef : LefPlug; up : LefPlug
                        pos = (j + 1) << 1;
                        CntPlug = 0;
                        do {
                            pos += 2;
                            if (((k >> pos) & 3) == LefPlug) CntPlug++;
                            else if (((k >> pos) & 3) == RigPlug) CntPlug--;
                        } while (~CntPlug);
                        InsHash(remain ^ (3 << pos), f[las][id]);
                        break;
                    case 8 : // lef : RigPlug; up : RigPlug
                        pos = j << 1;
                        CntPlug = 0;
                        do {
                            pos -= 2;
                            if (((k >> pos) & 3) == LefPlug) CntPlug--;
                            else if (((k >> pos) & 3) == RigPlug) CntPlug++;
                        } while (~CntPlug);
                        InsHash(remain ^ (3 << pos), f[las][id]);
                        break;
                    case 5 : // lef : LefPlug; up : RigPlug
                        if (i == n - 1 && j == m - 1) 
                            InsHash(remain, f[las][id]);
                        break;
                    case 7 : // lef : RigPlug; up : LefPlug
                        InsHash(remain, f[las][id]);
                        break;
                }
            }
            swap(las, now);
            hashCnt[now] = 0;
        }
        for (int k = 1; k <= hashCnt[las]; k++) {
            val[las][k] <<= 2;
        }
    }
    write(search(0) * 2, 10);
    return 0;
}

// Create File Date : 2020-06-22