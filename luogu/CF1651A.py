def findmin(x, seq):
    t = 1000000000
    for i in seq:
        if abs(x - i) < t:
            t = abs(x - i)
            choose = i
    return (choose, t)

t = int(input())
for i in range(t):
    n = int(input())
    a = list(map(int, input().split(' ')))
    b = list(map(int, input().split(' ')))
    ca0, xa0 = findmin(a[0], b)
    ca1, xa1 = findmin(a[-1], b)
    cb0, xb0 = findmin(b[0], a)
    cb1, xb1 = findmin(b[-1], a)
    match00 = (ca0 == b[0] or cb0 == a[0])
    match01 = (ca0 == b[-1] or cb1 == a[0])
    match10 = (ca1 == b[0] or cb0 == a[-1])
    match11 = (ca1 == b[-1] or cb1 == a[-1])
    value00 = min(xa0 + xb0, abs(a[0] - b[0]))
    value01 = min(xa0 + xb1, abs(a[0] - b[-1]))
    value10 = min(xa1 + xb0, abs(a[-1] - b[0]))
    value11 = min(xa1 + xb1, abs(a[-1] - b[-1]))
    if match00:
        value00 = xa0 if ca0 == b[0] else xb0
    if match01:
        value01 = xa0 if ca0 == b[-1] else xb1
    if match10:
        value10 = xa1 if ca1 == b[0] else xb0
    if match11:
        value11 = xa1 if ca1 == b[-1] else xb1
    print(min(value00 + value11, value01 + value10))