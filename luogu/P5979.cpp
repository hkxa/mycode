/*************************************
 * @problem:      [PA2014]Druzyny.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-26.
 * @language:     C++.
 * @fastio_ver:   20200820.
*************************************/ 
#pragma GCC optimize(3)
#pragma GCC optimize("Ofast")
#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore space, \t, \r, \n
            ch = getchar();
            while (ch == ' ' || ch == '\t' || ch == '\r' || ch == '\n') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            Int flag = 1;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = -1, endch = getchar();
            i = endch & 15;
            while (isdigit(endch = getchar())) i = (i << 3) + (i << 1) + (endch & 15);
            if (flag == -1) i = -i;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            Int flag = 1;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = -1, endch = getchar();
            Int i = endch & 15;
            while (isdigit(endch = getchar())) i = (i << 3) + (i << 1) + (endch & 15);
            if (flag == -1) i = -i;
            return i;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64
// #define int128 int64
// #define int128 __int128

const int P = 1e9 + 7;

int c[1000010], d[1000010];
inline int max(const int &a, const int &b) { return a > b ? a : b; }
inline int min(const int &a, const int &b) { return a < b ? a : b; }
struct state {
    int f, g;
};
inline state operator+(const state &a, const state &b) {
    if (a.f > b.f) return a;
    if (a.f < b.f) return b;
    return (state){a.f, (a.g + b.g) % P};
}
state f[1000010], segf[4000010];
int cn[4000010], cx[4000010], dx[4000010], dn[4000010];
inline void update(int x, int l, int r, const int &p) {
    if (l == r)
    {
        segf[x] = f[p - 1];
        cx[x] = cn[x] = c[l], dx[x] = dn[x] = d[l];
        return;
    }
    int mid = (l + r) >> 1;
    if (p <= mid)
        update(x << 1, l, mid, p);
    else
        update(x << 1 | 1, mid + 1, r, p);
    segf[x] = segf[x << 1] + segf[x << 1 | 1];
    cx[x] = std::max(cx[x << 1], cx[x << 1 | 1]);
    dx[x] = std::max(dx[x << 1], dx[x << 1 | 1]);
    cn[x] = std::min(cn[x << 1], cn[x << 1 | 1]);
    dn[x] = std::min(dn[x << 1], dn[x << 1 | 1]);
}
inline void query(int x, int l, int r, const int &p, int cxr, int dnr)
{
    if (l > p)
        return;
    if (r <= p)
    {
        if (segf[x].f < f[p].f) return;
        if (max(cn[x], cxr) > p - l + 1) return;
        if (min(dx[x], dnr) < p - r + 1) return;
        if (max(cx[x], cxr) <= p - r + 1 && min(dn[x], dnr) >= p - l + 1)
        {
            f[p] = f[p] + segf[x];
            return;
        }
    }
    int mid = (l + r) >>1;
    if (segf[x << 1].f > segf[x << 1 | 1].f)
        query(x << 1, l, mid, p, std::max(cxr, cx[x << 1 | 1]), std::min(dnr, dn[x << 1 | 1])), query(x << 1 | 1, mid + 1, r, p, cxr, dnr);
    else
        query(x << 1 | 1, mid + 1, r, p, cxr, dnr), query(x << 1, l, mid, p, std::max(cxr, cx[x << 1 | 1]), std::min(dnr, dn[x << 1 | 1]));
}

int main() {
    int n;
    read >> n;
    for (int i = 1; i <= n; ++i) read >> c[i] >> d[i];
    memset(cx, 0xcf, sizeof(cx));
    memset(dx, 0xcf, sizeof(dx));
    memset(cn, 0x3f, sizeof(cn));
    memset(cn, 0x3f, sizeof(cn));
    for (int i = 1; i <= n << 2; ++i)
        cx[i] = dx[i] = -1e9, cn[i] = dn[i] = 1e9;
    f[0].g = 1;
    update(1, 1, n, 1);
    for (int i = 1; i <= n; ++i) {
        f[i].f = -1e9;
        query(1, 1, n, i, -1e9, 1e9);
        ++f[i].f;
        if (i != n) update(1, 1, n, i + 1);
    }
    if (!f[n].g) write << "NIE\n";
    else write << f[n].f << ' ' << f[n].g << '\n';
    return 0;
}