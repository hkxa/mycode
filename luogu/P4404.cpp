/*************************************
 * problem:      P4404 [JSOI2010]缓存交换.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-16.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define puts_return(x) { puts(x); return 0; }
#define write_return(x) { write(x); return 0; }

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

struct memory_block {
    int memoryId, nextQueryId;
    memory_block(int mi, int nqi) : memoryId(mi), nextQueryId(nqi) {}
    bool operator < (const memory_block &otherMemory) const
    {
        return nextQueryId < otherMemory.nextQueryId;
    }
};


priority_queue<memory_block> cache;
int cacheMiss = 0, cacheCount = 0;
const int n = read<int>(), m = read<int>();
map<int, int> nextQueryMap;
map<int, int> inCache;
int nextQuery[100001], query[100001];

int main()
{
    for (int i = 1; i <= n; i++) {
        query[i] = read<int>();
    }
    for (int i = n; i > 0; i--) {
        if (nextQueryMap[query[i]] != 0) nextQuery[i] = nextQueryMap[query[i]];
        else nextQuery[i] = n + 1;
        nextQueryMap[query[i]] = i;
    }
    for (int i = 1; i <= n; i++) {
        if (inCache[query[i]] != i) {
            // printf("[Cache Miss] query #%d : ", i);
            if (cacheCount == m) {
                while (inCache[cache.top().memoryId] != cache.top().nextQueryId) cache.pop();
                inCache[cache.top().memoryId] = 0;
                // printf("pop %d ", cache.top().memoryId);
                cache.pop();
                cacheCount--;
            } 
            // printf("push %d\n", query[i]);
            cacheCount++;
            cacheMiss++;
        }
        cache.push(memory_block(query[i], nextQuery[i]));
        inCache[query[i]] = nextQuery[i];
    }
    write(cacheMiss, 10);
    return 0;
}