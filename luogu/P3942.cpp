// luogu-judger-enable-o2
#include <vector>
#include <stdio.h>
#include <string.h>
#include <algorithm>
using namespace std;
int n, k, t, a[100007], fa[100007], depth[100007], closest[100007];
int ft[27];
// array a : 按深度排序的编号(从儿子到父亲) 
int ans;
// int me, f1, f2;
vector<int> G[100007];
int u, v;
bool vis[100007] = {0};

bool cmp(int x, int y) 
{
    return depth[x] > depth[y];
}

void dfs(int p)
{
    vis[p] = 1;
    for (vector<int>::iterator it = G[p].begin(); it != G[p].end(); it++) {
        if (!vis[*it]) {
            fa[*it] = p;
            depth[*it] = depth[p] + 1;
            // printf("%d 's fa is %d\n", *it, p);
            dfs(*it);
        }
    }
}

int main()
{
    scanf("%d%d%d", &n, &k, &t);
    if (k == 0) {
        printf("%d", n);
        return 0;
    }
    memset(closest, 0x3f, sizeof(closest));
    for (int i = 2; i <= n; i++) {
        scanf("%d%d", &u, &v);
        G[u].push_back(v);
        G[v].push_back(u);
    }
    depth[1] = 0;
    dfs(1);
    a[1] = 1;
    for (int i = 2; i <= n; i++) {
        // depth[i] = depth[fa[i]] + 1;
        a[i] = i;
    }
    sort(a + 1, a + n + 1, cmp);
    for (int i = 1; i <= n; i++) {
        ft[0] = a[i];
        // fprintf(stderr, "%d ing...", a[i]);
        for (int i = 1; i <= k; i++) {
            ft[i] = fa[ft[i - 1]];
            if (closest[ft[i]] + i < closest[ft[0]])
                closest[ft[0]] = closest[ft[i]] + i;
        }
        // fprintf(stderr, "%d-%d-%d\n", me, f1, f2);
        // fprintf(stderr, "haha t (%d^%d^%d)", closest[me], closest[f1], closest[f2]);
        // closest[me] = min(closest[me], min(closest[f1] + 1, closest[f2] + 2));
        // fprintf(stderr, " => %d.\n", closest[ft[0]]);
        if (closest[ft[0]] > k) {
            // 家附近没有可以来救援的消防站 => 建在爷爷处！
            // fprintf(stderr, "made firestation in node %d.\n", ft[k]);
            ans++;
            ft[0] = ft[k];
            closest[ft[0]] = 0;
            for (int i = 1; i <= k; i++) {
                if (k - i < closest[ft[i]])
                    closest[ft[i]] = k - i;
            }
            for (int i = 1; i <= k; i++) {
                ft[i] = fa[ft[i - 1]];
                if (i < closest[ft[i]])
                    closest[ft[i]] = i;
            }
        }
    }
    printf("%d", ans);
    return 0;
}