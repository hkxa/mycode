/*************************************
 * @problem:      P5858 「SWTR-03」Golden Sword.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-03-09.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// #define int int64
int n, w, s;
int64 a[5507];
int64 f[5507][5507], ans = 0xcfcfcfcfcfcfcfcf;
int q[5507], head = 1, tail = 0;

signed main()
{
    memset(f, 0xcf, sizeof(f));
    memset(f[0], 0, sizeof(f[0]));
    n = read<int>();
    w = read<int>();
    s = read<int>();
    for (int i = 1; i <= n; i++) a[i] = read<int>();
    for (int i = 1; i <= n; i++) {
        head = 1; tail = 0;
        for (int j = 1; j <= i; j++) {
            if (head <= tail && q[head] < j - s) head++;
            if (j < i) {
                while (head <= tail && f[i - 1][q[tail]] <= f[i - 1][j]) tail--;
                q[++tail] = j;
            }
            if (j >= i - w + 1) {
                f[i][j] = f[i - 1][q[head]] + a[i] * (i - j + 1); 
                // printf("[head = %d] -> ", q[head]);
                // printf("f[%d][%d] = %I64d.\n", i, j, f[i][j]);
            }
        }
    }
    /**
     * -5 3 -1 -4 7 -6 5
     * 1 2 2 3 4 3 4
     * f[1][1] f[2][1] f[3][2] f[4][2] f[5][2] f[6][4] f[7][4];
     */
    // printf("f[%d][%d] = %I64d.\n", 1, 1, f[1][1]);
    // printf("f[%d][%d] = %I64d.\n", 2, 1, f[2][1]);
    // printf("f[%d][%d] = %I64d.\n", 3, 2, f[3][2]);
    // printf("f[%d][%d] = %I64d.\n", 4, 2, f[4][2]);
    // printf("f[%d][%d] = %I64d.\n", 5, 2, f[5][2]);
    // printf("f[%d][%d] = %I64d.\n", 6, 4, f[6][4]);
    // printf("f[%d][%d] = %I64d.\n", 7, 4, f[7][4]);
    // printf("--------------------\n");
    // printf("f[%d][%d] = %I64d.\n", 5, 3, f[5][3]);
    // printf("f[%d][%d] = %I64d.\n", 5, 4, f[5][4]);
    for (int i = n - w + 1; i <= n; i++) ans = max(ans, f[n][i]);
    write(ans, 10);
    return 0;
}