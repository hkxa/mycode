/*************************************
 * @problem:      最小圆覆盖.
 * @author:       brealid.
 * @time:         2021-03-21.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const double eps = 1e-8;

struct vec {
    double x, y;
    vec() {}
    vec(double X, double Y) : x(X), y(Y) {}
    vec operator + (const vec &b) const { return vec(x + b.x, y + b.y); }
    vec operator - (const vec &b) const { return vec(x - b.x, y - b.y); }
    vec operator * (const double &k) const { return vec(x * k, y * k); }
    vec operator / (const double &k) const { return vec(x / k, y / k); }
    double len_sqr() const { return x * x + y * y; }
    double len() const { return sqrt(len_sqr()); }
    vec rotate_90() const { return vec(-y, x); }
};

double dot(vec a, vec b) { return a.x * b.x + a.y * b.y; }
double crs(vec a, vec b) { return a.x * b.y - a.y * b.x; }

vec intersect(vec p1, vec v1, vec p2, vec v2) {
    double t = crs((p2 - p1), v2) / crs(v1, v2);
    return p1 + v1 * t;
}

struct circle {
    vec o;
    double len_sqr;
    circle() {}
    circle(vec _o, double _len_sqr) : o(_o), len_sqr(_len_sqr) {}
    circle(vec a, vec b) {
        o = (a + b) * 0.5;
        len_sqr = (a - o).len_sqr();
    }
    circle(vec a, vec b, vec c) {
        if (abs(crs(b - a, c - a)) < eps) {
            double ab = (a - b).len_sqr();
            double bc = (b - c).len_sqr();
            double ac = (a - c).len_sqr();
            if (ab <= ac && bc <= ac) *this = circle(a, c);
            else if (ab <= bc && ac <= bc) *this = circle(b, c);
            else *this = circle(a, b);
        }
        o = intersect((a + b) * 0.5, (b - a).rotate_90(),
                      (a + c) * 0.5, (c - a).rotate_90());
        len_sqr = (a - o).len_sqr();
    }
    bool is_include(vec p) const { return (p - o).len_sqr() < len_sqr; }
    double len() const { return sqrt(len_sqr); }
};

const int N = 1e5 + 7;
int n;
vec p[N];

signed main() {
    scanf("%d", &n);
    for (int i = 1; i <= n; ++i) scanf("%lf%lf", &p[i].x, &p[i].y);
    random_shuffle(p + 1, p + n + 1);
    circle c(vec(0, 0), 0);
    for (int i = 1; i <= n; ++i) {
        if (c.is_include(p[i])) continue;
        c = circle(p[i], 0);
        for (int j = 1; j < i; ++j) {
            if (c.is_include(p[j])) continue;
            c = circle(p[i], p[j]);
            for (int k = 1; k < j; ++k) {
                if (c.is_include(p[k])) continue;
                c = circle(p[i], p[j], p[k]);
            }
        }
    }
    printf("%.10lf\n%.10lf %.10lf\n", c.len(), c.o.x, c.o.y);
    return 0;
}