/*************************************
 * @contest:      Codeforces Round #622 (Div. 2).
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-02-23.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}
#define int int64

int n;
int m[500007];
int pm[500007], nm[500007]; // PreMin, NxtMin
int64 ps[500007], ns[500007]; // PreSum, NxtSum
int Ans; int64 Max;
struct S {
    int l;
    int h;
};
deque<S> q;
#define makeS(a, b) ((S){(a), (b)})

signed main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) m[i] = read<int>();
    for (int i = 1; i <= n; i++) pm[i] = min(pm[i - 1], m[i]);
    for (int i = n; i >= 1; i--) nm[i] = min(nm[i + 1], m[i]);
    for (int i = 1, L; i <= n; i++) {
        if (q.empty() || m[i] >= q.back().h) {
            ps[i] = ps[i - 1] + m[i];
            q.push_back(makeS(1, m[i]));
        } else {
            L = 1;
            while (!q.empty() && m[i] < q.back().h) {
                L += q.back().l;
                q.pop_back();
            }
            ps[i] = ps[i - L] + L * m[i];
            q.push_back(makeS(L, m[i]));
        }
        // printf("%lld%c", ps[i], " \n"[i == n]);
    }
    while (!q.empty()) q.pop_back();
    for (int i = n, L; i >= 1; i--) {
        if (q.empty() || m[i] >= q.back().h) {
            ns[i] = ns[i + 1] + m[i];
            q.push_back(makeS(1, m[i]));
        } else {
            L = 1;
            while (!q.empty() && m[i] < q.back().h) {
                L += q.back().l;
                q.pop_back();
            }
            ns[i] = ns[i + L] + L * m[i];
            q.push_back(makeS(L, m[i]));
        }
    }
    // for (int i = 1; i <= n; i++) printf("%lld%c", ns[i], " \n"[i == n]);
    for (int i = 1; i <= n; i++) {
        if (ps[i - 1] + ns[i] > Max) {
            Max = ps[i - 1] + ns[i];
            Ans = i;
        }
    }
    for (int j = Ans - 1; j >= 1; j--) m[j] = min(m[j], m[j + 1]);
    for (int j = Ans + 1; j <= n; j++) m[j] = min(m[j], m[j - 1]);
    for (int i = 1; i <= n; i++) write(m[i], 32);
    putchar(10);
    return 0;
}