/*************************************
 * problem:      CF27E Number With The Given Amount Of Divisors.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-16.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define puts_return(x) { puts(x); return 0; }
#define write_return(x) { write(x); return 0; }

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;
int primes[] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47};
long long ans = 1e18 + 7;

void dfs(int64 now, int PrimeId, int maxPower, int divisorsCnt) {
    if (divisorsCnt > n) return;
    if (now <= 0 || now >= ans) return;
    if (PrimeId > 15) return;
    if (divisorsCnt == n) {
        ans = now;
        return;
    }
    for (int i = 1; i <= maxPower; i++) {
        dfs(now *= primes[PrimeId], PrimeId + 1, i, divisorsCnt * (i + 1));
    }
}

int main()
{
    n = read<int>();
    dfs(1, 0, 64, 1);
    write(ans, 10);
    return 0;
}
