/*************************************
 * problem:      P3467 [POI2008]PLA-Postering.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-07-22.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

int n, h, ans;

int main()
{
	scanf("%d", &n);
    ans = n;
    stack<int> s;
	for (int i = 0; i < n; i++) {
        read<int>();
		h = read<int>();
        while (!s.empty() && h <= s.top()){
            if (h == s.top()) ans--;
            s.pop();
        }
        s.push(h);
    }
    write(ans);
	return 0;
}