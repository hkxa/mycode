/*************************************
 * problem:      id_name.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-14.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define puts_return(x) { puts(x); return 0; }
#define write_return(x) { write(x); return 0; }

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

size_t n, m;
vector<pair<uint32, uint8> > G[100003];
int8 col[100003];
uint64 block_cnt, edge_cnt, cnt[4];
enum Kinds_block {strong_connect = 1, weak_connect = 2} block_kind;

void clear_data()
{
    block_cnt = edge_cnt = cnt[0] = cnt[1] = cnt[2] = 0;
    block_kind = weak_connect;
}

void color(int u)
{
    // printf("color(%d) [myCol = %d].\n", u, col[u]);
    block_cnt++;
    cnt[col[u]]++;
    for (size_t i = 0; i < G[u].size(); i++) {
        if (G[u][i].second == 1) edge_cnt++;
        if (col[G[u][i].first] != -1) {
            if (col[G[u][i].first] != (col[u] + G[u][i].second) % 3) block_kind = strong_connect;
        } else {
            col[G[u][i].first] = (col[u] + G[u][i].second) % 3;
            color(G[u][i].first);
        }
    }
}

int main()
{
    n = read<size_t>();
    m = read<size_t>();
    uint32 u, v;
    while (m--) {
        u = read<uint32>();
        v = read<uint32>();
        G[u].push_back(make_pair(v, 1));
        G[v].push_back(make_pair(u, 2));
    }
    uint64 ans(0);
    memset(col, -1, sizeof(col));
    for (size_t i = 1; i <= n; i++) {
        if (col[i] == -1) {
            clear_data();
            col[i] = 0;
            color(i);
            if (block_kind == strong_connect) {
                // printf("[Point #%u] Case #1 : block_cnt = %llu\n", i, block_cnt);
                ans += block_cnt * block_cnt;
            } else if (cnt[0] && cnt[1] && cnt[2]) {
                // printf("[Point #%u] Case #2 : cnt = {%llu, %llu, %llu}\n", i, cnt[0], cnt[1], cnt[2]);
                ans += cnt[0] * cnt[1] + cnt[0] * cnt[2] + cnt[1] * cnt[2]; 
            } else {
                // printf("[Point #%u] Case #3 : edge_cnt = %llu\n", i, edge_cnt);
                ans += edge_cnt;
            }
        }
    }
    write(ans, 10);
    return 0;
}