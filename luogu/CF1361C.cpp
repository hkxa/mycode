//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      Codeforces Round #647 (Div. 1).
 * @user_name:    hkxadpall.
 * @time:         2020-06-04.
 * @language:     C++.
 * @upload_place: Codeforces.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 988244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64
const int N = 5e5 + 7, Fill = (1 << 20) + 7;

int n;
int a[N], b[N];
int ans[N * 2], ansCnt = 0;
bool vis[Fill];
vector<pair<int, int> > G[Fill];

/*

*/

void setVis(int u) {
    vis[u] = true;
    for (size_t i = 0; i < G[u].size(); i++) {
        if (!vis[G[u][i].first]) setVis(G[u][i].first);
    }
}

bool check(int bit) {
    int GET_BIT = (1 << bit) - 1;
    for (int i = 0; i <= GET_BIT; i++) {
        G[i].clear();
        vis[i] = false;
    }
    for (int i = 0, u, v; i < n; i++) {
        u = a[i] & GET_BIT;
        v = b[i] & GET_BIT;
        G[u].push_back(make_pair(v, i << 1));
        G[v].push_back(make_pair(u, i << 1 | 1));
        // printf("E (%d, %d)\n", u, v);
    }
    bool circle = 0;
    for (int i = 0; i <= GET_BIT; i++) {
        if (G[i].size() & 1) return false;
        if (!G[i].size()) continue;
        if (!vis[i]) {
            if (circle) return false;
            setVis(i);
            circle = true;
        }
    }
    return true;
}

void travel(int u, int fa = -1) {
    // printf("travel %d !\n", u);
    while (!G[u].empty()) {
        pair<int, int> ele = G[u].back();
        G[u].pop_back();
        if (!vis[ele.second >> 1]) {
            vis[ele.second >> 1] = true;
            travel(ele.first, ele.second);
        } 
    }
    G[u].clear();
    if (~fa) {
        ans[ansCnt++] = fa ^ 1;
        ans[ansCnt++] = fa;
    }
}

void GetPermutation(int bit) {
    int GET_BIT = (1 << bit) - 1;
    for (int i = 0; i < n; i++) vis[i] = false;
    // printf("GB = %d\n", GET_BIT);
    for (int i = 0; i <= GET_BIT; i++) {
        // if (i == 1048575) printf("???\n");
        if (G[i].size()) travel(i);
    }
    for (int i = 0; i < (n << 1); i++) {
        write(ans[i] + 1, 32);
    }
    putchar(10);
}

signed main() {
    n = read<int>();
    for (int i = 0; i < n; i++) {
        a[i] = read<int>();
        b[i] = read<int>();
    }
    for (int i = 20; i >= 0; i--) {
        if (check(i)) {
            write(i, 10);
            // if (n == 500000) return 0;
            GetPermutation(i);
            return 0;
        }
    }
    return 0;
}