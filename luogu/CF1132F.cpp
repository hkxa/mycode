//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Clear the String.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-09.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

namespace against_cpp11 {
    const int N = 500 + 7;
    int n;
    char s[N];
    int f[N][N], g[N][N][26];
    bool occured[26];
    signed main() {
        read >> n;
        scanf("%s", s + 1);
        for (int i = 1, j = 1; i <= n; i++) {
            if (s[i] != s[i - 1]) s[j++] = s[i];
            if (i == n) n = j - 1;
        }
        s[n + 1] = '\0';
        // printf("%s\n", s + 1);
        for (int i = 1; i <= n; i++) {
            s[i] -= 'a';
        }
        for (int len = 1; len <= n; len++) {
            for (int i = 1, j = len; j <= n; i++, j++) {
                f[i][j] = len;
                for (int k = i; k <= j; k++) occured[s[k]] = true;
                for (int ch = 0; ch < 26; ch++) {
                    if (occured[ch]) {
                        g[i][j][ch] = len;
                        occured[ch] = false;
                    } else g[i][j][ch] = n + 1;
                }
                for (int ch = 0; ch < 26; ch++) {
                    for (int k = i; k < j; k++) {
                        g[i][j][ch] = min(g[i][j][ch], g[i][k][ch] + f[k + 1][j]);
                        g[i][j][ch] = min(g[i][j][ch], f[i][k] + g[k + 1][j][ch]);
                        g[i][j][ch] = min(g[i][j][ch], g[i][k][ch] + g[k + 1][j][ch] - 1);
                    }
                    f[i][j] = min(f[i][j], g[i][j][ch]);
                }
            }
        }
        write << f[1][n] << '\n';
        return 0;
    }
}

signed main() { return against_cpp11::main(); }