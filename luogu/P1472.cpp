/*************************************
 * problem:      P1472 奶牛家谱 Cow Pedigrees.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-11-09.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

int n, k;
int f[101][201] = {0};

int main()
{
    cin >> n >> k;
    for (int i = 1; i <= k; i++) f[i][1] = 1;
    for (int l = 1; l <= k; l++)
        for (int i = 3; i <= n; i += 2) 
            for (int j = 1; j < i; j += 2)
                f[l][i] = (f[l][i] + f[l - 1][j] * f[l - 1][i - j - 1]) % 9901;
    printf("%d", (f[k][n] - f[k - 1][n] + 9901) % 9901);
    return 0;
}