/*************************************
 * @contest:      【LGR-086】洛谷 5 月月赛 II & EZEC Round 8.
 * @author:       brealid.
 * @time:         2021-05-09.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

// #define USE_FREAD  // 使用 fread  读入，去注释符号
// #define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

int ask1(int u, int v) {
    // return v / 2;
    printf("? 1 %d %d\n", u, v);
    fflush(stdout);
    int nRet;
    scanf("%d", &nRet);
    return nRet;
}

void ask2(int u, unordered_set<int> &dst, unordered_set<int> &src) {
    // static int nCount = 0, nPrint = 0;
    // printf("\rnCount = %d :: nPrint = %d :: ", ++nCount, nPrint);
    // if (u % 2 == 0) {
    //     ++nPrint;
    //     dst.insert(u), src.erase(u);
    //     return;
    // }
    // nPrint += 2000 - u + 1;
    // for (int i = u; i <= 2000; ++i) dst.insert(i), src.erase(i);
    // return;

    printf("? 2 %d\n", u);
    fflush(stdout);
    int n;
    kin >> n;
    for (int i = 0, x; i < n; ++i) {
        kin >> x;
        // dst.insert(x), src.erase(x);
        dst.insert(x);
        if (src.find(x) != src.end()) src.erase(x);
    }
}

int n, dep[2007], fa[2007];
vector<int> G[2007];
// vector<int> dep_nodes[2007];

void GetTree(int u) {
    for (unsigned i = 0; i < G[u].size(); ++i) {
        int v = G[u][i];
        if (v != fa[u]) {
            fa[v] = u;
            GetTree(v);
        }
    }
}

void Add_Edge(int u, int v) {
    static int edges_cnt = 0;
    ++edges_cnt;
    G[u].push_back(v), G[v].push_back(u);
    if (edges_cnt >= n - 1) {
        GetTree(1);
        printf("! ");
        for (int i = 2; i <= n; ++i) printf("%d%c", fa[i], " \n"[i == n]);
        fflush(stdout);
        exit(0);
    }
}

void solve(int u, unordered_set<int> &s) {
    if (s.find(u) != s.end()) s.erase(u);
    if (s.empty()) return;
    vector<int> direct_son;
    for (unordered_set<int>::iterator it = s.begin(); it != s.end(); ++it) {
        int v = *it;
        if (dep[v] == dep[u] + 1) {
            direct_son.push_back(v);
            fa[v] = u;
        }
    }
    if (direct_son.size() > 100) random_shuffle(direct_son.begin(), direct_son.end());
    for (unsigned i = 0; i < direct_son.size(); ++i) Add_Edge(u, direct_son[i]);
    int DirectSon_Remain = direct_son.size();
    for (unsigned i = 1; i < direct_son.size(); ++i) {
        if ((int)s.size() == DirectSon_Remain) break;
        int v = direct_son[i];
        unordered_set<int> ss;
        ask2(v, ss, s);
        solve(v, ss);
        --DirectSon_Remain;
    }
    if ((int)s.size() > DirectSon_Remain) solve(direct_son[0], s);
}

signed main() {
    srand(time(0));
    scanf("%d", &n);
    for (int i = 2; i <= n; ++i)
        dep[i] = ask1(1, i);
    unordered_set<int> son_of_root;
    for (int i = 1; i <= n; ++i) son_of_root.insert(i);
    solve(1, son_of_root);
    return 0;
}