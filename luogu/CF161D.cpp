//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Distance in Tree.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-11.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 988244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

const int N = 50007;

int n, k, Q;
int vis[N];
int h[N], cnt;
vector<pair<int, int> > G[N];
int ans = 0;
// bitset<100000007> happen;

int siz[N], root[N], rootMax, MainRoot;

void basicFindRoot_getSiz(int u) {
    siz[u] = 1;
    vis[u] = true;
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i].first;
        if (!vis[v]) {
            basicFindRoot_getSiz(v);
            siz[u] += siz[v];
        }
    }
    vis[u] = false;
}
void basicFindRoot_getRoot(int u, int si, int &root) {
    int tMax = 1;
    vis[u] = true;
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i].first;
        if (!vis[v]) {
            basicFindRoot_getRoot(v, si, root);
            tMax = max(tMax, siz[v]);
        }
    }
    tMax = max(tMax, si - siz[u]);
    if (tMax < rootMax) {
        root = u;
        rootMax = tMax;
    }
    vis[u] = false;
}

void findRoot(int u) {
    rootMax = n + 1;
    basicFindRoot_getSiz(u);
    basicFindRoot_getRoot(u, siz[u], root[u]);
    // printf("Found : root[%d] = %d\n", u, root[u]);
}

void basic_pretreatRoot(int u) {
    vis[u] = true;
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i].first;
        if (!vis[v]) {
            findRoot(v);
            // root[root[v]] = root[v];
            basic_pretreatRoot(root[v]);
        }
    }
    vis[u] = false;
}

void pretreatRoot(int u) {
    findRoot(u);
    MainRoot = root[u];
    // root[root[u]] = root[u];
    basic_pretreatRoot(root[u]);
}

void flatten(int u, int Dis) {
    vis[u] = true;
    h[cnt++] = Dis;
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i].first, w = G[u][i].second;
        if (!vis[v]) {
            flatten(v, Dis + w);
        }
    }
    vis[u] = false;
}

int calcAnswer(int u, int Dis) {
    cnt = 0;
    flatten(u, Dis);
    sort(h, h + cnt);
    int res = 0;
    // printf("flatten : { %d", h[0]);
    // for (int i = 1; i < cnt; i++) printf(", %d", h[i]);
    // printf(" }\n");
    for (int i = 0, j = cnt - 1; i < j; i++) {
        while (i < j && h[i] + h[j] > k) j--;
        res += j - i;
    }
    // printf("calcAnswer(%d, %d) = %d\n", u, Dis, res);
    return res;
}
int C = 0, ta, t1, t2;
void solve(int u) {
    // printf("solve(%d)\n", u);
    C++;
    // if (rand() < 10) printf("C = %05d/%d... with ta = %d, t1 = %d, t2 = %d\n", C % n, n, clock(), t1, clock() - t1);
    ans += calcAnswer(u, 0);
    vis[u] = true;
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i].first, w = G[u][i].second;
        if (!vis[v]) {
            ta = clock();
            ans -= calcAnswer(v, w);
            t1 += clock() - ta;
            findRoot(v);
            solve(root[v]);
        }
    }
    vis[u] = false;
}

signed main() {
    n = read<int>();
    k = read<int>();
    // Q = read<int>();
    for (int i = 1, u, v; i < n; i++) {
        u = read<int>();
        v = read<int>();
        // w = read<int>();
        G[u].push_back(make_pair(v, 1));
        G[v].push_back(make_pair(u, 1));
    }
    pretreatRoot(1);
    // printf("Mainly... Where's wrong? t = %d\n", clock());
    // for (int i = 1; i <= n; i++) printf("root[%d] = %d\n", i, root[i]);
    // for (int i = 1, lans; i <= Q; i++) {
    int lans;
    ans = 0;
    solve(MainRoot);
    // printf("solve : k = %d, gain %d\n", k, ans);
    if (!ans) puts("0");
    else {
        lans = ans;
        ans = 0;
        k--;
        solve(MainRoot);
        // printf("solve : k = %d, gain %d\n", k, ans);
        // puts(ans ^ lans ? "AYE" : "NAY");
        write(lans - ans, 10);
    }
    // }
    return 0;
}

// Create File Date : 2020-06-11
/*
7 10
1 6 13 
6 3 9 
3 5 7 
4 1 3 
2 4 20 
4 7 2 
1
2
3
4
5
6
7
8
9
10
*/