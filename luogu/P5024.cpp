//*todo
/*************************************
 * problem:      P5024 保卫王国.
 * user ID:      85848.
 * user name:    hkxadpall.
 * time:         2019-08-31.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define ForInVec(vectorType, vectorName, iteratorName) for (vector<vectorType>::iterator iteratorName = vectorName.begin(); iteratorName != vectorName.end(); iteratorName++)
#define ForInVI(vectorName, iteratorName) ForInVec(int, vectorName, iteratorName)
#define ForInVE(vectorName, iteratorName) ForInVec(Edge, vectorName, iteratorName)
#define MemWithNum(array, num) memset(array, num, sizeof(array))
#define Clear(array) MemWithNum(array, 0)
#define MemBint(array) MemWithNum(array, 0x3f)
#define MemInf(array) MemWithNum(array, 0x7f)
#define MemEof(array) MemWithNum(array, -1)
#define ensuref(condition) do { if (!(condition)) exit(0); } while(0)

#ifdef DEBUG
# define passing() cerr << "passing line [" << __LINE__ << "]." << endl
# define debug(...) printf(__VA_ARGS__)
# define show(x) cerr << #x << " = " << x << endl
#else
# define passing() do if (0) cerr << "passing line [" << __LINE__ << "]." << endl;  while(0)
# define debug(...) do if (0) printf(__VA_ARGS__); while(0)
# define show(x) do if (0) cerr << #x << " = " << x << endl; while(0)
#endif

int n, m;
char type[3];
int p[100007];
vector<int> G[100007];
long long f[100007][2];
int a, x, b, y;

void treeDP(int u, int fa)
{
    bool hasSon = false;
    f[u][1] = p[u];
    ForInVI(G[u], it) {
        if (*it == fa) continue;
        hasSon = true;
        treeDP(*it, u);
        f[u][0] += f[*it][1];
        f[u][1] += min(f[*it][1], f[*it][0]);
    }
    if (!hasSon) {
        // f[u][0] = 0;
        f[u][1] = p[u];
    }
    if (u == a) {
        if (x) f[u][0] = 0x3f3f3f3f;
        else f[u][1] = 0x3f3f3f3f;
    } else if (u == b) {
        if (y) f[u][0] = 0x3f3f3f3f;
        else f[u][1] = 0x3f3f3f3f;
    }
    debug("node %d : f = {%lld, %lld}\n", u, f[u][0], f[u][1]);
}

/*
   1(2)
    |
    |
   5(9) 
   / \
  /   \
2(4)  3(1)
       |
       |
      4(3)
*/

int main()
{
    scanf("%d%d%s", &n, &m, type);
    for (int i = 1; i <= n; i++) {
        scanf("%d", p + i);
    }
    for (int i = 1; i < n; i++) {
        scanf("%d%d", &a, &b);
        G[a].push_back(b);
        G[b].push_back(a);
    }
    bool noAnswer;
    for (int i = 1; i <= m; i++) {
        scanf("%d%d%d%d", &a, &x, &b, &y);
        if (!x && !y) {
            noAnswer = false;
            ForInVI(G[a], it) {
                if (*it == b) {
                    puts("-1");
                    noAnswer = true;
                }
            }
            if (noAnswer) continue;
        }
        Clear(f);
        treeDP(1, 0);
        printf("%lld\n", min(f[1][0], f[1][1]));
    }
    return 0;
}