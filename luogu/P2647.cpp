/*************************************
 * problem:      P2647.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-06-19.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

struct Things {
    int w, r;

    void readIn()
    {
        w = read<int>();
        r = read<int>();
    }

    bool operator < (const Things &other) const 
    {
        return r > other.r;
    }
};

int n;
Things a[3007];
int f[3007][3007] = {0}, ans = 0;

#define max3(a, b, c) max(max(a, b), c)


int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        a[i].readIn();
    }
    sort(a + 1, a + n + 1);
    for (int i = 1; i < n; i++) { 
        for (int j = 1; j <= n; j++) {
            f[i][j] = max(f[i - 1][j], f[i - 1][j - 1] + a[i].w - a[i].r * (j - 1));
            // printf("f[%d][%d] = %d.\n", i, j, f[i][j]);
        }
    }
    for (int j = 1; j <= n; j++) {
        ans = max3(ans, f[n - 1][j], f[n - 1][j - 1] + a[n].w - a[n].r * (j - 1));
        // printf("f[%d][%d] = %d.\n", n, j, f[n][j]);
    }
    write(ans);
    return 0;
}
