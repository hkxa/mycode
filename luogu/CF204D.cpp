/*************************************
 * @problem:      Little Elephant and Retro Strings.
 * @author:       brealid.
 * @time:         2021-02-01.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 1e6 + 7, P = 1e9 + 7;

int n, k;
char s[N];
int __sum_X[N], power_2[N];
int f1[N], f2[N], f3[N];

int inv(int64 a) {
    int64 ret(1);
    for (int remain = P - 2; remain; remain >>= 1) {
        if (remain & 1) ret = ret * a % P;
        a = a * a % P;
    }
    return ret;
}

#define sum_X(l, r) (__sum_X[r] - __sum_X[(l) - 1])

int inv_pow2_sum_X(int l, int r) {
    return inv(power_2[sum_X(l, r)]);
}

signed main() {
    kin >> n >> k >> (s + 1);
    f1[0] = power_2[0] = 1;
    for (int i = 1; i <= n; ++i) {
        __sum_X[i] = __sum_X[i - 1] + (s[i] == 'X');
        power_2[i] = (power_2[i - 1] << 1) % P;
    }
    int64 ans = 0;
    for (int i = 1, lst_W = 0; i <= n; ++i) {
        lst_W = (s[i] == 'W') ? 0 : lst_W + 1;
        f1[i] = (s[i] == 'X') ? 2LL * f1[i - 1] % P : f1[i - 1];
        f2[i] = f2[i - 1];
        if (k <= lst_W && s[i - k] != 'B') {
            int64 sub = (i == k) ? 1 : f1[i - k - 1];
            f1[i] = (f1[i] + P - sub) % P;
            f2[i] = (f2[i] + sub * power_2[sum_X(i + 1, n)]) % P;
        }
    }
    f3[n + 1] = f3[n + 2] = 1;
    for (int i = n, lst_W = 0; i; --i) {
        lst_W = (s[i] == 'B') ? 0 : lst_W + 1;
        f3[i] = (s[i] == 'X') ? 2LL * f3[i + 1] % P : f3[i + 1];
        if (k <= lst_W && s[i + k] != 'W') {
            int64 sub = f3[i + k + 1];
            f3[i] = (f3[i] + P - sub) % P;
            ans = (ans + sub * f2[i - 1] % P * inv_pow2_sum_X(i, n) % P) % P;
        }
    }
    kout << ans << '\n';
    return 0;
}