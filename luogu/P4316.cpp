/*************************************
 * @problem:      P4316 绿豆蛙的归宿.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-01-22.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, m;
vector<pair<int, int> > G[100007];
int indeg[100007], rest_indeg[100007];
double f[100007];

void push(int u)
{
    for (unsigned i = 0; i < G[u].size(); i++) {
        f[G[u][i].first] += (f[u] + G[u][i].second) / indeg[G[u][i].first];
        rest_indeg[G[u][i].first]--;
        if (!rest_indeg[G[u][i].first]) push(G[u][i].first);
    }
}

int main()
{
    n = read<int>();
    m = read<int>();
    for (int i = 1, a, b, c; i <= m; i++) {
        a = read<int>();
        b = read<int>();
        c = read<int>();
        G[b].push_back(make_pair(a, c));
        rest_indeg[a]++;
        indeg[a]++;
    }
    f[n] = 0;
    push(n);
    // for (int i = 1; i <= n; i++) {
    //     printf("f[%d] = %.2lf\n", i, f[i]);
    // }
    printf("%.2lf\n", f[1]);
    return 0;
} 