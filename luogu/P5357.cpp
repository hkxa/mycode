#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

namespace Jalg {
    int TJ[200000 + 7];
    const int charSize = 26;
    const char baseChar = 'a';

    struct ACtrie {
        ACtrie *fail, *son[charSize];
        int count;
        string originalString;
        bool found;
        int inputId;
        int fa_sonId; // 在爸爸那里自己的ID，即本节点代表的字母
        ACtrie() : fail(NULL), count(0), found(0), fa_sonId('~' - baseChar)
        {
            memset(son, 0, sizeof(son));
        }
    };

    ACtrie *root = new ACtrie;

    void insert(string s, int inputId)
    {
        ACtrie *p = root;
        for (string::iterator it = s.begin(); it != s.end(); it++) {
            if (!p->son[*it - baseChar]) {
                p->son[*it - baseChar] = new ACtrie;
                p->son[*it - baseChar]->fa_sonId = *it - baseChar;
            }
            // printf("insert %c after %c.\n", *it, p->fa_sonId + baseChar);
            p = p->son[*it - baseChar];
        }
        p->count++;
        p->originalString = s;
        p->inputId = inputId;
    }

    void make_fail()
    {
        queue<ACtrie*> q;
        // puts("CNT 60");
        for (int i = 0; i < charSize; i++) {
            if (root->son[i]) {
                // printf("CNT 63 - %c.\n", i + baseChar);
                q.push(root->son[i]);
                root->son[i]->fail = root;
            }
        }
        // puts("CNT 68");
        while (!q.empty()) {
            ACtrie *p = q.front();
            q.pop();
            // printf("q ===p=o=p==> %c.\n", p->fa_sonId + baseChar);
            for (int i = 0; i < charSize; i++) {
                if (!p->son[i]) continue;
                // printf("[%c]'s son %c\n", p->fa_sonId + baseChar, i + baseChar);
                ACtrie *f = p->fail;
                while (true) {
                    if (f->son[i]) {
                        p->son[i]->fail = f->son[i];
                        // printf("char %c:find my pFail!\n", i + baseChar);
                        break;
                    }
                    if (f == root) {
                        p->son[i]->fail = root;
                        break;
                    }
                    f = f->fail;
                }
                q.push(p->son[i]);
            }
        }
        // puts("CNT 88");
    }

    void build_ACtrie(vector<string> sArr)
    {
        int count = 0;
        for (vector<string>::iterator it = sArr.begin(); it != sArr.end(); it++) {
            insert(*it, count++);
        }
        make_fail();
    }

    void match(string txt) 
    {
        ACtrie *u = root, *v;
        // int ans(0);
        for (string::iterator it = txt.begin(); it != txt.end(); it++) {
            // printf("pos %c:\n", *it);

            // if (u->count && !u->found) {
            //     printf("ans = %d + %d.\n", ans, u->count);
            //     ans += u->count;
            //     u->found = true;
            //     u = u->fail;
            // } else {
            //     while (!u->son[*it - baseChar]) u = u->fail;
            //     v = u = u->son[*it - baseChar];
            // }
            if (u->son[*it - baseChar]) u = u->son[*it - baseChar];
            else {
                while (!u->son[*it - baseChar] && u != root) {
                    u = u->fail;
                }
                if (u->son[*it - baseChar]) {
                    u = u->son[*it - baseChar];
                }
            }
            
            v = u;
            // while (v->count && !v->found) {
            while (v != root) {
                // printf("ans = %d + %d.\n", ans, v->count);
                // ans += v->count;
                // v->found = true;
                if (v->count) TJ[v->inputId]++;
                v = v->fail;
            }
            // u = v;

            // while (v->count && !v->found) {
            //     printf("ans = %d + %d.\n", ans, v->count);
            //     ans += v->count;
            //     v->found = true;
            //     v = v->fail;
            // }
        }
        // return ans;
    }

    void clean(ACtrie *rt = root) 
    {
        rt->originalString = "";
        rt->count = 0;
        rt->inputId = 0;
        rt->fail = NULL;
        for (int i = 0; i < charSize; i++) {
            if (rt->son[i]) {
                clean(rt->son[i]);
                // delete rt->son[i];
            }
        }
        // if (rt == root) {
        //     delete rt;
        //     rt = new ACtrie;
        // }
    }
};

struct forSort {
    string s;
    int id;
    int val;
    bool operator < (const forSort &other) const {
        // return val ^ other.val ? val > other.val : id < other.id;
        return id < other.id;
    }
} a[200000 + 7];

int main()
{
    int n = read<int>();
    memset(Jalg::TJ, 0, sizeof(Jalg::TJ));
    Jalg::clean();
    string s, txt;
    vector<string> v;
    for (int i = 1; i <= n; i++) {
        cin >> s;
        v.push_back(s);
    }
    Jalg::build_ACtrie(v);
    cin >> txt;
    Jalg::match(txt);
    for (int i = 0; i < n; i++) {
        a[i] = (forSort){v[i], i, Jalg::TJ[i]};
    }
    sort(a, a + n);
    int i = 0;
    while (i < n) {
        write(a[i++].val, 10);
    }
    return 0;
}