/*************************************
 * problem:      CF1186C Vus the Cossack and Strings.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-06-29.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

char a[1000007], b[1000007];
int n, l, ans = 0;
bool c = true;

int main()
{
    scanf("%s", a);
    scanf("%s", b);
    n = strlen(a);
    l = strlen(b);
    for (int i = 0; i < l; i++) {
        if (b[i] == '1') c = !c;
    }
    for (int i = 0; i < l; i++) {
        if (a[i] == '1') c = !c;
    }
    // printf("c:%d\n", c);
    for (int i = 0; i <= n - l; i++) {
        if (c) ans++;
        if (a[i] == '1') c = !c;
        if (a[i + l] == '1') c = !c;
    }
    write(ans, 10);
    return 0;
}