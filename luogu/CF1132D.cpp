//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Painting the Fence.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-09.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

namespace against_cpp11 {
    const int N = 2e5 + 7;
    int n, k;
    int64 a[N], S;
    int b[N];
    int64 state[N];
    vector<int> charge[N];
    // struct state {
    //     int64 a, tim;
    //     int b;
    //     state(int64 u, int v) : a(u % v), tim(u / v), b(v) {}
    //     bool operator < (const state &b) const {
    //         return tim > b.tim;
    //     }
    // };
    // bool check(int64 X) {
    //     priority_queue<state> q;
    //     for (int i = 1; i <= n; i++) q.push(state(a[i], b[i]));
    //     int now = 0;
    //     while (!q.empty() && now < k) {
    //         if (q.top().tim < now++) return false;
    //         state u = q.top();
    //         q.pop();
    //         q.push(state(u.a + u.b * u.tim + X, u.b));
    //     }
    //     return true;
    // }
    bool check(int64 X) {
        for (int i = 0; i < k; i++) charge[i].clear();
        for (int i = 1; i <= n; i++) {
            state[i] = a[i];
            if (state[i] / b[i] < k)
                charge[state[i] / b[i]].push_back(i);
        }
        int tim = 0, j = 0;
        // printf("check(%lld)\n", X);
        while (j < k) {
            for (size_t i = 0; i < charge[j].size(); i++) {
                if (tim++ > j) return false;
                const int &u = charge[j][i];
                state[u] += X;
                // printf("tim %d : charge %d (need to be done by tim %d) from %lld to %lld\n", tim, u, j + 1, state[u] - (tim - 1) * b[u] - X, state[u] - (tim - 1) * b[u]);
                if (state[u] / b[u] < k)
                    charge[state[u] / b[u]].push_back(u);
            }
            j++;
        }
        return true;
    }
    signed main() {
        read >> n >> k;
        k--;
        for (int i = 1; i <= n; i++) {
            read >> a[i];
            // a[i]++;
        }
        for (int i = 1; i <= n; i++) {
            read >> b[i];
            S += b[i];
        }
        int64 l = 0, r = 2e12 + 1, mid, ans = -1;
        while (l <= r) {
            mid = (l + r) >> 1;
            if (check(mid)) r = mid - 1, ans = mid;
            else l = mid + 1;
        }
        write << ans << '\n';
        return 0;
    }
}

signed main() { return against_cpp11::main(); }