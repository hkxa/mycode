/*************************************
 * @problem:      Addition on Segments.
 * @author:       brealid.
 * @time:         2021-02-03.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 1e4 + 7;

int n, q;

bitset<N> tr[N << 2], ans;
vector<int> op[N << 2];

void add_op(int u, int l, int r, int ml, int mr, int x) {
    if (l >= ml && r <= mr) {
        op[u].push_back(x);
        return;
    }
    int mid = (l + r) >> 1;
    if (mid >= ml) add_op(u << 1, l, mid, ml, mr, x);
    if (mid < mr) add_op(u << 1 | 1, mid + 1, r, ml, mr, x);
}

void solve(int u, int l, int r) {
    for (int x: op[u]) tr[u] |= tr[u] << x;
    if (l == r) {
        ans |= tr[u];
        return;
    }
    int mid = (l + r) >> 1, ls = u << 1, rs = ls | 1;
    tr[ls] = tr[rs] = tr[u];
    solve(ls, l, mid), solve(rs, mid + 1, r);
}

signed main() {
    kin >> n >> q;
    for (int i = 1, l, r, x; i <= q; ++i) {
        kin >> l >> r >> x;
        add_op(1, 1, n, l, r, x);
    }
    tr[1].set(0);
    solve(1, 1, n);
    int cnt = 0;
    for (int x = ans._Find_next(0); x <= n; x = ans._Find_next(x)) ++cnt;
    kout << cnt << '\n';
    for (int x = ans._Find_next(0); x <= n; x = ans._Find_next(x))
        kout << x << ' ';
    kout << '\n';
    return 0;
}