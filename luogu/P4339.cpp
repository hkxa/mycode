//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      id_name.
 * @user_id:      63720.
 * @user_name:    brealid.
 * @time:         2020-mm-dd.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// #define int int64

int T;
int64 m, K;

int64 gcd(int64 m, int64 n) {
    return !n ? m : gcd(n, m % n);
}

int64 solve(int64 r, int64 t, int64 K) {
    int64 g = gcd(m, K), K_ = K / g, m_ = m / g;
    if (g == 1 || r <= K_) return 0;
    if ((double)K_ / m_ < t) return r - K_;
    t *= m_;
    return solve(K_ - t, t, K_) + r - K_;
} 

signed main()
{
    T = read<int>();
    while (T--) {
        m = read<int64>();
        K = read<int64>();
        int64 g = gcd(m, K), K_ = K / g;
        write(K - solve(K - 1, 1, K), 10);
    }
    return 0;
}

/*
![col-释.PNG](https://i.loli.net/2020/05/28/4iBNLTpul8ek1Ys.png)
![m2k4-1.PNG](https://i.loli.net/2020/05/28/VK8ypSsOA4fhYH9.png)
![m2k4-2.PNG](https://i.loli.net/2020/05/28/QBijkSXCFGZ1Kl6.png)
![m6k8-1.PNG](https://i.loli.net/2020/05/28/83U4mx5pAfGeLkS.png)
![m6k8-2.PNG](https://i.loli.net/2020/05/28/N3T7uEMWpzZB92F.png)
![m2k8-1.PNG](https://i.loli.net/2020/05/28/AZYbGrfgD8N9ClR.png)
![m2k8-2.PNG](https://i.loli.net/2020/05/28/GcAs1dNBXMKi5xg.png)
![m2k8-3.PNG](https://i.loli.net/2020/05/28/6yZGAf8H2crbJtF.png)
![m3k3-1.PNG](https://i.loli.net/2020/05/28/zQpynxAokwlZSI9.png)
![m3k3-2.PNG](https://i.loli.net/2020/05/28/Ghgw43kiKF2WufB.png)
*/