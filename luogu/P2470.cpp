/*************************************
 * problem:      P2470 [SCOI2007]压缩.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-13.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, f[50 + 3][50 + 3][2]; 
char s[50 + 3];

int dfs(int l, int r, int op) 
{
    if (f[l][r][op] != -1) return f[l][r][op];
    if (l == r) return f[l][r][op] = 2;
    int i, res = 0x3f3f3f3f; 
    if (op) {
        res = r - l + 2; 
        int mid = (l + r) >> 1; 
        bool flag = (r - l) & 1;
        for (i = l; i <= mid; i++)
            if (s[i] != s[mid + i - l + 1]) flag = 0;
        if (flag) res = min(res, dfs(l, mid, 1) + 1);
        for (i = l; i < r; i++) res = min(res, dfs(l, i, 1) + r - i);
    } else for (i = l; i < r; i++) {
        res = min(res, dfs(l, i, 0) + dfs(i + 1, r, 0));
        if (i + 1 < r) res = min(res, dfs(l, i, 0) + dfs(i + 1, r, 1));
        if (l < i) res = min(res, dfs(l, i, 1) + dfs(i + 1, r, 0));
        if (l < i && i + 1 < r) res = min(res, dfs(l, i, 1) + dfs(i + 1, r, 1));
    }
    return f[l][r][op] = res;
}

int main() 
{
    memset(f, -1, sizeof(f));
    scanf("%s", s + 1); 
    n = strlen(s + 1);
    dfs(1, n, 0); 
    dfs(1, n, 1);
    write(min(f[1][n][0], f[1][n][1]) - 1, 10);
    return 0;
}