//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      最大异或和.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-03.
 * @language:     C++.
*************************************/ 
#pragma GCC optimize(3)
#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

namespace against_cpp11 {
    const int N = 6e5 + 7, SIZ = 5e7 + 7;

    int node_cnt;
    int ch[SIZ][2], end[SIZ];
    int root[N];
    int n, m, XOR;

    inline char GetOpt() {
        static char buffer;
        while (!isupper(buffer = getchar()));
        return buffer;
    }

    void XOR_insert(int lst, int &u, int k, int num, int id) {
        if (k < 0) {
            end[u] = id;
            return;
        }
        int wh = (num >> k) & 1;
        ch[u][!wh] = ch[lst][!wh];
        XOR_insert(ch[lst][wh], ch[u][wh] = ++node_cnt, k - 1, num, id);
        end[u] = max(end[ch[u][0]], end[ch[u][1]]);
    }

    int XOR_find(int u, int k, int num, int limit) {
        if (k < 0) return 0;
        int wh = (num >> k) & 1;
        if (ch[u][!wh] && end[ch[u][!wh]] >= limit)
            return XOR_find(ch[u][!wh], k - 1, num, limit) | (1 << k);
        else 
            return XOR_find(ch[u][wh], k - 1, num, limit);
    }

    signed main() {
        read >> n >> m;
        root[0] = ++node_cnt;
        // XOR_insert(node_cnt, node_cnt, 23, XOR, i);
        for (int i = 1, a; i <= n; i++) {
            read >> a;
            XOR ^= a;
            XOR_insert(root[i - 1], root[i] = ++node_cnt, 23, XOR, i);
        }
        for (int i = 1, l, r, x; i <= m; i++) {
            if (GetOpt() == 'A') {
                read >> x;
                XOR ^= x;
                XOR_insert(root[n], root[n + 1] = ++node_cnt, 23, XOR, n + 1);
                ++n;
            } else {
                read >> l >> r >> x;
                write << max(XOR_find(root[r - 1], 23, XOR ^ x, l - 1), (l == 1 ? (XOR ^ x) : 0)) << '\n';
            }
        }
        return 0;
    }
}

signed main() { return against_cpp11::main(); }