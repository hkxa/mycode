/*************************************
 * @problem:      P3195 [HNOI2008]玩具装箱.
 * @user_id:      63720.
 * @user_name:    brealid.
 * @time:         2020-05-16.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 
#include <bits/stdc++.h>
using namespace std;
typedef long long int64;

int n, L, C[50007];
int64 sum[50007], dp[50007];
int q[50007], head = 1, tail = 0;

#define a(i) (sum[i] + (i))
#define b(i) (sum[i] + (i) + L + 1)
#define x(i) (b(i))
#define y(i) (dp[i] + b(i) * b(i))
#define slope(i, j) ((double)(y(i) - y(j)) / (x(i) - x(j)))

#define pushBack(i) (q[++tail] = (i))
#define popFront() void(head++)
#define popBack() void(tail--)
#define front() slope(q[head], q[head + 1])
#define back() slope(q[tail - 1], q[tail])
#define newer(i) slope(q[tail - 1], i)
#define exist() (head < tail)

/*
exist as queue head
*/

int main()
{
    scanf("%d%d", &n, &L);
    for (int i = 1; i <= n; i++) {
        scanf("%d", C + i);
        sum[i] = sum[i - 1] + C[i];
    }
    pushBack(0);
    for (int i = 1; i <= n; i++) {
        while (exist() && front() < 2 * a(i)) popFront();
        dp[i] = dp[q[head]] + (a(i) - b(q[head])) * (a(i) - b(q[head]));
        while (exist() && back() > newer(i)) popBack();
        pushBack(i);
        // printf("%lld%c", dp[i], " \n"[i == n]);
    }
    printf("%lld\n", dp[n]);
    return 0;
}