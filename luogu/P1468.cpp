/*************************************
 * problem:      P1468 派对灯 Party Lamps.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-11-09.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, c;
int status[6], int_status = 0;
int t;
int cost[64];
const int xor_way[4] = {63, 42, 21, 36};
bool isok, noAns = 1;
int main()
{
    n = read<int>();
    c = read<int>();
    for (t = read<int>(); ~t; t = read<int>()) {
        status[t % 6] = 2;
    }
    for (t = read<int>(); ~t; t = read<int>()) {
        if (status[t % 6] == 2) {
            // printf("88\n");
            puts("IMPOSSIBLE");
            return 0;
        } else {
            status[t % 6] = 1;
        }
    }
    // for (int j = 1; j <= 6; j++) {
    //     putchar(status[j % 6] | 48);
    // }
    // putchar(10);
    memset(cost, 0x3f, sizeof(cost));
    queue<int> q;
    q.push(63);
    cost[63] = 0;
    while (!q.empty()) {
        int fr = q.front();
        q.pop();
        for (int i = 0; i < 4; i++) {
            if (cost[fr ^ xor_way[i]] == 0x3f3f3f3f) {
                cost[fr ^ xor_way[i]] = cost[fr] + 1;
                q.push(fr ^ xor_way[i]);
            }
        }
    }
    for (int i = 0; i <= 63; i++) {
        // printf("cost %d = %d.\n", i, cost[i]);
        isok = 1;
        for (int j = 1; j <= 6; j++) {
            if (status[j % 6] && (status[j % 6] - 1) != (bool)(i & (1 << (6 - j)))) {
                // printf("fail when %d digit %d (my = %d, i = %d).\n", i, j, (status[j % 6] - 1), (bool)(i & (1 << (6 - j))));
                isok = 0;
                break;
            }
        }
        // if (isok) printf("%d : sOk. cost = %d.\n", i, cost[i]);
        if (isok && cost[i] <= c) {
            if (c - cost[i] == 1) continue;
            noAns = 0;
            for (int j = 1; j <= n; j++) {
                putchar((bool)(i & (1 << (6 - ((j - 1) % 6 + 1)))) | 48);
            }
            putchar(10);
        }
    }
    if (noAns) puts("IMPOSSIBLE");
    return 0;
}