/*************************************
 * problem:      P1828 香甜的黄油 Sweet Butter.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-11-11.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

struct Graph {
    struct Node {
        int u, val;
        bool operator < (const Node &other) const
        {
            return val > other.val;
        }
    };
    struct Edge {
        int v, w;
    } e[1450 * 2 + 3];
    int head[800 + 3], next[1450 * 2 + 3], edgeCnt;

    Graph() : edgeCnt(0)
    {
        memset(head, -1, sizeof(head));
    }

    void addEdge(int u, int v, int w)
    {
        e[++edgeCnt] = (Edge){v, w};
        next[edgeCnt] = head[u];
        head[u] = edgeCnt;
        e[++edgeCnt] = (Edge){u, w};
        next[edgeCnt] = head[v];
        head[v] = edgeCnt;
    }

    void dij(int n, int s, int *dis)
    {
        memset(dis, 0x3f, sizeof(int) * (n + 1));
        dis[s] = 0;
        priority_queue<Node> q;
        q.push((Node){s, 0});
        while (!q.empty()) {
            while (q.top().val != dis[q.top().u] && !q.empty()) q.pop();
            if (q.empty()) break;
            int fr = q.top().u;
            q.pop();
            #define to e[i].v
            #define va e[i].w
            for (int i = head[fr]; ~i; i = next[i]) {
                if (dis[to] - va > dis[fr]) {
                    dis[to] = dis[fr] + va;
                    q.push((Node){to, dis[to]});
                }
            }
            #undef to
            #undef va
        }
    }
} G;

int n, p, c;
int cnt[800 + 3];
int dis[800 + 3];
int ans = 0x7fffffff, sum;

int main()
{
    int u, v, w;
    n = read<int>();
    p = read<int>();
    c = read<int>();
    while (n--) cnt[read<int>()]++;
    while (c--) {
        u = read<int>();
        v = read<int>();
        w = read<int>();
        G.addEdge(u, v, w);
    }
    for (int i = 1; i <= p; i++) {
        // if (cnt[i]) {
            G.dij(p, i, dis);
            sum = 0;
            for (int j = 1; j <= p; j++) {
                sum += dis[j] * cnt[j];
            }
            ans = min(ans, sum);
        // }
    }
    write(ans, 10);
    return 0;
}