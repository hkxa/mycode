/**
 * - $author = brealid
 * Do not mind how heavily it is raining,
 * as there's always sun shinning after the rain passed.
 * 
 * NOIP.countdown_day = 2
 * NOIP.rp++
 */
#include <bits/stdc++.h>

namespace fastio {
    namespace base {
        template<typename I> inline void get_int(I &x) { 
            static char ch = 0;
            bool negative = false;
            while (!isdigit(ch = getchar()) && ch != '-' && ch != EOF);
            if (ch == '-') {
                negative = true;
                x = getchar() & 15;
            } else x = ch & 15;
            while (isdigit(ch = getchar())) x = (((x << 2) + x) << 1) + (ch & 15);
            if (negative) x = -x;
        }
        template<typename I> inline void get_uint(I &x) { 
            static char ch = 0;
            while (!isdigit(ch = getchar()) && ch != EOF);
            x = ch & 15;
            while (isdigit(ch = getchar())) x = (((x << 2) + x) << 1) + (ch & 15);
        }
        inline void get_str(char *str) { 
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            if (*str != EOF)
                while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
        }
        inline void get_cpp_str(std::string &str) { 
            str.clear();
            char cc;
            while (((cc = getchar()) == ' ' || cc == '\n' || cc == '\r' || cc == '\t') && cc != EOF);
            while (cc != ' ' && cc != '\n' && cc != '\r' && cc != '\t' && cc != EOF) {
                str.push_back(cc);
                cc = getchar();
            }
        }
        inline void get_ch(char &ch) { 
            while ((ch = getchar()) == ' ' || ch == '\n' || ch == '\r' || ch == '\t');
        }
        template<typename I> inline void attach_int(I x) { 
            static char buf[23];
            static int top = 0;
            if (x < 0) putchar('-'), x = -x;
            do {
                buf[++top] = '0' | (x % 10);
                x /= 10;
            } while (x);
            while (top) putchar(buf[top--]);
        }
        template<typename I> inline void attach_uint(I x) { 
            static char buf[23];
            static int top = 0;
            do {
                buf[++top] = '0' | (x % 10);
                x /= 10;
            } while (x);
            while (top) putchar(buf[top--]);
        }
        inline void attach_str(const char *str) { 
            while (*str) putchar(*str++);
        }
    }
    struct InputStream {
        InputStream& operator >> (int &x) { base::get_int(x); return *this; }
        InputStream& operator >> (long long &x) { base::get_int(x); return *this; }
        InputStream& operator >> (unsigned &x) { base::get_uint(x); return *this; }
        InputStream& operator >> (unsigned long long &x) { base::get_uint(x); return *this; }
        InputStream& operator >> (char &x) { base::get_ch(x); return *this; }
        InputStream& operator >> (char *x) { base::get_str(x); return *this; }
        InputStream& operator >> (std::string &x) { base::get_cpp_str(x); return *this; }
    };
    struct OutputStream {
        OutputStream& operator << (const int &x) { base::attach_int(x); return *this; }
        OutputStream& operator << (const long long &x) { base::attach_int(x); return *this; }
        OutputStream& operator << (const unsigned &x) { base::attach_uint(x); return *this; }
        OutputStream& operator << (const unsigned long long &x) { base::attach_uint(x); return *this; }
        OutputStream& operator << (const char &x) { putchar(x); return *this; }
        OutputStream& operator << (const char *x) { base::attach_str(x); return *this; }
        OutputStream& operator << (const std::string &x) { base::attach_str(x.c_str()); return *this; }
        void put(const char &c) { putchar(c); }
        void flush() { fflush(stdout); }
    };
}
fastio::InputStream kin;
fastio::OutputStream kout;
template<typename T> T read() { printf("Error type for template-read: Not supportive.\n"); exit(1); }
template<> int read() { int x; kin >> x; return x; }
template<> long long read() { long long x; kin >> x; return x; }
template<> unsigned read() { unsigned x; kin >> x; return x; }
template<> unsigned long long read() { unsigned long long x; kin >> x; return x; }
template<> char read() { return getchar(); }
template<> std::string read() { std::string x; kin >> x; return x; }

inline bool query(int fr, int to) {
    static int answer = 0;
    kout << "? " << fr << ' ' << to << '\n';
    kout.flush();
    kin >> answer;
    return answer;
}

int sum[2005];

int main() {
    int n = read<int>(), p = 1;
    int now = sum[1] = query(1, 1);
    if (sum[1] == 1) p = 2;
    for (int i = 2; i <= n; ++i) {
        while (p < i) {
            if (!query(i, p)) break;
            now = p * (p + 1) / 2;
            ++p;
        }
        if (p == i && query(i, i)) {
            sum[i] = now = i * (i + 1) / 2;
            ++p;
            continue;
        }
        int l = 1, r = p - 1, mid, ans = p;
        while (l <= r) {
            mid = (l + r) >> 1;
            if (query(mid, p)) ans = mid, r = mid - 1;
            else l = mid + 1;
            query(p, mid);
        }
        sum[i] = now + p - ans;
    }
    kout << "! ";
    for (int i = 1; i <= n; ++i)
        kout << sum[i] - sum[i - 1] << (i == n ? '\n' : ' ');
    return 0;
}