/*************************************
 * @user_id:      ZJ-00071.
 * @time:         2020-04-25.
 * @language:     C++.
 * @upload_place: NOI Online.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 998244353;

int n, m;
char belong[5007];
vector<int> G[5007];
int dfn[5007], end[5007], dfnCnt = 0;
int fa[5007], siz[3][5007];
// int lis[2][2507], lisCnt[2], vis[2507];
int64 f[5007][5007], tmp[5007], ans[2507];
int64 C[5007][5007];

// int fac(int n)
// {
//     int res = 1;
//     for (int i = 2; i <= n; i++) res = (int64)res * i % P;
//     return res;
// }
int64 fpow(int64 a, int n)
{
    int64 res = 1;
    while (n) {
        if (n & 1) res = res * a % P;
        a = a * a % P;
        n >>= 1;
    }
    return res;
}
#define inv(x) fpow(x, P - 2)

void dfs(int u, int fa) {
    f[u][0] = 1;
    for (uint32 i = 0; i < G[u].size(); i++) {
        if (G[u][i] == fa) continue;
        const int &v = G[u][i];
        dfs(v, u);
        memset(tmp, 0, sizeof(tmp));
        for (int i = 0; i <= siz[2][u]; i++) 
            for (int j = 0; j <= siz[2][v]; j++) 
                tmp[i + j] = (tmp[i + j] + f[u][i] * f[v][j]) % P;
        siz[0][u] += siz[0][v];
        siz[1][u] += siz[1][v];
        siz[2][u] += siz[2][v];
        for (int i = 0; i <= siz[2][u]; i++)
            f[u][i] = tmp[i];
    }
    if (belong[u]) {
        siz[1][u]++;
		for (int i = siz[0][u]; i > 0; i--)
			f[u][i] = (f[u][i] + f[u][i - 1] * (siz[0][u] - i + 1)) % P;
    } else {
        siz[0][u]++;
		for (int i = siz[1][u]; i > 0; i--)
			f[u][i] = (f[u][i] + f[u][i - 1] * (siz[1][u] - i + 1)) % P;
    }
    siz[2][u]++;
}

void _transform(int64 *A, int64 *B) {
    int64 Fac = 1;
    for (int i = m - 1; i >= 0; i--) {
        Fac = Fac * (m - i) % P;
        A[i] = A[i] * Fac % P;
    }
    // for (int i = 0; i <= m; i++) write(A[i], i == m ? 10 : 32);
    for (int i = 0; i <= m; i++) 
        for (int j = i; j <= m; j++) 
            if ((j - i) & 1) B[i] = (B[i] - C[j][i] * A[j] % P + P) % P;
            else B[i] = (B[i] + C[j][i] * A[j]) % P;
}

int main()
{
    // freopen("match.in", "r", stdin);
    // freopen("match.out", "w", stdout);
    n = read<int>();
    m = n / 2;
    scanf("%s", belong + 1);
    for (int i = 1; i <= n; i++) {
        belong[i] &= 1;
        // lis[belong[i]][++lisCnt[belong[i]]] = i;
    }
    for (int i = 1, u, v; i < n; i++) {
        u = read<int>();
        v = read<int>();
        G[u].push_back(v);
        G[v].push_back(u);
    }
    // C[1][0] = C[1][1] = 1;
    C[0][0] = 1;
    for (int i = 1; i <= m; i++) {
        C[i][0] = C[i][i] = 1;
        for (int j = 1; j < i; j++) {
            C[i][j] = (C[i - 1][j - 1] + C[i - 1][j]) % P;
            // printf("C(%d, %d) = %lld\n", i, j, C[i][j]);
        }
    }
    dfs(1, 0);
    // for (int i = 1; i <= n; i++)
    //     for (int j = 0; j <= m; j++) 
    //         write(f[i][j], j == m ? 10 : 32);
    _transform(f[1], ans);
    // ans[0] = 0;
    for (int i = 0; i <= m; i++) write(ans[i], 10);
    return 0;
}

/*
8
10010011
1 2
1 3
2 4
2 5
5 6
3 7
3 8

8 
10010011
1 2
2 6
6 8
8 3
3 5
5 7
7 4
*/