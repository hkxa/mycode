/*************************************
 * problem:      %s.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-**-**.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  
/*
100000000888888879
100000000888888879 
*/

#define times10(a) (((a << 2) + a) << 1)

int k,m;
int base, len;
long long mi[22];
long long ans;

int calc(int k) 
{ 
    char s[12];
    sprintf(s, "%d", k);
    int ans(0), w(0);
    len = strlen(s);
    for (int i = 0; i < len; i++) {
        w = times10(w) + (s[i] & 15);
        ans -= mi[i] - w - 1;
    }
    return ans; 
}

int main()
{
    mi[0] = 1;
    for (int i = 1; i < 19; i++) mi[i] = times10(mi[i - 1]);
    k = read<int>();
    m = read<int>();
    for (int i = 0; i < 10; i++) {
        if (k == mi[i] && m != i + 1){
            puts("0");
            return 0;
        }
    }
    base = calc(k);
    if (m < base) {
        puts("0");
        return 0;
    }
    if (m == base) {
        write(k, 10);
        return 0;
    }
    ans = mi[len];
    m -= base;
    for (int i = 1; ; i++) {
        long long tmp = k * mi[i] - mi[len + i - 1];
        if (m > tmp) {
            m -= tmp; 
            ans = times10(ans);
        }
        else break;
    }
    ans = ans + m;
    ans--;
    write(ans, 10);
    return 0;
}