/*************************************
 * @problem:      Beautiful numbers.
 * @author:       brealid.
 * @time:         2021-01-30.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 2520;
int t, book[N + 1];
vector<int> num;
int64 l, r, f[25][N + 1][50]; //特别注意：DP数组一定要开long long类型，不然会炸

inline int gcd(int x, int y) { return x ? gcd(y % x, x) : y; }
inline int lcm(int x, int y) { return x * y / gcd(x, y); }

int64 dfs(int pos, int sum_now, int now_lcm, bool is_bounded) {
    if (pos == -1) return sum_now % now_lcm == 0;
    if (!is_bounded && f[pos][sum_now][book[now_lcm]] != -1) return f[pos][sum_now][book[now_lcm]];
    int64 ans = 0;
    for (int i = is_bounded ? num[pos] : 9; i >= 0; --i) {
        int next_sum = (sum_now * 10 + i) % N;
        int next_lcm = now_lcm;
        if (i) next_lcm = lcm(next_lcm, i);
        ans += dfs(pos - 1, next_sum, next_lcm, is_bounded && i == num[pos]);
    }
    if (!is_bounded) f[pos][sum_now][book[now_lcm]] = ans;
    return ans;
}

inline int64 work(int64 now) {
    num.clear();
    while (now) {
        num.push_back(now % 10);
        now /= 10;
    }
    return dfs(num.size() - 1, 0, 1, true);
}

int main() {
    memset(f, -1, sizeof(f));
    int num = 0;
    for (int i = 1; i <= N; ++i)
        if (N % i == 0)
            book[i] = ++num;
    for (int cases = kin.get<int>(); cases--;) {
        kin >> l >> r;
        kout << work(r) - work(l - 1) << '\n';
    }
    return 0;
}