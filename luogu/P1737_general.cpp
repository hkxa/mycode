/*************************************
 * problem:      P1737 [NOI2016]旷野大计算. [general]
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2019-04-05.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

int main()
{
    char buf[107] = {0};
    FILE *outf = fopen("tempOut.txt", "w");
    while (scanf("%[^\n]", buf) != EOF) {
        fprintf(outf, "\rputs(\"%s\");", buf); getchar();
    }
    return 0;
}