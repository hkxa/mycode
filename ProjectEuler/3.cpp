#include <bits/stdc++.h>

const long long solve = 600851475143LL;

int main()
{
    int SqrtSolve = sqrt(solve);
    long long largest = 0, now = solve;
    for (int i = 2; i <= SqrtSolve; i++) {
        while (now % i == 0) {
            now /= i;
            largest = i;
        }
    }
    if (now > 1) largest = now;
    printf("%lld\n", largest);
}