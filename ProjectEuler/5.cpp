#include "ProjectEuler.hpp"

int64 gcd(int64 a, int64 b)
{
    return a ? gcd(b % a, a) : b;
}

#define lcm(a, b) ((a) * (b) / gcd((a), (b)))

int main()
{
    int64 ans = 1;
    for (int i = 1; i <= 20; i++) ans = lcm(ans, i);
    printf("%d\n", ans);
}