#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

#define lcm(a, b) ((a) * (b) / gcd((a), (b)))
#define C_2(n) ((n) * ((n) + 1) / 2)
#define sqr(x) ((x) * (x))

bool IsPalindrome(int64 a) // 回文数判断
{
    static int buf[10], n;
    n = 0;
    while (a) {
        buf[++n] = a % 10;
        a /= 10;
    }
    for (int i = 1, j = n; i < j; i++, j--) if (buf[i] != buf[j]) return 0;
    return 1;
}

int64 gcd(int64 a, int64 b)
{
    return a ? gcd(b % a, a) : b;
}

inline int64 SumSpr1toN(int n)
{
    register int64 res = 0;
    for (int i = 1; i <= n; i++) res += sqr(i);
    return res;
}

#ifdef DefinePrimeEuler 
    bool isnp[DefinePrimeEuler + 5];                             
    int primes[DefinePrimeEuler + 5], cnt = 0;                   
    void getPrimes()                                
    {                                               
        for (int i = 2; i <= DefinePrimeEuler; i++) {              
            if (!isnp[i]) {                         
                primes[++cnt] = i;                  
                for (int j = i + i; j <= DefinePrimeEuler; j += i) 
                    isnp[j] = true;                 
            }                                       
        }                                           
    }                                               
    #define IsPrime(x) (!isnp[x])
    #define getKthPrime(x) primes[x]
#endif