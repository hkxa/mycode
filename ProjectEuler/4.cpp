#include "ProjectEuler.hpp"
#define LOW 100
#define HIGH 999

int main()
{
    int ans = 0;
    for (int i = LOW; i <= HIGH; i++) 
        for (int j = LOW; j <= HIGH; j++) {
            if (i * j > ans && IsPalindrome(i * j)) ans = i * j;
        }
    printf("%d\n", ans);
}