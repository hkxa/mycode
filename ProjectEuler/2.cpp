#include <bits/stdc++.h>
using namespace std;

int main()
{
    long long ans = 0;
    int f1 = 0, f2 = 1, f3 = 1;
    while (f3 < 4000000) {
        f3 = f1 + f2;
        if (!(f3 & 1)) ans += f3;
        f1 = f2;
        f2 = f3;
    }
    printf("%lld\n", ans);
    return 0;
}