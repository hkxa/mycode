template <typename _T, int lb, int rb>
struct signed_array {
    typedef _T          value_type;
    typedef size_t      size_type;
    typedef _T*         iterator;
    typedef const _T*   const_iterator;
    typedef _T&         reference;
    typedef const _T&   const_reference;
    value_type ARR_DATA[rb - lb + 1];
    size_type size() const { return rb - lb + 1; }
    reference operator[] (int get_pos) { return ARR_DATA[get_pos - lb]; }
    reference front() { return ARR_DATA[0]; }
    reference back() { return ARR_DATA[rb - lb]; }
    iterator begin() { return ARR_DATA; }
    iterator end() { return ARR_DATA + rb - lb; }
    const_reference operator[] (int get_pos) const { return ARR_DATA[get_pos - lb]; }
    const_reference front() const { return ARR_DATA[0]; }
    const_reference back() const { return ARR_DATA[rb - lb]; }
    const_iterator begin() const { return ARR_DATA; }
    const_iterator end() const { return ARR_DATA + rb - lb; }
    void memset(int val = 0) { ::memset(ARR_DATA, val, sizeof(ARR_DATA)); }
};
 