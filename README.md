# mycode

这是一个私人项目，保存以洛谷为主的，各大OJ上的我的代码

欢迎围观，拒绝抄袭

欢迎借鉴

## todo-list

见 [todo-list.md](todo-list.md)


### Git Commands
```
git clone https://gitee.com/hkxa/mycode
git commit -m "commit"
git pull --tags origin master
git push origin master:master
```

## 文件夹组织结构 (链接去往存放该OJ代码的文件夹，OJ地址自行百度)

*旧版，尚未更新*

1. 题库类  
   1. [XJOI(dev)](XJOI(dev)/)  
   2. [luogu](luogu/) (包含Codeforces,Uva,SPOJ,AtCoder题目)  
   3. [YBTOJ](YBTOJ/)  
   4. [noi.openjudge.cn](noi.openjudge.cn/)  
   5. [csp.ac](csp.ac/)  
   6. [CCFOJ](CCFOJ/)  
   7. [AtCoder](AtCoder/)  
   8. [[special] contest](contest/)
2. 个人测试类  
   1. [interesting-test](interesting-test/)  
3. 库类  
   1. [algorithm](algorithm/)  
   2. [lib-tool](lib-tool/)  
   3. [testlib](testlib/)  
4. 功能文件夹
   1. [.git](.git/)  
   2. [.vscode](.vscode/)

### Documentary

算法文档等 https://gitee.com/hkxa/OI-documentary

Novel https://www.cnblogs.com/CQzhangyu/p/8757042.html

QQ"退群杯" 1046196050

[Idle Space](http://idlegame.gitee.io/idlespace2)
[MiKTeX @vscode](https://blog.csdn.net/yinqingwang/article/details/79684419)
[latex studio](https://www.latexstudio.net/archives/51742.html)