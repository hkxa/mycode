#include <bits/stdc++.h>

namespace NTT {
    typedef long long int64;
    typedef unsigned long long uint64;
    namespace arr {
        template<typename T> inline void clear(T *f, int n) {
            for (int i = 0; i < n; ++i) f[i] = 0;
        }
        template<typename T> inline void copy(T *Dst, const T *Src, int n) {
            for (int i = 0; i < n; ++i) Dst[i] = Src[i];
        }
    }
    namespace math {
        template <typename T> inline T phi(T n) {
            T zc = n, all = sqrt(n);
            for (T i = 2; i <= all; i++) {
                if (n % i != 0) continue;
                zc = zc / i * (i - 1);
                while (n % i == 0) n /= i;
            }
            if (n > 1) zc = zc / n * (n - 1);
            return zc;
        }
        inline int64 pow(int64 x, int64 y, const int64 mod) {
            int64 res = 1;
            while (y) {
                if (y & 1) res = res * x % mod;
                x = x * x % mod;
                y >>= 1;
            }
            return res;
        }
        inline int64 inv(int64 x, int64 mod) {
            return pow(x, mod - 2, mod);
        }
    }
    namespace primitive_root {
        int G(const int64 m) {
            int64 factor[100], PHI = math::phi(m), tmp = PHI;
            int limit = sqrt(PHI), cnt = 0;
            for (int i = 2; i <= limit; i++)
                if (tmp % i == 0) {
                    factor[++cnt] = PHI / i;
                    while (tmp % i == 0) tmp /= i;
                }
            if (tmp > 1) factor[++cnt] = PHI / tmp;
            for (int g = 2; ; ++g) {
                if (math::pow(g, PHI, m) != 1) continue;
                bool isok = 1;
                for (int i = 1; isok && i <= cnt; i++)
                    if (math::pow(g, factor[i], m) == 1) isok = false;
                if (isok) return g;
            }
        }
    }
    namespace bit {
        inline int highest(int x) {
            if (x == 0) return -1;
            int ret = 0;
            if (x >= 65536) x >>= 16, ret += 16;
            if (x >= 256) x >>= 8, ret += 8;
            if (x >= 16) x >>= 4, ret += 4;
            if (x >= 4) x >>= 2, ret += 2;
            if (x >= 2) x >>= 1, ret += 1;
            return ret;
        }
    }
    const int N = 2.1e6 + 7;
    const int P = 998244353, g = primitive_root::G(P), inv_g = math::inv(g, P);
    namespace c_style {
        int bt[N], NTT_temp[N];
        void init_butterflyTransfer(int n) {
            bt[0] = 0;
            for (int i = 1; i < n; ++i)
                bt[i] = (bt[i >> 1] >> 1) | ((i & 1) ? n >> 1 : 0);
        }
        void poly_NTT(int *dxs, int n, int op) {
            static uint64 f[N], w[N] = {1};
            for (int i = 0; i < n; ++i) f[i] = dxs[bt[i]];
            for (int step = 1; step < n; step <<= 1) {
                int len = step << 1, unityRoot = math::pow(op ? g : inv_g, (P - 1) / len, P);
                for (int i = 1; i <= step; ++i) w[i] = w[i - 1] * unityRoot % P;
                for (int k = 0; k < n; k += len) {
                    int R = k | step;
                    for (int pos = 0; pos < step; ++pos) {
                        int tt = w[pos] * f[R | pos] % P;
                        f[R | pos] = f[k | pos] - tt + P;
                        f[k | pos] += tt;
                    }
                }
                if (step == (1 << 10))
                    for (int i = 0; i < n; ++i) f[i] %= P;
            }
            if (!op) {
                int64 inv_n = math::inv(n, P);
                for (int i = 0; i < n; ++i) dxs[i] = f[i] * inv_n % P;
            } else {
                for (int i = 0; i < n; ++i) dxs[i] = f[i] % P;
            }
        }
        inline void poly_basicOp_addTo(int *Dst, const int *Src, int n) {
            for (int i = 0; i < n; ++i) Dst[i] = (Dst[i] + Src[i]) % P;
        }
        inline void poly_basicOp_subTo(int *Dst, const int *Src, int n) {
            for (int i = 0; i < n; ++i) Dst[i] = (Dst[i] - Src[i] + P) % P;
        }
        inline void poly_basicOp_mulTo(int *Dst, const int *Src, int n) {
            for (int i = 0; i < n; ++i) Dst[i] = (int64)Dst[i] * Src[i] % P;
        }
        void poly_convolution_add(int *Dst, const int *Src, int n) {
            int len = 1 << (bit::highest(n) + 2);
            init_butterflyTransfer(len);
            arr::clear(NTT_temp, len);
            arr::copy(NTT_temp, Src, n);
            poly_NTT(Dst, len, 1), poly_NTT(NTT_temp, len, 1);
            poly_basicOp_mulTo(Dst, NTT_temp, len);
            poly_NTT(Dst, len, 0);
            arr::clear(Dst + n, len - n);
        }
    }
    typedef std::vector<int> poly;
    inline poly operator + (const poly &a, const poly &b) {
        poly res(a);
        c_style::poly_basicOp_addTo(res.data(), b.data(), a.size());
        return res;
    }
    inline poly operator - (const poly &a, const poly &b) {
        poly res(a);
        c_style::poly_basicOp_subTo(res.data(), b.data(), a.size());
        return res;
    }
    inline poly operator * (const poly &a, const poly &b) {
        static int A[N], B[N];
        int n1 = a.size(), n2 = b.size();
        int len = 1 << (bit::highest(n1 + n2 - 1) + 1);
        arr::clear(A, len), arr::clear(B, len);
        arr::copy(A, a.data(), n1), arr::copy(B, b.data(), n2);
        c_style::poly_convolution_add(A, B, n1 + n2 - 1);
        poly res(n1 + n2 - 1);
        arr::copy(res.data(), A, n1 + n2 - 1);
        return res;
    }
}

using namespace NTT;

void TestPoly() {
    int n, m;
    scanf("%d%d", &n, &m);
    poly a(n + 1), b(m + 1);
    for (int i = 0; i <= n; ++i) scanf("%d", &a[i]);
    for (int i = 0; i <= m; ++i) scanf("%d", &b[i]);
    poly ret1 = a * b;
    for (int i = 0; i <= n + m; ++i) printf("%d%c", ret1[i], " \n"[i == n + m]);
    // poly ret2 = a * a;
    // for (int i = 0; i <= n + n; ++i) printf("%d%c", ret2[i], " \n"[i == n + n]);
    // poly ret3 = b * b;
    // for (int i = 0; i <= m + m; ++i) printf("%d%c", ret3[i], " \n"[i == m + m]);
}


int main() {
    // TestArr();
    TestPoly();
    return 0;
}