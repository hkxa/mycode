#include <bits/stdc++.h>

namespace FFT {
    struct cplex {
        double re, im;
        cplex() : re(0), im(0) {}
        cplex(double Real, double Imag) : re(Real), im(Imag) {}
        cplex operator + (const cplex &b) const { return cplex(re + b.re, im + b.im); }
        cplex operator - (const cplex &b) const { return cplex(re - b.re, im - b.im); }
        cplex operator * (const cplex &b) const { return cplex(re * b.re - im * b.im, re * b.im + im * b.re); }
        cplex operator * (const double &b) const { return cplex(re * b, im * b); }
        cplex operator / (const double &b) const { return cplex(re / b, im / b); }
    };

    inline cplex unityRoot(int k, int n) {
        const static double pi2 = 2 * acos(-1);
        double angle = k * pi2 / n;
        return cplex(cos(angle), sin(angle));
    }

    const int N = 2.1e6 + 7;

    int bt[N];
    void init_butterflyTransfer(int n) {
        bt[0] = 0;
        for (int i = 1; i < n; ++i)
            bt[i] = (bt[i >> 1] >> 1) | ((i & 1) ? n >> 1 : 0);
    }

    void fft(cplex *f, int n, int flag) {
        // Butterfly Transfer
        for (int i = 0; i < n; ++i)
            if (i < bt[i]) std::swap(f[i], f[bt[i]]);
        // FFT
        for (int step = 1; step < n; step <<= 1) {
            int len = step << 1;
            cplex unit = unityRoot(flag, len);
            for (int s = 0; s < n; s += len) {
                cplex now(1, 0);
                for (int p = s; p < s + step; ++p) {
                    cplex rightPart = now * f[p + step];
                    f[p + step] = f[p] - rightPart;
                    f[p] = f[p] + rightPart;
                    now = now * unit;
                }
            }
        }
    }

    void mul_naive(int n, cplex *a, cplex *b, cplex *res) {
        fft(a, n, 1), fft(b, n, 1);
        for (int i = 0; i < n; ++i) res[i] = a[i] * b[i];
        fft(res, n, -1);
        for (int i = 0; i < n; ++i) res[i] = res[i] / n;
    }

    void mul_self(int n, cplex *a) {
        fft(a, n, 1);
        for (int i = 0; i < n; ++i) a[i] = a[i] * a[i];
        fft(a, n, -1);
        for (int i = 0; i < n; ++i) a[i] = a[i] / n;
    }

    namespace sample {
        void P3803_naive() {
            // 【模板】多项式乘法（FFT）普通3次暴力
            int n, m;
            scanf("%d%d", &n, &m);
            int len = 1;
            while (len < n + m + 1) len <<= 1;
            init_butterflyTransfer(len);
            cplex *a = new cplex[len], *b = new cplex[len], *res = new cplex[len];
            for (int i = 0; i <= n; ++i) scanf("%lf", &a[i].re);
            for (int i = 0; i <= m; ++i) scanf("%lf", &b[i].re);
            mul_naive(len, a, b, res);
            for (int i = 0; i <= n + m; ++i) printf("%d%c", int(res[i].re + 0.49), " \n"[i == n + m]);
            delete[] a;
            delete[] b;
            delete[] res;
        }
        void P3803_3to2() {
            // 【模板】多项式乘法（FFT）3次变2次优化
            int n, m;
            scanf("%d%d", &n, &m);
            int len = 1;
            while (len < n + m + 1) len <<= 1;
            init_butterflyTransfer(len);
            cplex *f = new cplex[len];
            for (int i = 0; i <= n; ++i) scanf("%lf", &f[i].re);
            for (int i = 0; i <= m; ++i) scanf("%lf", &f[i].im);
            mul_self(len, f);
            for (int i = 0; i <= n + m; ++i) printf("%d%c", int(f[i].im + 0.49) >> 1, " \n"[i == n + m]);
            delete[] f;
        }
        void P1919() {
            char a[1000007], b[1000007], ans[2000007];
            scanf("%s%s", a, b);
            int lena = strlen(a), lenb = strlen(b);
            memset(ans, 0, sizeof(ans));
            int n = 1;
            while (n < lena + lenb) n <<= 1;
            init_butterflyTransfer(n);
            cplex *f = new cplex[n];
            for (int i = 0; i < lena; ++i) f[i].re = a[lena - i - 1] & 15;
            for (int i = 0; i < lenb; ++i) f[i].im = b[lenb - i - 1] & 15;
            mul_self(n, f);
            long long remain = 0;
            for (int i = 0; i <= lena + lenb; ++i) {
                long long num = ((long long)(f[i].im + 0.99) >> 1) + remain;
                ans[lena + lenb - i] = (num % 10) | 48;
                remain = num / 10;
            }
            char *p = ans;
            while (*p == '0' && *(p + 1)) ++p;
            printf("%s\n", p);
            delete[] f;
        }
    }
}