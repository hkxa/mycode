template <typename T, typename Container = vector<T>, typename Comparer = less<T> >
class heap {
  private:
    priority_queue<T, Container, Comparer> que, del; 
    inline void push_del_tags() {
        while (!del.empty() && !que.empty() && del.top() == que.top())
            del.pop(), que.pop();
    }
  public:
    inline size_t size() {
        return que.size() - del.size();
    }
    inline bool empty() {
        return que.size() == del.size();
    }
    inline T top() {
        push_del_tags();
        return que.top();
    }
    inline void pop() {
        push_del_tags();
        que.pop();
    }
    inline void insert(const T &x) { que.push(x); }
    inline void erase(const T &x) { del.push(x); }
    inline void swap(heap &x) {
        que.swap(x.que);
        del.swap(x.del);
    }
};