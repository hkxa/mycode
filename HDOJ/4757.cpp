//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Tree.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-03.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

namespace against_cpp11 {
    const int N = 1e5 + 7, ARR_SIZ = 3e6 + 7;
    int n, m;
    vector<int> G[N];
    int w[N], dep[N], f[N][21];
    int siz[ARR_SIZ], ch[ARR_SIZ][2], trie_node_cnt;
    int root[N];
    void trie_insert(int u, int prev, int number, int k = 16) {
        siz[u] = siz[prev] + 1;
        if (k < 0) return;
        int p = (number >> k) & 1;
        ch[u][!p] = ch[prev][!p];
        trie_insert(ch[u][p] = ++trie_node_cnt, ch[prev][p], number, k - 1);
    }
    int trie_query(int x, int y, int z, int number, int k = 16) {
        if (k < 0) return 0;
        int p = (number >> k) & 1;
        // if (k <= 2) printf("(want, ch[%d]) Siz_ChX = %d, Siz_ChY = %d, Siz_ChZ = %d\n", !p, siz[ch[x][!p]], siz[ch[y][!p]], siz[ch[z][!p]]);
        // if (k <= 2) printf("(ch[%d]) Siz_ChX = %d, Siz_ChY = %d, Siz_ChZ = %d\n", p, siz[ch[x][p]], siz[ch[y][p]], siz[ch[z][p]]);
        if (siz[ch[x][!p]] + siz[ch[y][!p]] > 2 * siz[ch[z][!p]])
            return trie_query(ch[x][!p], ch[y][!p], ch[z][!p], number, k - 1) | (1 << k);
        else
            return trie_query(ch[x][p], ch[y][p], ch[z][p], number, k - 1);
    }
    void dfs(int u, int fa) {
        trie_insert(root[u] = ++trie_node_cnt, root[fa], w[u]);
        f[u][0] = fa;
        dep[u] = dep[fa] + 1;
        for (int i = 0; i < 19; i++)
            f[u][i + 1] = f[f[u][i]][i];
        for (size_t i = 0; i < G[u].size(); i++) {
            int &v = G[u][i];
            if (v != fa) dfs(v, u);
        }
    }
    int LCA(int u, int v) {
        if (dep[u] < dep[v]) swap(u, v);
        for (int k = 19; k >= 0; k--)
            if (dep[u] - dep[v] >= (1 << k)) u = f[u][k];
        if (u == v) return u;
        for (int k = 19; f[u][0] != f[v][0]; k--)
            if (f[u][k] != f[v][k]) {
                u = f[u][k];
                v = f[v][k];
            }
        return f[u][0];
    }
    void clear_datas() {
        trie_node_cnt = 0;
        for (int i = 1; i <= n; i++) G[i].clear();
        memset(root, 0, sizeof(root));
        memset(siz, 0, sizeof(siz));
        memset(ch, 0, sizeof(ch));
        memset(f, 0, sizeof(f));
    }
    signed main() {
        while (scanf("%d%d", &n, &m) == 2) {
            clear_datas();
            for (int i = 1; i <= n; i++) read >> w[i];
            for (int i = 1, a, b; i < n; i++) {
                read >> a >> b;
                G[a].push_back(b);
                G[b].push_back(a);
            }
            dfs(1, 0);
            for (int i = 1, x, y, z, val; i <= m; i++) {
                read >> x >> y >> val;
                z = LCA(x, y);
                // printf("Query %d in (trie(%d) + trie(%d) - 2 * trie(%d))\n", val, x, y, z);
                write << max(trie_query(root[x], root[y], root[z], val), val ^ w[z]) << '\n';
            }
        }
        return 0;
    }
}

signed main() { return against_cpp11::main(); }