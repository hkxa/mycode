//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      2020 年百度之星·程序设计大赛 - 复赛.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-09.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

namespace against_cpp11 {
    const int N = 2e5 + 7;
    const int effect_bound = 1 << 19;
    const int Mask = effect_bound - 1;
    int T, n;
    char s[N], t[N];
    int cnt0s[N], cnt1t[N], cntdif[N], upgrade[N];
    int tot, ans, now, eff;
    int num_s, num_g, add_tag = 0;;
    int ppcnt_table[] = {0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4};
    int pop_count(int u) {
        if (u < 16) return ppcnt_table[u];
        else return pop_count(u >> 4) + ppcnt_table[u & 15]; 
    }
    #define sum_dif(i, j) (cntdif[j] - cntdif[(i) - 1])
    signed main() {
        read >> T;
        while (T--) {
            scanf("%d%s%s", &n, s + 1, t + 1);
            n++;
            s[n + 2] = t[n + 2] = s[n + 1] = t[n + 1] = s[n] = t[n] = '0';
            for (int i = 1; i <= n; i++) {
                cnt0s[i] = cnt0s[i - 1] + (s[i] == '0');
                cnt1t[i] = cnt1t[i - 1] + (t[i] == '1');
                cntdif[i] = cntdif[i - 1] + (t[i] != s[i]);
            }
            cntdif[n + 2] = cntdif[n + 1] = cntdif[n];
            ans = cntdif[n];
            for (int i = 1; i <= n; i++) {
                ans = min(ans, (s[i + 1] == '1') + (t[i + 1] == '0') + cnt0s[i] + 1 + cnt1t[i] + sum_dif(i + 2, n + 2));
            }
            write << ans << '\n';
        }
        return 0;
    }
}

signed main() { return against_cpp11::main(); }