/*************************************
 * @problem:      fence.
 * @author:       赵奕.
 * @time:         2021-03-08.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 2e3 + 7, inf = 0x3f3f3f3f;

struct pos {
    int x, y;
    pos() {}
    pos(int X, int Y) : x(X), y(Y) {}
    bool operator < (const pos &b) const { return x != b.x ? x < b.x : y < b.y; }
} a[N];

int64 OuterProduct(const pos &a, const pos &b) {
    return (int64)a.x * b.y - (int64)a.y * b.x;
}

struct edge {
    int x, y, z;
    double o;
    edge() {}
    edge(int _x, int _y, int _z) : x(_x), y(_y), z(_z) { o = atan2(a[y].x - a[x].x, a[y].y - a[x].y); }
} e[N];

int n, m, cnt;
map<pos, int> PosID;

bool del[N];
int from[N];

namespace GetArea {
    struct cmp {
        bool operator()(int a, int b) { return e[a].o < e[b].o; }
    };
    set<int, cmp> g[N];
    set<int, cmp>::iterator k;
    int q[N], t;
    void work() {
        for (int i = 0; i < m + m; ++i)
            if (!del[i]) {
                for (int j = q[t = 1] = i;; q[++t] = j = *k) {
                    k = g[e[j].y].find(j ^ 1);
                    ++k;
                    if (k == g[e[j].y].end()) k = g[e[j].y].begin();
                    if (*k == i) break;
                }
                int64 s = 0;
                for (int j = 1; j <= t; ++j)
                    s += OuterProduct(a[e[q[j]].x], a[e[q[j]].y]), del[q[j]] = 1;
                if (s <= 0) continue;
                ++cnt;
                for (int j = 1; j <= t; ++j)
                    from[q[j]] = cnt;
            }
    }
}

namespace Net_MF {
    typedef int value_type;
    const int Net_Node = 2e3, Net_Edge = 2e3;
    const value_type inf = (value_type)0x3f3f3f3f3f3f3f3f; // 当 value_type 为 int 的时候，自动类型强转为 0x3f3f3f3f
    struct edge {
        int to, nxt_edge;
        value_type flow;
    } e[Net_Edge * 2 + 5];
    int depth[Net_Node + 5], head[Net_Node + 5], cur[Net_Node + 5], ecnt = 1;
    int node_total, st, ed;
    // 清零，此函数适用于多组数据
    void clear() {
        memset(head, 0, sizeof(int) * (node_total + 1));
        ecnt = 1;
        st = ed = 0;
    }
    // 添加边（正向边和反向边均会自动添加）
    inline void add_edge(const int &from, const int &to, const value_type &flow = (value_type)1) {
        // Add "positive going edge"
        e[++ecnt].to = to;
        e[ecnt].flow = flow;
        e[ecnt].nxt_edge = head[from];
        head[from] = ecnt;
        // Add "reversed going edge"
        e[++ecnt].to = from;
        e[ecnt].flow = 0;
        e[ecnt].nxt_edge = head[to];
        head[to] = ecnt;
    }
    // Dinic 算法 bfs 函数
    inline bool dinic_bfs() {
        memset(depth, 0x3f, sizeof(int) * (node_total + 1));
        memcpy(cur, head, sizeof(int) * (node_total + 1));
        std::queue<int> q;
        q.push(st);
        depth[st] = 0;
        while (!q.empty()) {
            int u = q.front(); q.pop();
            for (int i = head[u]; i; i = e[i].nxt_edge)
                if (depth[e[i].to] > depth[u] + 1 && e[i].flow) {
                    depth[e[i].to] = depth[u] + 1;
                    if (e[i].to == ed) return ed; // 后续 bfs 到的节点, depth 一定大于 ed, 没有丝毫用处
                    q.push(e[i].to);
                }
        }
        return false;
    }
    // 将所有流还原(即: 还原残量网络至原图)
    inline void restore_flow() {
        for (int i = 2; i <= ecnt; i += 2) {
            e[i].flow += e[i | 1].flow;
            e[i | 1].flow = 0;
        }
    }
    // Dinic 算法 dfs 函数
    value_type dinic_dfs(int u, value_type now) {
        if (u == ed) return now;
        value_type max_flow = 0, nRet;
        for (int &i = cur[u]; i && now; i = e[i].nxt_edge)
            if (depth[e[i].to] == depth[u] + 1 && (nRet = dinic_dfs(e[i].to, std::min(now, e[i].flow)))) {
                now -= nRet;
                max_flow += nRet;
                e[i].flow -= nRet;
                e[i ^ 1].flow += nRet;
            }
        return max_flow;
    }
    // Dinic 算法总工作函数，需要提供节点数（偏大影响时间），起始点（默认 1），结束点（默认 node_count）
    value_type dinic_work(int node_count, int start_node = 1, int finish_node = -1) {
        node_total = node_count;
        st = start_node;
        ed = ~finish_node ? finish_node : node_count;
        value_type max_flow = 0;
        while (dinic_bfs())
            max_flow += dinic_dfs(st, inf);
        return max_flow;
    }
}

namespace GeomoryHu_Tree {
    int a[N], t[N];
    int work(int l, int r) {
        if (l >= r) return 0;
        Net_MF::restore_flow();
        int cut = Net_MF::dinic_work(cnt + 1, a[l], a[l + 1]);
        int p1 = l - 1, p2 = r + 1;
        for (int i = l; i <= r; ++i)
            if (Net_MF::depth[a[i]] != Net_MF::inf) t[++p1] = a[i];
            else t[--p2] = a[i];
        memcpy(a + l, t + l, (r - l + 1) * sizeof(int));
        return cut + work(l, p1) + work(p2, r);
    }
}

inline int getid() {
    int x, y;
    kin >> x >> y;
    pos p(x, y);
    if (PosID[p]) return PosID[p];
    PosID[p] = ++n;
    a[n] = p;
    return n;
}

int main() {
    int T_case_count = kin.get<int>();
    for (int iCount = 1; iCount <= T_case_count; ++iCount) {
        n = cnt = 0;
        PosID.clear();
        kin >> m;
        for (int i = 0, x, y, z; i < m; ++i) {
            x = getid(), y = getid();
            kin >> z;
            e[i << 1] = edge(x, y, z);
            e[i << 1 | 1] = edge(y, x, z);
        }
        for (int i = 0; i < m + m; ++i)
            from[i] = del[i] = 0;
        for (int i = 1; i <= n; ++i)
            GetArea::g[i].clear();
        for (int i = 0; i < m + m; ++i)
            GetArea::g[e[i].x].insert(i);
        GetArea::work();
        Net_MF::clear();
        for (int i = 0; i < m + m; ++i)
            Net_MF::add_edge(from[i] + 1, from[i ^ 1] + 1, e[i].z);
        for (int i = 1; i <= cnt + 1; ++i) GeomoryHu_Tree::a[i] = i;
        printf("Case #%d: %d\n", iCount, GeomoryHu_Tree::work(1, cnt + 1));
    }
    return 0;
}