#include <stdio.h>

/*

确定进制(qdjz.cpp)
描述
6 * 9 = 42  对于十进制来说是错误的，但是对于 13 进制来说是正确的。即, 6(13)* 9(13)= 42(13)，   而  42(13)= 4 * 
131+ 2 * 130= 54(10)。
你的任务是写一段程序，读入三个整数 p、 q 和  r，然后确定一个进制  B(2<=B<=16)  使得  p * q = r。   如果  B  有
很多选择,  输出最小的一个。
例如： p = 11, q = 11, r = 121.  则有  11(3)* 11(3)= 121(3)因为  11(3)= 1 * 31+ 1 * 30= 4(10)和  121(3)= 1 * 32+ 2 * 31+ 
1 * 30= 16(10)。  对于进制  10，同样有  11(10)* 11(10)= 121(10)。这种情况下，应该输出  3。如果没有合适的进制，
则输出  0。
输入
一行，包含三个整数 p、q、r。  p、q、r 的所有位都是数字，并且 1 <= p、q、r <= 1,000,000。
输出
一个整数：即使得 p * q = r 成立的最小的 B。如果没有合适的 B，则输出  0。
样例输入
6 9 42
样例输出

*/

int qd(int i)
{
	int ans = 0;
	while (i) ans++, i/=10;
	return ans;
}

void puiz(int &ib)
{
	int tib = ib;
	int lon = qd(ib);
	int *bp = new int[lon];
	for (int i = 0; i < lon; i++) bp[lon-i-1] = tib%10, tib/=10;
	int ans = 0;
	while (lon--) ans = ans*10+bp[lon];
	ib = ans;
	//printf("DEBUG://ib = %d, lon = %d, ans = %d\n", ib, lon, ans);
}

bool qdjz(int jinzhi, int q, int p, int r)
{
	int aq = 0, ap = 0, ar = 0;
	//printf("\nDEBUG:jinzhi = %d\n", jinzhi);
	while (q) aq = aq*jinzhi+q%10, q/=10;
	//printf("aq = %d, q = %d\n", aq, q);
	while (p) ap = ap*jinzhi+p%10, p/=10;
	//printf("ap = %d, p = %d\n", ap, p);
	while (r) ar = ar*jinzhi+r%10, r/=10;
	//printf("ar = %d, r = %d\n", ar, r);
	return (aq*ap == ar);
}

int main()
{
	freopen("qdjz.in", "r", stdin);
	freopen("qdjz.out", "w", stdout);
	int q, p, r;
	scanf("%d%d%d", &q, &p, &r);
	puiz(q);
	puiz(p);
	puiz(r);
	for (int i = 2; i <= 16; i++) {
		if (qdjz(i, q, p, r)) {
			printf("%d", i);
			return 0;
		}
	}
	printf("0");
	return 0;
	
}
