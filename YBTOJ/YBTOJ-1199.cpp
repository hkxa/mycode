#include <iostream>
#include <stdio.h>
#include <cmath>
#include <string.h>
using namespace std;

typedef char Char[1001];
typedef bool Bool[1001];

#define OK true
#define NO false

Char ans, let;
Bool flag;
int len;

void dfshs(int num)
{
	for (int i = 0; i < len; i++) {
		if (!flag[let[i]]) {
			flag[let[i]] = OK;
			ans[num] = let[i];
			if (num == len-1) printf("%s\n", ans);
			else dfshs(num+1);
			flag[let[i]] = NO;
		}
	}
}

int main()
{
	scanf("%s", let);
	len = strlen(let);
	dfshs(0);
	return 0;
}
