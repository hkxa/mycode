#include <iostream>
#include <stdio.h>
#include <cmath>
#include <string.h>
using namespace std;

/*
题目：1202:Pell数列
作者：HKXA0933 
*/

int ans[1048575];

int work(int num)
{
	if (ans[num] != 0) return ans[num];
	if (num == 1) return ans[1] = 1;
	if (num == 2) return ans[2] = 2;
	return ans[num] = (2*work(num-1)+work(num-2))%32767;
}

int main()
{
	for (int i = 0; i < 1048576; i++) work(i);
	int num, ques;
	scanf("%d", &num);
	for (int i = 0; i < num; i++) {
		scanf("%d", &ques);
		printf("%d\n", ans[ques]);
	}
	return 0;
}
