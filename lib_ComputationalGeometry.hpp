const double eps = 1e-9;

struct vec {
    double x, y;
    vec() {}
    vec(double X, double Y) : x(X), y(Y) {}
    vec operator + (const vec &b) const { return vec(x + b.x, y + b.y); }
    vec operator - (const vec &b) const { return vec(x - b.x, y - b.y); }
    vec operator * (const double &k) const { return vec(x * k, y * k); }
    vec operator / (const double &k) const { return vec(x / k, y / k); }
    double len_sqr() const { return x * x + y * y; }
    double len() const { return sqrt(len_sqr()); }
    vec rotate_90() const { return vec(-y, x); }
};

struct line {
    vec p, v;
    line() {}
    line(vec P, vec V) : p(P), v(V) {}
    line(const double &K, const double &B) : p(0, B), v(1, K) {}
};

double dot(vec a, vec b) { return a.x * b.x + a.y * b.y; }
double crs(vec a, vec b) { return a.x * b.y - a.y * b.x; }

line plumb(vec a, vec b) {
    return line((a + b) * 0.5, (b - a).rotate_90());
}

vec intersect(line a, line b) {
    double t = crs((b.p - a.p), b.v) / crs(a.v, b.v);
    return a.p + a.v * t;
}

struct circle {
    vec o;
    double len_sqr;
    circle() {}
    circle(vec _o, double _len_sqr) : o(_o), len_sqr(_len_sqr) {}
    circle(vec a, vec b) {
        o = (a + b) * 0.5;
        len_sqr = (a - o).len_sqr();
    }
    circle(vec a, vec b, vec c) {
        if (abs(crs(b - a, c - a)) < eps) {
            double ab = (a - b).len_sqr();
            double bc = (b - c).len_sqr();
            double ac = (a - c).len_sqr();
            if (ab <= ac && bc <= ac) *this = circle(a, c);
            else if (ab <= bc && ac <= bc) *this = circle(b, c);
            else *this = circle(a, b);
        }
        o = intersect(plumb(a, b), plumb(a, c));
        len_sqr = (a - o).len_sqr();
    }
    bool is_include(vec p) const { return (p - o).len_sqr() < len_sqr; }
    double len() const { return sqrt(len_sqr); }
};