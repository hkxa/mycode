#include <bits/stdc++.h>
using namespace std;

class Reader {
public:
    char nxtch;
    Reader() : nxtch(0) {}
    template <typename T>
    Reader& operator >> (T &num) {
        bool negative = false;
        nxtch = getchar();
        while (nxtch < '0' || nxtch > '9') {
            if (nxtch == '-') negative = true;
            nxtch = getchar();
        }
        num = nxtch & 15;
        while ((nxtch = getchar()) >= '0' && nxtch <= '9') num = (((num << 2) + num) << 1) + (nxtch & 15);
        if (negative) num = ~num + 1;
        return *this;
    }
} read;

/*
-4712 年 1 月 1 日正午 12 点
1. 公元 1582 年 10 月 15 日（含）以后：适用格里高利历，每年一月 31 天、
二月 28 天或 29 天、三月 31 天、四月 30 天、五月 31 天、六月 30 天、七
月 31 天、八月 31 天、九月 30 天、十月 31 天、十一月 30 天、十二月 31
天。其中，闰年的二月为 29 天，平年为 28 天。当年份是 400 的倍数，或
日期年份是 4 的倍数但不是 100 的倍数时，该年为闰年。
2. 公元 1582 年 10 月 5 日（含）至 10 月 14 日（含）：不存在，这些日期被
删除，该年 10 月 4 日之后为 10 月 15 日。
3. 公元 1582 年 10 月 4 日（含）以前：适用儒略历，每月天数与格里高利历
相同，但只要年份是 4 的倍数就是闰年。
*/

void output(int64_t YY, int64_t MM, int64_t DD) {
    if (YY > 0) printf("%lld %lld %lld\n", DD, MM, YY);
    else printf("%lld %lld %lld BC\n", DD, MM, 1 - YY);
}

int64_t calc_month_RN(int64_t d) {
    if (d < 182) {
        if (d < 31) return 1;
        else if (d < 60) return 2;
        else if (d < 91) return 3;
        else if (d < 121) return 4;
        else if (d < 152) return 5;
        else return 6;
    } else {
        if (d < 213) return 7;
        else if (d < 244) return 8;
        else if (d < 274) return 9;
        else if (d < 305) return 10;
        else if (d < 335) return 11;
        else return 12;
    }
}

int64_t calc_month_not_RN(int64_t d) {
    if (d < 181) {
        if (d < 31) return 1;
        else if (d < 59) return 2;
        else if (d < 90) return 3;
        else if (d < 120) return 4;
        else if (d < 151) return 5;
        else return 6;
    } else {
        if (d < 212) return 7;
        else if (d < 243) return 8;
        else if (d < 273) return 9;
        else if (d < 304) return 10;
        else if (d < 334) return 11;
        else return 12;
    }
}

int64_t calc_day_RN(int64_t d) {
    if (d < 182) {
        if (d < 31) return d + 1;
        else if (d < 60) return d - 30;
        else if (d < 91) return d - 59;
        else if (d < 121) return d - 90;
        else if (d < 152) return d - 120;
        else return d - 151;
    } else {
        if (d < 213) return d - 181;
        else if (d < 244) return d - 212;
        else if (d < 274) return d - 243;
        else if (d < 305) return d - 273;
        else if (d < 335) return d - 304;
        else return d - 334;
    }
}

int64_t calc_day_not_RN(int64_t d) {
    if (d < 181) {
        if (d < 31) return d + 1;
        else if (d < 59) return d - 30;
        else if (d < 90) return d - 58;
        else if (d < 120) return d - 89;
        else if (d < 151) return d - 119;
        else return d - 150;
    } else {
        if (d < 212) return d - 180;
        else if (d < 243) return d - 211;
        else if (d < 273) return d - 242;
        else if (d < 304) return d - 272;
        else if (d < 334) return d - 303;
        else return d - 333;
    }
}

void julian_solve(int64_t d) {
    int64_t year_base = d / 1461 * 4 - 4712;
    int64_t according = d % 1461;
    if (according < 366) output(year_base, calc_month_RN(according), calc_day_RN(according));
    else if (according < 731) output(year_base + 1, calc_month_not_RN(according - 366), calc_day_not_RN(according - 366));
    else if (according < 1096) output(year_base + 2, calc_month_not_RN(according - 731), calc_day_not_RN(according - 731));
    else output(year_base + 3, calc_month_not_RN(according - 1096), calc_day_not_RN(according - 1096));
}

bool is_RN_glgl(int64_t year) {
    return year % 400 == 0 || ((year % 100) && year % 4 == 0);
}

void solve_1583to1599(int64_t d) {
    for (int64_t year = 1583; ; ++year) {
        if (is_RN_glgl(year)) {
            if (d < 366) {
                output(year, calc_month_RN(d), calc_day_RN(d));
                return;
            } else d -= 366;
        } else {
            if (d < 365) {
                output(year, calc_month_not_RN(d), calc_day_not_RN(d));
                return;
            } else d -= 365;
        } 
    }
}

int64_t calc_do(int64_t &d) {
    if (d < 36525) {
        // [0, 100)
        int64_t year_base = d / 1461 * 4;
        d %= 1461;
        if (d < 366) return year_base;
        else if (d < 731) {
            d -= 366;
            return year_base + 1;
        } else if (d < 1096) {
            d -= 731;
            return year_base + 2;
        } else {
            d -= 1096;
            return year_base + 3;
        }
    } else {
        int64_t y_base;
        if (d < 73049) {
            // [100, 200)
            y_base = 100;
            d -= 36525;
        } else if (d < 109573) {
            // [200, 300)
            y_base = 200;
            d -= 73049;
        } else {
            // [300, 400)
            y_base = 300;
            d -= 109573;
        }
        if (d < 365) return y_base;
        ++d;
        int64_t year_base = d / 1461 * 4;
        d %= 1461;
        if (d < 366) return y_base + year_base;
        else if (d < 731) {
            d -= 366;
            return y_base + year_base + 1;
        } else if (d < 1096) {
            d -= 731;
            return y_base + year_base + 2;
        } else {
            d -= 1096;
            return y_base + year_base + 3;
        }
    }
}

void geligaoli_solve(int64_t d) { // after 1583.1.1 d day(s)
    if (d < 6209) solve_1583to1599(d);
    else {
        d -= 6209;
        int64_t round400 = d / 146097 * 400 + 1600;
        int64_t restday = d % 146097;
        int64_t y = round400 + calc_do(restday);
        if (is_RN_glgl(y)) output(y, calc_month_RN(restday), calc_day_RN(restday));
        else output(y, calc_month_not_RN(restday), calc_day_not_RN(restday));
    }
}

signed main() {
    freopen("julian.in", "r", stdin);
    freopen("julian.out", "w", stdout);
    int64_t T, day;
    read >> T;
    while (T--) {
        read >> day;
        if (day <= 2299160) julian_solve(day); // 公元 1582 年 10 月 4 日（含）以前：适用儒略历
        else if (day <= 2299238) output(1582, calc_month_not_RN(day - 2298874), calc_day_not_RN(day - 2298874)); // 1582 年特殊处理
        else geligaoli_solve(day - 2299239); // Ge Li Gao Li Calender
    }
    return 0;
}