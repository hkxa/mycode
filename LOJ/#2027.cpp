/*************************************
 * @problem:      「SHOI2016」黑暗前的幻想乡.
 * @author:       brealid.
 * @time:         2020-12-03.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;
#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;


const int N = 18, M = 137, P = 1e9 + 7;

int64 inv(int64 a) {
    int64 ret(1);
    for (int k = P - 2; k; ((a *= a) %= P), k >>= 1)
        if (k & 1) (ret *= a) %= P;
    return ret;
}

namespace determinant {
    int64 resolve(int64 (*a)[N], int n) {
        int64 ans = 1;
        for (int i = 1; i <= n; ++i) {
            for (int j = i; j <= n; ++j)
                if (a[j][i]) {
                    if (i != j) {
                        swap(a[i], a[j]);
                        ans = P - ans;
                    }
                    break;
                }
            ans = ans * a[i][i] % P;
            int64 inv_aii = inv(a[i][i]);
            for (int j = i + 1; j <= n; ++j) {
                int64 factor = a[j][i] * inv_aii % P;
                for (int k = i; k <= n; ++k)
                    a[j][k] = (a[j][k] - factor * a[i][k] % P + P) % P;
            }
        }
        return ans;
    }
}

int n;
int deg[N];
int64 f[N][N];
struct company {
    int u[M], v[M], cnt;
    template<typename stream> inline void input(stream &in) {
        in >> cnt;
        for (int i = 1; i <= cnt; ++i) in >> u[i] >> v[i];
    }
    inline void contribute() {
        for (int i = 1; i <= cnt; ++i)
            --f[u[i]][v[i]], --f[v[i]][u[i]], ++f[u[i]][u[i]], ++f[v[i]][v[i]];
    }
} c[N];

signed main() {
    kin >> n;
    for (int i = 1; i < n; ++i) c[i].input(kin);
    int mask = 1 << (n - 1);
    int64 ans = 0;
    for (int b = 0; b < mask; ++b) {
        int removed = 0;
        memset(f, 0, sizeof(f));
        for (int i = 1; i < n; ++i)
            if ((b >> (i - 1)) & 1) c[i].contribute();
            else ++removed;
        if (removed & 1) ans -= determinant::resolve(f, n - 1);
        else ans += determinant::resolve(f, n - 1);
    }
    kout << (ans % P + P) % P << '\n';
    return 0;
}