#include <bits/stdc++.h>
#include "library.h"
using namespace std;

const int N = 1000 + 7;

int e1[N], e2[N];
vector<int> M, res;

void InsertEdge(int u, int v) {
    // printf("[info] InsertEdge(%d, %d)\n", u, v);
    if (e1[u]) e2[u] = v;
    else e1[u] = v;
    if (e1[v]) e2[v] = u;
    else e1[v] = u;
}

int CountNear(int x, int l, int r) {
    for (int i = l; i <= r; ++i) M[i - 1] = 1;
    int nRet1 = Query(M);
    M[x - 1] = 1;
    int nRet2 = Query(M);
    for (int i = l; i <= r; ++i) M[i - 1] = 0;
    M[x - 1] = 0;
    // printf("[info] CountNear(%d, %d, %d) = %d\n", x, l, r, nRet1 - nRet2 + 1);
    return nRet1 - nRet2 + 1;
}

void GetNear(int x, int l, int r, int nNear) {
    if (l == r) {
        InsertEdge(x, l);
        return;
    }
    int mid = (l + r) >> 1;
    int nNear_l = CountNear(x, l, mid), nNear_r = nNear - nNear_l;
    if (nNear_l) GetNear(x, l, mid, nNear_l);
    if (nNear_r) GetNear(x, mid + 1, r, nNear_r);
}

void DfsChain(int u, int fa) {
    res.push_back(u);
    if (!e2[u] && fa) return;
    if (e1[u] == fa) DfsChain(e2[u], u);
    else DfsChain(e1[u], u);
}

void Solve(int N) {
    if (N == 1) {
        Answer(vector<int>(1, 1));
        return;
    }
    M.resize(N, 0), res.reserve(N);
    for (int i = 1; i < N; ++i) {
        int nNear = CountNear(i, i + 1, N);
        if (nNear) GetNear(i, i + 1, N, nNear);
    }
    for (int i = 1; i <= N; ++i)
        if (!e2[i]) { DfsChain(i, 0); break; }
    Answer(res);
}