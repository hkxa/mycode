/*************************************
 * @problem:      [SDOI2016]游戏.
 * @author:       brealid.
 * @time:         2020-11-17.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#if true
#define getchar() (i_P1 == i_P2 && (i_P2 = (i_P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), i_P1 == i_P2) ? EOF : *i_P1++)
char BUF[1 << 21], *i_P1 = BUF, *i_P2 = BUF;
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        inline Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        inline Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        inline Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        inline Reader& operator >> (double &lf) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF && endch != '.') endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            if (endch != '.') {
                lf = (endch & 15);
                while (isdigit(endch = getchar())) lf = lf * 10 + (endch & 15);
            } else lf = 0;
            if (endch == '.') {
                double len = 1;
                while (isdigit(endch = getchar())) lf += (endch & 15) * (len *= 0.1);
            }
            if (flag) lf = -lf;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        inline Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        inline Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        inline Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        inline Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 100000 + 7;

struct Edge {
    int v, w;
    Edge(int V = 0, int W = 0) : v(V), w(W) {}
};

int n, m;
vector<Edge> G[N];
int fa[N], dep[N], siz[N];
Edge wson[N];
int beg[N], dfn[N], id[N], dft;
int64 dis[N];

struct funct {
    int64 k, b;
    funct() : k(0), b(123456789123456789LL) {}
    funct(const int64 &K, const int64 &B) : k(K), b(B) {}
    int64 operator () (int val) const {
        return k * dis[val] + b;
    }
} tr[N << 2];
int64 tr_min[N << 2];

#define form_a_function(x, y, k) funct((k), (y) - (k) * dis[x])

inline void pushup(const int &u, const int &l, const int &r) {
    if (l == r) tr_min[u] = tr[u](l);
    else {
        tr_min[u] = min(tr_min[u << 1], tr_min[u << 1 | 1]);
        if (tr[u].k > 0) tr_min[u] = min(tr_min[u], tr[u](l));
        else tr_min[u] = min(tr_min[u], tr[u](r));
    }
}

void init_build(const int &u, const int &l, const int &r) {
    tr_min[u] = 123456789123456789LL;
    if (l == r) return;
    int mid = (l + r) >> 1;
    init_build(u << 1, l, mid), init_build(u << 1 | 1, mid + 1, r);
}

void segt_insert(const int &u, const int &l, const int &r, const int &ml, const int &mr, const funct &f) {
    int mid = (l + r) >> 1;
    if (l >= ml && r <= mr) {
        if (tr[u].b == 123456789123456789LL) {
            tr[u] = f;
            pushup(u, l, r);
            return;
        }
        if (l == r) {
            if (f(l) < tr[u](l)) tr[u] = f;
            pushup(u, l, r);
            return;
        }
        if (f.k < tr[u].k) {
            if (f(mid) < tr[u](mid)) segt_insert(u << 1, l, mid, ml, mr, tr[u]), tr[u] = f;
            else segt_insert(u << 1 | 1, mid + 1, r, ml, mr, f);
        } else {
            if (f(mid) < tr[u](mid)) segt_insert(u << 1 | 1, mid + 1, r, ml, mr, tr[u]), tr[u] = f;
            else segt_insert(u << 1, l, mid, ml, mr, f);
        }
    } else {
        if (mid >= ml) segt_insert(u << 1, l, mid, ml, mr, f);
        if (mid < mr) segt_insert(u << 1 | 1, mid + 1, r, ml, mr, f);
    }
    pushup(u, l, r);
}

int64 segt_query(const int &u, const int &l, const int &r, const int &ml, const int &mr) {
    if (l >= ml && r <= mr) return tr_min[u];
    int mid = (l + r) >> 1;
    int64 result = min(tr[u](max(l, ml)), tr[u](min(r, mr)));
    if (mid >= ml) result = min(result, segt_query(u << 1, l, mid, ml, mr));
    if (mid < mr) result = min(result, segt_query(u << 1 | 1, mid + 1, r, ml, mr));
    return result;
}

void tcs_dfs_1(int u, int fat) {
    fa[u] = fat;
    dep[u] = dep[fat] + 1;
    siz[u] = 1;
    for (uint32 i = 0; i < G[u].size(); ++i) {
        int v = G[u][i].v;
        if (v != fat) {
            tcs_dfs_1(v, u);
            siz[u] += siz[v];
            if (siz[v] > siz[wson[u].v]) wson[u] = G[u][i];
        }
    }
}

void tcs_dfs_2(int u, int cbeg, int d = 0) {
    dfn[u] = ++dft;
    dis[dft] = dis[dft - 1] + d;
    beg[u] = cbeg;
    if (!wson[u].v) return;
    tcs_dfs_2(wson[u].v, cbeg, wson[u].w);
    for (uint32 i = 0; i < G[u].size(); ++i) {
        int v = G[u][i].v;
        if (v != fa[u] && v != wson[u].v)
            tcs_dfs_2(v, v, G[u][i].w);
    }
}

pair<int, int64> get_lca_and_dist(int u, int v) {
    int64 dist = 0;
    while (beg[u] != beg[v]) {
        if (dep[beg[u]] < dep[beg[v]]) swap(u, v);
        dist += dis[dfn[u]] - dis[dfn[beg[u]] - 1];
        u = fa[beg[u]];
    }
    if (dep[u] < dep[v]) swap(u, v);
    return make_pair(v, dist + dis[dfn[u]] - dis[dfn[v]]);
}

void update(int u, int g, int k, int64 b) {
    while (beg[u] != beg[g]) {
        segt_insert(1, 1, n, dfn[beg[u]], dfn[u], form_a_function(dfn[u], b, -k));
        b += k * (dis[dfn[u]] - dis[dfn[beg[u]] - 1]);
        u = fa[beg[u]];
    }
    segt_insert(1, 1, n, dfn[g], dfn[u], form_a_function(dfn[u], b, -k));
}

int64 query(int u, int v) {
    int64 ret = 123456789123456789LL;
    while (beg[u] != beg[v]) {
        if (dep[beg[u]] < dep[beg[v]]) swap(u, v);
        ret = min(ret, segt_query(1, 1, n, dfn[beg[u]], dfn[u]));
        u = fa[beg[u]];
    }
    if (dep[u] < dep[v]) swap(u, v);
    return min(ret, segt_query(1, 1, n, dfn[v], dfn[u]));
}

signed main() {
    kin >> n >> m;
    init_build(1, 1, n);
    for (int i = 1, u, v, w; i < n; ++i) {
        kin >> u >> v >> w;
        G[u].push_back(Edge(v, w));
        G[v].push_back(Edge(u, w));
    }
    tcs_dfs_1(1, 0);
    tcs_dfs_2(1, 1);
    for (int i = 1, opt, s, t, a, b; i <= m; ++i) {
        kin >> opt >> s >> t;
        if (opt == 1) {
            kin >> a >> b;
            pair<int, int64> info = get_lca_and_dist(s, t);
            update(s, info.first, a, b);
            update(t, info.first, -a, b + info.second * a);
        } else {
            kout << query(s, t) << '\n';
        }
    }
    return 0;
}