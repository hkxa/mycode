// 15 pts
/*************************************
 * @problem:      「联合省选 2020 A」组合数问题.
 * @author:       brealid.
 * @time:         2020-11-23.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int M = 1e3 + 7;

int n, x, p, m;
int a[M];

int64 f(int64 x) {
    int64 ret(0), now(1);
    for (int i = 0; i <= m; ++i) {
        ret += a[i] * now % p;
        now = now * x % p;
    }
    return ret % p;
}

int64 fpow(int64 x, int n) {
    int64 ret(1);
    while (n) {
        if (n & 1) ret = ret * x % p;
        x = x * x % p;
        n >>= 1;
    }
    return ret;
}

void subtak1_3() {
    int64 C[M][M], ans(0);
    memset(C, 0, sizeof(C));
    for (int i = 0; i <= n; ++i) {
        C[i][0] = 1;
        for (int j = 1; j <= i; ++j)
            C[i][j] = (C[i - 1][j - 1] + C[i - 1][j]) % p;
    }
    for (int k = 0; k <= n; ++k)
        ans += f(k) * fpow(x, k) % p * C[n][k] % p;
    kout << ans % p << '\n';
}

void subtask4_6() {
    int64 fac[100007], ifac[100007];
    fac[0] = 1;
    for (int i = 1; i <= n; ++i) fac[i] = fac[i - 1] * i % p;
    ifac[n] = fpow(fac[n], p - 2);
    for (int i = n; i >= 1; --i) ifac[i - 1] = ifac[i] * i % p;
    int64 ans(0);
    for (int k = 0; k <= n; ++k) {
        int64 comb = fac[n] * ifac[k] % p * ifac[n - k] % p;
        ans += fpow(x, k) % p * comb % p;
    }
    kout << ans % p << '\n';
}

signed main() {
    kin >> n >> x >> p >> m;
    for (int i = 0; i <= m; ++i) kin >> a[i];
    if (n <= 1000 && m <= 1000) return subtak1_3(), 0;
    if (n <= 1e5 && m == 0) return subtask4_6(), 0;
    return 0;
}