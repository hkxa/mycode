//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      「ZJOI2020」传统艺能.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-01.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

struct Reader {
    char endch;
    Reader() { endch = '\0'; }
    template <typename Int>
    Reader& operator >> (Int &i) {
        i = baseFastio::read<Int>(endch);
        return *this;
    }
    template <typename Int>
    inline Int get_int() {
        return baseFastio::read<Int>();
    }
    inline char get_nxt() {
        return endch;
    }
} read;

struct Writer {
    Writer& operator << (const char *ch) {
        // char *p = ch;
        while (*ch) putchar(*(ch++));
        return *this;
    }
    Writer& operator << (const char ch) {
        putchar(ch);
        return *this;
    }
    template <typename Int>
    Writer& operator << (const Int i) {
        baseFastio::write(i);
        return *this;
    }
} write;

// #define int int64
const int N = 4e5 + 7, P = 998244353;

int n, k;
char s[N];
int m[N], l[N], r[N], cnt = 1;
bitset<32> tag;

void treeRead(int u, int ml, int mr) {
    if (ml == mr) return;
    read >> m[u];
    treeRead(l[u] = ++cnt, ml, m[u]);
    treeRead(r[u] = ++cnt, m[u] + 1, mr);
}
int ans;

void modify(int u, int ml, int mr, int ul = 1, int ur = n) {
    if (ul >= ml && ur <= mr) {
        tag.set(u);
        return;
    }
    if (ul > mr || ur < ml) {
        return;
    }
    if (tag.test(u)) {
        tag.set(l[u]);
        tag.set(r[u]);
        tag.reset(u);
    }
    modify(l[u], ml, mr, ul, m[u]);
    modify(r[u], ml, mr, m[u] + 1, ur);
}

void solve(int K) {
    if (!K) {
        ans = (ans + tag.count()) % P;
        return;
    }
    bitset<32> backup = tag;
    for (int i = 1; i <= n; i++) {
        for (int j = i; j <= n; j++) {
            tag = backup;
            modify(1, i, j);
            solve(K - 1);
        }
    }
}

int64 fpow(int64 a, int n) {
    int64 ret = 1;
    while (n) {
        if (n & 1) ret = ret * a % P;
        a = a * a % P;
        n >>= 1;
    }
    return ret;
}

signed main()
{
    read >> n >> k;
    treeRead(1, 1, n);
    solve(k);
    write << ans << " / " << fpow(n * (n + 1) / 2, k) << '\n';
    write << ans * fpow(fpow(n * (n + 1) / 2, k), P - 2) % P << '\n';
    return 0;
}