#include <bits/stdc++.h>

const int N = 1e5 + 7;

int n, m;
std::vector<int> G[N];
int ind[N];

struct big_integer {
    int a[6];
    big_integer(int x = 0) { memset(a, 0, sizeof(a)); a[0] = x; }
    big_integer operator / (int val) const {
        big_integer ret;
        for (int i = 5, remain = 0; i >= 0; --i) {
            ret.a[i] = (a[i] + remain * 10000) / val;
            remain = (a[i] + remain * 10000) % val;
        }
        return ret;
    }
    big_integer operator * (big_integer val) const {
        big_integer ret;
        for (int i = 0; i < 6; ++i) {
            for (int j = 0; i + j < 6; ++j)
                ret.a[i + j] += a[i] * val.a[j];
            ret.a[i + 1] += ret.a[i] / 10000;
            ret.a[i] %= 10000;
        }
        return ret;
    }
    big_integer& operator += (big_integer b) {
        for (int i = 0; i < 6; ++i) {
            a[i] += b.a[i];
            if (a[i] >= 10000) {
                a[i] -= 10000;
                ++a[i + 1];
            }
        }
        return *this;
    }
    big_integer& operator -= (big_integer b) {
        for (int i = 0; i < 6; ++i) {
            a[i] -= b.a[i];
            if (a[i] < 0) {
                a[i] += 10000;
                --a[i + 1];
            }
        }
        return *this;
    }
    big_integer& operator /= (int val) { return *this = *this / val; }
    bool operator < (const big_integer &b) const {
        for (int i = 5; i >= 0; --i)
            if (a[i] ^ b.a[i]) return a[i] < b.a[i];
        return false;
    }
    bool is_zero() const {
        for (int i = 0; i < 6; ++i)
            if (a[i]) return false;
        return true;
    }
    bool is_divide_abled(int val) const {
        int remain = 0;
        for (int i = 5; i >= 0; --i)
            remain = (a[i] + remain * 10000) % val;
        return !remain;
    }
    void print() const {
        if (is_zero()) putchar('0');
        else {
            int p = 5;
            while (!a[p]) --p;
            for (printf("%d", a[p--]); p >= 0; --p)
                printf("%04d", a[p]);
        }
    }
} f[N];

void reduce(big_integer& a, big_integer& b) {
    for (int i = 5; i >= 2; --i)
        for (int j = 0; j < 11 && a.is_divide_abled(i) && b.is_divide_abled(i); ++j)
            a /= i, b /= i;
}

namespace file_io {
    void set_to_file(std::string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

signed main() {
    file_io::set_to_file("water");
    std::ios::sync_with_stdio(false);
    std::cout.tie(0);
    scanf("%d%d", &n, &m);
    for (int i = 1, t, x; i <= n; ++i) {
        scanf("%d", &t);
        while (t--) {
            scanf("%d", &x);
            G[i].push_back(x);
            ++ind[x];
        }
    }
    std::queue<int> q;
    big_integer BASE = 60;
    BASE = BASE * BASE * BASE * BASE * BASE;
    BASE = BASE * BASE * 60;
    for (int i = 1; i <= m; ++i) {
        f[i] = BASE;
        q.push(i);
    }
    while (!q.empty()) {
        int u = q.front();
        q.pop();
        if (G[u].empty()) continue;
        big_integer t = f[u] / G[u].size();
        for (size_t i = 0; i < G[u].size(); ++i) {
            int v = G[u][i];
            f[v] += t;
            if (!--ind[v]) q.push(v);
        }
    }
    for (int i = 1; i <= n; ++i)
        if (G[i].empty()) {
            big_integer b = BASE;
            reduce(f[i], b);
            f[i].print();
            putchar(32);
            b.print();
            putchar(10);
        }
    return 0;
}