#include <bits/stdc++.h>
typedef long long int64;

const int N = 5e5 + 7, P = 1e9 + 7;

int64 fpow(int64 x, int64 n) {
    int64 res = 1;
    while (n) {
        if (n & 1) res = res * x % P;
        x = x * x % P;
        n >>= 1;
    }
    return res;
}

int64 under_power(int64 n, int m) {
    int64 ret = 1;
    for (int i = 0; i < m; ++i) ret = ret * (n - i) % P;
    return ret;
}

int64 fac[N];

void init_f() {
    fac[0] = 1;
    for (int i = 1; i < N; i++)
        fac[i] = fac[i - 1] * i % P;
}

// 计算 1^m+2^m+3^m+...+n^m
int64 calc_kth(int64 n, int64 m) {
    int64 res = 0;
    if (n <= m + 2) {
        for (int i = 1; i <= n; i++)
            res = (res + fpow(i, m)) % P;
    } else {
        int64 t = under_power(n - 1, m + 2);
        int64 y = 0;
        int flag = (m + 2) % 2 ? 1 : -1;
        for (int i = 1; i <= m + 2; i++) {
            y = (y + fpow(i, m)) % P;
            res += flag * y * t % P * fpow(n - i, P - 2) % P * fpow(fac[i - 1] * fac[m + 2 - i] % P, P - 2) % P;
            flag = -flag;
        }
        res = (res % P + P) % P;
    }
    return res;
}

int w[13], e[13], l[N][13], r[N][13];
int a[13], b[13], h[13];
int64 f[13][13];
int c[N], d[N];
int n, m;

bool special_check() {
    for (int i = 1; i <= m; ++i) if (e[i] || l[n][i] - r[n][i] >= w[i]) return false;
    return true;
}

int main() {
    init_f();
    freopen("walk.in", "r", stdin);
    freopen("walk.out", "w", stdout);
    scanf("%d%d", &n, &m);
    for (int i = 1; i <= m; i++)
        scanf("%d", w + i);
    for (int i = 1; i <= n; i++) {
        scanf("%d%d", c + i, d + i);
        memcpy(l[i], l[i - 1], sizeof(l[i - 1]));
        memcpy(r[i], r[i - 1], sizeof(r[i - 1]));
        l[i][c[i]] = std::min(l[i][c[i]], e[c[i]] += d[i]);
        r[i][c[i]] = std::max(r[i][c[i]], e[c[i]]);
    }
    if (special_check()) return puts("-1"), 0;

    for (int j = 1; j <= m; j++)
        a[j] = w[j] - (r[n][j] - l[n][j]);
    int64 ans = 0;
    for (int i = 0; i <= n; i++) {
        int64 s = 1;
        for (int j = 1; j <= m; j++)
            s = s * std::max(0, (w[j] - (r[i][j] - l[i][j]))) % P;
        ans = (ans + s) % P;
    }

    for (int i = 1; i <= n; i++)
        for (int j = 1; j <= m; j++) {
            r[i][j] = std::max(0, r[i][j] + e[j] - r[n][j]);
            l[i][j] = std::min(0, l[i][j] + e[j] - l[n][j]);
        }
    for (int j = 1; j <= m; j++)
        b[j] = r[n][j] - l[n][j];

    // printf("Round %d: ans = %lld\n", 0, (ans % P + P) % P);
    int last = -1;
    for (int i = 1; i <= n; i++) {
        for (int j = 0; j <= m; j++) f[0][j] = 0;
        f[0][0] = 1;
        int t = 2147483647;
        for (int j = 1; j <= m; j++) {
            int x = a[j] - r[i][j] + l[i][j];
            if (x <= 0) goto output;
            if (b[j] > 0) t = std::min(t, x / b[j]);
            for (int k = 0; k <= m; k++) {
                f[j][k] = f[j - 1][k] * x % P;
                if (k > 0)
                    f[j][k] = (f[j][k] - f[j - 1][k - 1] * b[j]) % P;
            }
        }
        ans += f[m][0] * (t + 1) % P;
        if (t != last) {
            last = t;
            for (int j = 1; j <= m; j++) h[j] = calc_kth(t, j);
        }
        for (int j = 1; j <= m; j++)
            ans += h[j] * f[m][j] % P;
        // printf("Round %d: ans = %lld\n", i, (ans % P + P) % P);
    }
output:
    printf("%lld\n", (ans % P + P) % P);
    return 0;
}