/*************************************
 * @problem:      最小费用流.
 * @user_name:    brealid.
 * @time:         2020-11-15.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

namespace Network_MinCostMaxFlow {
    typedef long long int64;
    const int Net_Node = 400, Net_Edge = 15000;
    const int64 inf = 0x3f3f3f3f3f3f3f3f; // 当 int64 为 int 的时候，自动类型强转为 0x3f3f3f3f
    struct edge {
        int to, nxt_edge;
        int64 flow, fee;
    } e[Net_Edge * 2 + 5];
    int head[Net_Node + 5], ecnt = 1;
    int node_total, st, ed;
    int64 flow[Net_Node + 5], dis[Net_Node + 5];
    int from[Net_Node + 5];
    bool inq[Net_Node + 5];
    // 清零，此函数适用于多组数据
    void clear() {
        memset(head, 0, sizeof(head));
        ecnt = 1;
        st = ed = 0;
    }
    // 添加边（正向边和反向边均会自动添加）
    void add_edge(int from, int to, int64 flow, int64 fee) {
        // Add "positive going edge"
        e[++ecnt].to = to;
        e[ecnt].flow = flow;
        e[ecnt].fee = fee;
        e[ecnt].nxt_edge = head[from];
        head[from] = ecnt;
        // Add "reversed going edge"
        e[++ecnt].to = from;
        e[ecnt].flow = 0;
        e[ecnt].fee = -fee; // 反边费用相反
        e[ecnt].nxt_edge = head[to];
        head[to] = ecnt;
    }
    // SPFA 算法
    bool spfa() {
        memset(dis, 0x3f, sizeof(int64) * (node_total + 1));
        memset(flow, 0, sizeof(int64) * (node_total + 1));
        memset(inq, 0, sizeof(bool) * (node_total + 1));
        from[st] = -1;
        dis[st] = 0;
        flow[st] = inf;
        queue<int> q;
        q.push(st);
        inq[st] = true;
        while (!q.empty()) {
            int u = q.front(); q.pop();
            inq[u] = false;
            for (int i = head[u]; i; i = e[i].nxt_edge)
                if (dis[e[i].to] > dis[u] + e[i].fee && e[i].flow) {
                    dis[e[i].to] = dis[u] + e[i].fee;
                    flow[e[i].to] = min(flow[u], e[i].flow);
                    from[e[i].to] = i;
                    if (!inq[e[i].to]) {
                        q.push(e[i].to);
                        inq[e[i].to] = true;
                    }
                }
        }
        return dis[ed] != inf;
    }
    // 记录 MCMF(Min Cost Max Flow) 的结果
    struct MCMF_result {
        int64 max_flow, min_cost;
        MCMF_result(int MF, int MC) : max_flow(MF), min_cost(MC) {}
    };
    // MCMF(Edmond_Karp + SPFA) 总工作函数，需要提供节点数，起始点（默认 1），结束点（默认 node_count)
    MCMF_result work(int node_count, int start_node = 1, int finish_node = -1) {
        node_total = node_count;
        st = start_node;
        ed = ~finish_node ? finish_node : node_count;
        int64 max_flow = 0, min_cost = 0;
        while (spfa()) {
            int now_flow = flow[ed];
            max_flow += now_flow;
            for (int i = from[ed]; i; i = from[e[i ^ 1].to]) {
                e[i].flow -= now_flow;
                e[i ^ 1].flow += now_flow;
                min_cost += e[i].fee * now_flow;
            }
        }
        return MCMF_result(max_flow, min_cost);
    }
}


signed main() {
    int n, m;
    read >> n >> m;
    for (int i = 1, u, v, w, f; i <= m; ++i) {
        read >> u >> v >> w >> f;
        Network_MinCostMaxFlow::add_edge(u, v, w, f);
    }
    Network_MinCostMaxFlow::MCMF_result result = Network_MinCostMaxFlow::work(n);
    write << result.max_flow << ' ' << result.min_cost << '\n';
    return 0;
}