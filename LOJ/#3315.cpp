//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      「ZJOI2020」抽卡.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-02.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

struct Reader {
    char endch;
    Reader() { endch = '\0'; }
    template <typename Int>
    Reader& operator >> (Int &i) {
        i = baseFastio::read<Int>(endch);
        return *this;
    }
    template <typename Int>
    inline Int get_int() {
        return baseFastio::read<Int>();
    }
    inline char get_nxt() {
        return endch;
    }
} read;

struct Writer {
    Writer& operator << (const char *ch) {
        // char *p = ch;
        while (*ch) putchar(*(ch++));
        return *this;
    }
    Writer& operator << (const char ch) {
        putchar(ch);
        return *this;
    }
    template <typename Int>
    Writer& operator << (const Int i) {
        baseFastio::write(i);
        return *this;
    }
} write;

// #define int int64

const int P = 998244353;

inline int64 fpow(int64 a, int n) {
    int64 ret = 1;
    while (n) {
        if (n & 1) ret = ret * a % P;
        a = a * a % P;
        n >>= 1;
    }
    return ret;
}

#define inv(a) fpow(a, P - 2)
#define add(a, b) (((a) + (b)) % P)
#define dec(a, b) add(a, P - (b) % P)
#define mul(a, b) ((a) * (b) % P)
#define div(a, b) ((a) * inv(b))

int m, k, a[5007];
int64 fl[5007][5007], fr[5007][5007];

inline void contribute(int l, int r) {
    if (r - l + 1 < k) return;
    for (int i = l; i <= r; i++) {
        ;
    }
}

inline bool only_one() {
    int l = 1, r;
    bool occured = 0;
    while (l <= m) {
        r = l;
        while (a[r] + 1 == a[r + 1]) r++;
        if (r - l + 1 > k) return false;
        else if (r - l + 1 == k) {
            if (occured) return false;
            occured = true;
        }
        l = r + 1;
    }
    return true;
}

inline int sub1() {
    int64 ans = 0;
    for (int i = k; i >= 1; i--) {
        ans = add(ans, div(m, i));
    }
    write << ans << '\n';
    return 0;
}

inline int cnt_dont_cross() {
    int l = 1, r;
    int occ_cnt = 0;
    while (l <= m) {
        r = l;
        while (a[r] + 1 == a[r + 1]) r++;
        if (r - l + 1 > k) return false;
        else if (r - l + 1 == k) occ_cnt++;
        l = r + 1;
    }
    return occ_cnt;
}

inline int orig(int kp)  {
    int64 ans = 0;
    for (int i = kp; i >= 1; i--) {
        ans = add(ans, div(m, i));
    }
    return ans;
}

inline int sub2() {
    int64 ans = 0;
    for (int i = k; i >= 1; i--) {
        ans = add(ans, div(m, i));
    }
    write << ans << '\n';
    return 0;
}

signed main()
{
    read >> m >> k;
    for (int i = 1; i <= m; i++) read >> a[i];
    if (only_one()) return sub1();
    if (cnt_dont_cross()) return sub2();
    int l = 1, r;
    while (l <= m) {
        r = l;
        while (a[r] + 1 == a[r + 1]) r++;
        contribute(l, r);
        l = r + 1;
    }
    return 0;
}