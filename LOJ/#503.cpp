/*************************************
 * @problem:      「LibreOJ β Round」ZQC 的课堂.
 * @user_name:    brealid/hkxadpall/zhaoyi20/j(o)moo/littleTortoise.
 * @time:         2020-09-24.
 * @language:     C++.
 * @fastio_ver:   20200913.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0,    // input
        flush_stdout = 1 << 1,  // output
        flush_stderr = 1 << 2,  // output
    };
    enum number_type_flags {
        output_double_stable = 1 << 0,  // output
        output_double_faster = 1 << 1   // output
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f & ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore character set = {' ', '\r', '\n', '\t'}
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (double &lf) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            lf = endch & 15;
            while (isdigit(endch = getchar())) lf = lf * 10 + (endch & 15);
            if (endch == '.') {
                double digit = 0.1;
                while (isdigit(endch = getchar())) {
                    lf += (endch & 15) * digit;
                    digit *= 0.1;
                }
            }
            if (flag) lf = -lf;
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            Int d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return d;
        }
    } read;

    struct Writer {
        Writer& operator << (io_flags f) {
            if (f & flush_stdout) fflush(stdout);
            if (f & flush_stderr) fflush(stderr);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
        inline void operator () (double val, int eps_digit = 6, char endch = '\0', number_type_flags flg = output_double_faster) {
            if (flg & output_double_stable) {
                static char output_format[10];
                sprintf(output_format, "%%.%dlf", eps_digit);
                printf(output_format, val);
            } else if (flg & output_double_faster) {
                if (val < 0) {
                    putchar('-');
                    val = -val;
                }
                double eps_number = 0.5;
                for (int i = 1; i <= eps_digit; i++) eps_number /= 10;
                val += eps_number;
                (*this) << (int64)val;
                val -= (int64)val;
                if (eps_digit) putchar('.');
                while (eps_digit--) {
                    val *= 10;
                    putchar((int)val | '0');
                    val -= (int)val;
                }
            } else {
               (*this) << '1';
                if (eps_digit) {
                    (*this) << ".E";
                    for (int i = 2; i <= eps_digit; i++) (*this) << '0';
                }
            }
            if (endch) putchar(endch);
        }
    } write;
}
using namespace Fastio;

namespace File_IO {
    void init_IO(const char *file_name) {
        char buff[107];
        sprintf(buff, "%s.in", file_name);
        freopen(buff, "r", stdin);
        sprintf(buff, "%s.out", file_name);
        freopen(buff, "w", stdout);
    }
}

// #define int int64

const int N = 1e5 + 7;

template<typename T>
class BalanceTree {
  private:  
    struct ScapegoatNode {
        T v;
        int tot, siz, l, r;
    } t[N * 2];
    int root;
    int temp[N * 2];
    int TempCnt, NodeCnt;

    bool Balance(int x) {
        return t[t[x].l].siz < t[x].siz * 0.75 && 
               t[t[x].r].siz < t[x].siz * 0.75;
    }
    void Flatten(int x) {
        if (!x) return;
        Flatten(t[x].l);
        if (t[x].tot) temp[++TempCnt] = x;
        Flatten(t[x].r);
    }
    void Do_ReBuild(int &x, int l, int r) {
        if (l > r) return void(x = 0);
        if (l == r) {
            x = temp[l];
            t[x].l = 0;
            t[x].r = 0;
            t[x].siz = t[x].tot;
            return;
        }
        int mid = (l + r) >> 1;
        x = temp[mid];
        Do_ReBuild(t[x].l, l, mid - 1);
        Do_ReBuild(t[x].r, mid + 1, r);
        t[x].siz = t[t[x].l].siz + t[t[x].r].siz + t[x].tot;
    }
    void ReBuild(int &x) {
        TempCnt = 0;
        Flatten(x);
        Do_ReBuild(x, 1, TempCnt);
    }
    void Check(int &a) {
        if (!a) return;
        if (!Balance(a)) ReBuild(a);
        else if (t[t[a].l].siz > t[t[a].r].siz) Check(t[a].l);
        else Check(t[a].r);
    }
    void Insert(int &a, T v) {
        if (!a) {
            a = ++NodeCnt;
            t[a].v = v;
            t[a].tot = 1;
        } else if (v == t[a].v) {
            t[a].tot++;
        } else if (v < t[a].v) {
            Insert(t[a].l, v);
        } else {
            Insert(t[a].r, v);
        }
        t[a].siz++;
    }
    bool Delete(int a, T v) {
        if (!a) return false;
        bool succeed = false;
        if (v == t[a].v) {
            if (t[a].tot) {
                --t[a].tot;
                succeed = true;
            }
        } else if (v < t[a].v) {
            if (Delete(t[a].l, v)) succeed = true;
        } else {
            if (Delete(t[a].r, v)) succeed = true;
        }
        if (succeed) --t[a].siz;
        return succeed;
    }
    T GetVal(int rank) {
        int a = root;
        while (true) {
            if (t[t[a].l].siz < rank && t[t[a].l].siz + t[a].tot >= rank) return t[a].v;
            else if (t[t[a].l].siz >= rank) a = t[a].l;
            else rank -= t[t[a].l].siz + t[a].tot, a = t[a].r;
        }
    }
    int GetRank_lower(T val) {
        int a = root, rank = 1;
        while (a) {
            if (t[a].v >= val) a = t[a].l;     
            else rank += t[t[a].l].siz + t[a].tot, a = t[a].r;     
        }
        return rank;
    }
    int GetRank_upper(T val) {
        int a = root, rank = 1;
        while (a) {
            if (t[a].v > val) a = t[a].l;     
            else rank += t[t[a].l].siz + t[a].tot, a = t[a].r;     
        }
        return rank;
    }
  public:
    void insert(T val) {
        Insert(root, val);
        Check(root);
    }
    bool remove(T val) {
        if (Delete(root, val)) {
            Check(root);
            return true;
        } else return false;
    }
    void lower_bound(T val) {
        return GetVal(GetRank_lower(val));
    }
    void upper_bound(T val) {
        return GetVal(GetRank_upper(val));
    }
    int count_less(T val) {
        return GetRank_lower(val) - 1;
    }
    int count_greater(T val) {
        return t[root].siz - GetRank_upper(val) + 1;
    }
};

struct axis_coordinate {
    BalanceTree<int> minp, maxp;
    int n, p;
    int pos[N];
    int left_answer, dif_total;
    void init(int cnt_nodes) {
        n = cnt_nodes;
        for (int i = 2; i <= n; i++) {
            minp.insert(min(pos[i - 1], pos[i]));
            maxp.insert(max(pos[i - 1], pos[i]));
        }
    }
    void forward() {
        if (p >= n) return;
        minp.remove(min(pos[p] - dif_total, pos[p + 1]));
        maxp.remove(max(pos[p] - dif_total, pos[p + 1]));
        pos[p + 1] += dif_total;
        if (pos[p - 1] * pos[p] < 0) ++left_answer;
        ++p;
    }
    void backward() {
        if (p <= 1) return;
        --p;
        if (pos[p - 1] * pos[p] < 0) --left_answer;
        pos[p + 1] -= dif_total;
        minp.insert(min(pos[p] - dif_total, pos[p + 1]));
        maxp.insert(max(pos[p] - dif_total, pos[p + 1])); 
    }
    void update(int new_pos) {
        dif_total += new_pos - pos[p] + pos[p - 1];
        pos[p] = pos[p - 1] + new_pos;
    }
    int query() {
        // printf("[# %d => %d] ", (pos[p - 1] * pos[p] < 0), 
        //        left_answer + n - p - minp.count_greater(-dif_total) - maxp.count_less(-dif_total) + (pos[p - 1] * pos[p] < 0));
        return left_answer + n - p - minp.count_greater(-dif_total) - maxp.count_less(-dif_total) + (pos[p - 1] * pos[p] < 0);
    }
    axis_coordinate() : p(1), left_answer(0), dif_total(0) {
        pos[0] = 1;
    }
} axis_x, axis_y;

int n, q;

signed main() {
    // File_IO::init_IO("「LibreOJ β Round」ZQC 的课堂");
    read >> n;
    for (int i = 1; i <= n; i++) {
        axis_x.pos[i] = axis_x.pos[i - 1] + read.get<int>();
        axis_y.pos[i] = axis_y.pos[i - 1] + read.get<int>();
    }
    axis_x.init(n);
    axis_y.init(n);
    read >> q;
    char opt;
    for (int i = 1; i <= q; i++) {
        read >> opt;
        if (opt == 'B') axis_x.backward(), axis_y.backward();
        else if (opt == 'F') axis_x.forward(), axis_y.forward();
        else if (opt == 'Q') write << axis_x.query() + axis_y.query() << '\n';
        else {
            axis_x.update(read.get<int>());
            axis_y.update(read.get<int>());
        }
    }
    return 0;
}