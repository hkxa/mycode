/**
 * Problem:	P1083 借教室. 
 * Author:	航空信奥. 
 * Date:	2018/08/23. 
 * Upload:	Luogu. 
 */
#include <stdio.h>
#include <string.h>

namespace brealid {
    template <typename _TpInt> inline _TpInt read();
    template <typename _TpInt> inline void write(_TpInt x);
    
#	define Max_N 1000007
    
    int n, m, r[Max_N];
    int d[Max_N], s[Max_N], t[Max_N];
    int dif[Max_N]; 
    
    bool judge(int x)
    {
        memset(dif, 0, sizeof(dif));
        for (int i = 1; i <= x; i++) {
            dif[s[i]] += d[i];
            dif[t[i] + 1] -= d[i]; 
        }
        int now = 0;
        for (int i = 1; i <= n; i++) {
            now += dif[i];
            if (now > r[i]) return false;
        }
        return true;
    } 
    
    void Binary_search() 
    {
        int l = 0, r = m, mid;
        while (l < r) {
            mid = (l + r + 1) >> 1;
            if (judge(mid)) l = mid;
            else r = mid - 1;
        }
        if (l == m) putchar('0');
        else puts("-1"), write(l + 1);
    }
    
    int main() 
    {
        n = read<int>();
        m = read<int>();
        for (int i = 1; i <= n; i++) {
            r[i] = read<int>();
        }
        for (int i = 1; i <= m; i++) {
            d[i] = read<int>();
            s[i] = read<int>();
            t[i] = read<int>();
        }
        Binary_search();
        return 0;
    }

    char BufferRead[1 << 17];
    int rLen = 0, rPos = 0;
    inline char Getchar()
    {
        if (rPos == rLen) rPos = 0, rLen = fread(BufferRead, 1, 1 << 17, stdin);
        if (rPos == rLen) return EOF;
        return BufferRead[rPos++];
    } 

    template <typename _TpInt>
    inline _TpInt read()       
    {
        register int flag = 1;
        register char c = Getchar();
        while ((c > '9' || c < '0') && c != '-') 
            c = Getchar();
        if (c == '-') flag = -1, c = Getchar();
        register _TpInt init = (c & 15);
        while ((c = Getchar()) <= '9' && c >= '0') 
            init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename _TpInt>
    inline void write(_TpInt x)
    {
        if (x < 0) {
            putchar('-');
            write<_TpInt>(~x + 1);
        }
        else {
            if (x > 9) write<_TpInt>(x / 10);   
            putchar(x % 10 + '0');
        }
    }
}

int main()
{
    brealid::main();
    return 0;
}