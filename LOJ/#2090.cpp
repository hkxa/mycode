//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      [ZJOI2016]旅行者.
 * @user_name:    brealid.
 * @time:         2020-06-08.
 * @language:     C++.
 * @upload_place: loj.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 988244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64
const int N = 50007;
int n, m, Q;
int cost[N][4];
int dis[N];
const int _dX[4] = {0, 1, 0, -1}, _dY[4] = {1, 0, -1, 0};

struct point {
    int x, y;
    point() {}
    point(int _X, int _Y) : x(_X), y(_Y) {}
    inline operator int() const { return x * m + y; }
    inline point move(int dir) const { return point(x + _dX[dir], y + _dY[dir]); }
};

struct dijNode {
    point u; int dist;
    dijNode() {}
    dijNode(point U, int Dist) : u(U), dist(Dist) {}
    inline bool isLatest() const { return dist == dis[u]; }
    inline bool operator < (const dijNode &b) const { return dist > b.dist; }
};

struct interval {
    int l, r;
    interval() : l(0), r(-1) {}
    interval(int L, int R) : l(L), r(R) {}
    inline bool exist() { return l <= r; }
    inline bool include(int p) { return p >= l && p <= r; }
    inline int len() { return r - l + 1; }
    inline int mid() { return (l + r) >> 1; }
    inline interval left() { return interval(l, mid() - 1); }
    inline interval right() { return interval(mid() + 1, r); }
};

struct square {
    interval x, y;
    square() : x(), y() {}
    square(interval _X, interval _Y) : x(_X), y(_Y) {}
    square(int xl, int xr, int yl, int yr) : x(xl, xr), y(yl, yr) {}
    inline bool exist() { return x.exist() && y.exist(); }
    inline bool include(point a) { return x.include(a.x) && y.include(a.y); }
    inline bool direction() { return x.len() < y.len(); }
    inline square left() {
        return direction() ? square(x, y.left()) : square(x.left(), y);
    } 
    inline square right() {
        return direction() ? square(x, y.right()) : square(x.right(), y);
    } 
};

inline void reset(int *Array, square R) {
    point i;
    for (i.x = R.x.l; i.x <= R.x.r; i.x++) 
        for (i.y = R.y.l; i.y <= R.y.r; i.y++)
            Array[i] = 0x3fffffff;
}

struct question {
    point u, v;
    int id;
} q[100007];
int ans[100007];

void Dij(point s, square range) {
    // printf("Dij : start{%d, %d}\n", s.x, s.y);
    static priority_queue<dijNode> q;
    reset(dis, range);
    dis[s] = 0;
    q.push(dijNode(s, 0));
    point u, v;
    while (!q.empty()) {
        while (!q.empty() && !q.top().isLatest()) q.pop();
        if (q.empty()) break;
        u = q.top().u; 
        q.pop();
        for (int i = 0; i < 4; i++) {
            v = u.move(i);
            if (!range.include(v)) continue;
            if (dis[v] > dis[u] + cost[u][i]) {
                dis[v] = dis[u] + cost[u][i];
                q.push(dijNode(v, dis[v]));
            }
        }
    }
}

void solve(interval qRange, square range) {
    // printf("qRange : {%d, %d}, range : {{%d ~ %d}, {%d ~ %d}}\n", 
    //         qRange.l, qRange.r, range.x.l, range.x.r, range.y.l, range.y.r);
    if (!qRange.exist() || !range.exist()) return;
    queue<question> lef, rig;
    if (range.direction()) {
        point a;
        a.y = range.y.mid();
        for (a.x = range.x.l; a.x <= range.x.r; a.x++) {
            Dij(a, range);
            for (int i = qRange.l; i <= qRange.r; i++) {
                // if (q[i].id == 1 && ans[q[i].id] > 50676 && dis[q[i].u] + dis[q[i].v] == 50676) printf("X. Emm.S{%d, %d}\n", a.x, a.y);
                ans[q[i].id] = min(ans[q[i].id], dis[q[i].u] + dis[q[i].v]);
                // printf("dis[{%d, %d}] = %d, dis[{%d, %d}] = %d\n", 
                //         q[i].u.x, q[i].u.y, dis[q[i].u], q[i].v.x, q[i].v.y, dis[q[i].v]);
                // if (!range.include(q[i].u) || !range.include(q[i].v)) printf("??? !!! range.inclue is false!\n");
            }
        }
        for (int i = qRange.l; i <= qRange.r; i++) {
            if (q[i].u.y < range.y.mid() && q[i].v.y < range.y.mid()) {
                lef.push(q[i]);
            } else if (q[i].u.y > range.y.mid() && q[i].v.y > range.y.mid()) {
                rig.push(q[i]);
            } 
        }
        int u1 = qRange.l, u2;
        while (lef.size()) {
            q[u1++] = lef.front();
            lef.pop();
        }
        u2 = u1;
        while (rig.size()) {
            q[u2++] = rig.front();
            rig.pop();
        }
        solve(interval(qRange.l, u1 - 1), range.left());
        solve(interval(u1, u2 - 1), range.right());
    } else {
        point a;
        a.x = range.x.mid();
        for (a.y = range.y.l; a.y <= range.y.r; a.y++) {
            Dij(a, range);
            for (int i = qRange.l; i <= qRange.r; i++) {
                // if (q[i].id == 1 && ans[q[i].id] > 50676 && dis[q[i].u] + dis[q[i].v] == 50676) printf("Y. Emm.S{%d, %d}\n", a.x, a.y);
                ans[q[i].id] = min(ans[q[i].id], dis[q[i].u] + dis[q[i].v]);
                // printf("dis[{%d, %d}] = %d, dis[{%d, %d}] = %d\n", 
                //         q[i].u.x, q[i].u.y, dis[q[i].u], q[i].v.x, q[i].v.y, dis[q[i].v]);
            }
        }
        for (int i = qRange.l; i <= qRange.r; i++) {
            if (q[i].u.x < range.x.mid() && q[i].v.x < range.x.mid()) {
                lef.push(q[i]);
            } else if (q[i].u.x > range.x.mid() && q[i].v.x > range.x.mid()) {
                rig.push(q[i]);
            } 
        }
        int u1 = qRange.l, u2;
        while (lef.size()) {
            q[u1++] = lef.front();
            lef.pop();
        }
        u2 = u1;
        while (rig.size()) {
            q[u2++] = rig.front();
            rig.pop();
        }
        solve(interval(qRange.l, u1 - 1), range.left());
        solve(interval(u1, u2 - 1), range.right());
    } 
}

signed main() {
    n = read<int>();
    m = read<int>();
    for (int i = 1, r; i <= n; i++)
        for (int j = 1; j < m; j++) {
            r = read<int>();
            cost[point(i, j)][0] = r;
            cost[point(i, j + 1)][2] = r;
        }
    for (int i = 1, c; i < n; i++)
        for (int j = 1; j <= m; j++) {
            c = read<int>();
            cost[point(i, j)][1] = c;
            cost[point(i + 1, j)][3] = c;
        }
    Q = read<int>();
    for (int i = 1; i <= Q; i++) {
        q[i].u.x = read<int>();
        q[i].u.y = read<int>();
        q[i].v.x = read<int>();
        q[i].v.y = read<int>();
        q[i].id = i;
        ans[i] = 0x3fffffff;
    }
    // printf("Input Ended\n");
    solve(interval(1, Q), square(1, n, 1, m));
    for (int i = 1; i <= Q; i++) {
        write(ans[i], 10);
    }
    return 0;
}

// Create File Date : 2020-06-08
// Command : type P3350-Input | run P3350 -std=c++11 -f >P3350-Output & fc .\P3350-Answer. .\P3350-Output.