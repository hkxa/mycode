//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      「ZJOI2020」序列.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-01.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

struct Reader {
    char endch;
    Reader() { endch = '\0'; }
    template <typename Int>
    Reader& operator >> (Int &i) {
        i = baseFastio::read<Int>(endch);
        return *this;
    }
    template <typename Int>
    inline Int get_int() {
        return baseFastio::read<Int>();
    }
    inline char get_nxt() {
        return endch;
    }
} read;

struct Writer {
    Writer& operator << (const char *ch) {
        // char *p = ch;
        while (*ch) putchar(*(ch++));
        return *this;
    }
    Writer& operator << (const char ch) {
        putchar(ch);
        return *this;
    }
    template <typename Int>
    Writer& operator << (const Int i) {
        baseFastio::write(i);
        return *this;
    }
} write;

// #define int int64
const int N = 4e5 + 7, P = 998244353;

int n;
int a[N];

int64 ans = 1e9 + 7;

void dfs(int64 cnt) {
    if (cnt >= ans) return;
    int l = 1;
    while (l <= n && !a[l]) l++;
    if (l > n) {
        ans = cnt;
        return;
    }
    int r = l;
    int num = a[r];
    while (r < n && a[r + 1]) {
        r++;
        num = min(num, a[r]);
    }
    for (int i = l; i <= r; i++) a[i] -= num;
    dfs(cnt + num);
    for (int i = l; i <= r; i++) a[i] += num;
    l = 1;
    while (l <= n && !a[l]) l += 2;
    if (l <= n) {
        r = l;
        num = a[r];
        while (r + 1 < n && a[r + 2]) {
            r += 2;
            num = min(num, a[r]);
        }
        for (int i = l; i <= r; i += 2) a[i] -= num;
        dfs(cnt + num);
        for (int i = l; i <= r; i += 2) a[i] += num;
    }
    l = 2;
    while (l <= n && !a[l]) l += 2;
    if (l <= n) {
        r = l;
        num = a[r];
        while (r + 1 < n && a[r + 2]) {
            r += 2;
            num = min(num, a[r]);
        }
        for (int i = l; i <= r; i += 2) a[i] -= num;
        dfs(cnt + num);
        for (int i = l; i <= r; i += 2) a[i] += num;
    }
}

signed _main()
{
    read >> n;
    ans = 1LL << 62;
    for (int i = 1; i <= n; i++) read >> a[i];
    dfs(0);
    write << ans << '\n';
    return 0;
}

signed main() {
    int T = read.get_int<int>();
    while (T--) _main();
    return 0;
}