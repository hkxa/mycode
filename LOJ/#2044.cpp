//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      [CQOI2016]手机号码.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-21.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 998244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? Module(w - P) : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

int64 l, r;
int64 n;
int64 f[11 + 2][11 + 2][2][2][3][2];
int digit[11];
// f[第几位][上次位][occur4][occur8][有3连号(2)连2(1)未连(0)][是否顶格]

// int64 pow10[] = {1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000, 10000000000LL};

int64 dfs(int i, int cur, bool c4, bool c8, int conti, bool almost) {
    if (c4 && c8) return 0;
    if (i < 0) return conti >= 2;
    int64 &ans = f[i][cur][c4][c8][conti][almost];
    if (ans) return ans;
    int lim = almost ? digit[i] : 9;
    for (int j = (i == 10); j <= lim; j++) {
        if (cur == j) ans += dfs(i - 1, j, c4, c8, min(conti + 1, 2), almost && (j == digit[i]));
        else ans += dfs(i - 1, j, c4 | (j == 4), c8 | (j == 8), conti >= 2 ? 2 : 0, almost && (j == digit[i]));
    }
    // if (i <= 3) printf("f[%d][%d][%d][%d][%d][%d] = %lld\n", i, cur, c4, c8, conti, almost, ans);
    return ans;
}

int64 solve(int64 ng) {
    bool special = false;
    if (ng < 10000000000LL) {
        ng++;
        special = true;
    }
    n = ng;
    int cur = 0;
    while (cur <= 10) {
        digit[cur++] = ng % 10;
        ng /= 10;
    }
    memset(f, 0, sizeof(f));
    // printf("solve(%lld) : Answer = %lld\n", n, dfs(10, 0, 0, 0, 0, 1) - special);
    return dfs(10, 0, 0, 0, 0, 1) - special;
}

signed main() {
    l = read<int64>();
    r = read<int64>();
    // for (int64 i = l - 1; i <= r; i++) solve(i);
    write(solve(r) - solve(l - 1), 10);
    return 0;
}

// Create File Date : 2020-06-21