#include <bits/stdc++.h>

const int N = 53, M = 403, Limit_Kmax = 820000;
int n, m, col[N][M], top[N];

int StepFrom[Limit_Kmax + 7], StepTo[Limit_Kmax + 7], k;

inline void MoveBall(int x, int y) {
    ++k;
    col[y][++top[y]] = col[x][top[x]--];
    StepFrom[k] = x, StepTo[k] = y;
}

inline void MoveBall(int x, int y, int kth) {
    while (kth--) MoveBall(x, y);
}

// 计数: x 柱上有多少 <= require 的盘
inline int count(int x, int require) {
    int res(0);
    for (int i = 1; i <= m; ++i)
        if (col[x][i] <= require) ++res;
    return res;
}

bool request_finished[N];
// 将 x, y 柱上的颜色按 <= col 与 > col 分开，利用辅助柱 z
// 最终成功的柱子，request_finished 将会被置为 true
void divide(int x, int y, int z, int require) {
    if (count(x, require) + count(y, require) >= m) {
        int less = count(x, require);
        MoveBall(y, z, less);
        for (int i = m; i >= 1; --i)
            if (col[x][i] <= require) MoveBall(x, y);
            else MoveBall(x, z);
        MoveBall(y, x, less);
        MoveBall(z, x, m - less);
        MoveBall(y, z, m - less);
        MoveBall(x, y, m - less);
        for (int i = m; i >= 1; --i)
            if (col[z][i] <= require && top[x] < m) MoveBall(z, x);
            else MoveBall(z, y);
        request_finished[x] = true;
    } else {
        int greater = m - count(y, require);
        MoveBall(x, z, greater);
        for (int i = m; i >= 1; --i)
            if (col[y][i] > require) MoveBall(y, x);
            else MoveBall(y, z);
        MoveBall(x, y, greater);
        MoveBall(z, y, m - greater);
        MoveBall(x, z, m - greater);
        MoveBall(y, x, m - greater);
        for (int i = m; i >= 1; --i)
            if (col[z][i] <= require || top[y] >= m) MoveBall(z, x);
            else MoveBall(z, y);
        request_finished[y] = true;
    }
}

void solve(int l, int r) {
    if (l == r) return;
    memset(request_finished, 0, sizeof(request_finished));
    int mid = (l + r) >> 1;
    for (int i = l; i <= mid; ++i)
        for (int j = mid + 1; j <= r; ++j)
            if (!request_finished[i] && !request_finished[j])
                divide(i, j, n + 1, mid);
    solve(l, mid);
    solve(mid + 1, r);
}

namespace file_io {
    void set_to_file(std::string file_name) {
        freopen((file_name + ".in").c_str(), "r", stdin);
        freopen((file_name + ".out").c_str(), "w", stdout);
    }
}

signed main() {
    file_io::set_to_file("ball");
    scanf("%d%d", &n, &m);
    for (int i = 1; i <= n; ++i) {
        for (int j = 1; j <= m; ++j)
            scanf("%d", col[i] + j);
        top[i] = m;
    }
    solve(1, n);
    printf("%d\n", k);
    for (int i = 1; i <= k; ++i)
        printf("%d %d\n", StepFrom[i], StepTo[i]);
    return 0;
}