//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      「ZJOI2020」染色游戏.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-02.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

struct Reader {
    char endch;
    Reader() { endch = '\0'; }
    template <typename Int>
    Reader& operator >> (Int &i) {
        i = baseFastio::read<Int>(endch);
        return *this;
    }
    template <typename Int>
    inline Int get_int() {
        return baseFastio::read<Int>();
    }
    inline char get_nxt() {
        return endch;
    }
} read;

struct Writer {
    Writer& operator << (const char *ch) {
        // char *p = ch;
        while (*ch) putchar(*(ch++));
        return *this;
    }
    Writer& operator << (const char ch) {
        putchar(ch);
        return *this;
    }
    template <typename Int>
    Writer& operator << (const Int i) {
        baseFastio::write(i);
        return *this;
    }
} write;

// #define int int64

int n, m, S, T;
bool s[507], t[507];
int head[507], to[1007], val[1007], nxt[1007], cnt;
int dis[507], dep[507];
#define AddE(u, v, l) to[++cnt] = v; val[cnt] = l; nxt[cnt] = head[u]; head[u] = cnt;

bool specialCheck() {
    if (t[1]) {
        write << "1000000\n";
        return true;
    }
    return false;
}

namespace basicTree {
    int f[507][10];
    bool hass[507], hast[507];
    int dfn[507], r[507], sear[507], dft;
    int cost[507], nearest_end[507], nearest_s[507];
    int las_node, dis_tot;
    struct Edge {
        int u, v, dis;
        bool operator < (const Edge &b) const {
            return dis < b.dis;
        }
    } e[250007]; int ecnt;
    
    inline void dfs(int u, int fa) {
        dfn[u] = ++dft;
        sear[dft] = u;
        f[u][0] = fa;
        dep[u] = dep[fa] + 1;
        hass[u] = s[u];
        hast[u] = t[u];
        for (int i = 0; i < 9; i++)
            f[u][i + 1] = f[f[u][i]][i];
        for (int e = head[u]; ~e; e = nxt[e]) {
            if (to[e] != fa) {
                dfs(to[e], u);
                hass[u] |= hass[to[e]];
                hast[u] |= hast[to[e]];
            }
        }
        r[u] = dft;
        // printf("node %d : manage [%d, %d]\n", u, dfn[u], r[u]);
    }
    inline void jump(int &u, int k) {
        for (int i = 0; i < 10; i++)
            if (k & (1 << i)) u = f[u][i];
    }
    inline int lca(int u, int v) {
        if (dep[u] < dep[v]) swap(u, v);
        jump(u, dep[u] - dep[v]);
        if (u == v) return u;
        for (int k = 9; f[u][0] ^ f[v][0]; k--)
            if (f[u][k] ^ f[v][k]) {
                u = f[u][k];
                v = f[v][k];
            }
        return f[u][0];
    }
    inline int dist(int u, int v) {
        return dis[u] + dis[v] - 2 * dis[lca(u, v)];
    }
    inline int solve(int u) {
        nearest_end[u] = s[u] ? 0 : 1000000;
        nearest_s[u] = s[u] ? 0 : 2000000;
        cost[u] = 0;
        int rid = 0;
        for (int e = head[u]; ~e; e = nxt[e]) {
            if (to[e] != f[u][0]) {
                dis[to[e]] = dis[u] - rid + val[e];
                rid = min(rid + solve(to[e]), dis[u]);
                cost[u] += cost[to[e]];
                nearest_end[u] = min(nearest_end[u], nearest_end[to[e]] + val[e]);
                nearest_s[u] = min(nearest_s[u], nearest_s[to[e]] + val[e]);
            }
        }
        if (u != 1 && (t[u] || max(nearest_s[u] - dis[u] + 1, 0) < cost[u])) {
            // printf("[%d | %d] ", nearest_s[u], dis[u]);
            cost[u] = hass[u] ? max(nearest_s[u] - dis[u] + 1, 0) : 3000000;
            rid = max(nearest_s[u] - cost[u], 0);
            nearest_end[u] = hass[u] ? 0 : 1000000;
        }
        // printf("{ %d } ", dis[u]);
        // printf("node %d : nearest_s = %d, nearest_end = %d, cost = %d, rid = %d\n", u, nearest_s[u], nearest_end[u], cost[u], rid);
        return rid;
    }
    inline void getRest(int u) {
        if (!hast[u]) return;
        if (!nearest_end[u]) {
            dis_tot += dist(u, las_node) - 2;
            las_node = u;
            return;
        }
        for (int e = head[u]; ~e; e = nxt[e]) {
            if (to[e] != f[u][0]) {
                getRest(to[e]);
            }
        }
    }
}

signed _main_tree() {
    using namespace basicTree;
    dft = 0;
    dfs(1, 0);
    // dis[1] = -1;
    solve(1);
    for (int e = head[1]; ~e; e = nxt[e])
        if (!hass[to[e]] && hast[to[e]]) {
            write << "1000000\n";
            return 0;
        }
    las_node = 1;
    dis_tot = 0;
    getRest(1);
    dis_tot += dist(1, las_node);
    dis_tot >>= 1;
    // write << cost[1] << ' ' << nearest_end[1] << ' ' << dis_tot << ' ';
    // write << max(cost[1] - nearest_end[1] + 1, 0) << ' ';
    // write << max(cost[1] - dis_tot + 1, 0) << ' ';
    // write << max(cost[1] + 1, 0) << '\n';
    // write << cost[1] << " - " << dis_tot << " = ";
    // write << max(cost[1] - dis_tot, 0) << '\n';
    write << max(cost[1], 0) << '\n';
    return 0;
}

namespace circleBased {
    bool circle[507];
};

signed _main_circleBasedTree() {
    using namespace circleBased;
    puts("Sorry but I don't know the answer.");
    return 0;
}

signed main()
{
    int C = read.get_int<int>();
    while (C--) {
        cnt = 1;
        read >> n >> m >> S >> T;
        if (C == 10000 - 882) fprintf(stderr, "%d %d %d %d\n", n, m, S, T);
        for (int i = 1; i <= n; i++) {
            head[i] = -1;
            s[i] = t[i] = false;
        }
        for (int i = 1, u, v, l; i <= m; i++) {
            read >> u >> v >> l;
            AddE(u, v, l + 1);
            AddE(v, u, l + 1);
            if (C == 10000 - 882) fprintf(stderr, "%d %d %d\n", u, v, l);
        }
        if (C == 10000 - 882) {
            int ty;
            for (int i = 1; i <= S; i++) {
                s[ty = read.get_int<int>()] = true;
                fprintf(stderr, "%d%c", ty, " \n"[i == S]);
            }
            for (int i = 1; i <= T; i++) {
                t[ty = read.get_int<int>()] = true;
                fprintf(stderr, "%d%c", ty, " \n"[i == T]);
            }
        } else {
            for (int i = 1; i <= S; i++) s[read.get_int<int>()] = true;
            for (int i = 1; i <= T; i++) t[read.get_int<int>()] = true;
        }
        if (specialCheck()) continue;
        if (m == n - 1) _main_tree();
        else _main_circleBasedTree();
    }
    return 0;
}