//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      「APIO2018」选圆圈.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-07.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 2e6 + 7;
const double alpha = 0.75, eps = 1.1e-3;
const double theta = 0.19260817, sin_theta = sin(theta), cos_theta = cos(theta);

struct position {
    double v[2];
    double R;
    int id;
    bool operator < (const position &b) const {
        return R != b.R ? R > b.R : id < b.id;
    }
} pos[N];

int ans[N];

namespace kD_Tree {
    struct kD_Tree_Node {
        int id;
        double v[2];
        double R;
        double minv[2], maxv[2];
        int siz, remain;
        int l, r;
    } t[N];
    int memory_pool[N], pool_top;
    #define get_memory_from_pool() (memory_pool[pool_top--])
    #define recycle_memory(pos) (memory_pool[++pool_top] = (pos))
    #define get_memory() (pool_top ? get_memory_from_pool() : ++kD_Tree_Node_cnt)
    int kD_Tree_temp_node_cnt, kD_Tree_Node_cnt, root;
    inline bool cmp_v0_inc(const position &a, const position &b) {
        return a.v[0] != b.v[0] ? a.v[0] < b.v[0] : a.v[1] < b.v[1];
    }
    inline bool cmp_v1_inc(const position &a, const position &b) {
        return a.v[1] != b.v[1] ? a.v[1] < b.v[1] : a.v[0] < b.v[0];
    }
    typedef bool cmp_v_inc(const position &a, const position &b);
    cmp_v_inc *cmp_func[2] = { cmp_v0_inc, cmp_v1_inc };
    inline double pow2(const double rn) {
        return rn * rn;
    }
    inline double sqr_dist(const kD_Tree_Node &a, const double &x, const double &y) {
        return pow2(a.v[0] - x) + pow2(a.v[1] - y);
    }
    inline bool could_erase(const kD_Tree_Node &a, const double &x, const double &y, const double &R) {
        return pow2(a.R + R) - (pow2(a.v[0] - x) + pow2(a.v[1] - y)) > -eps;
    }
    inline bool is_maybe_reach(const kD_Tree_Node &a, const double &x, const double &y, const double &R) {
        return a.maxv[0] > x - R && a.minv[0] < x + R && a.maxv[1] > y - R && a.minv[1] < y + R;
    }
    inline void pushup(int u) {
        t[u].minv[0] = t[u].v[0] - t[u].R;
        t[u].maxv[0] = t[u].v[0] + t[u].R;
        t[u].minv[1] = t[u].v[1] - t[u].R;
        t[u].maxv[1] = t[u].v[1] + t[u].R;
        t[u].siz = 1;
        t[u].remain = 1;
        if (t[u].l) {
            t[u].minv[0] = min(t[u].minv[0], t[t[u].l].minv[0]);
            t[u].minv[1] = min(t[u].minv[1], t[t[u].l].minv[1]);
            t[u].maxv[0] = max(t[u].maxv[0], t[t[u].l].maxv[0]);
            t[u].maxv[1] = max(t[u].maxv[1], t[t[u].l].maxv[1]);
            t[u].siz += t[t[u].l].siz;
            t[u].remain += t[t[u].l].remain;
        }
        if (t[u].r) {
            t[u].minv[0] = min(t[u].minv[0], t[t[u].r].minv[0]);
            t[u].minv[1] = min(t[u].minv[1], t[t[u].r].minv[1]);
            t[u].maxv[0] = max(t[u].maxv[0], t[t[u].r].maxv[0]);
            t[u].maxv[1] = max(t[u].maxv[1], t[t[u].r].maxv[1]);
            t[u].siz += t[t[u].r].siz;
            t[u].remain += t[t[u].r].remain;
        }
    }
    void build(int &u, int l, int r, int D) {
        if (l > r) return;
        u = get_memory();
        int mid = (l + r) >> 1;
        nth_element(pos + l, pos + mid, pos + r + 1, cmp_func[D]);
        t[u].v[0] = pos[mid].v[0];
        t[u].v[1] = pos[mid].v[1];
        t[u].id = pos[mid].id;
        t[u].R = pos[mid].R;
        build(t[u].l, l, mid - 1, !D);
        build(t[u].r, mid + 1, r, !D);
        pushup(u);
    }
    // void flatten(int u) {
    //     if (t[u].l) flatten(t[u].l);
    //     pos[++kD_Tree_temp_node_cnt].v[0] = t[u].v[0];
    //     pos[kD_Tree_temp_node_cnt].v[1] = t[u].v[1];
    //     if (t[u].r) flatten(t[u].r);
    //     t[u].l = t[u].r = 0;
    //     recycle_memory(u);
    // }
    // inline void rebuild(int &u, int D) {
    //     kD_Tree_temp_node_cnt = 0;
    //     flatten(u);
    //     build(u, 1, kD_Tree_temp_node_cnt, D);
    // }
    // void maintain_balance(int &u, int D) {
    //     if (!u) return;
    //     else if (t[t[u].l].siz >= t[u].siz * alpha || t[t[u].r].siz >= t[u].siz * alpha) rebuild(u, D);
    //     else if (t[t[u].l].siz > t[t[u].r].siz) maintain_balance(t[u].l, !D);
    //     else maintain_balance(t[u].r, !D);
    // }
    // void insert(int &u, const position &a, int D) {
    //     if (!u) {
    //         u = get_memory();
    //         t[u].v[0] = a.v[0];
    //         t[u].v[1] = a.v[1];
    //         t[u].id = a.id;
    //         t[u].R = a.R;
    //     } else if (a.v[D] < t[u].v[D]) {
    //         insert(t[u].l, a, !D);
    //     } else {
    //         insert(t[u].r, a, !D);
    //     }
    //     pushup(u);
    // }
    // void insert(const int &x, const int &y) {
    //     static position ForInsertTempPosition;
    //     ForInsertTempPosition.v[0] = x;
    //     ForInsertTempPosition.v[1] = y;
    //     insert(root, ForInsertTempPosition, 0);
    //     maintain_balance(root, 0);
    // }
    void find_could_union(const int &u, const double &x, const double &y, const double &R, const int &erase_from) {
        // if (!ans[t[u].id]) printf("circle %d : meet { { %.3lf, %.3lf }, %.3lf, %d }, could_erase = %d\n", 
        //                            t[u].id, x, y, R, erase_from, could_erase(t[u], x, y, R));
        if (!ans[t[u].id] && could_erase(t[u], x, y, R)) {
            ans[t[u].id] = erase_from;
            t[u].remain--;
        }
        if (!is_maybe_reach(t[u], x, y, R) || !t[u].remain) return;
        if (t[u].l) find_could_union(t[u].l, x, y, R, erase_from);
        if (t[u].r) find_could_union(t[u].r, x, y, R, erase_from);
    }
}

namespace against_cpp11 {
    int n, k;
    signed main() {
        read >> n;
        for (int i = 1, x, y, r; i <= n; i++) {
            read >> x >> y >> r;
            pos[i].v[0] = x * sin_theta + y * cos_theta;
            pos[i].v[1] = x * cos_theta - y * sin_theta;
            // pos[i].v[0] = x;
            // pos[i].v[1] = y;
            pos[i].R = r;
            pos[i].id = i;
        }
        kD_Tree::build(kD_Tree::root, 1, n, 0);
        sort(pos + 1, pos + n + 1);
        for (int i = 1; i <= n; i++) {
            if (ans[pos[i].id]) continue;
            // printf("step %d : choose %d to erase circles\n", i, pos[i].id);
            kD_Tree::find_could_union(kD_Tree::root, pos[i].v[0], pos[i].v[1], pos[i].R, pos[i].id);
        }
        for (int i = 1; i <= n; i++)
            write << ans[i] << " \n"[i == n];
        return 0;
    }
}

signed main() { return against_cpp11::main(); }