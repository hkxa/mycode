/*************************************
 * @problem:      「NOI2016」优秀的拆分.
 * @user_name:    brealid/hkxadpall/zhaoyi20/j(o)moo/littleTortoise.
 * @time:         2020-08-31.
 * @language:     C++.
 * @fastio_ver:   20200827.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        // simple io flags
        ignore_int = 1 << 0,    // input
        char_enter = 1 << 1,    // output
        flush_stdout = 1 << 2,  // output
        flush_stderr = 1 << 3,  // output
        // combination io flags
        endline = char_enter | flush_stdout // output
    };
    enum number_type_flags {
        // simple io flags
        output_double_stable = 1 << 0,  // output
        output_double_faster = 1 << 1   // output
        // combination io flags
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f & ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore character set : ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (double &lf) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            lf = endch & 15;
            while (isdigit(endch = getchar())) lf = lf * 10 + (endch & 15);
            if (endch == '.') {
                double digit = 0.1;
                while (isdigit(endch = getchar())) {
                    lf += (endch & 15) * digit;
                    digit *= 0.1;
                }
            }
            if (flag) lf = -lf;
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            Int d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return d;
        }
    } read;

    struct Writer {
        Writer& operator << (io_flags f) {
            if (f & char_enter) putchar(10);
            if (f & flush_stdout) fflush(stdout);
            if (f & flush_stderr) fflush(stderr);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
        inline void operator () (double val, int eps_digit = 6, char endch = '\0', number_type_flags flg = output_double_faster) {
            if (flg & output_double_stable) {
                static char output_format[10];
                sprintf(output_format, "%%.%dlf", eps_digit);
                printf(output_format, val);
            } else if (flg & output_double_faster) {
                if (val < 0) {
                    putchar('-');
                    val = -val;
                }
                double eps_number = 0.5;
                for (int i = 1; i <= eps_digit; i++) eps_number /= 10;
                val += eps_number;
                (*this) << (int64)val;
                val -= (int64)val;
                putchar('.');
                while (eps_digit--) {
                    val *= 10;
                    putchar((int)val | '0');
                    val -= (int)val;
                }
            } else {
                (*this) << "1.#ERROR_NOT_IDENTIFIED_FLAG";
            }
            if (endch) putchar(endch);
        }
    } write;
}
using namespace Fastio;
namespace File_IO {
    void init_IO() {
        freopen("「NOI2016」优秀的拆分.in", "r", stdin);
        freopen("「NOI2016」优秀的拆分.out", "w", stdout);
    }
}

// #define int int64

const int N = 3e4, LG = 15;
int T, n;
int lg2[N + 5];
int a[N + 5], b[N + 5];
int64 ans = INT64_MIN;
struct SuffixArray {
    int n, m;
    char a[N + 5];
    int sa[N + 5], rk[N * 2 + 5], tpr[N * 2 + 5], c[N + 5], height[N + 5];
    int st[N + 5][LG + 5];
    void build() {
        memset(sa, 0, sizeof sa);
        memset(rk, 0, sizeof rk);
        memset(tpr, 0, sizeof tpr);
        n = strlen(a + 1), m = 127;
        memset(c, 0, sizeof c);
        for (int i = 1; i <= n; ++i)
            ++c[rk[i] = a[i]]; 
        for (int i = 1; i <= m; ++i)
            c[i] += c[i - 1]; 
        for (int i = n; i; --i)
            sa[c[rk[i]]--] = i; 
        for (int w = 1, p = 0; p < n; w <<= 1, m = p) {
            p = 0; 
            for (int i = 1; i <= w; ++i)
                tpr[++p] = n - w + i; 
            for (int i = 1; i <= n; ++i)
                if (sa[i] > w)
                    tpr[++p] = sa[i] - w; 
            memset(c, 0, sizeof c); 
            for (int i = 1; i <= n; ++i)
                ++c[rk[i]]; 
            for (int i = 1; i <= m; ++i)
                c[i] += c[i - 1]; 
            for (int i = n; i; --i)
                sa[c[rk[tpr[i]]]--] = tpr[i]; 
            swap(rk, tpr), rk[sa[1]] = p = 1; 
            for (int i = 2; i <= n; ++i)
                rk[sa[i]] = tpr[sa[i]] == tpr[sa[i - 1]] && tpr[sa[i] + w] == tpr[sa[i - 1] + w] ? p : ++p; 
        }
        for (int i = 1, k = 0; i <= n; ++i) {
            k -= (bool)k; 
            while (i + k <= n && sa[rk[i] - 1] + k <= n && a[i + k] == a[sa[rk[i] - 1] + k]) ++k;
            height[rk[i]] = st[rk[i]][0] = k; 
        }
        for (int i = 1; i <= LG; ++i)
            for (int j = 1; j + (1 << i) - 1 <= n; ++j)
                st[j][i] = min(st[j][i - 1], st[j + (1 << (i - 1))][i - 1]); 
    }
    int query(int l, int r) {
        l = rk[l], r = rk[r]; 
        if (l > r) swap(l, r); 
        ++l; 
        int lg = lg2[r - l + 1]; 
        return min(st[l][lg], st[r - (1 << lg) + 1][lg]); 
    }
} s, t; 

int main() {
    // File_IO::init_IO(); 
    for (int i = 2; i <= N; ++i)
        lg2[i] = lg2[i / 2] + 1; 
    read >> T;
    while (T--) {
        scanf("%s", s.a + 1); 
        n = strlen(s.a + 1); 
        for (int i = 1; i <= n; ++i)
            t.a[i] = s.a[n - i + 1]; 
        s.build(), t.build(); 
        memset(a, 0, sizeof a), memset(b, 0, sizeof b); 
        for (int w = 1; w <= n / 2; ++w)
            for (int i = w; i <= n; i += w) {
                int l = i, r = i + w; 
                int lcp = s.query(l, r), lcs = t.query(n - r + 2, n - l + 2); 
                lcp = min(lcp, w), lcs = min(lcs, w - 1); 
                if (lcp + lcs >= w) {
                    ++b[l - lcs];
                    --b[l - w + lcp + 1];
                    ++a[r + w - lcs - 1];
                    --a[r + lcp]; 
                }
            }
        for (int i = 1; i <= n; ++i)
            a[i] += a[i - 1], b[i] += b[i - 1]; 
        ans = 0; 
        for (int i = 1; i < n; ++i)
            ans += a[i] * b[i + 1]; 
        write << ans << endline;
    }
}