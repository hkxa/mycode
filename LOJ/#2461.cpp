/*************************************
 * @problem:      「2018 集训队互测 Day 1」完美的队列.
 * @user_name:    brealid/hkxadpall/zhaoyi20/j(o)moo/littleTortoise.
 * @time:         2020-09-26.
 * @language:     C++.
 * @fastio_ver:   20200913.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0,    // input
        flush_stdout = 1 << 1,  // output
        flush_stderr = 1 << 2,  // output
    };
    enum number_type_flags {
        output_double_stable = 1 << 0,  // output
        output_double_faster = 1 << 1   // output
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f & ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore character set = {' ', '\r', '\n', '\t'}
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (double &lf) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            lf = endch & 15;
            while (isdigit(endch = getchar())) lf = lf * 10 + (endch & 15);
            if (endch == '.') {
                double digit = 0.1;
                while (isdigit(endch = getchar())) {
                    lf += (endch & 15) * digit;
                    digit *= 0.1;
                }
            }
            if (flag) lf = -lf;
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            Int d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return d;
        }
    } read;

    struct Writer {
        Writer& operator << (io_flags f) {
            if (f & flush_stdout) fflush(stdout);
            if (f & flush_stderr) fflush(stderr);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
        inline void operator () (double val, int eps_digit = 6, char endch = '\0', number_type_flags flg = output_double_faster) {
            if (flg & output_double_stable) {
                static char output_format[10];
                sprintf(output_format, "%%.%dlf", eps_digit);
                printf(output_format, val);
            } else if (flg & output_double_faster) {
                if (val < 0) {
                    putchar('-');
                    val = -val;
                }
                double eps_number = 0.5;
                for (int i = 1; i <= eps_digit; i++) eps_number /= 10;
                val += eps_number;
                (*this) << (int64)val;
                val -= (int64)val;
                if (eps_digit) putchar('.');
                while (eps_digit--) {
                    val *= 10;
                    putchar((int)val | '0');
                    val -= (int)val;
                }
            } else {
               (*this) << '1';
                if (eps_digit) {
                    (*this) << ".E";
                    for (int i = 2; i <= eps_digit; i++) (*this) << '0';
                }
            }
            if (endch) putchar(endch);
        }
    } write;
}
using namespace Fastio;

namespace File_IO {
    void init_IO(const char *file_name) {
        char buff[107];
        sprintf(buff, "%s.in", file_name);
        freopen(buff, "r", stdin);
        sprintf(buff, "%s.out", file_name);
        freopen(buff, "w", stdout);
    }
}

const int N = 1e5 + 7, BN = 325;

int n; // n: std::queue<int> 的个数
int m; // m: 操作个数
int q_siz[N]; // 记录每个 std:queue<int> 的 max_size (created by readIn)
int kill_time[N]; // 被完全销毁的时间

int bsiz; // 块大小
int belong[N]; // 属于哪个块

// 记录 OpInfo
struct OpInfo {
    int x; // 这个操作的颜色
    int l, r; // 这个操作的区间 [l, r]
} op[N];

vector<int> b_op[BN]; // b_op[i]: 与块 i 有关的操作
vector<pair<int, int> > col_exist[N]; // col_exist[c]: 颜色 c 存在的时间

int ans[N]; // 记录答案（输出前为差分值）

#define all_cover(opx) (bl >= opx.l && br <= opx.r)

// 处理第 bid 块 -> Range[bl, br] 的 all_cover 的操作
void block_solve_all_cover(int bid, int bl, int br) {
    // printf("deal block %d -> range[%d, %d]\n", bid, bl, br);
    static int now[N]; // 当前的 q.size() (剩余的)
    int dif = 0; // 标记: now 整体加减
    int mx_now = 0; // now[bl:br] 的最大元素
    for (int i = bl; i <= br; i++) {
        now[i] = q_siz[i];
        mx_now = max(mx_now, now[i]);
    }
    for (int i = 0, j = -1; i < b_op[bid].size(); i++) { // i, j 双指针
        int id = b_op[bid][i]; // 操作编号 id
        if (all_cover(op[id])) {
            if (i <= j) ++dif; // 去除操作 id 自身的影响
        } else {
            if (i <= j) { // 被处理过
                // 暴力维护 now
                mx_now = 0;
                for (int k = bl; k <= br; k++) {
                    if (k >= op[id].l && k <= op[id].r)
                        ++now[k];
                    mx_now = max(mx_now, now[k]);
                }
            }
            continue; // 这部分答案无法处理, 在 block_solve_not_all_cover 中处理
        }
        if (i > j) j = i; // 指针 j 快速跳跃
        // 未越界且 now 未被清零
        while (j + 1 < b_op[bid].size() && mx_now + dif > 0) {
            ++j;
            int p = b_op[bid][j];
            if (all_cover(op[p])) {
                --dif; // 整块操作: 直接打标
            } else {
                // 暴力维护 now
                mx_now = 0;
                for (int k = bl; k <= br; k++) {
                    if (k >= op[p].l && k <= op[p].r)
                        --now[k];
                    mx_now = max(mx_now, now[k]);
                }
            }
        }
        // printf("upd kill_time[%d] using %d\n", id, mx_now + dif > 0 ? m + 1 : b_op[bid][j]);
        if (mx_now + dif > 0) kill_time[id] = max(kill_time[id], m + 1);
        else kill_time[id] = max(kill_time[id], b_op[bid][j]);
    }
}

// 处理第 bid 块 -> Range[bl, br] 的未 all_cover 的操作
void block_solve_not_all_cover(int bid, int bl, int br) {
    static int v0[N], c0; // 记录未完全覆盖的 Query
    static int v1[N], c1; // 记录完全覆盖的 Query
    static int pre[N]; // 记录未完全覆盖的 Query 的前一个最近的完全覆盖的 Query 在 v1 中的 id
    c0 = c1 = 0;
    for (int i = 0; i < b_op[bid].size(); i++) {
        int p = b_op[bid][i];
        if (all_cover(op[p])) v1[++c1] = p;
        else v0[++c0] = p, pre[c0] = c1;
    }
    v0[++c0] = 0; // 终止处理标志
    pre[c0] = c1;
    for (int x = bl; x <= br; x++) {
        int cnt = q_siz[x];
        for (int i = 1, j = -1; i <= c0; i++) { // i, j 双指针
            int id = v0[i];
            if (x > op[id].r || x < op[id].l) continue; // 不在区间中的元素的处理
            if (i <= j) ++cnt; // 去除影响
            else j = i; // 指针 j 快速跳跃
            while (j + 1 <= c0 && cnt - (pre[j] - pre[i]) > 0) { // 未越界且 now 未被清零
                ++j;
                if (x >= op[v0[j]].l && x <= op[v0[j]].r) --cnt;
            }
            bool j_in_range = (x >= op[v0[j]].l && x <= op[v0[j]].r);
            int val = cnt - (pre[j] - pre[i]);
            if (val > 0) kill_time[id] = max(kill_time[id], m + 1);
            else if (val == 0 && j_in_range) kill_time[id] = max(kill_time[id], v0[j]);
            else kill_time[id] = max(kill_time[id], v1[pre[i] + cnt + j_in_range]);
        }
    }
}

signed main() {
    fprintf(stderr, "Warning: used fread\nTry Ctrl+Z in local mode\n");
    read >> n >> m;
    bsiz = sqrt(n);
    int maxc = 0; // 最大的颜色值
    for (int i = 1; i <= n; i++) {
        read >> q_siz[i];
        belong[i] = (i - 1) / bsiz + 1;
    }
    for (int i = 1; i <= m; i++) {
        read >> op[i].l >> op[i].r >> op[i].x;
        maxc = max(maxc, op[i].x);
        int lef = belong[op[i].l], rig = belong[op[i].r]; // 与操作 i 有关的块 [lef, rig]
        // printf("op-%d related block : [%d, %d]\n", i, lef, rig);
        for (int j = lef; j <= rig; j++)
            b_op[j].push_back(i);
    }
    for (int i = 1; i <= belong[n]; i++) {
        int bl = (i - 1) * bsiz + 1, br = min(i * bsiz, n);
        block_solve_all_cover(i, bl, br);
        block_solve_not_all_cover(i, bl, br);
    }
    for (int i = 1; i <= m; i++) col_exist[op[i].x].push_back(make_pair(i, kill_time[i]));
    for (int col = 1; col <= maxc; col++) {
        int lst = -1;
        for (size_t idx = 0; idx < col_exist[col].size(); idx++) {
            // printf("col %d : exist time %d~%d\n", col, col_exist[col][idx].first, col_exist[col][idx].second);
            int lef = max(lst, col_exist[col][idx].first), rig = col_exist[col][idx].second;
            if (lef < rig) {
                ++ans[lef];
                --ans[lst = rig];
            }
        }
    }
    for (int i = 1; i <= m; i++)
        write << (ans[i] += ans[i - 1]) << '\n';
    return 0;
}