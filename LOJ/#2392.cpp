/*************************************
 * @problem:      「JOISC 2017 Day 1」烟花棒.
 * @user_name:    brealid/hkxadpall/zhaoyi20/j(o)moo/littleTortoise.
 * @time:         2020-08-30.
 * @language:     C++.
 * @fastio_ver:   20200827.
*************************************/ 
/**
 * function 'check_remain_period' unfinished
 * note : 时间倒流地考虑剩余的两段
 */

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        // simple io flags
        ignore_int = 1 << 0,    // input
        char_enter = 1 << 1,    // output
        flush_stdout = 1 << 2,  // output
        flush_stderr = 1 << 3,  // output
        // combination io flags
        endline = char_enter | flush_stdout // output
    };
    enum number_type_flags {
        // simple io flags
        output_double_stable = 1 << 0,  // output
        output_double_faster = 1 << 1   // output
        // combination io flags
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f & ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore character set : ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (double &lf) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            lf = endch & 15;
            while (isdigit(endch = getchar())) lf = lf * 10 + (endch & 15);
            if (endch == '.') {
                double digit = 0.1;
                while (isdigit(endch = getchar())) {
                    lf += (endch & 15) * digit;
                    digit *= 0.1;
                }
            }
            if (flag) lf = -lf;
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            Int d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return d;
        }
    } read;

    struct Writer {
        Writer& operator << (io_flags f) {
            if (f & char_enter) putchar(10);
            if (f & flush_stdout) fflush(stdout);
            if (f & flush_stderr) fflush(stderr);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
        inline void operator () (double val, int eps_digit = 6, char endch = '\0', number_type_flags flg = output_double_faster) {
            if (flg & output_double_stable) {
                static char output_format[10];
                sprintf(output_format, "%%.%dlf", eps_digit);
                printf(output_format, val);
            } else if (flg & output_double_faster) {
                if (val < 0) {
                    putchar('-');
                    val = -val;
                }
                double eps_number = 0.5;
                for (int i = 1; i <= eps_digit; i++) eps_number /= 10;
                val += eps_number;
                (*this) << (int64)val;
                val -= (int64)val;
                putchar('.');
                while (eps_digit--) {
                    val *= 10;
                    putchar((int)val | '0');
                    val -= (int)val;
                }
            } else {
                (*this) << "1.#ERROR_NOT_IDENTIFIED_FLAG";
            }
            if (endch) putchar(endch);
        }
    } write;
}
using namespace Fastio;
namespace File_IO {
    void init_IO() {
        freopen("「JOISC 2017 Day 1」烟花棒.in", "r", stdin);
        freopen("「JOISC 2017 Day 1」烟花棒.out", "w", stdout);
    }
}

// #define int int64

const int N = 1e5 + 7;

int n, K, t;
int x[N];

bool check_remain_period(int64 now, queue<pair<int64, int64> > &l, queue<pair<int64, int64> > &r) {
    // all are dec.
    int nl = l.size(), nr = r.size();
    pair<int64, int64> lef[l.size()], rig[r.size()];
    priority_queue<pair<int64, int> > q;
    for (int i = 0; i < nl; i++) {
        lef[i] = l.front();
        l.pop();
        q.push(make_pair(lef[i].first, i));
        printf("lef[%d] : {%lld, %lld}\n", i, lef[i].first, lef[i].second);
    }
    for (int i = 0; i < nr; i++) {
        rig[i] = r.front();
        r.pop();
        q.push(make_pair(rig[i].first, i + nl));
        printf("rig[%d] : {%lld, %lld}\n", i, rig[i].first, rig[i].second);
    }
    for (int i = 0, j; i < nl; i++)  {
        j = i;
        int64 diff = lef[i].first, max_need = x[i + 1] - x[i];
        while (diff < 0 && --j >= 1) {
            max_need = max(max_need, x[j + 1] - x[j] - diff);
            diff += Speed - (x[j + 1] - x[j]);
        }
        // if (j == 0) diff += Speed;
        if (diff < 0) {
            for (int j = i; j >= 1; j--)
                lef.push(make_pair(x[j + 1] - x[j], Speed - (x[j + 1] - x[j])));
        } else lef.push(make_pair(max_need, diff));
    }
    for (int i = K, j; i < n; i = j + 1) {
        j = i;
        int64 diff = Speed - (x[i + 1] - x[i]), max_need = x[i + 1] - x[i];
        while (diff < 0 && ++j < n) {
            max_need = max(max_need, x[j + 1] - x[j] - diff);
            diff += Speed - (x[j + 1] - x[j]);
        }
        // if (j == n) diff += Speed;
        if (diff < 0) {
            for (int j = i; j < n; j++)
                rig.push(make_pair(x[j + 1] - x[j], Speed - (x[j + 1] - x[j])));
        } else rig.push(make_pair(max_need, diff));
    }
    int64 now = Speed;
    while (!lef.empty() && !rig.empty()) {
        // printf("#1. STATUS : Road Remain %lld [lef.front() = {%lld, %lld}] [rig.front() = {%lld, %lld}]\n", now, lef.front().first, lef.front().second, rig.front().first, rig.front().second);
        if (lef.front().first <= now && rig.front().first <= now) {
            if (lef.front().second < 0 && rig.front().second < 0) {
                // printf("### Special Check\n");
                return check_remain_period(now, lef, rig);
            } else if (lef.front().second > rig.front().second) {
                now += lef.front().second;
                lef.pop();
            } else {
                now += rig.front().second;
                rig.pop();
            }
        } else if (lef.front().first <= now) {
            now += lef.front().second;
            lef.pop();
        } else if (rig.front().first <= now) {
            now += rig.front().second;
            rig.pop();
        } else return false;
    }
    while (!lef.empty()) {
        // printf("#2. STATUS : Road Remain %lld [lef.front() = {%lld, %lld}]\n", now, lef.front().first, lef.front().second);
        if (lef.front().first > now) return false;
        now += lef.front().second;
        lef.pop();
    }
    while (!rig.empty()) {
        // printf("#3. STATUS : Road Remain %lld [rig.front() = {%lld, %lld}]\n", now, rig.front().first, rig.front().second);
        if (rig.front().first > now) return false;
        now += rig.front().second;
        rig.pop();
    }
    return true;
}

bool check(int V) {
    int64 Speed = (int64)V * t;
    queue<pair<int64, int64> > lef, rig;
    for (int i = K - 1, j; i >= 1; i = j - 1) {
        j = i;
        int64 diff = Speed - (x[i + 1] - x[i]), max_need = x[i + 1] - x[i];
        while (diff < 0 && --j >= 1) {
            max_need = max(max_need, x[j + 1] - x[j] - diff);
            diff += Speed - (x[j + 1] - x[j]);
        }
        // if (j == 0) diff += Speed;
        if (diff < 0) {
            for (int j = i; j >= 1; j--)
                lef.push(make_pair(x[j + 1] - x[j], Speed - (x[j + 1] - x[j])));
        } else lef.push(make_pair(max_need, diff));
    }
    for (int i = K, j; i < n; i = j + 1) {
        j = i;
        int64 diff = Speed - (x[i + 1] - x[i]), max_need = x[i + 1] - x[i];
        while (diff < 0 && ++j < n) {
            max_need = max(max_need, x[j + 1] - x[j] - diff);
            diff += Speed - (x[j + 1] - x[j]);
        }
        // if (j == n) diff += Speed;
        if (diff < 0) {
            for (int j = i; j < n; j++)
                rig.push(make_pair(x[j + 1] - x[j], Speed - (x[j + 1] - x[j])));
        } else rig.push(make_pair(max_need, diff));
    }
    int64 now = Speed;
    while (!lef.empty() && !rig.empty()) {
        // printf("#1. STATUS : Road Remain %lld [lef.front() = {%lld, %lld}] [rig.front() = {%lld, %lld}]\n", now, lef.front().first, lef.front().second, rig.front().first, rig.front().second);
        if (lef.front().first <= now && rig.front().first <= now) {
            if (lef.front().second < 0 && rig.front().second < 0) {
                // printf("### Special Check\n");
                return check_remain_period(now, lef, rig);
            } else if (lef.front().second > rig.front().second) {
                now += lef.front().second;
                lef.pop();
            } else {
                now += rig.front().second;
                rig.pop();
            }
        } else if (lef.front().first <= now) {
            now += lef.front().second;
            lef.pop();
        } else if (rig.front().first <= now) {
            now += rig.front().second;
            rig.pop();
        } else return false;
    }
    while (!lef.empty()) {
        // printf("#2. STATUS : Road Remain %lld [lef.front() = {%lld, %lld}]\n", now, lef.front().first, lef.front().second);
        if (lef.front().first > now) return false;
        now += lef.front().second;
        lef.pop();
    }
    while (!rig.empty()) {
        // printf("#3. STATUS : Road Remain %lld [rig.front() = {%lld, %lld}]\n", now, rig.front().first, rig.front().second);
        if (rig.front().first > now) return false;
        now += rig.front().second;
        rig.pop();
    }
    return true;
}

signed main() {
    // File_IO::init_IO();
    read >> n >> K >> t;
    t <<= 1;
    for (int i = 1; i <= n; i++) read >> x[i];
    write << "check(157794) : " << (check(157794) ? "True" : "False") << endline; return 0;
    // write << "check(173188) : " << (check(173188) ? "True" : "False") << endline; return 0;
    // write << "check(25317) : " << (check(25317) ? "True" : "False") << endline; return 0;
    int l = 0, r = 1e9, mid, ans;
    while (l <= r) {
        mid = (l + r) >> 1;
        if (check(mid)) r = mid - 1, ans = mid;
        else l = mid + 1;
    }
    write << ans << '\n';
    priority_queue<int> q;
    q.swap(1);
    return 0;
}