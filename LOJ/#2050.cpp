/*************************************
 * @problem:      [HNOI2016]树.
 * @user_name:    brealid.
 * @time:         2020-11-09.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if true
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

const int N = 1e5 + 7;
int n, m, Q;

struct s_PersistentSegmentTree {
    int root[N];
    struct PersistentSegmentTree_Node {
        int sum, ls, rs;
    } tr[N << 5];
    int TreeNow, cnt;
    s_PersistentSegmentTree() : TreeNow(0), cnt(0) {}
    void PST_insert(int &u, int v, int l, int r, int x) {
        tr[u = ++cnt].sum = tr[v].sum + 1;
        if (l == r) return;
        int mid = (l + r) >> 1;
        if (x <= mid) {
            tr[u].rs = tr[v].rs;
            PST_insert(tr[u].ls, tr[v].ls, l, mid, x);
        } else {
            tr[u].ls = tr[v].ls;
            PST_insert(tr[u].rs, tr[v].rs, mid + 1, r, x);
        }
    }
    // Tree { u - v }
    int PST_query_kth(int u, int v, int l, int r, int kth) {
        if (l == r) return l;
        int ls_sum = tr[tr[u].ls].sum - tr[tr[v].ls].sum, mid = (l + r) >> 1;
        if (kth <= ls_sum) return PST_query_kth(tr[u].ls, tr[v].ls, l, mid, kth);
        else return PST_query_kth(tr[u].rs, tr[v].rs, mid + 1, r, kth - ls_sum);
    }
    void insert(int value) {
        ++TreeNow;
        PST_insert(root[TreeNow], root[TreeNow - 1], 1, n, value);
    }
    int query_kth(int l, int r, int k) {
        return PST_query_kth(root[r], root[l - 1], 1, n, k);
    }
} PersistentSegmentTree;

struct s_TemplateTree {
    vector<int> trG[N];
    int siz[N], dep[N], fa[N], wson[N];
    int beg[N], dfn[N], lst[N], dft;
    void initial_dfs(int u, int father) {
        PersistentSegmentTree.insert(u);
        dfn[u] = ++dft;
        fa[u] = father;
        dep[u] = dep[father] + 1;
        siz[u] = 1;
        wson[u] = 0;
        for (size_t i = 0; i < trG[u].size(); ++i) {
            int &v = trG[u][i];
            if (v != father) {
                initial_dfs(v, u);
                siz[u] += siz[v];
                if (siz[v] > siz[wson[u]]) wson[u] = v;
            }
        }
        lst[u] = dft;
    }
    void dfs_for_tcs(int u, int cbeg) {
        beg[u] = cbeg;
        if (!wson[u]) return;
        dfs_for_tcs(wson[u], cbeg);
        for (size_t i = 0; i < trG[u].size(); ++i) {
            int &v = trG[u][i];
            if (v != fa[u] && v != wson[u]) dfs_for_tcs(v, v);
        }
    }
    void readin() {
        for (int i = 1, fr, to; i < n; ++i) {
            read >> fr >> to;
            trG[fr].push_back(to);
            trG[to].push_back(fr);
        }
    }
    void init() {
        initial_dfs(1, 0);
        dfs_for_tcs(1, 1);
    }
    inline int qLCA(int u, int v) {
        while (beg[u] != beg[v]) {
            if (dep[beg[u]] < dep[beg[v]]) v = fa[beg[v]];
            else u = fa[beg[u]];
        }
        return dep[u] < dep[v] ? u : v;
    }
    inline int qDist(int u, int v) {
        return dep[u] + dep[v] - (dep[qLCA(u, v)] << 1);
    }
    inline int qChainDist(int u, int upper) {
        return dep[u] - dep[upper];
    }
} TemplateTree;

namespace BigTree {
    int x[N];
    int64 dis[N];
    int siz[N], dep[N], fa[N], wson[N];
    int beg[N], dfn[N], dft;
    vector<pair<int, int> > trG[N];
    int64 endid[N], ttree_fa[N], orig_to[N];
    // 节点 u 是大树上的哪个节点
    int whichBigTrNode(int64 u, int RangeLim = m) {
        return lower_bound(endid, endid + RangeLim + 1, u) - endid;
    }
    // 节点 u 是模板树上的哪个节点
    int whichTemplateTrNode(int64 u, int id) {
        if (u <= n) return u;
        int &xnow = x[id];
        return PersistentSegmentTree.query_kth(TemplateTree.dfn[xnow], 
                                               TemplateTree.lst[xnow], 
                                               u - endid[id - 1]);
    }
    void initial_dfs(int u) {
        dfn[u] = ++dft;
        dep[u] = dep[fa[u]] + 1;
        siz[u] = 1;
        wson[u] = 0;
        for (size_t i = 0; i < trG[u].size(); ++i) {
            int &v = trG[u][i].first;
            if (v != fa[u]) {
                dis[v] = dis[u] + trG[u][i].second;
                initial_dfs(v);
                siz[u] += siz[v];
                if (!wson[u] || siz[v] > siz[wson[u]]) wson[u] = v;
            }
        }
        // lst[u] = dft;
    }
    void dfs_for_tcs(int u, int cbeg) {
        beg[u] = cbeg;
        if (!wson[u]) return;
        dfs_for_tcs(wson[u], cbeg);
        for (size_t i = 0; i < trG[u].size(); ++i) {
            int &v = trG[u][i].first;
            if (v != fa[u] && v != wson[u]) dfs_for_tcs(v, v);
        }
    }
    void readin() {
        endid[0] = n;
        x[0] = 1;
        for (int i = 1; i <= m; ++i) {
            read >> x[i] >> orig_to[i];
            endid[i] = endid[i - 1] + TemplateTree.siz[x[i]];
            fa[i] = whichBigTrNode(orig_to[i], i);
            int dep_diff = TemplateTree.qChainDist(ttree_fa[i] = whichTemplateTrNode(orig_to[i], fa[i]), x[fa[i]]);
            trG[fa[i]].push_back(make_pair(i, dep_diff + 1));
        }
    }
    void init() {
        initial_dfs(0);
        dfs_for_tcs(0, 0);
    }
    bool CompareDfn(const int &u, const pair<int, int> &v)  {
        return dfn[u] < dfn[v.first];
    }
    inline int qWhichSon(int u, int v) {
        return (--upper_bound(trG[u].begin(), trG[u].end(), v, CompareDfn))->first;
    }
    inline bool in_one_chain(int u, int v) {
        if (dep[u] < dep[v]) swap(u, v);
        while (beg[u] != beg[v]) {
            u = fa[beg[u]];
            if (dep[u] < dep[v]) return false;
        }
        return true;
    }
    inline int64 qDist_OneChain(int64 u, int64 v, int idu, int idv) {
        if (dep[idu] < dep[idv]) swap(u, v), swap(idu, idv);
        int ttu = whichTemplateTrNode(u, idu), ttv = whichTemplateTrNode(v, idv);
        if (idu == idv) return TemplateTree.qDist(whichTemplateTrNode(u, idu), 
                                                  whichTemplateTrNode(v, idv));
        int near_v = qWhichSon(idv, idu);
        return dis[idu] - dis[near_v] + TemplateTree.qChainDist(ttu, x[idu]) + TemplateTree.qDist(ttv, ttree_fa[near_v]) + 1;
    }
    inline int qLCA(int u, int v) {
        while (beg[u] != beg[v]) {
            if (dep[beg[u]] < dep[beg[v]]) v = fa[beg[v]];
            else u = fa[beg[u]];
        }
        return dep[u] < dep[v] ? u : v;
    }
    inline int64 qDist(int64 u, int64 v) {
        int idu = whichBigTrNode(u), idv = whichBigTrNode(v);
        if (idu == idv) return TemplateTree.qDist(whichTemplateTrNode(u, idu), 
                                                  whichTemplateTrNode(v, idv));
        if (in_one_chain(idu, idv)) return qDist_OneChain(u, v, idu, idv);
        int BigTrNode_LCA = qLCA(idu, idv);
        int near_u = qWhichSon(BigTrNode_LCA, idu), near_v = qWhichSon(BigTrNode_LCA, idv);
        return qDist_OneChain(u, orig_to[near_u], idu, BigTrNode_LCA) + qDist_OneChain(v, orig_to[near_v], idv, BigTrNode_LCA) +
               TemplateTree.qDist(ttree_fa[near_u], ttree_fa[near_v]);
    }
};

signed main() {
    read >> n >> m >> Q;
    TemplateTree.readin();
    TemplateTree.init();
    BigTree::readin();
    BigTree::init();
    while (Q--) write << BigTree::qDist(read.get<int64>(), read.get<int64>()) << '\n';
    return 0;
}