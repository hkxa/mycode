//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      「APIO2018」铁人两项.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-11.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
        inline char get_option() {
            static char ForRead;
            ForRead = getchar();
            while (!isalnum(ForRead)) ForRead = getchar();
            return ForRead;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64
const int N = 200007;

namespace against_cpp11 {
    int n, m, q;
    int64 answer;
    vector<int> original_graph[N], G[N];
    namespace tarjan_values {
        int dfn[N], low[N], dft;
        bool instack[N];
        int sta[N], top;
    }
    int connected_subgraph_where[N], subgraph_id;
    int w[N];
    int square_node_cnt;
    int fa[N];
    namespace tree_chain_split_values {
        int dep[N], dis[N], siz[N], wson[N];
        int dfn[N], beg[N], dft;
    }
    multiset<int> s[N];
    void tarjan(int u) {
        using namespace tarjan_values;
        connected_subgraph_where[u] = subgraph_id;
        low[u] = dfn[u] = ++dft;
        sta[++top] = u;
        instack[u] = true;
        for (size_t i = 0; i < original_graph[u].size(); i++) {
            int &v = original_graph[u][i];
            if (!dfn[v]) {
                tarjan(v);
                if (dfn[u] <= low[v]) {
                    ++square_node_cnt;
                    do {
                        instack[sta[top]] = false;
                        G[square_node_cnt].push_back(sta[top]);
                        G[sta[top]].push_back(square_node_cnt);
                        w[square_node_cnt]++;
                    } while (sta[top--] != v);
                    G[square_node_cnt].push_back(u);
                    G[u].push_back(square_node_cnt);
                    w[square_node_cnt]++;
                }
                low[u] = min(low[u], low[v]);
            } else if (instack[v] && 1) {
                low[u] = min(low[u], dfn[v]);
            }
        }
    }
    void TCS_dfs1(int u, int f) {
        using namespace tree_chain_split_values;
        fa[u] = f;
        dep[u] = dep[f] + 1;
        dis[u] = dis[f] + w[u];
        siz[u] = u <= n;
        for (size_t i = 0; i < G[u].size(); i++) {
            int &v = G[u][i];
            if (v != f) {
                TCS_dfs1(v, u);
                siz[u] += siz[v];
                if (siz[v] >= siz[wson[u]]) wson[u] = v;
            }
        }
    }
    void TCS_dfs2(int u, int c_beg) {
        using namespace tree_chain_split_values;
        dfn[u] = ++dft;
        beg[u] = c_beg;
        if (wson[u]) TCS_dfs2(wson[u], c_beg);
        for (size_t i = 0; i < G[u].size(); i++) {
            int &v = G[u][i];
            if (v != fa[u] && v != wson[u]) TCS_dfs2(v, v);
        }
    }
    int query_LCA(int u, int v) {
        using tree_chain_split_values::beg;
        using tree_chain_split_values::dep;
        while (beg[u] != beg[v]) 
            if (dep[beg[u]] > dep[beg[v]]) u = fa[beg[u]];
            else v = fa[beg[v]];
        if (dep[u] > dep[v]) return v;
        else return u;
    }
    int query_dist(int u, int v) {
        using tree_chain_split_values::dis;
        if (connected_subgraph_where[u] != connected_subgraph_where[v]) return 0;
        int g = query_LCA(u, v);
        return dis[u] + dis[v] - dis[g] - dis[fa[g]];
    }
    void get_ans(int u, int root) {
        using namespace tree_chain_split_values;
        int sum = u <= n;
        for (size_t i = 0; i < G[u].size(); i++) {
            int &v = G[u][i];
            if (v != fa[u]) {
                get_ans(v, root);
                answer += (int64)w[u] * sum * siz[v];
                sum += siz[v];
            }
        }
        answer += (int64)w[u] * siz[u] * (siz[root] - siz[u]);
    }
    signed main() {
        using tree_chain_split_values::dfn;
        read >> n >> m;
        for (int i = 1; i <= n; i++) w[i] = -1;
        for (int i = 1, u, v; i <= m; i++) {
            read >> u >> v;
            original_graph[u].push_back(v);
            original_graph[v].push_back(u);
        }
        square_node_cnt = n;
        for (int i = 1; i <= n; i++)
            if (!dfn[i]) {
                ++subgraph_id;
                tarjan(i);
                TCS_dfs1(i, 0);
                TCS_dfs2(i, i);
                get_ans(i, i);
            }
        write << answer * 2 << '\n';
        return 0;
    }
}

signed main() { return against_cpp11::main(); }