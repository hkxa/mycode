//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      「ZJOI2020」字符串.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-01.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

struct Reader {
    char endch;
    Reader() { endch = '\0'; }
    template <typename Int>
    Reader& operator >> (Int &i) {
        i = baseFastio::read<Int>(endch);
        return *this;
    }
    template <typename Int>
    inline Int get_int() {
        return baseFastio::read<Int>();
    }
    inline char get_nxt() {
        return endch;
    }
} read;

struct Writer {
    Writer& operator << (const char *ch) {
        // char *p = ch;
        while (*ch) putchar(*(ch++));
        return *this;
    }
    Writer& operator << (const char ch) {
        putchar(ch);
        return *this;
    }
    template <typename Int>
    Writer& operator << (const Int i) {
        baseFastio::write(i);
        return *this;
    }
} write;

// #define int int64
const int N = 2e5 + 7, P1 = 16777213, P2 = 20170933;
const int SubtaskN = 5e3 + 7;

int n, q;
char s[N];
int pow1[N], pow2[N];
int str1[N], str2[N];
int conti[SubtaskN][SubtaskN], f[SubtaskN][SubtaskN];

inline int substr1(int l, int r) {
    return (str1[r] - (int64)str1[l - 1] * pow1[r - l + 1] % P1 + P1) % P1;
}

inline int substr2(int l, int r) {
    return (str2[r] - (int64)str2[l - 1] * pow2[r - l + 1] % P2 + P2) % P2;
}

inline pair<int, int> substr(int l, int r) {
    return make_pair(substr1(l, r), substr2(l, r));
}

inline bool is_beautiful(int l, int r) {
    return ((r - l) & 1) &&
           substr1(l, (l + r) >> 1) == substr1(((l + r) >> 1) + 1, r) &&
           substr2(l, (l + r) >> 1) == substr2(((l + r) >> 1) + 1, r);
}

signed main()
{
    read >> n >> q;
    scanf("%s", s + 1);
    pow1[0] = 1;
    pow2[0] = 1;
    for (int i = 1; i <= n; i++) {
        str1[i] = (str1[i - 1] * 3 + (s[i] - 'a' + 1)) % P1;
        str2[i] = (str2[i - 1] * 3 + (s[i] - 'a' + 1)) % P2;
        pow1[i] = (pow1[i - 1] * 3) % P1;
        pow2[i] = (pow2[i - 1] * 3) % P2;
    }
    // for (int j = 1; j <= n; j++) {
    //     for (int i = j; i >= 1; i--) {
    //         conti[i][j] = conti[i + 1][j] + is_beautiful(i, j);
    //     }
    //     for (int i = 1; i <= j; i++)
    //         write << conti[i][j] << " \n"[i == j];
    // }
    set<pair<int, int> > s;
    for (int i = 1; i <= n; i++) {
        // for (int j = 1; j < i; j++) write << "  ";
        s.clear();
        for (int j = i; j <= n; j++) {
            for (int k = i; k <= j; k++)
                if (is_beautiful(k, j))
                    s.insert(substr(k, j));
            f[i][j] = s.size();
        }
    }
    for (int i = 1, l, r; i <= q; i++) {
        read >> l >> r;
        // s.clear();
        // for (int i = l; i <= r; i++)
        //     for (int j = i; j <= r; j++)
        //         if (is_beautiful(i, j)) {
        //             // printf("beautiful string s[%d...%d] (hash = %d, %d)\n", i, j, substr1(i, j), substr2(i, j));
        //             s.insert(make_pair(substr1(i, j), substr2(i, j)));
        //         }
        // write << s.size() << '\n';
        write << f[l][r] << '\n';
    }
    return 0;
}