//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      「APIO2018」新家.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-13.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 3e5 + 7;

typedef multiset<int>::iterator ms_i_iter;
template <typename Iter> Iter nxt(Iter it) { return ++it; }
template <typename Iter> Iter pre(Iter it) { return --it; }

namespace against_cpp11 {
    const int MY_INT_MIN = INT_MIN / 3, MY_INT_MAX = INT_MAX / 3;
    int n, k, q, tot, root, tree_cnt, num;
    int tree[N << 4], ls[N << 4], rs[N << 4], ans[N];
    multiset<int> p[N], s[N << 4];
    struct node {
        int pos, tim, id, opt;
        bool operator < (const node &b) const {
            return tim ^ b.tim ? tim < b.tim : opt < b.opt;
        }
    } t[N << 2];
    void modify(int l, int r, int pos, int v, int type, int &cur) {
        if (!cur) cur = ++tree_cnt;
        if (l == r) {
            if (type) s[cur].insert(v);
            else s[cur].erase(s[cur].find(v));
            // for (ms_i_iter it = s[cur].begin(); it != s[cur].end() ;it++)
            //     printf("%d\n", *it);
            if (!s[cur].empty()) tree[cur] = *s[cur].begin();
            else tree[cur] = MY_INT_MAX;
            return;
        }
        int mid = (l + r) >> 1;
        if (pos <= mid) modify(l, mid, pos, v, type, ls[cur]);
        else modify(mid + 1, r, pos, v, type, rs[cur]);
        tree[cur] = min(tree[ls[cur]], tree[rs[cur]]);
    }
    int query(int pos) {
        if (num < k) return -1; // No Enough Stores
        int l = 1, r = MY_INT_MAX, cur = root, mid, midmi, rmi = MY_INT_MAX;
        while (l < r) {
            mid = (l + r) >> 1;
            midmi = min(rmi, tree[rs[cur]]);
            if (pos > mid || midmi < 2 * pos - mid)
                cur = rs[cur], l = mid + 1;
            else
                rmi = midmi, cur = ls[cur], r = mid;
        }
        return l - pos;
    }
    int main() {
        read >> n >> k >> q;
        tree[0] = MY_INT_MAX;
        for (int i = 1; i <= k; i++) {
            p[i].insert(MY_INT_MIN), p[i].insert(MY_INT_MAX);
            modify(1, MY_INT_MAX, MY_INT_MAX, MY_INT_MIN, 1, root);
        }
        for (int i = 1, x, id, a, b; i <= n; i++) {
            read >> x >> id >> a >> b;
            t[++tot] = (node){x, a, id, 1};
            t[++tot] = (node){x, b + 1, id, 0};
        }
        for (int i = 1,pos, tim; i <= q; i++) {
            read >> pos >> tim;
            t[++tot] = (node){pos, tim, i, 2};
        }
        sort(t + 1, t + tot + 1);
        for (int i = 1; i <= tot; i++) {
            int opt = t[i].opt, id = t[i].id, pos = t[i].pos;
            ms_i_iter it;
            if (opt == 0) {
                it = p[id].lower_bound(pos);
                modify(1, MY_INT_MAX, *nxt(it), pos, 0, root);
                modify(1, MY_INT_MAX, *nxt(it), *pre(it), 1, root);
                modify(1, MY_INT_MAX, pos, *pre(it), 0, root);
                p[id].erase(p[id].find(pos));
                if (p[id].size() == 2) num--;
            } else if (opt == 1) {
                it = p[id].lower_bound(pos);
                modify(1, MY_INT_MAX, *it, pos, 1, root);
                modify(1, MY_INT_MAX, *it, *pre(it), 0, root);
                modify(1, MY_INT_MAX, pos, *pre(it), 1, root);
                if (p[id].size() == 2) num++;
                p[id].insert(pos);
            } else if (opt == 2)
                ans[id] = query(pos);
        }
        for (int i = 1; i <= q; i++) write << ans[i] << '\n';
        return 0;
    }
}

signed main() { return against_cpp11::main(); }