//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      最小生成树计数.
 * @user_name:    brealid.
 * @time:         2020-06-08.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 31011;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 3; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

const int N = 300000 + 7;

int n, m, fa[N];
int back[N];

// struct edge {
//     int v, w;
// };
// vector<edge> G[N];

struct FullEdge {
    int u, v, w;
    bool used;
    bool operator < (const FullEdge &b) const { return w < b.w; }
} Ke[N];

int find(int x) { return fa[x] < 0 ? x : fa[x] = find(fa[x]); }
bool connect(int x, int y) {
    int fx = find(x), fy = find(y);
    if (fx != fy) {
        if (fa[fx] < fa[fy]) {
            fa[fy] = fx;
        } else if (fa[fx] > fa[fy]) {
            fa[fx] = fy;
        } else {
            fa[fy] = fx;
            fa[fx]--;
        }
        return true;
    }
    return false;
}

set<int> Used;

int PopCountS[16] = {0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4};
inline int popcount(int u) { return PopCountS[u & 15] + PopCountS[(u >> 4) & 15] + PopCountS[(u >> 8) & 15]; }

void Kruskal() {
    sort(Ke, Ke + m);
    memarr(n, -1, fa);
    int cnt = n - 1;
    for (int e = 0; e < m && cnt; e++) {
        if (connect(Ke[e].u, Ke[e].v)) {
            Ke[e].used = true;
            Used.insert(Ke[e].w);
            cnt--;
        }
    }
}

signed main() {
    n = read<int>();
    m = read<int>();
    for (int i = 0; i < m; i++) {
        Ke[i].u = read<int>();
        Ke[i].v = read<int>();
        Ke[i].w = read<int>();
        // G[Ke[i].u].push_back((edge){Ke[i].v, Ke[i].w});
        // G[Ke[i].v].push_back((edge){Ke[i].u, Ke[i].w});
    }
    Kruskal();
    vector<int> v;
    int ans = 1, cnt, siz, family, t;
    for (set<int>::iterator it = Used.begin(); it != Used.end(); it++) {
        v.clear();
        memarr(n, -1, fa);
        for (int e = 0; e < m; e++)
            if (Ke[e].w == *it) v.push_back(e);
            else if (Ke[e].used) connect(Ke[e].u, Ke[e].v);
        siz = v.size();
        family = cnt = 0;
        for (int i = 1; i <= n; i++) if (find(i) == i) family++;
        MEM_ARR(back, n, fa[i]);
        // printf("family = %d, siz = %d\n", family, siz);
        for (int i = 0; i < (1 << siz); i++) {
            t = family;
            // printf("%d%d%d%d%d : popcount = %d\n", !!(i & 16), !!(i & 8), !!(i & 4), !!(i & 2), !!(i & 1), popcount(i));
            if (popcount(i) + 1 != t) continue;
            MEM_ARR(fa, n, back[i]);
            for (int j = 0; j < siz; j++) {
                if (i & (1 << j)) t -= connect(Ke[v[j]].u, Ke[v[j]].v);
            }
            // printf("%d%d%d%d%d : t = %d\n", !!(i & 16), !!(i & 8), !!(i & 4), !!(i & 2), !!(i & 1), t);
            if (t == 1) cnt++;
        }
        ans = mul(ans, cnt);
        // printf("E w = %d : situation_count = %d\n", *it, cnt);
    }
    write(ans, 10);
    return 0;
}

// Create File Date : 2020-06-08