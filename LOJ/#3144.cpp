// not correct : int64 is not ok : int128 is expected


//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      「APIO2019」奇怪装置.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-18.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

struct Reader {
    char endch;
    Reader() { endch = '\0'; }
    template <typename Int>
    Reader& operator >> (Int &i) {
        i = baseFastio::read<Int>(endch);
        return *this;
    }
    template <typename Int>
    inline Int get_int() {
        return baseFastio::read<Int>();
    }
    inline char get_nxt() {
        return endch;
    }
} read;

struct Writer {
    Writer& operator << (const char *ch) {
        // char *p = ch;
        while (*ch) putchar(*(ch++));
        return *this;
    }
    Writer& operator << (const char ch) {
        putchar(ch);
        return *this;
    }
    template <typename Int>
    Writer& operator << (const Int i) {
        baseFastio::write(i);
        return *this;
    }
} write;

// #define int int64

const int N = 1e6 + 7;

int n;
int64 A, B;
int64 l[N], r[N];

namespace subtask1 {
    bool isDataFit() {
        int64 S = 0;
        for (int i = 1; i <= n; i++) {
            S += r[i] - l[i] + 1;
            if (S > 1000000) return false;
        }
        return true;
    }
    int solve() {
        set<pair<int64, int64> > s;
        for (int i = 1; i <= n; i++)
            for (int64 t = l[i]; t <= r[i]; t++)
                s.insert(make_pair((t + t / B) % A, t % B));
        write << s.size() << '\n';
        return 0;
    }
};

namespace subtask2 {
    bool isDataFit() {
        return false;
    }
    int solve() {
        return 0;
    }
};

namespace subtask3 {
    bool isDataFit() {
        return A <= 1000000 && B <= 1000000 && A * B <= 1000000;
    }
    int trans(int64 tim) {
        int64 x = (tim + tim / B) % A, y = tim % B;
        return x * B + y;
    }
    int solve() {
        set<pair<int64, int64> > s;
        bitset<1000007> occur;
        int64 C = A * B;
        for (int i = 1; i <= n; i++) {
            l[i] = l[i];
            r[i] = r[i];
            if (l[i] / C != r[i] / C) {
                s.insert(make_pair(l[i] % C, C - 1));
                s.insert(make_pair(0, r[i] % C));
            } else {
                s.insert(make_pair(l[i] % C, r[i] % C));
            }
        }
        int64 st = -1, ed = -2;
        for (set<pair<int64, int64> >::iterator it = s.begin(); it != s.end(); it++) {
            if (it->first - ed <= 1) ed = max(ed, it->second);
            else {
                for (int64 i = st; i <= ed; i++)
                    occur.set(trans(i));
                st = it->first;
                ed = it->second;
            }
        }
        for (int64 i = st; i <= ed; i++)
            occur.set(trans(i));
        write << occur.count() << '\n';
        return 0;
    }
};

namespace subtask4 {
    bool isDataFit() {
        return B == 1;
    }
    int solve() {
        if (!(A & 1)) A >>= 1;
        set<pair<int64, int64> > s;
        for (int i = 1; i <= n; i++) {
            if (l[i] / A != r[i] / A) {
                s.insert(make_pair(l[i] % A, A - 1));
                s.insert(make_pair(0LL, r[i] % A));
            } else {
                s.insert(make_pair(l[i] % A, r[i] % A));
            }
        }
        int64 st = -1, ed = -2, ans = 0;
        for (set<pair<int64, int64> >::iterator it = s.begin(); it != s.end(); it++) {
            if (it->first - ed <= 1) ed = max(ed, it->second);
            else {
                ans += ed - st + 1;
                st = it->first;
                ed = it->second;
            }
        }
        ans += ed - st + 1;
        write << ans << '\n';
        return 0;
    }
};

namespace subtask5 {
    bool isDataFit() {
        return false;
    }
    int solve() {
        return 0;
    }
};

namespace subtask6 {
    bool isDataFit() {
        return false;
    }
    int solve() {
        return 0;
    }
};

namespace subtask7 {
    bool isDataFit() {
        return false;
    }
    int solve() {
        return 0;
    }
};

namespace subtask8 {
    bool isDataFit() {
        return false;
    }
    int solve() {
        int64 G = A / __gcd(A, B + 1) * B;
        // printf("G = %lld\n", G);
        set<pair<int64, int64> > s;
        for (int i = 1; i <= n; i++) {
            int diff = abs(l[i] / G - r[i] / G);
            if (diff >= 2) {
                write << G << '\n';
                return 0;
            } else if (diff == 1) {
                s.insert(make_pair(l[i] % G, G - 1));
                s.insert(make_pair(0LL, r[i] % G));
            } else {
                s.insert(make_pair(l[i] % G, r[i] % G));
            }
        }
        int64 st = -1, ed = -2, ans = 0;
        for (set<pair<int64, int64> >::iterator it = s.begin(); it != s.end(); it++) {
            if (it->first - ed <= 1) ed = max(ed, it->second);
            else {
                ans += ed - st + 1;
                st = it->first;
                ed = it->second;
            }
        }
        ans += ed - st + 1;
        write << ans << '\n';
        return 0;
    }
};

signed main()
{
    read >> n >> A >> B;
    for (int i = 1; i <= n; i++) {
        read >> l[i] >> r[i];
    }
#define DEBUG subtask8
#ifdef DEBUG
    return DEBUG::solve();
#endif
    if (subtask1::isDataFit()) return subtask1::solve();
    if (subtask2::isDataFit()) return subtask2::solve();
    if (subtask3::isDataFit()) return subtask3::solve();
    if (subtask4::isDataFit()) return subtask4::solve();
    if (subtask5::isDataFit()) return subtask5::solve();
    if (subtask6::isDataFit()) return subtask6::solve();
    if (subtask7::isDataFit()) return subtask7::solve();
    if (subtask8::isDataFit()) return subtask8::solve();
    return 0;
}