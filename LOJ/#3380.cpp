#include <bits/stdc++.h>
using namespace std;
typedef long long int64;
typedef unsigned long long uint64;

class Reader {
public:
    char nxtch;
    Reader() : nxtch(0) {}
    template <typename T>
    Reader& operator >> (T &num) {
        bool negative = false;
        nxtch = getchar();
        while (nxtch < '0' || nxtch > '9') {
            if (nxtch == '-') negative = true;
            nxtch = getchar();
        }
        num = nxtch & 15;
        while ((nxtch = getchar()) >= '0' && nxtch <= '9') num = (((num << 2) + num) << 1) + (nxtch & 15);
        if (negative) num = ~num + 1;
        return *this;
    }
} read;

const int N = 1e6 + 7, CmaxMem = 1e8 + 7;
bool enabled_feed[CmaxMem];
int p[N], q[N];

uint64 calc_2k(int k) {
    if (k == 64) return (1ull << 63) | ((1ull << 63) - 1);
    else return (1ull << k) - 1;
}

int popcount(uint64 uu) {
    return uu ? popcount(uu ^ (uu & (~uu + 1))) + 1 : 0;
}

signed main() {
    freopen("zoo.in", "r", stdin);
    freopen("zoo.out", "w", stdout);
    int n, m, c, k;
    read >> n >> m >> c >> k;
    uint64 a_or = 0, a;
    for (int i = 1; i <= n; ++i) {
        scanf("%llu", &a);
        a_or |= a;
    }
    for (int i = 1; i <= m; ++i) {
        read >> p[i] >> q[i];
        if (a_or & (1ull << p[i])) enabled_feed[q[i]];
    }
    uint64 enable = calc_2k(k);
    for (int i = 1; i <= m; ++i)
        if (!(a_or & (1ull << p[i])) && !enabled_feed[q[i]])
            enable &= ~(1ull << p[i]);
    if (n == 0 && popcount(enable) == 64) puts("18446744073709551616");
    else printf("%llu\n", calc_2k(popcount(enable)) - (uint64)n + 1ull);
    return 0;
}