/*************************************
 * @problem:      「CTSC2016」时空旅行.
 * @author:       brealid.
 * @time:         2020-11-26.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;
namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
}
#define getchar() (i_P1 == i_P2 && (i_P2 = (i_P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), i_P1 == i_P2) ? EOF : *i_P1++)
char BUF[1 << 21], *i_P1 = BUF, *i_P2 = BUF;
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        inline void ignore_next_int() {
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            while (isdigit(endch = getchar()));
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 5e5 + 5;

int n, m, id[N];
struct planet_info {
    int64 x: 21, c0: 43;
} planet[N];
vector<int> next_universe[N];
int root[N << 2], trcnt;
struct funct {
    int64 k: 23, b: 41;
    funct() {}
    funct(int K, int64 B) : k(K), b(B) {}
    inline int64 calc(int64 x) const { return k * x + b; }
    inline bool operator == (const funct &rhs) const { return k == rhs.k && b == rhs.b; }
};
struct segt_node {
    int ls, rs;
    funct f;
    inline void clear() { f.k = 0; f.b = 1.09e12; }
} tr[N * 23];

void segt_insert(int &u, int l, int r, const funct &f) {
    if (!u) {
        tr[u = ++trcnt].f = f;
        return;
    }
    if (l == r) {
        if (f.calc(l) < tr[u].f.calc(l)) tr[u].f = f;
        return;
    }
    int mid = (l + r) >> 1;
    if (f.k > tr[u].f.k) {
        if (f.calc(mid) < tr[u].f.calc(mid)) {
            segt_insert(tr[u].rs, mid + 1, r, tr[u].f);
            tr[u].f = f;
        } else segt_insert(tr[u].ls, l, mid, f);
    } else {
        if (f.calc(mid) < tr[u].f.calc(mid)) {
            segt_insert(tr[u].ls, l, mid, tr[u].f);
            tr[u].f = f;
        } else segt_insert(tr[u].rs, mid + 1, r, f);
    }
}

int64 segt_query(int u, int l, int r, int x) {
    if (!u) return 1.09e12;
    int mid = (l + r) >> 1;
    return x <= mid ? min(tr[u].f.calc(x), segt_query(tr[u].ls, l, mid, x)) :
                      min(tr[u].f.calc(x), segt_query(tr[u].rs, mid + 1, r, x));
}

void range_insert(int u, int l, int r, int ml, int mr, const funct &f) {
    if (l >= ml && r <= mr) {
        segt_insert(root[u], -1e6, 1e6, f);
        return;
    }
    int mid = (l + r) >> 1;
    if (mid >= ml) range_insert(u << 1, l, mid, ml, mr, f);
    if (mid < mr) range_insert(u << 1 | 1, mid + 1, r, ml, mr, f);
}

int64 query(int u, int l, int r, int pos, int x) {
    if (l == r) return segt_query(root[u], -1e6, 1e6, x);
    int mid = (l + r) >> 1;
    return pos <= mid ? min(segt_query(root[u], -1e6, 1e6, x), query(u << 1, l, mid, pos, x)) :
                        min(segt_query(root[u], -1e6, 1e6, x), query(u << 1 | 1, mid + 1, r, pos, x));
}

vector<int> s[N];

template <typename Iter> Iter nxt(Iter x) { return ++x; }

int dfn[N], dft;
void predfs(int u) {
    dfn[u] = ++dft;
    if (id[u] >= 0) s[id[u]].push_back(dft);
    else s[-id[u]].push_back(dft - 1);
    for (uint32 i = 0; i < next_universe[u].size(); ++i) {
        int v = next_universe[u][i];
        predfs(v);
    }
    if (id[u] >= 0) s[id[u]].push_back(dft);
    else s[-id[u]].push_back(dft + 1);
}

signed main() {
    file_io::set_to_file("travel");
    tr[0].clear();
    kin >> n >> m;
    planet[0].c0 = kin.get<uint64>();
    for (int i = 1, opt, fr; i < n; ++i) {
        kin >> opt >> fr >> id[i];
        next_universe[fr].push_back(i);
        if (opt == 0) {
            planet[id[i]].x = kin.get<int64>();
            kin.ignore_next_int();
            kin.ignore_next_int();
            planet[id[i]].c0 = kin.get<uint64>();
        } else {
            id[i] = -id[i];
        }
    }
    // int mxx = 0;
    // for (int p = 0; p < n; ++p)
    //     mxx = max(mxx, abs(planet[p].x));
    predfs(0);
    for (int p = 0; p < n; ++p) {
        vector<int> &now = s[p];
        funct f(-2 * planet[p].x, (int64)planet[p].x * planet[p].x + planet[p].c0);
        for (size_t i = 0; i < now.size(); i += 2) {
            if (now[i] > now[i + 1]) continue;
            range_insert(1, 1, n, now[i], now[i + 1], f);
        }
    }
    for (int i = 1, s, x0; i <= m; ++i) {
        kin >> s >> x0;
        kout << query(1, 1, n, dfn[s], x0) + (int64)x0 * x0 << '\n';
    }
    return 0;
}

// qry x:
// ans = \min\limits_{i}{ info[i].x^2 + c0 - 2 * qry_x * info[i].x } + qry_x * qry_x
// k = -2 * info[i].x
// b = info[i].x^2 + c0