/*************************************
 * @problem:      迷失的字符串.
 * @user_name:    brealid/hkxadpall/zhaoyi20/j(o)moo/littleTortoise.
 * @time:         2020-08-31.
 * @language:     C++.
 * @fastio_ver:   20200827.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        // simple io flags
        ignore_int = 1 << 0,    // input
        char_enter = 1 << 1,    // output
        flush_stdout = 1 << 2,  // output
        flush_stderr = 1 << 3,  // output
        // combination io flags
        endline = char_enter | flush_stdout // output
    };
    enum number_type_flags {
        // simple io flags
        output_double_stable = 1 << 0,  // output
        output_double_faster = 1 << 1   // output
        // combination io flags
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f & ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore character set : ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (double &lf) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            lf = endch & 15;
            while (isdigit(endch = getchar())) lf = lf * 10 + (endch & 15);
            if (endch == '.') {
                double digit = 0.1;
                while (isdigit(endch = getchar())) {
                    lf += (endch & 15) * digit;
                    digit *= 0.1;
                }
            }
            if (flag) lf = -lf;
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            Int d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return d;
        }
    } read;

    struct Writer {
        Writer& operator << (io_flags f) {
            if (f & char_enter) putchar(10);
            if (f & flush_stdout) fflush(stdout);
            if (f & flush_stderr) fflush(stderr);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
        inline void operator () (double val, int eps_digit = 6, char endch = '\0', number_type_flags flg = output_double_faster) {
            if (flg & output_double_stable) {
                static char output_format[10];
                sprintf(output_format, "%%.%dlf", eps_digit);
                printf(output_format, val);
            } else if (flg & output_double_faster) {
                if (val < 0) {
                    putchar('-');
                    val = -val;
                }
                double eps_number = 0.5;
                for (int i = 1; i <= eps_digit; i++) eps_number /= 10;
                val += eps_number;
                (*this) << (int64)val;
                val -= (int64)val;
                putchar('.');
                while (eps_digit--) {
                    val *= 10;
                    putchar((int)val | '0');
                    val -= (int)val;
                }
            } else {
                (*this) << "1.#ERROR_NOT_IDENTIFIED_FLAG";
            }
            if (endch) putchar(endch);
        }
    } write;
}
using namespace Fastio;
namespace File_IO {
    void init_IO() {
        freopen("迷失的字符串.in", "r", stdin);
        freopen("迷失的字符串.out", "w", stdout);
    }
}

// #define int int64

const int inf = 0x3f3f3f3f, N = 30010, M = 35010;

struct Edge{
    int data, next, vote;
} e[N * 2];

int head[N], place, n, m, sum, s[N], t[N], ch[M], belong[M], result_output[N], q[N], pl, pr, use[N], wi[200]; 
char st[N];
bitset <M> f[N], g[N], pre, bak, hav[27], ans, nowpre, nowbak;

void build(int u, int v, int c){
    e[++place].data = v; e[place].next = head[u]; head[u] = place; e[place].vote = c;
}

void dfs(int x, int fa){
    for (int ed = head[x]; ed != 0; ed = e[ed].next)
        if (e[ed].data != fa)
            dfs(e[ed].data, x);
    q[++pr] = x;
}

void bfs(){
    pl = 1, pr = n; 
    while (pl <= pr){
        int x = q[pl++]; 
        use[x] = true;
        f[x] = pre; 
        g[x] = bak;
        for (int ed = head[x]; ed != 0; ed = e[ed].next)
            if (use[e[ed].data] == true){
                nowpre = (f[e[ed].data] << 1) & hav[e[ed].vote];
                nowbak = (g[e[ed].data] >> 1) & hav[e[ed].vote];
                ans = ans | (f[x] & (nowbak >> 1)) | (nowpre & (g[x] >> 1));
                f[x] = f[x] | nowpre; g[x] = g[x] | nowbak;
        }
    } 
}
int main(){
    // File_IO::init_IO();
    read >> n;
    for (int i = 1, u, v; i < n; i++){
        char c;
        read >> u >> v >> c;
        c -= 'a' - 1;
        build(u, v, c); build(v, u, c);
        wi[c] = true;
    }
    read >> m;
    sum = 0;
    for (int i = 1; i <= m; i++){
        s[i] = sum;
        scanf("%s", st + 1);  
        int l = strlen(st + 1); 
        if (l == 1){
            if (wi[st[1] - 'a' + 1] == true)
                result_output[i] = true;
            continue;
        }
        for (int j = 1; j <= l; j++)
            ch[sum + j] = st[j] - 'a' + 1;
        t[i] = sum + l + 1;
        for (int j = s[i]; j < t[i]; j++) belong[j] = i;
        sum = sum + l + 1;
    }
    for (int i = 1; i <= m; i++) pre.set(s[i]);
    for (int i = 1; i <= m; i++) bak.set(t[i]);
    for (int i = 1; i <= 26; i++)
        for (int j = 0; j <= sum; j++)
            if (ch[j] == i) hav[i].set(j);
    pl = 1, pr = 0; dfs(1, 0);
    bfs();
    for (int i = 0; i <= sum; i++)
        if (ans[i] == 1) result_output[belong[i]] = true;
    for (int i = 1; i <= m; i++)
        write << (result_output[i] == true ? "YES" : "NO") << endline;
    return 0;
}