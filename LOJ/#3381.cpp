/*************************************
 * @problem:      函数调用.
 * @user_name:    brealid.
 * @time:         2020-11-08.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

void File_IO_init(string file_name) {
    freopen((file_name + ".in" ).c_str(), "r", stdin);
    freopen((file_name + ".out").c_str(), "w", stdout);
}

const int N = 1e5 + 7, P = 998244353;
int n, m, Q;
int64 a[N];
struct funct {
    int typ, p, v;
    vector<int> CallFunc;
    friend Reader& operator >> (Reader &inp, funct &fun) {
        inp >> fun.typ;
        switch (fun.typ) {
            case 1: inp >> fun.p >> fun.v; break;
            case 2: inp >> fun.v; break;
            case 3: for (int k = inp.get<int>(); k--;) fun.CallFunc.push_back(inp.get<int>()); break;
        }
        return inp;
    }
} f[N];
int64 aftmul[N];
bool vis[N];
int ind[N];
int64 CalcFactor[N];

void init_aftmul(int u) {
    if (~aftmul[u]) return;
    switch (f[u].typ) {
        case 1: aftmul[u] = 1; break;
        case 2: aftmul[u] = f[u].v; break;
        case 3: 
            aftmul[u] = 1;
            vector<int> &Call = f[u].CallFunc;
            for (size_t i = 0; i < Call.size(); ++i) {
                int &v = Call[i];
                init_aftmul(v);
                aftmul[u] = aftmul[u] * aftmul[v] % P;
            }
            break;
    }
}

void init_ind(int u) {
    vis[u] = true;
    if (f[u].typ != 3) return;
    vector<int> &Call = f[u].CallFunc;
    for (size_t i = 0; i < Call.size(); ++i) {
        int &v = Call[i];
        ++ind[v];
        if (!vis[v]) init_ind(v);
    }
}

void solve_add() {
    CalcFactor[m] = 1;
    queue<int> q;
    q.push(m);
    while (!q.empty()) {
        int u = q.front(); q.pop();
        int64 now = CalcFactor[u];
        switch (f[u].typ) {
            case 1: a[f[u].p] = (a[f[u].p] + f[u].v * now) % P; break;
            case 2: break;
            case 3: 
                vector<int> &Call = f[u].CallFunc;
                for (int i = (int)Call.size() - 1; i >= 0; --i) {
                    int &v = Call[i];
                    CalcFactor[v] = (CalcFactor[v] + now) % P;
                    now = now * aftmul[v] % P;
                    if (!--ind[v]) q.push(v);
                }
                break;
        }
    }
}

signed main() {
    freopen("call.in", "r", stdin);
    freopen("call.out", "w", stdout);
    memset(aftmul, -1, sizeof(aftmul));
    read >> n;
    for (int i = 1; i <= n; ++i) read >> a[i];
    read >> m;
    for (int i = 1; i <= m; ++i) read >> f[i];
    read >> Q;
    f[++m].typ = 3;
    vector<int> &QueryCallFunction = f[m].CallFunc;
    for (int i = 1; i <= Q; ++i) QueryCallFunction.push_back(read.get<int>());
    init_aftmul(m);
    init_ind(m);
    for (int i = 1; i <= n; ++i) a[i] = a[i] * aftmul[m] % P;
    solve_add();
    for (int i = 1; i <= n; ++i) write << a[i] << ' ';
    write << '\n';
    return 0;
}