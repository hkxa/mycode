/*************************************
 * @problem:      「SDOI2016」探险路线.
 * @author:       brealid.
 * @time:         2020-11-16.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#if true
#define getchar() (i_P1 == i_P2 && (i_P2 = (i_P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), i_P1 == i_P2) ? EOF : *i_P1++)
char BUF[1 << 21], *i_P1 = BUF, *i_P2 = BUF;
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 800 + 3;
const int64 Inf = 0x3f3f3f3f3f3f3f3f;

int n, m, gain[N][N], now[N][N];
int64 sum_l[N][N], sum_u[N][N];
int64 f[N][N][2], L[N], R[N], up[N][2], dn[N][2], trans[N];
int64 ans = -Inf;

void update_max(int64 &val, int64 to_upd) { if (val < to_upd) val = to_upd; }

void init_sum_and_memsetInf() {
    for (int i = 1; i <= n; ++i) {
        for (int j = 1; j <= m; ++j) {
            sum_l[i][j] = sum_l[i][j - 1] + now[i][j];
            sum_u[i][j] = sum_u[i - 1][j] + now[i][j];
            f[i][j][0] = f[i][j][1] = -Inf;
        }
        L[i] = R[i] = -Inf;
    }
}

#define max_3(a, b, c) max(max(a, b), c)

void calc() {
    init_sum_and_memsetInf();
    for (int i = 1; i <= n; ++i)
        f[i][1][1] = sum_u[i][1], f[i][1][0] = now[1][1];
    for (int i = 1; i <= m; ++i)
        f[1][i][1] = now[1][1], f[1][i][0] = sum_l[1][i];
    for (int i = 2; i <= n; ++i) {
        for (int j = 2; j <= m; ++j) {
            update_max(f[i][j][0], max_3(f[i - 1][j][0],
                                     f[i - 1][j - 1][1] + sum_u[i - 1][j] + sum_l[i][j],
                                     f[i][j - 1][0] + now[1][j]));
            update_max(f[i][j][1], max_3(f[i][j - 1][1],
                                     f[i - 1][j - 1][0] + sum_u[i - 1][j] + sum_l[i][j],
                                     f[i - 1][j][1] + now[i][1]));
        }
    }
    for (int i = 1; i <= n; ++i)
        for (int j = 1; j <= m; ++j)
            update_max(L[i], f[i][j][1]);
    for (int i = 1; i <= m; ++i)
        trans[i] = sum_l[1][m] - sum_l[1][i];
    R[1] = sum_l[1][m];
    for (int i = 2; i <= n; ++i)
        for (int j = 1; j < m; ++j) {
            trans[j] = max(trans[j] + now[i][m], sum_u[i][j + 1] + sum_l[i][m] - sum_l[i][j + 1]);
            update_max(R[i], f[i][j][0] + trans[j]);
        }
}

void solve() {
    for (int i = 1; i <= n; ++i)
        for (int j = 1; j <= m; ++j)
            now[i][j] = gain[n - i + 1][m - j + 1];
    calc();
    for (int i = 1; i <= n; ++i)
        dn[i][0] = R[n - i + 1], dn[i][1] = L[n - i + 1];
    for (int i = 1; i <= n; ++i)
        for (int j = 1; j <= m; ++j)
            now[i][j] = gain[i][j];
    calc();
    for (int i = 1; i <= n; ++i)
        up[i][0] = L[i], up[i][1] = R[i];
    int64 l = 0, r = sum_l[1][m - 1];
    for (int i = 2; i <= n; ++i) {
        update_max(up[i][0], max(l + sum_u[i][1], r + sum_u[i][m] + sum_l[i][m] - now[i][m]));
        update_max(up[i][1], max(r + sum_u[i][m], l + sum_u[i][1] + sum_l[i][m] - now[i][1]));
        update_max(l, up[i][0] - sum_u[i][1]);
        update_max(r, up[i][1] - sum_u[i][m]);
    }
    l = sum_l[n][m], r = now[n][m];
    update_max(ans, max(dn[1][0], up[n][1]));
    for (int i = n - 1; i > 1; --i) {
        l = max(l + now[i][1], dn[i][0]);
        r = max(r + now[i][m], dn[i][1]);
        update_max(ans, max(l + up[i - 1][0], r + up[i - 1][1]));
    }
}
signed main() {
    kin >> n >> m;
    for (int i = 1; i <= n; ++i)
        for (int j = 1; j <= m; ++j)
            kin >> gain[i][j];
    solve();
    for (int i = 1; i <= n; ++i)
        for (int j = i + 1; j <= m; ++j)
            swap(gain[i][j], gain[j][i]);
    swap(n, m);
    solve();
    kout << ans << '\n';
    return 0;
}