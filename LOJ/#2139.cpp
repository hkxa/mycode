/*************************************
 * @problem:      「ZJOI2015」幻想乡 Wi-Fi 搭建计划.
 * @author:       brealid.
 * @time:         2020-11-16.
*************************************/ 
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#if true
#define getchar() (i_P1 == i_P2 && (i_P2 = (i_P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), i_P1 == i_P2) ? EOF : *i_P1++)
char BUF[1 << 21], *i_P1 = BUF, *i_P2 = BUF;
#endif

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 100 + 7;

int n, m;
int64 R;
int sx[N], sy[N];
int wx[N], wy[N], wc[N];
bool G[N][N];
int sight[N], cnts;
int wifi_up[N], cntu;
int wifi_dn[N], cntd;
int f[N][N][N];

inline bool check(int64 x1, int64 y1, int64 x2, int64 y2) {
    return (x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1) <= R;
}

template <int *compare_arg>
bool comp_less(const int &a, const int &b) {
    return compare_arg[a] < compare_arg[b];
}

signed main() {
    kin >> n >> m >> R;
    for (int i = 1; i <= n; ++i) kin >> sx[i] >> sy[i];
    for (int i = 1; i <= m; ++i) {
        kin >> wx[i] >> wy[i] >> wc[i];
        if (wy[i] > R) wifi_up[++cntu] = i;
        else wifi_dn[++cntd] = i;
    }
    R = R * R;
    for (int i = 1; i <= n; ++i) {
        bool have_a_connected_wifi = false;
        for (int j = 1; j <= m; ++j)
            if (check(sx[i], sy[i], wx[j], wy[j])) {
                G[i][j] = true;
                have_a_connected_wifi = true;
            }
        if (have_a_connected_wifi) sight[++cnts] = i;
    }
    sort(sight + 1, sight + cnts + 1, comp_less<sx>);
    sort(wifi_up + 1, wifi_up + cntu + 1, comp_less<wx>);
    sort(wifi_dn + 1, wifi_dn + cntd + 1, comp_less<wx>);
    for (int i = 0; i <= cnts; ++i)
        for (int j = 0; j <= cntu; ++j)
            for (int k = 0; k <= cntd; ++k)
                f[i][j][k] = 0x3f3f3f3f;
    f[0][0][0] = 0;
    for (int i = 1; i <= cnts; ++i)
        for (int j = 0; j <= cntu; ++j)
            for (int k = 0; k <= cntd; ++k) {
                int &f_value_pre = f[i - 1][j][k], &now = sight[i];
                if (f_value_pre < 0x3f3f3f3f) {
                    if (G[now][wifi_up[j]] || G[now][wifi_dn[k]])
                        f[i][j][k] = min(f[i][j][k], f_value_pre);
                    for (int l = j + 1; l <= cntu; ++l)
                        if (G[now][wifi_up[l]])
                            f[i][l][k] = min(f[i][l][k], f_value_pre + wc[wifi_up[l]]);
                    for (int l = k + 1; l <= cntd; ++l)
                        if (G[now][wifi_dn[l]])
                            f[i][j][l] = min(f[i][j][l], f_value_pre + wc[wifi_dn[l]]);
                }
            }
    int ans = 0x3f3f3f3f;
    for (int i = 0; i <= cntu; ++i)
        for (int j = 0; j <= cntd; ++j)
            ans = min(ans, f[cnts][i][j]);
    kout << cnts << '\n' << ans << '\n';
    return 0;
}