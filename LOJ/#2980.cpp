//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      「THUSCH 2017」大魔法师.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-12.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int P = 998244353, N = 2.5e5 + 7;

struct param { int a[4]; };
struct matrix { int a[4][4]; };

const param empty_param = {{0, 0, 0, 0}};
const matrix empty_matrix = {{{0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}}};
const matrix unit_matrix = {{{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}}};
const matrix opt1_matrix = {{{1, 0, 0, 0}, {1, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}}};
const matrix opt2_matrix = {{{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 1, 1, 0}, {0, 0, 0, 1}}};
const matrix opt3_matrix = {{{1, 0, 1, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}}};

param create_param(int A, int B, int C) { return (param){{A, B, C, 1}}; }
matrix create_opt4_matrix(int v) { return (matrix){{{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {v, 0, 0, 1}}}; }
matrix create_opt5_matrix(int v) { return (matrix){{{1, 0, 0, 0}, {0, v, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}}}; }
matrix create_opt6_matrix(int v) { return (matrix){{{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 0, 0}, {0, 0, v, 1}}}; }

bool operator != (const matrix x, const matrix y) {
    for (int i = 0; i < 4; i++)
        for (int j = 0; j < 4; j++)
            if (x.a[i][j] != y.a[i][j]) return true;
    return false;
}

matrix operator * (const matrix x, const matrix y) {
    matrix ret = empty_matrix;
    for (int i = 0; i < 4; i++)
        for (int j = 0; j < 4; j++)
            for (int k = 0; k < 4; k++)
                ret.a[i][k] = (ret.a[i][k] + (int64)x.a[i][j] * y.a[j][k]) % P;
    return ret;
}

param operator * (const param x, const matrix y) {
    param ret = empty_param;
    for (int i = 0; i < 4; i++)
        for (int j = 0; j < 4; j++)
            ret.a[j] = (ret.a[j] + (int64)x.a[i] * y.a[i][j]) % P;
    return ret;
}

param operator + (const param x, const param y) {
    param ret;
    for (int i = 0; i < 4; i++)
        ret.a[i] = (x.a[i] + y.a[i]) % P;
    return ret;
}

int n, m;
param a[N];
param t[N << 2];
matrix tag[N << 2];

void build_tree(int u, int l, int r) {
    tag[u] = unit_matrix;
    if (l == r) return void(t[u] = a[l]);
    int mid = (l + r) >> 1;
    build_tree(u << 1, l, mid);
    build_tree(u << 1 | 1, mid + 1, r);
    t[u] = t[u << 1] + t[u << 1 | 1];
}

void pushdown(int u) {
    if (tag[u] != unit_matrix) {
        t[u << 1] = t[u << 1] * tag[u];
        tag[u << 1] = tag[u << 1] * tag[u];
        t[u << 1 | 1] = t[u << 1 | 1] * tag[u];
        tag[u << 1 | 1] = tag[u << 1 | 1] * tag[u];
        tag[u] = unit_matrix;
    }
}

param query(int u, int l, int r, int ml, int mr) {
    if (l > mr || r < ml) return empty_param;
    if (l >= ml && r <= mr) return t[u];
    int mid = (l + r) >> 1;
    pushdown(u);
    return query(u << 1, l, mid, ml, mr) + query(u << 1 | 1, mid + 1, r, ml, mr);
}

matrix update_matrix_value;
void update(int u, int l, int r, int ml, int mr) {
    if (l > mr || r < ml) return;
    if (l >= ml && r <= mr) {
        t[u] = t[u] * update_matrix_value;
        tag[u] = tag[u] * update_matrix_value;
        return;
    }
    int mid = (l + r) >> 1;
    pushdown(u);
    update(u << 1, l, mid, ml, mr);
    update(u << 1 | 1, mid + 1, r, ml, mr);
    t[u] = t[u << 1] + t[u << 1 | 1];
}

signed main() {
    read >> n;
    for (int i = 1, A, B, C; i <= n; i++) {
        read >> A >> B >> C;
        a[i] = create_param(A, B, C);
    }
    build_tree(1, 1, n);
    read >> m;
    for (int i = 1, opt, l, r, v; i <= m; i++) {
        read >> opt >> l >> r;
        if (opt >= 4 && opt <= 6) read >> v;
        switch (opt) {
            case 1 :
                update_matrix_value = opt1_matrix;  
                update(1, 1, n, l, r);
                break;
            case 2 :
                update_matrix_value = opt2_matrix;  
                update(1, 1, n, l, r);
                break;
            case 3 :
                update_matrix_value = opt3_matrix;  
                update(1, 1, n, l, r);
                break;
            case 4 :
                update_matrix_value = create_opt4_matrix(v);  
                update(1, 1, n, l, r);
                break;
            case 5 :
                update_matrix_value = create_opt5_matrix(v);  
                update(1, 1, n, l, r);
                break;
            case 6 :
                update_matrix_value = create_opt6_matrix(v);  
                update(1, 1, n, l, r);
                break;
            case 7 : 
                param ret = query(1, 1, n, l, r);
                write << ret.a[0] << ' ' << ret.a[1] << ' ' << ret.a[2] << '\n';
                break;
        }
    }
    return 0;
}