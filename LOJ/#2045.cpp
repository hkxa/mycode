/*************************************
 * @problem:      [CQOI2016]密钥破解.
 * @author:       brealid.
 * @time:         2020-12-01.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace zymath {
    inline int64 qmul(int64 a, int64 b, int64 p) {
#ifdef ONLINE_JUDGE 
        return (__int128)a * b % p;
#endif
        int64 ret = 0;
        while (b) {
            if (b & 1) if ((ret += a) >= p) ret -= p;
            if ((a <<= 1) >= p) a -= p; 
            b >>= 1;
        }
        return ret;
    }
    inline int64 qpow(int64 a, int64 b, int64 p) {
        int64 ret = 1;
        while (b) {
            if (b & 1) ret = qmul(ret, a, p);
            a = qmul(a, a, p);
            b >>= 1;
        }
        return ret;
    }
    inline int64 gcd(int64 a, int64 b) {
        if (!a || !b) return a | b;
        int t = __builtin_ctzll(a | b);
        a >>= __builtin_ctzll(a);
        do {
            b >>= __builtin_ctzll(b) ;
            if (a > b) swap(a, b);
            b -= a;
        } while(b);
        return a << t;
    }
}

namespace NumberTheory {
    namespace Miller_Rabin_Prime {
        const int prime[] = {2, 61, 137};
        const size_t prime_cnt = sizeof(prime) / sizeof(int);
    }
}
class Miller_Rabin {
  public:
    inline bool operator () (int64 a) {
        using namespace NumberTheory::Miller_Rabin_Prime;
        for (size_t prime_id = 0; prime_id < prime_cnt; prime_id++)
            if (a == prime[prime_id]) return true;
        if (a < 2 || !(a & 1)) return false;
        int64 val = a - 1, cnt2 = 0;
        while (!(val & 1)) {
            val >>= 1;
            cnt2++;
        }
        for (size_t prime_id = 0; prime_id < prime_cnt; prime_id++) {
            int64 now = zymath::qpow(prime[prime_id] % a, val, a), newer_value;
            for (int i = 0; i < cnt2; i++) {
                newer_value = zymath::qmul(now, now, a);
                if (newer_value == 1 && now != 1 && now != a - 1) return false;
                now = newer_value;
            }
            if (now != 1) return false;
        }
        return true;
    }
};
class Pollard_Rho {
  private:
    Miller_Rabin test_prime;
    mt19937 rnd;
    inline int64 gen_next(int64 u, int64 c, int64 P) {
        return (zymath::qmul(u, u, P) + c) % P;
    }
    inline int64 get_a_factor(int64 a) {
        if (test_prime(a)) return a;
        while (true) {
            int64 c = rnd() % a, x = rnd() % a;
            int64 u = gen_next(x, c, a), v = gen_next(x, c, a);
            int64 now = 1;
            for (int goal = 1; goal <= 16384; goal <<= 1) {
                int cnt = 0;
                while (cnt <= goal) {
                    u = gen_next(u, c, a);
                    if (u != v) now = zymath::qmul(now, abs(u - v), a);
                    if (++cnt % 127 == 0) {
                        int64 g = zymath::gcd(now, a);
                        if (g > 1) return g;
                    }
                }
                int64 g = zymath::gcd(now, a);
                if (g > 1) return g;
                v = u;
            }
        }
    }
  public:
    Pollard_Rho() : rnd(chrono::steady_clock::now().time_since_epoch().count()) {}
    void resolve_factor_base(int64 a, vector<int64> &saveP) {
        if (a == 1) return;
        if (test_prime(a)) saveP.push_back(a);
        else {
            int64 ret = get_a_factor(a);
            resolve_factor_base(ret, saveP);
            resolve_factor_base(a / ret, saveP);
        }
    }
    vector<int64> resolve_factor(int64 a) {
        vector<int64> ret;
        if (a <= 1) ret.push_back(a);
        else resolve_factor_base(a, ret);
        sort(ret.begin(), ret.end());
        return ret;
    }
    int64 resolve_maximum_factor(int64 a) {
        if (test_prime(a)) return a;
        if (a == 1) return 1;
        if (!(a & 1)) {
            while (!(a & 1)) a >>= 1;
            if (a == 1) return 2;
        }
        int64 ret = get_a_factor(a);
        return max(resolve_maximum_factor(a / ret), resolve_maximum_factor(ret));
    }
};

int64 e, N, c, r;

void exgcd(int64 a, int64 b, int64 &x, int64 &y) {
    if (!b) x = 1, y = 0;
    else {
        exgcd(b, a % b, y, x);
        y = (r + y - zymath::qmul(a / b, x, r)) % r;
    }
}

signed main() {
    Pollard_Rho pr;
    cin >> e >> N >> c;
    vector<int64> v = pr.resolve_factor(N);
    assert(v.size() == 2);
    r = (v[0] - 1) * (v[1] - 1);
    int64 d, y;
    exgcd(e, r, d, y);
    cout << d << ' ' << zymath::qpow(c, d, N) << endl;
    return 0;
}