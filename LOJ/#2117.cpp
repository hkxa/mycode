/*************************************
 * @problem:      [HNOI2015]实验比较.
 * @author:       brealid.
 * @time:         2020-11-21.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;
#if false /* 需要使用 fread 优化，改此参数为 true */
#define getchar() (i_P1 == i_P2 && (i_P2 = (i_P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), i_P1 == i_P2) ? EOF : *i_P1++)
char BUF[1 << 21], *i_P1 = BUF, *i_P2 = BUF;
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 100 + 3, P = 1e9 + 7;

struct UnionFindSet {
    int fa[N + 5];
    UnionFindSet() {
        memset(fa, -1, sizeof(fa));
    }
    int find(int u) {
        return fa[u] < 0 ? u : fa[u] = find(fa[u]);
    }
    inline void merge_to(int u, int v) {
        fa[find(u)] = find(v);
    }
} ufs;

int n, m, less_than[N];
bool vis[N];
vector<int> bigger_than[N];
int64 C[N][N];

void init_combination() {
    for (int i = 0; i <= 100; ++i) {
        C[i][0] = 1;
        for (int j = 1; j <= i; ++j)
            C[i][j] = (C[i - 1][j - 1] + C[i - 1][j]) % P;
    }
}

struct dp {
    int64 val[N];
    int cnt;
    void clear(int c) {
        memset(val, 0, sizeof(val));
        cnt = c;
    }
    // '+' 无交换律，左边为含当前节点的，右边为含当前节点的孩子的
    dp operator + (const dp &rhs) const {
        dp result;
        result.clear(cnt + rhs.cnt);
        for (int i = 1; i <= result.cnt; ++i) {
            for (int j = 1; j <= cnt; ++j)
                for (int k = max(1, i - j); k <= rhs.cnt; ++k)
                    result.val[i] += val[j] * rhs.val[k] % P * C[i - 1][j - 1] % P * C[j - 1][k - i + j] % P;
            result.val[i] %= P;
        }
        return result;
    }
    int64 calc() const {
        int64 ret = 0;
        // for (int i = 1; i <= cnt; ++i)
        //     printf("val[%d] = %lld\n", i, val[i]);
        for (int i = 1; i <= cnt; ++i)
            ret += val[i];
        return ret % P;
    }
} f[N];

void dfs(int u) {
    vis[u] = true;
    f[u].clear(1);
    f[u].val[1] = 1;
    for (size_t i = 0; i < bigger_than[u].size(); ++i) {
        int v = bigger_than[u][i];
        if (v == less_than[u]) continue;
        dfs(v);
        f[u] = f[u] + f[v];
    }
}

signed main() {
    init_combination();
    kin >> n >> m;
    char op;
    for (int i = 1, k, x; i <= m; ++i) {
        kin >> k >> op >> x;
        if (op == '=') ufs.merge_to(x, k);
        else {
            k = ufs.find(k);
            x = ufs.find(x);
            less_than[x] = k;
            bigger_than[k].push_back(x);
        }
    }
    dp ans;
    ans.clear(1);
    ans.val[1] = 1;
    for (int i = 1; i <= n; ++i)
        if (i == ufs.find(i) && !less_than[i]) {
            dfs(i);
            ans = ans + f[i];
        }
    for (int i = 1; i <= n; ++i)
        if (i == ufs.find(i) && !vis[i]) {
            kout << "0\n";
            return 0;
        }
    kout << ans.calc() << '\n';
    return 0;
}