//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      数列分块入门 4.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-09.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 988244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

const int N = 50007;

int n, m;
int a[N];
int64 sum[N];
int64 bl[N];
int opt, l, r, c;

#define block(i) ((i) / m)
#define val(i) ((int64)a[i] + bl[block(i)])
#define BlockStart(i) max((i) * m, 1)
#define BlockEnd(i) min((i) * m + m - 1, n)
#define BlockLen(i) (BlockEnd(i) - BlockStart(i) + 1)

signed main() {
    n = read<int>();
    m = sqrt(n);
    for (int i = 1; i <= n; i++) a[i] = read<int>();
    for (int i = 0; i <= block(n); i++)
        for (int j = BlockStart(i); j <= BlockEnd(i); j++)
            sum[i] += a[j];
    for (int i = 1; i <= n; i++) {
        opt = read<int>();
        l = read<int>();
        r = read<int>();
        c = read<int>();
        int b1 = block(l), b2 = block(r);
        if (opt == 0) {
            if (b1 == b2) {
                for (int i = l; i <= r; i++) a[i] += c;
                sum[b1] += (int64)c * (r - l + 1);
            } else {
                for (int i = l; i < (b1 + 1) * m; i++) a[i] += c;
                for (int i = b2 * m; i <= r; i++) a[i] += c;
                sum[b1] += (int64)c * ((b1 + 1) * m - l);
                sum[b2] += (int64)c * (r - b2 * m + 1);
                for (int i = b1 + 1; i < b2; i++) {
                    bl[i] += c;
                    sum[i] += (int64)c * BlockLen(i);
                }
            }
        } else {
            int64 Ret = 0;
            if (b1 == b2) {
                for (int i = l; i <= r; i++) Ret = (Ret + val(i)) % (c + 1);
            } else {
                for (int i = l; i < (b1 + 1) * m; i++) Ret = (Ret + val(i)) % (c + 1);
                for (int i = b2 * m; i <= r; i++) Ret = (Ret + val(i)) % (c + 1);
                for (int i = b1 + 1; i < b2; i++) Ret = (Ret + sum[i]) % (c + 1);
            }
            write(Ret, 10);
        }
    }
    return 0;
}

// Create File Date : 2020-06-09