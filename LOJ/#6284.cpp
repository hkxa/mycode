//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      数列分块入门 8.
 * @user_name:    brealid/hkxadpall/jmoo/jomoo/zhaoyi20/航空信奥/littleTortoise.
 * @time:         2020-06-10.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 10007;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

const int N = 100007;

int n, m;
int a[N];
int same[N];

#define block(v) ((v) / m)
#define beg(v)   max((v) * m, 1)
#define end(v)   min(((v) + 1) * m - 1, n)
#define len(v)   (end(v) - beg(v) + 1)

inline int violentGetSame(int l, int r, int c) {
    if (same[block(l)]) return (same[block(l)] == c) * (r - l + 1);
    int cnt = 0;
    for (int i = l; i <= r; i++) if (a[i] == c) cnt++;
    return cnt;
}

inline void violentPushSame(int l, int r, int c) {
    int b = block(l);
    if (same[b]) {
        for (int i = beg(b); i < l; i++) a[i] = same[b];
        for (int i = r + 1; i <= end(b); i++) a[i] = same[b];
        same[b] = false;
    }
    for (int i = l; i <= r; i++) a[i] = c;
}

inline int blockGetSame(int b, int c) {
    if (same[b]) return (same[b] == c) * len(b);
    else return violentGetSame(beg(b), end(b), c);
}

inline void blockPushSame(int b, int c) {
    same[b] = c;
}

signed main() {
    n = read<int>();
    m = sqrt(n);
    for (int i = 1; i <= n; i++) a[i] = read<int>();
    for (int i = 1, l, r, c, b1, b2; i <= n; i++) {
        l = read<int>();
        r = read<int>();
        c = read<int>();
        b1 = block(l);
        b2 = block(r);
        if (b1 == b2) {
            write(violentGetSame(l, r, c), 10);
            violentPushSame(l, r, c);
        } else {
            int ans = violentGetSame(l, end(b1), c) + violentGetSame(beg(b2), r, c);
            violentPushSame(l, end(b1), c);
            violentPushSame(beg(b2), r, c);
            for (int i = b1 + 1; i < b2; i++) {
                ans += blockGetSame(i, c);
                blockPushSame(i, c);
            }
            write(ans, 10);
        }
    }
    return 0;
}

// Create File Date : 2020-06-10