/*************************************
 * @problem:      「SDOI2019」热闹的聚会与尴尬的聚会.
 * @author:       brealid.
 * @time:         2020-11-19.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#if true
#define getchar() (i_P1 == i_P2 && (i_P2 = (i_P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), i_P1 == i_P2) ? EOF : *i_P1++)
char BUF[1 << 21], *i_P1 = BUF, *i_P2 = BUF;
#endif

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

template <typename T, typename Container = vector<T>, typename Comparer = less<T> >
class heap {
  private:
    priority_queue<T, Container, Comparer> que, del; 
    inline void push_del_tags() {
        while (!del.empty() && !que.empty() && del.top() == que.top())
            del.pop(), que.pop();
    }
  public:
    inline size_t size() {
        return que.size() - del.size();
    }
    inline bool empty() {
        return que.size() == del.size();
    }
    inline T top() {
        push_del_tags();
        return que.top();
    }
    inline void pop() {
        push_del_tags();
        que.pop();
    }
    inline void push(const T &x) { que.push(x); }
    inline void erase(const T &x) { del.push(x); }
};

const int N = 1e4 + 7;

int T, n, m;
unordered_set<int> G[N], now[N];
heap<pair<int, int> > q;

int solve_excited() {
    for (int i = 1; i <= n; ++i) {
        now[i] = G[i];
        q.push(make_pair(-now[i].size(), i));
    }
    int MaxP = -1, MaxTime = -1, TimeNow = 0;
    while (!q.empty()) {
        int now_size = -q.top().first, u = q.top().second;
        q.pop();
        ++TimeNow;
        for (const int &v : now[u]) {
            q.erase(make_pair(-now[v].size(), v));
            now[v].erase(u);
            q.push(make_pair(-now[v].size(), v));
        }
        if (-q.top().first < now_size) {
            q.push(make_pair(19260817, u));
            MaxTime = TimeNow;
            break;
        }
    }
    kout << n - MaxTime + 1 << ' ';
    while (!q.empty()) {
        kout << q.top().second << ' ';
        q.pop();
    }
    kout << '\n';
    return n - MaxTime + 1;
}

typedef unsigned char uint8;
uint8 invite_vis[N];
void invite(int u) {
    invite_vis[u] |= 2;
    for (const int &v : G[u])
        invite_vis[v] |= 1;
}

#define get_col(x) (((x) & 1) ? (((x) & 2) ? 1 : 2) : (rand() % 2 + 1))

void solve_awkward(int minimum) {
    vector<int> v;
    v.resize(n);
    for (int i = 1; i <= n; ++i) v[i - 1] = i;
    for (int x_now = 1; ; ++x_now) {
        memset(invite_vis + 1, 0, sizeof(uint8) * n);
        int invited = 0;
        for (int i = 1; i <= n; ++i)
            if (!invite_vis[v[i - 1]]) invite(v[i - 1]), ++invited;
        if (invited >= minimum) {
            kout << invited << ' ';
            for (int i = 1; i <= n; ++i)
                if (invite_vis[i] & 2) kout << i << ' ';
            kout << '\n';
            return;
        }
        random_shuffle(v.begin(), v.end());
    }
}

void print_vector(const vector<int> &v) {
    kout << v.size() << ' ';
    for (const int &ele : v)
        kout << ele << ' ';
    kout << '\n';
}

signed main() {
    srand(time(0) * 10 + clock());
    for (kin >> T; T--;) {
        kin >> n >> m;
        for (int i = 1; i <= n; ++i) G[i].clear();
        for (int i = 1, u, v; i <= m; ++i) {
            kin >> u >> v;
            G[u].insert(v);
            G[v].insert(u);
        }
        int p = solve_excited();
        solve_awkward(n / p);
    }
    return 0;
}