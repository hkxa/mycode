/*************************************
 * @problem:      换教室.
 * @user_name:    brealid/hkxadpall/zhaoyi20/j(o)moo/littleTortoise.
 * @time:         2020-08-29.
 * @language:     C++.
 * @fastio_ver:   20200827.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        // simple io flags
        ignore_int = 1 << 0,    // input
        char_enter = 1 << 1,    // output
        flush_stdout = 1 << 2,  // output
        flush_stderr = 1 << 3,  // output
        // combination io flags
        endline = char_enter | flush_stdout // output
    };
    enum number_type_flags {
        // simple io flags
        output_double_stable = 1 << 0,  // output
        output_double_faster = 1 << 1   // output
        // combination io flags
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f & ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore character set : ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (double &lf) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            lf = endch & 15;
            while (isdigit(endch = getchar())) lf = lf * 10 + (endch & 15);
            if (endch == '.') {
                double digit = 0.1;
                while (isdigit(endch = getchar())) {
                    lf += (endch & 15) * digit;
                    digit *= 0.1;
                }
            }
            if (flag) lf = -lf;
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            Int d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return d;
        }
    } read;

    struct Writer {
        Writer& operator << (io_flags f) {
            if (f & char_enter) putchar(10);
            if (f & flush_stdout) fflush(stdout);
            if (f & flush_stderr) fflush(stderr);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
        inline void operator () (double val, int eps_digit = 6, char endch = '\0', number_type_flags flg = output_double_faster) {
            if (flg & output_double_stable) {
                static char output_format[10];
                sprintf(output_format, "%%.%dlf", eps_digit);
                printf(output_format, val);
            } else if (flg & output_double_faster) {
                if (val < 0) {
                    putchar('-');
                    val = -val;
                }
                double eps_number = 0.5;
                for (int i = 1; i <= eps_digit; i++) eps_number /= 10;
                val += eps_number;
                (*this) << (int64)val;
                val -= (int64)val;
                putchar('.');
                while (eps_digit--) {
                    val *= 10;
                    putchar((int)val | '0');
                    val -= (int)val;
                }
            } else {
                (*this) << "1.#ERROR_NOT_IDENTIFIED_FLAG";
            }
            if (endch) putchar(endch);
        }
    } write;
}
using namespace Fastio;
namespace File_IO {
    void init_IO() {
        freopen("换教室.in", "r", stdin);
        freopen("换教室.out", "w", stdout);
    }
}

// #define int int64

const int SizeN = 2010, SizeV = 310;
const double INF = 1e11;

int n, m, v, e; // n个时间段，m次更换机会，v间教室，e条道路 

int c[SizeN], d[SizeN]; // c:原教室；d:可以更换的新教室 
double k[SizeN], dk[SizeN]; // k:更换成功的概率 

int dis[SizeV][SizeV]; // dis:教室间路径，Floyed 

enum Classroom_Change_Status { NOT_CHANGED = 0, CHANGED = 1 }; // Classroom_Change_Status 代表是否更换了教室 

double dp[SizeN][SizeN][2], ans;

signed main() {	
    // File_IO::init_IO();
    memset(dis, 0x3f, sizeof(dis));
    for (int i = 1; i <= SizeV; i++) dis[i][i] = 0;
    read >> n >> m >> v >> e;
    for (int32 i = 1; i <= n; i++) read >> c[i];
    for (int32 i = 1; i <= n; i++) read >> d[i];
    for (int32 i = 1; i <= n; i++) {
		read >> k[i];
		dk[i] = 1 - k[i];
	}
    for (int32 i = 1, _a, _b, _v; i <= e; i++) {
        read >> _a >> _b >> _v;
        dis[_a][_b] = min(dis[_a][_b], _v);
        dis[_b][_a] = min(dis[_b][_a], _v);
	}
    
    for (int32 o = 1; o <= v; o++) {
    	for (int32 i = 1; i <= v; i++) if (o != i)  {
			for (int32 j = 1; j <= v; j++) if (o != j && i != j) {
				dis[i][j] = min(dis[i][j], dis[i][o] + dis[o][j]);
			}
		}
	}
    for (int32 i = 1; i <= v; i++) 
		dis[i][i] = dis[i][0] = dis[0][i] = 0;
	// Floyed ----求-出----> 最短路
	
	for (int32 i = 0; i <= n; i++) 
	for (int32 j = 0; j <= n; j++) 
		dp[i][j][CHANGED] = dp[i][j][NOT_CHANGED] = INF;
	dp[1][0][NOT_CHANGED] = dp[1][1][CHANGED] = 0;
	
    int C0, D0, C1, D1, MINi_m;
    double t1, t2, t3, t4, t5;
    for (int32 i = 2; i <= n; i++) {
    	C0 = c[i - 1];
    	D0 = d[i - 1];
    	C1 = c[i];
    	D1 = d[i];
    	MINi_m = min(i, m);
        dp[i][0][NOT_CHANGED] = dp[i - 1][0][NOT_CHANGED] + dis[c[i - 1]][c[i]];
    	for (int j = 1; j <= MINi_m; j++) {
    		t1 = dp[i - 1][j][NOT_CHANGED] + 
				 dis[C0][C1];
    		t2 = dp[i - 1][j][CHANGED] + 
				 dis[C0][C1] * dk[i - 1] + 
				 dis[D0][C1] * k[i - 1];
			// for NOT_CHANGED
			t3 = dp[i][j][CHANGED];
			t4 = dp[i - 1][j - 1][NOT_CHANGED] + 
				 dis[C0][C1] * dk[i] + dis[C0][D1] * k[i];
			t5 = dp[i - 1][j - 1][CHANGED] + 
				 dis[D0][D1] * k[i - 1] * k[i] + 
				 dis[D0][C1] * k[i - 1] * dk[i] + 
				 dis[C0][D1] * dk[i - 1] * k[i] + 
				 dis[C0][C1] * dk[i - 1] * dk[i];
			// for CHANGED
			
    		dp[i][j][NOT_CHANGED] = min(t1, t2);
			dp[i][j][CHANGED] = min(min(t3, t4), t5);
			// min值计算求解 
		}
	}
	//期望DP完成
    
	ans = INF;
	for (int i = 0; i <= m; i++) {
		ans = min(ans, min(dp[n][i][CHANGED], dp[n][i][NOT_CHANGED]));
	}
	//获得结果
    write(ans, 2, '\n');
    return 0;
} 