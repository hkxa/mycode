/*************************************
 * @problem:      「JOISC 2016 Day 3」回转寿司.
 * @user_name:    brealid/hkxadpall/zhaoyi20/j(o)moo/littleTortoise.
 * @time:         2020-09-24.
 * @language:     C++.
 * @fastio_ver:   20200913.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0,    // input
        flush_stdout = 1 << 1,  // output
        flush_stderr = 1 << 2,  // output
    };
    enum number_type_flags {
        output_double_stable = 1 << 0,  // output
        output_double_faster = 1 << 1   // output
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f & ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore character set = {' ', '\r', '\n', '\t'}
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (double &lf) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            lf = endch & 15;
            while (isdigit(endch = getchar())) lf = lf * 10 + (endch & 15);
            if (endch == '.') {
                double digit = 0.1;
                while (isdigit(endch = getchar())) {
                    lf += (endch & 15) * digit;
                    digit *= 0.1;
                }
            }
            if (flag) lf = -lf;
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            Int d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return d;
        }
    } read;

    struct Writer {
        Writer& operator << (io_flags f) {
            if (f & flush_stdout) fflush(stdout);
            if (f & flush_stderr) fflush(stderr);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
        inline void operator () (double val, int eps_digit = 6, char endch = '\0', number_type_flags flg = output_double_faster) {
            if (flg & output_double_stable) {
                static char output_format[10];
                sprintf(output_format, "%%.%dlf", eps_digit);
                printf(output_format, val);
            } else if (flg & output_double_faster) {
                if (val < 0) {
                    putchar('-');
                    val = -val;
                }
                double eps_number = 0.5;
                for (int i = 1; i <= eps_digit; i++) eps_number /= 10;
                val += eps_number;
                (*this) << (int64)val;
                val -= (int64)val;
                if (eps_digit) putchar('.');
                while (eps_digit--) {
                    val *= 10;
                    putchar((int)val | '0');
                    val -= (int)val;
                }
            } else {
               (*this) << '1';
                if (eps_digit) {
                    (*this) << ".E";
                    for (int i = 2; i <= eps_digit; i++) (*this) << '0';
                }
            }
            if (endch) putchar(endch);
        }
    } write;
}
using namespace Fastio;

namespace File_IO {
    void init_IO(const char *file_name) {
        char buff[107];
        sprintf(buff, "%s.in", file_name);
        freopen(buff, "r", stdin);
        sprintf(buff, "%s.out", file_name);
        freopen(buff, "w", stdout);
    }
}

// #define int int64

template<typename ToClearType>
void container_clear(ToClearType &x) {
    ToClearType empty_val;
    swap(x, empty_val);
}

const int N = 4e5 + 7, BN = 650;

int n, q, bsiz, bcnt;
int a[N];
int bid[N], bl[BN], br[BN];
priority_queue<int> ele[BN];
priority_queue<int, vector<int>, greater<int> > mark[BN];

void build_block(int block) {
    if (!ele[block].empty()) container_clear(ele[block]);
    for (int i = bl[block]; i <= br[block]; i++)
        ele[block].push(a[i]);
}

void pushdown_marks(int block) {
    priority_queue<int, vector<int>, greater<int> > &tags = mark[block];
    if (tags.empty()) return;
    for (int i = bl[block]; i <= br[block]; i++) {
        if (a[i] > tags.top()) {
            tags.push(a[i]);
            a[i] = tags.top();
            tags.pop();
        }
    }
    container_clear(tags);
    build_block(block);
}

int bf_query(int l, int r, int A) {
    pushdown_marks(bid[l]);
    for (int i = l; i <= r; i++) 
        if (a[i] > A) swap(a[i], A);
    build_block(bid[l]);
    return A;
}

int query(int l, int r, int A) {
    if (l > r) return query(1, r, query(l, n, A));
    int b1 = bid[l], b2 = bid[r];
    if (b1 == b2) return bf_query(l, r, A);
    A = bf_query(l, br[b1], A);
    for (int i = b1 + 1; i < b2; i++) {
        if (A > ele[i].top()) continue;
        mark[i].push(A);
        ele[i].push(A);
        A = ele[i].top();
        ele[i].pop();
    }
    return bf_query(bl[b2], r, A);
}

signed main() {
    // File_IO::init_IO("「JOISC 2016 Day 3」回转寿司");
    read >> n >> q;
    bsiz = sqrt(n);
    for (int i = 1; i <= n; i++) {
        read >> a[i];
        bid[i] = (i - 1) / bsiz + 1;
    }
    bcnt = bid[n];
    for (int i = 1; i <= bcnt; i++) {
        bl[i] = bsiz * (i - 1) + 1;
        br[i] = min(bsiz * i, n);
        build_block(i);
    }
    for (int i = 1, l, r, A; i <= q; i++) {
        read >> l >> r >> A;
        write << query(l, r, A) << '\n';
    }
    return 0;
}