/*************************************
 * @problem:      「联合省选 2020 A | B」冰火战士.
 * @author:       brealid.
 * @time:         2020-11-23.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#define getchar() (i_P1 == i_P2 && (i_P2 = (i_P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), i_P1 == i_P2) ? EOF : *i_P1++)
char BUF[1 << 21], *i_P1 = BUF, *i_P2 = BUF;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 2e6 + 7;

int q, t[N], x[N], y[N];
int val[N], cnt;
struct BinaryIndexTree {
    int64 tr[N];
    void add(int u, int dif) {
        for (; u <= cnt; u += u & -u) tr[u] += dif;
    }
    int64 qry(int u) {
        int64 ret(0);
        for (; u; u ^= u & -u) ret += tr[u];
        return ret;
    }
} ice, fire;

signed main() {
    kin >> q;
    for (int i = 1; i <= q; ++i) {
        if (kin.get<int>() == 1) {
            kin >> t[i] >> x[i] >> y[i];
            val[++cnt] = x[i];
        } else {
            t[i] = -1;
            kin >> x[i];
        }
    }
    sort(val + 1, val + cnt + 1);
    int64 sum_fire(0);
    for (int i = 1; i <= q; ++i) {
        if (~t[i]) {
            x[i] = lower_bound(val + 1, val + cnt + 1, x[i]) - val;
            if (t[i] == 0) ice.add(x[i], y[i]);
            else fire.add(x[i], y[i]), sum_fire += y[i];
        } else {
            int id = x[i];
            if (t[id] == 0) ice.add(x[id], -y[id]);
            else fire.add(x[id], -y[id]), sum_fire -= y[id];
        }
        int l = 1, r = cnt, divideP;
        int64 ans = 0; int ansp = -1;
        while (l <= r) {
            divideP = (l + r) >> 1;
            int64 lef = ice.qry(divideP), rig = sum_fire - fire.qry(divideP - 1), now = min(lef, rig);
            // printf("divideP %d : ice(left) = %lld, fire(right) = %lld\n", divideP, lef, rig);
            if (now > ans) {
                ans = now;
                ansp = divideP;
            }
            if (lef < rig) l = divideP + 1;
            else r = divideP - 1;
        }
        if (!ans || !~ansp) {
            kout << "Peace\n";
            continue;
        }
        int p = ansp, k = 1;
        while (p <= cnt && k) {
            divideP = p + k;
            int64 lef = ice.qry(divideP), rig = sum_fire - fire.qry(divideP - 1), now = min(lef, rig);
            if (p + k <= cnt && now == ans) p += k, k <<= 1;
            else k >>= 1;
        }
        kout << val[p] << ' ' << (ans << 1) << '\n';
    }
    return 0;
}