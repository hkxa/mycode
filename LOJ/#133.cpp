//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      #133. 二维树状数组 1：单点修改，区间查询.
 * @user_name:    hkxadpall.
 * @time:         2020-06-05.
 * @language:     C++.
 * @upload_place: LibreOJ.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 988244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

const int N = 4096 + 7;
int n, m;
int64 t[N][N];
int op, a, b, c, d, k;

#define lowbit(u) ((u) & (-(u)))

void modify(int x, int y, int k) {
    int _y = y;
    while (x <= n) {
        y = _y;
        while (y <= m) {
            t[x][y] += k;
            y += lowbit(y);
        }
        x += lowbit(x);
    }
}

int64 sum(int x, int y) {
    int64 ret = 0;
    int _y = y;
    while (x) {
        y = _y;
        while (y) {
            ret += t[x][y];
            y -= lowbit(y);
        }
        x -= lowbit(x);
    }
    return ret;
}

int64 query(int a, int b, int c, int d) {
    return sum(c, d) - sum(a - 1, d) - sum(c, b - 1) + sum(a - 1, b - 1);
}

signed main() {
    n = read<int>();
    m = read<int>();
    while (scanf("%d%d%d", &op, &a, &b) != EOF) {
        if (op == 1) {
            k = read<int>();
            modify(a, b, k);
        } else {
            c = read<int>();
            d = read<int>();
            write(query(a, b, c, d), 10);
        }
    }
    return 0;
}

// Create File Date : 2020-06-05