//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      疫情控制.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-20.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 300005;
int n, m, tot, na, nb;
int h[N], ne[N << 1], to[N << 1];
int64 w[N << 1];
int f[N][25], army[N];
int64 dis[N][25];

void add(int x, int y, int z) { to[++tot] = y, ne[tot] = h[x], h[x] = tot, w[tot] = z; }

void dfs(int x, int las, int64 havego) {
    f[x][0] = las, dis[x][0] = havego;
    for (int i = 1; i <= 21; ++i) {
        f[x][i] = f[f[x][i - 1]][i - 1];
        dis[x][i] = dis[x][i - 1] + dis[f[x][i - 1]][i - 1];
    }
    for (int i = h[x]; i != -1; i = ne[i])
        if (to[i] != las)
            dfs(to[i], x, w[i]);
}

struct node {
    int64 rest;
    int id;
} a[N], b[N];

int vis[N], used[N], restbj[N];
int64 restmin[N];

int checkok(int x, int las) {
    int bj = 1, i, bbj = 0;
    if (vis[x])
        return 1;
    for (i = h[x]; i != -1; i = ne[i]) {
        if (to[i] == las)
            continue;
        bbj = 1;
        if (!checkok(to[i], x)) {
            bj = 0;
            if (x == 1) b[++nb].id = to[i], b[nb].rest = w[i];
            else return 0;
        }
    }
    if (!bbj) return 0;
    return bj;
}

bool cmp(node x, node y) { return x.rest > y.rest; }

int check(int64 lim)
{
    int i, j, x, now;
    int64 num;
    na = nb = 0;
    for (i = 1; i <= n; ++i)
        vis[i] = restbj[i] = 0;
    for (i = 1; i <= m; ++i)
        used[i] = 0;
    for (i = 1; i <= m; ++i) {
        x = army[i], num = 0;
        for (j = 21; j >= 0; --j)
            if (f[x][j] > 1 && num + dis[x][j] <= lim)
                num += dis[x][j], x = f[x][j];
        if (f[x][0] == 1 && num + dis[x][0] <= lim) {
            a[++na].rest = lim - num - dis[x][0],
            a[na].id = i;
            if (!restbj[x] || a[na].rest < restmin[x])
                restmin[x] = a[na].rest, restbj[x] = i;
        } else vis[x] = 1;
    }
    if (checkok(1, 0)) return 1;
    sort(a + 1, a + 1 + na, cmp);
    sort(b + 1, b + 1 + nb, cmp);
    now = 1;
    used[0] = 1;
    for (i = 1; i <= nb; ++i) {
        if (!used[restbj[b[i].id]]) {
            used[restbj[b[i].id]] = 1;
            continue;
        }
        while (now <= na && (used[a[now].id] || a[now].rest < b[i].rest))
            ++now;
        if (now > na)
            return 0;
        used[a[now].id] = 1;
    }
    return 1;
}

int main()
{
    int i, x, y, z;
    int64 l = 0, r = 1e15 + 7, mid, ans = -1;
    read >> n;
    for (i = 1; i <= n; ++i)
        h[i] = -1;
    for (i = 1; i < n; ++i) {
        read >> x >> y >> z;
        add(x, y, z), add(y, x, z);
    }
    dfs(1, 0, 0);
    read >> m;
    for (i = 1; i <= m; ++i)
        read >> army[i];
    while (l <= r) {
        mid = (l + r) >> 1;
        if (check(mid))
            r = mid - 1, ans = mid;
        else
            l = mid + 1;
    }
    write << ans << '\n';
    return 0;
}