/*************************************
 * @problem:      贪吃蛇.
 * @user_name:    brealid.
 * @time:         2020-11-09.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 1
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

const int N = 1e6 + 7;

struct snake {
    int strength, id;
    snake() {}
    snake(int S, int I) : strength(S), id(I) {}
    bool operator < (const snake &b) const { return strength ^ b.strength ? strength < b.strength : id < b.id; }
    bool operator > (const snake &b) const { return strength ^ b.strength ? strength > b.strength : id > b.id; }
    // snake operator - (const snake &b) const { return snake(strength - b.strength, id); }
    snake& operator -= (const snake &b) { strength -= b.strength; return *this; }
};

int T, n;
snake s[N], q[N];

int solve() {
    for (int i = n, j = 1, head = 1, tail = 0; ;) {
        if ((i - j + 1) + (tail - head + 1) == 2) return 1;
        snake eater, eatee = s[j++];
        if (head > tail || (i >= j && s[i] > q[head])) eater = s[i--];
        else eater = q[head++];
        eater -= eatee;
        if (i < j || eater < s[j]) {
            // 不能吃
            int remain_snake = (i - j + 1) + (tail - head + 1) + 1;
            int round2_coount = 0;
            while (true) {
                eatee = eater;
                ++round2_coount;
                if ((i - j + 1) + (tail - head + 1) == 1) 
                    return round2_coount & 1 ? remain_snake + 1 : remain_snake;
                if (head > tail || (i >= j && s[i] > q[head])) eater = s[i--];
                else eater = q[head++];
                eater -= eatee;
                if ((i >= j && eater > s[j]) || (head <= tail && eater > q[tail]))
                    return round2_coount & 1 ? remain_snake + 1 : remain_snake;
            }
        }
        q[++tail] = eater;
    }
}

signed main() {
    freopen("snakes.in", "r", stdin);
    freopen("snakes.out", "w", stdout);
    read >> T >> n;
    for (int i = 1; i <= n; ++i) {
        read >> s[i].strength;
        s[i].id = i;
    }
    write << solve() << '\n';
    while (--T) {
        for (int NeedModify = read.get<int>(); NeedModify--;)
            read >> s[read.get<int>()].strength;
        write << solve() << '\n';
    }
    return 0;
}