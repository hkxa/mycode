#include <vector>
typedef long long int64;

template <typename T>
T pow_i(T a, int n, const T &P) {
    T ret = 1;
    while (n) {
        if (n & 1) ret = ret * a % P;
        a = a * a % P;
        n >>= 1;
    }
    return ret;
}

#define inverse_i32(a, P) pow_i<int>(a, P - 2, P)
#define inverse_i64(a, P) pow_i<int64>(a, P - 2, P)

template <int64 P>
struct poly : public std::vector<int64> {
    poly<P> operator + (const poly<P> &other) const {
        int n = size();
        poly<P> res(n);
        for (int i = 0; i < n; ++i) res[i] = ((*this)[i] + other[i]) % P;
        return res;
    }
};

template <int64 P>
int64 FunctionCalc(poly<P> f, int64 x) {
    int n = f.size();
    int64 t = 1, ret = 0;
    for (int i = 0; i < n; ++i) {
        ret = (ret + f[i] * t) % P;
        t = t * x % P;
    }
    return ret;
}

template <int64 P>
int64 Lagrange(poly<P> f, int64 x) {
    int64 ret(0), now;
    for (int i = 0; i < n; ++i) {
        now = 1;
        for (int j = 0; j < n; ++j)
            if (i != j) now = now * (x - j) % P * inverse_i64(i - j, P) % P;
        ret = (ret + now * f[i]) % P;
    }
    return (ret + P) % P;
}