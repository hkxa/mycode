#include "testlib.h"
#include <bits/stdc++.h>
// #include <windows.h>
using namespace std;

int row[4], col[4], dia[2];
int usr[4][4];

bool chk()
{
    int t = 0;
    for (int i = 0; i < 4; i++) {
        t = 0;
        for (int j = 0; j < 4; j++) {
            t += usr[i][j];
        }
        if (t != row[i]) return false;
    }
    for (int j = 0; j < 4; j++) {
        t = 0;
        for (int i = 0; i < 4; i++) {
            t += usr[i][j];
        }
        if (t != col[j]) return false;
    }
    t = 0;
    for (int i = 0; i < 4; i++) {
        t += usr[i][i];
    }
    if (t != dia[0]) return false;
    t = 0;
    for (int i = 0; i < 4; i++) {
        t += usr[i][3 - i];
    }
    if (t != dia[1]) return false;
    return true;
}

char numCh[5] = "1234";

string getWrong()
{
    int t = 0;
    for (int i = 0; i < 4; i++) {
        t = 0;
        for (int j = 0; j < 4; j++) {
            t += usr[i][j];
        }
        if (t != row[i]) return (string)"sum of row " + numCh[i];
    }
    for (int j = 0; j < 4; j++) {
        t = 0;
        for (int i = 0; i < 4; i++) {
            t += usr[i][j];
        }
        if (t != col[j]) return (string)"sum of col " + numCh[j];
    }
    t = 0;
    for (int i = 0; i < 4; i++) {
        t += usr[i][i];
    }
    if (t != dia[0]) return (string)"sum of dia [0, 0] to [3, 3]";
    t = 0;
    for (int i = 0; i < 4; i++) {
        t += usr[i][3 - i];
    }
    if (t != dia[1]) return (string)"sum of dia [0, 3] to [3, 0]";
    return (string)"nothing";
}

int main(int argc, char* argv[]) 
{
    registerTestlibCmd(argc, argv);
    for (int i = 0; i < 4; i++) row[i] = inf.readInt();
    for (int i = 0; i < 4; i++) col[i] = inf.readInt();
    for (int i = 0; i < 2; i++) dia[i] = inf.readInt();
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            usr[i][j] = ouf.readInt();
            if (usr[i][j] < 1 || usr[i][j] > 300) {
                quitf(_wa, "Your output's num should be in range [1, 300].");
            }
        }
    }
    int x, y, n;
    for (int i = 0; i < 4; i++) {
        x = inf.readInt();
        y = inf.readInt();
        n = inf.readInt();
        if (usr[x][y] != n) {
            quitf(_wa, "Your output should contain the value of the given position.");
        }
    }
    if (chk()) quitf(_ok, "The answer is acceptable.");
    else quitf(_wa, "The answer isn't acceptable : %s is wrong.", getWrong().c_str());
    return 0;
}