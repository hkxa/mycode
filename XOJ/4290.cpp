//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      新年好.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-10.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 100003;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

int n, m;
vector<pair<int, int> > G[50007];
int pos[6];
int dis[6][100007];
int ans = 0x3fffffff, cur;

void spfa(int s, int *dis) {
    queue<int> q;
    q.push(s);
    memarr(n, 0x3fffffff, dis);
    dis[s] = 0;
    while (!q.empty()) {
        int f = q.front();
        q.pop();
        for (size_t i = 0; i < G[f].size(); i++) {
            int to = G[f][i].first, v = G[f][i].second;
            if (dis[to] > dis[f] + v) {
                dis[to] = dis[f] + v;
                q.push(to);
            }
        }
    }
}

signed main() {
    n = read<int>();
    m = read<int>();
    pos[0] = 1;
    for (int i = 1; i <= 5; i++) pos[i] = read<int>();
    for (int i = 1, u, v, t; i <= m; i++) {
        u = read<int>();
        v = read<int>();
        t = read<int>();
        G[u].push_back(make_pair(v, t));
        G[v].push_back(make_pair(u, t));
    }
    for (int i = 0; i < 6; i++) spfa(pos[i], dis[i]);
    int p[6] = {0, 1, 2, 3, 4, 5};
    do {
        cur = 0;
        for (int i = 0; i < 5; i++)
            cur += dis[p[i]][pos[p[i + 1]]];
        ans = min(ans, cur);
    } while (next_permutation(p + 1, p + 6));
    write(ans, 10);
    return 0;
}

// Create File Date : 2020-06-10