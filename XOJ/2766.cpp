//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      有根树 Rooted Tree.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-08.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

struct Reader {
    char endch;
    Reader() { endch = '\0'; }
    template <typename Int>
    Reader& operator >> (Int &i) {
        i = baseFastio::read<Int>(endch);
        return *this;
    }
    template <typename Int>
    inline Int get_int() {
        return baseFastio::read<Int>();
    }
    inline char get_nxt() {
        return endch;
    }
} read;

struct Writer {
    Writer& operator << (const char *ch) {
        // char *p = ch;
        while (*ch) putchar(*(ch++));
        return *this;
    }
    Writer& operator << (const char ch) {
        putchar(ch);
        return *this;
    }
    template <typename Int>
    Writer& operator << (const Int i) {
        baseFastio::write(i);
        return *this;
    }
} write;

// #define int int64

int n;
int parent[100007];
vector<int> son[100007];
string ans[100007];

string type(int u) {
    if (!~parent[u]) return "root";
    else if (son[u].size()) return "internal node";
    else return "leaf";
}

string getson(int u) {
    stringstream ret;
    ret << '[';
    for (size_t i = 0; i < son[u].size(); i++) {
        if (i) ret << ", ";
        ret << son[u][i];
    }
    ret << ']';
    return ret.str();
}

void dfs(int u, int dep) {
    stringstream ss;
    ss << "node " << u << ": parent = " << parent[u] << ", depth = " << dep << ", " << type(u) << ", " << getson(u);
    ans[u] = ss.str();
    for (size_t i = 0; i < son[u].size(); i++) {
        dfs(son[u][i], dep + 1);
    }
}

signed main()
{
    memset(parent, -1, sizeof(parent));
    read >> n;
    for (int i = 0; i < n; i++) {
        int id = read.get_int<int>();
        int k = read.get_int<int>();
        while (k--) {
            int s = read.get_int<int>();
            parent[s] = id;
            son[id].push_back(s);
        }
    }
    for (int i = 0; i < n; i++) {
        if (!~parent[i]) {
            dfs(i, 0);
            for (int j = 0; j < n; j++)
                write << ans[j].c_str() << '\n';
            return 0;
        }
    }
    return 0;
}