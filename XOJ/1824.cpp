#include <stdio.h>
#include <algorithm>
#include <string.h>
#include <iostream>
#include <math.h>
#define Char_Int(a) ((a) - '0')
#define Int_Char(a) ((a) + '0')
using namespace std;

inline int readint()       
{
    char c = getchar();
    while (c > '9' || c < '0') c = getchar();
    int init = Char_Int(c);
    while ((c = getchar()) <= '9' && c >= '0') init = (init << 3) + (init << 1) + Char_Int(c);
    return init;
}

struct BCJ {
    int fa[30007];
    int dis[30007];
    int num[30007];
    
    BCJ() {
        for (int i = 1; i < 30001; i++) fa[i] = i, num[i] = 1, dis[i] = 0;
    }
    
    int find(int a) {
        if (fa[a] == a) return a;
		else {
			int fa_a = find(fa[a]);
			dis[a] += dis[fa[a]];
			return fa[a] = fa_a;
		}
    }
    
    inline void merge(int a, int b) {
        int r1 = find(a), r2 = find(b);
		fa[r1] = r2;
        dis[r1] += num[r2];
        num[r2] += num[r1];
		num[r1] = 0;
    }
    
    inline int query(int a, int b) {
        if (find(a) != find(b))
            return -1;
        return abs(dis[a] - dis[b]) - 1;
    }
} bcj; 

int main()
{
    int n = readint();
    char ch; int x, y;
    for (int i = 0; i < n; i++) { 
        ch = getchar();
    	while (ch != 'M' && ch != 'C') ch = getchar();
    	x = readint();
    	y = readint();
    	if (ch == 'M') {
    		bcj.merge(x, y);
    	}
    	else {
    		printf("%d\n", bcj.query(x, y));
    	}
    }
    return 0;
} 