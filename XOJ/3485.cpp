//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Gerald and Giant Chess.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-20.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 1e9 + 7;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? Module(w - P) : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

const int HW = 2e5 + 7, N = 2e3 + 7;

int fac[HW], ifac[HW];

int fpow(int a, int n) {
    int ret = 1;
    while (n) {
        if (n & 1) ret = mul(ret, a);
        a = mul(a, a);
        n >>= 1;
    }
    return ret;
}

void PreC(int n) {
    fac[0] = fac[1] = 1;
    for (int i = 2; i <= n; i++) fac[i] = mul(fac[i - 1], i);
    // for (int i = 1; i <= n; i++) printf("fac[%d] = %d\n", i, fac[i]);
    ifac[n] = fpow(fac[n], P - 2);
    for (int i = n; i >= 1; i--) ifac[i - 1] = mul(ifac[i], i);
}

int C(int n, int m) {
    // printf("C(%d, %d) = %d (%d * %d * %d)\n", n, m, mul(fac[m], ifac[m - n], ifac[n]), fac[m], ifac[m - n], ifac[n]);
    return mul(fac[m], ifac[m - n], ifac[n]);
}

// #define int int64

int h, w, n;
struct pos {
    int x, y;
    bool operator < (const pos &b) const {
        return x ^ b.x ? x < b.x : y < b.y;
    }
} a[N];

int f[N];

signed main() {
    h = read<int>();
    w = read<int>();
    n = read<int>();
    PreC(h + w);
    for (int i = 1; i <= n; i++) {
        a[i].x = read<int>();
        a[i].y = read<int>();
    }
    sort(a + 1, a + n + 1);
    f[0] = P - 1;
    a[0].x = 1;
    a[0].y = 1;
    a[n + 1].x = h;
    a[n + 1].y = w;
    for (int i = 1; i <= n + 1; i++) {
        for (int j = 0; j < i; j++) {
            if (a[j].y <= a[i].y) {
                f[i] = add(f[i], P - mul(f[j], C(a[i].x - a[j].x, a[i].x - a[j].x + a[i].y - a[j].y)));
            }
        }
    }
    write(f[n + 1], 10);
    return 0;
}

// Create File Date : 2020-06-20