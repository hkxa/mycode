//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Accumulation Degree.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-18.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 2e5 + 7;

int T, n;
vector<pair<int, int64> > G[N];
int64 sum[N], ans[N];

void dfs1(int u, int fa) {
    sum[u] = 0;
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i].first; 
        int64 w = G[u][i].second;
        if (v == fa) continue;
        dfs1(v, u);
        if (G[v].size() == 1) sum[u] += w;
        else sum[u] += min(w, sum[v]);
    }
}

void dfs2(int u, int fa) {
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i].first;
        int64 w = G[u][i].second;
        if (v == fa) continue;
        if (G[u].size() == 1) ans[v] = sum[v] + w;
        else ans[v] = sum[v] + min(w, ans[u] - min(w, sum[v]));
        dfs2(v, u);
    }
}

signed main() {
    read >> T;
    while (T--) {
        read >> n;
        for (int i = 1; i <= n; i++) G[i].clear();
        for (int i = 1, u, v; i < n; i++) {
            int64 c;
            read >> u >> v >> c;
            G[u].push_back(make_pair(v, c));
            G[v].push_back(make_pair(u, c));
        }
        if (n == 1) {
            puts("0");
            continue;
        }
        if (n == 2) {
            write << G[1][0].second << '\n';
            continue;
        }
        dfs1(1, 0);
        ans[1] = sum[1];
        dfs2(1, 0);
        int64 answer = 0;
        for (int i = 1; i <= n; i++) answer = max(answer, ans[i]);
        // for (int i = 1; i <= n; i++) printf("%d in_sea %d\n", i, ans[i]);
        write << answer << '\n';
    }
    return 0;
}