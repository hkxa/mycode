#include <stdio.h>
#include <iostream>
#include <string.h> 
 
using namespace std;
int n;

void swap(int &a, int &b)
{
    int t;
    t = a;
    a = b;
    b = t;
}

struct HEAP{
//  public:
    int d[32769];
    int size;
    
    void start()
    {
    	size = 0;
    	memset(d, 0, sizeof(d));
    }
    
    void put(int x)
    {
        int now, next;
        d[++size] = x;
        now = size;
        while (now > 1) {
            next = now >> 1;
            if (d[now] >= d[next]) return ;
            swap(d[now], d[next]);
            now = next;
        }
    }
    
    int get()
    {
        int now, next, res;
        res = d[1];
        d[1] = d[size--];
        now = 1;
        while (now * 2 <= size) {
        	next = now << 1;
        	if (next < size && d[next+1] < d[next]) next++;
        	if (d[now] <= d[next]) return res;
            swap(d[now], d[next]);
            now = next;
        }
        return res;
    }
};

void run()
{
    HEAP heap;
    heap.start();
	int i, x, y, ans = 0;
	cin >> n;
	for (int i = 1; i <= n; i++) {
	    scanf("%d", &x);
		heap.put(x);
	}
	for (int i = 1; i < n; i++) {
		x = heap.get();
		y = heap.get();
		ans += x + y;
		heap.put(x + y);
	}
	printf("%d", ans);
}

/*
input #1:
3
1 2 9
output #1:
15

input #2:
10
3 5 1 7 6 4 2 5 4 1
output #2:
120
*/


int main()
{
    run();
    return 0;
}