/*************************************
 * @problem:      Bugs 公司.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-23.
 * @language:     C++.
 * @fastio_ver:   20200820.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore space, \t, \r, \n
            ch = getchar();
            while (ch != ' ' && ch != '\t' && ch != '\r' && ch != '\n') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            Int flag = 1;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = -1, endch = getchar();
            i = endch & 15;
            while (isdigit(endch = getchar())) i = (i << 3) + (i << 1) + (endch & 15);
            if (flag == -1) i = -i;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            Int flag = 1;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = -1, endch = getchar();
            Int i = endch & 15;
            while (isdigit(endch = getchar())) i = (i << 3) + (i << 1) + (endch & 15);
            if (flag == -1) i = -i;
            return i;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64
// #define int128 int64
// #define int128 __int128

const int N = 200 + 7, Msiz = 20 + 7, BIT_SIZE = 6e4 + 7;

bool M[N][Msiz];
int power3[Msiz], x, m, f, a[N][N * Msiz], at[N], dp[2][BIT_SIZE], tot;

void dfs2(int y, int e, int t) {
    if (y >= m) return (void)(dp[(x + 1) & 1][e] = max(dp[(x + 1) & 1][e], dp[x & 1][f] + t));
    if (f / power3[y] % 3 == 2) {
        if (M[x + 1][y] && M[x + 1][y + 1])
            dfs2(y + 2, e + power3[y] + power3[y + 1], t);
        return;
    } else if (f / power3[y] % 3 == 1) {
        while (f / power3[y] % 3 == 1 && y < m)
            if (M[x + 1][y]) ++y;
            else return;
        dfs2(y, e, t);
        return;
    } else {
        if (M[x + 1][y] && M[x + 1][y + 1] && y + 1 < m)
            if (!(f / power3[y + 1] % 3))
                dfs2(y + 2, e + power3[y] * 2 + power3[y + 1] * 2, t + 1);
        if (M[x + 1][y] && M[x + 1][y + 1] && M[x + 1][y + 2] && y + 2 < m)
            if (!(f / power3[y + 1] % 3) && !(f / power3[y + 2] % 3))
                dfs2(y + 3, e + power3[y] + power3[y + 1] + power3[y + 2], t + 1);
        dfs2(y + 1, e, t), ++tot;
    }
}
void dfs1(int y, int e)
{
    if (y == m)
        return (void)(a[x][++at[x]] = e);
    if (M[x][y] && M[x][y + 1] && y + 1 < m)
    {
        dfs1(y + 2, e + power3[y] + power3[y + 1]),
            dfs1(y + 2, e + power3[y] * 2 + power3[y + 1] * 2);
        if (M[x][y + 2] && y + 2 < m)
            dfs1(y + 3, e + power3[y] + power3[y + 1] + power3[y + 2]);
    }
    dfs1(y + 1, e);
}

int solve() {
    static int n, K, l;
    read >> n >> m >> K;
    memset(M, 1, sizeof(M));
    memset(dp, -2, sizeof(dp));
    memset(at, 0, sizeof(at));
    dp[0][0] = 0;
    for (int i = 1, j, k; i <= K; ++i) {
        read >> j >> k;
        M[j][k - 1] = false;
    }
    for (x = 1; x <= n; ++x) dfs1(0, 0);
    ++at[0];
    for (x = 0; x < n; ++x) {
        for (l = 1; l <= at[x]; ++l) {
            f = a[x][l];
            dfs2(0, 0, 0);
            tot = 0;
        }
        memset(dp[x & 1], -2, sizeof(dp[x & 1]));
    }
    printf("%d\n", dp[n & 1][0]);
    return 0;
}

int main() {
    power3[0] = 1;
    for (int i = 1; i <= 19; ++i)
        power3[i] = power3[i - 1] * 3;
    int data_count;
    read >> data_count;
    while (data_count--) solve();
    return 0;
}
