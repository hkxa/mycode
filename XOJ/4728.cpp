#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

#define TrieNodeMax 1000007 
template <int charSetSize = 26, char base = 'a'>
struct Trie {
    bool isEndStr[TrieNodeMax];
    int son[TrieNodeMax][charSetSize], top;
    int cnt[TrieNodeMax];

    void clear() {
        top = 1;
        memset(son, 0, sizeof(son));
        memset(cnt, 0, sizeof(cnt));
        memset(isEndStr, 0, sizeof(isEndStr));
    }

    Trie() {
        clear();
    }

    bool insert(string &s) {
        int u = 1;
        for (int i = 0; i < s.length(); i++) {
            if (!son[u][s[i] - base]) son[u][s[i] - base] = ++top;
            u = son[u][s[i] - base];
            cnt[u]++;
            // if (i == s.length() - 1) cnt[u]++;
        }
        return 1;
    }

    int find(string &s) {
        int u = 1;
        for (int i = 0; i < s.length(); i++) {
            if (!son[u][s[i] - base]) return 0;
            u = son[u][s[i] - base];
            // printf("Char %c : cnt = %d\n", s[i], cnt[u]);
        }
        return cnt[u];
    }
};

Trie<26, 'a'> t;

void play()
{
    string s;
    while (true) {
        getline(cin, s);
        if (s == "") break;
        t.insert(s);
    }
    while (cin >> s) {
        cout << t.find(s) << '\n';
    }
    return;
}

int main() {
    ios::sync_with_stdio(0);
    play();
}