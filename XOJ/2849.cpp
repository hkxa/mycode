//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Amazing Robots.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-06.
 * @language:     C++.
*************************************/ 
#pragma GCC optimize(2)
#pragma GCC optimize(3)
#pragma GCC optimize("Ofast")
#pragma GCC optimize("inline")
#pragma GCC optimize("inline-functions")
#pragma GCC optimize("no-stack-protector")
#pragma GCC optimize("inline-small-functions")
#pragma GCC optimize("-finline-small-functions")
#pragma GCC optimize("inline-functions-called-once")
#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

struct Reader {
    char endch;
    Reader() { endch = '\0'; }
    template <typename Int>
    Reader& operator >> (Int &i) {
        i = baseFastio::read<Int>(endch);
        return *this;
    }
    template <typename Int>
    inline Int get_int() {
        return baseFastio::read<Int>();
    }
    inline char get_nxt() {
        return endch;
    }
} read;

struct Writer {
    Writer& operator << (const char *ch) {
        // char *p = ch;
        while (*ch) putchar(*(ch++));
        return *this;
    }
    Writer& operator << (const char ch) {
        putchar(ch);
        return *this;
    }
    template <typename Int>
    Writer& operator << (const Int i) {
        baseFastio::write(i);
        return *this;
    }
} write;

// #define int int64

const int dx[4] = {0, 0, 1, -1}, dy[4] = {1, -1, 0, 0};

char getop() {
    static char buffer[3];
    scanf("%s", buffer);
    return buffer[0];
}

struct pos {
    int x, y;
    pos() {}
    pos(int X, int Y) : x(X), y(Y) {}
    operator int() const { 
        return x * 21 + y; 
    }
    bool operator == (const pos b) const { return x == b.x && y == b.y; }
};

struct maze {
    int n, m, q;
    pos st;
    char mp[23][23];
    struct guard {
        int x, y;
        int len, len2;
        int d;
        pos where(int tim) {
            tim %= len2;
            if (tim < len) return pos(x + dx[d] * tim, y + dy[d] * tim);
            else return pos(x + dx[d] * (len2 - tim), y + dy[d] * (len2 - tim));
        }
        void input() {
            read >> x >> y >> len;
            len2 = len << 1;
            switch (getop()) {
                case 'S' : d = 0; break;
                case 'N' : d = 1; break;
                case 'E' : d = 2; break;
                case 'W' : d = 3; break;
            }
        }
    } g[13]; 
    void input() {
        read >> n >> m;
        for (int i = 1; i <= n; i++) {
            scanf("%s", mp[i] + 1);
            for (int j = 1; j <= m; j++) {
                if (mp[i][j] == 'X') {
                    mp[i][j] = '.';
                    st = pos(i, j);
                }
            }
        }
        read >> q;
        for (int i = 1; i <= q; i++) g[i].input();
    }
} a[2];

pos toPos(int status) {
    return pos(status / 21, status % 21);
}

int toInt(const int maze_id, pos s) {
    if (s.x < 1 || s.x > a[maze_id].n || s.y < 1 || s.y > a[maze_id].m) return 1;
    else return s;
}
    
pos move_dir(const int maze_id, pos r, int d) { 
    if (toInt(maze_id, r) == 1) return r;
    else return pos(r.x + dx[d], r.y + dy[d]); 
}

bool check_move(const int maze_id, int tim, pos las, pos now) {
    if (now.x < 1 || now.x > a[maze_id].n || now.y < 1 || now.y > a[maze_id].m) return true;
    for (int i = 1; i <= a[maze_id].q; i++) {
        if (now == a[maze_id].g[i].where(tim + 1)) return false;
        if (now == a[maze_id].g[i].where(tim) && las == a[maze_id].g[i].where(tim + 1)) return false;
    }
    return true;
}

int f[500][500][24];
struct FromEle {
    unsigned x : 12;
    unsigned y : 12;
    unsigned tim : 5;
    unsigned dir : 3;
} from[500][500][24];
pos robota, robotb;
char dir_char[4 + 1] = "EWSN";

void outputWay(FromEle u) {
    if (~f[u.x][u.y][u.tim]) {
        outputWay(from[u.x][u.y][u.tim]);
        write << dir_char[u.dir] << '\n';
    }
}

struct qnode {
    int a, b, c;
    qnode() {}
    qnode(int A, int B, int C) : a(A), b(B), c(C) {}
};

signed main()
{
    a[0].input(); a[0].input();
    memset(f, -1, sizeof(f));
    f[a[0].st][a[0].st][0] = 0;
    queue<qnode> q;
    q.push(qnode(a[0].st, a[0].st, 0));
    pos ta, tb;
    bool moved;
    int fval;
    while (!q.empty()) {
        robota = toPos(q.front().a);
        robotb = toPos(q.front().b);
        fval = f[robota][robotb][q.front().c];
        q.pop();
        for (int op = 3; op >= 0; op--) {
            moved = 0;
            ta = move_dir(0, robota, op);
            if (a[0].mp[ta.x][ta.y] != '#') {
                moved = true;
                if (!check_move(0, fval, robota, ta)) continue;
            } else ta = robota;
            tb = move_dir(1, robotb, op);
            if (a[0].mp[tb.x][tb.y] != '#') {
                moved = true;
                if (!check_move(1, fval, robotb, tb)) continue;
            } else tb = robotb;
            if (!moved) continue;
            int _a = toInt(0, ta), _b = toInt(1, tb), _c = (fval + 1) % 24;
            if (!~f[_a][_b][_c]) {
                f[_a][_b][_c] = fval + 1;
                from[_a][_b][_c] = (FromEle){robota, robotb, fval % 24, op};
                q.push(qnode(toInt(0, ta), toInt(1, tb), (fval + 1) % 24));
                // printf("f[{%d, %d}][{%d, %d}][%d] = %d\n", ta.x, ta.y, tb.x, tb.y, (fval + 1) % 24, fval + 1);
            }
        }
    }
    int ans = -1;
    for (int i = 0; i < 24; i++)
        if (~f[1][1][i] && (ans == -1 || f[1][1][i] < ans))
            ans = f[1][1][i];
    write << ans << '\n';
    if (ans != -1) 
        for (int i = 0; i < 24; i++)
            if (f[1][1][i] == ans) {
                outputWay(from[1][1][i]);
                return 0;
            }
    return 0;
}