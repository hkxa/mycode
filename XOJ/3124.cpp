//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      3124: Meeting Place.
 * @user_name:    zhaoyi20(brealid).
 * @time:         2020-05-26.
 * @language:     C++.
 * @upload_place: XOJ.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, m;
int p[1007];
vector<int> child[1007];
int dep[1007];
int siz[1007], wson[1007];
int beg[1007];

void dfs1(int u) {
    dep[u] = dep[p[u]] + 1;
    siz[u] = 1;
    for (unsigned i = 0; i < child[u].size(); i++) {
        dfs1(child[u][i]);
        siz[u] += siz[child[u][i]];
        if (siz[child[u][i]] > siz[wson[u]]) wson[u] = child[u][i];
    }
}

void dfs2(int u, int top) {
    beg[u] = top;
    if (wson[u]) dfs2(wson[u], top);
    for (unsigned i = 0; i < child[u].size(); i++) {
        if (child[u][i] == wson[u]) continue;
        dfs2(child[u][i], child[u][i]);
    }
}

int LCA(int u, int v) {
    while (beg[u] != beg[v]) {
        if (dep[beg[u]] < dep[beg[v]]) swap(u, v);
        u = p[beg[u]];
    }
    if (dep[u] < dep[v]) swap(u, v);
    return v;
}

int main()
{
    n = read<int>();
    m = read<int>();
    for (int i = 2; i <= n; i++) {
        p[i] = read<int>();
        child[p[i]].push_back(i);
    }
    dfs1(1);
    dfs2(1, 1);
    for (int i = 1, b, j; i <= m; i++) {
        b = read<int>();
        j = read<int>();
        write(LCA(b, j), 10);
    }
    return 0;
}