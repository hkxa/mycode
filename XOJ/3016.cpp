/*************************************
 * @problem:      Bribing FIPA.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-21.
 * @language:     C++.
 * @fastio_ver:   20200820.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore space, \t, \r, \n
            ch = getchar();
            while (ch != ' ' && ch != '\t' && ch != '\r' && ch != '\n') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            Int flag = 1;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = -1, endch = getchar();
            i = endch & 15;
            while (isdigit(endch = getchar())) i = (i << 3) + (i << 1) + (endch & 15);
            if (flag == -1) i = -i;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            Int flag = 1;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = -1, endch = getchar();
            Int i = endch & 15;
            while (isdigit(endch = getchar())) i = (i << 3) + (i << 1) + (endch & 15);
            if (flag == -1) i = -i;
            return i;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64
// #define int128 int64
// #define int128 __int128

const int SIZE = 2000 + 5;

int n, m;
int w[SIZE], fa[SIZE], s[SIZE], f[SIZE][SIZE];

map <string, int> serial;
vector <int> son[SIZE];

inline void DFS(int u) {
    f[u][0] = 0, s[u] = 1;
    for (int i = 0; i < son[u].size(); i++) {
        int v = son[u][i];
        DFS(v);
        s[u] += s[v];
        for (int j = n; j >= 0; j--) {
            for (int k = 0; k <= j; k++) {
                f[u][j] = min(f[u][j], f[u][j - k] + f[v][k]);
            }
        }
    }
    f[u][s[u]] = min(f[u][s[u]], w[u]);
    return;
}   

inline void init() {
    for (int i = 0; i <= n; i++) son[i].clear();
    serial.clear();
    for (int i = 0; i <= n; i++) {
        fa[i] = w[i] = s[i] = 0; 
        for (int j = 0; j <= n; j++) f[i][j] = 0x3f3f3f3f;
    }
}

int main() {
    char in[SIZE];
    while (fgets(in, 222, stdin)) {
        if (in[0] == '#') break;
        sscanf(in, "%d%d", &n, &m);
        init();
        int cnt = 0, ans = 2e9;
        string str, s;
        for (int i = 1; i <= n; i++) {
            cin >> str;
            if (!serial.count(str)) serial[str] = ++cnt;
            scanf("%d", &w[serial[str]]);
            while (getchar() != '\n') {
                cin >> s;
                if (!serial.count(s)) serial[s] = ++cnt;
                son[serial[str]].push_back(serial[s]);
                fa[serial[s]] = 1;
            }
        }
        for (int i = 1; i <= n; i++)
            if (!fa[i]) son[0].push_back(i);
        w[0] = 2e9;
        DFS(0);
        for (int i = m; i <= n; i++) {
            ans = min(ans, f[0][i]);
        }
        write << ans << '\n';
    }
    return 0;
}