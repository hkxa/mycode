//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Folding.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-16.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

char s[100 + 7];
string str;
int n;
int f[100 + 7][100 + 7];
string rec[100 + 7][100 + 7];

bool judge(int l, int r, int L, int R)
{
    int k = L - 1;
    while (true) {
        for (int i = l; i <= r; i++) {
            k++;
            if (s[k] != s[i]) return 0;
        }
        if (k == R) break; 
    }
    return 1;
}

string intToString(int ori)
{
    static char buf[17];
    sprintf(buf, "%d", ori);
    return string(buf);
}

#define lenDiff(n) (n < 10 ? 3 : n < 100 ? 4 : 5)

int main() {
    if (scanf("%s", s + 1) == EOF) return 1;
    n = strlen(s + 1);
    s[0] = ' ';
    str = s;
    memset(f, 0x3f, sizeof(f));
    for (int i = 1; i <= n; i++) {
        f[i][i] = 1;
        rec[i][i] = s[i];
    }
    for (int l = 1; l < n; l++){
        for (int i = 1, j = l + 1; j <= n; i++, j++) {
            f[i][j] = l + 1;
            rec[i][j] = str.substr(i, l + 1);
            for (int k = i; k < j; k++) {
                if (judge(i, k, k + 1, j)) {
                    if (f[i][j] > f[i][k] + lenDiff((l + 1) / (k - i + 1))) {
                        f[i][j] = f[i][k] + lenDiff((l + 1) / (k - i + 1));
                        rec[i][j] = intToString((l + 1) / (k - i + 1)) + '(' + rec[i][k] + ')';
                    }
                }
                if (f[i][j] >= f[i][k] + f[k + 1][j]) {
                    f[i][j] = f[i][k] + f[k + 1][j];
                    rec[i][j] = rec[i][k] + rec[k + 1][j];
                }
            }
        }
    }
    write << rec[1][n].c_str() << '\n';
    return 0;
}