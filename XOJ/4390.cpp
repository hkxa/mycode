//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      逃学的小孩.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-09.
 * @language:     C++.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x) 
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;
int a, b, c; 
int fa[200007], fa_dis[200007];
int64 diamDis[200007], DistToRoot[200007];
bool diamMark[200007];
struct edge {
    int v, cos;
    bool del;
    edge() {}
    edge(int a, int b, bool c = 0) : v(a), cos(b), del(c) {}
};
vector<edge> G[200007];

int dfs(int u, int father)
{
    fa[u] = father;
    int nRet, nRes = u;
    for (vector<edge>::iterator it = G[u].begin(); it != G[u].end(); it++) {
        if (it->v != father) {
            DistToRoot[it->v] = DistToRoot[u] + it->cos;
            fa_dis[it->v] = it->cos;
            nRet = dfs(it->v, u);
            if (DistToRoot[nRet] > DistToRoot[nRes]) nRes = nRet;
        }
    }
    return nRes;
}

int64 getAns(int u, int father)
{
    int64 res = 0;
    for (vector<edge>::iterator it = G[u].begin(); it != G[u].end(); it++) {
        if (it->v != father && !diamMark[it->v]) {
            res = max(res, getAns(it->v, u) + it->cos);
        }
    }
    return res;
}

#define mindis(c, a, b) min(diamDis[c] - diamDis[b], diamDis[a] - diamDis[c])

int main()
{
    n = read<int>(); read<int>();
    for (int i = 1, u, v, t; i < n; i++) {
        u = read<int>();
        v = read<int>();
        t = read<int>();
        G[u].push_back(edge(v, t));
        G[v].push_back(edge(u, t));
    }
    a = dfs(1, 0); 
    fa[a] = DistToRoot[a] = 0;
    b = dfs(a, 0);
    for (int up = b; up; up = fa[up]) {
        diamMark[up] = true;
        diamDis[fa[up]] = diamDis[up] + fa_dis[up];
    }
    int64 ans = 0;
    for (c = b; c; c = fa[c]) ans = max(ans, mindis(c, a, b) + getAns(c, 0));
    write(ans + diamDis[a], 10);
    return 0;
}
