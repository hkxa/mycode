//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Help Yourself.
 * @user_name:    zhaoyi20(brealid).
 * @time:         2020-05-20.
 * @language:     C++.
 * @upload_place: XOJ.
*************************************/ 
#pragma GCC optimize(1)
#pragma GCC optimize(2)
#pragma GCC optimize(3)

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int N = 100007;
int n;
vector<int> G[N];
int buf[N], r;
bool NotAns[N];

int check(int u, int fa, int s)
{
    // printf("check(%d, %d, %d)\n", u, fa, s);
    int l = r + 1;
    int nRet;
    for (unsigned i = 0; i < G[u].size(); i++) {
        int &v = G[u][i];
        if (v != fa) {
            if ((nRet = check(v, u, s)) == -1) {
                // printf("Receive -1!\n");
                return -1;
            }
            if (nRet) {
                // printf("nRet = %d\n", nRet);
                buf[++r] = nRet;
                // printf("CHK(%d, %d, %d) : save %d in %d\n", u, fa, s, nRet, r);
            }
        }
    }
    if (l > r) return 1;
    sort(buf + l, buf + r + 1);
    int rest = 0;
    for (int i = l, j = r; i <= j; i++, j--) {
        if (i == j) {
            if (!rest) rest = buf[i];
            else return -1;
        } else {
            if (buf[i] + buf[j] != s) {
                if (buf[i] + buf[j] > s) {
                    if (!rest) rest = buf[j];
                    else return -1;
                    j--;
                    if (buf[i] + buf[j] != s) return -1;
                } else {
                    if (!rest) rest = buf[i];
                    else return -1;
                    i++;
                    if (buf[i] + buf[j] != s) return -1;
                }
            }
        }
    }
    r = l - 1;
    return (rest + 1) % s;
}

int root = 1, cnt = 0;
int dep[N];

void PreDfs(int u, int fa)
{
    dep[u] = 1;
    for (unsigned i = 0; i < G[u].size(); i++) {
        int &v = G[u][i];
        if (v != fa) {
            PreDfs(v, u);
            dep[u] += dep[v];
        }
    }
}

int test(int s)
{
    int l = 1, r = 0;
    for (unsigned i = 0; i < G[root].size(); i++) {
        int &v = G[root][i];
        if (dep[v] % s) {
            buf[++r] = dep[v] % s;
        }
    }
    sort(buf + l, buf + r + 1);
    int rest = 0;
    // for (int i = l; i <= r; i++) write(buf[i], i == r ? 10 : 32); 
    for (int i = l, j = r; i <= j; i++, j--) {
        if (i == j) {
            if (!rest) rest = buf[i];
            else return -1;
        } else {
            if (buf[i] + buf[j] != s) {
                if (buf[i] + buf[j] > s) {
                    if (!rest) rest = buf[j];
                    else return -1;
                    j--;
                    if (buf[i] + buf[j] != s) return -1;
                } else {
                    if (!rest) rest = buf[i];
                    else return -1;
                    i++;
                    if (buf[i] + buf[j] != s) return -1;
                }
            }
        }
    }
    return rest;
}

int main() {
    n = read<int>();
    for (int i = 1, u, v; i < n; i++) {
        u = read<int>();
        v = read<int>();
        G[u].push_back(v);
        G[v].push_back(u);
    }
    for (int i = 1; i <= n; i++) {
        if (G[i].size() > G[root].size()) {
            root = i;
        } 
        if (G[i].size() > 2) cnt++;
    }
    if (cnt == 1) {
        PreDfs(root, 0);
        putchar(49);
        for (register int i = 2; i < n; i++) {
            if ((n - 1) % i || NotAns[i]) putchar(48);
            else if (!test(i)) putchar(49);
            else {
                putchar(48);
                for (int j = i; j * j <= n; j += i) {
                    NotAns[j] = 1;
                }
            }
        }
        putchar(10);
        return 0;
    }
    putchar(49);
    for (register int i = 2; i < n; i++) {
        r = 0;
        if ((n - 1) % i == 0 && check(root, 0, i) == 1) putchar(49);
        else putchar(48);
    }
    putchar(10);
    return 0;
}

/*
13 1 2 2 3 2 4 4 5 2 6 6 7 6 8 8 9 9 10 8 11 11 12 12 13 : 111000000000
13 1 2 2 3 2 4 4 5 2 6 2 7 6 8 10 9 3 10 8 11 11 12 12 13 : 111000000000
13 1 5 2 3 2 4 4 5 2 6 2 7 6 8 10 9 3 10 8 11 11 12 12 13 : 111101000000
13 1 2 2 3 3 4 4 5 5 6 6 7 7 8 8 9 9 10 10 11 11 12 12 13 : 111101000001
*/