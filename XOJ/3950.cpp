//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      3950: Subway tree systems.
 * @user_name:    brealid.
 * @time:         2020-08-05.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

// #define int int64

const int N = 3000 + 7;

int T, n;
char s1[N], s2[N];

string expand(char *p, char *ed) {
    string ret = "(";
    vector<string> pe;
    while (*p == '0') {
        pe.push_back(expand(p + 1, ed));
        p += pe.back().size();
    }
    sort(pe.begin(), pe.end());
    for (size_t i = 0; i < pe.size(); i++)
        ret += pe[i];
    return ret + ')';
}

signed main() {
    T = read<int>();
    while (T--) {
        scanf("%s%s", s1, s2);
        n = strlen(s1) >> 1;
        puts(expand(s1, s1 + n) == expand(s2, s2 + n) ? "same" : "different");
    }
    return 0;
}

// Create File Date : 2020-06-04