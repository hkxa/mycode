/*************************************
 * @problem:      Swapity Swapity Swap.
 * @user_name:    zhaoyi20,brealid.
 * @time:         2020-05-18.
 * @language:     C++.
 * @upload_place: XOJ.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, k, m;
int A[107], B[107];

struct mat {
    int a[100007];
    mat(int num = 0) {
        memset(a, 0, sizeof(a));
        if (num) for (int i = 1; i <= n; i++) a[i] = i;
    }
    mat operator * (const mat b) {
        mat res;
        for (int i = 1; i <= n; i++) 
            res.a[i] = b.a[a[i]];
        return res;
    }
} T, now, ans;

mat kpow(mat a, int n) {
    mat ret(1);
    while (n) {
        if (n & 1) ret = ret * a;
        a = a * a;
        n >>= 1;
    }
    return ret;
}

int main()
{
    n = read<int>();
    m = read<int>();
    k = read<int>();
    now = mat(1);
    for (int i = 1; i <= m; i++) {
        A[i] = read<int>();
        B[i] = read<int>();
    }
    for (int i = m; i >= 1; i--) {
        // printf("%d~%d\n", A[i], B[i]);
        for (int j = 1; j < A[i]; j++) T.a[j] = j;
        for (int j = A[i]; j <= B[i]; j++) {
            T.a[j] = B[i] - (j - A[i]);
            // printf("set %d to %d\n", j, B[i] - (j - A[i]));
        }
        for (int j = B[i] + 1; j <= n; j++) T.a[j] = j;
        now = now * T;
        // for (int j = 1; j <= n; j++) 
        //     write(T.a[j], j == n ? 10 : 32);
        // for (int j = 1; j <= n; j++) 
        //     write(now.a[j], j == n ? 10 : 32);
    }
    ans = kpow(now, k);
    for (int i = 1; i <= n; i++) 
        write(ans.a[i], 10);
    return 0;
}