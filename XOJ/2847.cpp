//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      八数码问题.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-06.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

struct Reader {
    char endch;
    Reader() { endch = '\0'; }
    template <typename Int>
    Reader& operator >> (Int &i) {
        i = baseFastio::read<Int>(endch);
        return *this;
    }
    template <typename Int>
    inline Int get_int() {
        return baseFastio::read<Int>();
    }
    inline char get_nxt() {
        return endch;
    }
} read;

struct Writer {
    Writer& operator << (const char *ch) {
        // char *p = ch;
        while (*ch) putchar(*(ch++));
        return *this;
    }
    Writer& operator << (const char ch) {
        putchar(ch);
        return *this;
    }
    template <typename Int>
    Writer& operator << (const Int i) {
        baseFastio::write(i);
        return *this;
    }
} write;

// #define int int64

const int dx[4] = {-1,  0, 0, 1}, dy[4] = { 0, -1, 1, 0};

map<int64, int64> m;
int64 x, tmp_div;
queue<int64> q;
int t[3][3];
int sx, sy;
int tx, ty;
int64 tmp;

int main()
{
    read >> x;
    q.push(x);
    m[x] = 0;
    while(!q.empty()) {
        x = q.front();
        q.pop();
        if (x == 123804765LL) {
        	break;	
		}
		tmp_div = x;
        for (int i = 2; i >= 0; i--) {
            for (int j = 2; j >= 0; j--) {
                t[i][j] = tmp_div % 10;
				tmp_div /= 10;
                if (t[i][j] == 0) {
                	sx = i;
                	sy = j;
				}
            }
        }
        for (int i = 0; i < 4; i++) {
            tx = sx + dx[i];
			ty = sy + dy[i];
			tmp = 0;
            if (tx < 0 || ty < 0 || tx > 2 || ty > 2) {
				continue; 
			}
            t[sx][sy] = t[tx][ty];
            t[tx][ty] = 0;
            for (int i = 0; i <= 2; i++) {
            	for (int j = 0; j <= 2; j++) {
            		tmp = (((tmp << 2) + tmp) << 1) + t[i][j];
				}
			}
            if (!m.count(tmp)) {
                m[tmp] = m[x] + 1;
                q.push(tmp);
            }
            t[tx][ty] = t[sx][sy];
            t[sx][sy] = 0;
        }
    }
    write << m[123804765] << '\n';
    return 0;
}
