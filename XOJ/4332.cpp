//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      相框.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-30.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

struct Reader {
    char endch;
    Reader() { endch = '\0'; }
    template <typename Int>
    Reader& operator >> (Int &i) {
        i = baseFastio::read<Int>(endch);
        return *this;
    }
    template <typename Int>
    inline Int get_int() {
        return baseFastio::read<Int>();
    }
    inline char get_nxt() {
        return endch;
    }
} read;

struct Writer {
    Writer& operator << (const char *ch) {
        // char *p = ch;
        while (*ch) putchar(*(ch++));
        return *this;
    }
    Writer& operator << (const char ch) {
        putchar(ch);
        return *this;
    }
    template <typename Int>
    Writer& operator << (const Int i) {
        baseFastio::write(i);
        return *this;
    }
} write;

// #define int int64

const int N = 1.1e5 + 7, M = 1e5 + 7;

int n, m;

int to[M], nxt[M], head[N], cnt;
int vis[N];
int deg[N];
int cnt_cut, cnt_link;
int ans_cut, ans_link;

void dfs(int u) {
    vis[u] = true;
    if (deg[u] > 2) cnt_cut++;
    if (deg[u] & 1) cnt_link++;
    for (int e = head[u]; ~e; e = nxt[e])
        if (!vis[to[e]]) dfs(to[e]);
}

signed main()
{
    memset(head, -1, sizeof(head));
    read >> n >> m;
    for (int i = 1, u, v; i <= m; i++) {
        read >> u >> v;
        if (!u) u = ++n;
        if (!v) v = ++n;
        to[++cnt] = v;
        nxt[cnt] = head[u];
        head[u] = cnt;
        to[++cnt] = u;
        nxt[cnt] = head[v];
        head[v] = cnt;
        deg[u]++; 
        deg[v]++;
    }
    for (int i = 1; i <= n; i++) {
        if (!deg[i]) continue;
        if (!vis[i]) {
            cnt_cut = 0;
            cnt_link = 0;
            dfs(i);
            if (!cnt_link) {
                if (!cnt_cut) cnt_cut = 1;
                cnt_link = 2;
            }
            ans_cut += cnt_cut;
            ans_link += cnt_link;
        }
    }
    write << ans_cut + (ans_link >> 1) << '\n';
    return 0;
}