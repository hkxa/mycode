/*************************************
 * @problem:      沙拉公主的困惑.
 * @user_name:    brealid.
 * @time:         2020-11-05.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if true
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

const int N = 1e7 + 7;

int T, R, lim;
int inv[N], fac[N], ifac[N];
bool isnp[N];
int prime[N], pcnt;
int pmul[N];

int exgcd(int a, int b, int64 &x, int64 &y) { // ax + by = gcd(a, b)
    if (b == 0) {
        x = 1;
        y = 0;
        return a;
    }
    int res = exgcd(b, a % b, x, y);
    int64 t = y;
    y = x - a / b * y;
    x = t;
    return res;
}

int get_inv(int a, int P) {
    int64 x, y;
    exgcd(a, P, x, y);
    return (x % P + P) % P;
}

void init() {
    fac[0] = ifac[0] = 1;
    inv[1] = 1;
    int lim = min(N, R);
    for (int i = 1; i < lim; ++i) {
        if (i > 1) inv[i] = R - ((int64)R / i * inv[R % i]) % R;
        fac[i] = (int64)fac[i - 1] * i % R;
    }
    ifac[lim - 1] = get_inv(fac[lim - 1], R);
    for (int i = lim - 1; i >= 2; --i) ifac[i - 1] = (int64)ifac[i] * i % R;
    pmul[0] = 1;
    pcnt = 0;
    for (int i = 2; i < N; ++i)
        if (!isnp[i]) {
            prime[++pcnt] = i;
            for (int j = i + i; j < N; j += i)
                isnp[j] = true;
        }
    for (int i = 1; i <= pcnt; ++i)
        pmul[i] = (int64)pmul[i - 1] * (prime[i] - 1) % R * inv[prime[i]] % R;
}

signed main() {
    read >> T >> R;
    init();
    for (int i = 1, n, m; i <= T; ++i) {
        read >> n >> m;
        write << (int64)fac[n] * pmul[upper_bound(prime + 1, prime + pcnt + 1, m) - prime - 1] % R << '\n';
    }
    return 0;
}