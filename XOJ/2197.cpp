//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      2197: 邮局问题.
 * @user_name:    zhaoyi20(brealid).
 * @time:         2020-05-28.
 * @language:     C++.
 * @upload_place: XOJ.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// #define int int64
const int N = 303, M = 33;
int n, m, a[N];
int cost[N][N];
int f[N][M][N], ans = 0x3f3f3f3f; // f[点编号][邮局编号][上一个邮局位置]

signed main()
{
    n = read<int>();
    m = read<int>();
    for (int i = 1; i <= n; i++) a[i] = read<int>();
    sort(a + 1, a + n + 1);
    memset(cost, 0x3f, sizeof(cost));
    for (int i = 1; i <= n; i++) {
        for (int j = i; j <= n; j++) {
            cost[i][j] = 0;
            for (int k = i + 1; k < j; k++) {
                cost[i][j] += min(a[k] - a[i], a[j] - a[k]);
            }
            // printf("cost[%d][%d] = %d\n", i, j, cost[i][j]);
        }
    }
    for (int i = 1; i <= n; i++) {
        cost[0][i] = 0;
        for (int k = 1; k < i; k++) {
            cost[0][i] += a[i] - a[k];
        }
        cost[i][n + 1] = 0;
        for (int k = i + 1; k <= n; k++) {
            cost[i][n + 1] += a[k] - a[i];
        }
        // printf("cost[%d][%d] = %d\n", 0, i, cost[0][i]);
        // printf("cost[%d][%d] = %d\n", i, n + 1, cost[i][n + 1]);
    }
    memset(f, 0x3f, sizeof(f));
    f[0][0][0] = 0;
    for (int i = 1; i <= n; i++) {
        f[i][0][0] = 0;
        for (int j = 1; j <= m; j++) {
            for (int k = 0; k <= i; k++) {
                f[i][j][k] = min(f[i][j][k], f[i - 1][j][k]);
                f[i][j][i] = min(f[i][j][i], f[i - 1][j - 1][k] + cost[k][i]);
                // printf("%d    %d\n", f[i - 1][j][k], f[i - 1][j - 1][k] + cost[k][i]);
                // printf("f[%d][%d][%d] = %d\n", i, j, k, f[i][j][k]);
                // if (f[i][j][k] != 0x3f3f3f3f) printf("f[%d][%d][%d] = %d\n", i, j, k, f[i][j][k]);
            }
        }
    }
    for (int i = 1; i <= n; i++) ans = min(ans, f[n][m][i] + cost[i][n + 1]);
    write(ans, 10);
    return 0;
}