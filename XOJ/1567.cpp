//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      虫食算.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-06.
 * @language:     C++.
*************************************/ 

/**
 * line 127~132 的剪枝是关键(82->100) : 每一列最多进一，最少进零
 */

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

struct Reader {
    char endch;
    Reader() { endch = '\0'; }
    template <typename Int>
    Reader& operator >> (Int &i) {
        i = baseFastio::read<Int>(endch);
        return *this;
    }
    template <typename Int>
    inline Int get_int() {
        return baseFastio::read<Int>();
    }
    inline char get_nxt() {
        return endch;
    }
} read;

struct Writer {
    Writer& operator << (const char *ch) {
        // char *p = ch;
        while (*ch) putchar(*(ch++));
        return *this;
    }
    Writer& operator << (const char ch) {
        putchar(ch);
        return *this;
    }
    template <typename Int>
    Writer& operator << (const Int i) {
        baseFastio::write(i);
        return *this;
    }
} write;

// #define int int64

int n;
char a[3][26];
bool vis[26], used[26];
int num[26];
int order[26];

void dfs(int id) {
    if (id >= n) {
        for (int i = 0; i < n; i++)
            write << num[i] << " \n"[i == n - 1];
        exit(0);
    }
    if (vis[a[0][0]] && vis[a[1][0]] && vis[a[0][0]] + vis[a[1][0]] >= n) return;
    int u = order[id];
    for (int i = 0; i < n; i++) {
        if (used[i]) continue;
        used[i] = true;
        num[u] = i;
        vis[u] = true;
        for (int j = n - 1, up = 0; j >= 0; j--) {
            if (vis[a[0][j]] && vis[a[1][j]] && vis[a[2][j]]) {
                if ((num[a[0][j]] + num[a[1][j]] + up) % n != num[a[2][j]]) 
                    goto finish_dfs_for;
                up = ((num[a[0][j]] + num[a[1][j]] + up) >= n);
                if (!j && up) goto finish_dfs_for;
            } else {
                while (j >= 0) {
                    if (vis[a[0][j]] && vis[a[1][j]] && vis[a[2][j]] &&
                        (num[a[0][j]] + num[a[1][j]]) % n != num[a[2][j]] && 
                        (num[a[0][j]] + num[a[1][j]] + 1) % n != num[a[2][j]]) goto finish_dfs_for;
                    j--;
                }
            }
        }
        dfs(id + 1);
        finish_dfs_for:;
        used[i] = false;
    }
    num[u] = 0;
    vis[u] = false;
}

signed main()
{
    read >> n;
    scanf("%s%s%s", a[0], a[1], a[2]);
    for (int i = 0; i < n; i++) {
        a[0][i] -= 'A';
        a[1][i] -= 'A';
        a[2][i] -= 'A';
    }
    for (int i = n - 1, cnt = 0; i >= 0; i--) {
        if (!used[a[0][i]]) {
            order[cnt++] = a[0][i];
            used[a[0][i]] = true;
        }
        if (!used[a[1][i]]) {
            order[cnt++] = a[1][i];
            used[a[1][i]] = true;
        }
        if (!used[a[2][i]]) {
            order[cnt++] = a[2][i];
            used[a[2][i]] = true;
        }
    }
    memset(used, 0, sizeof(used));
    dfs(0);
    return 0;
}