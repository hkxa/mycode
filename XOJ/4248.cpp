//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      4248: 背单词.
 * @user_name:    brealid.
 * @time:         2020-06-04.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 988244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

int n;
char s[510000 + 7]; int len;
int ch[510000 + 7][26], TrieNodeCount = 1;
bool tag[510000 + 7];
int tot[510000 + 7];
vector<int> G[510000 + 7]; 

int fa[510000 + 7];
int find(int x) { return x == fa[x] ? x : fa[x] = find(fa[x]); }

bool cmp(int u, int v) { return tot[u] < tot[v]; }

void trie_insert() {
    len = strlen(s);
    int u = 1;
    for (int i = len - 1; i >= 0; i--) {
        int &v = ch[u][s[i] - 'a'];
        if (!v) v = ++TrieNodeCount;
        u = v;
    }
    tag[u] = true;
}

void Pretreat1(int u) {
    for (int i = 0; i < 26; i++)
        if (ch[u][i]) {
            if (!tag[ch[u][i]]) fa[ch[u][i]] = find(u);
            else G[find(u)].push_back(ch[u][i]);
            Pretreat1(ch[u][i]);
        }
}

void Pretreat2(int u) {
    tot[u] = 1;
    for (size_t i = 0; i < G[u].size(); i++) {
        Pretreat2(G[u][i]);
        tot[u] += tot[G[u][i]];
    }
    sort(G[u].begin(), G[u].end(), cmp);
}

int dft = 0; int64 Ans = 0;
void getAnswer(int u) {
    int dfn = dft++;
    for (size_t i = 0; i < G[u].size(); i++) {
        Ans += dft - dfn;
        getAnswer(G[u][i]);
    }
}

signed main() {
    n = read<int>();
    for (int i = 1; i <= 510000; i++) fa[i] = i;
    for (int i = 1; i <= n; i++) {
        scanf("%s", s);
        trie_insert();
    }
    Pretreat1(1);
    Pretreat2(1);
    getAnswer(1);
    write(Ans, 10);
    return 0;
}

// Create File Date : 2020-06-04

/*
6
a
ba
aa
ca
nba
cba
[O] 10
6
a
cba
nba
fga
hga
yhga
[O] 12
5
a
zta
yta
zra
yra
[O] 11
6
a
zta
yta
zra
yra
ra
[O] 10
*/