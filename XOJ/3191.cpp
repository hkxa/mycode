//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      数列分块入门 6.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-10.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 988244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

const int N = 200007, SqrtN = 707;

int n, m, q;
int t[N];
vector<int> a[SqrtN];
int opt, l, r, c;

inline void Flatten() {
    int i = 0, cnt = 0;
    while (!a[i].empty()) {
        for (int j = 0; j < a[i].size(); j++) t[++cnt] = a[i][j];
        i++;
    }
}

inline void Build() {
    m = sqrt(n);
    for (int i = 0; i <= (n / m); i++) {
        a[i].clear();
        for (int j = max(i * m, 1); j < min((i + 1) * m, n + 1); j++)
            a[i].push_back(t[j]);
    }
}

inline void Rebuild() {
    Flatten();
    Build();
}

inline bool Insert(size_t pos, int val) {
    size_t i = 0, siz = 0;
    while (siz + a[i].size() < pos) siz += a[i++].size();
    // printf("[Before Insert] Block %d : { %d", i, a[i][0]);
    // for (int j = 1; j < a[i].size(); j++) printf(", %d", a[i][j]);
    // printf(" }\n");
    a[i].insert(a[i].begin() + (pos - siz - 1), val);
    n++;
    // printf("[After Insert] Block %d : { %d", i, a[i][0]);
    // for (int j = 1; j < a[i].size(); j++) printf(", %d", a[i][j]);
    // printf(" }\n");
    return a[i].size() > 2 * m;
}

inline int Query(size_t pos) {
    size_t i = 0, siz = 0;
    while (siz + a[i].size() < pos) siz += a[i++].size();
    return a[i][pos - siz - 1];
}

signed main() {
    q = n = read<int>();
    for (int i = 1; i <= n; i++) t[i] = read<int>();
    Build();
    for (int i = 1; i <= q; i++) {
        opt = read<int>();
        l = read<int>();
        r = read<int>();
        c = read<int>();
        if (!opt) {
            if (Insert(l, r)) Rebuild();
        } else {
            write(Query(r), 10);
        }
    }
    return 0;
}

// Create File Date : 2020-06-10