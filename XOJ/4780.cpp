/*************************************
 * @problem:      涂抹果酱.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-21.
 * @language:     C++.
 * @fastio_ver:   20200820.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore space, \t, \r, \n
            ch = getchar();
            while (ch != ' ' && ch != '\t' && ch != '\r' && ch != '\n') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            Int flag = 1;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = -1, endch = getchar();
            i = endch & 15;
            while (isdigit(endch = getchar())) i = (i << 3) + (i << 1) + (endch & 15);
            if (flag == -1) i = -i;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            Int flag = 1;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = -1, endch = getchar();
            Int i = endch & 15;
            while (isdigit(endch = getchar())) i = (i << 3) + (i << 1) + (endch & 15);
            if (flag == -1) i = -i;
            return i;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64
// #define int128 int64
// #define int128 __int128

const int P = 1e6;

int n, m;
int k, initial;
int ok_status[48], id[1 << 10], cnt;
bool turn[48][48];

void init_ok_status() {
    int mask = 1 << (m << 1);
    for (int bit = 0; bit < mask; bit++) {
        bool fail = false;
        for (int i = 0; i < m && !fail; i++) 
            if (!((bit >> (i << 1)) & 3)) fail = true;
        for (int i = 1; i < m && !fail; i++) 
            if (((bit >> ((i - 1) << 1)) & 3) == ((bit >> (i << 1)) & 3)) fail = true;
        if (!fail) {
            id[bit] = cnt;
            ok_status[cnt++] = bit;
        } else {
            id[bit] = -1;
        }
    }
    for (int i = 0; i < cnt; i++)
        for (int j = 0; j < cnt; j++) {
            turn[i][j] = true;
            for (int p = 0; p < m && turn[i][j]; p++)
                if (((ok_status[i] >> (p << 1)) & 3) == ((ok_status[j] >> (p << 1)) & 3)) 
                    turn[i][j] = false;
        }
    // fprintf(stderr, "m = %d | cnt = %d\n", m, cnt);
}

int f[10000][48];

void dp() {
    f[0][id[initial]] = 1;
    for (int i = 1; i < n; i++) 
        for (int j = 0; j < cnt; j++)
            for (int p = 0; p < cnt; p++)
                if (turn[j][p]) f[i][j] = (f[i][j] + f[i - 1][p]) % P;
}

int64 get_ans(int ln) {
    int64 res = 0;
    for (int i = 0; i < cnt; i++) res = (res + f[ln][i]) % P;
    return res;
}

signed main() {
    read >> n >> m >> k;
    for (int i = 0; i < m; i++)
        initial |= (read.get<int>() << (i << 1));
    init_ok_status();
    if (!~id[initial]) {
        puts("0");
        return 0;
    }
    dp();
    write << get_ans(k - 1) * get_ans(n - k) % P << '\n';
    return 0;
}