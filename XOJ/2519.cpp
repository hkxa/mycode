/*************************************
 * @problem:      Fence Obstacle Course 栅栏行动.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-25.
 * @language:     C++.
 * @fastio_ver:   20200820.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore space, \t, \r, \n
            ch = getchar();
            while (ch != ' ' && ch != '\t' && ch != '\r' && ch != '\n') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            Int flag = 1;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = -1, endch = getchar();
            i = endch & 15;
            while (isdigit(endch = getchar())) i = (i << 3) + (i << 1) + (endch & 15);
            if (flag == -1) i = -i;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            Int flag = 1;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = -1, endch = getchar();
            Int i = endch & 15;
            while (isdigit(endch = getchar())) i = (i << 3) + (i << 1) + (endch & 15);
            if (flag == -1) i = -i;
            return i;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64
// #define int128 int64
// #define int128 __int128

const int N = 200000 + 7;

struct SegmentTree {
    int tr[N << 2], tag[N << 2];
    inline void pushdown(int u) {
        if (tag[u]) {
            tr[u << 1] = tag[u << 1] = tag[u];
            tr[u << 1 | 1] = tag[u << 1 | 1] = tag[u];
            tag[u] = false;
        }
    }
    void modify(int u, int l, int r, int ml, int mr, int xval) {
        if (l >= ml && r <= mr) {
            tr[u] = tag[u] = xval;
            return;
        }
        pushdown(u);
        int mid = (l + r) >> 1;
        if (ml <= mid) modify(u << 1, l, mid, ml, mr, xval);
        if (mr > mid) modify(u << 1 | 1, mid + 1, r, ml, mr, xval);
    }
    int query(int u, int l, int r, int xpos) {
        if (l == r) return tr[u];
        pushdown(u);
        int mid = (l + r) >> 1;
        if (xpos <= mid) return query(u << 1, l, mid, xpos);
        else return query(u << 1 | 1, mid + 1, r, xpos);
    }
    inline void modify(int l, int r, int xval) {
        modify(1, 0, 200000, l, r, xval);
    }
    inline int query(int xpos) {
        return query(1, 0, 200000, xpos);
    }
} t;

int n, s;
int l[N], r[N];
int pre[N][2], f[N][2];

signed main() {
    read >> n >> s;
    s += 100000;
    for (int i = 1; i <= n; i++) {
        read >> l[i] >> r[i];
        l[i] += 100000;
        r[i] += 100000;
        pre[i][0] = t.query(l[i]);
        pre[i][1] = t.query(r[i]);
        t.modify(l[i], r[i], i);
    }
    for (int i = 0; i <= n; i++) f[i][0] = f[i][1] = 0x3fffffff;
    int toppest = t.query(s);
    l[0] = r[0] = 100000;
    f[toppest][0] = abs(s - l[toppest]), f[toppest][1] = abs(s - r[toppest]); 
    for (int i = toppest; i >= 1; i--) {
        // 到哪去的 dp
        f[pre[i][0]][0] = min(f[pre[i][0]][0], f[i][0] + abs(l[i] - l[pre[i][0]]));
        f[pre[i][0]][1] = min(f[pre[i][0]][1], f[i][0] + abs(l[i] - r[pre[i][0]]));
        f[pre[i][1]][0] = min(f[pre[i][1]][0], f[i][1] + abs(r[i] - l[pre[i][1]]));
        f[pre[i][1]][1] = min(f[pre[i][1]][1], f[i][1] + abs(r[i] - r[pre[i][1]]));
    }
    write << f[0][0] << '\n';
    return 0;
}
