//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      3737: 0103 最短Hamilton路径.
 * @user_id:      1677.
 * @user_name:    zhaoyi20(brealid).
 * @time:         2020-06-02.
 * @language:     C++.
 * @upload_place: XOJ.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// #define int int64

int n, a[20][20];
int f[20][1 << 20];
int way[1 << 20];

int Stable[16] = { 0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4 };
int popcountS(int a) { return Stable[a >> 4] + Stable[a & 15]; }
int popcount(int a) { return popcountS(a >> 16) + popcountS((a >> 4) & 255) + Stable[a & 15]; }
bool PopcountComp(int a, int b) { return popcount(a) < popcount(b); }

signed main() {
    n = read<int>();
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            a[i][j] = read<int>();
    memset(f, 0x3f, sizeof(f));
    f[0][1] = 0;
    for (int i = 0; i < (1 << n); i++) way[i] = i;
    sort(way, way + (1 << n), PopcountComp);
    for (int i = 0; i < (1 << n); i++) 
        for (int j = 0; j < n; j++) 
            if (i & (1 << j)) 
                for (int k = 0; k < n; k++) 
                    if (!(i & (1 << k))) 
                        f[k][i | (1 << k)] = min(f[k][i | (1 << k)], f[j][i] + a[j][k]);
    // for (int i = 0; i < n; i++)
    //     for (int j = 0; j < (1 << n); j++)
    //         if (f[i][j]) printf("f[%d][%d] = %d\n", i, j, f[i][j]);
    write(f[n - 1][(1 << n) - 1], 10);
    return 0;
}

// Create File Date : 2020-06-02