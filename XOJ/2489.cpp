//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      2489: 钓鱼（Fish）.
 * @user_id:      1677.
 * @user_name:    zhaoyi20(brealid).
 * @time:         2020-06-01.
 * @language:     C++.
 * @upload_place: XOJ.
*************************************/ 

const int N = 25 + 4, HT = 16 * 60 + 7;

int H, n;
int f[N], d[N], t[N];
int fish[N][HT];
int dp[N][HT];
#define dist(l, r) (t[r] - t[l])

signed main() {
    n = read<int>();
    H = read<int>() * 12;
    for (int i = 1; i <= n; i++) f[i] = read<int>();
    for (int i = 1; i <= n; i++) d[i] = read<int>();
    for (int i = 2; i <= n; i++) t[i] = t[i - 1] + read<int>();
    for (int i = 1; i <= n; i++) {
        int now = f[i];
        for (int j = 1; j <= H; j++) {
            fish[i][j] = fish[i][j - 1] + now;
            now = max(now - d[i], 0);
        }
    }
    memset(dp, 0xcf, sizeof(dp));
    dp[0][0] = 0;
    for (int i = 1; i <= n; i++) {
        for (int j = 0; j < i; j++) {
            for (int k = dist(j, i); k <= H; k++) {
                for (int p = k; p <= H; p++) {
                    dp[i][p] = max(dp[i][p], dp[j][k - dist(j, i)] + fish[i][p - k]);
                }
            }
        }
    }
    // for (int i = 1; i <= n; i++)
    //     for (int j = 0; j <= H; j++)
    //         printf("%-3d%c", dp[i][j], " \n"[j == H]);
    int ans = 0;
    for (int i = 1; i <= n; i++) ans = max(ans, dp[i][H]);
    write(ans, 10);
    return 0;
}

// Create File Date : 2020-06-01

/*
2 1
10 1
2 5
2
*/