//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      [ZJOI2017]树状数组.
 * @user_id:      63720.
 * @user_name:    brealid.
 * @time:         2020-06-06.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 
// #pragma GCC optimize(3)
#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int N = 1e5 + 7, P = 998244353, SIZ = 4e7 + 7;

#define FOR_EDGES(e, x) for (int e = head[x]; ~e; e = nxt[e])
#define ADD_EDGE(u, v) { to[cnt] = v; nxt[cnt] = head[u]; head[u] = cnt++; }
#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }
#define FA(u) (to[fae[u] ^ 1])

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

int Merge(int x, int y) { return add(mul(x, y), mul(1 + P - x, 1 + P - y)); }
template<typename... Type>
int Merge(int x, int y, Type... Args) { 
    return Merge(Merge(x, y), Args...); 
}

int fpow(int a, int n) {
    int res = 1;
    while (n) {
        if (n & 1) res = mul(res, a);
        a = mul(a, a);
        n >>= 1;
    }
    return res;
}

int n, m;
struct interval {
    int l, r;
    interval() : l(0), r(0) {}
    interval(int L, int R) : l(L), r(R) {}
    inline int mid() const { return (l + r) >> 1; }
    inline interval get_L() const { return interval(l, mid()); }        // get left
    inline interval get_R() const { return interval(mid() + 1, r); }    // get right
    inline int size() const { return r - l + 1; }
    inline bool exist() const { return l <= r; }
    inline bool isInclude(int pos) const { return pos >= l && pos <= r; }
    inline bool isInclude(interval s) const { return s.l >= l && s.r <= r; }
    inline bool noUnion(interval s) const { return s.r < l || s.l > r; }
    inline bool only_point(int pos) const { return pos == l && pos == r; }
} full;

struct square {
    interval x, y;
    square() : x(), y() {}
    square(interval L, interval R) : x(L), y(R) {}
    square(int xl, int xr, int yl, int yr) : x(xl, xr), y(yl, yr) {}
    inline square get_L() const { return square(x, y.get_L()); }            // get left
    inline square get_R() const { return square(x, y.get_R()); }            // get right
    inline square get_U() const { return square(x.get_L(), y); }            // get up
    inline square get_D() const { return square(x.get_R(), y); }            // get down
    inline square get_LU() const { return square(x.get_L(), y.get_L()); }   // get left-up
    inline square get_RU() const { return square(x.get_L(), y.get_R()); }   // get right-up
    inline square get_LD() const { return square(x.get_R(), y.get_L()); }   // get left-down
    inline square get_RD() const { return square(x.get_R(), y.get_R()); }   // get right-down
    inline bool exist() const { return x.exist() && y.exist(); }
    inline bool isInclude(int X, int Y) const { return x.isInclude(X) && y.isInclude(Y); }
    inline bool isInclude(square s) const { return x.isInclude(s.x) && y.isInclude(s.y); }
    inline bool noUnion(square s) const { return x.noUnion(s.x) || y.noUnion(s.y); }
    inline bool only_point(int X, int Y) const { return x.only_point(X) && y.only_point(Y); }
};

int rt[N * 4], l[SIZ], r[SIZ], val[SIZ], cnt = 0;

void update_(int &u, interval now, interval upd, int dif) {
    if (!now.exist() || upd.noUnion(now)) return;
    if (!u) val[u = ++cnt] = 1;
    if (upd.isInclude(now)) {
        val[u] = Merge(val[u], dif);
        // printf("(found [%d, %d] in [%d, %d]) val set to %d\n", now.l, now.r, upd.l, upd.r, val[u]);
        return;  
    }
    update_(l[u], now.get_L(), upd, dif);
    update_(r[u], now.get_R(), upd, dif);
}

void update(int u, interval now, square upd, int dif) {
    if (!now.exist() || upd.x.noUnion(now)) return;
    if (upd.x.isInclude(now)) {
        // printf("found sub_interval[%d, %d] in [%d, %d] ↓update_info\n", now.x.l, now.x.r, upd.x.l, upd.x.r);
        update_(rt[u], full, upd.y, dif);
        return;  
    }
    update(u << 1, now.get_L(), upd, dif);
    update(u << 1 | 1, now.get_R(), upd, dif);
}

int query_(const int &u, interval now, int x) {
    if (!now.exist() || !now.isInclude(x) || !u) return 1;
    if (now.only_point(x)) return val[u];
    return Merge(query_(l[u], now.get_L(), x), query_(r[u], now.get_R(), x), val[u]);
}

int query(int u, interval now, int x, int y) {
    if (!now.exist() || !now.isInclude(x)) return 1;
    if (now.only_point(x)) return query_(rt[u], full, y);
    if (x <= now.mid()) return Merge(query(u << 1, now.get_L(), x, y), query_(rt[u], full, y));
    else return Merge(query(u << 1 | 1, now.get_R(), x, y), query_(rt[u], full, y));
}

int main()
{
    n = read<int>() + 1;
    m = read<int>();
    full = interval(0, n);
    for (int i = 1, op, l, r; i <= m; i++) {
        op = read<int>();
        l = read<int>();
        r = read<int>();
        if (op == 1) {
            int InvP = fpow(r - l + 1, P - 2);
            // for square
            if (l > 1) update(1, full, square(1, l - 1, l, r), add(P - InvP, 1));
            if (r < n) update(1, full, square(l, r, r + 1, n), add(P - InvP, 1));
            update(1, full, square(l, r, l, r), add(P - InvP, P - InvP, 1));
            // for line
            if (l > 1) update(1, full, square(0, 0, 1, l - 1), 0);
            if (r < n) update(1, full, square(0, 0, r + 1, n), 0);
            update(1, full, square(0, 0, l, r), InvP);
        } else {
            write(query(1, full, l - 1, r), 10);
        }
        // for (int l = 0; l < n; l++)
        //     for (int r = l + 1; r < n; r++)
        //         fprintf(stderr, "query(%d, %d) = %d\n", l, r, query(1, full, l, r));
    }
    return 0;
}