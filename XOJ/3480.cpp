//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      The Battle of Chibi.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-20.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 1e9 + 7;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? Module(w - P) : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

const int N = 1007;

int n, k;
int a[N], f[N][N];

int t[N];

inline void update(int u, int x) {
    while (u <= (n + 1)) {
        t[u] = (t[u] + x) % P;
        u += (u & -u);
    }
}

inline int query(int u) {
    int ret = 0;
    while (u) {
        ret = (ret + t[u]) % P;
        u -= (u & -u);
    }
    return ret;
}

inline int query(int l, int r) {
    return (query(r) - query(l - 1) + P) % P;
}

inline void init() {
    memset(t, 0, sizeof(t));
}

inline void readln() {
    n = read<int>();
    k = read<int>();
    for (int i = 1; i <= n; i++) a[i] = read<int>();
}

inline void discretization() {
    static int buffer[N];
    for (int i = 1; i <= n; i++) buffer[i] = a[i];
    sort(buffer + 1, buffer + n + 1);
    for (int i = 1; i <= n; i++) a[i] = lower_bound(buffer + 1, buffer + n + 1, a[i]) - buffer + 1;
}

inline int solve() {
    for (int len = 1; len <= k; len++) {
        init();
        if (len == 1) update(1, 1);
        for (int i = 1; i <= n; i++) {
            f[len][i] = query(a[i] - 1);
            update(a[i], f[len - 1][i]);
            // printf("f[%d][%d] = %d\n", len, i, f[len][i]);
        }
    }
    int res = 0;
    for (int i = 1; i <= n; i++) res = (res + f[k][i]) % P;
    return res;
}

signed main() {
    int T = read<int>();
    for (int CASE = 1; CASE <= T; CASE++) {
        readln();
        discretization();
        printf("Case #%d: %d\n", CASE, solve());
    }
    return 0;
}

// Create File Date : 2020-08-20