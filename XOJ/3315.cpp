//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      韩爷的梦（字符Hash模板）.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-04.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64
const int P1 = 131, P2 = 167, P3 = 2017;

typedef char STR_1507[1507];
char s[1507];

struct Hash {
    unsigned h1 : 10;
    unsigned h2 : 13;
    unsigned h3 : 17;
    Hash() : h1(0), h2(0), h3(0) {}
    Hash(STR_1507 &str) : h1(0), h2(0), h3(0) {
        for (int i = 0, len = strlen(str); i < len; i++) {
            h1 = h1 * P1 + str[i];
            h2 = h2 * P2 + str[i];
            h3 = h3 * P3 + str[i];
        }
    }
    bool operator < (const Hash &b) const {
        if (h3 ^ b.h3) return h3 < b.h3;
        if (h2 ^ b.h2) return h2 < b.h2;
        return h1 < b.h1;
    }
    bool operator != (const Hash &b) const {
        return (h3 ^ b.h3) || (h2 ^ b.h2) || (h1 ^ b.h1);
    }
} h[20007];

namespace against_cpp11 {
    signed main() {
        int n = read.get_int<int>();
        for (int i = 0; i < n; i++) {
            scanf("%s", s);
            h[i] = Hash(s);
        }
        sort(h, h + n);
        int ans = 1;
        for (int i = 1; i < n; i++)
            if (h[i - 1] != h[i]) ans++;
        write << ans << '\n';
        return 0;
    }
}

signed main() { return against_cpp11::main(); }