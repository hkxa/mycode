//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Timeline.
 * @user_name:    zhaoyi20(brealid).
 * @time:         2020-05-20.
 * @language:     C++.
 * @upload_place: XOJ.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define N 100007

struct Edge {
    int to, v;
};
#define make_edge(u, v) ((Edge){(u), (v)})
vector<Edge> G[N];
int n, m;
int dis[N];
bool inq[N];
int vis[N];

int main()
{
    // [read data]
    n = read<int>();
    read<int>();
    m = read<int>();
    for (int i = 1, s; i <= n; i++) {
        s = read<int>();
        G[0].push_back(make_edge(i, s));
    }
    for (int i = 1, u, v, w; i <= m; i++) {
        u = read<int>();
        v = read<int>();
        w = read<int>();
        G[u].push_back(make_edge(v, w));
    }
    // [/read data] 
    // [spfa]
    queue<int> q;
    q.push(0);
    memset(dis, 0xcf, sizeof(dis));
    dis[0] = 0;
    while (!q.empty()) {
        int fr = q.front(); 
        q.pop();
        inq[fr] = 0;
        for (unsigned i = 0; i < G[fr].size(); i++) {
            if (dis[G[fr][i].to] < dis[fr] + G[fr][i].v) {
                dis[G[fr][i].to] = dis[fr] + G[fr][i].v;
                if (!inq[G[fr][i].to]) {
                    vis[G[fr][i].to]++;
                    // if (vis[G[fr][i].to] > n) {
                    //     puts("NO");
                    //     return 0;
                    // }
                    inq[G[fr][i].to] = true;
                    q.push(G[fr][i].to);
                }
            }
        }
    }
    // [/spfa]
    // [outpue answer]
    for (int i = 1; i <= n; i++)
        write(dis[i], 10);
    // [/outpue answer]
    return 0;
}