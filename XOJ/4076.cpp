/*************************************
 * @problem:      Interval GCD.
 * @user_name:    brealid/hkxadpall/zhaoyi20/j(o)moo/littleTortoise.
 * @time:         2020-10-03.
 * @language:     C++.
 * @fastio_ver:   20200913.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

#if 1
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0,    // input
        flush_stdout = 1 << 1,  // output
        flush_stderr = 1 << 2,  // output
    };
    enum number_type_flags {
        output_double_stable = 1 << 0,  // output
        output_double_faster = 1 << 1   // output
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f & ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore character set = {' ', '\r', '\n', '\t'}
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (double &lf) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            lf = endch & 15;
            while (isdigit(endch = getchar())) lf = lf * 10 + (endch & 15);
            if (endch == '.') {
                double digit = 0.1;
                while (isdigit(endch = getchar())) {
                    lf += (endch & 15) * digit;
                    digit *= 0.1;
                }
            }
            if (flag) lf = -lf;
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            Int d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return d;
        }
    } read;

    struct Writer {
        Writer& operator << (io_flags f) {
            if (f & flush_stdout) fflush(stdout);
            if (f & flush_stderr) fflush(stderr);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
        inline void operator () (double val, int eps_digit = 6, char endch = '\0', number_type_flags flg = output_double_faster) {
            if (flg & output_double_stable) {
                static char output_format[10];
                sprintf(output_format, "%%.%dlf", eps_digit);
                printf(output_format, val);
            } else if (flg & output_double_faster) {
                if (val < 0) {
                    putchar('-');
                    val = -val;
                }
                double eps_number = 0.5;
                for (int i = 1; i <= eps_digit; i++) eps_number /= 10;
                val += eps_number;
                (*this) << (int64)val;
                val -= (int64)val;
                if (eps_digit) putchar('.');
                while (eps_digit--) {
                    val *= 10;
                    putchar((int)val | '0');
                    val -= (int)val;
                }
            } else {
               (*this) << '1';
                if (eps_digit) {
                    (*this) << ".E";
                    for (int i = 2; i <= eps_digit; i++) (*this) << '0';
                }
            }
            if (endch) putchar(endch);
        }
    } write;
}
using namespace Fastio;

namespace File_IO {
    void init_IO(const char *file_name) {
        char buff[107];
        sprintf(buff, "%s.in", file_name);
        freopen(buff, "r", stdin);
        sprintf(buff, "%s.out", file_name);
        freopen(buff, "w", stdout);
    }
}

// #define int int64

inline int64 gcd(int64 a, int64 b) {
    if (!a || !b) return a | b;
    if (a < 0) a = -a;
    if (b < 0) b = -b;
    int t = __builtin_ctzll(a | b);
    a >>= __builtin_ctzll(a);
    do {
        b >>= __builtin_ctzll(b) ;
        if (a > b) swap(a, b);
        b -= a;
    } while(b);
    return a << t;
}

const int N = 5e5 + 7;

int n, m;

namespace BinaryIndexTree {
    int64 Tval[N];
    int64 query(int u) {
        int64 ret = 0;
        while (u) {
            ret += Tval[u];
            u -= u & -u;
        }
        return ret;
    }
    void modify(int u, int64 dif) {
        while (u <= n) {
            Tval[u] += dif;
            u += u & -u;
        }
    }
};

int64 tr[N << 2];

void modify(int u, int l, int r, int pos, int64 dif) {
    if (l == r) {
        tr[u] += dif;
        return;
    }
    int mid = (l + r) >> 1;
    if (pos <= mid) modify(u << 1, l, mid, pos, dif);
    else modify(u << 1 | 1, mid + 1, r, pos, dif);
    tr[u] = gcd(tr[u << 1], tr[u << 1 | 1]);
}

int64 query(int u, int l, int r, int ml, int mr) {
    if (l > mr || r < ml) return 0; 
    if (l >= ml && r <= mr) return tr[u];
    int mid = (l + r) >> 1;
    return gcd(query(u << 1, l, mid, ml, mr), query(u << 1 | 1, mid + 1, r, ml, mr));
}

signed main() {
    // File_IO::init_IO("Interval GCD");
    read >> n >> m;
    int64 las = 0, x;
    for (int i = 1; i <= n; i++) {
        read >> x;
        modify(1, 1, n, i, x - las);
        BinaryIndexTree::modify(i, x - las);
        las = x;
    }
    char opt;
    for (int i = 1, l, r; i <= m; i++) {
        read >> opt >> l >> r;
        if (opt == 'C') {
            read >> x;
            BinaryIndexTree::modify(l, x);
            modify(1, 1, n, l, x);
            if (r < n) {
                BinaryIndexTree::modify(r + 1, -x);
                modify(1, 1, n, r + 1, -x);
            }
        } else {
            write << gcd(BinaryIndexTree::query(l), query(1, 1, n, l + 1, r)) << '\n';
        }
    }
    return 0;
}