//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Cleaning Shifts 安排值班.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-19.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 25000 + 7, T = 1e6 + 7;

int n, t;
pair<int, int> cow[N];

int tmin[T << 2];
int query_min(int u, int l, int r, int ml, int mr) {
    if (l > mr || r < ml) return n + 1;
    if (l >= ml && r <= mr) return tmin[u];
    int mid = (l + r) >> 1;
    return min(query_min(u << 1, l, mid, ml, mr), query_min(u << 1 | 1, mid + 1, r, ml, mr));
}

void update_min(int u, int l, int r, int pos, int newer) {
    if (l == r) return void(tmin[u] = min(tmin[u], newer));
    int mid = (l + r) >> 1;
    if (pos <= mid) update_min(u << 1, l, mid, pos, newer);
    else update_min(u << 1 | 1, mid + 1, r, pos, newer);
    tmin[u] = min(tmin[u << 1], tmin[u << 1 | 1]);
}

signed main() {
    memset(tmin, 0x3f, sizeof(tmin));
    read >> n >> t;
    for (int i = 1; i <= n; i++)
        read >> cow[i].first >> cow[i].second;
    sort(cow + 1, cow + n + 1);
    update_min(1, 0, t, 0, 0);
    for (int i = 1; i <= n; i++)
        update_min(1, 0, t, cow[i].second, query_min(1, 0, t, cow[i].first - 1, cow[i].second) + 1);
    // for (int i = 0; i <= t; i++)
    //     printf("query_min(%d) = %d\n", i, query_min(1, 0, t, i, i));
    int ans = query_min(1, 0, t, t, t);
    if (ans == n + 1) puts("-1");
    else write << ans << '\n';
    return 0;
}