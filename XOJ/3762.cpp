//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      3762: The Pilots Brothers' refrigerator.
 * @user_id:      1677.
 * @user_name:    zhaoyi20(brealid).
 * @time:         2020-06-02.
 * @language:     C++.
 * @upload_place: XOJ.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// #define int int64

char t[5];
bool status[4][4], tmp[4][4];
int way[65536];

int Stable[16] = { 0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4 };
int popcountS(int a) { return Stable[a >> 4] + Stable[a & 15]; }
int popcount(int a) { return popcountS(a >> 8) + popcountS(a & 255); }
bool PopcountComp(int a, int b) { return popcount(a) < popcount(b); }

bool check(int way) {
    memcpy(tmp, status, sizeof(status));
    for (int i = 0; i < 16; i++) 
        if (way & (1 << i)) {
            int x = i >> 2, y = i & 3;
            tmp[x][0] ^= 1; tmp[x][1] ^= 1; tmp[x][2] ^= 1; tmp[x][3] ^= 1;
            tmp[0][y] ^= 1; tmp[1][y] ^= 1; tmp[2][y] ^= 1; tmp[3][y] ^= 1;
            tmp[x][y] ^= 1;
        }
    for (int i = 0; i < 4; i++)
        for (int j = 0; j < 4; j++)
            if (!tmp[i][j]) return false;
    return true;
}

signed main() {
    for (int i = 0; i < 4; i++) {
        scanf("%s", t);
        for (int j = 0; j < 4; j++)
            status[i][j] = t[j] == '-';
    }
    for (int i = 0; i < 65536; i++) way[i] = i;
    sort(way, way + 65536, PopcountComp);
    for (int i = 0; i < 65536; i++)
        if (check(way[i])) {
            printf("%d\n", popcount(way[i]));
            for (int j = 0; j < 16; j++) 
                if (way[i] & (1 << j))
                    printf("%d %d\n", (j >> 2) + 1, (j & 3) + 1);
            break;
        }
    return 0;
}

// Create File Date : 2020-06-02