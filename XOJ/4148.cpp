//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      后缀排序.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-05.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

namespace against_cpp11 {
    const int N = 1e6 + 7, SIZ = N * 10;
    int n, m;
    char s[N];
    int bucket[SIZ], x[SIZ], y[SIZ], SA[SIZ];
    void get_SuffixArray() {
        // pretreat : initial bucket sort
        for (int i = 0; i < n; i++) ++bucket[x[i] = s[i]];
        for (int i = 1; i <= m; i++) bucket[i] += bucket[i - 1];
        for (int i = n - 1; i >= 0; i--) SA[--bucket[x[i]]] = i;

        for (int k = 1, num; k < n; k *= 2) {
            fprintf(stderr, "%d(%.3lfs)..%c", k, (double)clock() / CLOCKS_PER_SEC, ".\n"[k * 2 >= n]);
            num = 0;
            for (int i = n - k; i < n; i++) y[num++] = i;
            for (int i = 0; i < n; i++)
                if (SA[i] >= k) y[num++] = SA[i] - k;
            for (int i = 0; i <= m; i++)
                bucket[i] = 0;
            for (int i = 0; i < n; i++)
                ++bucket[x[y[i]]];
            for (int i = 1; i <= m; i++)
                bucket[i] += bucket[i - 1];
            for (int i = n - 1; i >= 0; i--)
                SA[--bucket[x[y[i]]]] = y[i], y[i] = 0;
            for (int i = 0; i < n; i++)
                swap(x[i], y[i]);
            num = 0;
            x[SA[0]] = 0;
            for (int i = 1; i < n; i++)
                x[SA[i]] = (y[SA[i]] == y[SA[i - 1]] && y[SA[i] + k] == y[SA[i - 1] + k]) ? num : ++num;
            if (num + 1 == n) break;
            m = num;
        }
        return;
    }

    signed main() {
        scanf("%s", s);
        n = strlen(s);
        for (int i = 0; i < n; i++)
            if (isupper(s[i])) s[i] = s[i] - 'A' + 10;
            else if (islower(s[i])) s[i] = s[i] - 'a' + 36;
            else s[i] = s[i] - '0';
        m = 62;
        get_SuffixArray();
        for (int i = 0; i < n; i++)
            write << SA[i] + 1 << " \n"[i == n - 1];
        return 0;
    }
}

signed main() { return against_cpp11::main(); }