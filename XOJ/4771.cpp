/*************************************
 * @problem:      骑士.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-21.
 * @language:     C++.
 * @fastio_ver:   20200820.
*************************************/ 
#pragma GCC optimize(3)
#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore space, \t, \r, \n
            ch = getchar();
            while (ch != ' ' && ch != '\t' && ch != '\r' && ch != '\n') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            Int flag = 1;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = -1, endch = getchar();
            i = endch & 15;
            while (isdigit(endch = getchar())) i = (i << 3) + (i << 1) + (endch & 15);
            if (flag == -1) i = -i;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            Int flag = 1;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = -1, endch = getchar();
            Int i = endch & 15;
            while (isdigit(endch = getchar())) i = (i << 3) + (i << 1) + (endch & 15);
            if (flag == -1) i = -i;
            return i;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64
// #define int128 int64
// #define int128 __int128

const int N = 1e6 + 7;

int n, v[N], hate[N];
vector<int> G[N];
int visd[N], is_circle[N];
int cir[N], cir_cnt;
int64 f[N][2];

int find_circle(int u) {
    if (visd[u] == 1) {
        is_circle[u] = true;
        cir[cir_cnt++] = u;
        visd[u] = 3;
        return u;
    }
    visd[u] = 1;
    if (visd[hate[u]] >= 2) {
        visd[u] = 2;
        return 0;
    }
    int ret = find_circle(hate[u]);
    if (!ret || ret == u) {
        visd[u] = 2;
        return 0;
    }
    is_circle[u] = true;
    cir[cir_cnt++] = u;
    visd[u] = 3;
    return ret;
}

void tree_dp(int u) {
    f[u][1] = v[u];
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i];
        if (is_circle[v] || v == hate[u]) continue;
        tree_dp(v);
        f[u][1] += f[v][0];
        f[u][0] += max(f[v][1], f[v][0]);
    }
}

signed main() {
    read >> n;
    for (int i = 1; i <= n; i++) {
        read >> v[i] >> hate[i];
        G[hate[i]].push_back(i);
        G[i].push_back(hate[i]);
    }
    int64 ans = 0;
    for (int u = 1; u <= n; u++) {
        if (visd[u]) continue;
        cir_cnt = 0;
        find_circle(u);
        if (!cir_cnt) continue;
        for (int i = 0; i < cir_cnt; i++)
            tree_dp(cir[i]);
        // fprintf(stderr, "Circle Node {");
        // for (int i = 0; i < cir_cnt; i++)
        //     fprintf(stderr, " %d", cir[i]);
        // fprintf(stderr, " }\n");
        int64 f00, f01, f10, f11;
        f10 = f01 = -0x3fffffff;
        f00 = f[cir[0]][0];
        f11 = f[cir[0]][1];
        for (int i = 1; i < cir_cnt; i++) {
            int64 g00 = f00, g01 = f01, g10 = f10, g11 = f11;
            f00 = max(g00, g01) + f[cir[i]][0];
            f01 = g00 + f[cir[i]][1];
            f10 = max(g10, g11) + f[cir[i]][0];
            f11 = g10 + f[cir[i]][1];
        }
        ans += max(max(f00, f01), f10);
    }
    write << ans << '\n';
    return 0;
}