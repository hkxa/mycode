#include "..\eval.hpp"
#include <windows.h>
using namespace std;

int T = 30;

void random_data(int n, eval &e) {
    int cnt0 = n, cnt1 = n;
    while (cnt0 || cnt1) {
        if (cnt0 < cnt1) {
            if (cnt0) {
                if (randint(0, 1)) {
                    e << '1'; cnt1--;
                } else {
                    e << '0'; cnt0--;
                }
            } else {
                e << '1'; cnt1--;
            }
        } else {
            e << '0'; cnt0--;
        }
    }
    e << '\n';
    cnt0 = n; cnt1 = n;
    while (cnt0 || cnt1) {
        if (cnt0 < cnt1) {
            if (cnt0) {
                if (randint(0, 1)) {
                    e << '1'; cnt1--;
                } else {
                    e << '0'; cnt0--;
                }
            } else {
                e << '1'; cnt1--;
            }
        } else {
            e << '0'; cnt0--;
        }
    }
    e << '\n';
}

vector<int> G[3007];

void dfs(int u, eval &e) {
    e << '0';
    random_shuffle(G[u].begin(), G[u].end());
    for (size_t i = 0; i < G[u].size(); i++)
        dfs(G[u][i], e);
    e << '1';
}

void make_same(const int n, eval &e) {
    for (int i = 1; i <= n; i++) G[i].clear();
    for (int i = 2; i <= n; i++)
        G[randint(1, i - 1)].push_back(i);
    dfs(1, e); e << '\n';
    dfs(1, e); e << '\n';
}

int main()
{
    for (int i = 1; i <= 20; i++) {
        printf("generating data #%d...\n", i);
        // Sleep(1030);
        int n = i * 75;
        eval e;
        e.open("3950\\", i);
        e << T << '\n';
        for (int j = 1; j <= T; j++) {
            if (randint(0, 20) < 8) random_data(n, e);
            else make_same(n, e);
        }
        printf("running std ...\n");
        e.generate("std.exe");
    }
}