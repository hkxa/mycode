//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      3950: Subway tree systems.
 * @user_name:    brealid.
 * @time:         2020-06-04.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#define _USE_MATH_DEFINES
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <algorithm>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

const int N = 6e3 + 3, B = 2017, K = 201709, P = 20170933;

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

int T, n;
char s1[N], s2[N];
int st[N], top, nodecnt;
int l[2][N], r[2][N];
int Hash[N];
bool cmp(int i, int j) { return Hash[i] < Hash[j]; }
int tmp[N], siz;

void compare(int a, int u) {
    // printf("comp(%d, %d)\n", a, u);
    for (int i = l[a][u] + 1; i <= r[a][u]; i = r[a][i] + 1)
        compare(a, i);
    siz = 0;
    for (int i = l[a][u] + 1; i <= r[a][u]; i = r[a][i] + 1)
        tmp[siz++] = i;
    sort(tmp, tmp + siz, cmp);
    Hash[u] = add(mul(siz, B), K);
    for (int i = 0; i < siz; i++) {
        Hash[u] = add(mul(Hash[u], K), Hash[tmp[i]]);
    }
    // Hash[u] = add(mul(Hash[u], K), B);
}

signed main() {
    T = read<int>();
    while (T--) {
        scanf("%s%s", s1, s2);
        // gets(s1);
        // gets(s2);
        n = strlen(s1) / 2;
        nodecnt = top = 0;
        for (int i = 0; i < (n << 1); i++) {
            if (s1[i] & 1) {
                r[0][st[top--]] = nodecnt;
            } else {
                nodecnt++;
                l[0][nodecnt] = nodecnt;
                st[++top] = nodecnt;
            }
        }
        // printf("pas\n");
        nodecnt = top = 0;
        for (int i = 0; i < (n << 1); i++) {
            if (s2[i] & 1) {
                r[1][st[top--]] = nodecnt;
            } else {
                nodecnt++;
                l[1][nodecnt] = nodecnt;
                st[++top] = nodecnt;
            }
        }
        // printf("pas\n");
        l[0][0] = l[1][0] = 0;
        r[0][0] = r[1][0] = n;
        // for (int i = 0; i <= n; i++) 
        //     printf("{%d ~ %d} %c", l[0][i], r[0][i], " \n"[i == n]);
        // for (int i = 0; i <= n; i++) 
        //     printf("{%d ~ %d} %c", l[1][i], r[1][i], " \n"[i == n]);
        compare(0, 0);
        int hash1 = Hash[0];
        // printf("pas\n");
        compare(1, 0);
        int hash2 = Hash[0];
        // printf("pas\n");
        puts(hash1 == hash2 ? "same" : "different");
    }
    return 0;
}

// Create File Date : 2020-06-04