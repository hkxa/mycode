/*************************************
 * problem:      3943: 1803 City Game.
 * user name:    brealid.
 * time:         2020-06-05.
 * language:     C++.
 * upload place: XOJ.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, m;
bool a[2007][2007];
int l[2007][2007] = {0}, r[2007][2007] = {0}, up[2007][2007] = {0};

void getBlock(int x, int y)
{
    static char buf[3];
    scanf("%s", buf);
    a[x][y] = (buf[0] == 'F');
}

int main()
{
    int matrix = 1;
    n = read<int>();
    m = read<int>();
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++) {
            getBlock(i, j);
            up[i][j] = 1;
            l[i][j] = r[i][j] = j;
        }
    }
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j < m; j++) {
            if (a[i][j] && a[i][j + 1]) l[i][j + 1] = l[i][j];
        }
    }
    for (int i = 1; i <= n; i++) {
        for (int j = m; j > 1; j--) {
            if (a[i][j] && a[i][j - 1]) r[i][j - 1] = r[i][j];
        }
    }
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++) {
            if (i > 1 && a[i][j] && a[i - 1][j]) {
                l[i][j] = max(l[i][j], l[i - 1][j]);
                r[i][j] = min(r[i][j], r[i - 1][j]);
                // UP-2-lines : 使 up 数组的值正确
                up[i][j] = up[i - 1][j] + 1;
            }
            int d = r[i][j] - l[i][j] + 1; 
            int h = up[i][j];
            matrix = max(matrix, d * h);
        }
    }
    write(matrix * 3, 10);
    return 0;
}