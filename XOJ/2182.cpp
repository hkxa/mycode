//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      小胖守皇宫.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-18.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 2e5 + 7;

int n;
int f[N][3];
int cost[N];
bool have_father[N];
vector<int> G[N];

int ans = 0;

// void dp(int u) {
//     f[u][1] = cost[u];
//     if (!G[u].size()) ans += min(f[u][0], f[u][1]);
//     for (size_t i = 0; i < G[u].size(); i++) {
//         int v = G[u][i];
//         f[v][1] = min(f[u][0], f[u][1]);
//         f[v][0] = f[u][1];
//         dp(v);
//     }
// }

void dp(int u) {
    // [0] 未安排，需要安排
    // [1] 已安排
    // [2] 未安排，孩子已为自己安排
    f[u][1] = cost[u];
    int dif = 0x3fffffff;
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i];
        dp(v);
        f[u][0] += min(f[v][1], f[v][2]);
        f[u][1] += min(min(f[v][0], f[v][1]), f[v][2]);
        f[u][2] += min(f[v][1], f[v][2]);
        dif = min(dif, f[v][1] - min(f[v][1], f[v][2]));
    }
    f[u][2] += dif;
}

signed main() {
    read >> n;
    for (int i = 1, id, cnt, nod; i <= n; i++) {
        read >> id;
        read >> cost[id] >> cnt;
        while (cnt--) {
            read >> nod;
            have_father[nod] = true;
            G[id].push_back(nod);
        }
    }
    for (int i = 1; i <= n; i++)
        if (!have_father[i]) {
            dp(i);
            write << min(f[i][1], f[i][2]) << '\n';
            // write << ans << '\n';
            return 0;
        }
    return 0;
}