//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      3518: 次小生成树 Tree.
 * @user_name:    brealid.
 * @time:         2020-06-08.
 * @language:     C++.
 * @upload_place: XOJ.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 31011;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

const int N = 300000 + 7;

int n, m;
int UFS_fa[N];
int64 MinVal;
int ans = 2147483647;
int dep[N];
int fa[N][20], max1[N][20], max2[N][20];

void updateMultiply(int k, int u) {
    fa[u][k] = fa[fa[u][k - 1]][k - 1];
    int a = max1[u][k - 1], b = max1[fa[u][k - 1]][k - 1];
    int c = max2[u][k - 1], d = max2[fa[u][k - 1]][k - 1];
    max1[u][k] = max(a, b);
    if (a < b) max2[u][k] = max(a, d);
    else if (a > b) max2[u][k] = max(b, c);
    else max2[u][k] = max(c, d);
}

pair<int, int> GetMax(pair<int, int> u, int v1, int v2) {
    pair<int, int> res;
    res.first = max(u.first, v1);
    if (u.first < v1) res.second = max(u.first, v2);
    else if (u.first > v1) res.second = max(v1, u.second);
    else res.second = max(u.second, v2);
    return res;
}

struct edge {
    int v, w;
};
vector<edge> G[N];

struct Edge {
    int u, v, w;
    bool used;
    Edge() : used(0) {}
    bool operator < (const Edge &b) const { return w < b.w; }
} Ke[N];

int find(int x) { return UFS_fa[x] < 0 ? x : UFS_fa[x] = find(UFS_fa[x]); }
bool connect(int x, int y) {
    int fx = find(x), fy = find(y);
    if (fx != fy) {
        if (UFS_fa[fx] < UFS_fa[fy]) {
            UFS_fa[fy] = fx;
        } else if (UFS_fa[fx] > UFS_fa[fy]) {
            UFS_fa[fx] = fy;
        } else {
            UFS_fa[fy] = fx;
            UFS_fa[fx]--;
        }
        return true;
    }
    return false;
}

void Kruskal() {
    sort(Ke, Ke + m);
    memarr(n, -1, UFS_fa);
    int cnt = n - 1;
    for (int e = 0; e < m && cnt; e++) {
        if (connect(Ke[e].u, Ke[e].v)) {
            MinVal += Ke[e].w;
            cnt--;
            // printf("choose {%d, %d, %d}\n", Ke[e].u, Ke[e].v, Ke[e].w);
            G[Ke[e].u].push_back((edge){Ke[e].v, Ke[e].w});
            G[Ke[e].v].push_back((edge){Ke[e].u, Ke[e].w});
            Ke[e].used = true;
        }
    }
}

void preDfs(int u, int _Fa) {
    fa[u][0] = _Fa;
    dep[u] = dep[_Fa] + 1;
    for (int k = 1; k < 20 && fa[u][k - 1]; k++)
        updateMultiply(k, u);
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i].v;
        if (v != _Fa) {
            max1[v][0] = G[u][i].w;
            preDfs(v, u);
        }
    }
}

void jump(int &u, int k, pair<int, int> &ret) {
    for (int i = 0; i < 20; i++)
        if (k & (1 << i)) {
            // printf("u - up 2^%d {%d, %d}\n", i, max1[u][i], max2[u][i]);
            ret = GetMax(ret, max1[u][i], max2[u][i]);
            u = fa[u][i];
        }
}

pair<int, int> GetRoadMax(int u, int v) {
    pair<int, int> ret = {0, 0};
    if (dep[u] < dep[v]) swap(u, v);
    jump(u, dep[u] - dep[v], ret);
    if (u == v) return ret;
    for (int k = 19; k >= 0; k--) 
        if (fa[u][k] != fa[v][k]) {
            // printf("u - up 2^%d {%d, %d}\n", k, max1[u][k], max2[u][k]);
            // printf("v - up 2^%d {%d, %d}\n", k, max1[v][k], max2[v][k]);
            ret = GetMax(ret, max1[u][k], max2[u][k]);
            ret = GetMax(ret, max1[v][k], max2[v][k]);
            u = fa[u][k];
            v = fa[v][k];
        }
    return GetMax(GetMax(ret, max1[u][0], max2[u][0]), max1[v][0], max2[v][0]);
}

void solve(int u, int v, int w) {
    pair<int, int> mx = GetRoadMax(u, v);
    // printf("fa[u] = %d, fa[v] = %d\n", fa[u][0], fa[v][0]);
    // printf("Edge : {%d -> %d, val = %d}\nThe circle : mx = {%d, %d}\n", u, v, w, mx.first, mx.second);
    if (w == mx.first) {
        if (mx.second) ans = min(ans, w - mx.second);
    } else {
        ans = min(ans, w - mx.first);
    }
}

signed main() {
    n = read<int>();
    m = read<int>();
    for (int i = 0; i < m; i++) {
        Ke[i].u = read<int>();
        Ke[i].v = read<int>();
        Ke[i].w = read<int>();
    }
    Kruskal();
    preDfs(1, 0);
    for (int i = 0; i < m; i++)
        if (!Ke[i].used) solve(Ke[i].u, Ke[i].v, Ke[i].w);
    // printf("MinVal : %lld, ans : %d\n", MinVal, ans);
    write(ans + MinVal, 10);
    return 0;
}

// Create File Date : 2020-06-08
// 2030617 -> 2031787