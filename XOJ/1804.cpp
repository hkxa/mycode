//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      I Hate It.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-12.
 * @language:     C++.
*************************************/

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char int8;
typedef unsigned char uint8;
typedef short int16;
typedef unsigned short uint16;
typedef int int32;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace baseFastio
{
template <typename Int>
inline Int read()
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF)
        c = getchar();
    if (c == '-')
        flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar()))
        init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF)
        c = getchar();
    if (c == '-')
        flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar()))
        init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0)
        putchar('-'), x = ~x + 1;
    if (x > 9)
        write(x / 10);
    putchar((x % 10) | 48);
}

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}
} // namespace baseFastio

struct Reader
{
    char endch;
    Reader() { endch = '\0'; }
    template <typename Int>
    Reader &operator>>(Int &i)
    {
        i = baseFastio::read<Int>(endch);
        return *this;
    }
    template <typename Int>
    inline Int get_int()
    {
        return baseFastio::read<Int>();
    }
    inline char get_nxt()
    {
        return endch;
    }
} read;

struct Writer
{
    Writer &operator<<(const char *ch)
    {
        // char *p = ch;
        while (*ch)
            putchar(*(ch++));
        return *this;
    }
    Writer &operator<<(const char ch)
    {
        putchar(ch);
        return *this;
    }
    template <typename Int>
    Writer &operator<<(const Int i)
    {
        baseFastio::write(i);
        return *this;
    }
} write;

// #define int int64

const int MAXN = 6e4 + 5, INF = 0x3f3f3f3f;
int st[MAXN << 2], lazy[MAXN << 2];
int n, m;

void pushup(int rt)
{
    st[rt] = min(st[rt << 1], st[rt << 1 | 1]);
}

void pushdown(int rt)
{
    if (lazy[rt] == 0)
        return;
    lazy[rt << 1] += lazy[rt];
    lazy[rt << 1 | 1] += lazy[rt];
    st[rt << 1] -= lazy[rt];
    st[rt << 1 | 1] -= lazy[rt];
    lazy[rt] = 0;
}

void build(int l, int r, int rt)
{
    st[rt] = m;
    lazy[rt] = 0;
    if (l == r)
    {
        st[rt] = m;
        return;
    }
    int mid = (l + r) >> 1;
    pushdown(rt);
    build(l, mid, rt << 1);
    build(mid + 1, r, rt << 1 | 1);
    pushup(rt);
}

void update(int L, int R, int val, int l, int r, int rt)
{
    if (L <= l && r <= R)
    {
        lazy[rt] += val;
        st[rt] -= val;
        return;
    }
    pushdown(rt);
    int mid = (l + r) >> 1;
    if (L <= mid)
        update(L, R, val, l, mid, rt << 1);
    if (R > mid)
        update(L, R, val, mid + 1, r, rt << 1 | 1);
    pushup(rt);
}

int query(int L, int R, int l, int r, int rt)
{
    if (L <= l && r <= R)
        return st[rt];
    pushdown(rt);
    int mid = (l + r) >> 1, res = INF;
    if (L <= mid)
        res = min(res, query(L, R, l, mid, rt << 1));
    if (R > mid)
        res = min(res, query(L, R, mid + 1, r, rt << 1 | 1));
    pushup(rt);
    return res;
}

int main()
{
    int r, x, y, z;
    while (~scanf("%d%d%d", &n, &m, &r))
    {
        build(1, n, 1);
        while (r--)
        {
            read >> x >> y >> z;
            if (query(x, y - 1, 1, n, 1) >= z)
            {
                printf("T\n");
                update(x, y - 1, z, 1, n, 1);
            }
            else
                printf("F\n");
        }
        printf("\n");
    }
}
// python3 -m pygame.examples.aliens