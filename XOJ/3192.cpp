//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      数列分块入门 7.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-10.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 10007;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

const int N = 200007, SqrtN = 707;

int n, m;
int a[N];
int t[N], p[N];

#define block(v) ((v) / m)
#define beg(v)   max((v) * m, 1)
#define end(v)   min(((v) + 1) * m - 1, n)
#define val(v)   add(mul(a[v], t[block(v)]), p[block(v)])

inline void pushBlock(int x) {
    for(int i = beg(x); i <= end(x); i++)
        a[i] = val(i);
    t[x] = 1;
    p[x] = 0;
}

inline void modifyViolenceAdd(int l, int r, int c) {
    // printf("modifyViolenceAdd(%d, %d, %d)\n", l, r, c);
    // printf("DEBUG : ");
    // for (int i = 1; i <= n; i++) write(val(i), " \n"[i == n]);
    pushBlock(block(l));
    // printf("DEBUG : ");
    // for (int i = 1; i <= n; i++) write(val(i), " \n"[i == n]);
    for (int i = l; i <= r; i++) a[i] = add(a[i], c);
    // printf("DEBUG : ");
    // for (int i = 1; i <= n; i++) write(val(i), " \n"[i == n]);
}

inline void modifyViolenceMul(int l, int r, int c) {
    // printf("modifyViolenceMul(%d, %d, %d)\n", l, r, c);
    pushBlock(block(l));
    for (int i = l; i <= r; i++) a[i] = mul(a[i], c);
}

signed main() {
    n = read<int>();
    m = sqrt(n);
    for (int i = 1; i <= n; i++) a[i] = read<int>();
    for (int i = 0; i <= block(n); i++) t[i] = 1;
    for (int i = 1, opt, l, r, c, b1, b2; i <= n; i++) {
        opt = read<int>();
        l = read<int>();
        r = read<int>();
        c = read<int>() % P;
        b1 = block(l);
        b2 = block(r);
        switch (opt) {
            case 0:
                if (b1 == b2) modifyViolenceAdd(l, r, c);
                else {
                    modifyViolenceAdd(l, end(b1), c);
                    modifyViolenceAdd(beg(b2), r, c);
                    for (int i = b1 + 1; i < b2; i++) p[i] = add(p[i], c);
                }
                break;
            case 1:
                if (b1 == b2) modifyViolenceMul(l, r, c);
                else {
                    modifyViolenceMul(l, end(b1), c);
                    modifyViolenceMul(beg(b2), r, c);
                    for (int i = b1 + 1; i < b2; i++) {
                        t[i] = mul(t[i], c);
                        p[i] = mul(p[i], c);
                    }
                }
                break;
            case 2:
                write(val(r), 10);
                break;
        }
        // printf("DEBUG : ");
        // for (int i = 1; i <= n; i++) write(val(i), " \n"[i == n]);
    }
    return 0;
}

// Create File Date : 2020-06-10