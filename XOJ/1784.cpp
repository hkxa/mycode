//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      1784: 数列极差问题.
 * @user_id:      1677.
 * @user_name:    zhaoyi20(brealid).
 * @time:         2020-06-02.
 * @language:     C++.
 * @upload_place: XOJ.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// #define int int64

int n;
int64 a[107];
priority_queue<int64, vector<int64>, less<int64> > mn;
priority_queue<int64, vector<int64>, greater<int64> > mx;

signed main() {
    n = read<int>();
    for (int i = 1, num; i <= n; i++) {
        num = read<int>();
        mn.push(num);
        mx.push(num);
    }
    int64 t1, t2;
    while (--n) {
        t1 = mn.top(); mn.pop(); t2 = mn.top(); mn.pop();
        mn.push(t1 * t2 + 1);
        t1 = mx.top(); mx.pop(); t2 = mx.top(); mx.pop();
        mx.push(t1 * t2 + 1);
    }
    write(mx.top() - mn.top(), 10);
    return 0;
}

// Create File Date : 2020-06-02