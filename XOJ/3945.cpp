//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      3945: 1806 Matrix.
 * @user_name:    brealid.
 * @time:         2020-06-05.
 * @language:     C++.
 * @upload_place: XOJ.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 91666667, K = 2;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

const int N = 1007;

int n, m, a, b, q;
string str;
bool src[N][N], task[N][N];
int up[N][N], lef[N][N];
int power[N];
bool occur[P + 7];

#define hash_next(Ha, Nx) add(mul(Ha, K), Nx)
#define hash_Next(Ha, Nx) add(mul(Ha, power[a]), Nx)
#define getHashUp(u, i, j) add(up[j][u], P - mul(up[i][u], power[a]))
// #define getHashLef(u, i, j) add(lef[u][j], P - mul(lef[u][i], power[j - i]))

signed main() {
    n = read<int>();
    m = read<int>();
    a = read<int>();
    b = read<int>();
    power[0] = 1;
    for (int i = 1; i <= 1000; i++) power[i] = mul(power[i - 1], K);
    int PowerAB = 1;
    for (int i = 1; i <= a; i++) PowerAB = mul(PowerAB, power[b]);
    for (int i = 1; i <= n; i++) {
        cin >> str;
        for (int j = 1; j <= m; j++)
            src[i][j] = (str[j - 1] & 1);
    }
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++) {
            up[i][j] = hash_next(up[i - 1][j], src[i][j]);
            // lef[i][j] = hash_next(lef[i][j - 1], src[i][j]);
        }
    }
    int hash;
    for (int i = 1; i <= n - a + 1; i++) {
        hash = 0;
        for (int j = 1; j <= b; j++) {
            hash = hash_Next(hash, getHashUp(j, i - 1, i + a - 1));
        }
        occur[hash] = true;
        // printf("occur : hash = %d\n", hash);
        for (int j = 2; j <= m - b + 1; j++) {
            hash = hash_Next(hash, getHashUp(j + b - 1, i - 1, i + a - 1));
            // printf("(through %d) ", hash);
            hash = add(hash, P - mul(getHashUp(j - 1, i - 1, i + a - 1), PowerAB));
            occur[hash] = true;
            // printf("occur : hash = %d\n", hash);
        }
    }
    q = read<int>();
    while (q--) {
        for (int i = 1; i <= a; i++) {
            cin >> str;
            for (int j = 1; j <= b; j++)
                task[i][j] = (str[j - 1] & 1);
        }
        int hash = 0, h;
        for (int j = 1; j <= b; j++) {
            h = 0;
            for (int i = 1; i <= a; i++) {
                h = hash_next(h, task[i][j]);
            }
            // printf("h : %d\n", h);
            hash = hash_Next(hash, h);
        }
        // printf("hash : %d\n", hash);
        if (occur[hash]) puts("1");
        else puts("0");
    }
    return 0;
}

// Create File Date : 2020-06-05
/*
00000
00101
01011
11101
00101
*/