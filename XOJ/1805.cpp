//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      I Hate It.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-12.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

struct Reader {
    char endch;
    Reader() { endch = '\0'; }
    template <typename Int>
    Reader& operator >> (Int &i) {
        i = baseFastio::read<Int>(endch);
        return *this;
    }
    template <typename Int>
    inline Int get_int() {
        return baseFastio::read<Int>();
    }
    inline char get_nxt() {
        return endch;
    }
} read;

struct Writer {
    Writer& operator << (const char *ch) {
        // char *p = ch;
        while (*ch) putchar(*(ch++));
        return *this;
    }
    Writer& operator << (const char ch) {
        putchar(ch);
        return *this;
    }
    template <typename Int>
    Writer& operator << (const Int i) {
        baseFastio::write(i);
        return *this;
    }
} write;

// #define int int64

int n, m;
int op, x, y; int64 k;
int a[200007];

int tree[200007 << 4], lazy[200007 << 4];  
 
int pushup(int sta1, int sta2) { return sta1 | sta2; }
 
void pushdown(int l ,int r, int node) {
    if (lazy[node]) {
        tree[node << 1] = tree[node << 1 | 1] = lazy[node];
        lazy[node << 1] = lazy[node << 1 | 1] = lazy[node];
        lazy[node] = 0;
    }
}
 
void build(int l, int r, int node) {
    if (l == r) {
        tree[node] = 2;
        lazy[node] = 0;
        return;
    }
    int mid = (l + r) >> 1;
    build(l, mid, node << 1); build(mid + 1, r, node << 1 |1);
    tree[node] = pushup(tree[node << 1], tree[node << 1 | 1]);
}
 
int ql, qr, v;
 
void update(int l, int r, int node){
    if (ql <= l && qr >= r) {
        tree[node] = 1 << v;
        lazy[node] = 1 << v;
        return ;
    }
    pushdown(l, r, node);
    int mid = (l + r) >> 1;
    if(ql <= mid) update(l, mid, node << 1);
    if(qr > mid) update(mid + 1, r, node << 1 | 1);
    tree[node] = pushup(tree[node<<1], tree[node<<1|1]);
}
 
int query(int l, int r, int node){
    if(ql <= l && qr >= r) return tree[node];
    pushdown(l, r, node);
    int ans = 0;
    int mid = (l + r) >> 1;
    if(ql <= mid) ans |= query(l, mid, node << 1);
    if(qr > mid) ans |= query(mid + 1, r, node << 1 |1);
    return ans;
}
 
int main(){
    int l, t, q;
    while(~scanf("%d%d%d", &l, &t, &q)){
        build(1, l, 1);
        while(q --) {
            char c[3]; scanf("%s", c);
            if(c[0] == 'C') {
                scanf("%d%d%d", &ql, &qr, &v);
                if(ql > qr) swap (ql, qr);
                update(1, l, 1);
            } else {
                scanf("%d%d", &ql ,&qr);
                if(ql > qr) swap (ql, qr);
                int sta = query(1, l, 1);
                int ans = 0;
                while(sta) {
                    if(sta & 1) ans ++;
                    sta >>= 1;
                }
                printf("%d\n", ans);
            }
        }
    }
    return 0;
}