//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      I-country.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-17.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 16, Ksiz = 226;

int n, m, K, now, si, ans;
int a[N][N], sum[N][N], f[N][Ksiz][N][N][4];

struct node {
    int l, r, x;
} S[N][Ksiz][N][N][4], SS;


#define update(A, L, R, X) {                \
    int val = sum[i][r] - sum[i][l - 1] + A;\
    if (val >= f[i][j][l][r][x]) {          \
        S[i][j][l][r][x] = (node){L, R, X}; \
        f[i][j][l][r][x] = val;             \
    }                                       \
}

int main() {
    read >> n >> m >> K;
    for (int i = 1; i <= n; i++)
        for (int j = 1; j <= m; j++) {
            read >> a[i][j];
            sum[i][j] = sum[i][j - 1] + a[i][j];
        }
    for (int i = 1; i <= n; i++)
        for (int j = 1; j <= K; j++)
            for (int l = 1; l <= m; l++)
                for (int r = l, k = 1; k <= j && r <= m; r++, k++) {
                    int x = 0;
                    for (x = 0; x < 4; x++) update(0, m, 0, x);

                    x = 3;
                    for (int p = l; p <= r; p++)
                        for (int q = p; q <= r; q++)
                            update(f[i - 1][j - k][p][q][3], p, q, 3);

                    x = 0;
                    for (int p = 1; p <= l; p++)
                        for (int q = r; q <= m; q++)
                            for (int op = 0; op < 4; op++)
                                update(f[i - 1][j - k][p][q][op], p, q, op);

                    x = 2;
                    for (int p = l; p <= r; p++)
                        for (int q = r; q <= m; q++) {
                            update(f[i - 1][j - k][p][q][2], p, q, 2);
                            update(f[i - 1][j - k][p][q][3], p, q, 3);
                        }

                    x = 1;
                    for (int p = 1; p <= l; p++)
                        for (int q = l; q <= r; q++) {
                            update(f[i - 1][j - k][p][q][1], p, q, 1);
                            update(f[i - 1][j - k][p][q][3], p, q, 3);
                        }
                }
    for (int i = 1; i <= n; i++)
        for (int l = 1; l <= m; l++)
            for (int r = l; r <= m; r++)
                for (int x = 0; x < 4; x++)
                    if (ans < f[i][K][l][r][x]) {
                        ans = f[i][K][l][r][x];
                        si = i;
                        SS = (node){l, r, x};
                    }
    printf("Oil : %d\n", ans);
    while (K != 0) {
        for (int i = SS.l; i <= SS.r; i++)
            printf("%d %d\n", si, i);
        int nK = K - (SS.r - SS.l + 1);
        SS = S[si--][K][SS.l][SS.r][SS.x];
        K = nK;
    }
    return 0;
}