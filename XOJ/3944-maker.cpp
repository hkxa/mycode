#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

int n = 10000;

signed main() {
    printf("%d", n);
    for (int i = 1; i <= n - 2; i++) {
        printf("%d %d %d\n", i, 1, n);
    }
    printf("0 0 0\n");
    return 0;
}

// Create File Date : 2020--