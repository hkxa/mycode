//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      2330: 商务旅行.
 * @user_name:    zhaoyi20(brealid).
 * @time:         2020-05-26.
 * @language:     C++.
 * @upload_place: XOJ.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define int int64

int n, k;
vector<int> G[50007];
int f[50007];
int dep[50007];
int siz[50007], wson[50007];
int beg[50007];

void dfs1(int u, int fa) {
    f[u] = fa;
    dep[u] = dep[fa] + 1;
    siz[u] = 1;
    for (unsigned i = 0; i < G[u].size(); i++) {
        int v = G[u][i];
        if (v == fa) continue;
        dfs1(v, u);
        siz[u] += siz[v];
        if (siz[v] > siz[wson[u]]) wson[u] = v;
    }
}

void dfs2(int u, int top) {
    beg[u] = top;
    if (wson[u]) dfs2(wson[u], top);
    for (unsigned i = 0; i < G[u].size(); i++) {
        int v = G[u][i];
        if (v == f[u] || v == wson[u]) continue;
        dfs2(v, v);
    }
}

int LCA(int u, int v) {
    while (beg[u] != beg[v]) {
        if (dep[beg[u]] < dep[beg[v]]) swap(u, v);
        u = f[beg[u]];
    }
    if (dep[u] < dep[v]) swap(u, v);
    return v;
}

int Dist(int u, int v) {
    return dep[u] + dep[v] - 2 * dep[LCA(u, v)];
}

signed main()
{
    n = read<int>();
    for (int i = 1, a, b; i < n; i++) {
        a = read<int>();
        b = read<int>();
        G[a].push_back(b);
        G[b].push_back(a);
    }
    dfs1(1, 0);
    dfs2(1, 1);
    k = read<int>();
    int ans = 0;
    for (int i = 1, las = 1, p; i <= k; i++) {
        p = read<int>();
        ans += Dist(p, las);
        las = p;
    }
    write(ans, 10);
    return 0;
}