/*************************************
 * @problem:      Clock Tree.
 * @user_name:    zhaoyi20(brealid).
 * @time:         2020-05-19.
 * @language:     C++.
 * @upload_place: XOJ.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;
int a[2507], v[2507];
vector<int> G[2507];
int ans = 0;

bool dfs(int u, int fa)
{
    // if (fa && G[u].size() != 1) v[u] = (v[u] + 1) % 12;
    for (unsigned i = 0; i < G[u].size(); i++) {
        if (G[u][i] != fa) {
            dfs(G[u][i], u);
            v[u] = (v[u] + 12 - v[G[u][i]]) % 12;
        }
    }
    if (!fa) return v[u] <= 1;
}

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        a[i] = read<int>() % 12;
    }
    for (int i = 1, u, v; i < n; i++) {
        u = read<int>();
        v = read<int>();
        G[u].push_back(v);
        G[v].push_back(u);
    }
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= n; j++) v[j] = a[j];
        if (dfs(i, 0)) {
            ans++;
            // fprintf(stderr, "%d can be the root\n", i);
        }
    }
    write(ans, 10);
    return 0;
} 

/*
7
12 11 11 11 10 11 12
1 2 2 3 3 4 4 5 5 6 6 7
*/