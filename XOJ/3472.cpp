//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Polygon.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-16.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

inline int op_plus(const int &x, const int &y) { return x + y; }
inline int op_mul(const int &x, const int &y) { return x * y; }
typedef int op_calc(const int &x, const int &y);

const int N = 107;

int n;
op_calc *op[N];
int f[N][N], g[N][N];

signed main() {
    read >> n;
    memset(f, 0xcf, sizeof(f));
    memset(g, 0x3f, sizeof(g));
    for (int i = 1; i <= n; i++) {
        char buf[3];
        scanf("%s", buf);
        if (buf[0] == 't') op[i] = op[i + n] = op_plus;
        if (buf[0] == 'x') op[i] = op[i + n] = op_mul;
        read >> f[i][i];
        f[i + n][i + n] = g[i + n][i + n] = g[i][i] = f[i][i];
    }
    for (int len = 2; len <= n; len++) 
        for (int i = 1, j = len; j <= 2 * n; i++, j++) {
            for (int k = i + 1; k <= j; k++) {
                f[i][j] = max(f[i][j], op[k](f[i][k - 1], f[k][j]));
                f[i][j] = max(f[i][j], op[k](f[i][k - 1], g[k][j]));
                f[i][j] = max(f[i][j], op[k](g[i][k - 1], f[k][j]));
                f[i][j] = max(f[i][j], op[k](g[i][k - 1], g[k][j]));
                g[i][j] = min(g[i][j], op[k](f[i][k - 1], f[k][j]));
                g[i][j] = min(g[i][j], op[k](f[i][k - 1], g[k][j]));
                g[i][j] = min(g[i][j], op[k](g[i][k - 1], f[k][j]));
                g[i][j] = min(g[i][j], op[k](g[i][k - 1], g[k][j]));
            }
        }
    int ans = INT_MIN;
    vector<int> v;
    for (int i = 1; i <= n; i++) {
        if (f[i][i + n - 1] > ans) {
            ans = f[i][i + n - 1];
            v.clear(); 
            v.push_back(i);
        } else if (f[i][i + n - 1] == ans) {
            v.push_back(i);
        }
    }
    write << ans << '\n';
    for (size_t i = 0; i < v.size(); i++)
        write << v[i] << " \n"[i == v.size() - 1];
    return 0;
}
