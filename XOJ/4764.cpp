//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Boatherds 船夫.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-02.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// #define int int64

const int N = 40007;

int n, k[107], Q;
int vis[N];
int h[N], cnt;
int to[N << 1], val[N << 1], nxt[N << 1], head[N];
int ans = 0;
int happen[20000007];

// int tc, tGS, tGR, tCA, tFL, tST;

int siz[N], root, rootMax;
void basicFindRoot_getSiz(int u) {
    siz[u] = 1;
    vis[u] = true;
    for (int e = head[u]; ~e; e = nxt[e]) {
        if (!vis[to[e]]) {
            basicFindRoot_getSiz(to[e]);
            siz[u] += siz[to[e]];
        }
    }
    vis[u] = false;
}

void basicFindRoot_getRoot(int u, int si) {
    int tMax = 1;
    vis[u] = true;
    for (int e = head[u]; ~e; e = nxt[e]) {
        const int &v = to[e];
        if (!vis[v]) {
            basicFindRoot_getRoot(v, si);
            tMax = max(tMax, siz[v]);
        }
    }
    tMax = max(tMax, si - siz[u]);
    if (tMax < rootMax) {
        root = u;
        rootMax = tMax;
    }
    vis[u] = false;
}

inline int findRoot(int u) {
    rootMax = n + 1;
    // tc = clock();
    basicFindRoot_getSiz(u);
    // tGS += clock() - tc;
    // tc = clock();
    basicFindRoot_getRoot(u, siz[u]);
    // tGR += clock() - tc;
    return root;
}

void flatten(int u, int Dis) {
    vis[u] = true;
    h[cnt++] = min(Dis, 10000000);
    for (int e = head[u]; ~e; e = nxt[e]) {
        if (!vis[to[e]]) {
            flatten(to[e], Dis + val[e]);
        }
    }
    vis[u] = false;
}

inline void calcAnswer(int u, int Dis, int dif) {
    // tc = clock();
    cnt = 0;
    flatten(u, Dis);
    // tFL += clock() - tc;
    // int TmpC = clock();
    sort(h, h + cnt);
    // tST += clock() - TmpC;
    // printf("flatten : { %d", h[0]);
    // for (int i = 1; i < cnt; i++) printf(", %d", h[i]);
    // printf(" }\n");
    // for (int i = 0; i < cnt && h[i] <= 10000000; i++) {
    //     for (int j = i + 1; j < cnt && h[i] + h[j] <= 10000000; j++)
    //         happen[h[i] + h[j]] += dif;
    for (int g = 1; g <= Q; g++) {
        int i = 0, j = cnt - 1, x, y;
        while (i < j) {
            while (i < j && h[i] + h[j] > k[g]) j--;
            if (i >= j) continue;
            if (h[i] + h[j] == k[g]) {
                if (h[i] == h[j]) {
                    happen[g] += (j - i + 1) * (j - i) / 2 * dif;
                    break;
                }
                x = 0; y = 0;
                while (h[i + x] + h[j] == k[g]) x++;
                while (h[i] + h[j - y] == k[g]) y++;
                i = i + x;
                j = j - y;
                happen[g] += x * y * dif;
            } else i++;
        }
    }
    // int Pi = cnt - 1, Pj = cnt - 1;
    // while (h[Pi] > 10000000) Pi--;
    // for (int i = 0; i < Pi; i++) {
    //     while (h[i] + h[Pj] > 10000000) Pj--;
    //     for (int j = 0; j < Pj; j++)
    //         happen[h[i] + h[j]] += dif;
    // }
    // tCA += clock() - tc;
}

void solve(int u) {
    // printf("solve(%d)\n", u);
    calcAnswer(u, 0, 1);
    vis[u] = true;
    for (int e = head[u]; ~e; e = nxt[e]) {
        const int &v = to[e];
        if (!vis[v]) {
            calcAnswer(v, val[e], -1);
            solve(findRoot(v));
        }
    }
    vis[u] = false;
}

signed main() {
    n = read<int>();
    Solve_again:;
    memset(head, -1, sizeof(int) * (n + 2));
    cnt = 0;
    for (int u = 1, v, w; u <= n; u++) {
        Read_again:;
        v = read<int>();
        if (!v) continue;
        w = read<int>();
        to[++cnt] = v; val[cnt] = w; nxt[cnt] = head[u]; head[u] = cnt;
        to[++cnt] = u; val[cnt] = w; nxt[cnt] = head[v]; head[v] = cnt;
        goto Read_again;
    }
    Q = 0;
    while (k[++Q] = read<int>());
    --Q;
    solve(findRoot(1));
    for (int i = 1; i <= Q; i++) {
        puts(happen[i] > 0 ? "AYE" : "NAY");
        happen[i] = 0;
    }
    puts(".");
    n = read<int>();
    if (n) goto Solve_again;
    return 0;
}