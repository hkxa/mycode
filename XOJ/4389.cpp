//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      4389: 直径.
 * @user_name:    brealid.
 * @time:         2020-06-08.
 * @language:     C++.
 * @upload_place: XOJ.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 31011;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

const int N = 200000 + 7;

int n;
int Mxson[N], wson[N];
int64 dis[N];
struct edge {
    int v, w;
};
vector<edge> G[N];
int diamU, diamV;

void dfs(int u, int fa) {
    Mxson[u] = u;
    wson[u] = 0;
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i].v, w = G[u][i].w;
        if (v != fa) {
            dis[v] = dis[u] + w;
            dfs(v, u);
            if (dis[Mxson[v]] > dis[Mxson[u]]) {
                Mxson[u] = Mxson[v];
                wson[u] = v;
            }
        }
    }
}

signed main() {
    n = read<int>();
    for (int i = 1, u, v, w; i < n; i++) {
        u = read<int>();
        v = read<int>();
        w = read<int>();
        G[u].push_back((edge){v, w});
        G[v].push_back((edge){u, w});
    }
    dfs(1, 0);
    memarr(n, 0, dis);
    dfs(diamU = Mxson[1], 0);
    diamV = Mxson[diamU];
    write(dis[diamV], 10);
    int u = diamU;
    bool StopFlag = false;
    while (wson[u] && !StopFlag) {
        for (size_t i = 0; i < G[u].size(); i++) {
            int v = G[u][i].v;
            if (dis[Mxson[v]] == dis[Mxson[u]] && Mxson[v] != Mxson[u]) {
                StopFlag = true;
                break;
            }
        }
        if (!StopFlag) u = wson[u];
    }
    memarr(n, 0, dis);
    dfs(diamV, 0);
    int cnt = 0;
    StopFlag = false;
    while (wson[u] && !StopFlag) {
        for (size_t i = 0; i < G[u].size(); i++) {
            int v = G[u][i].v;
            if (dis[Mxson[v]] == dis[Mxson[u]] && Mxson[v] != Mxson[u]) {
                StopFlag = true;
                break;
            }
        }
        if (!StopFlag) {
            u = wson[u];
            cnt++;
        }
    }
    write(cnt, 10);
    return 0;
}

// Create File Date : 2020-06-08