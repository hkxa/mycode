//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Morning after holloween.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-06.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

struct Reader {
    char endch;
    Reader() { endch = '\0'; }
    template <typename Int>
    Reader& operator >> (Int &i) {
        i = baseFastio::read<Int>(endch);
        return *this;
    }
    template <typename Int>
    inline Int get_int() {
        return baseFastio::read<Int>();
    }
    inline char get_nxt() {
        return endch;
    }
} read;

struct Writer {
    Writer& operator << (const char *ch) {
        // char *p = ch;
        while (*ch) putchar(*(ch++));
        return *this;
    }
    Writer& operator << (const char ch) {
        putchar(ch);
        return *this;
    }
    template <typename Int>
    Writer& operator << (const Int i) {
        baseFastio::write(i);
        return *this;
    }
} write;

// #define int int64

const int dx[5] = {1, 0, 0, -1, 0}, dy[5] = {0, -1, 1, 0, 0};

int w, h, n;
char mp[17][17];
int f[12000000];
int sx[3], sy[3], ex[3], ey[3], x[3], y[3];
int s, e, tmp;
queue<int> q;

void split(int tmp_div) {
    for (int i = n - 1; i >= 0; i--) {
        sy[i] = tmp_div % 15; tmp_div /= 15;
        sx[i] = tmp_div % 15; tmp_div /= 15;
    } 
}

#define cross_each_other() (sx[i] == x[u] && sy[i] == y[u] && sx[u] == x[i] && sy[u] == y[i])
#define in_same_pos() (x[i] == x[u] && y[i] == y[u])

void dfs_next(const int original, int u) {
    if (u >= n) {
        tmp = 0;
        for (int i = 0; i < n; i++) tmp = tmp * 225 + x[i] * 15 + y[i];
        if (!~f[tmp]) {
            f[tmp] = f[original] + 1;
            q.push(tmp);
        }
        return;
    }
    for (int op = 0; op < 5; op++) {
        x[u] = sx[u] + dx[op];
        y[u] = sy[u] + dy[op];
        if (mp[x[u]][y[u]] == '#') goto NextOp;
        for (int i = 0; i < u; i++)
            if (u != i && (cross_each_other() || in_same_pos())) goto NextOp;
        dfs_next(original, u + 1);
        NextOp:;
    }
}

signed main()
{
    read >> w >> h >> n;
    while (w && h && n) {
        s = e = 0;
        for (int i = 0; i < h; i++) {
            while (getchar() != '#');
            mp[i][0] = '#';
            for (int j = 1; j < w; j++) {
                switch (mp[i][j] = getchar()) {
                    case 'a' : sx[0] = i; sy[0] = j; mp[i][j] = ' '; break;
                    case 'b' : sx[1] = i; sy[1] = j; mp[i][j] = ' '; break;
                    case 'c' : sx[2] = i; sy[2] = j; mp[i][j] = ' '; break;
                    case 'A' : ex[0] = i; ey[0] = j; mp[i][j] = ' '; break;
                    case 'B' : ex[1] = i; ey[1] = j; mp[i][j] = ' '; break;
                    case 'C' : ex[2] = i; ey[2] = j; mp[i][j] = ' '; break;
                    default : break;
                }
            }
        }
        for (int i = 0; i < n; i++) s = s * 225 + sx[i] * 15 + sy[i];
        for (int i = 0; i < n; i++) e = e * 225 + ex[i] * 15 + ey[i];
        memset(f, -1, sizeof(f));
        f[s] = 0;
        while (!q.empty()) q.pop();
        q.push(s);
        while (!q.empty()) {
            s = q.front(); q.pop();
            if (s == e) break;
            split(s);
            dfs_next(s, 0);
        }
        write << f[e] << '\n';
        read >> w >> h >> n;
    }
    return 0;
}