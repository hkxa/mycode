//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Fotile 模拟赛 L.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-03.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

namespace against_cpp11 {
    const int N = 12000 + 5, SIZ = N * 40, Sqr = 120 + 7, MaxK = 32;
    int64 n, m, q, cnt, i, j, lin, f[Sqr][N];
    int64 t, ans, l[SIZ], r[SIZ], sz[SIZ];
    int64 tot, power2[Sqr], a[N], rt[N], x, y, a2, b, c;

    void insert(int last, int o, int64 v, int bitK) {
        sz[o] = sz[last] + 1;
        if (bitK == 0) return;
        l[o] = l[last];
        r[o] = r[last];
        if (v & power2[bitK - 1]) {
            r[o] = ++tot;
            insert(r[last], r[o], v, bitK - 1);
        } else {
            l[o] = ++tot;
            insert(l[last], l[o], v, bitK - 1);
        }
    }

    void query(int last, int o, int64 v, int bitK) {
        if (bitK == 0)
            return;
        if (v & power2[bitK - 1]) {
            if (sz[l[o]] - sz[l[last]] > 0)
                query(l[last], l[o], v, bitK - 1);
            else {
                c -= power2[bitK - 1];
                query(r[last], r[o], v, bitK - 1);
            }
        } else {
            if (sz[r[o]] - sz[r[last]] > 0)
                c += power2[bitK - 1], query(r[last], r[o], v, bitK - 1);
            else {
                query(l[last], l[o], v, bitK - 1);
            }
        }
    }
    
    signed main() {
        read >> n >> q;
        fprintf(stderr, "case : n = %d, q = %d\n", n, q);
        cnt = sqrt(n);
        if (cnt * cnt < n) cnt++;
        for (i = 0; i <= 33; i++) power2[i] = (1 << i);
        rt[0] = ++tot;
        insert(rt[0], rt[0], 0, MaxK);
        for (i = 1; i <= n; i++) {
            read >> a[i];
            rt[i] = ++tot;
            a[i] ^= a[i - 1];
            insert(rt[i - 1], rt[i], a[i], MaxK);
        }
        for (i = 1; i <= cnt; i++) {
            for (j = (i - 1) * cnt + 1; j <= n; j++) {
                c = a[j];
                if (i == 1) {
                    query(rt[(i - 1) * cnt], rt[j], a[j], MaxK);
                    f[i][j] = max(f[i][j - 1], c);
                    f[i][j] = max(f[i][j], a[j]);
                } else {
                    query(rt[(i - 1) * cnt - 1], rt[j], a[j], MaxK);
                    f[i][j] = max(f[i][j - 1], c);
                }
            }
        }
        while (q--) {
            read >> x >> y;
            a2 = min(((x + ans) % n) + 1, ((y + ans) % n) + 1);
            b = max(((x + ans) % n) + 1, ((y + ans) % n) + 1);
            ans = 0;
            lin = 0;
            x = a2;
            y = b;
            for (i = 1; i <= cnt; i++) {
                if (x <= (i - 1) * cnt + 1 && (i - 1) * cnt + 1 <= y) {
                    ans = f[i][y];
                    lin = i;
                    break;
                }
            }
            if (lin) {
                for (i = x - 1; i < (cnt * (lin - 1)) + 1; i++) {
                    c = a[i];
                    query(rt[x - 2], rt[y], a[i], MaxK);
                    ans = max(ans, c);
                }
            } else {
                for (i = x - 1; i <= y; i++) {
                    c = a[i];
                    query(rt[x - 2], rt[y], a[i], MaxK);
                    ans = max(ans, c);
                }
            }
            write << ans << '\n';
        }
        fprintf(stderr, "Times Used = %dms\n", clock());
    }
}

signed main() { return against_cpp11::main(); }