//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      2928: Milking Grid.
 * @user_name:    brealid.
 * @time:         2020-06-05.
 * @language:     C++.
 * @upload_place: XOJ.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 988244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

const int R = 10000 + 3, C = 75 + 3;

int r, c;
char a[R][C];
int ans1 = 1, ans2 = 1;

int gcd(int m, int n) {
    return m ? gcd(n % m, m) : n;
}

int lcm(int a, int b) {
    return a * b / gcd(a, b);
}

int fail[R];

bool comp(int i, int j) {
    for (int k = 0; k < r; k++)
        if (a[k][i] != a[k][j]) return 1;
    return 0;
}

signed main() {
    r = read<int>();
    c = read<int>();
    for (int i = 0; i < r; i++) 
        scanf("%s", a[i]);
    // for (int i = 0, j, k; i < r; i++) {
    //     for (j = 0, k = 1; k < c; j++, k++) {
    //         if (a[i][j] != a[i][k]) j = -1;
    //     }
    //     ans1 = min(c, lcm(ans1, k - j));
    // }
    // for (int i = 0, j, k; i < c; i++) {
    //     for (j = 0, k = 1; k < r; j++, k++) {
    //         if (a[j][i] != a[k][i]) j = -1;
    //     }
    //     ans2 = min(r, lcm(ans2, k - j));
    // }
    int j, k; bool flag;
    for (j = -1, k = 1, fail[0] = -1; k < c; k++) {
        // printf("j = %d, k = %d\n", j, k);
        // flag = false;
        // for (int i = 0; i < r; i++) {
        //     if (a[i][j + 1] != a[i][k]) {
        //         // j = fail[j];
        //         flag = true;
        //         break;
        //     }
        // }
        while (~j && comp(j + 1, k)) {
            // printf("j = %d, k = %d\n", j, k);
            // flag = false;
            j = fail[j];
            // for (int i = 0; i < r; i++) {
            //     if (a[i][j + 1] != a[i][k]) {
            //         // j = fail[j];
            //         flag = true;
            //         break;
            //     }
            // }
        }
        // flag = false;
        // for (int i = 0; i < r; i++) {
        //     if (a[i][j + 1] != a[i][k]) {
        //         j = fail[j];
        //         flag = true;
        //         break;
        //     }
        // }
        // if (!flag) j++;
        if (!comp(j + 1, k)) j++;
        fail[k] = j;
        // printf("fail[%d] = %d\n", k, j);
    }
    ans1 = c - 1 - j;
    for (j = -1, k = 1, fail[0] = -1; k < r; k++) {
        // flag = strcmp(a[j + 1], a[k]);
        while (~j && strcmp(a[j + 1], a[k])) {
            j = fail[j];
            // flag = strcmp(a[j + 1], a[k]);
        }
        // flag = strcmp(a[j + 1], a[k]);
        if (!strcmp(a[j + 1], a[k])) j++;
        fail[k] = j;
    }
    ans2 = r - 1 - j;
    write(ans1 * ans2, 10);
    return 0;
}

// Create File Date : 2020-06-05
/*
5 8
BCABCABC
CABCABCA
ABCABCAB
BCABCABC
CABCABCA
4 7
AAAAAAA
ABBBBBA
ABBBBBA
AAAAAAA
4 7
AAAAAAA
AAAAAAA
AAABAAA
AAAAAAA
*/