//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Mr. Young's Picture Permutations 杨老师的照相排列.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-16.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &b1) {
            b1 = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int b1) {
            baseFastio::write(b1);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64


signed main() {
    register int k, a1 = 31, a2 = 16, a3 = 11, a4 = 8, a5 = 6;
    int64 f[32][17][12][9][7];
    memset(f, 0, sizeof(f));
    f[0][0][0][0][0] = 1;
    for (register int b1 = 0; b1 <= a1; b1++) {
        for (register int b2 = 0; b2 <= a2; b2++) {
            for (register int b3 = 0; b3 <= a3; b3++) {
                for (register int b4 = 0; b4 <= a4; b4++) {
                    for (register int b5 = 0; b5 <= a5; b5++) {
                        if (b1 < a1)
                            f[b1 + 1][b2][b3][b4][b5] += f[b1][b2][b3][b4][b5];
                        if (b2 < a2 && b2 < b1)
                            f[b1][b2 + 1][b3][b4][b5] += f[b1][b2][b3][b4][b5];
                        if (b3 < a3 && b3 < b2)
                            f[b1][b2][b3 + 1][b4][b5] += f[b1][b2][b3][b4][b5];
                        if (b4 < a4 && b4 < b3)
                            f[b1][b2][b3][b4 + 1][b5] += f[b1][b2][b3][b4][b5];
                        if (b5 < a5 && b5 < b4)
                            f[b1][b2][b3][b4][b5 + 1] += f[b1][b2][b3][b4][b5];
                    }
                }
            }
        }
    }
    while (true) {
        if (scanf("%d", &k) != 1 || !k) return 0;
        if (k >= 1) read >> a1; else a1 = 0;
        if (k >= 2) read >> a2; else a2 = 0;
        if (k >= 3) read >> a3; else a3 = 0;
        if (k >= 4) read >> a4; else a4 = 0;
        if (k >= 5) read >> a5; else a5 = 0;
        write << f[a1][a2][a3][a4][a5] << '\n';
    }
    return 0;
}