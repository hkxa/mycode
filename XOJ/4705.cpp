/*************************************
 * @problem:      Swapity Swap 交换.
 * @user_name:    zhaoyi20,brealid.
 * @time:         2020-05-18.
 * @language:     C++.
 * @upload_place: XOJ.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, k;
int A1, A2, B1, B2;
int a[107];

struct mat {
    int a[107][107];
    mat(int num = 0) {
        memset(a, 0, sizeof(a));
        for (int i = 1; i <= n; i++) a[i][i] = num;
    }
    mat operator * (const mat b) {
        mat res;
        for (int i = 1; i <= n; i++) 
            for (int j = 1; j <= n; j++) 
                for (int k = 1; k <= n; k++)
                    res.a[i][j] += a[i][k] * b.a[k][j];
        // for (int i = 1; i <= n; i++) 
        //     for (int j = 1; j <= n; j++) 
        //         write(res.a[i][j], " \n"[j == n]);
        return res;
    }
} A, B, ans;

mat kpow(mat a, int n) {
    mat ret(1);
    while (n) {
        if (n & 1) ret = ret * a;
        a = a * a;
        n >>= 1;
    }
    return ret;
}

int main()
{
    n = read<int>();
    k = read<int>();
    A1 = read<int>();
    A2 = read<int>();
    B1 = read<int>();
    B2 = read<int>();
    for (int i = 1; i <= n; i++) a[i] = i;
    for (int i = 1; i < A1; i++) A.a[i][i] = 1;
    for (int i = A1; i <= A2; i++) A.a[i][A2 - (i - A1)] = 1;
    for (int i = A2 + 1; i <= n; i++) A.a[i][i] = 1;
    for (int i = 1; i < B1; i++) B.a[i][i] = 1;
    for (int i = B1; i <= B2; i++) B.a[i][B2 - (i - B1)] = 1;
    for (int i = B2 + 1; i <= n; i++) B.a[i][i] = 1;
    ans = kpow(B * A, k);
    for (int i = 1; i <= n; i++) 
        for (int j = 1; j <= n; j++)
            if (ans.a[i][j]) {
                write(j, 10);
                break;
            }
    return 0;
}