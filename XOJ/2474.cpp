//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      2474: 天天爱跑步.
 * @user_name:    zhaoyi20(brealid).
 * @time:         2020-05-25.
 * @language:     C++.
 * @upload_place: XOJ.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int N = 300007;

int n, m;
vector<int> G[N];
int siz[N], wson[N];
int dep[N], top[N], f[N];
int w[N], s[N], t[N], l[N];

void dfs1(int u, int fa = 0) {
    f[u] = fa;
    dep[u] = dep[fa] + 1;
    siz[u] = 1;
    for (unsigned i = 0; i < G[u].size(); i++) {
        int v = G[u][i];
        if (v != fa) {
            dfs1(v, u);
            siz[u] += siz[v];
            if (siz[v] > siz[wson[u]]) wson[u] = v;
        }
    }
}

void dfs2(int u, int beg) {
    top[u] = beg;
    if (wson[u]) dfs2(wson[u], beg);
    for (unsigned i = 0; i < G[u].size(); i++) {
        int v = G[u][i];
        if (v != f[u] && v != wson[u]) {
            dfs2(v, v);
        }
    }
}

int LCA(int u, int v) {
    // if (i % 200 == 0) printf("query LCA(%d, %d) : ", u, v);
    // int cnt = 0;
    while (top[u] != top[v]) {
        if (dep[top[u]] < dep[top[v]]) swap(u, v);
        u = f[top[u]];
        // cnt++;
    }
    if (dep[u] < dep[v]) swap(u, v);
    // printf("%d times\n", cnt);
    // if (i % 200 == 0) printf("exited\n");
    return v;
}

int dif1[N];
// int dif2[N];
// int cnt[N];
int _tong[N * 2];
// int order[N];
int ans[N];
#define bucket(x) _tong[(x) + N]
vector<int> p1[N], m1[N];

bool cmp(int x, int y) { return dep[x] > dep[y]; }

void dfs4(int u) {
    int prev = bucket(dep[u] + w[u]);
    bucket(dep[u]) += dif1[u];
    for (size_t i = 0; i < G[u].size(); i++) if (f[u] != G[u][i]) dfs4(G[u][i]);
    ans[u] += bucket(dep[u] + w[u]) - prev;
    for (size_t i = 0; i < m1[u].size(); i++) {
        bucket(dep[s[m1[u][i]]])--;
    }
}

void dfs3(int u) {
    int prev = bucket(dep[u] - w[u]);
    for (size_t i = 0; i < p1[u].size(); i++) {
        bucket(p1[u][i])++;
        // printf("node %d : bucket(%d)++ now=%d\n", u, p1[u][i], bucket(p1[u][i]));
    }
    for (size_t i = 0; i < G[u].size(); i++) if (f[u] != G[u][i]) dfs3(G[u][i]);
    ans[u] += bucket(dep[u] - w[u]) - prev;
    for (size_t i = 0; i < m1[u].size(); i++) {
        bucket(2 * dep[l[m1[u][i]]] - dep[s[m1[u][i]]])--;
        // printf("node %d : bucket(%d)-- now=%d\n", u, m1[u][i], bucket(m1[u][i]));
    }
}

int main()
{
    n = read<int>();
    m = read<int>();
    for (int i = 1, u, v; i < n; i++) {
        u = read<int>();
        v = read<int>();
        G[u].push_back(v);
        G[v].push_back(u);
    }
    dfs1(1, 0);
    dfs2(1, 1);
    for (int i = 1; i <= n; i++) w[i] = read<int>();
    // for (int i = 1; i <= n; i++) order[i] = i;
    // printf("**\n");
    for (int i = 1; i <= m; i++) {
        // if (i % 500 == 0) printf("Player %d\n", i);
        s[i] = read<int>();
        t[i] = read<int>();
        l[i] = LCA(s[i], t[i]);
        dif1[s[i]]++;
        // dif2[f[l[i]]]--;
        p1[t[i]].push_back(2 * dep[l[i]] - dep[s[i]]);
        m1[l[i]].push_back(i);
    }
    // printf("**\n");
    dfs4(1);
    // printf("**\n");
    // for (int i = 1; i <= n; i++) write(ans[i], i == n ? 10 : 32);
    memset(_tong, 0, sizeof(_tong));
    dfs3(1);
    // printf("**\n");
    // for (int i = 1; i <= n; i++) write(ans[i], i == n ? 10 : 32);
    for (int i = 1; i <= n; i++) if (dep[s[i]] == dep[l[i]] + w[l[i]]) ans[l[i]]--;
    for (int i = 1; i <= n; i++) write(ans[i], i == n ? 10 : 32);
    return 0;
}

/*
5 3
1 2
2 3
2 4
1 5
0 1 0 3 0
3 1
1 4
5 5

7 5
1 2
1 3
2 4
2 5
3 6
3 7
2 1 3 0 2 4 0
1 5
7 1
4 6
3 2
4 5
*/