//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      1191: 家族.
 * @user_name:    zhaoyi20(brealid).
 * @time:         2020-05-15.
 * @language:     C++.
 * @upload_place: XOJ.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int N = 50007;

int n, m, p;
int fa[N];

int find(int x) {
    return (x == fa[x]) ? x : (fa[x] = find(fa[x]));
}

inline void combine(int x, int y) {
    // fa[find(x)] = find(y);
    fa[find(y)] = find(x);
    // int fx = find(x), fy = find(y);
    // printf("find(%d) = %d, find(%d) = %d\n", x, fx, y, fy);
    // if (fx != fy) {
    //     // if (fa[fx] < fa[fy]) swap(fx, fy);
    //     // fa[fy] += fa[fx];
    //     fa[fx] = fy;
    // }
}

// Θ(路径压缩 + 按秩合并) = Θ(nαn)
// Θ(路径压缩) = Θ(按秩合并) = Θ(nlogn) 

int main()
{
    n = read<int>();
    m = read<int>();
    p = read<int>();
    for (int i = 1; i <= n; i++) fa[i] = i;
    for (int i = 1, u, v; i <= m; i++) {
        u = read<int>();
        v = read<int>();
        combine(u, v);
    }
    for (int i = 1, u, v; i <= p; i++) {
        u = read<int>();
        v = read<int>();
        if (find(u) == find(v)) printf("Yes\n");
        else printf("No\n");
    }
    return 0;
}