//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      银河.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-15.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 998244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? Module(w - P) : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

const int N = 1e5 + 7;

int n, m;
vector<pair<int, int> > G[N], T[N];
int dfn[N], low[N], dft;
int fam[N], siz[N], cnt;

int s[N], top;
bool ins[N];
int ind[N];

int dis[N];

void tarjan(int u) {
    low[u] = dfn[u] = ++dft;
    s[++top] = u;
    ins[u] = true;
    for (size_t i = 0; i < G[u].size(); i++) {
        if (!dfn[G[u][i].first]) {
            tarjan(G[u][i].first);
            low[u] = min(low[u], low[G[u][i].first]);
        } else if (ins[G[u][i].first]) {
            low[u] = min(low[u], dfn[G[u][i].first]);
        }
    }
    if (dfn[u] == low[u]) {
        cnt++;
        while (low[s[top]] >= low[u]) {
            ins[s[top]] = false;
            fam[s[top--]] = cnt;
            siz[cnt]++;
        }
    }
}

void DAG_LongestRoad(int u) {
    static queue<int> q;
    q.push(u);
    while (!q.empty()) {
        u = q.front();
        q.pop();
        // printf("front_pop u(%d) dis(%d)\n", u, dis[u]);
        for (size_t i = 0; i < T[u].size(); i++) {
            if (dis[T[u][i].first] < dis[u] + T[u][i].second)
                dis[T[u][i].first] = dis[u] + T[u][i].second;
            ind[T[u][i].first]--;
            if (!ind[T[u][i].first])
                q.push(T[u][i].first);
        }
    }
}

signed main() {
    n = read<int>();
    m = read<int>();
    for (int i = 1, T, a, b; i <= m; i++) {
        T = read<int>();
        a = read<int>();
        b = read<int>();
        switch (T) {
            case 1 : 
                G[b].push_back(make_pair(a, 0)); // a - b >= 0
                G[a].push_back(make_pair(b, 0)); // b - a >= 0
                break;
            case 2 : 
                G[a].push_back(make_pair(b, 1)); // b - a >= 1
                break;
            case 3 : 
                G[b].push_back(make_pair(a, 0)); // a - b >= 0
                break;
            case 4 : 
                G[b].push_back(make_pair(a, 1)); // a - b >= 1
                break;
            case 5 : 
                G[a].push_back(make_pair(b, 0)); // b - a >= 0
                break;
        }
    }
    // for (int i = 1; i <= n; i++)
    //     for (size_t j = 0; j < G[i].size(); j++) 
    //         printf("E <%d, %d> <%d>\n", i, G[i][j].first, G[i][j].second);
    for (int i = 1; i <= n; i++)
        if (!dfn[i]) tarjan(i);
    for (int i = 1; i <= n; i++)
        for (size_t j = 0; j < G[i].size(); j++) 
            if (fam[i] == fam[G[i][j].first]) {
                if (G[i][j].second) {
                    puts("-1");
                    return 0;
                }
            } else {
                // printf("Connect edge <%d -> %d> (v = %d)\n", fam[i], fam[G[i][j].first], G[i][j].second);
                T[fam[i]].push_back(make_pair(fam[G[i][j].first], G[i][j].second));
                ind[fam[G[i][j].first]]++;
            }
    for (int i = 1; i <= cnt; i++)
        if (!ind[i]) DAG_LongestRoad(i);
    int64 ans = 0;
    for (int i = 1; i <= cnt; i++)
        ans += (int64)dis[i] * siz[i];
    write(ans + n, 10);
    return 0;
}

// Create File Date : 2020-06-15