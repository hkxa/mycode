//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      2304: MM不哭.
 * @user_name:    zhaoyi20(brealid).
 * @time:         2020-05-27.
 * @language:     C++.
 * @upload_place: XOJ.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// #define int int64
const int N = 1007;
int n, v;
struct WD {
    int w, d, id;
    bool operator < (const WD &other) const {
        return d < other.d;
    }
} a[N];
int f[2][N][N];
int _sumW[N];
#define cost(L, R) (_sumW[n] - _sumW[R] + _sumW[(L) - 1])
#define dist(L, R) (a[R].d - a[L].d)
// #define min4(a, b, c, d) min(min(a, b), min(c, d))

signed main()
{
    n = read<int>();
    v = read<int>();
    for (int i = 1; i <= n; i++) {
        a[i].d = read<int>();
        a[i].w = read<int>();
        a[i].id = i;
    }
    sort(a + 1, a + n + 1);
    memset(f, 0x3f, sizeof(f));
    for (int i = 1; i <= n; i++) {
        _sumW[i] = _sumW[i - 1] + a[i].w;
        if (v == a[i].id) f[0][i][i] = f[1][i][i] = 0;
        // printf("a[%d] : { .d = %d, .w = %d, .id = %d }\n", i, a[i].d, a[i].w, a[i].id);
    }
    for (int l = 2; l <= n; l++)
        for (int i = 1, j = l; j <= n; i++, j++) {
            f[0][i][j] = min(f[0][i + 1][j] + cost(i + 1, j) * dist(i, i + 1),
                             f[1][i + 1][j] + cost(i + 1, j) * dist(i, j));
            f[1][i][j] = min(f[0][i][j - 1] + cost(i, j - 1) * dist(i, j),
                             f[1][i][j - 1] + cost(i, j - 1) * dist(j - 1, j));
            // printf("f[0][%d][%d] = %-10d f[1][%d][%d] = %-10d\n", i, j, f[0][i][j], i, j, f[1][i][j]);
        }
    write(min(f[0][1][n], f[1][1][n]), 10);
    return 0;
}

/*
4 3
1 100
10 1
9 19260817
13 2
=> 857
*/