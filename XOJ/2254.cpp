//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      叶子的颜色.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-18.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 10000 + 7, INF = 0x3f3f3f3f;

int m, n;
int c[N];
int f[N][2], g[N][2];
vector<int> G[N];

void dp1(int u, int fa) {
    if (u <= n) {
        f[u][c[u]] = 1;
        f[u][c[u] ^ 1] = INF;
        return;
    }
    f[u][0] = f[u][1] = 1;
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i];
        if (v == fa) continue;
        dp1(v, u);
        f[u][0] += min(f[v][0] - 1, f[v][1]);
        f[u][1] += min(f[v][1] - 1, f[v][0]);
    }
}

void dp2(int u, int fa) {
    // printf("f[%d][0] = %d, f[%d][1] = %d, g[%d][0] = %d, g[%d][1] = %d\n", u, f[u][0], u, f[u][1], u, g[u][0], u, g[u][1]);
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i];
        if (v == fa || v <= n) continue;
        g[v][0] = f[v][0] + min(g[u][0] - min(f[v][0] - 1, f[v][1]) - 1, g[u][1] - min(f[v][0], f[v][1] - 1));
        g[v][1] = f[v][1] + min(g[u][1] - min(f[v][1] - 1, f[v][0]) - 1, g[u][0] - min(f[v][1], f[v][0] - 1));
        dp2(v, u);
    }
}

signed main() {
    read >> m >> n;
    for (int i = 1; i <= n; i++) read >> c[i];
    for (int i = 1, u, v; i < m; i++) {
        read >> u >> v;
        G[u].push_back(v);
        G[v].push_back(u);
    }
    dp1(m, 0);
    g[m][0] = f[m][0];
    g[m][1] = f[m][1];
    dp2(m, 0);
    int ans = 0;
    for (int i = n + 1; i <= m; i++) ans = min(g[i][0], g[i][1]);
    write << ans << '\n';
    return 0;
}