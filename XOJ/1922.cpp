//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      埃及分数.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-06.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

struct Reader {
    char endch;
    Reader() { endch = '\0'; }
    template <typename Int>
    Reader& operator >> (Int &i) {
        i = baseFastio::read<Int>(endch);
        return *this;
    }
    template <typename Int>
    inline Int get_int() {
        return baseFastio::read<Int>();
    }
    inline char get_nxt() {
        return endch;
    }
} read;

struct Writer {
    Writer& operator << (const char *ch) {
        // char *p = ch;
        while (*ch) putchar(*(ch++));
        return *this;
    }
    Writer& operator << (const char ch) {
        putchar(ch);
        return *this;
    }
    template <typename Int>
    Writer& operator << (const Int i) {
        baseFastio::write(i);
        return *this;
    }
} write;

// #define int int64

int64 gcd(int64 a, int64 b) {
    return b ? gcd(b, a % b) : a;
}

struct fraction {
    int64 a, b;
    fraction reduce() {
        int64 g = gcd(abs(a), b);
        a /= g; b /= g;
        return *this;
    }
    fraction() {}
    fraction(int64 num) : a(num), b(1) {}
    fraction(int64 c, int64 d) : a(c), b(d) { reduce(); }
    fraction operator * (fraction x) {
        return fraction(a * x.a, b * x.b);
    }
    fraction operator / (fraction x) {
        return fraction(a * x.b, b * x.a);
    }
    fraction operator + (fraction x) {
        return fraction(a * x.b + b * x.a, b * x.b);
    }
    fraction operator - (fraction x) {
        return fraction(a * x.b - b * x.a, b * x.b);
    }
    bool less_than_zero() {
        return a < 0;
    }
    bool is_unit_fraction() {
        return a == 1;
    }
};

int ans1 = 1e9, ans2 = 0;
int64 status[1000007], ans[1000007];

bool better(int n) {
    while (n) {
        if (status[n] < ans[n]) return true;
        if (status[n] > ans[n]) return false;
        n--;
    }
    return false;
}

void dfs(fraction remain, int cnt, int limit, int now) {
    // printf("dfs(frac{%lld, %lld}, %d, %d, %d)\n", remain.a, remain.b, cnt, limit, now);
    if (remain.is_unit_fraction() && remain.b >= now) {
        status[cnt] = remain.b;
        if (cnt < ans1) {
            ans1 = cnt;
            ans2 = remain.b;
            for (int i = 1; i <= cnt; i++) ans[i] = status[i];
        } else if (cnt == ans1 && better(cnt)) {
            ans2 = remain.b;
            for (int i = 1; i <= cnt; i++) ans[i] = status[i];
        }
        return;
    }
    if (cnt >= ans1 || limit < 0) return;
    while (remain.b > remain.a * now && now <= 10000000) now++;
    while (remain.b * limit > remain.a * now && now <= 10000000) {
        status[cnt] = now;
        dfs(remain - fraction(1, now), cnt + 1, limit - 1, now + 1);
        now++;
    }
}

signed main()
{
    fraction st;
    read >> st.a >> st.b;
    int lim = 1;
    while (ans1 == 1e9) {
        // printf("lim = %d\n", lim);
        dfs(st, 1, lim, 1);
        lim++;
    }
    for (int i = 1; i <= ans1; i++) 
        write << ans[i] << " \n"[i == ans1];
    return 0;
}