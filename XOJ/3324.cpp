//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Stammering Aliens 口吃的外星人.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-15.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// #define int int64

const int N = 1e6 + 7;
const int K = 133, P = 998244353;

int n, m;
char S[N];
int Hash[N], power[N];
int l1, r1, l2, r2;

inline int GetHash(int l, int r) {
    return (Hash[r] - (int64)Hash[l - 1] * power[r - l + 1] % P + P) % P;
}

unordered_map<int, int> occur;

int find_rightest(int len) {
    occur.clear();
    int right_most = 0;
    for (int i = 1; i <= n - len + 1; i++) {
        int &cnt = ++occur[GetHash(i, i + len - 1)];
        if (cnt >= m) right_most = i;
    } 
    return right_most;
}

signed main() {
    power[0] = 1;
    for (int i = 1; i <= 40001; i++) {
        power[i] = (int64)power[i - 1] * K % P;
    }
    while (true) {
        scanf("%d", &m);
        if (!m) return 0;
        scanf("%s", S + 1);
        n = strlen(S + 1);
        for (int i = 1; i <= n; i++) {
            Hash[i] = ((int64)Hash[i - 1] * K % P + (S[i] ^ 96)) % P;
        }
        register int l = 1, r = n, mid, ans = -1, occur_most;
        while (l <= r) {
            mid = (l + r) >> 1;
            occur.clear();
            occur_most = 0;
            for (int i = 0; i < n - mid + 1; i++) {
                occur_most = max(occur_most, ++occur[(Hash[i + mid] - (int64)Hash[i] * power[mid] % P + P) % P]);
                if (occur_most >= m) {
                    l = mid + 1; 
                    ans = mid;
                    break;
                }
            } 
            if (l <= mid) r = mid - 1;
        }
        if (!~ans) puts("none");
        else {
            write(ans, 32);
            write(find_rightest(ans) - 1, 10);
        }
    }
    return 0;
}

// Create File Date : 2020-06-15