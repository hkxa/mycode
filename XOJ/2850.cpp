//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Knight Moves.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-06.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

struct Reader {
    char endch;
    Reader() { endch = '\0'; }
    template <typename Int>
    Reader& operator >> (Int &i) {
        i = baseFastio::read<Int>(endch);
        return *this;
    }
    template <typename Int>
    inline Int get_int() {
        return baseFastio::read<Int>();
    }
    inline char get_nxt() {
        return endch;
    }
} read;

struct Writer {
    Writer& operator << (const char *ch) {
        // char *p = ch;
        while (*ch) putchar(*(ch++));
        return *this;
    }
    Writer& operator << (const char ch) {
        putchar(ch);
        return *this;
    }
    template <typename Int>
    Writer& operator << (const Int i) {
        baseFastio::write(i);
        return *this;
    }
} write;

// #define int int64

const int dx[8] = {-2, -2, -1, -1, 1, 1, 2, 2};
const int dy[8] = {-1, 1, -2, 2, -2, 2, -1, 1};
int s, t;
int num, x, y, mnum, mx, my;
int cost[64];

bool getPos(int &sav) {
    static char buf[3];
    if (scanf("%s", buf) == EOF) return false;
    sav = (buf[0] - 'a') << 3 | (buf[1] - '1');
    return true;
}

int bfs() {
    queue<int> q;
    q.push(s);
    memset(cost, -1, sizeof(cost));
    cost[s] = 0;
    while (!q.empty()) {
        num = q.front();
        q.pop();
        // write << "cost[" << (char)('a' + (num >> 3)) << (char)('0' | (num & 7))
        //       << "] = " << cost[num] << '\n';
        if (num == t) break;
        x = num >> 3; y = num & 7;
        for (int op = 0; op < 8; op++) {
            mx = x + dx[op];
            my = y + dy[op];
            mnum = mx << 3 | my;
            if (mx < 0 || mx > 7 || my < 0 || my > 7 || ~cost[mnum]) continue;
            cost[mnum] = cost[num] + 1;
            q.push(mnum);
        }
    }
    return cost[t];
}

signed main()
{
    while (getPos(s) && getPos(t)) {
        write << "To get from " << (char)('a' + (s >> 3)) << (char)('1' + (s & 7))
              << " to " << (char)('a' + (t >> 3)) << (char)('1' + (t & 7)) 
              << " takes " << bfs() << " knight moves.\n";
    }
    return 0;
}