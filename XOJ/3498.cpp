/*************************************
 * @problem:      股票交易（trade）.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-25.
 * @language:     C++.
 * @fastio_ver:   20200820.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore space, \t, \tail, \n
            ch = getchar();
            while (ch != ' ' && ch != '\t' && ch != '\r' && ch != '\n') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            Int flag = 1;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = -1, endch = getchar();
            i = endch & 15;
            while (isdigit(endch = getchar())) i = (i << 3) + (i << 1) + (endch & 15);
            if (flag == -1) i = -i;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            Int flag = 1;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = -1, endch = getchar();
            Int i = endch & 15;
            while (isdigit(endch = getchar())) i = (i << 3) + (i << 1) + (endch & 15);
            if (flag == -1) i = -i;
            return i;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64
// #define int128 int64
// #define int128 __int128

const int N = 2e3 + 7;

int n, m, ap, bp, as, bs, w, f[N][N], head, tail, q[N];
int ans = 0;

int main(){
    read >> n >> m >> w;
    memset(f, 0xcf, sizeof(f)); 
    for (int i = 1; i <= n; i++) {
        read >> ap >> bp >> as >> bs;
        // printf("i = %d (%d, %d, %d, %d)\n", i, ap, bp, as, bs);
        for (int j = 0; j <= as; j++)
            f[i][j] = -1 * j * ap; 
        for (int j = 0; j <= m; j++)
            f[i][j] = max(f[i][j], f[i - 1][j]); 
        if (i <= w) continue; 
        // printf("**\n");
        head = 1, tail = 0; 
        for (int j = 0; j <= m; j++) {
            // printf("j : %d/%d\n", j, m);
            while (head <= tail && q[head] < j - as) ++head;
            while (head <= tail && f[i - w - 1][q[tail]] + q[tail] * ap <= f[i - w - 1][j] + j * ap) --tail;
            q[++tail] = j; 
            if (head <= tail) f[i][j] = max(f[i][j], f[i - w - 1][q[head]] + q[head] * ap - j * ap);    
        }
        // printf("**\n");
        head = 1, tail = 0; 
        for (int j = m; j >= 0; j--){
            while (head <= tail && q[head] > j + bs) ++head;
            while (head <= tail && f[i - w - 1][q[tail]] + q[tail] * bp <= f[i - w - 1][j] + j * bp) --tail;
            q[++tail] = j; 
            if (head <= tail)f[i][j] = max(f[i][j], f[i - w - 1][q[head]] + q[head] * bp - j * bp); 
        }
    }
    for (int i = 0; i <= m; i++) ans = max(ans, f[n][i]); 
    write << ans << '\n';
    return 0;
}