//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      4287: Tree.
 * @user_name:    brealid.
 * @time:         2020-06-07.
 * @language:     C++.
 * @upload_place: XOJ.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 2147483647;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

const int N = 5e5 + 7, M = 1e6 + 7;

int n, k, m, need, fa[N];
int find(int x) { return fa[x] < 0 ? x : fa[x] = find(fa[x]); }
bool connect(int x, int y) {
    int fx = find(x), fy = find(y);
    if (fx != fy) {
        if (fa[fx] < fa[fy]) {
            fa[fy] = fx;
        } else if (fa[fx] > fa[fy]) {
            fa[fx] = fy;
        } else {
            fa[fy] = fx;
            fa[fx]--;
        }
        return true;
    }
    return false;
}

struct road {
    int u, v, w, col;
    bool operator < (const road &other) const {
        return col ^ other.col ? col < other.col : w < other.w;
    }
} G[M]; 

int check(int dif) {
    memarr(n, -1, fa);
    int rem = n - 1, White = 0;
    // int i = 0, j = k;
    // while (i < k && j < m) 
    //     if (G[i].w + dif <= G[j].w) (White += connect(G[i].u, G[i].v)), i++;
    //     else connect(G[j].u, G[j].v), j++;
    // while (i < k) White += connect(G[i].u, G[i].v), i++;
    for (int i = 0, j = k, e; rem && (i < k || j < m);) {
        if (i >= k) e = j++;
        else if (j >= m) e = i++;
        else if (G[i].w + dif > G[j].w) e = j++;
        else e = i++;
        if (connect(G[e].u, G[e].v)) {
            rem--;
            if (!G[e].col) White++;
        }
    }
    // printf("Add %d to all white_edge, than used %d white_edge(s)\n", dif, White);
    return White;
}

int solve(int dif) {
    memarr(n, -1, fa);
    // int rem = 0, ans = 0;
    // int i = 0, j = k;
    // while (i < k && j < m) 
    //     if (G[i].w + dif <= G[j].w) (ans += (G[i].w + dif) * connect(G[i].u, G[i].v)), i++;
    //     else (ans += G[j].w * connect(G[j].u, G[j].v)), j++;
    // while (i < k) (ans += (G[i].w + dif) * connect(G[i].u, G[i].v)), i++;
    // while (j < m) (ans += G[j].w * connect(G[j].u, G[j].v)), j++;
    int rem = n - 1, ans = 0;
    for (int i = 0, j = k, e; (i < k || j < m);) {
        if (i >= k) e = j++;
        else if (j >= m) e = i++;
        else if (G[i].w + dif > G[j].w) e = j++;
        else e = i++;
        if (connect(G[e].u, G[e].v)) {
            rem--;
            ans += G[e].w + !G[e].col * dif;
        }
    }
    // printf("solve(dif = %d), ans = %d\n", dif, rem ? 0x3f3f3f3f : ans - dif * need);
    return rem ? 0x3f3f3f3f : ans - dif * need;
}

signed main() {
    n = read<int>();
    m = read<int>();
    need = read<int>();
    for (int i = 0; i < m; i++) {
        G[i].u = read<int>();
        G[i].v = read<int>();
        G[i].w = read<int>();
        G[i].col = read<int>();
        if (!G[i].col) k++;
    }
    sort(G, G + m);
    // for (int i = 0; i < m; i++) 
    //     printf("{%d, %d, %d, %d}\n", G[i].u, G[i].v, G[i].w, G[i].col);
    int l = -100, r = 100, mid, ans;
    while (l <= r) {
        mid = (l + r) >> 1;
        if (check(mid) < need) r = mid - 1;
        else l = mid + 1, ans = mid;
    }
    int t = ans, mn = solve(ans);
    while (t > -100 && check(--t) == need) mn = min(solve(t), mn);
    t = ans;
    while (t < +100 && check(++t) == need) mn = min(solve(t), mn);
    write(mn, 10);
    return 0;
}

// Create File Date : 2020-06-07