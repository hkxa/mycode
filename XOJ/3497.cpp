/*************************************
 * @problem:      Estimation.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-25.
 * @language:     C++.
 * @fastio_ver:   20200820.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore space, \t, \r, \n
            ch = getchar();
            while (ch != ' ' && ch != '\t' && ch != '\r' && ch != '\n') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            Int flag = 1;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = -1, endch = getchar();
            i = endch & 15;
            while (isdigit(endch = getchar())) i = (i << 3) + (i << 1) + (endch & 15);
            if (flag == -1) i = -i;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            Int flag = 1;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = -1, endch = getchar();
            Int i = endch & 15;
            while (isdigit(endch = getchar())) i = (i << 3) + (i << 1) + (endch & 15);
            if (flag == -1) i = -i;
            return i;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64
// #define int128 int64
// #define int128 __int128

const int N = 2000 + 7;

int n, k;
int a[N];
int64 f[N][N];
int64 g[N][N];

signed main() {
    while (true) {
        read >> n >> k;
        if (!n && !k) return 0;
        for (int i = 1; i <= n; i++) read >> a[i];
        for (int i = 1; i <= n; i++) {
            priority_queue<int, vector<int>, less<int> > mx_heap;
            priority_queue<int, vector<int>, greater<int> > mn_heap;
            mx_heap.push(a[i]);
            g[i][i] = 0;
            int64 sum1 = a[i], sum2 = 0;
            for (int j = i + 1; j <= n; j++) {
                if (a[j] <= mx_heap.top()) {
                    if (mx_heap.size() <= mn_heap.size()) {
                        mx_heap.push(a[j]), sum1 += a[j];
                    } else {
                        mn_heap.push(mx_heap.top()), sum2 += mx_heap.top();
                        sum1 -= mx_heap.top(), mx_heap.pop();
                        mx_heap.push(a[j]), sum1 += a[j];
                    }
                } else {
                    if (mn_heap.size() && mn_heap.top() > a[j] && mx_heap.size() <= mn_heap.size())
                        mx_heap.push(a[j]), sum1 += a[j];
                    else if (mx_heap.size() <= mn_heap.size()) {
                        mx_heap.push(mn_heap.top()), sum1 += mn_heap.top();
                        sum2 -= mn_heap.top(), mn_heap.pop();
                        mn_heap.push(a[j]), sum2 += a[j];
                    } else {
                        mn_heap.push(a[j]), sum2 += a[j];
                    }
                }
                g[i][j] = (mx_heap.top() * mx_heap.size() - sum1) + (sum2 - mx_heap.top() * mn_heap.size());
            }
            if (i == 1) {
                for (int j = 1; j <= n; j++) f[j][1] = g[1][j];
            } else {
                for (int j = 2; j <= k; j++) {
                    f[i][j] = 1e18;
                    for (int p = 1; p <= i; p++)
                        f[i][j] = min(f[i][j], f[p][j - 1] + g[p + 1][i]);
                }
            }
        }
        write << f[n][k] << '\n';
    }
    return 0;
}