// 1635-checker

#define _USE_MATH_DEFINES
#include <stdio.h>
#include <string.h>
#include <iostream>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

int T, n, top = 0;
char s[3007];

signed main() {
    T = read<int>();
    for (int Case = 1; Case <= T; Case++) {
        scanf("%s", s);
        n = strlen(s);
        for (int i = 0; i < n; i++) {
            if (s[i] & 1) {
                top--;
                if (top < 0) {
                    printf("Data Error Case #%d [code = %d]\n", Case, 1);
                    return 0;
                }
            } else {
                top++;
            }
        }
        if (top > 0) {
            printf("Data Error Case #%d [code = %d]\n", Case, 2);
            return 0;
        }
        scanf("%s", s);
        n = strlen(s);
        for (int i = 0; i < n; i++) {
            if (s[i] & 1) {
                top--;
                if (top < 0) {
                    printf("Data Error Case #%d [code = %d]\n", Case, 3);
                    return 0;
                }
            } else {
                top++;
            }
        }
        if (top > 0) {
            printf("Data Error Case #%d [code = %d]\n", Case, 4);
            return 0;
        }
    }
    return 0;
}