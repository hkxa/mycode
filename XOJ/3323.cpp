//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Life Forms.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-15.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;
typedef long long int64;

// #define int int64

const int N = 1000 + 7, M = 3000 + 7;
const int K = 133, P = 998244353;

int n, limit;
string s[N];
int Hash[N][M], power[M];

inline int GetHash(int str_id, int l, int r) {
    return (Hash[str_id][r] - (int64)Hash[str_id][l - 1] * power[r - l + 1] % P + P) % P;
}

unordered_set<int> vis;
unordered_map<int, int> occur;
typedef unordered_set<int>::iterator u_s_it;

int check(int len) {
    occur.clear();
    for (int i = 1; i <= n; i++) {
        vis.clear();
        // printf("pass A : ");
        for (size_t j = 0; j + len - 1 < s[i].size(); j++) {
            vis.insert(GetHash(i, j + 1, j + len));
        }
        // printf("true\n");
        // printf("pass B : ");
        for (u_s_it it = vis.begin(); it != vis.end(); it++) {
            if (++occur[*it] >= limit) return true;
        }
        // printf("true\n");
    }
    return false;
}

vector<string> answer_list;
int find_answer(int len) {
    occur.clear();
    for (int i = 1; i <= n; i++) {
        vis.clear();
        for (size_t j = 0; j < s[i].length() - len + 1; j++) {
            vis.insert(GetHash(i, j + 1, j + len));
        }
        for (u_s_it it = vis.begin(); it != vis.end(); it++) {
            ++occur[*it];
        }
    }
    for (int i = 1; i <= n; i++) {
        for (size_t j = 0; j < s[i].length() - len + 1; j++) {
            int &cnt = occur[GetHash(i, j + 1, j + len)];
            if (cnt >= limit) {
                answer_list.push_back(s[i].substr(j, len));
                occur[GetHash(i, j + 1, j + len)] = 0;
            }
        }
    }
    return false;
}

signed main() {
    ios::sync_with_stdio(false);
    power[0] = 1;
    for (int i = 1; i <= 3004; i++) {
        power[i] = (int64)power[i - 1] * K % P;
    }
    bool is_the_first_case = 1;
    while (true) {
        cin >> n;
        if (!n) return 0;
        if (is_the_first_case) is_the_first_case = 0;
        else puts("");
        limit = (n >> 1) + 1;
        size_t max_length = 0;
        for (int i = 1; i <= n; i++) {
            cin >> s[i];
            max_length = max(max_length, s[i].length());
            for (size_t j = 0; j < s[i].length(); j++) {
                Hash[i][j + 1] = ((int64)Hash[i][j] * K % P + (s[i][j] ^ 96)) % P;
            }
        }
        int l = 1, r = max_length, mid, ans = -1;
        fprintf(stderr, "n = %d, [l, r] = [%d, %d]\n", n, l, r);
        while (l <= r) {
            mid = (l + r) >> 1;
            if (check(mid)) l = mid + 1, ans = mid;
            else r = mid - 1;
        }
        if (!~ans) puts("?");
        else {
            answer_list.clear();
            find_answer(ans);
            sort(answer_list.begin(), answer_list.end());
            for (size_t i = 0; i < answer_list.size(); i++)
                puts(answer_list[i].c_str());
        }
        fprintf(stderr, "solved : %d\n", ans);
    }
    return 0;
}

// Create File Date : 2020-06-15