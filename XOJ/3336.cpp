/*************************************
 * @problem:      Strange Way to Express Integers 表示整数的奇怪方法.
 * @user_name:    brealid.
 * @time:         2020-11-07.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

namespace Euclid_Gcd {
    int64 result_x, result_y;
    int64 gcd(int64 n, int64 m) {
        return !m ? n : gcd(m, n % m);
    }
    void exgcd(int64 n, int64 m) { // resolve x*n + y*m = gcd(n, m)
        if (!m) return void((result_x = 1, result_y = 0));
        exgcd(m, n % m);
        int64 tmp_y = result_y;
        result_y = result_x - n / m * result_y;
        result_x = tmp_y;
    }
}

int64 exCRT(int n, int64 *a, int64 *b) {
    int64 v = a[1], m = b[1];
    for (int i = 2; i <= n; ++i) {
        int64 d = Euclid_Gcd::gcd(m, b[i]), dif = v - a[i];
        if (dif % d) return -1;
        Euclid_Gcd::exgcd(m / d, b[i] / d);
        v -= (__int128)Euclid_Gcd::result_x * (dif / d) * m % (m * (b[i] / d));
        m *= (b[i] / d);
        v = (v % m + m) % m;
    }
    return v;
}

signed main() {
    const int N = 1e5 + 7;
    int n;
    int64 a[N], b[N];
    read >> n;
    for (int i = 1; i <= n; ++i) read >> b[i] >> a[i];
    write << exCRT(n, a, b) << '\n';
    return 0;
}