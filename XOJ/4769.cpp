/*************************************
 * @problem:      战略游戏.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-20.
 * @language:     C++.
 * @fastio_ver:   20200820.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore space, \t, \r, \n
            ch = getchar();
            while (ch != ' ' && ch != '\t' && ch != '\r' && ch != '\n') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            Int flag = 1;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = -1, endch = getchar();
            i = endch & 15;
            while (isdigit(endch = getchar())) i = (i << 3) + (i << 1) + (endch & 15);
            if (flag == -1) i = -i;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            Int flag = 1;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = -1, endch = getchar();
            Int i = endch & 15;
            while (isdigit(endch = getchar())) i = (i << 3) + (i << 1) + (endch & 15);
            if (flag == -1) i = -i;
            return i;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64
// #define int128 int64
// #define int128 __int128

int n;
struct edge {
    int next, to;
} e[4000];
int head[4000], tot, dp[4000][2], ind[4000];
void add(int x, int y) {
    e[++tot].next = head[x];
    head[x] = tot;
    e[tot].to = y;
}
void dfs(int x) {
    dp[x][1] = 1;
    for (int i = head[x]; i; i = e[i].next) {
        int v = e[i].to;
        dfs(v);
        dp[x][0] += dp[v][1];
        dp[x][1] += min(dp[v][0], dp[v][1]);
    }
}
int main() {
    while (scanf("%d", &n) == 1) {
        memset(dp, 0, sizeof(dp));
        memset(head, 0, sizeof(head));
        memset(ind, 0, sizeof(ind));
        tot = 0;
        for (int j = 1; j <= n; j++) {
            int a, b;
            read >> a >> b;
            for (int i = 1, c; i <= b; i++) {
                read >> c;
                ind[c]++;
                add(a, c);
            }
        }
        int rt;
        for (int i = 0; i <= n; i++)
            if (!ind[i]) {
                rt = i;
                break;
            }
        dfs(rt);
        write << min(dp[rt][1], dp[rt][0]) << '\n';
    }
}