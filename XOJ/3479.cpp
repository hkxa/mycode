//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      3479: Count The Repetitions.
 * @user_name:    zhaoyi20(brealid).
 * @time:         2020-05-28.
 * @language:     C++.
 * @upload_place: XOJ.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// #define int int64
const int N = 103;
char s1[N], s2[N];
int n1, n2, l1, l2;
pair<int, int> wh[N];

signed main()
{
    while (scanf("%s%d%s%d", s1 + 1, &n1, s2 + 1, &n2) != EOF) {
        l1 = strlen(s1 + 1);
        l2 = strlen(s2 + 1);
        // int m = l2 * n2 / l1 / n1;
        // if (m == 1) {
        //     puts("1");
        //     continue;
        // }
        memset(wh, 0, sizeof(wh));
        int cnt = 0, jcost = 0, j = 1;
        while (jcost <= n2) {
            for (int i = 1; i <= l1; i++) {
                while (s1[i] != s2[j]) {
                    if (jcost > n2) {
                        write(cnt / n1, 10);
                        goto fail;
                    }
                    j++;
                    if (j == 2) {
                        jcost++;
                    }
                    if (j > l2) {
                        j = 1;
                    }
                }
                j++;
                if (j == 2) {
                    jcost++;
                }
                if (j > l2) {
                    j = 1;
                }
            }
            if (jcost <= n2) cnt++;
            // cnt++;
            if (wh[j].first) break;
            else wh[j] = make_pair(cnt, jcost);
            // printf("wh[%d] = {%d, %d}\n", j, cnt, jcost);
        }
        // printf("cnt = %d, jcost = %d\n", cnt, jcost);
        // printf("wh[j = %d] = {%d, %d}\n", j, wh[j].first, wh[j].second);
        if (jcost <= n2) {
            //            s1_times      s2_times
            // last rec   wh[j].first   wh[j].second
            // this rec   cnt           jcost
            // *when pos j
            int fly = (n2 - jcost) / (jcost - wh[j].second);
            jcost += fly * (jcost - wh[j].second);
            cnt += fly * (cnt - wh[j].first);
        }
        // printf("cnt = %d, jcost = %d\n", cnt, jcost);
        while (jcost <= n2) {
            for (int i = 1; i <= l1; i++) {
                while (s1[i] != s2[j]) {
                    j++;
                    if (j == 2) {
                        jcost++;
                    }
                    if (j > l2) {
                        j = 1;
                    }
                }
                j++;
                if (j == 2) {
                    jcost++;
                }
                if (j > l2) {
                    j = 1;
                }
            }
            if (jcost <= n2) cnt++;
            // cnt++;
        }
        // printf("cnt = %d, jcost = %d\n", cnt, jcost);
        write(cnt / n1, 10);
        fail:;
    }
    return 0;
}

/*
aabaca 1
abc 11

acbcab 1
abc 11

acbcab 1
abcabcabc 3
*/