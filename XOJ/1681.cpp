//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      区间种西瓜（Intervals）.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-14.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 988244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

const int N = 50000 + 7;

int n;
vector<pair<int, int> > G[N];
int S[N];
bool inQ[N];

void SPFA_GetLongestRoad() {
    queue<int> q;
    q.push(0);
    memarr(50000, -n, S);
    memarr(50000, 0, inQ);
    S[0] = 0;
    inQ[0] = 1;
    while (!q.empty()) {
        int u = q.front(); q.pop();
        for (size_t i = 0; i < G[u].size(); i++) {
            int v = G[u][i].first, w = G[u][i].second;
            if (S[v] < S[u] + w) {
                S[v] = S[u] + w;
                if (!inQ[v]) {
                    q.push(v);
                    inQ[v] = true;
                }
            }
        }
        inQ[u] = 0;
    }
}

signed main() {
    n = read<int>();
    for (int i = 1, a, b, v; i <= n; i++) {
        a = read<int>();
        b = read<int>();
        v = read<int>();
        // S[b] - S[a - 1] >= v
        G[a - 1].push_back(make_pair(b, v));
    }
    for (int i = 1; i <= 50000; i++) {
        // S[i] - S[i - 1] >= 0
        G[i - 1].push_back(make_pair(i, 0));
        // S[i] - S[i - 1] <= 1
        // aka S[i - 1] - S[i] >= -1
        G[i].push_back(make_pair(i - 1, -1));
    }
    SPFA_GetLongestRoad();
    write(S[50000], 10);
    return 0;
}

// Create File Date : 2020-06-14