//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      The Rotation Game 拉链游戏.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-07.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

struct Reader {
    char endch;
    Reader() { endch = '\0'; }
    template <typename Int>
    Reader& operator >> (Int &i) {
        i = baseFastio::read<Int>(endch);
        return *this;
    }
    template <typename Int>
    inline Int get_int() {
        return baseFastio::read<Int>();
    }
    inline char get_nxt() {
        return endch;
    }
} read;

struct Writer {
    Writer& operator << (const char *ch) {
        // char *p = ch;
        while (*ch) putchar(*(ch++));
        return *this;
    }
    Writer& operator << (const char ch) {
        putchar(ch);
        return *this;
    }
    template <typename Int>
    Writer& operator << (const Int i) {
        baseFastio::write(i);
        return *this;
    }
} write;

// #define int int64

#define get(val, pos) ((val >> (pos << 1)) & 3)
#define set(val, pos, newer) (val = (((val & ~(3LL << (pos << 1))) | (newer << (pos << 1)))))
#define copy(newer, orig) set(a, newer, get(a, orig))
#define swap(num, id1, id2) int64 t = get(num, id1); set(num, id1, get(num, id2)); set(num, id2, t);
#define pull(b, c, d, e, f, g, h) int64 t = get(a, b); copy(b, c); copy(c, d); copy(d, e); copy(e, f); copy(f, g); copy(g, h); set(a, h, t);

struct state {
    int64 a;
    void A() { pull(1, 3, 7, 12, 16, 21, 23); }
    void B() { pull(2, 4, 9, 13, 18, 22, 24); }
    void C() { pull(11, 10, 9, 8, 7, 6, 5); }
    void D() { pull(20, 19, 18, 17, 16, 15, 14); }
    void E() { pull(24, 22, 18, 13, 9, 4, 2); }
    void F() { pull(23, 21, 16, 12, 7, 3, 1); }
    void G() { pull(14, 15, 16, 17, 18, 19, 20); }
    void H() { pull(5, 6, 7, 8, 9, 10, 11); }
    bool operator < (const state &b) const {
        return a < b.a;
    }
} s;

char ways[107];

map<state, char> f; 
queue<state> q;

/*
       1     2
       3     4
 5  6  7  8  9 10 11
      12    13 
14 15 16 17 18 19 20
      21    22
      23    24
*/

int test(const state &now) 
{   
    static int Cnt[4];
    Cnt[1] = Cnt[2] = Cnt[3] = 0;
    Cnt[get(now.a, 7)]++;
    Cnt[get(now.a, 8)]++;
    Cnt[get(now.a, 9)]++;
    Cnt[get(now.a, 12)]++;
    Cnt[get(now.a, 13)]++;
    Cnt[get(now.a, 16)]++;
    Cnt[get(now.a, 17)]++; 
    Cnt[get(now.a, 18)]++;
    return 8 - max(Cnt[1], max(Cnt[2], Cnt[3]));
}

bool ida_star(int g, int lim) {
    if (g + test(s) > lim) return 0;
    if (!test(s)) {
        for (int i = 0; i < g; i++)
            write << ways[i];
        write << '\n' << get(s.a, 7) << '\n';
        return true;
    }
    state save = s;
    ways[g] = 'A'; s = save; s.A(); if (ida_star(g + 1, lim)) return true;
    ways[g] = 'B'; s = save; s.B(); if (ida_star(g + 1, lim)) return true;
    ways[g] = 'C'; s = save; s.C(); if (ida_star(g + 1, lim)) return true;
    ways[g] = 'D'; s = save; s.D(); if (ida_star(g + 1, lim)) return true;
    ways[g] = 'E'; s = save; s.E(); if (ida_star(g + 1, lim)) return true;
    ways[g] = 'F'; s = save; s.F(); if (ida_star(g + 1, lim)) return true;
    ways[g] = 'G'; s = save; s.G(); if (ida_star(g + 1, lim)) return true;
    ways[g] = 'H'; s = save; s.H(); if (ida_star(g + 1, lim)) return true;
    s = save;
    return false;
}

signed main()
{
    while (true) {
        s.a = 0;
        set(s.a, 1, read.get_int<int64>());
        if (!get(s.a, 1)) break;
        for (int i = 2; i <= 24; i++) set(s.a, i, read.get_int<int64>());
        if (!test(s)) {
            write << "No moves needed\n" << get(s.a, 7) << '\n';
            continue;
        }
        for (int dep = test(s); ; dep++)
            if (ida_star(0, dep)) break;
    }
    return 0;
}