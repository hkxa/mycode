#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;
const int zt[60] = { 0,   1,   2,   4,   8,   9,   16,  17,  18,  32,  33,  34,  36,  64,  65,
                     66,  68,  72,  73,  128, 129, 130, 132, 136, 137, 144, 145, 146, 256, 257,
                     258, 260, 264, 265, 272, 273, 274, 288, 289, 290, 292, 512, 513, 514, 516,
                     520, 521, 528, 529, 530, 544, 545, 546, 548, 576, 577, 578, 580, 584, 585 };
int mp[101];
int dp[101][61][61];
inline int cnt(int n) {
    int ans = 0;
    while (n > 0) {
        ans += n & 1;
        n >>= 1;
    }
    return ans;
}
int main() {
    int n, m, i, j, k, l, ans = 0;
    char c;
    scanf("%d%d", &n, &m);
    for (i = 1; i <= n; i++)
        for (j = 0; j < m; j++) {
            mp[i] <<= 1;
            while (!isupper(c = getchar()));
            if (c == 'H')
                mp[i] |= 1;
        }
    for (i = 1; i <= n; i++)
        for (j = 0; j < 60 && zt[j] < 1 << m; j++)
            if (!(mp[i] & zt[j]))
                for (k = 0; k < 60 && zt[k] < 1 << m; k++)
                    if (!(zt[j] & zt[k]) && !(mp[i - 1] & zt[k]))
                        for (l = 0; l < 60 && zt[l] < 1 << m; l++)
                            if (!(mp[i - 2] & zt[l]) && !(zt[j] & zt[l]) && !(zt[k] & zt[l]) &&
                                dp[i - 1][k][l] + cnt(zt[j]) > dp[i][j][k])
                                dp[i][j][k] = dp[i - 1][k][l] + cnt(zt[j]);
    for (i = 0; i < 60 && zt[i] < 1 << m; i++)
        for (j = 0; j < 60 && zt[j] < 1 << m; j++)
            if (dp[n][i][j] > ans)
                ans = dp[n][i][j];
    printf("%d\n", ans);
    return 0;
}