//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      4694: Social Distancing.
 * @user_name:    zhaoyi20(brealid).
 * @time:         2020-05-31.
 * @language:     C++.
 * @upload_place: XOJ.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
       return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
       return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// #define int int64

const int N = 100007;

int n, m;
struct seq {
    int64 l, r;
    bool operator < (const seq &c) const {
        return l < c.l;
    }
} a[N];

bool check(int64 d) {
    int i = 1, cow = 1;
    int64 pos = a[1].l;
    while (cow < n) {
        pos += d;
        while (i <= m && pos > a[i].r) i++;
        if (i > m) return false;
        pos = max(pos, a[i].l);
        cow++;
    }
    return true;
}

signed main() {
    n = read<int>();
    m = read<int>();
    for (int i = 1; i <= m; i++) {
        a[i].l = read<int64>();
        a[i].r = read<int64>();
    }
    sort(a + 1, a + m + 1);
    int64 l = 1, r = 1e18, mid, ans;
    while (l <= r) {
        mid = (l + r) >> 1;
        if (check(mid)) l = mid + 1, ans = mid;
        else r = mid - 1;
    }
    write(ans, 10);
    return 0;
}

// Create File Date : 2020-05-31