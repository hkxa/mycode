#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

#define MaxN (100000 + 10)

struct TreeArray {
	int n;
	long long c[MaxN];
	long long res;
	
	TreeArray(void);
	inline void init(void);
	inline long long lowbit(long long);
	inline void update(long long, long long);
	inline long long getSum(long long);
} tree;

TreeArray::TreeArray()
{
	memset(c, 0, sizeof(c));
}
	
inline long long TreeArray::lowbit(long long a)
{
	return a & (-a);
}

inline void TreeArray::init()
{
	for (int i = 1; i <= n; i++) {
		res = read<int>();
		update(i, res);
		update(i + 1, -res);
	}
}

inline void TreeArray::update(long long x, long long v)
{
	while (x < MaxN) c[x] += v, x += lowbit(x);
}

inline long long TreeArray::getSum(long long x)
{
	res = 0;
	while (x > 0) res += c[x], x -= lowbit(x);
	return res;
}

int main()
{
	tree.n = read<int>();
    long long ans = 0;
	for (register int i = 0; i < tree.n; i++) {
        int ai = read<int>();
        ans += i - tree.getSum(ai + 1);
        tree.update(ai + 1, 1);
	}
    cout << ans << endl;
    return 0;
} 
