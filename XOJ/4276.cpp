//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Keywords Search.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-05.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

namespace Jalg {
    const int charSize = 26;
    const char baseChar = 'a';

    struct ACtrie {
        ACtrie *fail, *son[charSize];
        int count;
        bool found;
        int fa_sonId; // 在爸爸那里自己的ID，即本节点代表的字母
        ACtrie() : fail(NULL), count(0), found(0), fa_sonId('~' - baseChar)
        {
            memset(son, 0, sizeof(son));
        }
    };

    ACtrie *root;

    void insert(string s)
    {
        ACtrie *p = root;
        for (string::iterator it = s.begin(); it != s.end(); it++) {
            if (!p->son[*it - baseChar]) {
                p->son[*it - baseChar] = new ACtrie;
                p->son[*it - baseChar]->fa_sonId = *it - baseChar;
            }
            p = p->son[*it - baseChar];
        }
        p->count++;
    }

    void make_fail()
    {
        queue<ACtrie*> q;
        for (int i = 0; i < charSize; i++) {
            if (root->son[i]) {
                q.push(root->son[i]);
                root->son[i]->fail = root;
            }
        }
        while (!q.empty()) {
            ACtrie *p = q.front();
            q.pop();
            for (int i = 0; i < charSize; i++) {
                if (!p->son[i]) continue;
                ACtrie *f = p->fail;
                while (true) {
                    if (f->son[i]) {
                        p->son[i]->fail = f->son[i];
                        break;
                    }
                    if (f == root) {
                        p->son[i]->fail = root;
                        break;
                    }
                    f = f->fail;
                }
                q.push(p->son[i]);
            }
        }
    }

    void build_ACtrie(vector<string> sArr)
    {
        root = new ACtrie;
        for (vector<string>::iterator it = sArr.begin(); it != sArr.end(); it++) {
            insert(*it);
        }
        make_fail();
    }

    int match_task1(string txt) // 有多少个模式串在文本串里出现过（多次出现答案只算1，重复的模式串答案算多个）
    {
        ACtrie *u = root, *v;
        int ans(0);
        for (string::iterator it = txt.begin(); it != txt.end(); it++) {
            if (u->son[*it - baseChar]) u = u->son[*it - baseChar];
            else {
                while (!u->son[*it - baseChar] && u != root) {
                    u = u->fail;
                }
                if (u->son[*it - baseChar]) {
                    u = u->son[*it - baseChar];
                }
            }
            
            v = u;
            while (v && !v->found) {
                ans += v->count;
                v->found = true;
                v = v->fail;
            }
        }
        return ans;
    }
    void removeData(ACtrie *u = root) {
        for (int i = 0; i < 26; i++)
            if (u->son[i]) removeData(u->son[i]);
        delete u;
    }
};

int main()
{
    int T = read.get_int<int>();
    while (T--) {
        int n;
        read >> n;
        string s, txt;
        vector<string> v;
        for (int i = 1; i <= n; i++) {
            cin >> s;
            v.push_back(s);
        }
        Jalg::build_ACtrie(v);
        cin >> txt;
        write << Jalg::match_task1(txt) << '\n';
        Jalg::removeData();
    }
    return 0;
}