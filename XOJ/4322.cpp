//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      网络.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-18.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 998244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? Module(w - P) : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

const int N = 100 + 3;

int n;
char buf[1007], *p, *last;
vector<int> G[N];
int dfn[N], low[N], dft;
int Ans;

void tarjan(int u, int fa) {
    low[u] = dfn[u] = ++dft;
    int cnt = 0;
    // printf("%d by %d\n", u, fa);
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i];
        // if (v == fa) continue;
        if (!dfn[v]) {
            tarjan(v, u);
            low[u] = min(low[u], low[v]);
            if (low[v] >= dfn[u]) cnt++;
        } else {
            low[u] = min(low[u], dfn[v]);
        }
    }
    if (cnt && (u != 1 || cnt > 1)) Ans++;
    // if (cnt && (u != 1 || cnt > 1)) printf("node %d\n", u);
}

signed main() {
    while (n = read<int>()) {
        for (int i = 1; i <= n; i++) {
            G[i].clear();
            dfn[i] = 0;
        }
        int u, v;
        while (u = read<int>()) {
            scanf("%[0-9 ]", p = buf);
            last = 0;
            while (*p) {
                v = strtol(p, &p, 10);
                if (p == last) break;
                last = p;
                G[u].push_back(v);
                G[v].push_back(u);
            }
        }
        dft = Ans = 0;
        tarjan(1, 0);
        // for (int i = 1; i <= n; i++) 
        //     printf("%d : dfn = %d, low = %d\n", i, dfn[i], low[i]);
        write(Ans, 10);
    }
    return 0;
}

// Create File Date : 2020-06-18