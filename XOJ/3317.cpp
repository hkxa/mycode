//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Manacher “马拉车”（最长回文串模板）.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-03.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 2000000 + 7;

namespace against_cpp11 {
    char s[N], tmp[N];
    int p[N];
    int manacher(char *s, int len) {
        int mx = 0, ans = 0, pos = 0;
        for (int i = 1; i <= len; i++) {
            if (mx > i) p[i] = min(mx - i, p[2 * pos - i]);
            else p[i] = 1;
            while (s[i - p[i]] == s[i + p[i]]) p[i]++;
            if (p[i] + i > mx) {
                mx = p[i] + i;
                pos = i;
            }
            ans = max(ans, p[i]);
        }
        return ans - 1;
    }
    signed main() {
        int case_count = 0;
        s[0] = '#';
        while (true) {
            scanf("%s", tmp);
            if (!strcmp(tmp, "END")) return 0;
            int n = strlen(tmp);
            for (int i = 0; i < n; i++) {
                s[i * 2 + 1] = '*';
                s[i * 2 + 2] = tmp[i];
            }
            s[n * 2 + 1] = '*';
            write << "Case " << ++case_count << ": " << manacher(s, n * 2 + 1) << '\n';
        }
        return 0;
    }
}

signed main() { return against_cpp11::main(); }

/**
 * Problem: 3317
 * User: zhaoyi20
 * Language: C++
 * Time: 162 ms
 * Memory: 13736 KB
 */