#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <string.h>
#include <vector>
#include <map>
#include <set>
#define lowbit(a) ((a) & (~a + 1)) // define 快 
#define Clear(a, x) memset(a, x, sizeof(a))
#define get(zt, p) ((1 << p) & zt)
#define set(zta, p) (zt = zt | (1 << p))
#define debug(...) fprintf(stderr, "passing %s in line [%d]: ", __FUNCTION__, __LINE__), fprintf(stderr, __VA_ARGS__);
using namespace std;

namespace against_cpp11 {
    template <typename _TpInt> inline _TpInt read();
    template <typename _TpRealnumber> inline double readr();
    template <typename _TpInt> inline void write(_TpInt x);
    template <typename _TpSwap> inline void swap(_TpSwap &x, _TpSwap &y);
	
#	define Max_N 300007
#	define Max_Log 24
	
	struct Edge {
	    int to, val;
	    int next;
		Edge(int b = 0, int t = 0) : to(b), val(t) {}
	};
	
	int n, m;
	Edge edge[Max_N << 1];
	int head[Max_N] = {0};
	int cnt_edge = 0;
	int cnt_LCA = 0;
	int MaxLength;
	int p[Max_N], dis[Max_N], deep[Max_N], prestar[Max_N], fa[Max_N][Max_Log];
	int f[Max_N], a[Max_N], b[Max_N], l[Max_N], d[Max_N];

	inline void Add_Edge(int u, int v, int val)
	{
	    edge[cnt_edge].val = val;
	    edge[cnt_edge].to = v;
	    edge[cnt_edge].next = head[u];
	    head[u] = cnt_edge++;
	}
	
	void dfs_LCA(int u, int pre)
	{
	    p[++cnt_LCA] = u;
	
	    for (int i = 1; i <= Max_Log; i++) {
	        fa[u][i] = fa[fa[u][i - 1]][i - 1];
	        if(!fa[u][i]) break;
	    }
	
	    for (int i = head[u]; ~i; i = edge[i].next) {
	        int v = edge[i].to;
	        if (pre == v) continue;
	
	        dis[v] = dis[u] + edge[i].val;
	        deep[v] = deep[u] + 1;
	        fa[v][0] = u;
	        prestar[v] = edge[i].val;
	        dfs_LCA(v, u);
	    }
	}
	
	int LCA(int x, int y)
	{
		int tx = x, ty = y;
	    if (deep[x] > deep[y]) swap(x, y);
	    for (int i = Max_Log - 1; i >= 0; i--)
	        if(deep[fa[y][i]] >= deep[x])
	            y = fa[y][i];
	    for (int i = Max_Log - 1; i >= 0; i--)
	        if (fa[y][i] != fa[x][i]) {
	            x = fa[x][i]; 
				y = fa[y][i];
	        }
//		debug("%d, %d\'s LCA = %d\n", tx, ty, x);
	    if (x != y) x = fa[x][0];
//		debug("%d, %d\'s LCA = %d\n", tx, ty, x);
	    return x;
	}
	
	bool check(int k)
	{
	    memset(f, 0, sizeof(f));
	    int cnt = 0;
	
	    for (int i = 1; i <= m; i++) 
		    if (d[i] > k)
		    {
		        f[a[i]]++; 
				f[b[i]]++; 
				f[l[i]] -= 2;
		        cnt++;
		    }
	
	    for (int i = n; i >= 1; i--) {
	        f[fa[p[i]][0]] += f[p[i]];
	        if (prestar[p[i]] >= (MaxLength - k) && f[p[i]] == cnt) 
				return true;
	    }
	    return false;
	}
	
	int Binary_Search(int l, int r)
	{
	    int mid;
	    while (l < r) {
	        mid = (l + r) >> 1;
	        if (check(mid)) r = mid;
	        else l = mid + 1;
	    }
	    return l;
	}
	
	int main() 
	{
		n = read<int>();
		m = read<int>();
	    Clear(head, -1);
		int tmp_Max = 0;
		for (int i = 1; i <= n - 1; i++) {
			int a, b, t;
			a = read<int>();
			b = read<int>();
			t = read<int>();
			Add_Edge(a, b, t);
			Add_Edge(b, a, t);
			tmp_Max = max(tmp_Max, t);
		}
		deep[0] = -1;
		deep[1] = 1; 
		dfs_LCA(1, 0);
	
	    for (int i = 1; i <= m; i++) {
	        a[i] = read<int>();
	        b[i] = read<int>();
	        l[i] = LCA(a[i], b[i]);
	        d[i] = dis[a[i]] + dis[b[i]] - (dis[l[i]] << 1);
			MaxLength = max(MaxLength, d[i]);
	    }
		
	    write(Binary_Search(MaxLength - tmp_Max, MaxLength + 1));
        putchar(10);
	    return 0;
	}
    template <typename _TpInt>
    inline _TpInt read()       
    {
        int flag = 1;
        char c = getchar();
        while ((c > '9' || c < '0') && c != '-') 
            c = getchar();
        if (c == '-') flag = -1, c = getchar();
        _TpInt init = (c & 15);
        while ((c = getchar()) <= '9' && c >= '0') 
            init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename _TpRealnumber>
    inline double readr()       
    {
        int flag = 1;
        char c = getchar();
        while ((c > '9' || c < '0') && c != '-') 
            c = getchar();
        if (c == '-') flag = -1, c = getchar();
        _TpRealnumber init = (c & 15);
        while ((c = getchar()) <= '9' && c >= '0') 
            init = init * 10 + (c & 15);
        if (c != '.') return init * flag;
        _TpRealnumber l = 0.1;
        while ((c = getchar()) <= '9' && c >= '0') 
            init = init + (c & 15) * l, l *= 0.1;
        return init * flag;
    }

    template <typename _TpInt>
    inline void write(_TpInt x)
    {
        if (x < 0) {
            putchar('-');
            x = (~x + 1);
        }
        if (x > 9) write<_TpInt>(x / 10);   
        putchar(x % 10 + '0');
    }

    template <typename _TpSwap>
    inline void swap(_TpSwap &x, _TpSwap &y)
    {
        _TpSwap t = x;
        x = y;
        y = t;
    }
}

int main()
{
    against_cpp11::main();
    return 0;
}