//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      K-th Number.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-03.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

namespace against_cpp11 {
    const int N = 2e5 + 7, SIZ = 1e7 + 7;

    int n, m;
    int a[N], LSH[N];
    int root[N], val[SIZ], l[SIZ], r[SIZ], cnt;

    void build_tree(int u, int ml = 1, int mr = n) {
        if (ml == mr) return;
        int mid = (ml + mr) >> 1;
        build_tree(l[u] = ++cnt, ml, mid);
        build_tree(r[u] = ++cnt, mid + 1, mr);
    }

    void modify_tree(int base, int u, int loc, int diffValue, int ml = 1, int mr = n) {
        if (ml == mr) {
            val[u] = val[base] + diffValue;
            return;
        }
        int mid = (ml + mr) >> 1;
        if (loc <= mid) {
            r[u] = r[base];
            modify_tree(l[base], l[u] = ++cnt, loc, diffValue, ml, mid);
            val[u] = val[l[u]] + val[r[u]];
        } else {
            l[u] = l[base];
            modify_tree(r[base], r[u] = ++cnt, loc, diffValue, mid + 1, mr);
            val[u] = val[l[u]] + val[r[u]];
        }
    }

    inline void modify(int i, int v, int loc, int diffValue) {
        modify_tree(root[v], root[i] = ++cnt, loc, diffValue);
    }

    // Query in Tree{u - v}
    int query_tree(int u, int v, int k, int ml = 1, int mr = n) {
        if (ml == mr) return ml;
        int mid = (ml + mr) >> 1, LefSize = val[l[u]] - val[l[v]];
        if (k <= LefSize) return query_tree(l[u], l[v], k, ml, mid);
        else return query_tree(r[u], r[v], k - LefSize, mid + 1, mr);
    }

    inline int query(int l, int r, int kth) {
        return query_tree(root[r], root[l - 1], kth);
    }

    signed main() {
        read >> n >> m;
        build_tree(root[0] = ++cnt);
        for (int i = 1; i <= n; i++) LSH[i] = a[i] = read.get_int<int>();
        sort(LSH + 1, LSH + n + 1);
        for (int i = 1; i <= n; i++) {
            a[i] = lower_bound(LSH + 1, LSH + n + 1, a[i]) - LSH;
            modify(i, i - 1, a[i], 1);
        }
        // printf("array a : ");
        // for (int i = 1; i <= n; i++) printf("%d%c", a[i], " \n"[i == n]);
        // printf("array LSH : ");
        // for (int i = 1; i <= n; i++) printf("%d%c", LSH[i], " \n"[i == n]);
        for (int i = 1, l, r, k; i <= m; i++) {
            read >> l >> r >> k;
            // printf("DEBUG : original queried value = %d\n", query(l, r, k));
            // printf("Translated using array ~ LSH => ");
            write << LSH[query(l, r, k)] << '\n';
        }
        return 0;
    }
}

signed main() { return against_cpp11::main(); }