//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      [IOI2011]Race.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-02.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// #define int int64

namespace against_cpp11 {
    const int N = 200007;

    int n, k, Q;
    int vis[N];
    int cnt;
    struct Info {
        int dis, cnt;
    } h[N];
    int to[N << 1], val[N << 1], nxt[N << 1], head[N];
    int bucket[1000007], ans = N;

    int siz[N], root, rootMax;
    void basicFindRoot_getSiz(int u) {
        siz[u] = 1;
        vis[u] = true;
        for (int e = head[u]; ~e; e = nxt[e]) {
            if (!vis[to[e]]) {
                basicFindRoot_getSiz(to[e]);
                siz[u] += siz[to[e]];
            }
        }
        vis[u] = false;
    }

    void basicFindRoot_getRoot(int u, int si) {
        int tMax = 1;
        vis[u] = true;
        for (int e = head[u]; ~e; e = nxt[e]) {
            const int &v = to[e];
            if (!vis[v]) {
                basicFindRoot_getRoot(v, si);
                tMax = max(tMax, siz[v]);
            }
        }
        tMax = max(tMax, si - siz[u]);
        if (tMax < rootMax) {
            root = u;
            rootMax = tMax;
        }
        vis[u] = false;
    }

    inline int findRoot(int u) {
        rootMax = n + 1;
        // tc = clock();
        basicFindRoot_getSiz(u);
        // tGS += clock() - tc;
        // tc = clock();
        basicFindRoot_getRoot(u, siz[u]);
        // tGR += clock() - tc;
        return root;
    }

    void getDist(int u, int dis, int ecnt) {
        if (dis > k) return;
        h[cnt++] = (Info){dis, ecnt};
        // if (h[cnt - 1].dis < 0)
        //     printf("Amazing but true : you set dis(j = %d) to negative : %d\n", cnt - 1, h[cnt - 1].dis);
        vis[u] = true;
        for (int e = head[u]; ~e; e = nxt[e]) {
            // if (to[e] > n)
            //     printf("How could it be? I mean, how could the to[e] > n? u = %d, e = %d, to[e] = %d\n", u, e, to[e]);
            if (!vis[to[e]]) getDist(to[e], dis + val[e], ecnt + 1);
        }
        vis[u] = false;
    }

    void calcAnswer(int u) {	
        bucket[0] = 0;
        cnt = 0;
        for (int e = head[u]; ~e; e = nxt[e]) {
            if (!vis[to[e]]) {
                int RecentCnt = cnt;
                // if (to[e] > n)
                //     printf("How could it be? I mean, how could the to[e] > n? u = %d, e = %d, to[e] = %d\n", u, e, to[e]);
                getDist(to[e], val[e], 1);
                // printf("RecentCnt(%d) ~ cnt(%d)\n", RecentCnt, cnt);
                for (int j = RecentCnt; j < cnt; j++) {
                    // if (ans != 5 && bucket[k - h[j].dis] + h[j].cnt == 5) {
                    //     printf("EMM.. So here is it : h[j] = {%d, %d}\n", h[j].dis, h[j].cnt);
                    // }
                    ans = min(ans, bucket[k - h[j].dis] + h[j].cnt);
                }
                for (int j = RecentCnt; j < cnt; j++) 
                    bucket[h[j].dis] = min(bucket[h[j].dis], h[j].cnt);
                // for (int j = RecentCnt; j < cnt; j++) 
                //     printf("bucket[%d] => %d\n", h[j].dis, bucket[h[j].dis]);
            }
        }
        // printf("calcAnswer : %d : ans => %d\n", u, ans);
        for (int i = 0; i < cnt; i++) bucket[h[i].dis] = 0x3fffffff;
    }

    void solve(int u) {
        // printf("solve(%d)\n", u);
        vis[u] = true;
        calcAnswer(u);
        for (int e = head[u]; ~e; e = nxt[e]) {
            if (!vis[to[e]]) solve(findRoot(to[e]));
        }
        vis[u] = false;
    }

    signed main() {
        n = read<int>();
        k = read<int>();
        memset(head, -1, sizeof(int) * (n + 3));
        memset(bucket, 0x3f, sizeof(int) * (k + 3));
        for (int i = 1, u, v, w; i < n; i++) {
            u = read<int>() + 1; // node_id from 0
            v = read<int>() + 1; // node_id from 0
            w = read<int>();
            to[++cnt] = v; val[cnt] = w; nxt[cnt] = head[u]; head[u] = cnt;
            to[++cnt] = u; val[cnt] = w; nxt[cnt] = head[v]; head[v] = cnt;
        }
        solve(findRoot(1));
        if (ans == N) puts("-1");
        else write(ans, 10);
        return 0;
    }
}

signed main() { return against_cpp11::main(); }