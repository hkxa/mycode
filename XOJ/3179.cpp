//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      树上操作.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-01.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

template <typename SegmentTree_ValueType>
class SegmentTree_RangeModifyRangeQuery {
    // Powered by brealid, copyright reserved.
  public:
    typedef SegmentTree_ValueType ValueType;
  private:
    int n;
    ValueType *sum, *lazy;
    void free_space() {
        if (sum) { delete[] sum; sum = NULL; }
        if (lazy) { delete[] lazy; lazy = NULL; }
    }
    void build_tree(int u, int l, int r, const ValueType *w) {
        if (l == r) {
            sum[u] = w[l];
            lazy[u] = 0;
            return;
        }
        int mid = (l + r) >> 1;
        build_tree(u << 1, l, mid, w);
        build_tree(u << 1 | 1, mid + 1, r, w);
        sum[u] = (sum[u << 1] + sum[u << 1 | 1]);
    }
    void build_tree(int u, int l, int r, const ValueType StartValue) {
        if (l == r) {
            sum[u] = StartValue;
            lazy[u] = 0;
            return;
        }
        int mid = (l + r) >> 1;
        build_tree(u << 1, l, mid, StartValue);
        build_tree(u << 1 | 1, mid + 1, r, StartValue);
        sum[u] = (sum[u << 1] + sum[u << 1 | 1]);
    }
    void update_range_value(int u, int l, int r, const int &ml, const int &mr, const ValueType &val) {
        if (l >= ml && r <= mr) {
            sum[u] = (sum[u] + val * (r - l + 1));
            lazy[u] = (lazy[u] + val);
            return;
        }
        int mid = (l + r) >> 1;
        if (lazy[u]) {
            sum[u << 1] = (sum[u << 1] + (int64)lazy[u] * (mid - l + 1));
            sum[u << 1 | 1] = (sum[u << 1 | 1] + (int64)lazy[u] * (r - mid));
            lazy[u << 1] = (lazy[u << 1] + lazy[u]);
            lazy[u << 1 | 1] = (lazy[u << 1 | 1] + lazy[u]);
            lazy[u] = 0;
        }
        if (mid >= ml) update_range_value(u << 1, l, mid, ml, mr, val);
        if (mid < mr) update_range_value(u << 1 | 1, mid + 1, r, ml, mr, val);
        sum[u] = (sum[u << 1] + sum[u << 1 | 1]);
    }
    ValueType query_range_sum(int u, int l, int r, int ml, int mr) {
        if (l >= ml && r <= mr) return sum[u];
        int mid = (l + r) >> 1;
        if (lazy[u]) {
            sum[u << 1] = (sum[u << 1] + (int64)lazy[u] * (mid - l + 1));
            sum[u << 1 | 1] = (sum[u << 1 | 1] + (int64)lazy[u] * (r - mid));
            lazy[u << 1] = (lazy[u << 1] + lazy[u]);
            lazy[u << 1 | 1] = (lazy[u << 1 | 1] + lazy[u]);
            lazy[u] = 0;
        }
        ValueType ret = 0;
        if (mid >= ml) ret += query_range_sum(u << 1, l, mid, ml, mr);
        if (mid < mr) ret += query_range_sum(u << 1 | 1, mid + 1, r, ml, mr);
        return ret;
    }
  public:
    void init_space(int nodes_cnt) {
        n = nodes_cnt;
        sum = new ValueType[n * 4 + 2];
        lazy = new ValueType[n * 4 + 2];
    }
    void clear() {
        for (int i = 0; i < n * 4 + 2; i++) {
            sum[i] = lazy[i] = 0;
        }
    }
    void pretreat() {
        clear();
    }
    void pretreat(ValueType *w) {
        clear();
        build_tree(1, 1, n, w);
    }
    void Modify_Range(int l, int r, ValueType value) {
        // printf("sgt::ModifyRange(%d, %d, %d)\n", l, r, value);
        update_range_value(1, 1, n, l, r, value);
    }
    ValueType Query_Sum(int l, int r) {
        // printf("sgt::QuerySum(%d, %d)\n", l, r);
        return query_range_sum(1, 1, n, l, r);
    }
    SegmentTree_RangeModifyRangeQuery() : n(0), sum(NULL), lazy(NULL) {}
    ~SegmentTree_RangeModifyRangeQuery() { free_space(); }
};

template <typename TreeChainSplit_ValueType>
class TreeChainSplit {
    // Powered by brealid, copyright reserved.
  public:
    typedef TreeChainSplit_ValueType ValueType;
  private:
    int n;
    vector<int> *G;
    int *fa, *dep, *siz, *wson;
    int *dfn, *lst, *beg, *ed, dft;
    SegmentTree_RangeModifyRangeQuery<ValueType> sgt;
    void free_space() {
        if (fa) { delete[] fa; fa = NULL; }
        if (dep) { delete[] dep; dep = NULL; }
        if (siz) { delete[] siz; siz = NULL; }
        if (wson) { delete[] wson; wson = NULL; }
        if (dfn) { delete[] dfn; dfn = NULL; }
        if (lst) { delete[] lst; lst = NULL; }
        if (beg) { delete[] beg; beg = NULL; }
        if (ed) { delete[] ed; ed = NULL; }
    }
    void dfs1(int u, int Father) {
        fa[u] = Father;
        dep[u] = dep[Father] + 1;
        siz[u] = 1;
        for (size_t i = 0; i < G[u].size(); i++) {
            int v = G[u][i];
            if (v != Father) {
                dfs1(v, u);
                siz[u] += siz[v];
                if (siz[v] > siz[wson[u]]) 
                    wson[u] = v;
            }
        }
    }
    int dfs2(int u, int ChainBeg) {
        dfn[u] = ++dft;
        beg[u] = ChainBeg;
        if (wson[u]) {
            ed[u] = dfs2(wson[u], ChainBeg);
            for (size_t i = 0; i < G[u].size(); i++) {
                int v = G[u][i];
                if (v != fa[u] && v != wson[u])
                    dfs2(v, v);
            }
        } else ed[u] = u;
        lst[u] = dft;
        return ed[u];
    }
  public:
    void init_space(int nodes_cnt, vector<int> *Graph) {
        n = nodes_cnt;
        G = Graph;
        fa = new int[n + 2];
        dep = new int[n + 2];
        siz = new int[n + 2];
        wson = new int[n + 2];
        dfn = new int[n + 2];
        lst = new int[n + 2];
        beg = new int[n + 2];
        ed = new int[n + 2];
        sgt.init_space(n);
    }
    void clear() {
        for (int i = 0; i < n + 2; i++) 
            fa[i] = dep[i] = siz[i] = wson[i] = dfn[i] = lst[i] = beg[i] = ed[i] = 0;
        dft = 0;
    }
    void pretreat(ValueType *NodesValue, int Root) {
        clear();
        dfs1(Root, 0);
        dfs2(Root, Root);
        sgt.clear();
        for (int i = 1; i <= n; i++)
            sgt.Modify_Range(dfn[i], dfn[i], NodesValue[i]);
    }
    void Update_Subtree_Value(int u, const ValueType &val) {
        sgt.Modify_Range(dfn[u], lst[u], val);
    }
    void Update_Chain_Value(int u, int v, const ValueType &val) {
        while (beg[u] != beg[v]) {
            if (dep[beg[u]] < dep[beg[v]]) swap(u, v);
            sgt.Modify_Range(dfn[beg[u]], dfn[u], val);
            u = fa[beg[u]];
        }
        if (dfn[u] > dfn[v]) swap(u, v);
        sgt.Modify_Range(dfn[u], dfn[v], val);
    }
    ValueType Query_Subtree_Value(int u) {
        return sgt.Query_Sum(dfn[u], lst[u]);
    }
    ValueType Query_Chain_Value(int u, int v) {
        ValueType ans = 0;
        while (beg[u] != beg[v]) {
            if (dep[beg[u]] < dep[beg[v]]) swap(u, v);
            ans = (ans + sgt.Query_Sum(dfn[beg[u]], dfn[u]));
            u = fa[beg[u]];
        }
        if (dfn[u] > dfn[v]) swap(u, v);
        return (ans + sgt.Query_Sum(dfn[u], dfn[v]));
    }
    TreeChainSplit() : n(0), G(NULL), fa(NULL), dep(NULL), siz(NULL), wson(NULL), 
                       dfn(NULL), lst(NULL), beg(NULL), ed(NULL), dft(0), sgt() {}
    ~TreeChainSplit() { free_space(); }
};

namespace against_cpp11 {
    const int N = 5e5 + 7;
    int n, q, R = 1;
    int64 w[N];
    vector<int> G[N];
    TreeChainSplit<int64> tcs;
    char Getop() {
        static char Buffer[19];
        scanf("%s", Buffer);
        return Buffer[1];
    }
    signed main() {
        read >> n >> q;
        for (int i = 1; i <= n; i++) read >> w[i];
        for (int i = 1, u, v; i < n; i++) {
            read >> u >> v;
            G[u].push_back(v);
            G[v].push_back(u);
        }
        tcs.init_space(n, G);
        tcs.pretreat(w, R);
        for (int i = 1, opt, x, y; i <= q; i++) {
            read >> opt >> x;
            if (opt == 1) {
                read >> y;
                tcs.Update_Chain_Value(x, x, y);
            } else if (opt == 2) {
                read >> y;
                tcs.Update_Subtree_Value(x, y);
            } else if (opt == 3) {
                write << tcs.Query_Chain_Value(R, x) << '\n';
            } 
        }
        return 0;
    }
}

signed main() { return against_cpp11::main(); }