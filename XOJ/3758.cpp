//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      3758: 0601 Genius ACM.
 * @user_id:      1677.
 * @user_name:    zhaoyi20(brealid).
 * @time:         2020-06-01.
 * @language:     C++.
 * @upload_place: XOJ.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// #define int int64
const int N = 500007;
int T, n, m; int64 k;
int p[N], tmp[N], tmpMerge[N], save[N];

void merge(int p1, int e1, int p2, int e2) {
    int p = p1, it = p1;
    while (p1 <= e1 && p2 <= e2) {
        if (tmp[p1] < tmp[p2]) tmpMerge[p++] = tmp[p1++];
        else tmpMerge[p++] = tmp[p2++];
    }
    while (p1 <= e1) {
        tmpMerge[p++] = tmp[p1++];
    }
    while (p2 <= e2) {
        tmpMerge[p++] = tmp[p2++];
    }
    while (it <= e2) {
        tmp[it] = tmpMerge[it];
        it++;
    }
}

bool check(int l, int mid, int r) {
    // printf("CHK(%d, %d)\n", l, r);
    r = min(r, n);
    for (int i = mid + 1; i <= r; i++) tmp[i] = p[i];
    sort(tmp + mid + 1, tmp + r + 1);
    merge(l, mid, mid + 1, r);
    // for (int i = l; i <= r; i++) write(tmp[i], " \n"[i == r]);
    int64 ans(0);
    for (int i = 1, p = l, q = r; i <= m && p < q; i++, p++, q--) {
        ans += (int64)(tmp[p] - tmp[q]) * (tmp[p] - tmp[q]);
        if (ans > k) return false;
    }
    return true;
}

signed main() {
    T = read<int>();
    while (T--) {
        n = read<int>();
        m = read<int>();
        k = read<int64>();
        for (int i = 1; i <= n; i++) p[i] = read<int>();
        int cnt = 0;
        for (int l = 1, r, kth; l <= n; l = r + 1) {
            kth = 1; r = l;
            tmp[l] = p[l];
            while (kth && r <= n) {
                for (int i = l; i <= r; i++) save[i] = tmp[i];
                if (check(l, r, r + kth)) r += kth, kth <<= 1;
                else {
                    kth >>= 1;
                    for (int i = l; i <= r; i++) tmp[i] = save[i];
                }
            }
            cnt++;
            // printf("[%d, %d]\n", l, r);
        }
        write(cnt, 10);
    }
    return 0;
}

// Create File Date : 2020-06-01