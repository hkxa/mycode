//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Remember the word.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-04.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int Pmod = 1e9 + 9, P = 20071027;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= Pmod ? Module(w - Pmod) : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % Pmod; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

const int N = 3e6 + 7;
const int K = 131;

int n, m, q;
char S[N], T[N];
int HashS[N], power[N], HashT;
vector<int> from[N];
int ans[N];

inline int GetHash(int l, int r) {
    return add(HashS[r], Pmod - mul(HashS[l - 1], power[r - l + 1]));
}

signed main() {
    ans[0] = 1;
    int case_count = 0;
    Dealing:;
    if (scanf("%s", S + 1) != 1) return 0;
    n = strlen(S + 1);
    power[0] = 1;
    for (int i = 1; i <= n; i++) {
        HashS[i] = add(mul(HashS[i - 1], K), S[i]);
        power[i] = mul(power[i - 1], K);
        from[i].clear();
    }
    q = read<int>();
    for (int i = 1; i <= q; i++) {
        scanf("%s", T + 1);
        m = strlen(T + 1);
        HashT = 0;
        for (int j = 1; j <= m; j++)
            HashT = add(mul(HashT, K), T[j]);
        // printf("(%s) => HashT = %d\n", T + 1, HashT);
        for (int i = m; i <= n; i++)
            if (GetHash(i - m + 1, i) == HashT) 
                from[i].push_back(i - m);
    }
    for (int i = 1; i <= n; i++) {
        sort(from[i].begin(), from[i].end());
        from[i].erase(unique(from[i].begin(), from[i].end()), from[i].end());
        ans[i] = 0;
        for (size_t j = 0; j < from[i].size(); j++)
            ans[i] = (ans[i] + ans[from[i][j]]) % P;
    }
    // for (int i = 0; i <= n; i++)
    //     printf("ans[%d] = %d\n", i, ans[i]);
    printf("Case %d: %d\n", ++case_count, ans[n]);
    // fprintf(stderr, "Case %d: %d\n", case_count, ans[n]);
    goto Dealing;
    return 0;
}

// Create File Date : 2020-08-04