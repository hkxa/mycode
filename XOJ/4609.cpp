//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      4609: 最近公共祖先（LCA）.
 * @user_name:    zhaoyi20(brealid).
 * @time:         2020-05-25.
 * @language:     C++.
 * @upload_place: XOJ.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int N = 500007;

int n, m, root;
vector<int> G[N];
int siz[N], wson[N];
int dep[N], top[N], f[N];

void dfs1(int u, int fa = 0) {
    f[u] = fa;
    dep[u] = dep[fa] + 1;
    siz[u] = 1;
    for (unsigned i = 0; i < G[u].size(); i++) {
        int &v = G[u][i];
        if (v != fa) {
            dfs1(v, u);
            siz[u] += siz[v];
            if (siz[v] > siz[wson[u]]) wson[u] = v;
        }
    }
}

void dfs2(int u, int beg) {
    top[u] = beg;
    if (wson[u]) dfs2(wson[u], beg);
    for (unsigned i = 0; i < G[u].size(); i++) {
        int &v = G[u][i];
        if (v != f[u] && v != wson[u]) {
            dfs2(v, v);
        }
    }
}

int LCA(int u, int v) {
    while (top[u] != top[v]) {
        if (dep[top[u]] < dep[top[v]]) swap(u, v);
        u = f[top[u]];
    }
    if (dep[u] < dep[v]) swap(u, v);
    return v;
}

int main()
{
    n = read<int>();
    m = read<int>();
    root = read<int>();
    for (int i = 1, u, v; i < n; i++) {
        u = read<int>();
        v = read<int>();
        G[u].push_back(v);
        G[v].push_back(u);
    }
    dfs1(root, 0);
    dfs2(root, root);
    for (int i = 1, u, v; i <= m; i++) {
        u = read<int>();
        v = read<int>();
        write(LCA(u, v), 10);
    }
    return 0;
}