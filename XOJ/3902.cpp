//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      3902: Disruption.
 * @user_name:    zhaoyi20(brealid).
 * @time:         2020-05-26.
 * @language:     C++.
 * @upload_place: XOJ.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, m;
vector<pair<int, int> > G[50007];
int f[50007], connectNode[50007];
int dep[50007];
int siz[50007], wson[50007];
int dfn[50007], dft;
int beg[50007];
int ans[50007];

int t[50007 * 4], lazy[50007 * 4];
void pushDown(int u) {
    t[u << 1] = min(t[u << 1], lazy[u]);
    t[u << 1 | 1] = min(t[u << 1 | 1], lazy[u]);
    lazy[u << 1] = min(lazy[u], lazy[u << 1]);
    lazy[u << 1 | 1] = min(lazy[u], lazy[u << 1 | 1]);
    lazy[u] = 0x3f3f3f3f;
}

void upd(int u, int l, int r, int ul, int ur, int v) {
    if (l > ur || r < ul) return;
    if (l >= ul && r <= ur) {
        t[u] = min(v, t[u]);
        lazy[u] = min(v, lazy[u]);
        return;
    }
    pushDown(u);
    int mid = (l + r) >> 1;
    upd(u << 1, l, mid, ul, ur, v);
    upd(u << 1 | 1, mid + 1, r, ul, ur, v);
}

int query(int u, int l, int r, int pos) {
    if (l > pos || r < pos) return 0x3f3f3f3f;
    if (l == pos && r == pos) return t[u];
    pushDown(u);
    int mid = (l + r) >> 1;
    return min(query(u << 1, l, mid, pos), query(u << 1 | 1, mid + 1, r, pos));
}

void dfs1(int u, int fa) {
    f[u] = fa;
    dep[u] = dep[fa] + 1;
    siz[u] = 1;
    for (unsigned i = 0; i < G[u].size(); i++) {
        int v = G[u][i].first;
        if (v == fa) continue;
        connectNode[G[u][i].second] = v;
        dfs1(v, u);
        siz[u] += siz[v];
        if (siz[v] > siz[wson[u]]) wson[u] = v;
    }
}

void dfs2(int u, int top) {
    dfn[u] = ++dft;
    beg[u] = top;
    if (wson[u]) dfs2(wson[u], top);
    for (unsigned i = 0; i < G[u].size(); i++) {
        int v = G[u][i].first;
        if (v == f[u] || v == wson[u]) continue;
        dfs2(v, v);
    }
}

int LCA(int u, int v) {
    while (beg[u] != beg[v]) {
        if (dep[beg[u]] < dep[beg[v]]) swap(u, v);
        u = f[beg[u]];
    }
    if (dep[u] < dep[v]) swap(u, v);
    return v;
}

int UpdChain(int u, int v, int val) {
    while (beg[u] != beg[v]) {
        if (dep[beg[u]] < dep[beg[v]]) swap(u, v);
        // printf("upd chin %d~%d\n", beg[u], u);
        upd(1, 1, n, dfn[beg[u]], dfn[u], val);
        u = f[beg[u]];
    }
    if (dep[u] < dep[v]) swap(u, v);
    // printf("upd chin %d~%d\n", v, u);
    upd(1, 1, n, dfn[v] + 1, dfn[u], val); // ❤❤重要❤❤
    return v;
}

int main()
{
    memset(t, 0x3f, sizeof(t));
    memset(lazy, 0x3f, sizeof(lazy));
    n = read<int>();
    m = read<int>();
    for (int i = 1, a, b; i < n; i++) {
        a = read<int>();
        b = read<int>();
        G[a].push_back(make_pair(b, i));
        G[b].push_back(make_pair(a, i));
    }
    dfs1(1, 0);
    dfs2(1, 1);
    for (int i = 1, p, q, r; i <= m; i++) {
        p = read<int>();
        q = read<int>();
        r = read<int>();
        UpdChain(p, q, r);
    }
    for (int i = 1, ans; i < n; i++) {
        // printf("query node %d\n", connectNode[i]);
        ans = query(1, 1, n, dfn[connectNode[i]]);
        write(ans != 0x3f3f3f3f ? ans : -1, 10);
    }
    return 0;
}

/*
Input
9 3
1 2
1 3
1 4
2 7
3 5
3 6
7 8
7 9
3 4 3
2 6 5
5 9 6
Output
5
3
3
6
6
5
-1
6
*/