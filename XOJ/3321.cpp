//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Substring 子串.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-05.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;
#define REP(i, r, n) for(int i = r; i <= n; i++)

// #define int int64

const int SIGMA_SIZE = 65;
const int MAXNODE = 1000;
const int MAXS = 150 + 10;

struct ACautomata
{
    int ch[MAXNODE][SIGMA_SIZE];
    int f[MAXNODE];    
    int val[MAXNODE];  
    int last[MAXNODE]; 
    int cnt[MAXS];
    int sz;

    void init()
    {
        sz = 1;
        memset(ch[0], 0, sizeof(ch[0]));
        memset(cnt, 0, sizeof(cnt));
        
    }

    
    int idx(char c)
    {
        if (c >= '0' && c <= '9')
            return c - '0';
        else if (c >= 'A' && c <= 'Z')
            return c - 'A' + 10;
        else if (c >= 'a' && c <= 'z')
            return c - 'a' + 36;
    }

    
    void insert(char *s, int v)
    {
        int u = 0, n = strlen(s);
        for (int i = 0; i < n; i++)
        {
            int c = idx(s[i]);
            if (!ch[u][c])
            {
                memset(ch[sz], 0, sizeof(ch[sz]));
                val[sz] = 0;
                ch[u][c] = sz++;
            }
            u = ch[u][c];
        }
        val[u] = 1;
        
    }

    
    void print(int i, int j)
    {
        if (j)
        {
            cnt[val[j]]++;
            print(i, last[j]);
        }
    }

    
    int find(char *T)
    {
        int n = strlen(T);
        int j = 0; 
        for (int i = 0; i < n; i++)
        { 
            int c = idx(T[i]);
            j = ch[j][c];
            if (val[j])
                print(i, j);
            else if (last[j])
                print(i, last[j]); 
        }
    }

    
    void getFail()
    {
        queue<int> q;
        f[0] = 0;
        
        for (int c = 0; c < SIGMA_SIZE; c++)
        {
            int u = ch[0][c];
            if (u)
            {
                f[u] = 0;
                q.push(u);
                last[u] = 0;
            }
        }
        
        while (!q.empty())
        {
            int r = q.front();
            q.pop();
            for (int c = 0; c < SIGMA_SIZE; c++)
            {
                int u = ch[r][c];
                if (!u)
                {
                    ch[r][c] = ch[f[r]][c];
                    continue;
                }
                q.push(u);
                int v = f[r];
                while (v && !ch[v][c])
                    v = f[v];
                f[u] = ch[v][c];
                last[u] = val[f[u]] ? f[u] : last[f[u]];
                val[u] |= val[f[u]]; 
            }
        }
    }
};
int L;
ACautomata solver;
double d[MAXNODE][105];
bool vis[MAXNODE][105];
double prob[65];
double dfs(int u, int k)
{
    if (!k)
        return 1.0;
    if (vis[u][k])
        return d[u][k];
    double &ans = d[u][k];
    ans = 0;
    vis[u][k] = 1;
    for (int i = 0; i < SIGMA_SIZE; i++)
    {
        if (!solver.val[solver.ch[u][i]])
            ans += prob[i] * dfs(solver.ch[u][i], k - 1);
    }
    return ans;
}
int main()
{
    int T;
    scanf("%d", &T);
    int cas = 0;
    while (T--)
    {
        char str[30];
        int k;
        solver.init();
        scanf("%d", &k);
        REP(i, 1, k)
        {
            scanf("%s", str);
            solver.insert(str, i);
        }
        solver.getFail();
        memset(vis, 0, sizeof(vis));
        scanf("%d", &k);
        double a;
        REP(i, 0, 64)
        prob[i] = 0;
        REP(i, 1, k)
        {
            scanf("%s%lf", str, &a);
            prob[solver.idx(str[0])] = a;
        }
        int L;
        scanf("%d", &L);
        printf("Case #%d: %.6lf\n", ++cas, dfs(0, L));
    }
    return 0;
}