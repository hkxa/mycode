#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
	Int flag = 1;
	char c = getchar();
	while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
	if (c == '-') flag = -1, c = getchar();
	Int init = c & 15;
	while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
	Int flag = 1;
	c = getchar();
	while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
	if (c == '-') flag = -1, c = getchar();
	Int init = c & 15;
	while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
	if (x < 0) putchar('-'), x = ~x + 1;
	if (x > 9) write(x / 10);
	putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
	write(x);
	putchar(nextch);
}

template <typename Int>
class super_BIT {
// #define DEBUG_super_BIT
  private:
  	Int n;
  	Int *basicSum, *delta1, *delta2;
  	
	Int lowbit(Int x) 
	{
	    return x & (-x);
	}
	
	void arr_add(Int *arr, Int pos, Int x) 
	{
	    while (pos <= n) {
	    	arr[pos] += x;
			pos += lowbit(pos);
		}
	}
	
	Int arr_getsum(Int *arr, Int pos)
	{
	    Int sum = 0;
	    while (pos) {
			sum += arr[pos];
			pos -= lowbit(pos);
		}
	    return sum;
	}
	
	void free_space()
	{
		delete[] basicSum;
		delete[] delta1;
		delete[] delta2;
	}
  public:
  	super_BIT() 
	{
  		basicSum = NULL;
  		delta1 = NULL;
  		delta2 = NULL;
	}
  	~super_BIT() 
	{
		free_space(); 
	}
  	
  	void init(Int size, Int *arr = NULL)
  	{
  		free_space();
  		n = size;
  		basicSum = new Int[size + 1];
  		delta1 = new Int[size + 1];
  		delta2 = new Int[size + 1];
  		memset(basicSum, 0, (size + 1) * sizeof(Int));
  		memset(delta1, 0, (size + 1) * sizeof(Int));
  		memset(delta2, 0, (size + 1) * sizeof(Int));
        if (arr != NULL) {
            for (Int i = 1; i <= n; i++) {
                basicSum[i] = basicSum[i - 1] + arr[i];
            }
        }
	}
	
	void modify(Int l, Int r, Int x)
	{
	    arr_add(delta1, l, x);
		arr_add(delta1, r + 1, -x);
		arr_add(delta2, l, x * l);
		arr_add(delta2, r + 1, -x * (r + 1));
	}
	
	Int getsum(Int r)
	{
		return basicSum[r] + arr_getsum(delta1, r) * (r + 1) - arr_getsum(delta2, r);
	}
	
	Int query(Int l, Int r)
	{
	    return getsum(r) - getsum(l - 1);
	}

	void debug()
	{
#ifdef DEBUG_super_BIT
		printf("HKXA-lib::super_BIT debug():\n    ");
		for (Int i = 1; i <= n; i++) {
			printf("%d ", query(i, i));
		}
		printf("\n\n");
#endif
	}
}; 

char getcc() {
    static char bufe[5];
    scanf("%s", bufe);
    return bufe[0];
}

int64 a[50007];
super_BIT<int64> sbit;

int main()
{
    int T = read<int>();
    for (int i = 1; i <= T; i++) {
        printf("Case %d:\n", i);
        int n, q;
        n = read<int>(); 
        for (int i = 1; i <= n; i++) {
            a[i] = read<int>();
        }
        sbit.init(n, a);
        // q = read<int>();
        char ch;
        int x, y;
        while ((ch = getcc()) != 'E') {
            switch (ch) {
                case 'A' : 
                    x = read<int>();
                    y = read<int>();
                    sbit.modify(x, x, y);
                    break;
                case 'S' : 
                    x = read<int>();
                    y = read<int>();
                    sbit.modify(x, x, -y);
                    break;
                case 'Q' : 
                    x = read<int>();
                    y = read<int>();
                    write(sbit.query(x, y), 10);
                    break;
            }
        }
    }
	return 0;
}