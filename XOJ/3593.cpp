/*************************************
 * @problem:      GCD.
 * @user_name:    brealid.
 * @time:         2020-11-05.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

const int N = 1e7 + 7;

class MathCalcer {
public:
    int phi[N];
    int64 sum_phi[N];
    int prime[N], pcnt;
    MathCalcer() {
        sum_phi[0] = pcnt = 0;
        sum_phi[1] = 1;
        for (int i = 0; i < N; ++i) phi[i] = i;
        for (int i = 2; i < N; ++i) {
            if (phi[i] == i) {
                prime[++pcnt] = i;
                for (int j = i; j < N; j += i)
                    phi[j] = phi[j]  / i * (i - 1);
            }
            sum_phi[i] = sum_phi[i - 1] + phi[i];
        }
    }
} m;

signed main() {
    int n = read.get<int>();
    int64 ans = 0;
    for (int i = 1; i <= m.pcnt && n >= m.prime[i]; ++i)
        ans += m.sum_phi[n / m.prime[i]] * 2 - 1;
    write << ans << '\n';
    return 0;
}