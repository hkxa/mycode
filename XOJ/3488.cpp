/*************************************
 * @problem:      启示录（apo）.
 * @user_name:    brealid.
 * @time:         2020-11-07.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

int digit[20], n;

int mem[20][4][2];
int dfs(int u, int cnt6, int bounded) {
    if (u > n) return cnt6 == 3;
    int &ans = mem[u][cnt6][bounded];
    if (~ans) return ans;
    ans = 0;
    for (int i = bounded ? digit[u] : 9; i >= 0; --i)
        ans += dfs(u + 1, cnt6 == 3 ? 3 : (i == 6 ? min(cnt6 + 1, 3) : 0), bounded && i == digit[u]);
    return ans;
}

int count(int64 bound) {
    n = 0;
    while (bound) {
        digit[++n] = bound % 10;
        bound /= 10;
    }
    for (int l = 1, r = n; l < r; ++l, --r) swap(digit[l], digit[r]);
    memset(mem, -1, sizeof(mem));
    return dfs(1, 0, 1);
}

int64 solve(int X) {
    int64 l = 666, r = 1e10, mid, ans;
    while (l <= r) {
        mid = (l + r) >> 1;
        // printf("count(%lld) = %lld\n", mid, count(mid));
        if (count(mid) >= X) r = mid - 1, ans = mid;
        else l = mid + 1;
    }
    return ans;
}

signed main() {
    for (int T = read.get<int>(); T --> 0;) write << solve(read.get<int>()) << '\n';
    return 0;
}