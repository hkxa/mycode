#include "testlib.h"
using namespace std;

int n;
map<int, int> cn;

int main(int argc, char* argv[]) 
{
    registerTestlibCmd(argc, argv);
    n = inf.readInt();
    for (int i = 1; i <= n; i++) {
        cn[inf.readInt()]++;
    }
    int cnt = ans.readInt(), usr = ouf.readInt();
    if (cnt != usr) quitf(_wa, "the maximum number of the snowmen is different : expected %d, read %d", cnt, usr);
    for (int i = 1; i <= usr; i++) {
        int a = ouf.readInt();
        int b = ouf.readInt();
        int c = ouf.readInt();
        if (a == b || a == c || b == c) quitf(_wa, "snowmen %d-th : found snowball same (%d, %d, %d)\n", i, a, b, c);
        if (a < b || b < c) quitf(_wa, "snowmen %d-th : found snowball are not sorted in decresing order (%d, %d, %d)\n", i, a, b, c);
        if (!cn[a]) quitf(_wa, "snowmen %d-th : found snowball %d are used too much\n", i, a);
        if (!cn[b]) quitf(_wa, "snowmen %d-th : found snowball %d are used too much\n", i, b);
        if (!cn[c]) quitf(_wa, "snowmen %d-th : found snowball %d are used too much\n", i, c);
        cn[a]--; cn[b]--; cn[c]--;
    }
    quitf(_ok, "the answer is acceptable");
    return 0;

}