//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      3944: 内存分配.
 * @user_name:    brealid.
 * @time:         2020-06-03.
 * @language:     C++.
 * @upload_place: xoj.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 988244353, N = 2e5 + 7;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

int n, T;
int t, m, p;
struct node {
    int l, r, end;
    node() {}
    node(int L, int R, int E) : l(L), r(R), end(E) {}
    int len() { return r - l + 1; }
};
struct task {
    int len, P;
    task() {}
    task(int L, int p) : len(L), P(p) {}
};
list<node> a;
queue<task> wait;
typedef list<node>::iterator ln_it;

void print() {
#if defined(_WIN32) && !defined(NDEBUG)
    printf("[T = %d] ", T);
    for (ln_it it = a.begin(); it != a.end(); it++) {
        printf("{%d, %d, %d} ", it->l, it->r, it->end);
    }
    putchar(10);
#endif
}

int erase(int p) {
    int Mn = 2e9 + 7;
    for (ln_it it = a.begin(); it != a.end(); it++) {
        if (it->end && it->end <= Mn) {
            Mn = it->end;
        }
    }
    if (Mn > p) return 2e9 + 7;
    for (ln_it it = a.begin(); it != a.end(); it++) {
        if (it != a.end() && it->end && it->end == Mn) {
            it->end = 0;
            ln_it bef = it; if (it != a.begin()) bef--;
            if (it != a.begin() && !bef->end) {
                it->l = bef->l;
                a.erase(bef);
            }
            ln_it nxt = it; nxt++;
            if (nxt != a.end() && !nxt->end) {
                it->r = nxt->r;
                a.erase(nxt);
            }
        }
    }
    return Mn;
}

bool insert(task t) {
    for (ln_it it = a.begin(); it != a.end(); it++) {
        if (!it->end && it->len() >= t.len) {
            if (it->len() == t.len) it->end = T + t.P;
            else {
                a.insert(it, node(it->l, it->l + t.len - 1, T + t.P));
                it->l += t.len;
            }
            return true;
        }
    }
    return false;
}

signed main() {
    n = read<int>();
    a.push_back(node(1, n, 0));
    T = 0;
    int ret, cnt = 0; task now;
    while (scanf("%d%d%d", &t, &m, &p) != EOF && (t || m || p)) {
        print();
        while (true) {
            ret = erase(t);
            // printf("ret = %d\n", ret);
            if (ret == 2e9 + 7) break;
            T = ret;
            print();
            while (!wait.empty()) {
                if (insert(wait.front())) wait.pop();
                else break;
                print();
            }
        }
        print();
        T = max(T, t);
        now.len = m; now.P = p;
        if (!insert(now)) wait.push(now), cnt++;
        print();
    }
    print();
    while (a.size() > 1 || !wait.empty()) {
        ret = erase(2e9 + 7);
        if (ret == 2e9 + 7) break;
        T = ret;
        print();
        while (!wait.empty()) {
            if (insert(wait.front())) wait.pop();
            else break;
            print();
        }
    }
    print();
    write(T, 10);
    write(cnt, 10);
    return 0;
}

// Create File Date : 2020-06-03

/*
6
0 4 2
1 2 4
2 2 3
3 2 2
4 1 100
10 5 2
0 0 0
0 AAAA**
1 AAAABB
2 CC**BB
3 CCDDBB
4 ;
*/