/*************************************
 * problem:      P3620 [APIO/CTSC 2007]数据备份.
 * user ID:      63720.
 * user name:    brealid.
 * time:         2020-06-07.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, k;
int d[500007] = {0};
int a[500007] = {0};
bool vis[500007] = {0};
int l[500007], r[500007];

struct cmp {
	bool operator () (int x, int y)  {
		return a[x] > a[y];
	}
};

priority_queue <int, vector<int>, cmp> q;

int main()
{
    n = read<int>();
    k = read<int>();
    for (int i = 1; i <= n; i++) {
        d[i] = read<int>();
    }
    a[0] = a[n] = 0x3f3f3f3f;
    for (int i = 1; i < n; i++) {
        a[i] = d[i + 1] - d[i];
        l[i] = i - 1;
        r[i] = i + 1;
        q.push(i);
    }
    int64 ans = 0;
    while (k--) {
        while (vis[q.top()]) {
            // printf("pop a[%d] (= %d) \n", q.top(), a[q.top()]);
            q.pop();
        }
        int x = q.top();
        q.pop();
        if (a[x] <= 0) {
            break;
        }
        // printf("ans += a[%d] (= %d) \n", x, a[x]);
        ans += a[x];
        vis[l[x]] = vis[r[x]] = 1;
        a[x] = a[l[x]] + a[r[x]] - a[x];
        q.push(x);
        r[l[l[x]]] = x;
        l[r[r[x]]] = x;
        l[x] = l[l[x]];
        r[x] = r[r[x]];
    }
    write(ans);
    putchar(10);
    return 0;
}
/*
   10 9 10 13
5 2
0 10 19 29 42
*/