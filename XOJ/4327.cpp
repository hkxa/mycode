//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      欧拉回路.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-30.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

struct Reader {
    char endch;
    Reader() { endch = '\0'; }
    template <typename Int>
    Reader& operator >> (Int &i) {
        i = baseFastio::read<Int>(endch);
        return *this;
    }
    template <typename Int>
    inline Int get_int() {
        return baseFastio::read<Int>();
    }
    inline char get_nxt() {
        return endch;
    }
} read;

struct Writer {
    Writer& operator << (const char *ch) {
        // char *p = ch;
        while (*ch) putchar(*(ch++));
        return *this;
    }
    Writer& operator << (const char ch) {
        putchar(ch);
        return *this;
    }
    template <typename Int>
    Writer& operator << (const Int i) {
        baseFastio::write(i);
        return *this;
    }
} write;

// #define int int64
const int N = 1e5 + 7, M = 4e5 + 7;

int n, m;
int to[M], nxt[M], head[N], cnt;
bool vis[M];
int s[M], top;
#define AddEdge(u, v) to[++cnt] = v; nxt[cnt] = head[u]; head[u] = cnt;

namespace undirected_subtask {
    int deg[N];
    void euler(int u) {
        for (int e; ~head[u]; head[u] = nxt[head[u]]) {
            if (!vis[head[u] >> 1]) {
                vis[head[u] >> 1] = true;
                euler(to[e = head[u]]);
                s[++top] = e;
                if (!~head[u]) break;
            }
        }
    }
    void main() {
        cnt = 1;
        read >> n >> m;
        for (int i = 1, u, v; i <= m; i++) {
            read >> u >> v;
            AddEdge(u, v);
            AddEdge(v, u);
            deg[u]++; deg[v]++;
        }
        for (int i = 1; i <= n; i++) {
            if (deg[i] & 1) {
                puts("NO");
                return;
            }
        }
        for (int i = 1; i <= n; i++)
            if (deg[i]) {
                euler(i);
                break;
            }
        for (int i = 1; i <= m; i++) {
            if (!vis[i]) {
                puts("NO");
                // printf("Edge %d\n", i);
                return;
            }
        }
        puts("YES");
        while (top) {
            if (s[top] & 1) write << '-' << (s[top] >> 1) << ' ';
            else write << (s[top] >> 1) << ' ';
            top--;
        }
        write << '\n';
    }
};


namespace directed_subtask {
    int ind[N], outd[N];
    void euler(int u) {
        for (int e; ~head[u]; head[u] = nxt[head[u]]) {
            if (!vis[head[u]]) {
                vis[head[u]] = true;
                euler(to[e = head[u]]);
                s[++top] = e;
                if (!~head[u]) break;
            }
        }
    }
    void main() {
        cnt = 0;
        read >> n >> m;
        for (int i = 1, u, v; i <= m; i++) {
            read >> u >> v;
            AddEdge(u, v);
            outd[u]++; ind[v]++;
        }
        for (int i = 1; i <= n; i++) {
            if (ind[i] ^ outd[i]) {
                puts("NO");
                return;
            }
        }
        for (int i = 1; i <= n; i++)
            if (ind[i]) {
                euler(i);
                break;
            }
        for (int i = 1; i <= m; i++) {
            if (!vis[i]) {
                puts("NO");
                return;
            }
        }
        puts("YES");
        while (top) {
            write << s[top] << ' ';
            top--;
        }
        write << '\n';
    }
};

signed main()
{
    memset(head, -1, sizeof(head));
    switch (read.get_int<int>()) {
        case 1 : undirected_subtask::main(); break;
        case 2 : directed_subtask::main(); break;
    }
    return 0;
}