/*************************************
 * @problem:      [CTS2019]随机立方体.
 * @author:       brealid.
 * @time:         2021-03-14.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 5e6 + 7, Pmod = 998244353;

namespace kmath {
    typedef unsigned uint32;
    typedef long long int64;
    typedef unsigned long long uint64;

    template <typename T>
    inline T gcd_BitOptimize_ull(T a, T b) {
        if (!a || !b) return a | b;
        int t = __builtin_ctzll(a | b);
        a >>= __builtin_ctzll(a);
        do {
            b >>= __builtin_ctzll(b);
            if (a > b) swap(a, b);
            b -= a;
        } while(b);
        return a << t;
    }
    template <typename T>
    inline T gcd_BitOptimize_ui(T a, T b) {
        if (!a || !b) return a | b;
        int t = __builtin_ctz(a | b);
        a >>= __builtin_ctz(a);
        do {
            b >>= __builtin_ctz(b);
            if (a > b) swap(a, b);
            b -= a;
        } while(b);
        return a << t;
    }
    template <typename T>
    T gcd_EuclidMethod(T a, T b) {
        return a ? gcd_EuclidMethod(b % a, a) : b;
    }

    template <typename T>
    inline T fmul(T a, T b, T p) {
        T ret = 0;
        while (b) {
            if (b & 1) if ((ret += a) >= p) ret -= p;
            if ((a <<= 1) >= p) a -= p; 
            b >>= 1;
        }
        return ret;
    }
    template <typename T>
    inline T fpow_FmulMethod(T a, T b, T p) {
        T ret = 1;
        while (b) {
            if (b & 1) ret = fmul(ret, a, p);
            a = fmul(a, a, p);
            b >>= 1;
        }
        return ret;
    }
    template <typename T, typename UpTurn = int64>
    inline T fpow(T a, T b, T p) {
        T ret = 1;
        while (b) {
            if (b & 1) ret = (UpTurn)ret * a % p;
            a = (UpTurn)a * a % p;
            b >>= 1;
        }
        return ret;
    }
    template <typename T> inline T gcd(T a, T b) { return gcd_EuclidMethod(a, b); }
    template <> inline int64 gcd(int64 a, int64 b) { return gcd_BitOptimize_ull(a, b); }
    template <> inline uint64 gcd(uint64 a, uint64 b) { return gcd_BitOptimize_ull(a, b); }
    template <> inline int gcd(int a, int b) { return gcd_BitOptimize_ui(a, b); }
    template <> inline uint32 gcd(uint32 a, uint32 b) { return gcd_BitOptimize_ui(a, b); }

    template <typename T>
    inline T lcm(T n, T m) {
        return n / gcd(n, m) * m;
    }

    template<typename Ta, typename Tb, typename ...Targ>
    inline Ta gcd(Ta a, Tb b, Targ ...arg) {
        return gcd(gcd(a, b), arg...);
    }

    template <typename T>
    void exgcd(T n, T m, T &x, T &y) {
        if (n == 1) x = 1, y = 0;
        else exgcd(m, n % m, y, x), y -= n / m * x;
    }

    template <typename T>
    T inv(T a, T m) {
        T x, y;
        exgcd(a, m, x, y);
        return (x % m + m) % m;
    }

    template <typename T>
    inline T inv_FimaMethod(T a, T m) {
        return fpow(a, m - 2, m);
    }

    template<int N, int64 ModP>
    struct CombinationNumber {
        int64 fac[N + 2], ifac[N + 2];
        CombinationNumber() {
            fac[0] = 1;
            for (int i = 1; i <= N; ++i) fac[i] = fac[i - 1] * i % ModP;
            ifac[N] = inv(fac[N], ModP);
            for (int i = N; i >= 1; --i) ifac[i - 1] = ifac[i] * i % ModP;
        }
        inline int64 C(int n, int m) {
            return fac[n] * ifac[n - m] % ModP * ifac[m] % ModP;
        }
        inline int64 P(int n, int m) {
            return fac[n] * ifac[n - m] % ModP;
        }
    };
};

kmath::CombinationNumber<5000000, Pmod> mt;

int T, n, m, l, k, bound;
int64 isum[N];

// 返回: 至少有 k 个极大值点的情况数
int64 f(int k) {
    return mt.P(n, k) * mt.P(m, k) % Pmod * mt.P(l, k) % Pmod * isum[k] % Pmod;
}

signed main() {
    for (kin >> T; T--;) {
        kin >> n >> m >> l >> k;
        bound = min(min(n, m), l);
        int64 S_tot = 1;
        for (int i = 1; i <= bound; ++i) {
            int64 Si = (int64)n * m % Pmod * l - (int64)(n - i) * (m - i) % Pmod * (l - i);
            S_tot = S_tot * (Si % Pmod + Pmod) % Pmod;
        }
        isum[bound] = kmath::inv(S_tot, (int64)Pmod);
        for (int i = bound; i >= 1; --i) {
            int64 Si = (int64)n * m % Pmod * l - (int64)(n - i) * (m - i) % Pmod * (l - i);
            isum[i - 1] = isum[i] * (Si % Pmod) % Pmod;
        }
        int64 ans = 0;
        for (int i = k; i <= bound; ++i) {
            if ((i ^ k) & 1) ans -= mt.C(i, k) * f(i) % Pmod;
            else ans += mt.C(i, k) * f(i) % Pmod;
        }
        kout << (ans % Pmod + Pmod) % Pmod << '\n';
    }
    return 0;
}