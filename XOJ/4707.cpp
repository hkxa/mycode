/*************************************
 * @problem:      Triangles.
 * @user_name:    zhaoyi20,brealid.
 * @time:         2020-05-18.
 * @language:     C++.
 * @upload_place: XOJ.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define P 1000000007
#define int int64

int n;
int X[100007], Y[100007];
vector<int> x[20007], y[20007];
int sum[100007];

bool cmpX(int a, int b) { return X[a] < X[b]; }
bool cmpY(int a, int b) { return Y[a] < Y[b]; }

signed main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        X[i] = read<int>() + 10000;
        Y[i] = read<int>() + 10000;
        x[X[i]].push_back(i);
        y[Y[i]].push_back(i);
    }
    for (int i = 0; i <= 20000; i++) {
        if (x[i].size() > 1) {
            sort(x[i].begin(), x[i].end(), cmpY);
            for (unsigned j = 1; j < x[i].size(); j++) {
                sum[x[i][0]] += Y[x[i][j]] - Y[x[i][0]];
            }
            for (unsigned j = 1; j < x[i].size(); j++) {
                sum[x[i][j]] = (sum[x[i][j - 1]] + (Y[x[i][j]] - Y[x[i][j - 1]]) * (2 * j - x[i].size())) % P;
            }
            // for (unsigned j = 0; j < x[i].size(); j++)
            //     printf("Point %d : SameX disTot = %d\n", x[i][j], sum[x[i][j]]);
        }
    }
    int ans = 0;
    for (int i = 0, tot; i <= 20000; i++) {
        if (y[i].size() > 1) {
            sort(y[i].begin(), y[i].end(), cmpX);
            tot = 0;
            for (unsigned j = 1; j < y[i].size(); j++) {
                tot += X[y[i][j]] - X[y[i][0]];
            }
            // printf("Point %d : SameY disTot = %d\n", y[i][0], tot);
            ans = (ans + sum[y[i][0]] * tot) % P;
            for (unsigned j = 1; j < y[i].size(); j++) {
                tot = tot + (X[y[i][j]] - X[y[i][j - 1]]) * (2 * j - y[i].size());
                // printf("Point %d : SameY disTot = %d\n", y[i][j], tot);
                ans = (ans + sum[y[i][j]] * tot) % P;
            }
        }
    }
    write(ans, 10);
    return 0;
}