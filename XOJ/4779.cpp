/*************************************
 * @problem:      国王.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-20.
 * @language:     C++.
 * @fastio_ver:   20200820.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore space, \t, \r, \n
            ch = getchar();
            while (ch != ' ' && ch != '\t' && ch != '\r' && ch != '\n') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            Int flag = 1;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = -1, endch = getchar();
            i = endch & 15;
            while (isdigit(endch = getchar())) i = (i << 3) + (i << 1) + (endch & 15);
            if (flag == -1) i = -i;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            Int flag = 1;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = -1, endch = getchar();
            Int i = endch & 15;
            while (isdigit(endch = getchar())) i = (i << 3) + (i << 1) + (endch & 15);
            if (flag == -1) i = -i;
            return i;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64
// #define int128 int64
// #define int128 __int128

int n, k;

int state[144 + 3], attack[144 + 3], popcnt[144 + 3], cnt;
int64 f[10 + 3][144 + 3][100 + 3];

inline int get_popcnt(int u) {
    int ret = 0;
    while (u) {
        u -= u & -u;
        ++ret;
    }
    return ret;
}

void init_ln() {
    cnt = 0;
    int mask = (1 << n);
    for (int i = 0; i < mask; i++) {
        if (i & ((i << 1) | (i >> 1))) continue;
        state[cnt] = i;
        attack[cnt] = i | (i << 1) | (i >> 1);
        popcnt[cnt] = get_popcnt(i);
        // printf("state %d : popcnt = %d\n", i, popcnt[cnt]);
        cnt++;
    }
}

int64 dp() {
    memset(f, 0, sizeof(f));
    for (int i = 0; i < cnt; i++) 
        if (popcnt[i] <= k) f[0][i][popcnt[i]] = 1;
    for (int i = 1; i < n; i++) 
        for (int j = 0; j < cnt; j++)
            for (int p = 0; p < cnt; p++) 
                if (!(state[j] & attack[p])) 
                    for (int q = popcnt[p]; q + popcnt[j] <= k; q++)
                        f[i][j][q + popcnt[j]] += f[i - 1][p][q];
    int64 ans = 0;
    for (int i = 0; i < cnt; i++) 
        ans += f[n - 1][i][k];
    return ans;
}

signed main() {
    while (scanf("%d%d", &n, &k) == 2) {
        init_ln();
        write << dp() << '\n';
    }
    return 0;
}