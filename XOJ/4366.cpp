//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Jury Compromise 陪审团.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-17.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <algorithm>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 200 + 7, M = 20 + 7, VAL = 800 + 7, MIN = 0xcfcfcfcf;

int a[N], b[N];
int n, m;
int f[N][M][VAL], from[N][M][VAL];
int case_id;

void output_way(int i, int j, int k) {
    if (j == 0) return;
    output_way(from[i][j][k] - 1, j - 1, k - (a[from[i][j][k]] - b[from[i][j][k]]));
    write << ' ' << from[i][j][k];
}

signed main() {
    read >> n >> m;
    TAG_MAIN_START:;
    for (int i = 1; i <= n; i++) {
        read >> a[i] >> b[i];
        a[i] += 20;
    }
    memset(f, 0xcf, sizeof(f));
    f[0][0][0] = 0;
    for (int i = 1; i <= n; i++) {
        for (int j = 0; j <= min(i - 1, m); j++)
            for (int k = 0; k <= 40 * j; k++) {
                f[i][j][k] = f[i - 1][j][k];
                from[i][j][k] = from[i - 1][j][k];
                // if (f[i][j][k] >= 0) printf("f[%d][%d][%d] = %d\n", i, j, k, f[i][j][k]);
            }
        for (int j = 1; j <= min(i, m); j++) { // 算上自己，选出的
            for (int k = 0; k <= 40 * (j - 1); k++) { // 已得到的 Diff
                if (f[i][j][k + a[i] - b[i]] < f[i - 1][j - 1][k] + a[i] + b[i] - 20) {
                    f[i][j][k + a[i] - b[i]] = f[i - 1][j - 1][k] + a[i] + b[i] - 20;
                    from[i][j][k + a[i] - b[i]] = i;
                    // if (f[i][j][k + a[i] - b[i]] >= 0) printf("f[%d][%d][%d] = %d\n", i, j, k + a[i] - b[i], f[i][j][k + a[i] - b[i]]);
                }
            }
        }
    }
    int where = -1;
    for (int i = 0; i <= 40 * m; i++)
        if (f[n][m][i] >= 0) {
            if (where != -1) {
                if (abs(i - 20 * m) > abs(where - 20 * m)) continue;
                if (abs(i - 20 * m) == abs(where - 20 * m) && f[n][m][i] < f[n][m][where]) continue;
            }
            where = i;
        }
    printf("Jury #%d\n", ++case_id);
    int diff = where - 20 * m, sum = f[n][m][where];
    printf("Best jury has value %d for prosecution and value %d for defence:\n", (sum + diff) / 2, (sum - diff) / 2);
    output_way(n, m, where);
    putchar(10);
    putchar(10);
    read >> n >> m;
    if (!n && !m) return 0;
    else goto TAG_MAIN_START;
    return 0;
}