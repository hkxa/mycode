// UNAC 9~27pts
//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      4287: Tree.
 * @user_name:    brealid.
 * @time:         2020-06-07.
 * @language:     C++.
 * @upload_place: XOJ.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 2147483647;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

const int N = 5e5 + 7, M = 1e6 + 7;

int n, m, fa[N];
int find(int x) { return fa[x] < 0 ? x : fa[x] = find(fa[x]); }
bool connect(int x, int y) {
    int fx = find(x), fy = find(y);
    if (fx != fy) {
        if (fa[fx] < fa[fy]) {
            fa[fy] = fx;
        } else if (fa[fx] > fa[fy]) {
            fa[fx] = fy;
        } else {
            fa[fy] = fx;
            fa[fx]--;
        }
        return true;
    }
    return false;
}

struct road {
    int u, v, w;
    bool operator < (const road &other) const {
        return w < other.w;
    }
} G[M]; 

int randL() {
    return ((rand() & 1) << 30) | (rand() << 15) | rand();
}

double randR() {
    return (double)(randL() ^ randL() ^ 2017) / 2147483647;
}
bool accept(double per) {
    return randR() < per;
}

int64 Min, ans = 9223372036854775807LL, now;
int cnt;
double t;
void getMin() {
    memarr(n, -1, fa);
    Min = 0;
    cnt = n - 1;
    for (int e = 0; e < m && cnt; e++) {
        if (connect(G[e].u, G[e].v)) {
            Min += G[e].w;
            cnt--;
        }
    }
}

void SA(const double startT, const double lowerT) {
    memarr(n, -1, fa);
    now = 0;
    t = startT;
    cnt = n - 1;
    for (int e = 0; e < m && cnt; e++) {
        if (accept(t) && connect(G[e].u, G[e].v)) {
            now += G[e].w;
            cnt--;
        }
        t *= lowerT;
    }
#ifdef _WIN32
    if (cnt) printf("*** ");
    if (!cnt && now == Min) printf("&&& ");
    if (!cnt && ans > 2031787 && now == 2031787) printf("Wow! When time = %d\n", clock());
#endif
    if (!cnt && now != Min && now < ans) ans = now;
}

double kpow(double a, int n) {
    double ret = 1;
    while (n) {
        if (n & 1) ret = ret * a;
        a = a * a;
        n >>= 1;
    }
    return ret;
}

double GetSqrt(int kth, double Rval, double Lval) {
    double l = Lval, r = Rval, mid, Need = Lval / Rval;
    for (int i = 1; i <= 100; i++) {
        mid = (l + r) / 2;
        if (kpow(mid, kth) > Need) r = mid;
        else l = mid;
    }
    return (l + r) / 2;
}

signed main() {
    srand(time(0) ^ 20170933);
    n = read<int>();
    m = read<int>();
    for (int i = 0; i < m; i++) {
        G[i].u = read<int>();
        G[i].v = read<int>();
        G[i].w = read<int>();
    }
    sort(G, G + m);
    getMin();
    // printf("MIN = %lld\n", Min);
    int timLim = CLOCKS_PER_SEC * 0.95;
    // double start = 1.024, lowerT = 0.999968;
    double start = 1.024, lowerT = GetSqrt(m, start, 0.994);
    // printf("lowerT = %.6lf\n", lowerT);
    while (clock() < timLim) SA(start, lowerT);
    write(ans, 10);
    return 0;
}

// 2031787

// Create File Date : 2020-06-07