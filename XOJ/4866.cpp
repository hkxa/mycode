//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      分离与合体.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-17.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 300 + 7;

uint32 n, a[N], sum[N], f[N][N], from[N][N];

signed main() {
    read >> n;
    // fprintf(stderr, "%d\n", n);
    for (uint32 i = 1; i <= n; i++) {
        read >> a[i];
        // fprintf(stderr, "%d%c", a[i], " \n"[i == n]);
        // f[i][i] = a[i];
        sum[i] = sum[i - 1] + a[i];
    }
    for (uint32 len = 2; len <= n; len++)
        for (uint32 i = 1, j = len; j <= n; i++, j++) {
            for (uint32 k = i; k < j; k++) {
                if (f[i][j] < f[i][k] + f[k + 1][j] + (a[i] + a[j]) * a[k]) {
                    f[i][j] = f[i][k] + f[k + 1][j] + (a[i] + a[j]) * a[k];
                    from[i][j] = k;
                }
            }
        }
    write << f[1][n] << '\n';
    queue<pair<uint32, uint32> > opt;
    opt.push(make_pair(1, n));
    while (!opt.empty()) {
        uint32 l = opt.front().first, r = opt.front().second;
        opt.pop();
        if (l >= r || (l == n && r == n)) continue;
        // else if (l == r) write << l << ' ';
        else {
            write << from[l][r] << ' ';
            opt.push(make_pair(l, from[l][r]));
            opt.push(make_pair(from[l][r] + 1, r));
        }
    }
    write << '\n';
    return 0;
}