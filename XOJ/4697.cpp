//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      4697: Haircut 剪头发
 * @user_name:    zhaoyi20(brealid).
 * @time:         2020-05-31.
 * @language:     C++.
 * @upload_place: XOJ.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// #define int int64

const int N = 100007;

int n;
int a[N];

vector<int> lis[N];
int orig[N];
int64 ans[N];
int t[N], cnt[N];
#define lowbit(x) ((x) & (-(x)))
void upd(int *t, int u, const int dif) {
    while (u <= n) {
        t[u] += dif;
        u += lowbit(u);
    }
}
int query(int *t, int u) {
    if (u <= 0) return 0;
    int res = 0;
    while (u) {
        res += t[u];
        u -= lowbit(u);
    }
    return res;
}

signed main() {
    n = read<int>();
    for (int i = 1; i <= n; i++) 
        a[i] = read<int>() + 1;
    for (int i = n; i >= 1; i--) {
        orig[i] = query(t, a[i] - 1);
        ans[n + 1] += orig[i];
        upd(t, a[i], 1);
        if (a[i] == n + 1) upd(cnt, i, 1);
        else lis[a[i]].push_back(i);
    }
    for (int i = n; i >= 1; i--) {
        ans[i] = ans[i + 1];
        for (unsigned j = 0; j < lis[i].size(); j++)
            ans[i] -= query(cnt, lis[i][j]);
        // for (unsigned j = 0; j < lis[i].size(); j++)
        //     printf("query(%d) = %d\n", lis[i][j], query(cnt, lis[i][j]));
        for (unsigned j = 0; j < lis[i].size(); j++)
            upd(cnt, lis[i][j], 1);
        // for (unsigned j = 0; j < lis[i].size(); j++)
        //     printf("upd(%d)\n", lis[i][j]);
    }
    for (int i = 1; i <= n; i++)
        write(ans[i], 10);
    return 0;
}
// 6 4 3 5 2
// * *   *
// Create File Date : 2020-05-31