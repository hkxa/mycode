//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Mondriaan's Dream.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-19.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

int n, m;
int64 f[11][5741];

int ok_type[5741], cnt;
int id[4194304];

/*
state 0 for 
[x] <== this
[x]
state 1 for
[x]
[x] <== this
state 2 for
[x][x]
*/

void init(int m) {
    cnt = 0;
    for (int i = 0; i < (1 << (m << 1)); i++) {
        bool lst_off = 0, fail = 0;
        for (int j = 0; j < m && !fail; j++) {
            if (((i >> (j << 1)) & 3) == 3) fail = true;
            else if (((i >> (j << 1)) & 3) == 2) lst_off = !lst_off;
            else if (lst_off) fail = true;
        }
        id[i] = 0;
        if (fail || lst_off) {
            id[i] = 0;
            continue;
        }
        id[i] = cnt;
        ok_type[cnt++] = i;
    }
}

int64 dp(int ln, int state, int pos, int now, bool lst_off) {
    if (pos >= m) {
        if (lst_off) return 0;
        else return f[ln][id[now]];
    } else if (((state >> (pos << 1)) & 3) == 1) {
        if (lst_off) return 0;
        else return dp(ln, state, pos + 1, now, 0);
    } else {
        if (lst_off) return dp(ln, state, pos + 1, now | (2 << (pos << 1)), 0);
        return dp(ln, state, pos + 1, now | (1 << (pos << 1)), 0) + dp(ln, state, pos + 1, now | (2 << (pos << 1)), 1);
    }
}

bool could_be_beg(int state, int m) {
    for (int i = 0; i < m; i++)
        if (((state >> (i << 1)) & 3) == 1) return false;
    // printf("state %d could be beg\n", state);
    return true;
}

bool could_be_end(int state, int m) {
    for (int i = 0; i < m; i++)
        if (((state >> (i << 1)) & 3) == 0) return false;
    // printf("state %d could be end\n", state);
    return true;
}

int64 solve() {
    if ((n & 1) && (m & 1)) return 0;
    if (n < m) swap(n, m);
    init(m);
    for (int i = 0; i < cnt; i++) 
        if (could_be_beg(ok_type[i], m)) f[0][i] = 1;
        else f[0][i] = 0;
    for (int ln = 1; ln < n; ln++) 
        for (int j = 0; j < cnt; j++) // now
            f[ln][j] = dp(ln - 1, ok_type[j], 0, 0, 0);
    int64 ans = 0;
    for (int i = 0; i < cnt; i++) 
        if (could_be_end(ok_type[i], m)) ans += f[n - 1][i];
    // for (int i = 0; i < cnt; i++) 
    //     if (could_be_end(ok_type[i], m)) printf("end state %d : f = %lld\n", ok_type[i], f[n - 1][i]);
    return ans;
}

signed main() {
    while (true) {
        read >> n >> m;
        if (!n && !m) return 0;
        write << solve() << '\n';
    }
    return 0;
}