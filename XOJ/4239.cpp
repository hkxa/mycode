//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      4239: 门票.
 * @user_name:    brealid.
 * @time:         2020-06-03.
 * @language:     C++.
 * @upload_place: xoj.
*************************************/ 
#pragma GCC optimize(1)
#pragma GCC optimize(2)
#pragma GCC optimize(3)

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int N = 2e6;
int A, B, C, x, x1, x2;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= C ? w - C : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % C; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64
int a[2000007], nxt[2000007];
int head[200007];

bool find(int u, int id, int x) {
    for (int e = head[u]; ~e; e = nxt[e]) {
        if (a[e] == x) return true;
    }
    a[id] = x;
    nxt[id] = head[u];
    head[u] = id;
    return false;
}

signed main() {
    memarr(199997, -1, head);
    A = read<int>();
    B = read<int>();
    C = read<int>();
    A %= C;
    nxt[0] = -1;
    head[x = 1] = 0;
    for (int i = 1; i <= N; i++) {
        x = ((int64)A * x + x % B) % C;
        // printf("a[%d] = %d\n", i, x);
        x1 = x / 199997;
        x2 = x % 199997;
        if (find(x2, i, x1)) {
            write(i, 10);
            return 0;
        }
    }
    puts("-1");
    return 0;
}

// Create File Date : 2020-06-03