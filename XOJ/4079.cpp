#include <bits/stdc++.h>
using namespace std;
#define ll long long
#define rp(i, x, y) for (register int i = x; i <= y; ++i)
#define my(i, x, y) for (register int i = y; i >= x; --i)

const int N = 250000 + 10, sqtN = 500 + 10;
int x, y, pl, rl, n, len, mx[sqtN], fr[sqtN], bl[N], to[sqtN], ans;
bool vis[N];
queue<int> Q;

inline int read()
{
    register char ch = getchar();
    register int x = 0;
    register bool y = 1;
    while (ch != '-' && (ch < '0' || ch > '9'))
        ch = getchar();
    if (ch == '-')
        ch = getchar(), y = 0;
    while (ch >= '0' && ch <= '9')
        x = (x << 1) + (x << 3) + (ch ^ '0'), ch = getchar();
    return y ? x : -x;
}

struct node
{
    int m, p, r, bl;
    double jl;
    inline void rd()
    {
        int xx = read(), yy = read();
        jl = (double)sqrt((ll)(x - xx) * (x - xx) + (ll)(y - yy) * (y - yy));
        m = read();
        p = read();
        r = read();
    }
} st[N];

inline bool cmp1(node gold, node genius) { return gold.m < genius.m; }
inline bool cmp2(node gold, node genius) { return gold.bl == genius.bl ? gold.jl < genius.jl : gold.bl < genius.bl; }
inline void pre()
{
    x = read();
    y = read();
    st[0].p = pl = read();
    st[0].r = rl = read();
    n = read();
    len = sqrt(n);
    rp(i, 1, n) st[i].rd();
    sort(st + 1, st + 1 + n, cmp1);
    rp(i, 1, n) st[i].bl = (i - 1) / len + 1;
    rp(i, 1, (n - 1) / len + 1) fr[i] = (i - 1) * len + 1, to[i] = min(n, i * len), mx[i] = st[to[i]].m;
    sort(st + 1, st + 1 + n, cmp2);
}
inline void work()
{
    Q.push(0);
    while (!Q.empty())
    {
        int now = Q.front();
        Q.pop();
        rp(i, 1, (n - 1) / len + 1)
        {
            if (mx[i] > st[now].p)
            {
                rp(j, fr[i], to[i]) if (st[j].jl < st[now].r && st[j].m <= st[now].p && !vis[j]) vis[j] = 1, Q.push(j), ++ans;
                break;
            }
            rp(j, fr[i], to[i])
            {
                if (st[j].jl > st[now].r)
                    break;
                if (!vis[j])
                    vis[j] = 1, Q.push(j), ++ans;
                ++fr[i];
            }
        }
    }
    printf("%d\n", ans);
}

int main()
{
    pre();
    work();
}