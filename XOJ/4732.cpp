//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      New Year Snowmen.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-08.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

struct Reader {
    char endch;
    Reader() { endch = '\0'; }
    template <typename Int>
    Reader& operator >> (Int &i) {
        i = baseFastio::read<Int>(endch);
        return *this;
    }
    template <typename Int>
    inline Int get_int() {
        return baseFastio::read<Int>();
    }
    inline char get_nxt() {
        return endch;
    }
} read;

struct Writer {
    Writer& operator << (const char *ch) {
        // char *p = ch;
        while (*ch) putchar(*(ch++));
        return *this;
    }
    Writer& operator << (const char ch) {
        putchar(ch);
        return *this;
    }
    template <typename Int>
    Writer& operator << (const Int i) {
        baseFastio::write(i);
        return *this;
    }
} write;

// #define int int64

struct FuckPair {
    int first, second;
    bool operator < (const FuckPair b) const {
        return first ^ b.first ? first < b.first : second > b.second;
    }
};

int n;
map<int, int> cnt;
priority_queue<FuckPair> q;
int ans[100007][3], ans_cnt;
FuckPair a[3];

signed main()
{
    read >> n;
    for (int i = 1; i <= n; i++)
        cnt[read.get_int<int>()]++;
    for (map<int, int>::iterator it = cnt.begin(); it != cnt.end(); it++) {
        q.push((FuckPair){it->second, it->first});
    }
    while (q.size() >= 3) {
        ans_cnt++;
        a[0] = q.top(); q.pop();
        a[1] = q.top(); q.pop();
        a[2] = q.top(); q.pop();
        ans[ans_cnt][0] = a[0].second;
        ans[ans_cnt][1] = a[1].second;
        ans[ans_cnt][2] = a[2].second;
        a[0].first--;
        a[1].first--;
        a[2].first--;
        if (a[0].first) q.push(a[0]);
        if (a[1].first) q.push(a[1]);
        if (a[2].first) q.push(a[2]);
    }
    write << ans_cnt << '\n';
    for (int i = 1; i <= ans_cnt; i++) {
        sort(ans[i], ans[i] + 3, greater<int>());
        write << ans[i][0] << ' ' << ans[i][1] << ' ' << ans[i][2] << '\n';
    }
    return 0;
}