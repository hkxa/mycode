//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      I Hate It.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-12.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

struct Reader {
    char endch;
    Reader() { endch = '\0'; }
    template <typename Int>
    Reader& operator >> (Int &i) {
        i = baseFastio::read<Int>(endch);
        return *this;
    }
    template <typename Int>
    inline Int get_int() {
        return baseFastio::read<Int>();
    }
    inline char get_nxt() {
        return endch;
    }
} read;

struct Writer {
    Writer& operator << (const char *ch) {
        // char *p = ch;
        while (*ch) putchar(*(ch++));
        return *this;
    }
    Writer& operator << (const char ch) {
        putchar(ch);
        return *this;
    }
    template <typename Int>
    Writer& operator << (const Int i) {
        baseFastio::write(i);
        return *this;
    }
} write;

// #define int int64

int n, m;
int op, x, y; int64 k;
int a[200007];

int t[200007 << 4];  

inline void pushup(int p) {
    t[p] = max(t[p << 1], t[p << 1 | 1]);
}

inline void build(int p, int l, int r)
{
    // printf("build point %d seq [%d, %d]\n", p, l, r);
    if (l > r) return;
    if (l == r) {
        t[p] = a[l];
        return;
    }
    int mid = (l + r) >> 1;
    build(p << 1, l, mid);
    build(p << 1 | 1, mid + 1, r);
    pushup(p);
}

inline void modify(int p, int l, int r, int pos, int64 k)
{
    // printf("modify point %d seq [%d, %d]\tgoal_seq [%d, %d] diff %lld\n", p, l, r, ml, mr, k);
    if (l > pos || r < pos) return;
    if (l == r) {
        t[p] = k;
        return;
    }
    int mid = (l + r) >> 1;
    modify(p << 1, l, mid, pos, k);
    modify(p << 1 | 1, mid + 1, r, pos, k);
    pushup(p);
}

inline int64 query(int p, int l, int r, int ml, int mr)
{
    if (l > mr || r < ml) return 0;
    if (l >= ml && r <= mr) return t[p];
    int mid = (l + r) >> 1;
    return max(query(p << 1, l, mid, ml, mr), query(p << 1 | 1, mid + 1, r, ml, mr));
}

char getcc() {
    static char bufe[5];
    scanf("%s", bufe);
    return bufe[0];
}

int main()
{
    while (scanf("%d%d", &n, &m) != EOF) {
        for (int i = 1; i <= n; i++) a[i] = baseFastio::read<int>();
        memset(t, 0, sizeof(t));
        build(1, 1, n);
        char ch;
        while (m--) {
            ch = getcc();
            read >> x >> y;
            if (ch == 'U') {
                modify(1, 1, n, x, y);
            } else {
                write << query(1, 1, n, x, y) << '\n';
            }
        }
    }
    return 0;
}