//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Help Yourself.
 * @user_name:    zhaoyi20(brealid).
 * @time:         2020-05-20.
 * @language:     C++.
 * @upload_place: XOJ.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int N = 100007, N2 = 200007, P = 1000000007, inv2 = 500000004;

struct Point {
    int l, r;
    // Point() {}
    bool operator < (const Point &b) const {
        return l ^ b.l ? l < b.l : r < b.r;
    }
} a[N];
int n, cnt[N2];
int64 ans;

int fpow(int a, int n) {
    int ret = 1;
    while (n) {
        if (n & 1) ret = (int64)ret * a % P;
        a = (int64)a * a % P;
        n >>= 1;
    }
    return ret;
}
#define inv(x) kpow(x, P - 2)

int main() {
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        a[i].l = read<int>();
        a[i].r = read<int>();
        cnt[a[i].r]++;
    }
    sort(a + 1, a + n + 1);
    for (int i = 1; i <= (n << 1); i++)
        cnt[i] += cnt[i - 1];
    for (int i = 1; i <= n; i++)
        ans = ((ans << 1) + fpow(2, cnt[a[i].l - 1])) % P;
    write(ans, 10);
    return 0;
}

/*
3 1 2 3 4 5 6 : 12
3 1 6 2 3 4 5 : 8
3 1 2 1 2 1 2 : 7
3 1 3 2 5 4 6 : 8
*/