//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      矩阵取数游戏.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-16.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &b1) {
            b1 = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int b1) {
            baseFastio::write(b1);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 100 + 7;

int n, m;
int64 a[N];

const int64 BigIntWidth = 100000000;
struct BigInt {
    int64 data[6];
    BigInt(int64 a = 0) {
        data[0] = a;
        data[1] = data[2] = data[3] = data[4] = data[5] = 0ll;
    }
    BigInt pow2(int kth) {
        while (kth) {
            int now = kth > 27 ? 27 : kth;
            int64 dif = 1ll << now;
            for (int i = 0; i < 6; i++) data[i] *= dif;
            for (int i = 0; i < 5; i++) {
                data[i + 1] += data[i] / BigIntWidth;
                data[i] %= BigIntWidth;
            }
            kth -= now;
        }
        return *this;
    }
    BigInt operator + (const BigInt b) const {
        BigInt res;
        for (int i = 0; i < 6; i++) res.data[i] = data[i] + b.data[i];
        for (int i = 0; i < 5; i++) {
            if (res.data[i] >= BigIntWidth) {
                res.data[i] -= BigIntWidth;
                res.data[i + 1]++;
            }
        }
        return res;
    }
    bool operator < (const BigInt &b) const {
        for (int i = 5; i >= 0; i--) if (data[i] ^ b.data[i]) return data[i] < b.data[i];
        return 0;
    }
    void output() const {
        int u = 5;
        while (u >= 0 && !data[u]) u--;
        if (u < 0) {
            puts("0");
            return;
        }
        printf("%lld", data[u--]);
        while (u >= 0) printf("%08lld", data[u--]);
        putchar(10);
    }
};

BigInt ans, f[N][N];

BigInt solve() {
    memset(f, 0, sizeof(f));
    for (int i = 1; i <= m; i++) f[i][i] = BigInt(a[i]).pow2(m);
    for (int l = 2; l <= m; l++)
        for (int i = 1, j = l; j <= m; i++, j++)
            f[i][j] = max(f[i + 1][j] + BigInt(a[i]).pow2(m - l + 1), f[i][j - 1] + BigInt(a[j]).pow2(m - l + 1));
    return f[1][m];
}

signed main() {
    read >> n >> m;
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++) read >> a[j];
        ans = ans + solve();
    }
    ans.output();
    return 0;
}