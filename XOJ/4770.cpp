//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      旅游规划.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-19.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 2e5 + 7;

template <typename ITER> ITER pre(ITER it) { return --it; }
template <typename ITER> ITER nxt(ITER it) { return ++it; }

int n;
vector<int> G[N];
pair<int, int> f[N];

pair<int, int> dfs(int u, int fa) {
    f[u] = make_pair(-1, u);
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i];
        if (v == fa) continue;
        f[u] = max(f[u], dfs(v, u));
    }
    return f[u] = make_pair(f[u].first + 1, f[u].second);
}

bool towards_deepest[N];

int len;

void get_ans(int u, int fa) {
    map<int, vector<int>, greater<int> > cnt;
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i];
        if (v == fa) continue;
        get_ans(v, u);
        cnt[f[v].first].push_back(v);
    }
    // if (cnt.size() >= 2) printf("node %d : Deepest %d, 2nd Deepest %d\n", u, cnt.begin()->first, nxt(cnt.begin())->first);
    if (cnt.size() >= 2 && cnt.begin()->first + nxt(cnt.begin())->first + 2 == len) {
        towards_deepest[u] = true;
        vector<int> &v = nxt(cnt.begin())->second;
        for (size_t i = 0; i < v.size(); i++)
            towards_deepest[v[i]] = true;
    }
}

vector<int> ans;

void solve(int u, int fa, bool typ) {
    if (typ) ans.push_back(u);
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i];
        if (v == fa) continue;
        solve(v, u, towards_deepest[v] || (typ && f[v].first + 1 == f[u].first));
    }
}

signed main() {
    read >> n;
    for (int i = 1, u, v; i < n; i++) {
        read >> u >> v;
        G[u].push_back(v);
        G[v].push_back(u);
    }
    int root = dfs(1, -1).second;
    len = dfs(root, -1).first;
    towards_deepest[root] = true;
    get_ans(root, -1);
    solve(root, -1, 1);
    sort(ans.begin(), ans.end());
    // printf("root = %d, len = %d\n", root, len);
    for (size_t i = 0; i < ans.size(); i++)
        write << ans[i] << '\n';
    return 0;
}