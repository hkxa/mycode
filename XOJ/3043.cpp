//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      棋盘分割.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-16.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

int n, sum[9][9];
double f[9][9][9][9][16];
double average;

double dp(int a, int b, int c, int d, int cnt) {
    double &ans = f[a][b][c][d][cnt];
    if (ans >= 0) return ans;
    if (cnt == 1) {
        double val = (sum[b][d] - sum[a - 1][d] - sum[b][c - 1] + sum[a - 1][c - 1]) - average;
        // printf("f[%d][%d][%d][%d][%d] = %.3lf\n", a, b, c, d, cnt, val * val);
        return ans = val * val;
    }
    if ((b - a + 1) * (d - c + 1) < cnt) return ans = 1e18;
    ans = 1e18;
    for (int i = a; i < b; i++) {
        ans = min(ans, dp(a, i, c, d, 1) + dp(i + 1, b, c, d, cnt - 1));
        ans = min(ans, dp(a, i, c, d, cnt - 1) + dp(i + 1, b, c, d, 1));
    }
    for (int i = c; i < d; i++) {
        ans = min(ans, dp(a, b, c, i, 1) + dp(a, b, i + 1, d, cnt - 1));
        ans = min(ans, dp(a, b, c, i, cnt - 1) + dp(a, b, i + 1, d, 1));
    }
    // printf("f[%d][%d][%d][%d][%d] = %.3lf\n", a, b, c, d, cnt, ans);
    return ans;
}

signed main() {
    read >> n;
    for (int i = 1; i <= 8; i++)
        for (int j = 1; j <= 8; j++)
            sum[i][j] = sum[i - 1][j] + sum[i][j - 1] - sum[i - 1][j - 1] + read.get_int<int>();
    for (int a = 1; a <= 8; a++)
        for (int b = 1; b <= 8; b++)
            for (int c = 1; c <= 8; c++)
                for (int d = 1; d <= 8; d++)
                    for (int e = 1; e <= n; e++)
                        f[a][b][c][d][e] = -1;
    average = (double)sum[8][8] / n;
    printf("%.3lf\n", sqrt(dp(1, 8, 1, 8, n) / n));
    return 0;
}