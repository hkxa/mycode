//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      开车旅行.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-19.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

template <typename Iter> Iter pre(Iter it) { return --it; }
template <typename Iter> Iter nxt(Iter it) { return ++it; }

#define Max_N 100007 
#define LogN 18 
#define Max_LogN_SizeArray 20 

int n;
int64 h[Max_N] = {0};
struct Sort_h{
    int64 h_num;
    int Id;
    bool operator < (const Sort_h &o) const 
    {
        return h_num < o.h_num;
    }
} sh[Max_N] = {0};
int64 min1[Max_N] = {0}, min2[Max_N] = {0};
int x0, m;
int s[Max_N] = {0};
int64 x[Max_N] = {0};
int64 fA[Max_N][Max_LogN_SizeArray] = {0}, fB[Max_N][Max_LogN_SizeArray] = {0};
int To_Point[Max_N][Max_LogN_SizeArray] = {0};

bool min_cmp(int &jzo, int &p1, int &p2)  {
    if (p1 < 1) return 0;
    if (p2 > n) return 1;
    return (h[jzo] - h[p1]) <= (h[p2] - h[jzo]);
}

void make_f() {
    for (int i = 1; i <= n; i++) {
        if (To_Point[i][0] == n + 1) To_Point[i][0] = 0;
    }
    for (int k = 1; k <= LogN; k++) {
        for (int i = 1; i <= n; i++) {
            fA[i][k] = fA[i][k - 1] + fA[To_Point[i][k - 1]][k - 1];
            fB[i][k] = fB[i][k - 1] + fB[To_Point[i][k - 1]][k - 1];
            To_Point[i][k] = To_Point[To_Point[i][k - 1]][k - 1];
        }
    }
}

void get_ans(int64 &aSave, int64 &bSave, int from, int64 h) {
    aSave = bSave = 0;
    for (int i = LogN; ~i; i--) {
        if (To_Point[from][i] && aSave + bSave + fA[from][i] + fB[from][i] <= h) {
            aSave += fA[from][i];
            bSave += fB[from][i];
            from = To_Point[from][i];
        }
    }
    if (min2[from] != 0 && aSave + bSave + fA[from][0] <= h) aSave += fA[from][0];
    return;
}

int main() {
    read >> n;
    for (int i = 1; i <= n; i++) read >> h[i];
    read >> x0 >> m;
    for (int i = 1; i <= m; i++) read >> s[i] >> x[i];
    h[0] = -3e9;
    h[n + 1] = 3e9;
    multiset<Sort_h> ms;
    ms.insert((Sort_h){(int64)-3e9, 0});
    ms.insert((Sort_h){(int64)-3e9, 0});
    ms.insert((Sort_h){(int64)3e9, n + 1});
    ms.insert((Sort_h){(int64)3e9, n + 1});

    int Left_Id, Right_Id, This_Id, llid, rrid;
    multiset<Sort_h>::iterator it;
    Sort_h taa;
    for (int i = n; i; i--) {
        taa.h_num = h[i];
        taa.Id = i;
        ms.insert(taa);
        it = ms.lower_bound(taa); 
        This_Id = it->Id;
        Left_Id = pre(it)->Id;
        Right_Id = nxt(it)->Id;
        if (min_cmp(This_Id, Left_Id, Right_Id)) {
            min1[This_Id] = Left_Id;
            llid = pre(pre(it))->Id;
            if (min_cmp(This_Id, llid, Right_Id)) min2[This_Id] = llid; 
            else min2[This_Id] = Right_Id;
        } else {
            min1[This_Id] = Right_Id;
            rrid = nxt(nxt(it))->Id;
            if (min_cmp(This_Id, Left_Id, rrid)) min2[This_Id] = Left_Id;
            else min2[This_Id] = rrid;
        }
    }
    for (int i = 1; i <= n; i++) {
        To_Point[i][0] = min1[min2[i]];
        fA[i][0] = abs(h[min2[i]] - h[i]);
        fB[i][0] = abs(h[min1[min2[i]]] - h[min2[i]]);
    }
    make_f();
    int64 ansa, ansb;
    double minn = 1e13;
    int s0 = 0;
    for(int i = 1; i <= n; i++){
        get_ans(ansa, ansb, i, x0);
        if (ansb && (1.0 * ansa / ansb < minn)){
            minn = 1.0 * ansa / ansb;
            s0 = i;
        }
    }
    write << s0 << '\n';
    for (int i = 1; i <= m; i++) {
        get_ans(ansa, ansb, s[i], x[i]);
        write << ansa << ' ' << ansb << '\n';
    }
    return 0;
}