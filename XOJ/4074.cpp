//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      4074: 楼兰图腾.
 * @user_name:    brealid.
 * @time:         2020-06-07.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 988244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64
const int N = 200000 + 7;
int n, y[N];
int lu[N], ld[N], ru[N], rd[N]; // LeftUp, LeftDown, RightUp, RightDown
int t[N];

#define lowbit(x) ((x) & (-(x)))
int query(int u) {
    int ret = 0;
    while (u) {
        ret += t[u];
        u -= lowbit(u);
    }
    return ret;
}
void add(int u, int dif) {
    while (u <= n) {
        t[u] += dif;
        u += lowbit(u);
    }
}

signed main() {
    n = read<int>();
    for (int i = 1; i <= n; i++) y[i] = read<int>();
    for (int i = 1; i <= n; i++) {
        ld[i] = query(y[i]);
        lu[i] = i - 1 - ld[i];
        add(y[i], 1);
    }
    memarr(n, 0, t);
    for (int i = n; i >= 1; i--) {
        rd[i] = query(y[i]);
        ru[i] = n - i - rd[i];
        add(y[i], 1);
    }
    int64 ansU = 0, ansD = 0;
    for (int i = 1; i <= n; i++) {
        ansU += (int64)lu[i] * ru[i];
        ansD += (int64)ld[i] * rd[i];
    }
    write(ansU, 32);
    write(ansD, 10);
    return 0;
}

// Create File Date : 2020-06-07