/// @brief DLX 跳舞链模板
class DancingLinksX {
private:
    struct DancingLinksX_Node {
        int l, r, u, d; // l, r, u, d 是指向左, 右, 上, 下的指针
        int row, col;   // 第 row 行，第 col 列
        DancingLinksX_Node() : l(0), r(0), u(0), d(0), row(0), col(0) {}
    };
    std::vector<DancingLinksX_Node> node; // 存储节点
    std::vector<int> first;               // 存储每行的头结点
    std::vector<int> siz;                 // 存储每列 '1' 的个数
    std::vector<int> result;              // 存放答案
    int cnt;
    /**
     * @brief 删除节点
     * @param u 节点编号
     */
    void remove(int u) {
        DancingLinksX_Node &self = node[u];
        node[self.l].r = self.r;
        node[self.r].l = self.l;
        for (int i = self.u; i != u; i = node[i].u)
            for (int j = node[i].l; j != i; j = node[j].l) {
                node[node[j].d].u = node[j].u;
                node[node[j].u].d = node[j].d;
                --siz[node[j].col];
            }
    }
    /**
     * @brief 恢复节点
     * @param u 节点编号
     */
    void recover(int u) {
        DancingLinksX_Node &self = node[u];
        for (int i = self.d; i != u; i = node[i].d)
            for (int j = node[i].r; j != i; j = node[j].r) {
                node[node[j].d].u = node[node[j].u].d = j;
                ++siz[node[j].col];
            }
        node[self.l].r = node[self.r].l = u;
    }
    /**
     * @brief 核心搜索 (跳跃 dance)
     */
    bool dance() {
        if (!node[0].r) return true;
        int minimum_col = node[0].r;
        for (int i = node[0].r; i != 0; i = node[i].r)
            if (siz[i] < siz[minimum_col]) minimum_col = i;
        remove(minimum_col);
        result.push_back(0);
        for (int i = node[minimum_col].d; i != minimum_col; i = node[i].d) {
            result.back() = node[i].row;
            for (int j = node[i].l; j != i; j = node[j].l) remove(node[j].col);
            if (dance()) return true;
            for (int j = node[i].r; j != i; j = node[j].r) recover(node[j].col);
        }
        result.pop_back();
        recover(minimum_col);
        return false;
    }
public:
    DancingLinksX() : cnt(0) {}
    /**
     * @brief 构造函数
     * @param row 行数
     * @param col 列数
     * @param count_of_1 最多有几个'1'
     */
    DancingLinksX(int row, int col, int count_of_1) { init(row, col, count_of_1); }
    /**
     * @brief 初始化函数
     * @param row 行数
     * @param col 列数
     * @param count_of_1 最多有几个'1'
     */
    void init(int row, int col, int count_of_1) {
        cnt = col;
        first.assign(row + 1, 0);
        siz.assign(col + 1, 0);
        node.assign(col + count_of_1 + 1, DancingLinksX_Node());
        for (int i = 0; i <= col; ++i) {
            node[i].l = i - 1;
            node[i].r = i + 1;
            node[i].u = node[i].d = i;
        }
        node[0].l = col;
        node[col].r = 0;
    }
    /**
     * @brief 插入节点
     * @param r 第几行
     * @param c 第几列
     */
    void insert(int r, int c) {
        DancingLinksX_Node &self = node[++cnt];
        self.row = r;
        self.col = c;
        ++siz[c];
        self.u = node[c].u;
        node[self.u].d = cnt;
        self.d = c;
        node[c].u = cnt;
        if (!first[r]) first[r] = self.l = self.r = cnt;
        else {
            self.l = node[first[r]].l;
            self.r = first[r];
            node[node[first[r]].l].r = cnt;
            node[first[r]].l = cnt;
        }
        for (int j = node[first[r]].l; j != first[r]; j = node[j].l);
        for (int j = node[first[r]].r; j != first[r]; j = node[j].r);
        for (int j = node[cnt].l; j != cnt; j = node[j].l);
        for (int j = node[cnt].r; j != cnt; j = node[j].r);
    }
    /**
     * @brief 工作函数（无解返回空 vector）
     * @return vector<int> 答案数组
     */
    const vector<int>& solve() {
        result.clear();
        dance();
        return result;
    }
};