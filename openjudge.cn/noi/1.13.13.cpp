#include <stdio.h>
/*

人民币支付(rmb.cpp)
描述
从键盘输入一指定金额（以元为单位，如 345），然后输出支付该金额的各种面额的人民币数量，显示 100 元，
50 元，20 元，10 元，5 元，1 元各多少张，要求尽量使用大面额的钞票。
输入
一个小于 1000 的正整数。
输出
输出分行，每行显示一个整数，从上到下分别表示 100 元，50 元，20 元，10 元，5 元，1 元人民币的张数
样例输入
735
样例输出
7
0
1
1
1
0

*/
//从上到下分别表示 100 元，50 元，20 元，10 元，5 元，1 元人民币的张数.
#define F1 100
#define F2 50
#define F3 20
#define F4 10
#define F5 5
#define F6 1

int main()
{
	freopen("rmb.in", "r", stdin);
	freopen("rmb.out", "w", stdout);
	int rmb;
	scanf("%d", &rmb);
	printf("%d\n", rmb/F1);
	rmb%=F1;
	printf("%d\n", rmb/F2);
	rmb%=F2;
	printf("%d\n", rmb/F3);
	rmb%=F3;
	printf("%d\n", rmb/F4);
	rmb%=F4;
	printf("%d\n", rmb/F5);
	rmb%=F5;
	printf("%d\n", rmb/F6);
	rmb%=F6;
	return 0;
	
}
