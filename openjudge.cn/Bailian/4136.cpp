//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      矩形分割.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-19.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

#define int int64

const int N = 1e4 + 7;

struct node {
    int l, t, w, h;
} nodes[N];

int r, n;

long long check(int cutline) {
    long long larea = 0, rarea = 0;
    for (int i = 0; i < n; i++) {
        if (nodes[i].l >= cutline) {
            rarea += nodes[i].w * nodes[i].h;     
        } else if (nodes[i].l + nodes[i].w > cutline) {
            rarea += (nodes[i].l + nodes[i].w - cutline) * nodes[i].h;
            larea += (cutline - nodes[i].l) * nodes[i].h;
        } else {
            larea += nodes[i].w * nodes[i].h;
        }
    }
    return larea - rarea;
}

int SearchInHalf() {
    int64 lleft = 0, rright = r, mid;
    int64 nRet;
    while (lleft <= rright) {
        mid = (lleft + rright) >> 1;
        nRet = check(mid);
        if (nRet < 0) lleft = mid + 1;
        else if (nRet > 0) rright = mid - 1;
        else return mid;
    } 
    return lleft;
}

signed main() {
    read >> r >> n;
    for (int i = 0; i < n; i++) {
        read >> nodes[i].l >> nodes[i].t >> nodes[i].w >> nodes[i].h;
    }
    if (n == 1 && nodes[n - 1].w == 1) {
        write << r << '\n';
        return 0;
    }
    int aa = SearchInHalf();
    while (check(aa) == check(aa + 1) && aa < r) aa++;
    write << aa << '\n';
    return 0;
}