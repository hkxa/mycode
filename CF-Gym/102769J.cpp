/*************************************
 * @problem:      Jewel Splitting.
 * @time:         2020-10-25.
 ************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef long long int64;
typedef unsigned long long uint64;

template <typename Int>
Int read() {
    Int d = 0;
    bool flag = 0;
    char ch = getchar();
    while ((ch < '0' || ch > '9') && ch != '-' && ch != EOF) ch = getchar();
    if (ch == '-') flag = 1, ch = getchar();
    d = ch & 15;
    while ((ch = getchar()) >= '0' && ch <= '9') d = (d << 3) + (d << 1) + (ch & 15);
    return flag ? -d : d;
}

template <typename Int>
void write(Int x) {
    static char buffer[33];
    static int top = 0;
    if (!x) {
        putchar('0');
        return;
    }
    if (x < 0) putchar('-'), x = -x;
    while (x) {
        buffer[++top] = '0' | (x % 10);
        x /= 10;
    }
    while (top) putchar(buffer[top--]);
}

void File_IO_init_IO(string file_name) {
    freopen((file_name + ".in" ).c_str(), "r", stdin);
    freopen((file_name + ".out").c_str(), "w", stdout);
}

template<const unsigned maxSize, const unsigned mK, const unsigned mP>
class HashEngine {
  private:
    int64 power_mK[maxSize], hash_value[maxSize];
  public:
    template <typename T>
    void init(T *arr, int n) {
        power_mK[0] = hash_value[0] = 1;
        for (int i = 1; i <= n; ++i) {
            power_mK[i] = power_mK[i - 1] * mK % mP;
            hash_value[i] = (hash_value[i - 1] * mK + arr[i]) % mP;
        }
    }
    unsigned qHash(int l, int r) {
        return (hash_value[r] - hash_value[l - 1] * power_mK[r - l + 1] % mP + mP) % mP;
    }
};

template<const unsigned N, const unsigned bucketSize>
class HashSet {
  private:
    uint64 val[N];
    int cnt[N], nxt[N], tot;
    int head[bucketSize], headTimeX[bucketSize], TimeXnow;
  public:
    HashSet() : tot(0), TimeXnow(1) {
        memset(head, 0, sizeof(head));
        memset(headTimeX, 0, sizeof(headTimeX));
    }
    void clear() {
        tot = 0;
        ++TimeXnow;
    }
    int insert(uint64 v) {
        int bp = v % bucketSize;
        if (headTimeX[bp] != TimeXnow) {
            headTimeX[bp] = TimeXnow;
            head[bp] = 0;
        }
        for (int u = head[bp]; u; u = nxt[u])
            if (val[u] == v) return ++cnt[u];
        val[++tot] = v;
        cnt[tot] = 1;
        nxt[tot] = head[bp];
        head[bp] = tot;
        return 1;
    }
    int erase(uint64 v) {
        int bp = v % bucketSize;
        if (headTimeX[bp] != TimeXnow) {
            headTimeX[bp] = TimeXnow;
            head[bp] = 0;
        }
        for (int u = head[bp]; u; u = nxt[u])
            if (val[u] == v) return cnt[u]--;
        return 0;
    }
};

const int N = 1e6 + 7, P = 998244353;
int n;
char s[N];
int inv[N];

HashEngine<N, 101, 1000000009> h1;
HashEngine<N, 541, 1000000007> h2;
HashSet<N, 20170933> sh, vh;

void init_inv(int n) {
    inv[1] = 1;
    for (int i = 2; i <= n; ++i) inv[i] = (int64)(P - P / i) * inv[P % i] % P;
}

uint64 GetStrHash(int l, int r) {
    uint64 ret = (uint64)h1.qHash(l, r) << 31 | h2.qHash(l, r);
    ret ^= ret >> 15;
    ret ^= ret << 7;
    ret ^= ret >> 19;
    return ret ^ 19260817;
}

int work(int d) {
    static uint64 lhv[N], rhv[N];
    int m = 0;
    uint64 now = 1, sum = 0, ans = 0;
    for (int i = 1; i + d - 1 <= n; i += d)
        lhv[++m] = GetStrHash(i, i + d - 1);
    for (int i = n, now = m; i - d + 1 >= 1; i -= d)
        rhv[now--] = GetStrHash(i - d + 1, i);
    sh.clear();
    vh.clear();
    for (int i = 1; i <= m; ++i) {
        sum += lhv[i];
        now = now * i % P * inv[sh.insert(lhv[i])] % P;
    }
    ans = now;
    vh.insert(sum);
    for (int i = m; i >= 1; --i) {
        now = now * sh.erase(lhv[i]) % P;
        now = now * inv[sh.insert(rhv[i])] % P;
        sum += rhv[i] - lhv[i];
        if (vh.insert(sum) == 1) ans += now;
    }
    // printf("work(%d) = %llu\n", d, ans % P);
    return ans % P;
}

void do_once() {
    scanf("%s", s + 1);
    n = strlen(s + 1);
    init_inv(n);
    h1.init(s, n);
    h2.init(s, n);
    uint64 ans = 0;
    for (int i = 1; i <= n; ++i) ans += work(i);
    write(ans % P);
    putchar(10);
}

signed main() {
    int T;
    scanf("%d", &T);
    for (int i = 1; i <= T; ++i) {
        printf("Case #%d: ", i);
        do_once();
    }
    return 0;
}