// Eval.hpp by I_love_him52
#ifndef EVAL_HPP
#define EVAL_HPP
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <vector>
#include <algorithm>
#include <cmath>
#include <ctime>
#include <string>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <map>
namespace std
{
    bool __Flag__Rand__ = 0;

    const double PI = 3.141592653589793;
    const double E = 2.718281828459045;
    const string ALPHABET_SMALL = "abcdefghijklmnopqrstuvwxyz";
    const string ALPHABET_CAPITAL = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    const string ALPHABET = ALPHABET_SMALL + ALPHABET_CAPITAL;
    const string NUMBERS = "0123456789";

    template<class _Int>
    _Int gcd(_Int a,_Int b)
    {
        if((a & 1) == 0 && (b & 1) == 0)
            return (gcd(a >> 1,b >> 1) << 1);
        if(a == b)
            return a;
        return gcd(max(a,b) - min(a,b),min(a,b));
    }

    template<class _Int>
    _Int lcm(_Int a,_Int b)
    {
        if(a == 1)
            return b;
        else if(b == 1)
            return a;
        else return a * b / gcd(a,b);
    }

    inline long long xrand()
    {
        if(!__Flag__Rand__)
            srand(time(0)),__Flag__Rand__ = 1;
        long long a = ((long long)rand() << 16 | (long long)rand()) << 31 | ((long long)rand() << 16 | (long long)rand());
        long long b = ((long long)rand() << 16 | (long long)rand()) << 31 | ((long long)rand() << 16 | (long long)rand());
        return a | b;
    }

    inline void do_not_reset_seed()
    {
        __Flag__Rand__ = 1;
    }

    inline long long randint(const long long &a,const long long &b)
    {
        return xrand() % (b - a + 1) + a;
    }

    inline string randstr(size_t len,const string &charset)
    {
        string ret;
        ret.resize(len);
        for(register size_t i = 0;i < len;++i)
            ret[i] = charset[randint(0u,charset.length() - 1)];
        return ret;
    }

    inline string randstr_prefix(size_t len,const string &charset,const string &prefix)
    {
        return prefix + randstr(len,charset);
    }

    inline string randstr_suffix(size_t len,const string &charset,const string &suffix)
    {
        return randstr(len,charset) + suffix;
    }

    template<class _Pointer>
    _Pointer choose(_Pointer _beg,_Pointer _end)
    {
        size_t len = _end - _beg;
        return _beg + xrand() % len;
    }

    class eval
    {
        ofstream data_in;
        string file_prefix;
        string file_name_in;
        string file_name_out;
        int data_id;
        stringstream ss;
    public:
        eval(){}
        ~eval(){clear();}
        eval(const eval &td)
        {
            *this = td;
        }
        eval(const string &prefix,int id):file_prefix(prefix),data_id(id)
        {
            ss.clear();
            ss.str("");
            ss << file_prefix << data_id << ".in";
            ss >> file_name_in;
            ss.clear();
            ss.str("");
            ss << file_prefix << data_id << ".out";
            ss >> file_name_out;
            data_in.open(file_name_in.c_str());
        }
        eval(const string &in,const string &out):file_name_in(in),file_name_out(out)
        {
            data_in.open(file_name_in.c_str());
        }
        void open(const string &prefix,int id)
        {
            file_prefix = prefix;
            data_id = id;
            ss.clear();
            ss.str("");
            ss << file_prefix << data_id << ".in";
            ss >> file_name_in;
            ss.clear();
            ss.str("");
            ss << file_prefix << data_id << ".out";
            ss >> file_name_out;
            data_in.open(file_name_in.c_str());
        }
        void open(const string &in,const string &out)
        {
            file_name_in = in;
            file_name_out = out;
            data_in.open(file_name_in.c_str());
        }
        template<class T>
        eval &operator<<(T a)
        {
            data_in << a;
            return *this;
        }
        void clear()
        {
            data_in.close();
            file_prefix.clear();
            file_name_in.clear();
            file_name_out.clear();
            ss.clear();
            ss.str("");
        }
        void generate(const string& std_program)
        {
            data_in.close();
            string str;
            str = std_program + " < " + file_name_in + " > " + file_name_out;
            system(str.c_str());
        }
        eval &operator=(const eval &td)
        {
            memcpy(this,&td,sizeof *this);
            return *this;
        }
    };

    template<class _ValueType>
    class graph
    {
    public:
        typedef _ValueType value_type;
    private:
        struct edge
        {
            int u,v;
            value_type w;
        };
        vector<edge> _edge;
        bool weight;
        size_t ver;
        size_t ed;
    public:
        graph(){}
        ~graph(){clear();}
        void clear()
        {
            _edge.clear();
            weight = 0;
            ver = 0;
            ed = 0;
        }
        void generate_tree(size_t n,value_type lower,value_type upper)
        {
            clear();
            ver = n;
            ed = n - 1;
            weight = 1;
            for(size_t i = 2;i <= n;++i)
            {
                int v = randint(1,i - 1);
                _edge.push_back((edge){i,v,randint(lower,upper)});
            }
            random_shuffle(_edge.begin(),_edge.end());
        }
        void generate_tree(size_t n)
        {
            clear();
            ver = n;
            ed = n - 1;
            for(size_t i = 2;i <= n;++i)
            {
                int v = randint(1,i - 1);
                _edge.push_back((edge){i,v});
            }
            random_shuffle(_edge.begin(),_edge.end());
        }
        void generate_dag(size_t n,size_t m,value_type lower,value_type upper)
        {
            if(m < n)
            {
                throw "The number of edges is too small.";
                return ;
            }
            clear();
            ver = n;
            ed = m;
            weight = 1;
            size_t aver = m / (n - 1);
            bool bz[n + 10];
            for(size_t i = 2;i <= n;++i)
            {
                memset(bz,0,sizeof bz);
                for(size_t j = 1;j <= min(m,min(aver,i - 1));++j)
                {
                    int v = randint(1,i - 1);
                    while(bz[v])
                        v = randint(1,i - 1);
                    bz[v] = 1;
                    _edge.push_back((edge){i,v,randint(lower,upper)});
                }
                m -= min(aver,i - 1);
            }
            memset(bz,0,sizeof bz);
            for(size_t i = 1;i <= m;++i)
            {
                int v = randint(1,n - 1);
                while(bz[v])
                    v = randint(1,n - 1);
                bz[v] = 1;
                _edge.push_back((edge){n,v,randint(lower,upper)});
            }
            random_shuffle(_edge.begin(),_edge.end());
        }
        void generate_dag(size_t n,size_t m)
        {
            if(m < n)
            {
                throw "The number of edges is too small.";
                return ;
            }
            clear();
            ver = n;
            ed = m;
            size_t aver = m / (n - 1);
            bool bz[n + 10];
            for(size_t i = 2;i <= n;++i)
            {
                memset(bz,0,sizeof bz);
                for(size_t j = 1;j <= min(m,min(aver,i - 1));++j)
                {
                    int v = randint(1,i - 1);
                    while(bz[v])
                        v = randint(1,i - 1);
                    bz[v] = 1;
                    _edge.push_back((edge){i,v});
                }
                m -= min(aver,i - 1);
            }
            memset(bz,0,sizeof bz);
            for(size_t i = 1;i <= m;++i)
            {
                int v = randint(1,n - 1);
                while(bz[v])
                    v = randint(1,n - 1);
                bz[v] = 1;
                _edge.push_back((edge){n,v});
            }
            random_shuffle(_edge.begin(),_edge.end());
        }
        void generate_graph(size_t n,size_t m,value_type lower,value_type upper)
        {
            if(m < n)
            {
                throw "The number of edges is too small.";
                return ;
            }
            clear();
            map< pair<int,int>,bool > bz;
            weight = 1;
            ver = n;
            ed = m;
            for(size_t i = 2;i <= n;++i)
            {
                int v = randint(1,i - 1);
                _edge.push_back((edge){i,v,randint(lower,upper)});
                bz[make_pair(i,v)] = bz[make_pair(v,i)] = 1;
            }
            for(size_t i = n;i <= m;++i)
            {
                int u,v;
                do
                    u = randint(1,n),v = randint(1,n);
                while(bz.count(make_pair(u,v)));
                _edge.push_back((edge){u,v,randint(lower,upper)});
                bz[make_pair(u,v)] = bz[make_pair(v,u)] = 1;
            }
            random_shuffle(_edge.begin(),_edge.end());
        }
        void generate_graph(size_t n,size_t m)
        {
            if(m < n)
            {
                throw "The number of edges is too small.";
                return ;
            }
            clear();
            map< pair<int,int>,bool > bz;
            ver = n;
            ed = m;
            for(size_t i = 2;i <= n;++i)
            {
                int v = randint(1,i - 1);
                _edge.push_back((edge){i,v});
                bz[make_pair(i,v)] = bz[make_pair(v,i)] = 1;
            }
            for(size_t i = n;i <= m;++i)
            {
                int u,v;
                do
                    u = randint(1,n),v = randint(1,n);
                while(bz.count(make_pair(u,v)));
                _edge.push_back((edge){u,v});
                bz[make_pair(u,v)] = bz[make_pair(v,u)] = 1;
            }
            random_shuffle(_edge.begin(),_edge.end());
        }
        void generate_chain(size_t n,value_type lower,value_type upper)
        {
            clear();
            weight = 1;
            ver = n;
            ed = n - 1;
            for(size_t i = 2;i <= n;++i)
                _edge.push_back((edge){i,i - 1,randint(lower,upper)});
            random_shuffle(_edge.begin(),_edge.end());
        }
        void generate_chain(size_t n)
        {
            clear();
            ver = n;
            ed = n - 1;
            for(size_t i = 2;i <= n;++i)
                _edge.push_back((edge){i,i - 1});
            random_shuffle(_edge.begin(),_edge.end());
        }
        void generate_flower(size_t n,value_type lower,value_type upper)
        {
            clear();
            weight = 1;
            ver = n;
            ed = n - 1;
            int cen = randint(1,n);
            for(size_t i = 1;i <= n;++i)
                if(i != cen)
                    _edge.push_back((edge){i,cen,randint(lower,upper)});
        }
        void generate_flower(value_type n)
        {
            clear();
            ver = n;
            ed = n - 1;
            int cen = randint(1,n);
            for(size_t i = 1;i <= n;++i)
                if(i != cen)
                    _edge.push_back((edge){i,cen});
        }
        const size_t &count_vertices() const
        {
            return ver;
        }
        const size_t &count_edges() const
        {
            return ed;
        }
        friend eval &operator<<(eval &td,const graph &obj)
        {
            td << obj.ver << " " << obj.ed << "\n";
            for(size_t i = 0;i < obj._edge.size();++i)
            {
                td << obj._edge[i].u << " " << obj._edge[i].v;
                if(obj.weight)
                    td << " " << obj._edge[i].w;
                td << "\n";
            }
            return td;
        }
        void print()
        {
            for(size_t i = 0;i < _edge.size();++i)
            {
                cout << _edge[i].u << " " << _edge[i].v;
                if(weight)
                    cout << " " << _edge[i].w;
                cout << "\n";
            }
        }
    };
}
#endif