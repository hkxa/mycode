#include <bits/stdc++.h>
using namespace std;
// v1.1.1

template<typename T>
class dynamic_array {
public:
    typedef T                                       value_type;
    typedef size_t                                  size_type;
    typedef value_type*                             pointer;
    typedef value_type*                             iterator;
    typedef const value_type*                       const_iterator;
    typedef value_type&                             reference;
    typedef const value_type&                       const_reference;
    typedef std::reverse_iterator<iterator>         reverse_iterator;
    typedef const std::reverse_iterator<iterator>   const_reverse_iterator;
private:
    value_type *pStart, *pFinish, *pEnd_of_Storage;
public:
    dynamic_array() {
        pFinish = pStart = new value_type[4];
        pEnd_of_Storage = pStart + 4;
    }
    template<typename Init_Iterator>
    dynamic_array(Init_Iterator x, Init_Iterator y) {
        pFinish = pStart = new value_type[4];
        pEnd_of_Storage = pStart + 4;
        while (x != y) {
            if (pFinish == pEnd_of_Storage) reserve((pEnd_of_Storage - pStart) << 1);
            *(pFinish++) = *(x++);
        }
    }  
    dynamic_array(const dynamic_array<value_type> &from) {
        pFinish = pStart = new value_type[from.reserved_size()];
        pEnd_of_Storage = pStart + from.reserved_size();
        while (pFinish - pStart < from.size()) {
            *pFinish = from[pFinish - pStart];
            ++pFinish;
        }
    }
    dynamic_array<value_type>& operator = (dynamic_array<value_type> &from) { 
        delete[] pStart;
        pFinish = pStart = new value_type[from.reserved_size()];
        pEnd_of_Storage = pStart + from.reserved_size();
        while (pFinish - pStart < from.size()) {
            *pFinish = *from[pFinish - pStart];
            ++pFinish;
        }
        return *this;
    }
    dynamic_array<value_type>& operator = (dynamic_array<value_type> from) { *this = from; }
    ~dynamic_array() { delete[] pStart; }
    iterator begin() const { return pStart; }
    iterator end() const { return pFinish; }
    reverse_iterator rbegin() const { return reverse_iterator(pFinish); }
    reverse_iterator rend() const { return reverse_iterator(pStart); }
    bool empty() const { return pStart == pFinish; }
    size_t size() const { return pFinish - pStart; }
    size_t reserved_size() const { return pEnd_of_Storage - pStart; }
    reference front() { return *pStart; }
    reference back() { return *(pFinish - 1); }
    reference operator [] (int pos) { return *(pStart + pos); }
    const_reference front() const { return *pStart; }
    const_reference back() const { return *(pFinish - 1); }
    const_reference operator [] (int pos) const { return *(pStart + pos); }
    pointer data() const { return pStart; }
    void clear() {
        delete[] pStart;
        pFinish = pStart = new value_type[4];
        pEnd_of_Storage = pStart + 4;
    }
    void reserve(size_t new_size) {
        iterator newStart = new value_type[new_size];
        size_t copy_part = pFinish - pStart;
        if (new_size < copy_part) copy_part = new_size;
        memcpy(newStart, pStart, copy_part * sizeof(value_type));
        delete[] pStart;
        pStart = newStart;
        pFinish = newStart + copy_part;
        pEnd_of_Storage = newStart + new_size;
    }
    void resize(size_t new_size) {
        reserve(new_size);
        while (pFinish != pEnd_of_Storage) *(pFinish++) = value_type();
    }
    void push_back(const value_type val) {
        if (pFinish == pEnd_of_Storage) reserve((pEnd_of_Storage - pStart) << 1);
        *(pFinish++) = val;
    }
    void pop_back() {
        --pFinish;
        if ((pFinish - pStart) < ((pEnd_of_Storage - pStart) >> 1) && ((pEnd_of_Storage - pStart) >> 1) >= (size_t)4) 
            reserve((pEnd_of_Storage - pStart) >> 1);
    }
    void swap(dynamic_array<value_type> &b) {
        swap(pStart, b.pStart);
        swap(pFinish, b.pFinish);
        swap(pEnd_of_Storage, b.pEnd_of_Storage);
    }
};