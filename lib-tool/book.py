import os
import random
Tmp_Directory = 'C:\\Users\\xyz\\AppData\\Local\\Temp\\'

book_info = {
    'ynyh': {
        'name': 'Yi Nian Yong Heng',
        'short name': 'ynyh',
        'source': 'https://jueshitangmen.info/',
        'prefix': 'https://jueshitangmen.info/23/',
        'encoding': 'gb2312',
        'menu': 'C:\\Users\\xyz\\AppData\\Local\\Temp\\source-ynyh-menu.txt'
    },
    'lwcs': {
        'name': 'Long Wang Chuan Shuo',
        'short name': 'lwcs',
        'source': 'https://jueshitangmen.info/',
        'prefix': 'https://jueshitangmen.info/22/',
        'encoding': 'gb2312',
        'menu': 'C:\\Users\\xyz\\AppData\\Local\\Temp\\lwcs_menu.src'
    },
    'sldss': {
        'name': 'Shan Liang De Si Shen',
        'short name': 'sldss',
        'source': 'https://jueshitangmen.info/',
        'prefix': 'https://jueshitangmen.info/18/',
        'encoding': 'gb2312',
        'menu': 'C:\\Users\\xyz\\AppData\\Local\\Temp\\sldss_menu.src'
    }
}


def load_menu(Book: dict) -> list:
    result = []
    with open(Book['menu'], encoding = Book['encoding']) as f:
        n = int(f.readline())
        for i in range(n):
            ln = f.readline()
            pos = ln.find(' ')
            result.append((ln[:pos], ln[pos + 1:-1]))
    return result

def get_tmp_file() -> str:
    unique_id = ''
    for i in range(8):
        unique_id += random.choice('abcdefghijklmnopqrstuvwxyz')
    return f'{Tmp_Directory}\\tmp-output-{unique_id}.txt'

def load_web(Url: str) -> str:
    # print(f"Url: {Url}")
    ff = get_tmp_file()
    os.system(f'powershell irm {Url} >{ff}')
    with open(ff) as f:
        s = ''
        while f.readline() != '    <div id="content">\n':
            pass
        while True:
            now = f.readline()
            if now == '    <div class="bottem2">\n':
                break
            s += now
    os.system(f'rm {ff}')
    s = s[:s.find('</p><p>')]
    s = s.replace('\t', '').replace('<p>', '').replace('</p>', '')
    return s

def get_content(name: str, chapter: int) -> str:
    info = book_info[name]
    title = load_menu(info)[chapter + 1]
    return title[1] + '\n\n' + load_web(info['prefix'] + title[0] + '.html')

def show_ynyh(chapter: int):
    print(get_content('ynyh', chapter))

def show_lwcs(chapter: int):
    print(get_content('lwcs', chapter))

def show_sldss(chapter: int):
    print(get_content('sldss', chapter))

if __name__ == '__main__':
    c = int(input('请输入章编号: '))
    show_sldss(c)