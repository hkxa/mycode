import os, re, time

try:
    with open('./music/v1.0.log', 'a') as f:
        print("[start main]" if __name__ == '__main__' else "[import lib]", time.time(), file = f)
except FileNotFoundError:
    os.mkdir('music')
    with open('./music/v1.0.log', 'a') as f:
        print("[start main]" if __name__ == '__main__' else "[import lib]", time.time(), file = f)

print('Module require: <requests>, <fake_useragent>, <lxml>')
try:
    import requests
except ModuleNotFoundError:
    print("Wait a moment: Trying to install required python-module <requests>")
    os.system('pip install requests -q -q -q')
    import requests
try:
    from fake_useragent import UserAgent
except ModuleNotFoundError:
    print("Wait a moment: Trying to install required python-module <fake_useragent>")
    os.system('pip install fake_useragent -q -q -q')
    from fake_useragent import UserAgent
try:
    from lxml import etree
except ModuleNotFoundError:
    print("Wait a moment: Trying to install required python-module <lxml>")
    os.system('pip install lxml -q -q -q')
    from lxml import etree
print("Module require: OK")

pre = 'https://link.hhtjim.com/163/'

def download(ids: list):
    with open('./music/v1.0.log', 'a') as f:
        print("[call download]", time.time(), f'args={ids}', file = f)
    headers = { "User-Agent": UserAgent().chrome }
    for i in range(len(ids)):
        M_url = f'https://music.163.com/song?id={ids[i]}'
        response = requests.get(M_url, headers = headers)
        html = etree.HTML(response.text)

        music_info = html.xpath('//title/text()')
        music_name = music_info[0].split('-')[0][:-2]
        singer = music_info[0].split('-')[1][1:]

        full_name = music_name + ' - ' + singer

        music_url = pre + str(ids[i]) + '.mp3'
        music = requests.get(music_url).content
        with open('./music/' + full_name + '.mp3', 'wb') as file:
            file.write(music)
        print(f"正在下载第 {str(i + 1)} 首: {full_name} (id = {ids[i]})")

# def get_playlist(playlist_id):
#     headers = { "User-Agent": UserAgent().chrome }
#     M_url = f'https://music.163.com/playlist?id={playlist_id}'
#     response = requests.get(M_url, headers = headers)
#     html = etree.HTML(response.text)

#     playlist_info = html.xpath('//title/text()')
#     playlist_name = playlist_info[0].split('-')[0][:-2]

#     print(f'get_playlist: {playlist_name}')

if __name__ == '__main__':
    t = [int(x) for x in input('Input Song Ids(seperate with space):').split()]
    download(t)