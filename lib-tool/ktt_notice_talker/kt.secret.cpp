#include "ktnt_header.hpp"
#include "ktnt_secret.hpp"

int main(int argc, char **argv) {
    if (argc != 4 && argc != 5) {
        printf("Error: argument is not right!\nUsage: kt.secret <Token> <Operate> <String> [<OpId>]\n");
        return EXIT_FAILURE;
    }
    string token = argv[1], operate = argv[2], str = argv[3];
    int opid;
    if (argc == 4) opid = 1;
    else opid = atoi(argv[4]);
    if (token == "ktnt_vip_hgoi") printf("Welcome, VIP hgoi!\n");
    else if (token == "ktnt_svip_zy11") printf("Welcome, SVIP zy!\n");
    else {
        printf("Error: Token error!\n");
        return EXIT_FAILURE;
    } 
    if (operate == "encode") printf("Encode Result:\n%s\n", ktnt_codec.encode(str, opid).c_str());
    else if (operate == "decode") printf("Decode Result:\n%s\n", ktnt_codec.decode(str, opid).c_str());
    else {
        printf("Error: Operate error!\n");
        return EXIT_FAILURE;
    } 
    return EXIT_SUCCESS;
}