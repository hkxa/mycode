#include <bits/stdc++.h>
#include "ktnt_defines.h"
using namespace std;

int randint() {
    static mt19937 rnd(chrono::steady_clock::now().time_since_epoch().count());
    return rnd() & INT_MAX;
}

int randint(int l, int r) {
    return randint() % (r - l + 1) + l;
}

uint32_t randuint() {
    return ((uint32_t)randint()) << 1 | ((uint32_t)randint() & 1);
}

uint64_t randull() {
    return ((uint64_t)randuint()) << 32 | randuint();
}

int64_t randll() {
    return randull() >> 1;
}

string randstr(string content, int len) {
    string ret;
    while (len--) ret.push_back(content[randint(0, content.size() - 1)]);
    return ret;
}