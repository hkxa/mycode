#ifndef HEADER_FILE_ktnt_time_hpp
#define HEADER_FILE_ktnt_time_hpp

#include <bits/stdc++.h>
using namespace std;

class ktnt_time {
private:
public:
    ktnt_time() {}
    ~ktnt_time() {}
    double clock() {
        return (double)(::clock()) / CLOCKS_PER_SEC;
    }
} ktnt_clock;

#endif // #ifndef HEADER_FILE_ktnt_time_hpp