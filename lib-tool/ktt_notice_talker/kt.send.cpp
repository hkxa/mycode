#include "ktnt_header.hpp"
#include "ktnt_defines.h"
#include "ktnt_connectServer.hpp"

int main(int argc, char **argv) {
    if (argc == 1) {
        printf("Error: argument is not right!\nUsage: kt.send <message...>\n");
        return EXIT_FAILURE;
    }
    if (get_username() == KT_USERNAME_NULL) {
        printf("Error: Please setname first!\nUsage: kt.setname <your_username>\n");
        return EXIT_FAILURE;
    }
    string server_folder = sys.get_server_folder();
    if (sys.read_file(connect_folder(server_folder, "message_cnt.ktt_notice_talker")).second == -1) {
        printf("Error: can't visit server_folder (%s)!\n", server_folder.c_str());
        return EXIT_FAILURE;
    }
    string message = get_username() + ":";
    for (int id = 1; id < argc; id++) {
        message += " ";
        message += argv[id];
    }
    if (connectServer.connectServer() == -1) {
        printf("Error: can't connect server!\n");
        return EXIT_FAILURE;
    }
    int nowcnt = getcnt() + 1;
    writecnt(nowcnt);
    char road_to_message[207];
    sprintf(road_to_message, "%s\\%d%s", server_folder.c_str(), nowcnt, MSG_FILE_EXT_NAME);
    FILE *fp = fopen(road_to_message, "w");
    fprintf(fp, "%s", message.c_str());
    fclose(fp);
    connectServer.unlock();
    return EXIT_SUCCESS;
}