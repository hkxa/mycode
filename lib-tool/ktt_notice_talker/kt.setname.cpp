#include "ktnt_header.hpp"
#include "ktnt_defines.h"

int main(int argc, char **argv) {
    if (argc == 1 || argc > 2) {
        printf("Usage: kt.setname <new_username>\n");
        return EXIT_FAILURE;
    }
    if (sys.call("cd " LocalSaveDir).second) 
        sys.call("mkdir " LocalSaveDir);
    sys.write_file(NameSaveFile, argv[1]);
    printf("kt.setname: success! new username: %s\n", argv[1]);
    return EXIT_SUCCESS;
}