#ifndef HEADER_FILE_ktnt_sys_hpp
#define HEADER_FILE_ktnt_sys_hpp

#include <bits/stdc++.h>
#include <windows.h>
#include <conio.h>
#include "ktnt_defines.h"
#include "ktnt_secret.hpp"
#include "ktnt_random.hpp"
using namespace std;

class ktnt_sys {
private:
public:
    ktnt_sys() {}
    ~ktnt_sys() {}
    pair<string, int> read_file(string file_name) {
        string sRet;
        char ch;
        FILE *fin = fopen(file_name.c_str(), "r");
        if (fin == NULL) return make_pair(string(), -1);
        while (fscanf(fin, "%c", &ch) == 1) sRet += ch;
        fclose(fin);
        return make_pair(sRet, (int)sRet.size());
    }
    int write_file(string file_name, string content) {
        FILE *fout = fopen(file_name.c_str(), "w");
        if (fout == NULL) return -1;
        fprintf(fout, "%s", content.c_str());
        fclose(fout);
        return content.size();
    }
    pair<string, int> call(string cmd) {
        string sRet, TmpFile = randstr(charset_alnum, 32) + ".tmp_file";
        int nRet;
        char ch;
        cmd = cmd + " 1>" + TmpFile + " 2>&1";
        nRet = system(cmd.c_str());
        pair<string, int> ReadResult = read_file(TmpFile);
        system(("del " + TmpFile).c_str());
        if (ReadResult.second == -1) return make_pair(string("Read Output Failed"), nRet);
        return make_pair(ReadResult.first, nRet);
    }
    bool remove_file(string file_name) {
        return call("del \"" + file_name + "\"").second;
    }
    string get_server_folder() {
        pair<string, int> ReadResult = read_file(ServerFolderSaveFile);
        if (ReadResult.second == -1) return FOLDER;
        return ktnt_codec.decode(ReadResult.first);
    }
} sys;

#endif // #ifndef HEADER_FILE_ktnt_sys_hpp