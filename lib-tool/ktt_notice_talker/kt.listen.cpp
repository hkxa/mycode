#include "ktnt_header.hpp"
#include "ktnt_defines.h"
#include "ktnt_connectServer.hpp"

string server_folder = FOLDER;

string get_massage(int cnt) {
    char road_to_message[307];
    sprintf(road_to_message, "%s\\%d%s", server_folder.c_str(), cnt, MSG_FILE_EXT_NAME);
    pair<string, int> result = sys.read_file(road_to_message);
    if (result.second == -1) result.first = "Error: can't read message\n" + to_string(cnt);
    printf("[message_id %d] %s\n", cnt, result.first.c_str());
    return result.first;
}

int main(int argc, char **argv) {
    if (argc >= 2) {
        printf("Error: argument is not right!\nUsage: kt.listen\n");
        return EXIT_FAILURE;
    }
    server_folder = sys.get_server_folder();
    if (sys.read_file(connect_folder(server_folder, "message_cnt.ktt_notice_talker")).second == -1) {
        printf("Error: can't visit server_folder (%s)!\n", server_folder.c_str());
        return EXIT_FAILURE;
    }
    if (get_username() == ";NULL;") {
        printf("Error: Please setname first!\nUsage: kt.setname <your_username>\n");
        return EXIT_FAILURE;
    }
    printf("Listening...\n");
    pair<string, int> TmpCallSendJoinResult = sys.call("kt.send " + get_username() + " joined");
    if (TmpCallSendJoinResult.second) {
        printf("Warning: kt.send failed!\ninfo : %s\n", TmpCallSendJoinResult.first.c_str());
        return EXIT_FAILURE;
    }
    int cnt = getcnt();
    while (true) {
        Sleep(1000);
        if (connectServer.connectServer() == -1) {
            printf("Error: can't connect server_folder!\n");
            return EXIT_FAILURE;
        }
        int nowcnt = getcnt();
        if (cnt > nowcnt) {
            printf("Error: The cnt in the server error!");
            show_message("Error: The cnt in the server error!");
            return EXIT_FAILURE;
        }
        while (cnt < nowcnt) {
            Sleep(100);
            string message = get_massage(++cnt);
            show_message(message);
        }
        connectServer.unlock();
    }
    return EXIT_SUCCESS;
}