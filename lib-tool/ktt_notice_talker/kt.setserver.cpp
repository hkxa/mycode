#include "ktnt_header.hpp"
#include "ktnt_defines.h"
#include "ktnt_secret.hpp"

int main(int argc, char **argv) {
    if (argc == 1 || argc > 2) {
        printf("Usage: kt.setserver <new_server>\n");
        return EXIT_FAILURE;
    }
    string FactPath = ktnt_codec.decode(argv[1]);
    if (FactPath == "Decode Failed" || sys.read_file(connect_folder(FactPath, "message_cnt.ktt_notice_talker")).second == -1) {
        printf("Error: server not found (IDX: \"%s\")\n", argv[1]);
        return EXIT_FAILURE;
    }
    if (sys.call("cd " LocalSaveDir).second) 
        sys.call("mkdir " LocalSaveDir);
    sys.write_file(ServerFolderSaveFile, argv[1]);
    printf("kt.setserver: success! new server: %s\n", FactPath.c_str());
    return EXIT_SUCCESS;
}