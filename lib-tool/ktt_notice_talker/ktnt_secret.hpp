#ifndef HEADER_FILE_ktnt_secret_hpp
#define HEADER_FILE_ktnt_secret_hpp

#include <bits/stdc++.h>
#include "ktnt_defines.h"
using namespace std;

class FenceCode {
public:
    string encode(string s, const int k = 2) {
        if ((int)s.size() < k) return s;
        string ret;
        for (int i = 0; i < k; i++)
            for (int j = i; j < (int)s.size(); j += k)
                ret += s[j];
        return ret;
    }
    string decode(string s, const int k = 2) {
        if ((int)s.size() < k) return s;
        string ret;
        int from[k];
        from[0] = 0;
        for (int i = 1; i < k; i++)
            from[i] = from[i - 1] + (s.size() + k - i) / k;
        for (int i = 0; i < (int)s.size(); i++)
            ret += s[from[i % k] + i / k];
        return ret;
    }
};

class Base64_ZyVersion{
private:
    const char *char_set = "ZYzyqwertuioplkjhgfdsaxcvbnm1234560987QAXSWEDCVFRTGBNHMJKLUPIO+-";
    int convert_val[128];
    int to_uint(char orig) {
        return (int)((unsigned char&)orig);
    }
    int convert(char orig) {
        return convert_val[(int)orig];
    }
public:
    Base64_ZyVersion() {
        for (int i = 0; i < 64; i++) convert_val[(int)char_set[i]] = i;
    }
    string encode(string s) {
        int ex = (3 - s.size() % 3) % 3;
        string ret;
        for (int i = 0; i < ex; i++) s += char_set[i];
        for (size_t i = 0; i < s.size(); i += 3) {
            uint32_t val = to_uint(s[i]) | to_uint(s[i + 1]) << 8 | to_uint(s[i + 2]) << 16;
            ret.push_back(char_set[val & 63]);
            ret.push_back(char_set[(val >> 6) & 63]);
            ret.push_back(char_set[(val >> 12) & 63]);
            ret.push_back(char_set[(val >> 18) & 63]);
        }
        if (ex == 1) ret += ':';
        else if (ex == 2) ret += '*';
        else ret += ';';
        return ret;
    }
    string decode(string s) {
        if (s.size() % 4 != 1) return "Decode Failed";
        char typ = s.back(); 
        s.pop_back();
        string ret;
        for (size_t i = 0; i < s.size(); i += 4) {
            uint32_t val = convert(s[i]) | (convert(s[i + 1]) << 6) | (convert(s[i + 2]) << 12) | (convert(s[i + 3]) << 18);
            ret.push_back((char)(val & 255));
            ret.push_back((char)((val >> 8) & 255));
            ret.push_back((char)((val >> 16) & 255));
        }
        if (typ == ':') ret.pop_back();
        else if (typ == '*') ret.pop_back(), ret.pop_back();
        else if (typ != ';') return "Decode Failed";
        return ret;
    }
};

class code_created_by_zy_1 {
private:
    uint32_t gen_next(uint64_t &x) {
        x ^= x << 13;
        x ^= x >> 17;
        x ^= x << 5;
        return (x >> 32) ^ (x & ((1ll << 32) - 1));
    }
public:
    string encode(string s, uint64_t x = 19260817, string code = charset_alnum + "@encode@::OIer_(^_^)Thanks") {
        string ret;
        for (size_t i = 0; i < s.size(); i++) {
            ret += s[i];
            ret += code[gen_next(x) % code.size()];
        }
        return ret;
    }
    string decode(string s) {
        string ret;
        for (size_t i = 0; i < s.size(); i += 2)
            ret += s[i];
        return ret;
    }
};

class ktnt_secret {
private:
public:
    ktnt_secret() {}
    ~ktnt_secret() {}
    FenceCode fence;
    Base64_ZyVersion base64_zy;
    code_created_by_zy_1 zy1;
    string encode(string s, int encode_type = 1) {
        switch (encode_type) {
            case 1:
                s = base64_zy.encode(s);
                s = zy1.encode(s, 20170933, charset_print);
                s = fence.encode(s, 2);
                reverse(s.begin(), s.end());
                s = base64_zy.encode(s);
                s = zy1.encode(s);
                s = fence.encode(s, 5);
                break;
            case 2:
                s = base64_zy.encode(s);
                s = fence.encode(s, 11);
                s = zy1.encode(s, 20170933, charset_print);
                s = fence.encode(s, 2);
                reverse(s.begin(), s.end());
                s = fence.encode(s, 5);
                s = base64_zy.encode(s);
                s = fence.encode(s, 3);
                s = zy1.encode(s);
                s = fence.encode(s, 7);
                break;
            default:
                s = "Invalid encode_type! encode failed!";
                break;
        }
        return s;
    }
    string decode(string s, int decode_type = 1) {
        switch (decode_type) {
            case 1:
                s = fence.decode(s, 5);
                s = zy1.decode(s);
                s = base64_zy.decode(s);
                reverse(s.begin(), s.end());
                s = fence.decode(s, 2);
                s = zy1.decode(s);
                s = base64_zy.decode(s);
                break;
            case 2:
                s = fence.decode(s, 7);
                s = zy1.decode(s);
                s = fence.decode(s, 3);
                s = base64_zy.decode(s);
                s = fence.decode(s, 5);
                reverse(s.begin(), s.end());
                s = fence.decode(s, 2);
                s = zy1.decode(s);
                s = fence.decode(s, 11);
                s = base64_zy.decode(s);
                break;
            default:
                s = "Invalid decode_type! decode failed!";
                break;
        }
        return s;
    }
} ktnt_codec;

#endif // #ifndef HEADER_FILE_ktnt_secret_hpp