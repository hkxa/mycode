#ifndef HEADER_FILE_ktnt_defines_h
#define HEADER_FILE_ktnt_defines_h

#include <string>

#define LocalSaveDir "C:\\ktt_client"
#define NameSaveFile LocalSaveDir "\\ktnt_name.ktnt-db"
#define ServerFolderSaveFile LocalSaveDir "\\ktnt_server_folder.ktnt-db"

#define FOLDER "\\\\hgoi05\\ktt_server"
#define LOCK_FILE "server.lock"
#define MSG_CNT_FILE "message_cnt.ktt_notice_talker"
#define MSG_FILE_EXT_NAME ".ktt_notice_talker-message"

#define KT_USERNAME_NULL ";NULL;"

const std::string charset_lower = "abcdefghijklmnopqrstuvwxyz";
const std::string charset_upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
const std::string charset_digit = "0123456789";
const std::string charset_symbol = "`~!@#$^&-_=+/*<>(){};:'\",.?\\|";
const std::string charset_alpha = charset_lower + charset_upper;
const std::string charset_alnum = charset_alpha + charset_digit;
const std::string charset_print = charset_alnum + charset_symbol;

#endif // #ifndef HEADER_FILE_ktnt_defines_h