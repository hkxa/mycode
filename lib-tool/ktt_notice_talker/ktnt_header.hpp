#ifndef HEADER_FILE_ktnt_header_hpp
#define HEADER_FILE_ktnt_header_hpp

#include <bits/stdc++.h>
#include <windows.h>
#include <conio.h>
#include "ktnt_defines.h"
#include "ktnt_sys.hpp"
using namespace std;

string connect_folder(string FaFolder, string SonElement) {
    if (FaFolder.back() == '\\') return FaFolder + SonElement;
    else return FaFolder + '\\' + SonElement;
}

string msg_file() {
    return connect_folder(sys.get_server_folder(), MSG_CNT_FILE);
}

string lock_file() {
    return connect_folder(sys.get_server_folder(), LOCK_FILE);
}

int system_no_message(string cmd) {
    return sys.call(cmd).second;
}

string get_username() {
    pair<string, int> result = sys.read_file(NameSaveFile);
    if (result.second == -1) return ";NULL;";
    return result.first;
}

bool check_if_the_message_is_by_self(string message) {
    string name = get_username();
    int name_len = name.length();
    if (message.length() < name_len) return false;
    return message.substr(0, name_len) == name;
}

void show_message(string message, string header = "kt.listen") {
    if (check_if_the_message_is_by_self(message)) return;
    MessageBox(NULL, message.c_str(), header.c_str(), MB_OK);
}

string ito_string(int val) {
    stringstream ss;
    ss << val;
    return ss.str();
}

int getcnt() {
    string file = sys.read_file(msg_file()).first;
    return atoi(file.c_str());
}

void writecnt(int cnt) {
    sys.write_file(msg_file(), to_string(cnt));
}

#endif // #ifndef HEADER_FILE_ktnt_header_hpp