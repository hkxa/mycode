#ifndef HEADER_FILE_ktnt_connectServer_hpp
#define HEADER_FILE_ktnt_connectServer_hpp

#include <bits/stdc++.h>
#include "ktnt_time.hpp"
#include "ktnt_sys.hpp"
#include "ktnt_defines.h"
using namespace std;

class ktnt_connectServer {
private:
public:
    ktnt_connectServer() {}
    ~ktnt_connectServer() {}
    int lock() {
        return sys.write_file(lock_file(), "");
    }
    void unlock() {
        sys.remove_file(lock_file());
    }
    bool connectServer(double WaitTimeMax = 8) {
        double from = ktnt_clock.clock();
        FILE *fp = fopen(lock_file().c_str(), "r");
        while (fp) {
            fclose(fp);
            Sleep(rand() % 100);
            if (ktnt_clock.clock() - from > WaitTimeMax) {
                printf("[ktnt_connectServer] An error occur (from: lock tag in the server)\n[ktnt_connectServer] Fixed.\n");
                show_message("An error occur (from: lock tag in the server)\nFixed.\n", "ktnt.connectServer");
                return lock();
            }
            fp = fopen(lock_file().c_str(), "r");
        }
        return lock();
    }
} connectServer;

#endif // #ifndef HEADER_FILE_ktnt_connectServer_hpp