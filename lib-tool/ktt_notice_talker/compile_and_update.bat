echo off
echo ============================================
echo ============== ktnt compile ================
echo ============================================
echo 1. Compile kt.listen.cpp
g++ kt.listen.cpp -o kt.listen.exe -std=c++11
echo 2. Compile kt.send.cpp
g++ kt.send.cpp -o kt.send.exe -std=c++11
echo 3. Compile kt.setname.cpp
g++ kt.setname.cpp -o kt.setname.exe -std=c++11
echo 4. Compile kt.setserver.cpp
g++ kt.setserver.cpp -o kt.setserver.exe -std=c++11
echo 5. Compile kt.secret.cpp
g++ kt.secret.cpp -o kt.secret.exe -std=c++11
echo ============================================
echo =========== ktnt server update =============
echo ============================================
copy *.exe D:\ktt_server\ /Y
echo ============================================
echo on