echo off
cls
echo ===================================================
echo ==================== kt.update ====================
echo ===================================================

set server_folder=\\hgoi05\\ktt_server

echo 1. Update kt.listen
copy %server_folder%\kt.listen.exe kt.listen.exe >info.file 2>err.info
if errorlevel 1 ( echo    Error: can't update kt.listen )
echo 2. Update kt.send
copy %server_folder%\kt.send.exe kt.send.exe >info.file 2>err.info
if errorlevel 1 ( echo    Error: can't update kt.send )
echo 3. Update kt.setname
copy %server_folder%\kt.setname.exe kt.setname.exe >info.file 2>err.info
if errorlevel 1 ( echo    Error: can't update kt.setname )
echo 4. Update kt.setserver
copy %server_folder%\kt.setserver.exe kt.setserver.exe >info.file 2>err.info
if errorlevel 1 ( echo    Error: can't update kt.setserver )
echo 5. Update kt.secret
copy %server_folder%\kt.secret.exe kt.secret.exe >info.file 2>err.info
if errorlevel 1 ( echo    Error: can't update kt.secret )

echo Update Info:
type %server_folder%\update_info.txt >info.file 2>err.info
if errorlevel 1 ( echo    Error: can't get update info! ) else ( type info.file & copy info.file update_info.txt >err.info 2>&1 )

del info.file err.info

echo update finished.
pause