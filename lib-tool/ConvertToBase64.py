import base64
from io import BytesIO

def get_file_content(name: str, way: str = "rb") -> str:
    f = open(name, way)
    return f.read()

def get_extension_name(name: str) -> str:
    p = len(name) - 1
    while name[p] != '.':
        p -= 1  
    return name[p + 1 : len(name)]

def get_extension(name: str) -> (str, str):
    extension_name = get_extension_name(name)
    if extension_name in ["png", "ico", "jpg", "jpeg", "gif", "bmp", "svg"]:
        return ("image", extension_name)
    else:
        return ("text", extension_name)

def get_noExtension_name(name: str) -> str:
    p = len(name) - 1
    while name[p] != '.':
        p -= 1
    return name[0 : p]

def convert_to_str(data: bytes) -> str:
    ss = str(data)
    return ss[2 : len(ss) - 1]

file_name = input("请输入要转码的文件路径: ")
prefix = "data:%s/%s;base64,"%get_extension(file_name)
data = base64.b64encode(get_file_content(file_name))
TXT_name = get_noExtension_name(file_name) + '(' + get_extension_name(file_name) + ") - base64.txt"
ouf = open(TXT_name, "w")
ouf.write(prefix + convert_to_str(data))
ouf.close()
print("转码结果已经输出到文件: \"" + TXT_name + "\" 中")