#include <bits/stdc++.h>
using namespace std;

bool isprime(long long num) {
    if (num == 1) return 0;
    long long s = sqrt(num);
    for (long long i = 2; i <= s; i++)
        if (num % i == 0) return 0;
    return 1;
}

int main(int argc, char **argv)
{
    if (argc <= 1) {
        printf("Receive nothing.\n");
        return 1;
    }
    for (int i = 1; i < argc; i++) {
        long long num = atoll(argv[i]);
        if (isprime(num))
            printf("%lld is a prime\n", num);
        else 
            printf("%lld isn't a prime\n", num);
    }
	return 0;
} 
