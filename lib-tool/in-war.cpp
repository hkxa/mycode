#include <stdio.h>
#include <time.h>
#include <string.h>
#include <iostream>
#include <algorithm>
#include <windows.h> 
#include <conio.h> 
#include <iostream>
// #include <afxinet.h>
#include <wininet.h>
// #pragma comment( lib, "wininet.lib" )
#define Max_N 103
typedef long long int64;

#define DownloadFile DLF::DownloadFileAndSaveToLocal1
using namespace std;

// namespace DLF {
// 	#define MAXBLOCKSIZE 1024
// 	using namespace std;
// 	bool DownloadFileAndSaveToLocal1(char *url, char *localPath) {
// 		LPCSTR newLocalPathName[256];
// 		memset(newLocalPathName, 0, sizeof(newLocalPathName));
// 		MultiByteToWideChar(CP_ACP, 0, localPath, strlen(localPath) + 1, newLocalPathName, sizeof(newLocalPathName) / sizeof(newLocalPathName[0]));
// 		LPCSTR wszUrlName[256];
// 		LPCSTR wszPathName[1024];
// 		memset(wszUrlName, 0, sizeof(wszUrlName));
// 		memset(wszPathName, 0, sizeof(wszPathName));
// 		MultiByteToWideChar(CP_ACP, 0, url, strlen(url) + 1, wszUrlName,
// 							sizeof(wszUrlName) / sizeof(wszUrlName[0]));
// 		MultiByteToWideChar(CP_ACP, 0, url, strlen(url) + 1, wszPathName,
// 							sizeof(wszPathName) / sizeof(wszPathName[0]));

// 		HINTERNET internetOpen = InternetOpen(NULL, INTERNET_OPEN_TYPE_DIRECT, NULL, NULL, 0);
// 		if (internetOpen == NULL)
// 			return FALSE;
// 		HINTERNET internetOpenUrl = InternetOpenUrl(internetOpen, wszUrlName, NULL, 0, INTERNET_FLAG_TRANSFER_BINARY | INTERNET_FLAG_PRAGMA_NOCACHE, 0);
// 		if (internetOpenUrl == NULL) {
// 			InternetCloseHandle(internetOpen);
// 			return FALSE;
// 		}
// 		DWORD dwStatusCode;
// 		DWORD dwStatusSize = sizeof(dwStatusCode);
// 		HttpQueryInfo(internetOpenUrl, HTTP_QUERY_STATUS_CODE | HTTP_QUERY_FLAG_NUMBER, &dwStatusCode, &dwStatusSize, NULL);
// 		if (dwStatusCode != 200) {
// 			InternetCloseHandle(internetOpenUrl);
// 			InternetCloseHandle(internetOpen);
// 			return FALSE;
// 		}
// 		DWORD m_iSize;
// 		DWORD dwLengthSize = sizeof(m_iSize);
// 		HttpQueryInfo(internetOpenUrl, HTTP_QUERY_CONTENT_LENGTH | HTTP_QUERY_FLAG_NUMBER, &m_iSize, &dwLengthSize, NULL);
// 		if (m_iSize < 0) {
// 			InternetCloseHandle(internetOpenUrl);
// 			InternetCloseHandle(internetOpen);
// 			return FALSE;
// 		}
// 		//HANDLE createFile = CreateFile(wszPathName, GENERIC_WRITE | GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
// 		//HANDLE createFile = CreateFile(newLocalPathName, GENERIC_WRITE, 0, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
// 		HANDLE createFile = CreateFile(newLocalPathName, GENERIC_WRITE | GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
// 		if (createFile == INVALID_HANDLE_VALUE)
// 			return FALSE;
// 		char buffer[100000];
// 		memset(buffer, 0, sizeof(buffer));
// 		DWORD byteRead = 0;
// 		BOOL hwrite;
// 		DWORD written;
// 		int a = 0;
// 		if (internetOpenUrl != NULL) {
// 			while (true) {
// 				InternetReadFile(internetOpenUrl, buffer, sizeof(buffer), &byteRead);
// 				if (byteRead == 0) break;
// 				hwrite = WriteFile(createFile, buffer, byteRead, &written, NULL);
// 				if (hwrite == 0) break;
// 			}
// 		}
// 		InternetCloseHandle(internetOpenUrl);
// 		InternetCloseHandle(internetOpen);

// 		return TRUE;
// 	}
// } // namespace DLF

template <typename Int>
Int get() {
	char ch = getchar();
	while ((ch > '9') || (ch < '0')) ch = getchar();
	Int a = (ch & 15);
	ch = getchar();
	while ((ch <= '9') && (ch >= '0')) {
		a = (a << 3) + (a << 1) + (ch & 15);
		ch = getchar();
	}
	return a;
}

int n;
int a[Max_N] = {0};
int f[Max_N][Max_N] = {0};
string user_name;

void firstRun() {
	system("cls");
	FILE *tmp;
	tmp = fopen("C:\\ProgramData\\in-war\\V0-0-1.in-war", "r");
	if (tmp != NULL) {
		fclose(tmp);
		return;
	}
	tmp = fopen("C:\\ProgramData\\in-war\\alls.in-war", "r");
	bool had = 0;
	if (tmp != NULL) {
		had = 1;
		fclose(tmp);
		return;
	}
	printf("\n\t检测到你的电脑上尚未进行V0.0.1环境配置,是否开始环境配置?(Y/n)");
	char ch;
	ch = getch();
	if (ch == 'n' || ch == 'N') {
		printf("\n正在退出配置..."); 
		Sleep(3000);
		exit(0);
	}
	if (ch != 'y' && ch != 'Y') {
		printf("\n检测到不可读字符!"); 
		Sleep(3000);
		exit(3);
	}
	system("cls"); Sleep(5000);
	printf("\n");
	for (int i = 1; i <= 3; i++) {
		printf("\n\t please waiting now     "); Sleep(97);
		printf("\r\t please waiting now.    "); Sleep(97);
		printf("\r\t please waiting now..   "); Sleep(97);
		printf("\r\t please waiting now...  "); Sleep(97);
		printf("\r\t please waiting now.... "); Sleep(97);
		printf("\r\t please waiting now....."); Sleep(97);
	}
	Sleep(1000);
	system("cls"); Sleep(1000);
	printf("in-war 初始化程序\n");
	printf("\n正在新建配置文件夹...");
	system("md C:\\ProgramData\\in-war"); Sleep(100);
	printf("\n正在设置in-war标志... (1/2)");
	fclose(fopen("C:\\ProgramData\\in-war\\alls.in-war", "w")); Sleep(100);
	printf("\r正在设置in-war标志... (2/2)");
	fclose(fopen("C:\\ProgramData\\in-war\\V0-0-1.in-war", "w")); Sleep(100);
	printf("\n正在建立一级目录结构... (1/3)");
	system("md C:\\ProgramData\\in-war\\V0-0-1"); Sleep(370);
	printf("\r正在建立一级目录结构... (2/3)");
	system("md C:\\ProgramData\\in-war\\V0-0-1\\users"); Sleep(370);
	printf("\r正在建立一级目录结构... (3/3)");
	if (!had) system("md C:\\ProgramData\\in-war\\timeCount"); Sleep(370);
	printf("\n正在初始化main用户... (1/4)");
	system("md C:\\ProgramData\\in-war\\V0-0-1\\users\\main"); Sleep(370);
	printf("\r正在初始化main用户... (2/4)");
	tmp = fopen("C:\\ProgramData\\in-war\\V0-0-1\\users\\main\\.code", "w"); Sleep(100);
	printf("\r正在初始化main用户... (3/4)");
	fprintf(tmp, "123456"); Sleep(1000);
	printf("\r正在初始化main用户... (4/4)");
	fclose(tmp); Sleep(100);
	printf("\n正在初始化软件注册信息... (1/3)");
	if (!had) tmp = fopen("C:\\ProgramData\\in-war\\timeCount\\.finish", "w"); Sleep(100);
	printf("\r正在初始化软件注册信息... (2/3)");
	if (!had) fprintf(tmp, "%lld", time(0) + 60 * 60 * 24 * 7); Sleep(1000);
	printf("\r正在初始化软件注册信息... (3/3)");
	if (!had) fclose(tmp); Sleep(100);
	printf("\n\n请重新运行以应用配置!");
	while (1) getchar();
	return;
}

void startRun()
{
	system("title 军团大战 (in-war) V0.0.1");
	system("cls");
	printf(
		"\n"
		"\n"
		"\n"
		"\n"
		"\t\t┏━━━━┓┏━━━━┓　　　┓　　　┣┓┃　  \n"
		"\t\t┏━┣━━┓┃　　　┓┃┗━━┣━┛　┃　┣┻┓\n"
		"\t\t╭━┛┃　　┃━┳━┣┃　　　┃　　┏┻┓┃　　\n"
		"\t\t┗━━┣━┛┃　┃　┃┃　　　┃　　┃　┃┣━╯\n"
		"\t\t┏━━┣━┓┃━╯╰┛┃　　　┃━╮┃　┃┃　╮\n"
    	"\t\t  　　┛　　┗━━━━╯┗━━╯　┛┗━╯╰━┛\n"
	); // prepare pic
	Sleep(1000);
	printf(
    	"\n"
    	"\t\tLoading..."
	); // prepare loading
	Sleep(2000);
}

void testOut()
{
	system("cls");
	FILE *tmp = fopen("C:\\ProgramData\\in-war\\timeCount\\.finish", "r");
	int64 end;
	fscanf(tmp, "%lld", &end);
	fclose(tmp);
	if (~end && time(0) > end) {
		printf("已到期！输入密钥以继续: ");
		string key;
		cin >> key;
		if (key != "in-war-register-FD45GT61928U85RHX") {
			printf("密钥错误！");
			Sleep(3000);
			exit(3); 
		} else {
			tmp = fopen("C:\\ProgramData\\in-war\\timeCount\\.finish", "w");
			fprintf(tmp, "-1");
			fclose(tmp);
		}
	}
	Sleep(2000);
}

void testVer()
{
	// if (DownloadFile(
	// 	"https://files-cdn.cnblogs.com/files/hkxadpall/in-war.ver.xap", 
	// 	"C:\\temp\\in-war-new_version.ver")) {
	// 		// Do;
	// }
}

void Login()
{
	Start_Login:;
	system("cls");
	printf("提醒: 试用版仅可以试用7天, 超出期限后可以找洛谷@航空信奥 (uid=63720) 以获得正式版密钥。\n");
	printf("提醒: 如果你是第一次使用的玩家, 请使用昵称为 main , 密码为 123456 的账户进行登录。(后续如果需要其他账号, 可以使用 main 账户登录后在设置页面添加【此功能预计V0.0.4上线，暂不可用】)\n\n");
	printf("Nickname: ");
	string name;
	cin >> name;
	printf("Secret Code: ");
	char codech[101], t; int i = 0;
	while ((t = getch()) != 13 || i == 0) {
		if (t == 13) continue;
		if (t == '\b') {
			--i;
			printf("\b \b");
		}
		else {
			codech[i++] = t;
			printf("*");
		}
	}
	printf("\n");
	codech[i] = 0;
	string code = codech;
	string dir = "C:\\ProgramData\\in-war\\V0-0-1\\users\\" + name + "\\.code";
	FILE *tmp = fopen(dir.c_str(), "r");
	if (tmp == 0) {
		printf("\n用户名或密码错误!");
		Sleep(1000);
		goto Start_Login;
	}
	char trulyCodech[103];
	fscanf(tmp, "%s", trulyCodech);
	string trulyCode = trulyCodech;
	if (code != trulyCode) {
		printf("\n用户名或密码错误!");
		Sleep(1000);
		goto Start_Login;
	}
	printf("\n登陆成功！");
	user_name = name;
	Sleep(2000);
}

void pretreat_load_saves(string dir) {
	FILE *f = fopen(dir.c_str(), "w");
	for (int i = 1; i <= 6; i++) fprintf(f, "空存档 -1\n");
	fclose(f);
}

void try_to_load_saves() {
	string dir = "C:\\ProgramData\\in-war\\V0-0-1\\users\\" + user_name + "\\.sav-list";
	FILE *f = fopen(dir.c_str(), "r");
	if (!f) {
		pretreat_load_saves(dir);
		f = fopen(dir.c_str(), "r");
	}
	char TMP_CHAR_BUFFER[107];
	string saved_name[6];
	int saved_LV[6];
	fscanf(f, "%s%d", TMP_CHAR_BUFFER, saved_LV + 0); saved_name[0] = TMP_CHAR_BUFFER; 
	fscanf(f, "%s%d", TMP_CHAR_BUFFER, saved_LV + 1); saved_name[1] = TMP_CHAR_BUFFER; 
	fscanf(f, "%s%d", TMP_CHAR_BUFFER, saved_LV + 2); saved_name[2] = TMP_CHAR_BUFFER; 
	fscanf(f, "%s%d", TMP_CHAR_BUFFER, saved_LV + 3); saved_name[3] = TMP_CHAR_BUFFER; 
	fscanf(f, "%s%d", TMP_CHAR_BUFFER, saved_LV + 4); saved_name[4] = TMP_CHAR_BUFFER; 
	fscanf(f, "%s%d", TMP_CHAR_BUFFER, saved_LV + 5); saved_name[5] = TMP_CHAR_BUFFER;
	printf("\n");
	printf("\t存档1: %s LV.%d\n", saved_name[0].c_str(), max(saved_LV[0], 0));
	printf("\t存档2: %s LV.%d\n", saved_name[1].c_str(), max(saved_LV[1], 0));
	printf("\t存档3: %s LV.%d\n", saved_name[2].c_str(), max(saved_LV[2], 0));
	printf("\t存档4: %s LV.%d\n", saved_name[3].c_str(), max(saved_LV[3], 0));
	printf("\t存档5: %s LV.%d\n", saved_name[4].c_str(), max(saved_LV[4], 0));
	printf("\t存档6: %s LV.%d\n", saved_name[5].c_str(), max(saved_LV[5], 0));
    int saved_id;
    GOTOTAG_try_to_load_saves_INPUT_SAVED_ID:;
	printf("\n"
		   "\t\t请输入使用的存档编号: ");
    saved_id = get<int>();
    if (saved_id > 6 || saved_id <= 0) {
        printf("\t\tInvalid ID!");
        goto GOTOTAG_try_to_load_saves_INPUT_SAVED_ID;
    }
    --saved_id;
    if (saved_LV[saved_id] == -1) {
		printf("\t\t该存档是空存档\n");
		goto NewFileSaved;
	} else {
		printf("\t\t该存档不是空存档\n");
		int choice;
		GOTOTAG_try_to_load_saves_INPUT_CHOICE_SAVED_ID:;
		printf("\t\t请选择: 覆盖该存档(0), 或打开该存档(1)\n\t\t>>> ");
		choice = get<int>();
		if (choice < 0 || choice > 1) {
			printf("\t\tInvalid Choice!");
			goto GOTOTAG_try_to_load_saves_INPUT_CHOICE_SAVED_ID;
		}
		if (choice == 0) goto NewFileSaved;
		else goto OpenSaved;
	}
	NewFileSaved:;
	printf("\t\t新建存档中...\n");
	return;
	OpenSaved:;
	printf("\t\t读取存档中...\n");
	return;
}

int main()
{
	startRun();
	firstRun();
	testOut();
	testVer();
	Login();
	system("cls");
	try_to_load_saves();
	system("pause");
}
