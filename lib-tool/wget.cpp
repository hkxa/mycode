#include <stdio.h>
#include <windows.h>
#include <conio.h>
#include <bits/stdc++.h>

#ifdef URLDownloadToFile
#undef URLDownloadToFile
#endif
typedef int(__stdcall *UDF)(LPVOID, LPCSTR, LPCSTR, DWORD, LPVOID);
UDF URLDownloadToFile = (UDF)GetProcAddress(LoadLibrary("urlmon.dll"), "URLDownloadToFileA");

std::string get_name(std::string s) {
    while (s.back() == '/' || s.back() == '\\') s.pop_back();
    int p = s.size() - 1;
    while (s[p] != '/' && s[p] != '\\') --p;
    if (s.substr(p + 1).find('.') == s.npos) s += ".html";
    return s.substr(p + 1);
}

signed main(int argc, char **argv) {
    int cnt = 0, st = 1;
    bool quiet = false;
    if (argc > 1 && !strcmp(argv[1], "-q")) {
        quiet = true;
        ++st;
    }
    for (int i = st; i < argc; ++i) {
        std::string url = argv[i];
        if (!quiet) printf("wget: Downloading arg[%d]: %s ...", ++cnt, url.c_str());
        std::string name = get_name(url);
        if (i + 2 < argc && !strcmp(argv[i + 1], "-o")) {
            name = argv[i + 2];
            i += 2;
        }
        URLDownloadToFile(0, url.c_str(), name.c_str(), 0, 0);
        if (!quiet) printf(" (at %s)\n", name.c_str());
    }
    if (!cnt && !quiet) printf("wget: No input.\n");
    return 0;
}