// encode: *gb2312* 编码
#include <stdio.h>
#include <windows.h>
#include <conio.h>
#include <bits/stdc++.h>
typedef long long int64;
typedef unsigned uint32;

//////////////////////////////// <start> Difines
#define Version "v5.8.4"
// #pragma message("Compiling 163music " Version)
#define ConfigFileDir "C:\\ProgramData\\brealid_program\\download_163music\\"
#define ConfigFile "C:\\ProgramData\\brealid_program\\download_163music\\" Version ".conf"
#define NO_ENDL ('\0')
const std::string CharSet_lower = "abcdefghijklmnopqrstuvwxyz";
const std::string CharSet_upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
const std::string CharSet_digit = "0123456789";
const std::string CharSet_alpha = CharSet_lower + CharSet_upper;
const std::string CharSet_alnum = CharSet_alpha + CharSet_digit;
//////////////////////////////// <end> Difines

//////////////////////////////// <start> Download Library
#ifdef URLDownloadToFile
#undef URLDownloadToFile
#endif
typedef int(__stdcall *UDF)(LPVOID, LPCSTR, LPCSTR, DWORD, LPVOID);
UDF URLDownloadToFile = (UDF)GetProcAddress(LoadLibrary("urlmon.dll"), "URLDownloadToFileA");

void download(std::string url, std::string name) {
    URLDownloadToFile(0, url.c_str(), name.c_str(), 0, 0);
}
//////////////////////////////// <end> Download Library

//////////////////////////////// <start> Character-Coding Library
void unicodeToGB2312(const std::wstring &wstr, std::string &result) {
    int n = WideCharToMultiByte(CP_ACP, 0, wstr.c_str(), -1, 0, 0, 0, 0);
    result.resize(n);
    ::WideCharToMultiByte(CP_ACP, 0, wstr.c_str(), -1, (char*)result.c_str(), n, 0, 0);
}
 
void utf8ToUnicode(const std::string &src, std::wstring &result) {
    int n = MultiByteToWideChar(CP_UTF8, 0, src.c_str(), -1, NULL, 0);
    result.resize(n);
    ::MultiByteToWideChar(CP_UTF8, 0, src.c_str(), -1, (LPWSTR)result.c_str(), result.length());
}

std::string utf8_to_gb2312(const std::string &src) {
    std::wstring T;
    std::string result;
    utf8ToUnicode(src, T);
    unicodeToGB2312(T, result);
    return result;
}
//////////////////////////////// <end> Character-Coding Library

//////////////////////////////// <start> KT-library
namespace KT_random {
    struct KT_random_MUST {
        KT_random_MUST() { srand(time(0)); }
    } do_MUST;
    std::string gen_str(uint32 length, std::string seq = CharSet_alnum) {
        std::string ret;
        while (length--) ret.push_back(seq[rand() % seq.size()]);
        return ret;
    }
}

std::string int_to_str(int64 value) {
    std::stringstream ss;
    ss << value;
    return ss.str();
}

int64 str_to_int(std::string value) {
    std::stringstream ss(value);
    int64 ret = -1;
    ss >> ret;
    return ret;
}

std::string FormatTime(int tim) {
    if (tim < 1000) return int_to_str(tim) + "ms";
    if (tim < 60000) return int_to_str(tim / 1000) + "s " + int_to_str(tim % 1000) + "ms";
    if (tim < 3600000) return int_to_str(tim / 60000) + "min " + int_to_str(tim / 1000 % 60) + "s " + int_to_str(tim % 1000) + "ms";
    return int_to_str(tim / 3600000) + "h " + int_to_str(tim / 60000 % 60) + "min " + int_to_str(tim / 1000 % 60) + "s " + int_to_str(tim % 1000) + "ms";
}

bool if_same(std::string s1, std::string s2) {
    std::transform(s1.begin(), s1.end(), s1.begin(), ::tolower);
    std::transform(s2.begin(), s2.end(), s2.begin(), ::tolower);
    return s1 == s2;
}

namespace KT_sys {
    std::string closest_info;
    std::pair<std::string, int> read_file(std::string file_name) {
        std::string sRet;
        char ch;
        FILE *fin = fopen(file_name.c_str(), "r");
        if (fin == NULL) return std::make_pair(std::string(), -1);
        while (fscanf(fin, "%c", &ch) == 1) sRet += ch;
        fclose(fin);
        return make_pair(sRet, (int)sRet.size());
    }
    int write_file(std::string file_name, std::string content) {
        FILE *fout = fopen(file_name.c_str(), "w");
        if (fout == NULL) return -1;
        fprintf(fout, "%s", content.c_str());
        fclose(fout);
        return content.size();
    }
    int call(std::string command) {
        std::string output_file = KT_random::gen_str(10) + ".cmd_output";
        std::string call_command = command + " 1>" + output_file + " 2>&1";
        int nRet = system(call_command.c_str());
        call_command = "del " + output_file;
        closest_info = read_file(output_file).first;
        system(call_command.c_str());
        return nRet;
    }
    std::string download(std::string url, bool enable_utf8_to_gb2312 = false) {
        ::download(url, "KT_Lib_Download_Temp.txt");
        std::string xFile = read_file("KT_Lib_Download_Temp.txt").first;
        system("del KT_Lib_Download_Temp.txt");
        if (!enable_utf8_to_gb2312) return xFile;
        else return utf8_to_gb2312(xFile);
    }
}

std::string ReplaceStringChar(std::string s, char orig, char towards) {
    for (size_t i = 0; i < s.size(); ++i)
        if (s[i] == orig) s[i] = towards;
    return s;
}

std::string ReplaceStringStr(std::string s, std::string orig, std::string towards) {
    int len_orig = orig.size();
    std::string ret;
    for (size_t i = 0; i + len_orig - 1 < s.size(); ++i) {
        if (s.substr(i, len_orig) != orig) ret += s[i];
        else {
            ret += towards;
            i += len_orig - 1;
        }
    }
    return ret;
}

std::string MakeNameOK(std::string s) {
    s = ReplaceStringStr(s, "?", " ");
    s = ReplaceStringStr(s, "/", "、");
    s = ReplaceStringStr(s, ":", "：");
    // s = ReplaceStringChar(s, '?', '$');
    // s = ReplaceStringChar(s, '/', '-');
    // s = ReplaceStringChar(s, ':', '-');
    return s;
}
//////////////////////////////// <end> KT-library

void say(std::string info, char end = '\n') {
    std::cout << info;
    if (end) std::cout << end;
    std::cout << std::flush;
}

void ReadUntilEnd() {
    char c = '\0';
    while (c != '\n') scanf("%c", &c);
}

template <typename T>
T get() {
    T ret;
    std::cin >> ret;
    std::cin.clear();
    ReadUntilEnd();
    return ret;
}

template <typename T>
T get(std::ifstream &in) {
    T ret;
    in >> ret;
    // char ch = '\0';
    // while (ch != '\n') in >> ch;
    return ret;
}

struct Config {
    std::string lang, mode, name_format;
    void load_in() {
        std::ifstream fin(ConfigFile);
        lang = get<std::string>(fin);
        mode = get<std::string>(fin);
        getline(fin, name_format); // Ignore '\n'
        getline(fin, name_format);
    }
    void load_out() {
        std::ofstream fout(ConfigFile);
        fout << lang << '\n'
             << mode << '\n'
             << name_format << '\n';
    }
    void config_default() {
        lang = "cn";
        mode = "default";
        name_format = "{song} - {singer}";
        load_out();
    }
    void config_all() {
        lang = mode = "?";
        say("|~| Set Language (cn / en)");
        while (lang != "cn" && lang != "en") {
            say("|~| cn for Chinede(zh-CN), en for English(en): ");
            say("|~| >>> ", NO_ENDL);
            lang = get<std::string>();
            for (unsigned i = 0; i < lang.size(); ++i)
                lang[i] = ::tolower(lang[i]);
        }
        say("===============================================================");
        say("|~| Set Mode (Debug / Default)");
        while (mode != "debug" && mode != "default") {
            say("|~| default / debug: ");
            say("|~| >>> ", NO_ENDL);
            mode = get<std::string>();
            for (unsigned i = 0; i < mode.size(); ++i)
                mode[i] = ::tolower(mode[i]);
        }
        say("===============================================================");
        say("|~| Set Song-Name Format ({song} for song-name, {singer} for singer-name, {id} for song-id)");
        say("|~| Example: when song is Apple Tree by anonymous, '{song} - {singer}' is equal to 'Apple Tree - anonymous'");
        say("|~| Suggest: '{song} - {singer}'");
        say("|~| >>> ", NO_ENDL);
        getline(std::cin, name_format);
        say("===============================================================");
        load_out();
    }
    void config_menu() {
        say("===============================================================");
        say("|~| 1. Config Language(zh-CN / en)");
        say("|~| 2. Config Mode(debug / default)");
        say("|~| 3. Song-Name Format(now: '" + name_format + "')");
        say("|~| 100. Config All");
        say("|~| -1. Exit");
        int choice = 0;
        while (choice != 1 && choice != 2 && choice != 3 && choice != 100 && choice != -1) {
            say("|~| Input number to choose:");
            say("|~| >>> ", NO_ENDL);
            choice = get<int>();
        }
        if (choice == -1) return;
        if (choice == 1) {
            say("===============================================================");
            say("|~| Set Language (cn / en)");
            lang = "?";
            while (lang != "cn" && lang != "en") {
                say("|~| cn for Chinede(zh-CN), en for English(en): ");
                say("|~| >>> ", NO_ENDL);
                lang = get<std::string>();
                for (unsigned i = 0; i < lang.size(); ++i)
                    lang[i] = ::tolower(lang[i]);
            }
        }
        if (choice == 2) {
            say("===============================================================");
            say("|~| Set Mode (Debug / Default)");
            mode = "?";
            while (mode != "debug" && mode != "default") {
                say("|~| default / debug: ");
                say("|~| >>> ", NO_ENDL);
                mode = get<std::string>();
                for (unsigned i = 0; i < mode.size(); ++i)
                    mode[i] = ::tolower(mode[i]);
            }
        }
        if (choice == 3) {
            say("===============================================================");
            say("|~| Set Song-Name Format ({song} for song-name, {singer} for singer-name, {id} for song-id)");
            say("|~| Example: when song is Apple Tree by anonymous, '{song} - {singer}' is equal to 'Apple Tree - anonymous'");
            say("|~| Suggest: '{song} - {singer}'");
            say("|~| >>> ", NO_ENDL);
            getline(std::cin, name_format);
        }
        if (choice == 100) config_all();
        load_out();
    }
    Config() {
        say("===============================================================");
        FILE *f = fopen(ConfigFile, "r");
        if (f == NULL) {
            KT_sys::call("mkdir " ConfigFileDir);
            say("|~| Recoginized that you are the first time using the program in version " Version ",");
            say("|~| Would you like to config the program now?");
            std::string s_tmp;
            char op = '?';
            while (op != 'Y' && op != 'N') {
                say("|~| Y for yes, N for using default setting: ");
                say("|~| >>> ", NO_ENDL);
                s_tmp = get<std::string>();
                op = (s_tmp.length() == 1 ? s_tmp[0] : '?');
                if (op == 'y') op = 'Y';
                if (op == 'n') op = 'N';
            }
            say("===============================================================");
            if (op == 'Y') config_all();
            else config_default();
            say("===============================================================");
            return;
        }
        fclose(f);
        load_in();
    }
} conf;

std::string make_name(int id, std::string song, std::string singer) {
    std::string title = conf.name_format;
    while (true) {
        size_t p = title.find("{song}");
        if (p == title.npos) break;
        title.replace(p, 6, song);
    }
    while (true) {
        size_t p = title.find("{singer}");
        if (p == title.npos) break;
        title.replace(p, 8, singer);
    }
    while (true) {
        size_t p = title.find("{id}");
        if (p == title.npos) break;
        title.replace(p, 4, int_to_str(id));
    }
    return title;
}

struct Translater {
    std::string operator() (std::string info) {
        if (conf.lang == "cn") {
            if (info == "Song") return "歌曲";
            if (info == "Playlist") return "歌单";
            if (info == "Artist") return "歌手";
            if (info == "Album") return "专辑";
            if (info == "Total") return "总计";
            if (info == "download program") return "下载器";
            if (info == "163-music(cloudmusic) download program") return "(163)网易云音乐下载器";
            if (info == "by brealid") return "开发者: brealid";
            if (info == "open-source") return "开源";
            if (info == "Get Single Song") return "下载单曲";
            if (info == "Get Many Single Song") return "下载多首单曲";
            if (info == "Get Playlist Song") return "下载歌单";
            if (info == "Get Artist Song") return "下载歌手的歌曲";
            if (info == "Get Album Song") return "下载专辑";
            if (info == "Settings") return "设置";
            if (info == "Input number to choose") return "输入一个整数以选择";
            if (info == "Exit") return "退出";
            if (info == "This program will behaves undefined if you enter wrong id") return "如果你输入错误的ID，这个程序将会做未定义的行为";
            if (info == "Input Song-ID") return "输入歌曲 ID";
            if (info == "Input Playlist-ID") return "输入歌单 ID(歌单必须可见)";
            if (info == "Input Artist-ID") return "输入歌手 ID";
            if (info == "Input Album-ID") return "输入专辑 ID";
            if (info == "Input many ids, seperated with **enters**, end with -1") return "输入一些单曲 ID，用**回车**分隔，以 -1 结尾";
            if (info == "Warning: We can only download (at most) the first 10 song of the playlist for technical reasons.") return "警告: 因为技术原因我们最多只能下载歌单中的前 10 首歌";
            if (info == "Warning: We can only download (at most) the first 50 song of the artist for technical reasons.") return "警告: 因为技术原因我们最多只能下载歌手最热门的 50 首歌";
            // if (info == "Config Language(zh-CN / en)") return "设置语言(简体中文 / 英语)";
            // if (info == "Config Mode(debug / default)") return "设置模式(安静模式 / 默认模式)";
            // if (info == "Config All") return "设置所有项目";
            if (info == "Success") return "成功";
            if (info == "Failure") return "失败";
            if (info == "Input ID") return "输入ID";
            if (info == "Get ID from file") return "从文件中获取ID";
            if (info == "don't exist") return "不存在";
            if (info == "[Copyright-Restricted]163-cloudmusic doesn't have the music's copyright") return "版权受限(网易云音乐没有这首歌的版权)";
            if (info == "Please create a file contain ids, one id per line first.") return "请先创建一个文件，这个文件包含要下载的乐曲的id，一行一个id";
            if (info == "enter the name of the file(with its whole path)") return "输入一个文件名(建议为完整路径)";
            if (info == "Get Song From Local HTML(Playlist, Artist, Album, etc.)") return "从已下载的本地网页(歌单, 歌手, 专辑等)中读取歌曲";
            if (info == "Please download a file from https://music.163.com of Playlist, Artist, Album, etc..") return "从 https://music.163.com 中下载一个网页，包含歌单，歌手，专辑等";
            if (info == "Time cost") return "用时";
            if (info == "Sucessfully downloaded") return "成功下载了";
        }
        return info;
    }
} trans;

std::string GetSongName(int64 id) {
    std::string content = KT_sys::download("https://music.163.com/song?id=" + int_to_str(id), true);
    if (content.find("<i class=\"u-errlg u-errlg-404\"></i>") != content.npos) return "Song-404";
    size_t p1 = content.find("<title>");
    if (p1 >= content.size()) return trans("Song") + KT_random::gen_str(5);
    size_t p2 = content.find(" - ", p1 += 7u);
    if (p2 >= content.size()) return trans("Song") + KT_random::gen_str(5);
    return MakeNameOK(content.substr(p1, p2 - p1));
}

std::string GetSongSinger(int64 id) {
    std::string content = KT_sys::download("https://music.163.com/song?id=" + int_to_str(id), true);
    if (content.find("<i class=\"u-errlg u-errlg-404\"></i>") != content.npos) return "Singer-404";
    size_t p1 = content.find("<title>");
    if (p1 >= content.size()) return trans("Singer_") + KT_random::gen_str(5);
    p1 = content.find(" - ", p1);
    if (p1 >= content.size()) return trans("Singer_") + KT_random::gen_str(5);
    size_t p2 = content.find(" - ", p1 += 3u);
    if (p2 >= content.size()) return trans("Singer_") + KT_random::gen_str(5);
    return MakeNameOK(content.substr(p1, p2 - p1));
}

std::string GetPlaylistName(int64 id) {
    std::string content = KT_sys::download("https://music.163.com/playlist?id=" + int_to_str(id), true);
    if (content.find("<i class=\"u-errlg u-errlg-404\"></i>") != content.npos) return "Playlist-404";
    size_t p1 = content.find("<title>");
    if (p1 >= content.size()) return trans("Playlist_") + KT_random::gen_str(5);
    size_t p2 = content.find(" - ", p1 += 7u);
    if (p2 >= content.size()) return trans("Playlist_") + KT_random::gen_str(5);
    return MakeNameOK(content.substr(p1, p2 - p1));
}

std::string GetArtistName(int64 id) {
    std::string content = KT_sys::download("https://music.163.com/artist?id=" + int_to_str(id), true);
    if (content.find("<i class=\"u-errlg u-errlg-404\"></i>") != content.npos) return "Artist-404";
    size_t p1 = content.find("<title>");
    if (p1 >= content.size()) return trans("Artist_") + KT_random::gen_str(5);
    size_t p2 = content.find(" - ", p1 += 7u);
    if (p2 >= content.size()) return trans("Artist_") + KT_random::gen_str(5);
    return MakeNameOK(content.substr(p1, p2 - p1));
}

std::string GetAlbumName(int64 id) {
    std::string content = KT_sys::download("https://music.163.com/album?id=" + int_to_str(id), true);
    if (content.find("<i class=\"u-errlg u-errlg-404\"></i>") != content.npos) return "Album-404";
    size_t p1 = content.find("<title>");
    if (p1 >= content.size()) return trans("Album_") + KT_random::gen_str(5);
    size_t p2 = content.find(" - ", p1 += 7u);
    if (p1 >= content.size()) return trans("Album_") + KT_random::gen_str(5);
    p2 = content.find(" - ", p2 + 3u);
    if (p2 >= content.size()) return trans("Album_") + KT_random::gen_str(5);
    return MakeNameOK(content.substr(p1, p2 - p1));
}

std::string GetTitleFromLocalHTML(std::string file_name) {
    std::string content = KT_sys::read_file(file_name).first;
    size_t p1 = content.find("<title>");
    if (p1 >= content.size()) return trans("Unknown_Name_") + KT_random::gen_str(5);
    size_t p2 = content.find(" - ", p1 += 7u);
    if (p1 >= content.size()) return trans("Unknown_Name_") + KT_random::gen_str(5);
    p2 = content.find(" - ", p2 + 3u);
    if (p2 >= content.size()) return trans("Unknown_Name_") + KT_random::gen_str(5);
    return MakeNameOK(content.substr(p1, p2 - p1));
}

int GetSingleSong(int64 id) {
    // download("https://music.163.com/song/media/outer/url?id=" + int_to_str(id), GetSongName(id));
    std::string name = GetSongName(id), singer = GetSongSinger(id);
    std::string full_name = make_name(id, name, singer);
    if (name == "Song-404" || singer == "Singer-404") {
        say(trans("Get Single Song") + ": { song.id = " + int_to_str(id) + " }...", NO_ENDL);
        say(" " + trans("Failure") + " (" + trans("don't exist") + ")");
        return 404;
    }
    say(trans("Get Single Song") + ": " + full_name + "...", NO_ENDL);
    // if (conf.mode == "debug") fprintf(stderr, "[DEBUG] Source-Url: https://link.hhtjim.com/163/%s.mp3\n", int_to_str(id).c_str());
    // download("https://link.hhtjim.com/163/" + int_to_str(id) + ".mp3", full_name + ".mp3");
    download("https://music.163.com/song/media/outer/url?id=" + int_to_str(id), full_name + ".mp3");
    std::string music_content = KT_sys::read_file(full_name + ".mp3").first;
    if (music_content.length() < 15 || music_content.substr(0, 15) == "<!DOCTYPE html>") {
        say(" " + trans("Failure") + " (" + trans("[Copyright-Restricted]163-cloudmusic doesn't have the music's copyright") + ")");
        return -1;
    }
    // KT_sys::write_file(full_name + ".mp3", music_content);
    say(" " + trans("Success"));
    if (conf.mode == "debug") fprintf(stderr, "[DEBUG] Source-Url: https://music.163.com/song/media/outer/url?id=%s\n", int_to_str(id).c_str());
    // https://music.163.com/song/media/outer/url?id=25706282 
    // https://link.hhtjim.com/163/25706282.mp3
    // https://music.163.com/song?id=25706282 
    return 0;
}

void GetManySingleSong(std::vector<int64> v) {
    time_t StartDownload = clock();
    say(trans("Get Many Single Song") + "(" + trans("Total") + ": " + int_to_str(v.size()) + ")");
    int success = 0;
    for (size_t i = 0; i < v.size(); ++i) {
        printf("|~| %2d. ", int(i + 1));
        if (GetSingleSong(v[i]) == 0) ++success;
    }
    say("|~|------------------------------------------------------------");
    say("|~| " + trans("Sucessfully downloaded") + " " + int_to_str(success) + " " + trans("Song") + "(" + trans("Total") + ": " + int_to_str(v.size()) + ")");
    say("|~| " + trans("Time cost") + ": " + FormatTime(clock() - StartDownload));
}

void GetPlaylistSong(int64 id) {
    time_t StartDownload = clock();
    say(trans("Warning: We can only download (at most) the first 10 song of the playlist for technical reasons."));
    std::string name = GetPlaylistName(id);
    if (name == "Playlist-404") {
        say(trans("Get Playlist Song") + ": { playlist.id = " + int_to_str(id) + " }...", NO_ENDL);
        say(" " + trans("Failure") + " (" + trans("don't exist") + ")");
        return;
    }
    say(trans("Get Playlist Song") + ": " + name);
    if (conf.mode == "debug") fprintf(stderr, "[DEBUG] Source-Url: https://music.163.com/playlist?id=%s\n", int_to_str(id).c_str());
    std::string content = KT_sys::download("https://music.163.com/playlist?id=" + int_to_str(id), true);
    size_t i = 0, success = 0, cnt = 0;
    while (true) {
        i = content.find("<li><a href=\"/song?id=", i);
        if (i >= content.size()) break;
        size_t ed = content.find("\">", i += 22u);
        if (ed >= content.size()) break;
        int64 Song_ID = str_to_int(content.substr(i, ed - i));
        if (Song_ID <= 0) continue;
        printf("|~| %2d. ", int(++cnt));
        if (GetSingleSong(Song_ID) == 0) ++success;
    }
    say("|~|------------------------------------------------------------");
    say("|~| " + trans("Sucessfully downloaded") + " " + int_to_str(success) + " " + trans("Song") + "(" + trans("Total") + ": " + int_to_str(cnt) + ")");
    say("|~| " + trans("Time cost") + ": " + FormatTime(clock() - StartDownload));
    // download()
    // <li><a href="/song?id=
    // https://music.163.com/playlist?id=651630118
}

void GetArtistSong(int64 id) {
    time_t StartDownload = clock();
    say(trans("Warning: We can only download (at most) the first 50 song of the artist for technical reasons."));
    std::string name = GetArtistName(id);
    if (name == "Artist-404") {
        say(trans("Get Artist Song") + ": { artist.id = " + int_to_str(id) + " }...", NO_ENDL);
        say(" " + trans("Failure") + " (" + trans("don't exist") + ")");
        return;
    }
    say(trans("Get Artist Song") + ": " + name);
    if (conf.mode == "debug") fprintf(stderr, "[DEBUG] Source-Url: https://music.163.com/artist?id=%s\n", int_to_str(id).c_str());
    std::string content = KT_sys::download("https://music.163.com/artist?id=" + int_to_str(id), true);
    size_t i = 0, success = 0, cnt = 0;
    while (true) {
        i = content.find("<li><a href=\"/song?id=", i);
        if (i >= content.size()) break;
        size_t ed = content.find("\">", i += 22u);
        if (ed >= content.size()) break;
        int64 Song_ID = str_to_int(content.substr(i, ed - i));
        if (Song_ID <= 0) continue;
        printf("|~| %2d. ", int(++cnt));
        if (GetSingleSong(Song_ID) == 0) ++success;
    }
    say("|~|------------------------------------------------------------");
    say("|~| " + trans("Sucessfully downloaded") + " " + int_to_str(success) + " " + trans("Song") + "(" + trans("Total") + ": " + int_to_str(cnt) + ")");
    say("|~| " + trans("Time cost") + ": " + FormatTime(clock() - StartDownload));
    // download()
    // <li><a href="/song?id=
    // https://music.163.com/artist?id=12157336
    // https://music.163.com/artist?id=12957090
}

void GetAlbumSong(int64 id) {
    time_t StartDownload = clock();
    std::string name = GetAlbumName(id);
    if (name == "Album-404") {
        say(trans("Get Album Song") + ": { album.id = " + int_to_str(id) + " }...", NO_ENDL);
        say(" " + trans("Failure") + " (" + trans("don't exist") + ")");
        return;
    }
    say(trans("Get Album Song") + ": " + name);
    if (conf.mode == "debug") fprintf(stderr, "[DEBUG] Source-Url: https://music.163.com/album?id=%s\n", int_to_str(id).c_str());
    std::string content = KT_sys::download("https://music.163.com/album?id=" + int_to_str(id), true);
    size_t i = 0, success = 0, cnt = 0;
    while (true) {
        i = content.find("<li><a href=\"/song?id=", i);
        if (i >= content.size()) break;
        size_t ed = content.find("\">", i += 22u);
        if (ed >= content.size()) break;
        int64 Song_ID = str_to_int(content.substr(i, ed - i));
        if (Song_ID <= 0) continue;
        printf("|~| %2d. ", int(++cnt));
        if (GetSingleSong(Song_ID) == 0) ++success;
    }
    say("|~|------------------------------------------------------------");
    say("|~| " + trans("Sucessfully downloaded") + " " + int_to_str(success) + " " + trans("Song") + "(" + trans("Total") + ": " + int_to_str(cnt) + ")");
    say("|~| " + trans("Time cost") + ": " + FormatTime(clock() - StartDownload));
    // download()
    // <li><a href="/song?id=
    // https://music.163.com/album?id=2715905
    // 徐梦圆《曾经的纯音乐》https://music.163.com/album?id=3298101
    // 3Blue1Brown https://music.163.com/album?id=75427050&userid=1547324680
}

void GetSongFromLocalHTML(std::string file_name) {
    time_t StartDownload = clock();
    say(trans("Get Song From Local HTML(Playlist, Artist, Album, etc.)") + ": " + file_name);
    ///////////// Original File
    std::string name = GetTitleFromLocalHTML(file_name);
    std::string content = KT_sys::read_file(file_name).first;
    size_t i = 0, success = 0, cnt = 0;
    while (true) {
        i = content.find("<a href=\"/song?id=", i);
        if (i >= content.size()) break;
        size_t ed = content.find("\">", i += 18u);
        if (ed >= content.size()) break;
        int64 Song_ID = str_to_int(content.substr(i, ed - i));
        if (Song_ID <= 0) continue;
        printf("|~| %2d. ", int(++cnt));
        if (GetSingleSong(Song_ID) == 0) ++success;
    }
    i = 0;
    while (true) {
        i = content.find("<a href=\"https://music.163.com/song?id=", i);
        if (i >= content.size()) break;
        size_t ed = content.find("\">", i += 39u);
        if (ed >= content.size()) break;
        int64 Song_ID = str_to_int(content.substr(i, ed - i));
        if (Song_ID <= 0) continue;
        printf("|~| %2d. ", int(++cnt));
        if (GetSingleSong(Song_ID) == 0) ++success;
    }
    ///////////// Saved Resource
    if (conf.mode == "debug") say("[DEBUG] src: " + file_name.substr(0, file_name.size() - 5) + "_files\\saved_resource.html");
    content = KT_sys::read_file(file_name.substr(0, file_name.size() - 5) + "_files\\saved_resource.html").first;
    i = 0;
    while (true) {
        i = content.find("<a href=\"/song?id=", i);
        if (i >= content.size()) break;
        size_t ed = content.find("\">", i += 18u);
        if (ed >= content.size()) break;
        int64 Song_ID = str_to_int(content.substr(i, ed - i));
        if (Song_ID <= 0) continue;
        printf("|~| %2d. ", int(++cnt));
        if (GetSingleSong(Song_ID) == 0) ++success;
    }
    i = 0;
    while (true) {
        i = content.find("<a href=\"https://music.163.com/song?id=", i);
        if (i >= content.size()) break;
        size_t ed = content.find("\">", i += 39u);
        if (ed >= content.size()) break;
        int64 Song_ID = str_to_int(content.substr(i, ed - i));
        if (Song_ID <= 0) continue;
        printf("|~| %2d. ", int(++cnt));
        if (GetSingleSong(Song_ID) == 0) ++success;
    }
    say("|~|------------------------------------------------------------");
    say("|~| " + trans("Sucessfully downloaded") + " " + int_to_str(success) + " " + trans("Song") + "(" + trans("Total") + ": " + int_to_str(cnt) + ")");
    say("|~| " + trans("Time cost") + ": " + FormatTime(clock() - StartDownload));
}

signed main() {
    say(trans("163-music(cloudmusic) download program") + " " Version);
    say(trans("by brealid") + ", " + trans("open-source") + "[C++03]");
    while (true) {
        say("===============================================================");
        say("|~| 1. " + trans("Get Single Song"));
        say("|~| 2. " + trans("Get Many Single Song"));
        say("|~| 3. " + trans("Get Playlist Song"));
        say("|~| 4. " + trans("Get Artist Song"));
        say("|~| 5. " + trans("Get Album Song"));
        say("|~| 6. " + trans("Get Song From Local HTML(Playlist, Artist, Album, etc.)"));
        say("|~| 100. " + trans("Settings"));
        say("|~| -1. " + trans("Exit"));
        int choice = 0;
        while (choice != 1 && choice != 2 && choice != 3 && choice != 4 && choice != 5 &&  choice != 6 && choice != 100 && choice != -1) {
            say("|~| " + trans("Input number to choose") + ":");
            say("|~| >>> ", NO_ENDL);
            choice = get<int>();
        }
        if (choice == -1) return 0;
        if (choice == 1) {
            say("|~| " + trans("This program will behaves undefined if you enter wrong id") + ":");
            say("|~| " + trans("Input Song-ID") + ":");
            say("|~| >>> ", NO_ENDL);
            int64 id = get<int>();
            say("|~| ", NO_ENDL);
            time_t StartDownload = clock();
            int nRet = GetSingleSong(id);
            say("|~|------------------------------------------------------------");
            say("|~| " + trans("Sucessfully downloaded") + " " + int_to_str(nRet == 0) + " " + trans("Song") + "(" + trans("Total") + ": 1)");
            say("|~| " + trans("Time cost") + ": " + FormatTime(clock() - StartDownload));
        }
        if (choice == 2) {
            say("===============================================================");
            say("|~| 1. " + trans("Input ID"));
            say("|~| 2. " + trans("Get ID from file"));
            say("|~| -1. " + trans("Exit"));
            int floor2_choice = 0;
            while (floor2_choice != 1 && floor2_choice != 2 && floor2_choice != -1) {
                say("|~| " + trans("Input number to choose") + ":");
                say("|~| >>> ", NO_ENDL);
                floor2_choice = get<int>();
            }
            if (floor2_choice == 1) {
                say("|~| " + trans("Input many ids, seperated with **enters**, end with -1") + ":");
                int64 id = 0;
                std::vector<int64> ForDownload;
                while (~id) {
                    id = get<int64>();
                    if (~id) ForDownload.push_back(id);
                }
                GetManySingleSong(ForDownload);
            }
            if (floor2_choice == 2) {
                say("|~| " + trans("Please create a file contain ids, one id per line first."));
                std::string filename = "No_FILE";
                while (KT_sys::read_file(filename).second == -1) {
                    say("|~| " + trans("enter the name of the file(with its whole path)") + ":");
                    say("|~| >>> ", NO_ENDL);
                    getline(std::cin, filename);
                }
                std::string content = KT_sys::read_file(filename).first;
                std::stringstream ss(content);
                int64 id = 0;
                std::vector<int64> ForDownload;
                while (ss >> id) ForDownload.push_back(id);
                GetManySingleSong(ForDownload);
            }
        }
        if (choice == 3) {
            say("|~| " + trans("This program will behaves undefined if you enter wrong id") + ":");
            say("|~| " + trans("Input Playlist-ID") + ":");
            say("|~| >>> ", NO_ENDL);
            int64 id = get<int64>();
            GetPlaylistSong(id);
        }
        if (choice == 4) {
            say("|~| " + trans("This program will behaves undefined if you enter wrong id") + ":");
            say("|~| " + trans("Input Artist-ID") + ":");
            say("|~| >>> ", NO_ENDL);
            int64 id = get<int64>();
            GetArtistSong(id);
        }
        if (choice == 5) {
            say("|~| " + trans("This program will behaves undefined if you enter wrong id") + ":");
            say("|~| " + trans("Input Album-ID") + ":");
            say("|~| >>> ", NO_ENDL);
            int64 id = get<int64>();
            GetAlbumSong(id);
        }
        if (choice == 6) {
            say("|~| " + trans("Please download a file from https://music.163.com of Playlist, Artist, Album, etc.."));
            std::string filename = "No_FILE";
            while (KT_sys::read_file(filename).second == -1) {
                say("|~| " + trans("enter the name of the file(with its whole path)") + ":");
                say("|~| >>> ", '\0');
                getline(std::cin, filename);
            }
            GetSongFromLocalHTML(filename);
        }
        if (choice == 100) conf.config_menu();
    }
    return 0;
}

// Test Cmd: copy download_163music.cpp %tmp%\lib_v3.cpp /Y & c: & run lib_v3.cpp -f & d: