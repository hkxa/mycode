#include <bits/stdc++.h>
using namespace std;

double sum = 0;

int main(int argc, char **argv)
{
    if (argc <= 1) {
        printf("Receive nothing.\n");
        return 1;
    }
    for (int i = 1; i < argc; i++) sum += atof(argv[i]);
    printf("%.6lf\n", sum);
	return 0;
} 
