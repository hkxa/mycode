import os, re, time

try:
    with open('./music/v1.0.log', 'a') as f:
        print("[import lib]", time.time(), file = f)
except FileNotFoundError:
    os.mkdir('music')
    with open('./music/v1.0.log', 'a') as f:
        print("[import lib]", time.time(), file = f)
        
print('Module require: <requests>, <fake_useragent>, <lxml>')
try:
    import requests
except ModuleNotFoundError:
    print("Wait a moment: Trying to install required python-module <requests>")
    os.system('pip install requests -q -q -q')
    import requests
try:
    from fake_useragent import UserAgent
except ModuleNotFoundError:
    print("Wait a moment: Trying to install required python-module <fake_useragent>")
    os.system('pip install fake_useragent -q -q -q')
    from fake_useragent import UserAgent
try:
    from lxml import etree
except ModuleNotFoundError:
    print("Wait a moment: Trying to install required python-module <lxml>")
    os.system('pip install lxml -q -q -q')
    from lxml import etree
print("Module require: OK")

print('============= cloudmusic(163) download(artist) =============')
url = input('请输入歌手地址: ')
base_url = 'https://link.hhtjim.com/163/'

headers = { "User-Agent": UserAgent().chrome }
result = requests.get(url, headers = headers).text

dom = etree.HTML(result)
# print(result, file = open('tmp.txt', "w"))
ids = dom.xpath('//ul[@class="f-hide"]//li/a/@href')
# print(ids)
for i in range(len(ids)):
    ids[i] = re.sub('\D', '', ids[i])

print('ids:', ids)

for i in range(len(ids)):
    M_url = f'https://music.163.com/song?id={ids[i]}'
    response = requests.get(M_url, headers=headers)
    html = etree.HTML(response.text)

    music_info = html.xpath('//title/text()')
    music_name = music_info[0].split('-')[0]
    singer = music_info[0].split('-')[1]

    full_name = music_name + ' - ' + singer

    music_url = base_url + str(ids[i]) + '.mp3'
    music = requests.get(music_url).content
    with open('./music/' + full_name + '.mp3', 'wb') as file:
        file.write(music)
    print("正在下载第 {} 首: ({}) {}".format(str(i + 1), ids[i], full_name))