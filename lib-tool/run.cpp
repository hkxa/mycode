/*
    run.cpp -- Powered by Brealid
    Version History:
    Wed Oct 2  (2019) | Ver 0.0.0 | Alpha version 
    Sat Oct 19 (2019) | Ver 0.0.1 | Add: new compile argument
    Tue Jan 21 (2020) | Ver 0.1.0 | Add: random file name
    Wed Jan 22 (2020) | Ver 0.1.1 | Change: measure time precision
    Sun Feb 9  (2020) | Ver 0.1.3 | Change: the order of Compilation and InputTime
    Thu Feb 20 (2020) | Ver 0.1.4 | Change: turn all printf to fprintf(stderr)
    Thu Feb 20 (2020) | Ver 0.2.0 | Add: Argument system (-f, -q)
    Fri Mar 27 (2020) | Ver 0.2.1 | Add: Output compile log to file
    Sat Apr 4  (2020) | Ver 0.2.2 | Fix: OutputToFile FILE* (ToPlace) error
    Fri May 29 (2020) | Ver 0.2.3 | Add: Augument (-l, -h); Change: For user to choose compile.log
    Sat Jun 13 (2020) | Ver 0.2.4 | Add: fflush(stderr) after output; Change: output format
    Fri Jul 24 (2020) | Ver 0.2.5 | Modify: Hint Text; Fix: Warning Compile, HelpInfo(Small Modify)
    Mon Jul 27 (2020) | Ver 0.2.6 | Modify: g++ argument with "" included
    Tue Aug 18 (2020) | Ver 0.2.7 | Add: auto -std=c++11 (As NOI2020 enables c++11)
    Wed Sep 2  (2020) | Ver 0.2.8 | Fix: del exe time (move from before-mearsure to after-mearsure)
    Fri Sep 4  (2020) | Ver 0.2.9 | Fix: del exe time (Sleep 100ms)
    Sun Sep 6  (2020) | Ver 0.3.0 | Modify: Compile Text; Add: monitor choice (86400s, 32768MB)
    Mon Sep 7  (2020) | Ver 0.3.1 | Modify: monitor choice TimeLimit & MemLimit (10s, 16384MB)
    Sun Sep 13 (2020) | Ver 0.3.2 | Modify: Hint Text
    Sun Sep 20 (2020) | Ver 0.3.3 | Modify: Monitor Help; Hint Text
    Mon Oct 5  (2020) | Ver 0.3.4 | Modify: Hint Text
    Thu Nov 15 (2020) | Ver 0.3.5 | Modify: remove OutputCompileInformation
    Thu Nov 29 (2020) | Ver 0.3.5 | Modify: argv[1] (.cpp suffix added); Add: cmd title modify
    Sun Feb 14 (2021) | Ver 0.3.6 | Modify: auto -std=c++11 (recognize "-std=c++xx" in argv)
    Sun Mar 28 (2021) | Ver 0.3.7 | Repair: auto -std=c++11; Add: Auto Define(BREALID_RUN) 
    Tue Jul 28 (2021) | Ver 0.3.8 | Repair: add '.\' before EXE_NAME
*/

#include <bits/stdc++.h>
#include <windows.h>
using namespace std;
#define suffix(s) ((s) + strlen(s))
bool NoSleep = 0, NoSaying = 0, SaveLog = 0, MonitorOpen = 0, StdSettled = 0;

// void OutputCompileInformation(FILE* _FP, FILE* _ToPlace = stderr)
// {
//     bool read = 0;
//     char ch;
//     while (fscanf(_FP, "%c", &ch) != EOF) {
//         if (!read) fprintf(stderr, "g++ Compile information is as follows.\n");
//         fprintf(_ToPlace, "%c", ch);
//         read = true;
//     }
// }

// void OutputCompileInformation(const char *FILE_name, FILE* _ToPlace = stderr)
// {
//     FILE *_FP = fopen(FILE_name, "r");
//     OutputCompileInformation(_FP, _ToPlace);
//     fclose(_FP);
// }

void HelpInfo(int cmd) {
    printf("run.cpp (Powered by brealid, For shell-use OIer)\n"
           "gitee Project: https://gitee.com/hkxa/mycode/blob/master/lib-tool/run.cpp\n"
           "Welcome star & fork\n"
           "\n"
           "Para  | Another Name    | Explanation\n"
           "-f,-F | --fast          | ignore Data input time(3.2s)\n"
           "-q,-Q | --quiet         | do not output prompting message\n"
           "-m,-M | --memory        | mearsure memory(need freopen)\n"
           "-l    | --log,--savelog | save compile information to run.log_file\n"
           "-?,-h | --help          | output help information\n"
           "(other parameter(s) would be sent to g++)\n"
           "\n"
           "If you have any other question, please contact brealid in luogu.\n");
    if (cmd) {
        printf("(ignore other parameter)\n");
    }
    exit(0);
}
 
int main(int argc, char **argv) {
    if (argc == 1) {
        fprintf(stderr, "No input.\n");
        return EXIT_FAILURE;
    } else if (argc == 2 && (!strcmp(argv[1], "-?") || !strcmp(argv[1], "-h") || !strcmp(argv[1], "--help"))) {
        HelpInfo(0);
        return 0;
    }
    system((string("title run \"") + argv[1] + "\"").c_str());
    fclose(fopen("run.log_file", "w"));
    // Define 
    char compileCmd[1007], runPath[1007], delPath[1007], EXEname[9];
    // Generate EXEname, runPath, delPath
    srand(time(0));
    srand(time(0) ^ rand() ^ clock());
    for (int i = 0; i < 8; i++) EXEname[i] = rand() % 26 + 'A';
    EXEname[8] = 0;
    sprintf(runPath, ".\\%s.exe", EXEname);
    sprintf(delPath, "DEL .\\%s.exe", EXEname);
    // Generate compileCmd
    sprintf(compileCmd, "g++ \"%s\" -o .\\%s.exe -Wall -Wextra -Wpointer-arith -Wcast-qual -Wl,--stack=536870912 -DBREALID_RUN", argv[1], EXEname);
    // Get Arguments
    for (int i = 2; i < argc; i++) {
        if (!strcmp(argv[i], "-f") || !strcmp(argv[i], "-F") || !strcmp(argv[i], "--fast")) NoSleep = true;
        else if (!strcmp(argv[i], "-q") || !strcmp(argv[i], "-Q") || !strcmp(argv[i], "--quiet")) NoSaying = true;
        else if (!strcmp(argv[i], "-l") || !strcmp(argv[i], "--log") || !strcmp(argv[i], "--savelog")) SaveLog = true;
        else if (!strcmp(argv[i], "-m") || !strcmp(argv[i], "-M") || !strcmp(argv[i], "--memory")) MonitorOpen = true;
        else if (!strcmp(argv[i], "-?") || !strcmp(argv[i], "-h") || !strcmp(argv[i], "--help")) HelpInfo(1);
        else {
            sprintf(suffix(compileCmd), " \"%s\"", argv[i]);
            string arg = argv[i];
            if (arg.size() >= 5 && arg.substr(0, 5) == "-std=") StdSettled = true;
        }
    }
    if (!StdSettled) sprintf(suffix(compileCmd), " -std=c++11");
    if (MonitorOpen) {
        if (!system("where monitor >run.log_file")) 
            sprintf(runPath, "monitor %s.exe 10 16384", EXEname);
        else 
            fprintf(stderr, "No monitor for your operating system.\n");
    }
    // Compile File
    if (!NoSaying) fprintf(stderr, "[compiling...]\n"); 
    if (system(compileCmd)) {
        fprintf(stderr, "Compiled failed.\n");
        // OutputCompileInformation("run.log_file");
        if (!SaveLog) system("del run.log_file");
        return EXIT_FAILURE;
    }
    if (!NoSaying) fprintf(stderr, "Compiled ok.\n");
    // OutputCompileInformation("run.log_file");
    // Input Data
    if (!NoSaying && !NoSleep) fprintf(stderr, "[input time...]\n");
    if (!NoSleep) Sleep(3207);
    // Run grogram & measure time
    if (!NoSaying) fprintf(stderr, "The program is running.\n");
    if (!NoSaying) fprintf(stderr, "====================== %s.exe ======================\n", EXEname);
    fflush(stderr);
    clock_t bef = clock();
    int ret = system(runPath);
    clock_t aft = clock();
    if (!NoSaying) fprintf(stderr, "==========================================================\n");
    if (!NoSaying) fprintf(stderr, "exit code %d in %.2lf sec(s).\n", ret, (aft - bef) / 1000.0);
    Sleep(100);
    system(delPath);
    if (!SaveLog) system("del run.log_file");
    fflush(stderr);
    return EXIT_SUCCESS;
}