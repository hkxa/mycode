#include <windows.h>
#include <conio.h>
#include <bits/stdc++.h>
using namespace std;
#define FOLDER "\\\\hgoi05\\ktt_server"
#define CLIENT_EXE_NAME "ktt_client.exe"
#define UPDATE_EXE_NAME "ktt_update.exe"
#define MSG_CNT_FILE "message_cnt.ktt"
#define VERSION_FILE "version.ktt"
#define MSG_FILE_EXT_NAME ".ktt-message"
#define Version 10008

int cnt;
string nickname;

void try_to_update() {
    FILE *f = fopen(FOLDER "\\" VERSION_FILE, "r");
    int ret;
    fscanf(f, "%d", &ret);
    fclose(f);
    if (ret > Version) {
        printf("-------------------------------------\n");
        printf("A new version available : %d.%d.%d\n", ret / 10000, ret / 100 % 100, ret % 100);
        printf("Do you want to update now? [Y/N] ");
        string answer;
        cin >> answer;
        if (answer == "Y") {
            printf("updating...\n");
            char new_name[1007], cmd_line[1007];
            sprintf(new_name, "D:\\ktt_client\\ktt_client_%d_%d_%d.exe", ret / 10000, ret / 100 % 100, ret % 100);
            if (system("cd D:\\ktt_client")) system("mkdir D:\\ktt_client");
            sprintf(cmd_line, "%s %s", "copy " FOLDER "\\" CLIENT_EXE_NAME, new_name);
            system(cmd_line);
            printf("New version has been downloaded in %s.\nPlease start the program again.\n", new_name);
            system("explorer D:\\ktt_client");
            exit(0);
        } else {
            printf("You chose not to update.\n");
            printf("-------------------------------------\n");
        }
    }
}

int getcnt() {
    FILE *f = fopen(FOLDER "\\" MSG_CNT_FILE, "r");
    if (!f) return -1;
    int ret;
    fscanf(f, "%d", &ret);
    fclose(f);
    return ret;
}

string get_massage(int cnt) {
    char road_to_message[107];
    sprintf(road_to_message, "%s\\%d%s", FOLDER, cnt, MSG_FILE_EXT_NAME);
    FILE *f = fopen(road_to_message, "r");
    while (!f) f = fopen(road_to_message, "r");
    char ch;
    string ret;
    while (fscanf(f, "%c", &ch) != EOF) ret += ch;
    fclose(f);
    return ret;
}

void refreshMessage() {
    // printf("[DEBUG] try to refreshMessage\n");
    if (cnt == getcnt()) return;
    while (++cnt <= getcnt())
        cout << "\r                                                     \r" << get_massage(cnt) << '\n' << nickname << ": ";;
}

void modify_cnt(int new_cnt) {
    FILE *f = fopen(FOLDER "\\" MSG_CNT_FILE, "w");
    fprintf(f, "%d", new_cnt);
    fclose(f);
}

void enterMessage(string message) {
    // printf("[DEBUG] try to enterMessage\n");
    refreshMessage();
    modify_cnt(cnt = getcnt() + 1);
    char road_to_message[107];
    sprintf(road_to_message, "%s\\%d%s", FOLDER, cnt, MSG_FILE_EXT_NAME);
    FILE *f = fopen(road_to_message, "w");
    fprintf(f, "%s", message.c_str());
    fclose(f);
}

int main()
{
    printf("version : %d.%d.%d\n", Version / 10000, Version / 100 % 100, Version % 100);
    printf("Before use, make sure you can connect \"" FOLDER "\"\n");
    printf("Try to connect the server...\n");
    if (!~getcnt()) {
        printf("connect failed!\n");
        return 1;
    }
    printf("connect succeeded!\n");
    try_to_update();
    printf("input your Nickname...\n");
    cin >> nickname;
    while (kbhit());
    int pull_countdown = 1;
    cnt = getcnt();
    enterMessage(nickname + " joined");
    string my_message;
    char ch;
    cout << nickname << ": ";
    // printf("[DEBUG] cnt = %d\n", cnt);
    while (true) {
        if (!--pull_countdown) {
            refreshMessage();
            pull_countdown = 3000;
        }
        if (kbhit()) {
            ch = getchar();
            if (ch == '\r' || ch == '\n') {
                continue;
                while (kbhit());
                cout << nickname << ": ";
                continue;
            }
            while (ch != '\r' && ch != '\n') {
                my_message += ch;
                ch = getchar();
            }
            while (kbhit());
            enterMessage(nickname + ": " + my_message);
            cout << nickname << ": ";
            my_message = "";
        }
    }
}