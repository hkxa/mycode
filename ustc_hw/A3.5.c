#include <stdio.h>

int isprime(int n) {
    for (int i = 2; i * i <= n; ++i)
        if (n % i == 0) return 0;
    return 1;
}

int find(int n) {
    if (isprime(n - 2)) return 2;
    for (int i = 3; ; i += 2)
        if (isprime(i) && isprime(n - i)) return i;
}

int main() {
    int num, t;
    while (scanf("%d", &num) == 1) {
        t = find(num);
        printf("%d = %d + %d\n", num, t, num - t);
    }
    return 0;
}