#ifndef SOURCE_p_status_hpp
#define SOURCE_p_status_hpp
#include "header.h"
#include "button.hpp"

namespace P_status {
    int xuefen, energy = 100, hungry = 100, guake, money = 1500, health = 30;
    bool warn;
    void show() {
        KT_mp::set_pos(1, 1);
        // ●◆√×㊣＊
        KT_console::print("㊣", "green");
        printf("学分 %-6d", xuefen);
        KT_mp::set_pos(1, 14);
        KT_console::print("◆", "blue");
        printf("体力 %-6d", energy);
        KT_mp::set_pos(1, 27);
        KT_console::print("●", "purple");
        printf("饥饿 %-6d", hungry);
        KT_mp::set_pos(2, 1);
        KT_console::print("×", "red");
        printf("挂科 %-6d", guake);
        KT_mp::set_pos(2, 14);
        KT_console::print("＊", "yellow");
        printf("金钱 %-6d", money);
        KT_mp::set_pos(2, 27);
        KT_console::print("ш", "light green");
        printf("健康 %-6d", health);
        // KT_mp::set_pos(3, 1);

        if (health < 0) {
            ktnt_clock.wait(1.5);
            clear_textArea();
            immediate_hide();
            ktnt_clock.wait(1.15);
            KT_mp::set_pos(6, 1);
            KT_console::say("你的身体应该好好调养了呢……");
            ktnt_clock.wait(0.7);
            KT_mp::set_pos(7, 1);
            KT_console::say("你以体弱为由成功退学！");
            ktnt_clock.wait(0.8);
            exit_game.show();
            while (toupper(getch()) != 'E');
            exit(0);
        }
        
        if (guake >= 20) {
            ktnt_clock.wait(1.5);
            clear_textArea();
            immediate_hide();
            ktnt_clock.wait(1.15);
            KT_mp::set_pos(6, 1);
            KT_console::say("在科大，不认真学习是会被退学的……");
            ktnt_clock.wait(0.7);
            KT_mp::set_pos(7, 1);
            KT_console::say("你的挂科学分到达了 20，你被退学了");
            ktnt_clock.wait(0.8);
            exit_game.show();
            while (toupper(getch()) != 'E');
            exit(0);
        }

        if (xuefen >= 160) {
            ktnt_clock.wait(0.5);
            clear_textArea();
            immediate_hide();
            ktnt_clock.wait(1.15);
            KT_mp::set_pos(6, 1);
            KT_console::say("你的学分达到了 160，被准许毕业");
            ktnt_clock.wait(0.7);
            KT_mp::set_pos(7, 1);
            KT_console::say("当你多年后回想起蜗壳的这四年时光，希望你能够没有任何遗憾");
            ktnt_clock.wait(0.7);
            KT_mp::set_pos(9, 1);
            KT_console::say("恭喜你，亲爱的蜗壳学子，完美通关！");
            ktnt_clock.wait(0.8);
            exit_game.show();
            while (toupper(getch()) != 'E');
            exit(0);
        }

        if (guake < 10) warn = false;
        else if (!warn) {
            warn = true;
            ktnt_clock.wait(0.35);
            clear_textArea();
            ktnt_clock.wait(1.5);
            KT_mp::set_pos(6, 1);
            KT_console::say("警示：你的挂科学分已经到达 10！");
            ktnt_clock.wait(0.5);
            KT_mp::set_pos(7, 1);
            KT_console::say("当挂科学分到达 20 时，你将被退学！");
        }
    }
    int beefg = 2, milk, mooncake, coffee, pdf;
}

#endif