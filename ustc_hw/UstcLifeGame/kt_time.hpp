#ifndef SOURCE_kt_time_hpp
#define SOURCE_kt_time_hpp
#include "header.h"

class ktnt_time {
private:
public:
    ktnt_time() {}
    ~ktnt_time() {}
    double clock() {
        return (double)(::clock()) / CLOCKS_PER_SEC;
    }
    void wait(double t) {
        Sleep((int)(t * 1000));
    }
} ktnt_clock;

#endif // #ifndef HEADER_FILE_ktnt_time_hpp