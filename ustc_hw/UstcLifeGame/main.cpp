#include "header.h"
#include "button.hpp"
#include "p_status.hpp"
#include "event.hpp"

char wait_for_event() {
    string ok = "";
    if (start_game.is_show) ok += start_game.ch;
    if (exit_game.is_show) ok += exit_game.ch;
    if (explore.is_show) ok += explore.ch;
    if (sleep.is_show) ok += sleep.ch;
    if (shop.is_show) ok += shop.ch;
    if (bag.is_show) ok += bag.ch;
    if (partner.is_show) ok += partner.ch;
    return wait_for_input(ok);
}

void show_explore() {
    if (P_status::energy < 10) {
        clear_textArea();
        KT_mp::set_pos(6, 1);
        KT_console::say("你现在很累，精力不够了，不能探索");
        return;
    }
    if (P_status::hungry < 5) {
        clear_textArea();
        KT_mp::set_pos(6, 1);
        KT_console::say("你现在很饿，饥饿值不够了，不能探索");
        return;
    }
    P_status::energy -= 10;
    P_status::hungry -= 5;
    get_event()();
}

void show_bag() {
    clear_textArea();
    immediate_hide();
    exit_game.show();
    P_status::beefg = P_status::milk = P_status::mooncake = P_status::coffee = P_status::pdf = 1000;
    while (true) {
        KT_mp::set_pos(4, 2);
        printf("牛肉干(B): %d", P_status::beefg);
        KT_mp::set_pos(5, 2);
        printf("牛奶(M): %d", P_status::milk);
        KT_mp::set_pos(6, 2);
        printf("科大月饼(C): %d", P_status::mooncake);
        KT_mp::set_pos(7, 2);
        printf("浓咖啡(K): %d", P_status::coffee);
        KT_mp::set_pos(8, 2);
        printf("学习资料(P): %d", P_status::pdf);
        char ch = wait_for_input("BMCKPE");
        clear_textArea();
        if (ch == 'E') {
            clear_textArea();
            immediate_show();
            exit_game.hide();
            P_status::show();
            return;
        }
        KT_mp::set_pos(10, 3);
        switch (ch) {
            case 'B':
                if (!P_status::beefg) printf("你没有牛肉干！");
                else {
                    printf("你吃下了一块牛肉干，饥饿 + 50!");
                    P_status::hungry += 50;
                    --P_status::beefg;
                }
                break;
            case 'M':
                if (!P_status::milk) printf("你没有牛奶！");
                else {
                    P_status::hungry += 20;
                    --P_status::milk;
                    if (KT_random::randomEvent(0.15)) {
                        printf("你喝下了一瓶牛奶，饥饿 + 20，健康值 + 1!");
                        ++P_status::health;
                    } else {
                        printf("你喝下了一瓶牛奶，饥饿 + 20!");
                    }
                }
                break;
            case 'C':
                if (!P_status::mooncake) printf("你没有科大月饼！");
                else {
                    P_status::hungry += 100;
                    P_status::energy += 15;
                    --P_status::mooncake;
                    printf("你吃下了一份科大月饼，饥饿 + 100，体力 + 15!");
                    if (KT_random::randomEvent(0.05)) {
                        KT_mp::set_pos(12, 3);
                        printf("你在月饼中发现了一份学习资料，好耶！");
                        ++P_status::pdf;
                        KT_mp::set_pos(8, 2);
                        printf("学习资料(P): %d", P_status::pdf);
                    }
                }
                break;
            case 'K':
                if (!P_status::coffee) printf("你没有咖啡！");
                else {
                    P_status::energy += 50;
                    --P_status::coffee;
                    printf("你喝下了一杯咖啡，体力 + 50!");
                    if (KT_random::randomEvent(0.1)) {
                        KT_mp::set_pos(12, 3);
                        printf("咖啡喝多了对身体不好，你感觉头昏脑涨，健康值 - 1!");
                        --P_status::health;
                    }
                }
                break;
            case 'P':
                if (!P_status::pdf) printf("你没有学习资料！");
                else {
                    P_status::energy -= 20;
                    P_status::hungry -= 10;
                    --P_status::pdf;
                    if (KT_random::randomEvent(0.614)) {
                        printf("你学习了一段时间，体力 - 20，饥饿 - 10，感觉毫无进步!");
                    } else if (KT_random::randomEvent(0.862)) {
                        printf("你学习了一段时间，体力 - 20，饥饿 - 10，感觉有点进步，学分 + 1!");
                        ++P_status::xuefen;
                    } else {
                        printf("你学习了一段时间，体力 - 20，饥饿 - 10，感觉学到了很多东西，学分 + 2!");
                        P_status::xuefen += 2;
                    }
                }
                break;
        }
        P_status::show();
    }
}

void welcome() {
    KT_mp::set_pos(6, 16);
    KT_console::say("蜗壳人生");

    start_game.show();
    exit_game.show();
    if (wait_for_event() == 'E') exit(0);
    start_game.bling();
    ktnt_clock.wait(0.12);
    
    KT_mp::set_pos(6, 16);
    KT_console::print("        ");
    start_game.hide();
    exit_game.hide();

    P_status::show();
    explore.show();

    wait_for_event();
    explore.bling();
    clear_textArea();
    KT_mp::set_pos(6, 1);
    KT_console::say("欢迎你，亲爱的科大学子！让我们一起踏上这段蜗壳生活！");

    wait_for_event();
    explore.bling();
    clear_textArea();
    KT_mp::set_pos(6, 1);
    KT_console::say("要想毕业，你必须拿够 160 个学分。同时，千万不要挂科！");

    wait_for_event();
    explore.bling();
    clear_textArea();
    KT_mp::set_pos(6, 1);
    KT_console::say("更多的内容，留待你自己探索(*^▽^*)");

    bag.show();
}

int main() {
    KT_mp::HideCursor();
    system("mode con cols=42 lines=25");
    system("title 蜗壳人生 v0.0");

    welcome();

    while (true) {
        switch (wait_for_event()) {
            case 'F': show_explore(); break;
            case 'B': show_bag(); break;
        }
        P_status::show();
    }

    // printf("bag.x,y = %d,%d", bag.x, bag.y);
    KT_mp::set_pos(3, 6);
    system("pause");
    return 0;
}