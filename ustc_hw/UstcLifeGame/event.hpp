#ifndef SOURCE_event_hpp
#define SOURCE_event_hpp

#include "p_status.hpp"

void event1() {
    clear_textArea();
    KT_mp::set_pos(6, 1);
    KT_console::say("你捡到了 10 元钱");
    P_status::money += 10;
}

void event2() {
    clear_textArea();
    KT_mp::set_pos(6, 1);
    KT_console::say("你认真学习，获得了 1 学分");
    P_status::xuefen += 1;
}

void event3() {
    clear_textArea();
    KT_mp::set_pos(6, 1);
    KT_console::say("你逃课，一门课挂科了");
    P_status::guake += 1;
}

typedef void EventFunc();

vector<pair<int, EventFunc*> > event_list = { {100, event1}, 
                                              {20, event2},
                                              {7, event3} };

EventFunc* get_event() {
    int total = 0;
    for (size_t i = 0; i < event_list.size(); ++i) total += event_list[i].first;
    int tenv = KT_random::rnd() % total;
    for (size_t i = 0; i < event_list.size(); ++i)
        if (tenv < event_list[i].first) return event_list[i].second;
        else tenv -= event_list[i].first;
    return NULL;
}

#endif