#ifndef SOURCE_header_h
#define SOURCE_header_h
#include <bits/stdc++.h>
#include <windows.h>
#include <conio.h>
using namespace std;

#define FOLDER "C:\\ProgramData\\KTOJ\\"
#define SUBMISSION_FOLDER "Submission\\"
#define PROBLEM_FOLDER "Problem\\"

#define KT_LogFile (".\\info.log.KTOJ")
#ifdef KT_DEBUG
#define KT_Log(info) KT_sys::write_file(KT_LogFile, KT_sys::read_file(KT_LogFile).first + info)
#else
#define KT_Log(info) void()
#endif

const string CharSet_lower = "abcdefghijklmnopqrstuvwxyz";
const string CharSet_upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
const string CharSet_digit = "0123456789";
const string CharSet_alpha = CharSet_lower + CharSet_upper;
const string CharSet_alnum = CharSet_alpha + CharSet_digit;

string operator * (const string &ss, int st) {
    string ret;
    while (st--) ret += ss;
    return ret;
}
namespace KT_random {
    mt19937 rnd(chrono::steady_clock::now().time_since_epoch().count());
    string gen_str(int length, string seq = CharSet_alnum) {
        string ret;
        while (length--) ret.push_back(seq[rnd() % seq.size()]);
        return ret;
    }
    bool randomEvent(double p) {
        return rnd() % 10000 < 10000 * p;
    }
    double random() {
        return rnd() % 1000000 / 1000000.0;
    }
}

int str_to_int(string value) {
    stringstream ss(value);
    int ret = -1;
    ss >> ret;
    return ret;
}

bool if_same(string s1, string s2) {
    transform(s1.begin(), s1.end(), s1.begin(), ::tolower);
    transform(s2.begin(), s2.end(), s2.begin(), ::tolower);
    return s1 == s2;
}

namespace KT_sys {
    string closest_info;
    pair<string, int> read_file(string file_name) {
        // fprintf(stderr, "read_file(%s)\n", file_name.c_str());
        string sRet;
        char ch;
        FILE *fin = fopen(file_name.c_str(), "r");
        if (fin == NULL) return make_pair(string(), -1);
        while (fscanf(fin, "%c", &ch) == 1) sRet += ch;
        fclose(fin);
        return make_pair(sRet, (int)sRet.size());
    }
    int write_file(string file_name, string content) {
        // fprintf(stderr, "write_file(%s, %s)\n", file_name.c_str(), content.c_str());
        FILE *fout = fopen(file_name.c_str(), "w");
        if (fout == NULL) return -1;
        fprintf(fout, "%s", content.c_str());
        fclose(fout);
        return content.size();
    }
    int call(string command) {
        // fprintf(stderr, "call(%s)\n", command.c_str());
        string output_file = KT_random::gen_str(10) + ".cmd_output";
        string call_command = command + " 1>" + output_file + " 2>&1";
        KT_Log("KT_sys::call: " + call_command + "\n");
        int nRet = system(call_command.c_str());
        call_command = "del " + output_file;
        closest_info = read_file(output_file).first;
        KT_Log("KT_sys::call::output " + closest_info + "\n");
        system(call_command.c_str());
        return nRet;
    }
}

namespace KT_dbfile {
    string submission_total() {
        return string() + FOLDER + SUBMISSION_FOLDER + "submission.kt-db";
    }
    string prob_list() {
        return string() + FOLDER + PROBLEM_FOLDER + "problem.kt-db";
    }
    string prob_folder(int prob_id) {
        return string() + FOLDER + PROBLEM_FOLDER + to_string(prob_id) + "\\";
    }
    string prob_config(int prob_id) {
        return prob_folder(prob_id) + "config.kt-db";
    }
    string prob_statement(int prob_id, string ff) {
        return prob_folder(prob_id) + "statement\\" + ff;
    }
    string prob_data(int prob_id, string data_file) {
        return prob_folder(prob_id) + "data\\" + data_file;
    }
    string prob_std(int prob_id, string std_file) {
        return prob_folder(prob_id) + "std\\" + std_file;
    }
    string sub_id(int id) {
        return string() + FOLDER + SUBMISSION_FOLDER + "sub" + to_string(id) + ".kt-db";
    }
    string sub_src(int id, string lang) {
        return string() + FOLDER + SUBMISSION_FOLDER + "sub" + to_string(id) + "." + lang;
    }
    string temp_dir() {
        KT_sys::call("echo %temp%");
        while (!isalnum(KT_sys::closest_info.back())) KT_sys::closest_info.pop_back();
        return KT_sys::closest_info;
    }
}

char wait_for_input(string ok_input) {
    while (true) {
        char g = toupper(getch());
        if (ok_input.find(g) != string::npos) return g;
    }
}

#endif // SOURCE_header_h