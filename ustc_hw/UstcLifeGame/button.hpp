#ifndef SOURCE_button_hpp
#define SOURCE_button_hpp
#include "manage_pos.hpp"
#include "console_color.hpp"
#include "kt_time.hpp"

namespace KT_button {
    class button {
      public:
        string name;
        char ch;
        bool is_show;
        int x, y;
      public:
        button(string n, char c, bool i, int _x, int _y) :
            name(n), ch(c), is_show(i), x(_x), y(_y) {
            if (is_show) show();
        }
        void show() {
            if (is_show) return;
            is_show = true;
            // KT_console::set("black", "white");
            KT_mp::set_pos(x, y);
            printf("+-------+");
            KT_mp::set_pos(x + 1, y);
            printf("|%s(%c)|", name.c_str(), ch);
            KT_mp::set_pos(x + 2, y);
            printf("+-------+");
        }
        void hide() {
            if (!is_show) return;
            is_show = false;
            
            // KT_console::set("black", "white");
            KT_mp::set_pos(x, y);
            printf("         ");
            KT_mp::set_pos(x + 1, y);
            printf("         ");
            KT_mp::set_pos(x + 2, y);
            printf("         ");
        }
        void bling() {
            KT_console::set("black", "light white");
            KT_mp::set_pos(x, y);
            printf("+-------+");
            KT_mp::set_pos(x + 1, y);
            printf("|%s(%c)|", name.c_str(), ch);
            KT_mp::set_pos(x + 2, y);
            printf("+-------+");
            ktnt_clock.wait(0.12);

            KT_console::set("black", "white");
            KT_mp::set_pos(x, y);
            printf("+-------+");
            KT_mp::set_pos(x + 1, y);
            printf("|%s(%c)|", name.c_str(), ch);
            KT_mp::set_pos(x + 2, y);
            printf("+-------+");
        }
    };
}

KT_button::button start_game("��ʼ", 'S', false, 15, 16);
KT_button::button exit_game("�˳�", 'E', false, 21, 27);

KT_button::button explore("̽��", 'F', false, 17, 5);
KT_button::button sleep("˯��", 'H', false, 17, 27);
KT_button::button shop("�̵�", 'V', false, 21, 5);
KT_button::button bag("����", 'B', false, 21, 16);
KT_button::button partner("���", 'N', false, 21, 27);

int show_status;

void immediate_hide() {
    if (start_game.is_show) start_game.hide(), show_status |= 64;
    if (exit_game.is_show) exit_game.hide(), show_status |= 32;
    if (explore.is_show) explore.hide(), show_status |= 16;
    if (sleep.is_show) sleep.hide(), show_status |= 8;
    if (shop.is_show) shop.hide(), show_status |= 4;
    if (bag.is_show) bag.hide(), show_status |= 2;
    if (partner.is_show) partner.hide(), show_status |= 1;
}

void immediate_show() {
    if (show_status & 64) start_game.show();
    if (show_status & 32) exit_game.show();
    if (show_status & 16) explore.show();
    if (show_status & 8) sleep.show();
    if (show_status & 4) shop.show();
    if (show_status & 2) bag.show();
    if (show_status & 1) partner.show();
    show_status = 0;
}

#endif