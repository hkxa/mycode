#ifndef SOURCE_manage_pos_hpp
#define SOURCE_manage_pos_hpp
#include "header.h"

namespace KT_mp {
    void HideCursor() {
        CONSOLE_CURSOR_INFO cursor;    
        cursor.bVisible = FALSE;    
        cursor.dwSize = sizeof(cursor);    
        SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE), &cursor);
    }
    void set_pos(int x, int y){
        COORD pos;
        pos.X = y;
        pos.Y = x;
        SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), pos);
    }
}

void clear_textArea() {
    for (int i = 3; i <= 16; ++i) {
        KT_mp::set_pos(i, 1);
        for (int j = 1; j <= 42; ++j) printf(" ");
    }
}

#endif