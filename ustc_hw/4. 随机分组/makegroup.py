from collections import namedtuple
from copy import deepcopy
import random
from tkinter.filedialog import askopenfilename
from tkinter import Tk
import traceback
from typing import Iterable
import chardet
from math import exp
import pandas as pd

unsatisfy = {
    1 : 10,
    2 : 20,
    3 : 15,
    4 : 15,
    5 : 30,
    6 : 40,
    7 : 5,
    8 : 15
}

class requirement:
    def __init__(self, s) -> None:
        s = s.split(' ')
        self.type = int(s[0])
        if self.type == 1:
            self.info = int(s[1])
        elif self.type == 2:
            self.info = namedtuple('range1', ['l', 'r'])(int(s[1]), int(s[2]))
        elif 3 <= self.type <= 4:
            self.info = [int(s[1]), int(s[2])] + s[3:]
        elif 5 <= self.type <= 6:
            self.info = s[1]
        else:
            self.info = None

def split_requirement(x):
    # print(type(x), x)
    if type(x) is str:
        return [requirement(t) for t in x.split(';')]
    else:
        return []

class group:
    def __init__(self, leader, member: list) -> None:
        self.leader = leader
        self.member = deepcopy(member)

class student:
    def __init__(self, name: str, sex: str, college: str, grade: str, quest: Iterable[requirement]) -> None:
        self.name = name
        self.sex = sex
        self.college = college
        self.grade = grade
        self.quest = deepcopy(quest)
        for x in self.quest:
            if 3 <= x.type <= 4 and len(x.info) == 2:
                if x.type == 3:
                    x.info.append(self.college)
                else:
                    x.info.append(self.grade)

    def judge(self, status: group) -> int:
        unhappy = 0
        for x in self.quest:
            if x.type == 1:
                if len(status.member) != x.info:
                    unhappy += unsatisfy[x.type]
            elif x.type == 2:
                diff_sex = 0
                for s in status.member:
                    if s.sex != self.sex:
                        diff_sex += 1
                if diff_sex < x.info.l or diff_sex > x.info.r:
                    unhappy += unsatisfy[x.type]
            elif x.type == 3:
                ok = 0
                for s in status.member:
                    if str(x.info[2]) == str(s.college):
                        ok += 1
                if ok < x.info[0] or ok > x.info[1]:
                    unhappy += unsatisfy[x.type]
            elif x.type == 4:
                ok = 0
                for s in status.member:
                    if str(x.info[2]) == str(s.grade):
                        ok += 1
                if ok < x.info[0] or ok > x.info[1]:
                    unhappy += unsatisfy[x.type]
            elif x.type == 5:
                ok = 0
                for s in status.member:
                    if x.info == s.name:
                        ok = 1
                if not ok:
                    unhappy += unsatisfy[x.type]
            elif x.type == 6:
                ok = 1
                for s in status.member:
                    if x.info == s.name:
                        ok = 0
                if not ok:
                    unhappy += unsatisfy[x.type]
            elif x.type == 7:
                if status.leader.name != self.name:
                    unhappy += unsatisfy[x.type]
            elif x.type == 8:
                if status.leader.name == self.name:
                    unhappy += unsatisfy[x.type]
        return unhappy



class solution:
    def __init__(self) -> None:
        self.stu = []

    def input(self, excelpath: str):
        extension = excelpath.split('.')[-1]
        if extension == 'csv':
            with open(excelpath, 'rb') as f:
                encode = chardet.detect(f.read())["encoding"] # 判断编码格式
            data = pd.read_csv(excelpath, header=0, names=["name", "sex", "college", "grade", "quest"], encoding=encode) # 读取csv文件
        else:
            data = pd.read_excel(excelpath, header=0, names=["name", "sex", "college", "grade", "quest"]) # 读取xls/xlsx文件
        for i in range(len(data["name"])):
            self.stu.append(student(name=data["name"][i],
                                    sex=data["sex"][i],
                                    college=data["college"][i],
                                    grade=data["grade"][i],
                                    quest=split_requirement(data["quest"][i])))
    
    def test(self, stu, num):
        result = 0
        for s in range(0, len(stu), num):
            g = group(stu[s], stu[s:s+num])
            result += sum([stu[s+p].judge(g) for p in range(num)])
        return result

    def schedule(self, stu, num):
        for i in range(10000):
            last = self.test(stu, num)
            if not last:
                break
            p1, p2 = random.randint(0, len(stu) - 1), random.randint(0, len(stu) - 1)
            stu[p1], stu[p2] = stu[p2], stu[p1]
            if self.test(stu, num) > last:
                stu[p1], stu[p2] = stu[p2], stu[p1]
        return [group(stu[s], stu[s:s+num]) for s in range(0, len(stu), num)], self.test(stu, num)

    def solve(self):
        n = len(self.stu)
        gr5 = []
        gr6 = []
        remains = []
        for s in self.stu:
            for x in s.quest:
                if x.type == 1:
                    if x.info == 5:
                        gr5.append(s)
                        break
                    elif x.info == 6:
                        gr6.append(s)
                        break
            else:
                remains.append(s)
        cnt5 = len(gr5)
        cnt6 = len(gr6)
        while len(gr5) % 5 != 0 or (n - len(gr5)) % 6 != 0:
            if len(remains) == 0:
                print("存在重大矛盾，无法分组")
                exit(0)
            p = random.randint(0, len(remains) - 1)
            gr5.append(remains[p])
            del remains[p]
        gr6 += remains
        result5 = self.schedule(gr5, 5)
        result6 = self.schedule(gr6, 6)
        return result5[0] + result6[0], result5[1] + result6[1]

if __name__ == '__main__':
    try:
        x = solution()
        Tk().withdraw()
        print(f"请在弹出的窗口中选择要打开的文件")
        filename = askopenfilename()
        print(f"从文件：{filename} 中读取数据……")
        x.input(filename)
        result = x.solve()
        for i, gr in enumerate(result[0]):
            print(f"小组 {i + 1}:")
            print(f"组长: {gr.leader.name}")
            print(f"成员: ", end = '')
            printed = False
            for stu in gr.member:
                if stu.name != gr.leader.name:
                    if printed:
                        print(end = ', ')
                    else:
                        printed = True
                    print(stu.name, end = '')
            print('\n')
        print(f"最终满意度: {40 * exp(-(result[1] ** .25) / 100) + 60}")
        if result[1] != 0:
            print("以下同学的要求没有得到完全满足：")
            printed = False
            for gr in result[0]:
                for stu in gr.member:
                    if stu.judge(gr) != 0:
                        if printed:
                            print(end = ', ')
                        else:
                            printed = True
                        print(stu.name, end = '')
            print()
        output_filename = f"{filename[:filename.rfind('.')]} - 分组结果.csv"
        with open(output_filename, 'w') as f:
            for i, gr in enumerate(result[0]):
                print(i + 1, gr.leader.name, sep = ',', end = '', file = f)
                for stu in gr.member:
                    if stu.name != gr.leader.name:
                        print(f",{stu.name}", end = '', file = f)
                print(file = f)
            print(file = f)
            print(f"最终满意度,{40 * exp(-(result[1] ** .25) / 100) + 60}", file = f)
            if result[1] != 0:
                print("以下同学的需求没有被完全满足：", file = f)
                printed = False
                for gr in result[0]:
                    for stu in gr.member:
                        if stu.judge(gr) != 0:
                            if printed:
                                print(end = ',', file = f)
                            else:
                                printed = True
                            print(stu.name, end = '', file = f)
        print(f"分组结果已经同步保存到 {output_filename} 中")
    except SystemExit:
        pass
    except:
        print("糟糕！我们好像遇到了一些错误。下面是一些可能有用的信息：")
        traceback.print_exc()

        