#include <bits/stdc++.h>
using namespace std;
typedef long long int64;

struct num { // 在模 32771 意义下进行运算，避免小数 / 分数的麻烦
    static const int P = 32771;
    int realValue;
    num(int setValue = 0) : realValue(setValue) {}
    num pow(int nth) const { // 快速幂
        int result = 1, now = realValue;
        while (nth) {
            if (nth & 1) result = result * now % P;
            now = now * now % P;
            nth >>= 1;
        }
        return result;
    }
    num inv() const { return pow(P - 2); } // 求逆元
    num operator + (const num &b) const { return num((realValue + b.realValue) % P); }
    num operator - (const num &b) const { return num((realValue - b.realValue + P) % P); }
    num operator * (const num &b) const { return num((realValue * b.realValue) % P); }
    num operator / (const num &b) const { return *this * b.inv(); }
    bool operator < (const num &b) const { return realValue < b.realValue; } // 供: next_permutation 用
};

void sort3(int &a, int &b, int &c) { // 排序 3 个数
    if (a > b) swap(a, b);
    if (a > c) swap(a, c);
    if (b > c) swap(b, c);
}

void sort4(int &a, int &b, int &c, int &d) { // 排序 4 个数
    sort3(a, b, c);
    if (d < a) swap(a, d);
    sort3(b, c, d);
}