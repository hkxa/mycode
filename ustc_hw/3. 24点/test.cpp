#include "solve24.hpp"

int main() {
    clock_t st = clock();
    printf("[+] solve24: programming is warming up... ");
    getAllPossibleWay(0, 0);
    printf("%.3lfs\n", double(clock() - st) / CLOCKS_PER_SEC);
    st = clock();
    printf("[+] solve24: Self-Check begin.. ");
    assert(p24_solve(5, 5, 5, 1) != "<Fail>");
    assert(p24_solve(3, 3, 8, 8) != "<Fail>");
    assert(p24_solve(2, 3, 10, 11) == "<Fail>");
    assert(p24_solve(1, 9, 12, 13) == "<Fail>");
    int countSuccess = 0;
    for (int a = 1; a <= 20; ++a)
        for (int b = a; b <= 20; ++b)
            for (int c = b; c <= 20; ++c)
                for (int d = c; d <= 20; ++d)
                    if (p24_solve(a, b, c, d) != "<Fail>")
                        ++countSuccess;
    assert(countSuccess == 5706);
    printf("%.3lfs\n", double(clock() - st) / CLOCKS_PER_SEC);
    printf("[:] solve24: input your 4 numbers in [1, 20]: ");
    int a, b, c, d;
    cin >> a >> b >> c >> d;
    string result = p24_solve(a, b, c, d);
    st = clock();
    if (result == "<Fail>")
        printf("[x] solve24: no solution for [%d, %d, %d, %d]\n", a, b, c, d);
    else
        printf("[+] solve24: found solution: %s\n", result.c_str());
    printf("[+] Complete solve in %.3lfs\n", double(clock() - st) / CLOCKS_PER_SEC);
    printf("[+] Completed whole program in %.3lfs\n", (double)clock() / CLOCKS_PER_SEC);
    return 0;
}