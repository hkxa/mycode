#include "lib.hpp"

struct possibly_way { // 记录方案: 0数字, 21+, 22-, 23*, 24/
    int record[7];
    num *val;
    int cur_rec, cur_val;
    num subcalc() { // 计算当前前缀表达式的值
        ++cur_rec;
        if (record[cur_rec] == 0) return val[++cur_val];
        else switch (record[cur_rec]) {
            case 21: return subcalc() + subcalc();
            case 22: return subcalc() - subcalc();
            case 23: return subcalc() * subcalc();
            default: return subcalc() / subcalc();
        }
    }
    num calc(num a[4]) { // 代入求值
        val = a;
        cur_rec = cur_val = -1;
        return subcalc();
    }
    string subget_expression(int level) { // 计算当前前缀表达式的中缀表达式
        ++cur_rec;
        if (record[cur_rec] == 0){
            stringstream ss;
            ss << val[++cur_val].realValue;
            return ss.str();
        } 
        string result = "";
        bool add_bracket = (level == 2 && record[cur_rec] <= 22); // 如果先加减再乘除，加括号
        switch (record[cur_rec]) {
            case 21: result = subget_expression(1) + " + " + subget_expression(1); break;
            case 22: result = subget_expression(1) + " - " + subget_expression(1); break;
            case 23: result = subget_expression(2) + " * " + subget_expression(2); break;
            default: result = subget_expression(2) + " / " + subget_expression(2); break;
        }
        if (add_bracket) result = "(" + result + ")"; // 加括号
        return result;
    }
    string get_expression(num a[4]) { // 代入求表达式
        val = a;
        cur_rec = cur_val = -1;
        return subget_expression(0) + " = 24";
    }
};
vector<possibly_way> pw;

possibly_way gapw_now; // 当前枚举到的方案
void getAllPossibleWay(int i, int j) { // 获取所有可能的方案
    if (i == 7) {
        pw.push_back(gapw_now);
        return;
    }
    if ((5 - i > 3 - j && i < (j << 1)) || i >= 5) {
        gapw_now.record[i] = 0;
        getAllPossibleWay(i + 1, j);
    }
    if (j < 3)
        for (int x = 21; x <= 24; ++x) {
            gapw_now.record[i] = x;
            getAllPossibleWay(i + 1, j + 1);
        }
}

string p24_solve(int a, int b, int c, int d) { // 枚举所有可能的情况，解 24 点
    sort4(a, b, c, d);
    num n[4];
    int i = 0;
    for (possibly_way way : pw) {
        n[0].realValue = a;
        n[1].realValue = b;
        n[2].realValue = c;
        n[3].realValue = d;
        do {
            if (way.calc(n).realValue == 24) return way.get_expression(n);
        } while (next_permutation(n, n + 4));
    }
    return "<Fail>";
}

// 5 * (5 - (1 / 5))
// *.-./..
// 23 0 22 0 24 0 0
// *.-/...
// 23 0 22 24 0 0 0