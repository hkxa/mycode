#include <stdio.h>

int getDay(int y, int m) {
    switch (m) {
        case 1: case 3: case 5: case 7: case 8: case 10: case 12:
            return 31;
        case 4: case 6: case 9: case 11:
            return 30;
        default: // 2
            return 28 + (y % 4 == 0 && (y % 100 != 0 || y % 400 == 0));
    }
}

int getWeek(int y, int m) {
    if (m <= 2) {
        y -= 1;
        m += 12;
    }
    int c = y / 100;
    y %= 100;
    return ((y + y / 4 + c / 4 - 2 * c + (13 * m + 13) / 5) % 7 + 7) % 7;
}

int main() {
    int y, m;
    scanf("%d%d", &y, &m);
    printf("Sun Mon Tue Wed Thu Fri Sat\n");
    int day = getDay(y, m), wk = getWeek(y, m);
    for (int i = 0; i < wk; ++i) printf("    ");
    for (int i = 1; i <= day; ++i) {
        printf("%2d  ", i);
        wk = (wk + 1) % 7;
        if (wk == 0 || i == day) printf("\n");
    }
    return 0;
}