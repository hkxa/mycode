{#include <stdio.h>
#include <string.h>

#define N 100007

int n, m;
char s[N], t[N];

int check(int p) {
    for (int i = 0; i < m; ++i)
        if (s[i + p] != t[i]) return 0;
    return 1;
}

int main() {
    scanf("%s%s", s, t);
    n = strlen(s);
    m = strlen(t);
    for (int i = n - m; i >= 0; --i)
        if (check(i)) {
            for (int j = 0; j < i; ++j) putchar(s[j]);
            for (int j = i + m; j < n; ++j) putchar(s[j]);
            return 0;
        }
    for (int j = 0; j < n; ++j) putchar(s[j]);
    return 0;
}}