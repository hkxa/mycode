#ifndef header_BIGINT
#define header_BIGINT
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct bint {
    int len;
    int a[3100];
} bint;

bint bint_const1 = {1, {1}}, bint_const0 = {1, {0}};

const bint pr[] = {
    {1, {2}},
    {1, {3}},
    {1, {7}},
    {1, {61}},
    {1, {24251}},
    {1, {479}},
    {1, {4337}},
    {1, {8753}},
    {1, {13421}},
    {1, {18253}}
};

void put(FILE* f, bint *a) {
    for (int i = a->len - 1; i >= 0; --i)fprintf(f, "%d", a->a[i]);
    fprintf(f, "\n");
}

int cmp(bint *a, bint *b) {
    if (a->len - b->len) return a->len - b->len;
    for (int i = a->len - 1; i >= 0; --i)if (a->a[i] - b->a[i]) return a->a[i] - b->a[i];
    return 0;
}

int ncmp(int *a, int *b, int n) {
    for (int i = 0, *x = a + n - 1, *y = b + n - 1; i < n; ++i, x--, y--) {
        if (*x - *y) return *x - *y;
    }
    return 0;
}

bint add1(bint *a, bint *b, int base) {
    bint c = bint_const0;
    c.len = a->len < b->len ? b->len : a->len;
    c.a[0] = 0;
    for (int i = 0; i < c.len; ++i) {
        c.a[i] += a->a[i] + b->a[i];
        c.a[i + 1] = c.a[i] / base;
        c.a[i] %= base;
    }
    if (c.a[c.len]) ++c.len;
    return c;
}

bint mul1(bint *a, bint *b, int base) {
    bint c = bint_const0;
    c.len = a->len + b->len - 1;
    for (int i = 0; i < b->len; ++i) {
        for (int j = 0; j < a->len; ++j) {
            c.a[i + j] += a->a[j] * b->a[i];
            c.a[i + j + 1] += c.a[i + j] / base;
            c.a[i + j] %= base;
        }
    }
    if (c.a[c.len]) ++c.len;
    return c;
}

bint minus1(bint *a, bint *b, int base) {
    bint c = bint_const0;
    c.len = a->len;
    for (int i = 0; i < c.len; ++i) {
        if (i < b->len) c.a[i] += a->a[i] - b->a[i];
        else c.a[i] += a->a[i];
        if (c.a[i] < 0) {
            --c.a[i + 1];
            c.a[i] += base;
        }
    }
    while (c.len > 0 && !c.a[c.len - 1]) --c.len;
    if (!c.len) c.len = 1;
    return c;
}

bint div2(bint *a, int base) {
    int t[a->len + 10], len = 0;
    bint c = *a;
    for (int i = c.len - 1; i >= 0; --i) {
        t[len] = c.a[i] / 2;
        c.a[i] -= t[len] * 2;
        if (i) c.a[i - 1] += c.a[i] * base;
        len++;
    }
    c = bint_const0;
    c.len = len;
    for (int i = 0; i < len; ++i) c.a[i] = t[len - i - 1];
    if (!c.a[c.len - 1]) --c.len;
    return c;
}

bint mulint(bint *a, int b, int base) {
    bint c = bint_const0;
    c.len = a->len;
    for (int i = 0; i < a->len; ++i) c.a[i] = a->a[i] * b;
    for (int i = 0; i < a->len; ++i) {
        c.a[i + 1] += c.a[i] / base;
        c.a[i] %= base;
    }
    while (c.a[c.len]) {
        ++c.len;
        c.a[c.len] = c.a[c.len - 1] / base;
        c.a[c.len - 1] %= base;
    }
    return c;
}

bint BaseToDec(bint *a, int base) {
    bint b = bint_const0, c = bint_const1;
    bint ten = {2, {1, 0}};
    for (int i = 0; i < a->len; ++i) {
        if (a->a[i]) {
            bint t = mulint(&c, a->a[i], 10);
            b = add1(&b, &t, 10);
        }
        c = mulint(&c, base, 10);
    }
    return b;
}

int Ncmp(int *a, int *b, int n) {
    for (int i = 0; i < n; ++i)if (*(a - i) - *(b - i))return *(a - i) - *(b - i);
    return 0;
}

bint Mod(bint *a, bint *b, int base) {
    int x = cmp(a, b);
    if (x < 0) return *a;
    if (!x) return bint_const0;
    if (a->len == 1) return (bint) {1, {a->a[0] % b->a[0]}};
    bint c = *a;
    int l = b->len;
    long long tb = (long long)b->a[l - 1] * base + b->a[l - 2];
    for (int i = c.len - 1; i >= l - 1; --i) {
        long long tc = (long long)c.a[i] * base + c.a[i - 1];
        long long m = tc / tb;
        bint t = mulint(b, m, base);
        bint tt = t;
        if (tt.len > l) {
            tt.len = l;
            tt.a[l - 1] += tt.a[l] * base;
            tt.a[l] = 0;
        }
        while (Ncmp(&c.a[i], &tt.a[l - 1], l) < 0) {
            t = minus1(&t, b, base);
            tt = t;
            if (tt.len > l) {
                tt.len = l;
                tt.a[l - 1] += tt.a[l] * base;
                tt.a[l] = 0;
            }
            m--;
        }
        for (int j = i - l + 1; j <= i; ++j) {
            c.a[j] -= tt.a[j + l - i - 1];
            if (c.a[j] < 0) {
                c.a[j] += base;
                c.a[j + 1]
                --;
            }
        }
        c.a[i - 1] += c.a[i] * base;
        c.a[i] = 0;
    }
    c.a[l - 1] = c.a[l - 2] / base;
    c.a[l - 2] %= base;
    while (c.len > 0 && !c.a[c.len - 1]) --c.len;
    if (!c.len) c.len = 1;
    return c;
}

bint mod0(bint *a, bint *b, int base) {
    int x = cmp(a, b);
    if (x < 0)return *a;
    if (!x)return bint_const0;
    bint c = *a;
    for (int j = c.len - b->len; j >= 0; --j) {
        bint t = *b, last = bint_const0;
        t.len = b->len + j;
        for (int i = b->len - 1; i >= 0; --i)t.a[i + j] = b->a[i];
        for (int i = 0; i < j; ++i)t.a[i] = 0;
        bint b0 = t;
        while (cmp(&t, &c) <= 0) {
            last = t;
            t = add1(&t, &b0, base);
        }
        c = minus1(&c, &last, base);
    }
    return c;
}

bint power(bint *A, bint *n, bint *p, int base) {
    if (!cmp(n, &bint_const1))return *A;
    bint m = div2(n, base);
    bint t = power(A, &m, p, base);
    t = mul1(&t, &t, base);
    t = Mod(&t, p, base);
    if (n->a[0] & 1) {
        t = mul1(&t, A, base);
        t = Mod(&t, p, base);
    }
    return t;
}

bint power1(bint *A, bint *n, bint *p, int base) {
    int len, a[3100];
    bint m = *n;
    while (cmp(&m, &bint_const0)) {
        a[len++] = m.a[0] & 1;
        m = div2(&m, base);
    }
    bint t = bint_const1;
    for (int i = len - 1; i >= 0; ++i) {
        t = mul1(&t, &t, base);
        t = Mod(&t, p, base);
        if (a[i]) {
            t = mul1(&t, A, base);
            t = Mod(&t, p, base);
        }
    }
    return t;
}

bint power2(bint *a, bint *n, bint *p) {
    bint c = bint_const1;
    for (int i = n->len - 1; i >= 0; --i) {
        c = mul1(&c, &c, 2);
        c = Mod(&c, p, 2);
        if (n->a[i]) {
            c = mul1(&c, a, 2);
            c = Mod(&c, p, 2);
        }
    }
    return c;
}

bint power2b(bint *a, bint *n, bint *p, int base) {
    bint c = bint_const1;
    for (int i = n->len - 1; i >= 0; --i) {
        for (int k = base >> 1; k; k >>= 1) {
            c = mul1(&c, &c, base);
            c = Mod(&c, p, base);
            if (n->a[i] & k) {
                c = mul1(&c, a, base);
                c = Mod(&c, p, base);
            }
        }
    }
    return c;
}

void read10(FILE *fp, bint *a, int Base) {
    *a = bint_const0;
    static const int x[10] = {1, 10, 100, 1000, 10000, 100000, 1000000};
    static char s[1200];
    fscanf(fp, "%s", s);
    int l = strlen(s);
    for (int i = 0; i < l; ++i) {
        *a = mulint(a, 10, 1 << Base);
        a->a[0] += s[i] - 48;
    }
    *a = mulint(a, 1, 1 << Base);
}

#endif