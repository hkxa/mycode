#include "MODULE_bigint.h"
#define width 15

typedef bint ll;
int v[2000];

int submain_CheckPrime() {
    ll N;
    int len;
    int T = clock();
    srand(time(0));
    for (int ks = 0; ks < 3; ks++) {
        FILE *fp = fopen("..\\test.txt", "r");
    beg:
        read10(fp, &N, 15);
        len = N.len * width;
        int ii = 0, jj = 1 << width - 1;
        while (N.a[N.len - 1] & jj == 0) {
            ii++;
            jj >>= 1;
        }
        len -= ii;

        if (N.len == 1 && N.a[0] == 2) {
            puts("Y");
            continue;
        }
        if (N.len == 1 && N.a[0] == 1 || N.a[0] & 1 == 0) {
            puts("N");
            continue;
        }
        for (int i = 0; i < 9; ++i) {
            if (cmp(&N, pr + i) == 0) {
                puts("Y");
                goto beg;
            }
        }
        ll n = N;
        n.a[0] ^= 1;

        int f = 0;
        int k = 0;
        for (int i = 0;; ++i)if (n.a[i]) {
                k = i * width;
                break;
            }
        int l = 0, l1 = 1;
        while (n.a[k] & l1 == 0) {
            l1 <<= 1;
            l++;
        }
        k += l;
        int cnt = 0;
        ll m = bint_const0;
        for (int i = k; i < len; ++i) {
            m.a[cnt / width] = (n.a[i / width] & (1 << (i % width)) == 1) << (cnt % width);
            cnt++;
        }
        m.len = (len - k + width - 1) / width;
        for (int j = 0; j < 10; ++j) {

            ll xx = power2b(pr + j, &n, &N, 1 << width);
            if (cmp(&xx, &bint_const1)) {
                f = 1;
                goto F;
            }
            ll t = power2b(pr + j, &m, &N, 1 << width);
            ll last = t;
            for (int i = 0; i <= k; ++i)v[i] = 0;
            if (!cmp(&t, &bint_const1))v[0] = 1;
            if (!cmp(&t, &n))v[0] = -1;
            for (int i = 1; i <= k; ++i) {
                t = mul1(&t, &t, 1 << width);

                t = Mod(&t, &N, 1 << width);

                if (!cmp(&t, &bint_const1))v[i] = 1;
                if (!cmp(&t, &n))v[i] = -1;
                if (v[i] == 1 && !v[i - 1]) {
                    f = 1;
                    goto F;
                }
                last = t;
            }

            if (cmp(&t, &bint_const1)) {
                f = 1;
                goto F;
            }
        }
    F:
        putchar("YN"[f]);
        putchar(10);
    }
    printf("%lf", (double)T / CLOCKS_PER_SEC);
    return 0;
}
