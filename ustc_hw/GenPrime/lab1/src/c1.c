#include "FUNC_check_prime.h"
#include "FUNC_gen_prime.h"

int main() {
    char s[1000];
    printf("======= Selecting mode =======\n"
           "1. Generate a prime\n"
           "2. Check whether three numbers in ./test.txt is a prime\n"
           "==============================\n"
           "> ");
redo:;
    gets(s);
    int choice = atoi(s);
    if (choice == 1) {
        printf("> Input length: ");
        submain_GenPrime();
    } else if (choice == 2) {
        submain_CheckPrime();
    } else {
        printf("error\n> ");
        goto redo;
    }
    return 0;
}
