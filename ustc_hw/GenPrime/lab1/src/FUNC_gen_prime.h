#include "MODULE_bigint.h"
#define width 15
typedef bint ll;
int hhh[] = {3, 5, 7, 11, 13, 17, 19, 23, 29};

int v[2000];

int Modi(bint *a, int b, int base) {
    bint c = *a;
    for (int i = c.len - 1; i > 0; --i) {
        c.a[i] %= b;
        c.a[i - 1] += c.a[i] * base;
    }
    return c.a[0] % b;
}

int submain_GenPrime() {
    ll N;
    int len;
    scanf("%d", &len);
    int T = clock();
    int TM = time(0);
    srand(TM);
    if (len == 2)return printf("3\n"), 0;
    if (len == 3)return printf(rand() & 1 ? "5\n" : "7\n"), 0;

    int cnt = 0;
    while (1) {
        while (rand() % 3 == 0) rand();
        printf("%d times attempt:\n", ++cnt);
        N = bint_const1;
        for (int i = 1; i < len - 1; ++i)N.a[i / width] |= (rand() & 1) << (i % width);
        N.len = (len + width - 1) / width;
        N.a[N.len - 1] |= 1 << ((len - 1) % width);
        ll n = N;
        n.a[0] ^= 1;
        int f = 0, k = 0, l = 0, l1 = 1;
        for (int i = 0; i < 9; ++i)if (!Modi(&N, hhh[i], 1 << width)) {
                f = 1;
                goto F;
            }
        for (int i = 0;; ++i)if (n.a[i]) {
                k = i * width;
                break;
            }
        while (n.a[k] & l1 == 0) {
            l1 <<= 1;
            l++;
        }
        k += l;
        int cnt = 0;
        ll m = bint_const0;
        for (int i = k; i < len; ++i) {
            m.a[cnt / width] = (n.a[i / width] & (1 << (i % width)) == 1) << (cnt % width);
            cnt++;
        }
        m.len = (len - k + width - 1) / width;
        for (int j = 0; j < 10; ++j) {
            ll xx = power2b(pr + j, &n, &N, 1 << width);
            if (cmp(&xx, &bint_const1)) {
                f = 1;
                goto F;
            }
            ll t = power2b(pr + j, &m, &N, 1 << width);



            ll last = t;
            for (int i = 0; i <= k; ++i)v[i] = 0;
            if (!cmp(&t, &bint_const1))v[0] = 1;
            if (!cmp(&t, &n))v[0] = -1;
            for (int i = 1; i <= k; ++i) {
                t = mul1(&t, &t, 2);
                t = Mod(&t, &N, 2);
                if (!cmp(&t, &bint_const1))v[i] = 1;
                if (!cmp(&t, &n))v[i] = -1;
                if (v[i] == 1 && !v[i - 1]) {
                    f = 1;
                    goto F;
                }
                last = t;
            }
            if (cmp(&t, &bint_const1)) {
                f = 1;
                goto F;
            }
        }
        F:
        if (!f) break;
    }
    N = BaseToDec(&N, 1 << width);
    FILE *fp = fopen("..\\result.txt", "w");
    put(fp, &N);
    T = clock() - T;
    printf("Time Used: %.3lf", (double)T / CLOCKS_PER_SEC);
    return 0;
}
