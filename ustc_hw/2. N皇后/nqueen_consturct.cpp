/*************************************
 * @problem:      N皇后.
 * @author:       brealid.
 * @time:         2022-04-09.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

#define AnswerAppend(x) kout << (x) << ' '

signed main() {
    file_io::set_to_file("nqueen", 0, 1);
    int n;
    cerr << "[N-Queen] [INFO] 请输入 N: ";
    cin >> n;
    if (2 <= n && n <= 3) {
        cerr << "[N-Queen] [ERROR] N <= 0, 无意义\n";
        return 0;
    }
    if (2 <= n && n <= 3) {
        cerr << "[N-Queen] [ERROR] 2 <= N <= 3, 无解\n";
        return 0;
    }
    cerr << fixed << setprecision(2) << "[N-Queen] [INFO] N 皇后的解正在构造, 预计需要 " << 7.886305e-09 * n * log(n) << " 秒\n";
    clock_t st = clock();
    kout << n << '\n';
    if (n % 6 != 2 && n % 6 != 3) {
        for (int i = 2; i <= n; i += 2) AnswerAppend(i);
        for (int i = 1; i <= n; i += 2) AnswerAppend(i);
    } else if (n % 6 == 2) {
        int k = n >> 1;
        for (int i = k; i <= n; i += 2) AnswerAppend(i);
        for (int i = k % 2 ? 1 : 2; i <= k - 2; i += 2) AnswerAppend(i);
        for (int i = k + 3; i <= n; i += 2) AnswerAppend(i);
        for (int i = k % 2 ? 2 : 1; i <= k + 1; i += 2) AnswerAppend(i);
    } else if (n % 6 == 3) {
        int k = n >> 1;
        for (int i = k; i < n; i += 2) AnswerAppend(i);
        for (int i = k % 2 ? 1 : 2; i <= k - 2; i += 2) AnswerAppend(i);
        for (int i = k + 3; i < n; i += 2) AnswerAppend(i);
        for (int i = k % 2 ? 2 : 1; i <= k + 1; i += 2) AnswerAppend(i);
        AnswerAppend(n);
    }
    cerr << fixed << setprecision(2) << "[N-Queen] [INFO] 构造完毕，实际用时 " << double(clock() - st) / CLOCKS_PER_SEC << " 秒\n";
    kout << '\n';
    return 0;
}