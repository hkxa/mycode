/*************************************
 * @problem:      N皇后.
 * @author:       brealid.
 * @time:         2022-04-09.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

#define AnswerAppend(x) kout << (x) << ' '

bool verify_and_set(int &x, int pos) {
    if (x >> pos & 1) return false;
    x |= 1 << pos;
    return true;
}

signed main() {
    cerr << "[N-Queen] [INFO] 正在打开存档...\n";
    freopen("nqueen.out", "r", stdin);
    int n;
    kin >> n;
    if (2 <= n && n <= 3) {
        cerr << "[N-Queen] [ERROR] 存档错误: N <= 0, 无意义\n";
        return 0;
    }
    if (2 <= n && n <= 3) {
        cerr << "[N-Queen] [ERROR] 存档错误: 2 <= N <= 3, 无解\n";
        return 0;
    }
    clock_t st = clock();
    int *row = new int[n / 32 + 1], *lu = new int[n / 16 + 1], *ru = new int [n / 16 + 1];
    memset(row, 0, sizeof(int) * (n / 32 + 1));
    memset(lu, 0, sizeof(int) * (n / 16 + 1));
    memset(ru, 0, sizeof(int) * (n / 16 + 1));
    cerr << fixed << setprecision(2) << "[N-Queen] [INFO] 存储空间初始化完毕, 消耗空间大约 " << abs((double)n / (1 << 31)) << " GB\n";
    cerr << fixed << setprecision(2) << "[N-Queen] [INFO] 开始验证, 预计需要 " << 5.344887e-09 * n * log(n) << " 秒\n";
    for (int i = 1, y; i <= n; ++i) {
        kin >> y;
        if (!verify_and_set(row[y / 32], y % 32) || 
            !verify_and_set(lu[(y + i) / 32], (y + i) % 32) || 
            !verify_and_set(ru[(y + n - i) / 32], (y + n - i) % 32)) {
            cerr << "[N-Queen] [INFO] 存在矛盾的情况, 皇后可以相互攻击\n";
            cerr << fixed << setprecision(2) << "[N-Queen] [INFO] 程序中断 (在开始验证 " << double(clock() - st) / CLOCKS_PER_SEC << " 秒后)\n";
            return 0;
        }
    }
    cerr << "[N-Queen] [INFO] 验证完成, 符合要求\n";
    cerr << fixed << setprecision(2) << "[N-Queen] [INFO] 程序正常结束 (在开始验证 " << double(clock() - st) / CLOCKS_PER_SEC << " 秒后)\n";
    return 0;
}