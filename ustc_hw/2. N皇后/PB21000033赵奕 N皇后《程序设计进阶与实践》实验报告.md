# 《程序设计进阶与实践》实验报告
实验名称：梅森质数判定  
姓名：赵奕 学号：PB21000033 日期：2022.4.10  
实验环境：
- CPU: AMD Ryzen 5 5600U with Radeon Graphics @ 2.30 GHz
- 内存：16.00 GB
- 操作系统：Windows 10
- gcc 版本号: 9.2.0 （有 -O2 优化）

## 1. 问题分析与思路
### 1.1 对于给定正整数 N，在 5 分钟内生成 1 个解，以文件形式存储。
#### 1.1.1 产生解
通过经典的回溯算法可以构造一组解，但是接近于暴力求解，不断的递归会消耗大量时间。  
此外，由于本实验只要求生成 1 个解，递归无疑会做许多没有必要的尝试。  
以 8 皇后为例，我们很难看出所有的解，但是显然有一组解 ``(1,2) (2,4) (3,6) (4,8) (5,1) (6,3) (7,5) (8,7)`` 满足条件。这启发我们，可以通过一个特定的算法，使任意数都可通过简单的构造得到一组解

#### 1.1.2 输出解
采用“输出文件的第 i 个数 x 表示皇后在第 i 列的第 x 行”的方式可以节省大量空间，并且易懂  
输出采用了自己写的输出函数，用 ``fread`` 作为输出调用函数，并使用 $2^{21}$ 的缓冲区以加快速度

### 1.2 验证文件中的解是否合法

首先，若读取文件时采用一个一个数来读取的方式，经过测试得效率过低，要花费大量时间用在读取上。因此采用一次性读入缓存区在进行解码的方法来提高效率。

在判断上，只需要判断 $y, y-i, y+i$ 分别有没有重复。即：对于所有的 $y_i,y_i-i,y_i+i$ 看是否有重复的出现即可

### 1.3 构造

**算法分析：** 可以通过构造解的形式，将构造的序列分为4类

详细证明参见 [证明](https://blog.csdn.net/lyy289065406/article/details/78955101) ，基本思路是：偶数用序列构造，奇数在偶数基础在最后一行一列上加上多出的一个皇后。

当 n mod 6 != 2 或 n mod 6 != 3 时：

2,4,6,8,...,n,1,3,5,7,...,n-1 (n为偶数)

2,4,6,8,...,n-1,1,3,5,7,...,n (n为奇数)

当 n mod 6 == 2 或 n mod 6 == 3 时 (当n为偶数,k=n/2；当n为奇数,k=(n-1)/2)

k,k+2,k+4,...,n, 2,4,...,k-2, k+3,k+5,...,n-1, 1,3,5,...,k+1  (k为偶数,n为偶数)

k,k+2,k+4,...,n-1,2,4,...,k-2,k+3,k+5,...,n-2,1,3,5,...,k+1,n (k为偶数,n为奇数)

k,k+2,k+4,...,n-1,1,3,5,...,k-2,k+3,...,n,2,4,...,k+1      (k为奇数,n为偶数)

k,k+2,k+4,...,n-2,1,3,5,...,k-2,k+3,...,n-1,2,4,...,k+1,n    (k为奇数,n为奇数)

## 2. 核心代码说明
### 2.1 输入输出 
```cpp
#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;
```
分析：

C++中可以采用stream读取文本文件，基本方式是一次一行，但是当文件行数较多时，文件读取IO次数就会随之增加，文件读取的时间会急剧增长。因为文件IO的时间要远大于CPU在内存中处理数据的时间，假如IO时间是毫秒级的，那么CPU在内存处理数据是纳秒级的。因此，C++中文本文件读取优化要解决的基本问题之一就是减少IO次数，最常用的方法之一是缓冲，也就是一次性将数据会部读入内存之后再处理。具体参见 [探寻C++最快的读取文件的方案](https://www.cnblogs.com/SBSOI/p/5575060.html) 

首先将文件中所有数据全部读入缓冲数组，再进行分析：
p指针不断移动，将字符串转化为数字。在p遇到空格分隔前，每位数字都要乘对应的位数再累加。
这样使用fread函数来读取数据，大大减少了文件读取的IO次数，因此效率远高于依次读取各个数字。实现了读取文件的优化。

### 2.2 合法性判断
```cpp
for (int i = 1, y; i <= n; ++i) {
    kin >> y;
    if (!verify_and_set(row[y / 32], y % 32) || 
        !verify_and_set(lu[(y + i) / 32], (y + i) % 32) || 
        !verify_and_set(ru[(y + n - i) / 32], (y + n - i) % 32)) {
        cerr << "[N-Queen] [INFO] 存在矛盾的情况, 皇后可以相互攻击\n";
        cerr << fixed << setprecision(2) << "[N-Queen] [INFO] 程序中断 (在开始验证 " << double(clock() - st) / CLOCKS_PER_SEC << " 秒后)\n";
        return 0;
    }
}
```
其中 verify_and_set(x, y) 可以查验 x 的第 y 位是否为 1，并置为 1
```cpp
bool verify_and_set(int &x, int pos) {
    if (x >> pos & 1) return false;
    x |= 1 << pos;
    return true;
}
```
在判断的时候，我采用了类似 bitset 的方式，利用位运算优化了空间，只需要原先的 $\frac{1}{8}$

## 3. 测试运行
1. 正确性检验：输入 4, 确认程序输出正确    
   ![fig-1-nqueenout](fig-1-nqueenout.png)  
2. 速度检验：输出 6000000000 ($6\cdot 10^9$)    
   ![fig-2-stdout](fig-2-stdout.png)  
   在 300s 内运行完成
3. 生成一个大小为 $16\times 16$ 的错误地图, 运行验证程序  
   ![fig-3-stdout](fig-3-stdout.png)  

## 4. 备注
### 4.1 关于其他的解决方式
在本实验中，由于本实验只要求生成1个解，针对此问题我们采取了构造解法，可以将时间复杂度控制在$O(n)$

事实上，若将此题稍作拓展（如要求给出所有解的总数），构造法则不再适用。  
此时我们应当采取其他算法，如： 
- 回溯法: <https://cloud.tencent.com/developer/article/1424758> 
- 爬山模拟退火算法: <https://www.jianshu.com/p/ef728b366f78>
- 位运算法: <https://www.cnblogs.com/tiny656/p/3918367.html>

都可以成功获得全部的解，应用范围更广。

## 5. 总结
通过本次N皇后判定实验，我掌握了：
1. C++语言文件读取写入的优化算法。
2. 多种n皇后判定的算法与定理。
3. 提高了编程与debug能力，加强了对C++语言的理解认识。
4. 有了初步进行单元测试的能力。
5. 提高了分解，抽象，概括问题的能力，提升了计算思维。

当然实验还有许多能改进的地方，比如文件输入的设置，验证的优化。且程序运行耗时较长，还有一些算法可以优化提高程序运行效率，今后还有很大的学习进步空间。

### 5.1 另附
源代码文件：
- nqueen_consturct.cpp (生成解)
- nqueen_verify.cpp (验证解)

### 5.2 参考资料

1. https://www.cnblogs.com/SBSOI/p/5575060.html
2. https://blog.csdn.net/qq_43166758/article/details/107291949
3. https://www.cnblogs.com/tiny656/p/3918367.html
4. https://blog.csdn.net/forever_dreams/article/details/82314237
5. https://cloud.tencent.com/developer/article/1424758