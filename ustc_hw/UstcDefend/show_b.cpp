//#define SHOW_CONSOLE
#include <graphics.h>
#include <vector>
#include <string>
#include <cstdlib>
#include <direct.h>
#include <bits/stdc++.h>
using namespace std;
#define DEBUG

string map_file = "map\\1.txt";
PIMAGE pimg_forbiddenBuilding = newimage();

void draw_block(int x, int y) {
	x -= 40;
	y -= 40;
	line(x, y, x, y + 80);
	line(x, y, x + 80, y);
	line(x + 80, y, x + 80, y + 80);
	line(x, y + 80, x + 80, y + 80);
}

void draw_highblock(int x, int y) {
	draw_block(x, y);
	// putimage_rotatezoom(nullptr, pimg_forbiddenBuilding, x, y, 0.5f, 0.5f, 0, 0.8, 1, 0xff, 1);
	line (x + 40, y - 40, x - 40, y + 40);
	line (x + 39, y - 40, x - 40, y + 39);
	line (x + 41, y - 40, x - 40, y + 41);
}

mouse_msg msg = {0};

int nRoads, nBlocks;
vector<int> road[100];
struct BlockInfo {
	int high;
	int x, y;
} block_wh[100];

struct monster {
	PIMAGE icon;
	int hp_tot, hp_now, attack;
	int x, y;
	int rd;
	int now_forward;
	bool move() {
		int tx = block_wh[road[rd][now_forward]].x, ty = block_wh[road[rd][now_forward]].y;
		if (x < tx) ++x;
		else if (x > tx) --x;
		else if (y < ty) ++y;
		else --y;
		if (x == tx && y == ty) ++now_forward;
		if (now_forward == (int)road[rd].size()) return true;
		return false;
	}
	void show() {
		if (hp_now > 0) {
			putimage_rotatezoom(nullptr, icon, x, y, 0.5f, 0.5f, 0, 0.58, 1, 0xff, 1);
			for (int i = 0; i < 4; ++i) {
				bool is_bound = i == 0 || i == 3;
				setcolor(CYAN);
				line(x - 14 + is_bound, y - 23 - i, x - 14 + hp_now * 28.0 / hp_tot, y - 23 - i);
				setcolor(LIGHTCYAN);
				line(x - 14 + hp_now * 28.0 / hp_tot, y - 23 - i, x + 14 - is_bound, y - 23 - i);
			}
		}
	}
} enemy[1000];
int ecnt = 0;

int money = 666;

struct person {
	PIMAGE icon;
	string name;
	int x, y;
	int init_x, init_y;
	bool selected;
	int hp_tot, hp_now;
	int attack, range;
	int value;
	int in_war;
	int fth, speed;
	void init(const char *fname, const char *pname, int X, int Y, int _hp, int _att, int _val, int _fth, int _sp, int _rg) {
		icon = newimage();
		name = pname;
		getimage(icon, fname);
		init_x = x = X;
		init_y = y = Y;
		selected = 0;
		hp_tot = hp_now = _hp;
		attack = _att;
		value = _val;
		in_war = 0;
		fth = _fth;
		speed = _sp;
		range = _rg;
	}
	void show() {
		if (!selected) {
			putimage_rotatezoom(nullptr, icon, x, y, 0.5f, 0.5f, 0, 0.825, 1, 0xff, 1);
			if (!in_war) {
				setfont(16, 0, "Times New Roman");
				setcolor(BLACK);
				xyprintf(x - 5, y + 28, "%d", value);
			} else {
				if (hp_now > 0) {
					putimage_rotatezoom(nullptr, icon, x, y, 0.5f, 0.5f, 0, 0.825, 1, 0xff, 1);
					for (int i = 0; i < 4; ++i) {
						bool is_bound = i == 0 || i == 3;
						setcolor(GREEN);
						line(x - 17 + is_bound, y - 30 - i, x - 17 + hp_now * 34.0 / hp_tot, y - 30 - i);
						setcolor(LIGHTGREEN);
						line(x - 17 + hp_now * 34.0 / hp_tot, y - 30 - i, x + 17 - is_bound, y - 30 - i);
					}
				}
			}
			return;
		}
		for (int i = 0; i < nBlocks; ++i)
			if (block_wh[i].high == 1 && msg.x >= block_wh[i].x - 40 && msg.y >= block_wh[i].y - 40 && msg.x <= block_wh[i].x + 40 && msg.y <= block_wh[i].y + 40)
				putimage_rotatezoom(nullptr, icon, block_wh[i].x, block_wh[i].y, 0.5f, 0.5f, 0, 0.83, 1, 0x7f, 1);
		putimage_rotatezoom(nullptr, icon, msg.x, msg.y, 0.5f, 0.5f, 0, 0.83, 1, 0xff, 1);
	}
	int find_target() {
		for (int i = 0; i < ecnt; ++i)
			if (enemy[i].hp_now > 0 && (x - enemy[i].x) * (x - enemy[i].x) + (y - enemy[i].y) * (y - enemy[i].y) <= range * range)
				return i;
		return -1;
	}
} p[12];

PIMAGE pimg_bubble = newimage();

struct bubble {
	bool used = false;
	int target;
	int attack, speed;
	int x, y;
	void move() {
		double len = sqrt((enemy[target].x - x) * (enemy[target].x - x) + (enemy[target].y - y) * (enemy[target].y - y));
		len = max(len / speed, 1.0);
		int dx = (enemy[target].x - x) / len, dy = (enemy[target].y - y) / len;
		x += dx, y += dy;
	}
	void show() {
		if (used) return;
		if (x == enemy[target].x && y == enemy[target].y) {
			used = true;
			enemy[target].hp_now -= attack;
			return;
		}
		putimage_rotatezoom(nullptr, pimg_bubble, x, y, 0.5f, 0.5f, 0, 0.6, 1, 0xff, 1);
	}
} ft[50007];
int bcnt;

int pcnt = 5;

PIMAGE enemy_img_pool[6];

void init_enemy() {
	for (int i = 0; i < 6; ++i) enemy_img_pool[i] = newimage();
	getimage_pngfile(enemy_img_pool[0], "img\\icons8-alpha-64.png");
	getimage_pngfile(enemy_img_pool[1], "img\\icons8-omega-64.png");
	getimage_pngfile(enemy_img_pool[2], "img\\icons8-pi-64.png");
	getimage_pngfile(enemy_img_pool[3], "img\\icons8-mu-64.png");
	getimage_pngfile(enemy_img_pool[4], "img\\icons8-sigma-64.png");
	getimage_pngfile(enemy_img_pool[5], "img\\icons8-ksi-64.png");
}

void init_person() {
	p[0].init("img\\xsy.jpg", "xsy", 241, 552, 100, 10, 10, 45, 5, 175);
	p[1].init("img\\czf.jpg", "czf", 241 + 74, 552, 100, 10, 10, 45, 5, 175);
	p[2].init("img\\syw.jpg", "syw", 241 + 74 * 2, 552, 100, 10, 10, 45, 5, 175);
	p[3].init("img\\zsb.jpg", "zsb", 241 + 74 * 3, 552, 100, 10, 10, 45, 5, 175);
	p[4].init("img\\py.jpg", "py", 241 + 74 * 4, 552, 100, 10, 10, 45, 5, 175);
}

void init_map() {
	FILE *f = fopen(map_file.c_str(), "r");
	fscanf(f, "%d", &nBlocks);
	for (int i = 0; i < nBlocks; ++i) {
		int is_h;
		fscanf(f, "%d%d%d", &is_h, &block_wh[i].x, &block_wh[i].y);
		printf("%d %d %d\n", is_h, block_wh[i].x, block_wh[i].y);
		block_wh[i].high = is_h;
	}
	printf("Read %d Blocks\n", nBlocks);
	fscanf(f, "%d", &nRoads);
	for (int i = 0; i < nRoads; ++i) {
		int cnt;
		fscanf(f, "%d", &cnt);
		road[i].resize(cnt);
		for (int j = 0; j < cnt; ++j)
			fscanf(f, "%d", &road[i][j]);
	}
	printf("Read %d Roads\n", nRoads);
	fscanf(f, "%d", &ecnt);
	int now = 0;
	while (now < ecnt) {
		int cnt, hp, att, rd;
		fscanf(f, "%d%d%d%d", &cnt, &hp, &att, &rd);
		for (int i = now; i < now + cnt; ++i) {
			enemy[i].hp_tot = enemy[i].hp_now = hp;
			enemy[i].attack = att;
			enemy[i].rd = rd;
			enemy[i].x = block_wh[road[rd][0]].x;
			enemy[i].y = block_wh[road[rd][0]].y;
			enemy[i].now_forward = 1;
			enemy[i].icon = enemy_img_pool[rand() % 6];
		}
		now += cnt;
	}
	printf("Read %d Enemies\n", ecnt);
}

int main() {
	srand(time(0));
	MUSIC m;
	m.OpenFile("sound\\����Ķ���.mp3");
	m.Play();

	static const int W = 800, H = 600;
	initgraph(W, H, INIT_RENDERMANUAL);
	setbkmode(TRANSPARENT);
	setcolor(WHITE);
	setfont(27, 0, "΢���ź�");
	// setfont(18, 0, "Consolas");

	PIMAGE pimg_background = newimage(), pimg_warn = newimage(), pimg_gold = newimage(), pimg_gpa = newimage();
	PIMAGE pimg_money = newimage();
	PIMAGE pe_lef = newimage(), pe_rig = newimage();
	// cleardevice();
	getimage_pngfile(pimg_bubble, "img\\bubble.png");
	getimage(pimg_background, "img\\2.jpg");
	getimage_pngfile(pimg_warn, "img\\warn.png");
	getimage_pngfile(pimg_gold, "img\\icons8-gold-bars-96.png");
	getimage_pngfile(pimg_gpa, "img\\icons8-gpa-calculator-48.png");
	getimage_pngfile(pimg_money, "img\\icons8-money-64.png");
	getimage_pngfile(pimg_forbiddenBuilding, "img\\icons8-university-96.png");
	
	init_enemy();
	init_person();
	init_map();
		
	int alpha = 0, dif = 5;
	person *selecting = NULL;
	bool waiting_for_mouse_up = 0;
	int money_bling = 0;
	int warn_time_remain = 305;
	int timx = 0;
	int gpa = 430;
	
	// putimage_rotatezoom(nullptr, pimg_background, W / 2, H / 2, 0.5f, 0.5f, 0, 1, 1, 0xff, 1);
	// putimage_rotatezoom(nullptr, pimg_forbiddenBuilding, W / 2, H / 2, 0.5f, 0.5f, 0, 1, 1, 0xff, 1);
	for (; is_run(); delay_fps(60)) {
		cleardevice();
		putimage_rotatezoom(nullptr, pimg_background, W / 2, H / 2, 0.5f, 0.5f, 0, 1, 1, 0xff, 1);
		// putimage_rotatezoom(nullptr, pimg_forbiddenBuilding, W / 2, H / 2, 0.5f, 0.5f, 0, 1, 1, 0xff, 1);
		// putimage_rotatezoom(nullptr, pimg_forbiddenBuilding, msg.x, msg.y, 0.5f, 0.5f, 0, 0.8, 1, 0xff, 1);
		setcolor(EGERGB(169, 215, 228)); // 100 660; 100 420
		for (int i = 0; i < nBlocks; ++i) {
			if (block_wh[i].high == 1) draw_block(block_wh[i].x, block_wh[i].y);
			if (block_wh[i].high == 2) draw_highblock(block_wh[i].x, block_wh[i].y);
		}

		if (warn_time_remain) {
			--warn_time_remain;
			putimage_rotatezoom(nullptr, pimg_warn, 55, 140, 0.5f, 0.5f, 0, 1, 1, alpha, 1);
			putimage_rotatezoom(nullptr, pimg_warn, 55, 380, 0.5f, 0.5f, 0, 1, 1, alpha, 1);
			alpha += dif;
			if (alpha == 255 || alpha == -5) alpha += (dif = -dif);
		}
		putimage_rotatezoom(nullptr, pimg_gold, 715, 140, 0.5f, 0.5f, 0, 1, 1, 0xff, 1);
		putimage_rotatezoom(nullptr, pimg_gold, 715, 380, 0.5f, 0.5f, 0, 1, 1, 0xff, 1);
		putimage_rotatezoom(nullptr, pimg_gpa, 50, 50, 0.5f, 0.5f, 0, 1, 1, 0xff, 1);
		while (mousemsg()) msg = getmouse();
		setfont(27, 0, "΢���ź�");
		setcolor(GREEN); 
		xyprintf(75, 38, "%.2lf", gpa / 100.0);
		setcolor(BLACK);
#ifdef DEBUG
		xyprintf(0, 0, "x = %3d  y = %3d", msg.x, msg.y);
		for (int i = 0; i < nBlocks; ++i) {
			xyprintf(block_wh[i].x - 11, block_wh[i].y - 11, "%d", i);
		}
#endif
		if (waiting_for_mouse_up && msg.is_up()) waiting_for_mouse_up = false;
		if (!waiting_for_mouse_up && msg.is_left() && msg.is_down()) {
			if (!selecting) {
				for (int i = 0; i < pcnt; ++i) p[i].selected = false;324
				for (int i = 0; i < pcnt; ++i)
					if (msg.x >= p[i].x - 33 && msg.x <= p[i].x + 33 && msg.y >= p[i].y - 33 && msg.y <= p[i].y + 33) {
						// xyprintf(300, 0, "Pressed %s", p[i].name.c_str());
						if (p[i].in_war || money >= p[i].value * 100) {
							if (!p[i].in_war) money -= p[i].value * 100;
							selecting = p + i;
							p[i].selected = 1;
							p[i].x = p[i].y = 0;
							p[i].in_war = 1;
						} else {
							money_bling = 60;
						}
						waiting_for_mouse_up = true;
					}
			} else {
				for (int i = 0; i < nBlocks; ++i)
					if (block_wh[i].high == 1 && msg.x >= block_wh[i].x - 40 && msg.y >= block_wh[i].y - 40 && msg.x <= block_wh[i].x + 40 && msg.y <= block_wh[i].y + 40) {
						selecting->in_war = 2;
						selecting->selected = 0;
						selecting->x = block_wh[i].x;
						selecting->y = block_wh[i].y;
						selecting = NULL;
						waiting_for_mouse_up = true;
					}
			}
		}
		putimage_rotatezoom(nullptr, pimg_money, 240, 40, 0.5f, 0.5f, 0, 1, 1, 0xff, 1);
		money += 1;
		setfont(25, 0, "Consolas");
		setcolor(BLACK);
		if (money_bling) {
			if (money_bling / 6 % 2) setcolor(BLACK);
			else setcolor(RED);
			--money_bling;
		}
		xyprintf(290, 22, "%d", money / 100);
		setfont(15, 0, "Consolas");
		setcolor(BLACK);
		xyprintf(335, 30, "0.6/s");
		for (int i = 0; i < 4; ++i) {
			setcolor(GREEN);
			line(285 - i, 50 + i * 2, 285 - i + money % 100, 50 + i * 2);
			line(285 - i, 50 + i * 2 + 1, 285 - i + money % 100, 50 + i * 2 + 1);
			setcolor(LIGHTGREEN);
			line(285 - i + money % 100 + 1, 50 + i * 2, 385 - i, 50 + i * 2);
			line(285 - i + money % 100 + 1, 50 + i * 2 + 1, 385 - i, 50 + i * 2 + 1);
		}
		++timx;
		for (int i = 0; i < pcnt; ++i) p[i].show();
		bool has_liveMonster = false;
		for (int i = 0; i < min(timx / 110 - 3, ecnt); ++i) {
			if (enemy[i].hp_now <= 0) continue;
			if (enemy[i].move()) {
				enemy[i].hp_now = 0;
				gpa -= enemy[i].attack;
				if (gpa < 0) break;
			}
			enemy[i].show();
			has_liveMonster = true;
			for (int j = 0; j < pcnt; ++j) {
				if (p[j].in_war == 2 && p[j].x == enemy[i].x && p[j].y == enemy[i].y) {
					p[j].hp_now -= enemy[i].hp_now;
					enemy[i].hp_now = -1;
					if (p[j].hp_now < 0) {
						p[j].in_war = 0;
						p[j].x = p[j].init_x;
						p[j].y = p[j].init_y;
						p[j].hp_now = p[j].hp_tot;
					}
				}
			}
		}
		for (int i = 0; i < pcnt; ++i) {
			if (p[i].in_war == 2 && p[i].hp_now > 0 && has_liveMonster && timx % p[i].fth == 0) {
				int tgt = p[i].find_target();
				if (tgt == -1) continue;
				ft[bcnt].x = p[i].x;
				ft[bcnt].y = p[i].y;
				ft[bcnt].attack = p[i].attack;
				ft[bcnt].speed = p[i].speed;
				ft[bcnt].target = tgt;
				++bcnt;
			}
		}
		for (int i = 0; i < bcnt; ++i) {
			ft[i].move();
			ft[i].show();
		}
		if (gpa < 0 || (!has_liveMonster && timx / 110 - 3 > ecnt)) break;
	}
	// ��������
	delay_ms(500);
	getimage_pngfile(pe_lef, "img\\endpage_1.png");
	getimage_pngfile(pe_rig, "img\\endpage_2.png");
	for (int i = 2; i <= 401; delay_fps(60), i += 3) {
		putimage_rotatezoom(nullptr, pimg_background, W / 2, H / 2, 0.5f, 0.5f, 0, 1, 1, 0xff, 1);
		putimage_rotatezoom(nullptr, pe_lef, i - 200, 300, 0.5f, 0.5f, 0, 1, 1, 0xff, 1);
		putimage_rotatezoom(nullptr, pe_rig, 1000 - i, 300, 0.5f, 0.5f, 0, 1, 1, 0xff, 1);
	}
	delay_ms(1000);
	setfont(36, 0, "��������");
	setcolor(BLACK);
	xyprintf(575, 540, "ս����ɨ�С���");
	delay_ms(3800);
	for (int i = 401; i >= 2; delay_fps(60), i -= 3) {
		putimage_rotatezoom(nullptr, pimg_background, W / 2, H / 2, 0.5f, 0.5f, 0, 1, 1, 0xff, 1);
		putimage_rotatezoom(nullptr, pe_lef, i - 200, 300, 0.5f, 0.5f, 0, 1, 1, 0xff, 1);
		putimage_rotatezoom(nullptr, pe_rig, 1000 - i, 300, 0.5f, 0.5f, 0, 1, 1, 0xff, 1);
	}
	delay_ms(1000);
	if (gpa < 0) {
		setfont(80, 0, "΢���ź�");
		setcolor(BLACK);
		xyprintf(170, 260, "�㱻��ѧ�ˣ�");
		delay_ms(3000);
		return 0;
	} else {
		setfont(60, 0, "΢���ź�");
		setcolor(BLACK);
		xyprintf(63, 260, "���� %d.%02d �ļ����ҵ�ˣ�", gpa / 100, gpa % 100);
		delay_ms(3000);
		return 0;
	}
	closegraph();
	return 0;
}
