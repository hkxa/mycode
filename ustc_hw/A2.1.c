#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#define m_Pi 3.14159265358979323846

double xA, yA, xB, yB, a1, a2;

int main() {
    // printf("请输入 A 的坐标，用空格分隔：");
    scanf("%lf%lf", &xA, &yA);
    // printf("请输入 B 的坐标，用空格分隔：");
    scanf("%lf%lf", &xB, &yB);
    // printf("请输入 ∠A ，用角度制：");
    scanf("%lf", &a1);
    // printf("请输入 ∠B ，用角度制：");
    scanf("%lf", &a2);
    (a1 <= 0 || a2 <= 0 || a1 + a2 >= 180) ? (printf("error"), exit(1)) : 0;
    a1 *= m_Pi / 180;
    a2 *= m_Pi / 180;
    double a_AB = atan2(yB - yA, xB - xA);
    double k1 = tan(a1 + a_AB), k2 = tan(a_AB - a2);
    // printf("k = %.2lf, %.2lf\n", k1, k2);
    double x = (yB - yA + k1 * xA - k2 * xB) / (k1 - k2);
    double y = k1 * (x - xA) + yA;
    // printf("点 C(%.2lf, %.2lf)\n", x, y);
    printf("%.2lf %.2lf\n", x, y);
    return 0;
}