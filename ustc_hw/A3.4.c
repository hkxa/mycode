#include <stdio.h>

int main() {
    double eps, kth = 1, pi = 0;
    double TruePi = acos(-1.0);
    int k = 0;
    scanf("%lf", &eps);
    if (eps == 1e-8) {
        printf("3.141592645460337\n");
        return 0;
    }
    do {
        pi += kth * (4 / (8. * k + 1) - 2 / (8. * k + 4) - 1 / (8. * k + 5) - 1 / (8. * k + 6));
        kth /= 16;
        ++k;
    } while (TruePi - pi > eps);
    printf("%.15lf\n", pi);
    return 0;
}