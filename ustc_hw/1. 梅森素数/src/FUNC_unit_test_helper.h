#include "MODULE_bigint.h"
#define width 15
#define assert(e) _assert(e, #e, __LINE__, __FILE__)
#define assert_expr(e, expr) _assert(e, #expr, __LINE__, __FILE__)
#define assert_eq(a, b) assert_expr(npcmp(a, b) == 0, a == b)

int min(int a, int b) {
    if (a < b)
        return a;
    return b;
}

int max(int a, int b) {
    if (a > b)
        return a;
    return b;
}

void _assert(int condition, const char *s, int line, const char *file) {
    if (!condition) {
        fprintf(stderr, "\nAssertion failed: In file %s, line [%d], %s\n", file, line, s);
        exit(0);
    }
}

unsigned random_bits(int bits) {
    srand(rand());
    return (rand() ^ rand()) % (1u << bits);
}

unsigned long long random_bits_int64(int bits) {
    assert(bits <= 64 && bits >= 1);
    unsigned long long value = (unsigned long long)random_bits(7)  << 57 |
                               (unsigned long long)random_bits(14) << 43 | 
                               (unsigned long long)random_bits(11) << 32 |
                               (unsigned long long)random_bits(5)  << 27 | 
                               (unsigned long long)random_bits(4)  << 23 | 
                               (unsigned long long)random_bits(8)  << 15 | 
                               (unsigned long long)random_bits(15);
    if (bits == 64) return value;
    return value & ((1ull << bits) - 1);
}

bint random_bint(int bits) {
    bint res;
    res.len = (bits + width - 1) / width;
    for (int i = 0; i < res.len - 1; ++i) res.a[i] = random_bits(width);
    res.a[res.len - 1] = random_bits((bits - 1) % 15) + (1 << ((bits - 1) % 15));
    return res;
}