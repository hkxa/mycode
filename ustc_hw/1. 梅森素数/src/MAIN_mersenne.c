#include "FUNC_check_prime.h"
#include "FUNC_unit_test.h"

int main() {
    srand(time(NULL));
    char s[1000];
    while (1) {
        printf("======= Selecting mode =======\n"
               "0. check mersenne prime with 1 <= n <= 1000 (with test about whether n is prime)\n"
               "1. check a mersenne prime\n"
               "2. unit test (basic): basical functions\n"
               "3. unit test (correct): correctness of test-prime and calc 2^n-1\n"
               "4. unit test (limit): let n approach 1000 (speed test) (no test about whether n is prime)\n"
               "5. unit test all\n"
               "-1. exit\n"
               "==============================\n"
               "> ");
        gets(s);
        int n;
        bint* N = (bint *)malloc(sizeof(bint));
        switch (atoi(s)) {
            case 0: 
                {
                    clock_t st = clock(), mxt = 0;
                    int countp = 0, count2p1 = 0;
                    for (int i = 1; i <= 1000; ++i) {
                        printf("===> [Test] check mersenne prime (1 ~ 1000): Testing... %6d/1000\r", i);
                        clock_t ist = clock();
                        if (int_isprime(i)) {
                            bint b;
                            ++countp;
                            if (check_mersenne(i, &b)) {
                                ++count2p1;
                                for (int i = 0; i < 80; ++i) printf(" ");
                                printf("\r> mersenne-prime %2d: Found 2^%d-1 (=", count2p1, i);
                                bint result = BaseToDec(&b, 1 << width);
                                put(stdout, &result);
                                printf(")\n");
                            }
                        }
                        mxt = max(mxt, clock() - ist);
                    }
                    double totalTime = (double)(clock() - st) / CLOCKS_PER_SEC;
                    double averageTime = totalTime / 1000;
                    double maxTime = (double)mxt / CLOCKS_PER_SEC;
                    printf("===> [Test] There's %d prime under 1000, %d prime in {2 ^ p - 1 | 1 <= p <= 1000}\n", countp, count2p1);
                    printf("===> [Test] check mersenne prime (1 ~ 1000): Total %.3lfs (Average: %.3lfs; Max: %.3lfs)\n", totalTime, averageTime, maxTime);
                }
                break;
            case 1:
                printf("> Input n for mersenne number 2^n-1: ");
                scanf("%d", &n);
                int isPrime = check_mersenne(n, N);
                if (isPrime)
                    printf("> Result: 2^%d-1 is prime\n", n);
                else
                    printf("> Result: 2^%d-1 isn't prime\n", n);
                printf("> 2^%d-1 = ", n);
                *N = BaseToDec(N, 1 << width);
                put(stdout, N);
                while (getchar() != '\n');
                break;
            case 2:
                unit_test_basic();
                break;
            case 3:
                unit_test_correct();
                break;
            case 4:
                unit_test_limit();
                break;
            case 5:
                unit_test_basic();
                unit_test_correct();
                unit_test_limit();
                break;
            case -1:
                printf("==============================\n"
                       "===== See you next time! =====\n"
                       "==============================\n");
                return 0;
        }
    }
    return 0;
}

/*
p    2^p-1
2    3
3    7
5    31
7    127
13   8191
17   131071
19   524287
31   2147483647
61   2305843009213693951
89   618970019642690137449562111
107  162259276829213363391578010288127
127  170141183460469231731687303715884105727
521  6864797660130609714981900799081393217269435300143305409394463459185543183397656052122559640661454554977296311391480858037121987999716643812574028291115057151
607  531137992816767098689588206552468627329593117727031923199444138200403559860852242739162502265229285668889329486246501015346579337652707239409519978766587351943831270835393219031728127
*/