#include "FUNC_check_prime.h"
#include "FUNC_unit_test_helper.h"

void unit_test_basic() {
    printf("====== unit test: basic ======\n");
    for (int i = 1; i <= 2880; ++i) {
        printf("===> [Test 1] Plus & Minus: Testing... %6d/2880\r", i);
        bint a = random_bint(i), b = random_bint(i), c = random_bint(i);
        assert_eq(add1(&a, &b, 1 << width), add1(&b, &a, 1 << width));
        bint ab = add1(&a, &b, 1 << width), bc = add1(&b, &c, 1 << width);
        assert_eq(a, minus1(&ab, &b, 1 << width));
        assert_eq(add1(&ab, &c, 1 << width), add1(&a, &bc, 1 << width));
    }
    printf("\n");
    for (int i = 1; i <= 960; ++i) {
        printf("===> [Test 2] Mul: Testing... %6d/960\r", i);
        bint a = random_bint(i), b = random_bint(i), c = random_bint(i);
        assert_eq(mul1(&a, &b, 1 << width), mul1(&b, &a, 1 << width));
        bint ab = mul1(&a, &b, 1 << width), bc = mul1(&b, &c, 1 << width);
        assert_eq(mul1(&ab, &c, 1 << width), mul1(&a, &bc, 1 << width));
    }
    printf("\n");
    for (int i = 1; i <= 2880; ++i) {
        printf("===> [Test 3] div2: Testing... %6d/2880\r", i);
        bint a = random_bint(i), a2 = div2(&a, 1 << width);
        bint adda = add1(&a2, &a2, 1 << width);
        assert_eq(a, add1(&adda, a.a[0] & 1 ? &bint_const1 : &bint_const0, 1 << width));
    }
    printf("\n");
    for (int i = 1; i <= 1440; ++i) {
        printf("===> [Test 4] Mod: Testing... %6d/1440\r", i);
        bint a = random_bint(i * 2), b = random_bint(i);
        assert(npcmp(Mod(&a, &b, 1 << width), b) < 0);
    }
    printf("\n");
}

void unit_test_correct() {
    printf("===== unit test: correct =====\n");
    for (int i = 1; i <= 32767; ++i) {
        printf("===> [Test 1] test-prime (small): Testing... %6d/32767\r", i);
        if (int_isprime(i)) assert(CheckPrime((bint){1, {i}}));
    }
    printf("\n");
    for (int i = 1; i <= 32767; ++i) {
        long long value = random_bits_int64(40);
        printf("===> [Test 2] test-prime (random-int64): Testing... %6d/32767\r", i);
        if (value < (1 << 15)) assert(int_isprime(value) == CheckPrime((bint){1, {value}}));
        else if (value < (1 << 30)) assert(int_isprime(value) == CheckPrime((bint){2, {value & 32767, value >> 15}}));
        else assert(int_isprime(value) == CheckPrime((bint){3, {value & 32767, (value >> 15) & 32767, value >> 30}}));
    }
    printf("\n");
    for (int i = 1; i <= 64; ++i) {
        printf("===> [Test 3] calc 2^n-1: Testing... %6d/64\r", i);
        bint b;
        gen_mersenne(i, &b);
        if (i <= 15) assert_eq(b, ((bint){1, {(1 << i) - 1}}));
        else if (i <= 30) assert_eq(b, ((bint){2, {32767, (1 << (i - 15)) - 1}}));
        else if (i <= 45) assert_eq(b, ((bint){3, {32767, 32767, (1 << (i - 30)) - 1}}));
        else if (i <= 60) assert_eq(b, ((bint){4, {32767, 32767, 32767, (1 << (i - 45)) - 1}}));
        else assert_eq(b, ((bint){5, {32767, 32767, 32767, 32767, (1 << (i - 60)) - 1}}));
    }
    printf("\n");
    int okNumber[] = {2, 3, 5, 7, 13, 17, 19, 31, 61, 89, 107, 127, 521, 607};
    for (int i = 1; i <= 14; ++i) {
        printf("===> [Test 4] mersenne prime (n under 1000) IsPrime = true: Testing... %6d/14\r", i);
        assert(check_mersenne(okNumber[i - 1], NULL));
    }
    printf("\n");
    // for (int i = 1, num = 1, id = 0; i <= 986; ++i) {
    //     printf("===> [Test 5] mersenne prime (n under 1000) IsPrime = false: Testing... %6d/986\r", i);
    //     assert(!check_mersenne(num, NULL));
    //     while (1) {
    //         ++num;
    //         if (id < 14 && num == okNumber[id]) ++id;
    //         else break;
    //     }
    // }
    // printf("\n");
}

void unit_test_limit() {
    printf("====== unit test: limit ======\n");
    clock_t st = clock(), mxt = 0;
    for (int i = 1; i <= 1000; ++i) {
        printf("===> [Test 1] check mersenne prime (1 ~ 1000): Testing... %6d/1000\r", i);
        clock_t ist = clock();
        check_mersenne(i, NULL);
        mxt = max(mxt, clock() - ist);
    }
    double totalTime = (double)(clock() - st) / CLOCKS_PER_SEC;
    double averageTime = totalTime / 1000;
    double maxTime = (double)mxt / CLOCKS_PER_SEC;
    printf("===> [Test 1] check mersenne prime (1 ~ 1000): Total %.3lfs (Average: %.3lfs; Max: %.3lfs)\n", totalTime, averageTime, maxTime);
}