#ifndef header_CHECK_PRIME
#define header_CHECK_PRIME
#include "MODULE_bigint.h"
#define width 15

int v[2000];

int CheckPrime(bint N) {
    int len;
    srand(time(0));
    len = N.len * width;
    int ii = 0, jj = 1 << (width - 1);
    while (N.a[N.len - 1] & jj == 0) {
        ii++;
        jj >>= 1;
    }
    len -= ii;

    if (N.len == 1 && N.a[0] == 2) return 1;
    if (N.len == 1 && N.a[0] == 1 || N.a[0] & 1 == 0) return 0;
    // put(stdout, &N);
    for (int i = 0; i < 10; ++i)
        if (cmp(&N, pr + i) == 0)
            return 1;
    bint n = N;
    n.a[0] ^= 1;

    int f = 0;
    int k = 0;
    for (int i = 0;; ++i)
        if (n.a[i]) {
            k = i * width;
            break;
        }
    int l = 0, l1 = 1;
    while (n.a[k] & l1 == 0) {
        l1 <<= 1;
        l++;
    }
    k += l;
    int cnt = 0;
    bint m = bint_const0;
    for (int i = k; i < len; ++i) {
        m.a[cnt / width] = (n.a[i / width] & (1 << (i % width)) == 1) << (cnt % width);
        cnt++;
    }
    m.len = (len - k + width - 1) / width;
    for (int j = 0; j < 10; ++j) {
        bint xx = power2b(pr + j, &n, &N, 1 << width);
        if (cmp(&xx, &bint_const1)) return 0;
        bint t = power2b(pr + j, &m, &N, 1 << width);
        bint last = t;
        for (int i = 0; i <= k; ++i)v[i] = 0;
        if (!cmp(&t, &bint_const1))v[0] = 1;
        if (!cmp(&t, &n))v[0] = -1;
        for (int i = 1; i <= k; ++i) {
            t = mul1(&t, &t, 1 << width);

            t = Mod(&t, &N, 1 << width);

            if (!cmp(&t, &bint_const1))v[i] = 1;
            if (!cmp(&t, &n))v[i] = -1;
            if (v[i] == 1 && !v[i - 1]) return 0;
            last = t;
        }

        if (cmp(&t, &bint_const1)) return 0;
    }
    return 1;
}

int fp_CheckPrime(FILE *fp) {
    bint N;
    int len;
    srand(time(0));
    read10(fp, &N, 15);
    return CheckPrime(N);
}

void gen_mersenne(int n, bint *N) {
    *N = bint_const1;
    for (int u = 1; u <= n; ++u) *N = mul1(N, &bint_const2, 1 << width);
    *N = minus1(N, &bint_const1, 1 << width);
    // printf("%d\n", N->len);
}

int check_mersenne(int n, bint *N) {
    if (N == NULL) N = (bint *)malloc(sizeof(bint));
    gen_mersenne(n, N);
    return CheckPrime(*N);
}

int int_isprime(long long n) {
    if (n <= 1) return 0;
    for (long long i = 2; i * i <= n; ++i)
        if (n % i == 0) return 0;
    return 1;
}
#endif