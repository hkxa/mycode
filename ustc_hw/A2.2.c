#include <stdio.h>

int main() {
    int year;
    scanf("%d", &year);
    printf("%c", "NY"[year % 4 == 0 && (year % 100 != 0 || year % 400 == 0)]);
    return 0;
}