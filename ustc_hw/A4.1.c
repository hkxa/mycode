#include <stdio.h>

int main() {
    long long n;
    scanf("%lld", &n);
    printf("%lld^3=%lld=", n, n * n * n);
    for (int i = 0; i < n; ++i) {
        if (i) putchar('+');
        printf("%lld", n * n - (n - 1) + 2 * i);
    }
    return 0;
}