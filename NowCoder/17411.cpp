//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      room.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-14.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 988244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

struct Edge {
    int u, v, w;
    int cost;
    int next;
    Edge() {}
    Edge(int _u, int _v, int _w, int _cost, int _next) : u(_u), v(_v), w(_w), cost(_cost), next(_next) {}
};

int n, m, s, t;
Edge edges[200007]; 
int head[10007], top = 0;
int could[10007];
int road[10007];

#define add(u, v, w, cost) edges[top] = Edge(u, v, w, cost, head[u]); head[u] = top++;

int dis[10007];
bool SPFA()
{
	queue <int> q;
	bool vis[10007] = {0};
	memset(dis, 0x3f, sizeof(dis));
	dis[s] = 0;
	vis[s] = 1;
	q.push(s);
	while (!q.empty()) {
		int now = q.front();
		q.pop();
		vis[now] = 0;
		for (int nNow = head[now]; nNow != -1; nNow = edges[nNow].next) {
			Edge &e = edges[nNow];
			if (e.w > 0 && dis[e.v] > dis[now] + e.cost) {
				dis[e.v] = dis[now] + e.cost;
				road[e.v] = nNow;
				if (!vis[e.v]) {
					q.push(e.v);
					vis[e.v] = 1;
				}
			}
		}
	}
	if (dis[t] != 0x3f3f3f3f) return true; // success
	else return false; // unsuccess
}

void Edmonds_Karp()
{
    int maxflow = 0, mincost = 0;
    queue <int> q;
    while (SPFA()) {
		int minn = 0x3f3f3f3f;
        for (int pNow = t; pNow != s; pNow = edges[road[pNow]].u) {
            if (edges[road[pNow]].w < minn) minn = edges[road[pNow]].w;
        }
        for (int pNow = t; pNow != s; pNow = edges[road[pNow]].u) {
            edges[road[pNow]].w -= minn;
            edges[road[pNow] ^ 1].w += minn;
        }
        maxflow += minn;
        mincost += dis[t] * minn;
    }
    printf("%d\n", mincost);
}

signed main()
{
    memset(head, -1, sizeof(head));
    n = read<int>();
    s = 0;
    t = 2 * n + 1;
    int x[4], y[4];
    int live[407];
    for (int i = 1; i <= n; i++) {
        scanf("%d%d%d%d", x, x + 1, x + 2, x + 3);
        live[x[0]] = i;
        live[x[1]] = i;
        live[x[2]] = i;
        live[x[3]] = i;
    }
    for (int i = 1, cost; i <= n; i++) {
        scanf("%d%d%d%d", y, y + 1, y + 2, y + 3);
        add(0, i, 1, 0);
        add(i, 0, 0, 0);
        for (int j = 1; j <= n; j++) {
            cost = 0;
            if (live[y[0]] != j) cost++;
            if (live[y[1]] != j) cost++;
            if (live[y[2]] != j) cost++;
            if (live[y[3]] != j) cost++;
            add(j, i + n, 1, cost);
            add(i + n, j, 0, -cost);
        }
        add(i + n, 2 * n + 1, 1, 0);
        add(2 * n + 1, i + n, 0, 0);
    }
    Edmonds_Karp();
    return 0;
}

// Create File Date : 2020-06-14