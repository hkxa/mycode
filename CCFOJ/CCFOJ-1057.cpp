#include <iostream>
#include <stdio.h>
#include <cmath>
#include <cstdlib>
using namespace std;
int r, c, n;
int temp2;
int rsp1[102][102], rsp2[102][102];

void rsp(int i, int j, int i2, int j2)
{
	temp2 = (rsp1[i2][j2]-rsp1[i][j]+3)%3;
	if (i2 == 0 || i2 == r+1 || j2 == 0 || j2 == c+1)
	{
		return;
	}
	else
	{
		switch (temp2)
		{
			case 1 : rsp2[i2][j2] = rsp1[i][j]; break;
			case 2 : rsp2[i][j] = rsp1[i2][j2]; break;
		}
	}
}

int main()
{
	cin >> r >> c >> n;
	
	char temp1;
	for (int i = 1; i <= r; i++)
	{
		for (int j = 1; j <= c; j++)
		{
			cin >> temp1;
			switch (temp1)
			{
				case 'R' : rsp2[i][j] = rsp1[i][j] = 0; break;
				case 'S' : rsp2[i][j] = rsp1[i][j] = 1; break;
				case 'P' : rsp2[i][j] = rsp1[i][j] = 2; break;
			}
		}
	}
	
	for (int i = 1; i <= n; i++)
	{
		for (int j = 1; j <= r; j++)
		{
			for (int k = 1; k <= c; k++)
			{
				rsp(j, k, j-1, k);
				rsp(j, k, j+1, k);
				rsp(j, k, j, k-1);
				rsp(j, k, j, k+1);
			}
		}
		for (int j = 1; j <= r; j++)
		{
			for (int k = 1; k <= c; k++)
			{
				rsp1[j][k] = rsp2[j][k];
			}
		}
	}
	
	for (int i = 1; i <= r; i++)
	{
		for (int j = 1; j <= c; j++)
		{
			switch (rsp1[i][j])
			{
				case 0 : cout << 'R'; break;
				case 1 : cout << 'S'; break;
				case 2 : cout << 'P'; break;
			}
		}
		cout << endl;
	}
	return 0;
}
