#include <iostream>
#include <stdio.h>
#include <cmath>
#include <cstdlib>
using namespace std;

int feb(int fe1, int fe2, int fe3, int num)
{
	if (num == 1)
		return fe3;
	else
		return feb(fe2, fe3, fe2+fe3, num-1);
}

int main()
{
	int num;
	cin >> num;
	if (num == 1)
	{
		printf("%d", 0);
	}
	else
	{
		printf("%d", feb(0, 0, 1, num-1));
	}
	return 0;
}
