#include <cstdio>
#include <iostream>
using namespace std;

int main()
{
	int value[100][100];
	int m;
	cin >> m;
	int tot = 0;
	int k = 0, l = 0;
	while (tot < m*m)
	{
		for (int i = l, j = k; i <= k; ) value[i++][j--] = ++tot;
		if (k < m-1) k++;
		else l++;
		for (int i = k, j = l; i >= l; ) value[i--][j++] = ++tot;
		if (k < m-1) k++;
		else l++;
	}
	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < m; j++)
		{
			cout << value[i][j] << ' ';
		}
		cout << endl;
	}
	return 0;
}
