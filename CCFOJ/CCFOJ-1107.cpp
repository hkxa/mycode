#include <stdio.h>

int main()
{
	int n;
	scanf("%d", &n);
	char left[20001][20];
	char right[20001][20];
	int ltop = 0, rtop = 0;
	for (int i = 0, t; i < n; i++) {
		scanf("%d", &t);
		if (t) {
			scanf(" %s", right[rtop]);
			rtop++;
		} 
		else {
			scanf(" %s", left[ltop]);
			ltop++;
		}
	}
	for (ltop--; ltop >= 0; ltop--) 
		printf("%s\n", left[ltop]);
	for (int i = 0; i < rtop; i++)
		printf("%s\n", right[i]);
	return 0;
	/*
	10
0 LZZ
0 HSY
0 TSW
1 LHS
1 WKA
0 LWJ
1 HT
0 ZZB
1 DYL
0 ZJX
	*/
} 
