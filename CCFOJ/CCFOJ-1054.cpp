#include <iostream>
#include <cmath>
#include <cstdlib>
using namespace std;

int updated[102][102];

int clean(int x, int y)
{
	for (int i = x-1; i <= x+1; i++)
		for (int j = y-1; j <= y+1; j++)
			if ( updated[i][j] != -1)
				updated[i][j]++;		
}

int main()
{
	int n, m;
	cin >> n >> m;
	
	char raw[102][102];
	
	for (int i = 1; i <= n; i++)
	{
		for (int j = 1; j <= m; j++)
		{
			cin >> raw[i][j];
		}
	}
	
	for (int i = 1; i <= n; i++)
	{
		for (int j = 1; j <= m; j++)
		{
			switch (raw[i][j])
			{
				case '*' : clean(i, j); updated[i][j] = -1; break;
				case '?' : break;
			}
		}
	}
	
	for (int i = 1; i <= n; i++)
	{
		for (int j = 1; j <= m; j++)
		{
			if (updated[i][j] == -1)
				cout << '*';
			else
				cout << updated[i][j];
		}
		cout << endl;
	}
	
	return 0;
}
