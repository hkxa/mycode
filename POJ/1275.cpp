/*************************************
 * @problem:      1275 Cashier Employment.
 * @user_name:    HKAX0933.
 * @time:         2020-03-06.
 * @language:     C++.
 * @upload_place: Poj.
*************************************/ 

// C++ includes used for precompiling -*- C++ -*-

// Copyright (C) 2003-2013 Free Software Foundation, Inc.
//
// This file is part of the GNU ISO C++ Library.  This library is free
// software; you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the
// Free Software Foundation; either version 3, or (at your option)
// any later version.

// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// Under Section 7 of GPL version 3, you are granted additional
// permissions described in the GCC Runtime Library Exception, version
// 3.1, as published by the Free Software Foundation.

// You should have received a copy of the GNU General Public License and
// a copy of the GCC Runtime Library Exception along with this program;
// see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see
// <http://www.gnu.org/licenses/>.

/** @file stdc++.h
 *  This is an implementation file for a precompiled header.
 */

// 17.4.1.2 Headers

// C
#ifndef _GLIBCXX_NO_ASSERT
#include <cassert>
#endif
#include <cctype>
#include <cerrno>
#include <cfloat>
#include <ciso646>
#include <climits>
#include <clocale>
#include <cmath>
#include <csetjmp>
#include <csignal>
#include <cstdarg>
#include <cstddef>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>

#if __cplusplus >= 201103L
#include <ccomplex>
#include <cfenv>
#include <cinttypes>
#include <cstdalign>
#include <cstdbool>
#include <cstdint>
#include <ctgmath>
#include <cwchar>
#include <cwctype>
#endif

// C++
#include <algorithm>
#include <bitset>
#include <complex>
#include <deque>
#include <exception>
#include <fstream>
#include <functional>
#include <iomanip>
#include <ios>
#include <iosfwd>
#include <iostream>
#include <istream>
#include <iterator>
#include <limits>
#include <list>
#include <locale>
#include <map>
#include <memory>
#include <new>
#include <numeric>
#include <ostream>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <stdexcept>
#include <streambuf>
#include <string>
#include <typeinfo>
#include <utility>
#include <valarray>
#include <vector>

#if __cplusplus >= 201103L
#include <array>
#include <atomic>
#include <chrono>
#include <condition_variable>
#include <forward_list>
#include <future>
#include <initializer_list>
#include <mutex>
#include <random>
#include <ratio>
#include <regex>
#include <scoped_allocator>
#include <system_error>
#include <thread>
#include <tuple>
#include <typeindex>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>
#endif

using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define int int64

int T = -1;
int n;
int need[24 + 3];
int start[24 + 3];

struct Edge {
    int to, v;
};
#define make_edge(u, v) ((Edge){(u), (v)})
vector<Edge> G[24 + 3];
int S[24 + 3];
bool inq[24 + 3];
int vis[24 + 3];
queue<int> q;

bool check(int S_24)
{
    // printf("---------------------------------------- (S_24 = %d)\n", S_24);
    /**
     * S[i] - S[i - 1] >= 0 (1 <= i <= 24)
     * S[i - 1] - S[i] >= -start[i - 1] (1 <= i <= 24)
     * S[i] - S[i - 8] >= need[i - 1] (8 <= i <= 24)
     * S[i] - S[i + 16] >= need[i - 1] - S_24; (1 <= i <= 7)
     */
    for (int i = 0; i <= 24; i++) G[i].clear();
    for (int i = 1; i <= 24; i++) G[i - 1].push_back(make_edge(i, 0));
    for (int i = 1; i <= 24; i++) G[i].push_back(make_edge(i - 1, -start[i - 1]));
    for (int i = 8; i <= 24; i++) G[i - 8].push_back(make_edge(i, need[i - 1]));
    for (int i = 1; i <= 7; i++) G[i + 16].push_back(make_edge(i, need[i - 1] - S_24));
    G[0].push_back(make_edge(24, S_24));
    G[24].push_back(make_edge(0, -S_24));
    while (!q.empty()) q.pop(); // empty the queue
    q.push(0);
    memset(vis, 0, sizeof(vis));
    memset(inq, 0, sizeof(inq));
    memset(S, 0xcf, sizeof(S));
    S[0] = 0;
    inq[0] = 0;
    while (!q.empty() && S[24] <= S_24) {
        int fr = q.front(); 
        // printf("Time *%d : S[%d] = %d.\n", fr, fr, S[fr]);
        q.pop();
        inq[fr] = 0;
        for (unsigned i = 0; i < G[fr].size(); i++) {
            if (S[G[fr][i].to] < S[fr] + G[fr][i].v) {
                S[G[fr][i].to] = S[fr] + G[fr][i].v;
                if (!inq[G[fr][i].to]) {
                    vis[G[fr][i].to]++;
                    // if (vis[G[fr][i].to] > 25) {
                    //     // return n + 1;
                    //     return 0;
                    // }
                    inq[G[fr][i].to] = true;
                    q.push(G[fr][i].to);
                }
            }
        }
    }
    // printf("When S_24 = %d, S = { ", S_24);
    // for (int i = 0; i < 24; i++) {
    //     printf("%d", S[i]);
    //     if (i < 24) putchar(',');
    // }
    // printf(" }\n");
    // if (S[24] >= S_24) return S[24];
    // else return n + 1;
    return S[24] <= S_24;
}

signed main()
{
    if (T == -1) T = read<int>();
    // Start Program
    memset(start, 0, sizeof(start));
    for (int i = 0; i < 24; i++) need[i] = read<int>();
    n = read<int>();
    for (int i = 0; i < n; i++) start[read<int>()]++;
    int l = 0, r = n, ans = n + 1, mid;
    while (l <= r) {
        mid = (l + r) >> 1;
        if (check(mid)) {
            ans = mid;
            r = mid - 1;
        } else l = mid + 1;
    }
    if (ans == n + 1) printf("No Solution\n");
    else write(ans, 10);
    // End Program
    T--;
    if (T) return main();
    else return 0;
}

/*
4
1 0 1 0 0 0 1 1 0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 1
5
0
24
22
1
10
1 0 1 0 0 0 1 1 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 1
5
0
23
22
1
10
1 0 1 0 0 0 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1
5
0
23
22
1
10
1 0 1 0 0 0 2 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 2
5
0
23
22
1
10
*/