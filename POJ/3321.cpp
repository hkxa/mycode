//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Apple Tree.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-11.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <vector>
#include <stdio.h>
#include <iostream>
#include <algorithm>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}

// #define int int64
using namespace Fastio;

int n, m;
// vector<int> G[500007];
vector<vector<int> > G(500007);
bool apple[500007];
int t[500007];
int L[500007], R[500007], dft;

#define lowbit(x) ((x) & -(x))

int query(int x) {
    int ret = 0;
    while (x) {
        ret += t[x];
        x -= lowbit(x);
    } 
    return ret;
}

void add(int x, int dif) {
    while (x <= n) {
        t[x] += dif;
        x += lowbit(x);
    } 
}

#define sum(l, r) (query(r) - query((l) - 1))

void dfs(int u, int fa) {
    L[u] = ++dft;
    for (size_t i = 0; i < G[u].size(); i++)
        if (G[u][i] != fa) dfs(G[u][i], u);
    R[u] = dft;
}

char getOper() {
    static char buffer[15];
    scanf("%s", buffer);
    return buffer[0];
}

signed main() {
    read >> n;
    for (int i = 1, u, v; i < n; i++) {
        read >> u >> v;
        G[u].push_back(v);
        G[v].push_back(u);
    }
    dfs(1, 0);
    read >> m;
    for (int i = 1, x; i <= m; i++) {
        if (getOper() == 'Q') {
            // read >> x;
            scanf("%d", &x);
            write << n - sum(L[x], R[x]) << '\n';
        } else {
            // read >> x;
            scanf("%d", &x);
            if (apple[x]) add(x, -1);
            else add(x, 1);
            apple[x] = !apple[x];
        }
    }
    return 0;
}