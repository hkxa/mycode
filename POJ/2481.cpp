//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      Cows.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-11.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <algorithm>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}

// #define int int64
using namespace Fastio;

int n;
int t[100007];
int ans[100007];

struct cow {
    int l, r, id;
    bool operator == (const cow &b) const {
        return l == b.l && r == b.r;
    }
    bool operator < (const cow &b) const {
        return r ^ b.r ? r > b.r : l < b.l;
    }
} a[100007];

#define lowbit(x) ((x) & -(x))

int query(int x) {
    int ret = 0;
    while (x) {
        ret += t[x];
        x -= lowbit(x);
    } 
    return ret;
}

void add(int x, int dif) {
    while (x <= 100003) {
        t[x] += dif;
        x += lowbit(x);
    } 
}

signed main()
{
    read >> n;
    again:;
    for (int i = 1; i <= n; i++) {
        read >> a[i].l >> a[i].r;
        a[i].l++; a[i].r++;
        a[i].id = i;
    }
    sort(a + 1, a + n + 1);
    for (int i = 1, j; i <= n; i = j + 1) {
        j = i;
        while (j < n && a[i] == a[j + 1]) j++;
        for (int k = i; k <= j; k++)
            ans[a[k].id] = query(a[k].l);
        add(a[i].l, j - i + 1);
    }
    for (int i = 1; i <= n; i++) write << ans[i] << " \n"[i == n];
    read >> n;
    if (n) {
        memset(t, 0, sizeof(t));
        goto again;
    }
    return 0;
}