#pragma GCC optimize("O1")
#pragma GCC optimize("O2")
#pragma GCC optimize("O3")
//优化优化 
#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <algorithm>
#include <vector>
#include <map>
#include <set>
//#include <etc...>
#define Char_Int(a) ((a) & 15)
#define Int_Char(a) ((a) + '0')
#define print_end(...) { printf(__VA_ARGS__); return 0; }
//#define DEBUG
using namespace std;

#ifdef DEBUG
# define Debug(...) printf(__VA_ARGS__)
#else
# define Debug(...) 
#endif
 
template <typename _TpInt> inline _TpInt read();
template <typename _TpRealnumber> inline double readr();
template <typename _TpInt> inline void write(_TpInt x);
 

struct COW{
	int start, finish;
	inline bool operator <(const COW &a) const {
		return (start == a.start) ? (finish > a.finish) : (start < a.start);
	}
};

int main()
{
	COW *a = new COW[1000001];
	int n(read<int>()), t(read<int>()), ans(1);
	for (int i = 0; i < n; i++)
		a[i].start = read<int>(), a[i].finish = read<int>();
	sort(a, a + n);
	if (a[0].start < 0) print_end("-1");
	int step = a[0].finish;
	int i(1);
	while (step < t && i < n) {
		if (a[i].start > step + 1) print_end("-1");
		int mx(a[i].finish), fp(i), p(i);
		while (a[p].start <= step + 1 && p < n) {
			if (mx < a[p].finish) {
				mx = a[p].finish;
				fp = p;
			}
			p++;
		}
		i = fp;
		step = a[i].finish;
		ans++;
	}
	/*
1 7
3 10
3 6
6 10
	*/
	write<int>(ans);
} 

template <typename _TpInt>
inline _TpInt read()       
{
    register int flag = 1;
    register char c = getchar();
    while ((c > '9' || c < '0') && c != '-') 
        c = getchar();
    if (c == '-') flag = -1, c = getchar();
    register _TpInt init = Char_Int(c);
    while ((c = getchar()) <= '9' && c >= '0') 
        init = (init << 3) + (init << 1) + Char_Int(c);
    return init * flag;
}

template <typename _TpRealnumber>
inline double readr()       
{
    register int flag = 1;
    register char c = getchar();
    while ((c > '9' || c < '0') && c != '-') 
        c = getchar();
    if (c == '-') flag = -1, c = getchar();
    register _TpRealnumber init = Char_Int(c);
    while ((c = getchar()) <= '9' && c >= '0') 
        init = init * 10 + Char_Int(c);
    if (c != '.') return init * flag;
    register _TpRealnumber l = 0.1;
    while ((c = getchar()) <= '9' && c >= '0') 
        init = init + Char_Int(c) * l, l *= 0.1;
    return init * flag;
}

template <typename _TpInt>
inline void write(_TpInt x)
{
    if (x < 0) {
    	putchar('-');
        write<_TpInt>(~x + 1);
    }
    else {
        if (x > 9) write<_TpInt>(x / 10);	
        putchar(Int_Char(x % 10));
    }
}
