#pragma GCC optimize(2)
#pragma GCC optimize(3)
#pragma GCC optimize("Ofast")
#pragma GCC optimize("inline")
#pragma GCC optimize("-fgcse")
#pragma GCC optimize("-fgcse-lm")
#pragma GCC optimize("-fipa-sra")
#pragma GCC optimize("-ftree-pre")
#pragma GCC optimize("-ftree-vrp")
#pragma GCC optimize("-fpeephole2")
#pragma GCC optimize("-ffast-math")
#pragma GCC optimize("-fsched-spec")
#pragma GCC optimize("unroll-loops")
#pragma GCC optimize("-falign-jumps")
#pragma GCC optimize("-falign-loops")
#pragma GCC optimize("-falign-labels")
#pragma GCC optimize("-fdevirtualize")
#pragma GCC optimize("-fcaller-saves")
#pragma GCC optimize("-fcrossjumping")
#pragma GCC optimize("-fthread-jumps")
#pragma GCC optimize("-funroll-loops")
#pragma GCC optimize("-fwhole-program")
#pragma GCC optimize("-freorder-blocks")
#pragma GCC optimize("-fschedule-insns")
#pragma GCC optimize("inline-functions")
#pragma GCC optimize("-ftree-tail-merge")
#pragma GCC optimize("-fschedule-insns2")
#pragma GCC optimize("-fstrict-aliasing")
#pragma GCC optimize("-fstrict-overflow")
#pragma GCC optimize("-falign-functions")
#pragma GCC optimize("-fcse-skip-blocks")
#pragma GCC optimize("-fcse-follow-jumps")
#pragma GCC optimize("-fsched-interblock")
#pragma GCC optimize("-fpartial-inlining")
#pragma GCC optimize("no-stack-protector")
#pragma GCC optimize("-freorder-functions")
#pragma GCC optimize("-findirect-inlining")
#pragma GCC optimize("-fhoist-adjacent-loads")
#pragma GCC optimize("-frerun-cse-after-loop")
#pragma GCC optimize("inline-small-functions")
#pragma GCC optimize("-finline-small-functions")
#pragma GCC optimize("-ftree-switch-conversion")
#pragma GCC optimize("-foptimize-sibling-calls")
#pragma GCC optimize("-fexpensive-optimizations")
#pragma GCC optimize("-funsafe-loop-optimizations")
#pragma GCC optimize("inline-functions-called-once")
#pragma GCC optimize("-fdelete-null-pointer-checks")
#include<iostream>
#include<algorithm>
#include<memory.h>
#include<stdio.h>
using namespace std;
#define MAX 100
int use[MAX], a[MAX], value, n;
// n为金子有几堆，sum为所有金子重量和，value为所要求的最少单堆金子重
int sum;

bool cmp(int a, int b)
{
	return a > b;
}
// 自定义比较函数，使数组a从大到小排列

const bool used = 1;
const bool unused = 0;

bool dfs(int i, int left, int total) // i:试到了第几堆;
                                   // left:还要多少能组成这堆金子;
                                   // total:组成的总重量。
{ 
	if (left == 0)
	{
		total -= value;//寻找新金子
		if (total == value)
			return true; //如果只剩下value重量的没组合，那么一定是能组合这个重量的
		if (total < value)
			return false; //如果剩下的重量小于原先金子的重量，那么一定组不成了，可以返回失败了
		for (i = 0; i < n; i++)
			if(use[i] == unused)
				break;
			use[i] = used; // 找到第一堆没有用过的金子，置为用过，去做深搜。
			if (dfs(i + 1, value - a[i], total))
				return true; //成功
			use[i] = unused; //不成功，重新置为0
			total += value; // 回溯 
			return false;
	}	
	else {
		for (int j = i; j < n; j++)
		{
			if (a[j] > left || use[j] == used)
				continue; // 如果用过或者大于剩余重量，略过
			if ((a[j] == a[j - 1]) && use[j - 1] == unused)
				continue; // 如果前一堆和后一堆相等，且前一堆没有用，
                  // 那么表示这个重量之前就失败了，这次可以不要去深搜了.
		  use[j] = used;					
			if (dfs(j + 1, left - a[j], total))
				return true; // 成功 
			use[j] = unused;				
		}
		return false;
	}
}

int FindAnswer(int a[], int n)
{
	int k = 0; // k是判断是否找到了小于sum的因子
  for (value = a[0]; value <= sum / 2; value++) {
		if (sum % value != 0)
			continue;
    k = dfs(0, value, sum);
    if(k != 0)
      return value;
	}
  return sum;
}

 
int main()
{   
	while((~scanf("%d", &n)) && n != 0)
	{
		memset(use, 0, sizeof(use));
		sum = 0;
		for (int i = 0; i < n; i++)
		{
			scanf("%d", &a[i]);
			sum += a[i];
		}
		sort(a, a + n, cmp);
		printf("%d\n", FindAnswer(a, n));
	}
	return 0;
}
