//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      The k-th Largest Group.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-11.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <stack>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <algorithm>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}

// #define int int64
using namespace Fastio;

int n, m;
int t[200007];
int fa[200007];

#define lowbit(x) ((x) & -(x))

int query(int x) {
    int ret = 0;
    while (x) {
        ret += t[x];
        x -= lowbit(x);
    } 
    return ret;
}

void add(int x, int dif) {
    while (x <= n) {
        t[x] += dif;
        x += lowbit(x);
    } 
}

int find(int x) {
    return fa[x] < 0 ? x : fa[x] = find(fa[x]);
}

void unify(int u, int v) {
    u = find(u); v = find(v);
    if (u == v) return;
    if (fa[u] > fa[v]) swap(u, v);
    add(n + fa[u] + 1, -1);
    add(n + fa[v] + 1, -1);
    fa[u] += fa[v];
    add(n + fa[u] + 1, 1);
    fa[v] = u;
}

int find_kth(int kth) {
    int l = 1, r = n, mid, ans = 1;
    while (l <= r) {
        mid = (l + r) >> 1;
        if (kth <= query(mid)) r = mid - 1, ans = mid;
        else l = mid + 1;
    }
    return ans;
}

signed main()
{
    read >> n >> m;
    memset(fa, -1, sizeof(fa));
    add(n, n);
    for (int i = 1, u, v; i <= m; i++) {
        if (!read.get_int<int>()) {
            read >> u >> v;
            unify(u, v);
        } else {
            read >> u;
            write << n - find_kth(u) + 1 << '\n';
        }
    }
    return 0;
}