/*************************************
 * problem:      UVA11362 Phone List.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-06-17.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <algorithm>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

#define TrieNodeMax 100007 
template <int charSetSize = 26, char base = 'a'>
struct Trie {
    bool isEndStr[TrieNodeMax];
    int son[TrieNodeMax][charSetSize], top;

    void clear()
    {
        top = 1;
        memset(son, 0, sizeof(son));
        memset(isEndStr, 0, sizeof(isEndStr));
    }

    Trie() 
    {
        clear();
    }

    bool insert(string s)
    {
        int u = 1;
        for (int i = 0; i < s.length(); i++) {
            if (!son[u][s[i] - base]) {
                son[u][s[i] - base] = ++top;
            } else if (isEndStr[son[u][s[i] - base]] || i == s.length() - 1) return 0;
            u = son[u][s[i] - base];
            if (i == s.length() - 1) isEndStr[u] = 1;
        }
        return 1;
    }
};

Trie<10, '0'> t;

void play()
{
    t.clear();
    int n = read<int>();
    string s;
    for (int i = 1; i <= n; i++) {
        cin >> s;
        if (!t.insert(s)) {
            puts("NO");
            while (i < n) {
                cin >> s;
                i++;
            }
            return;
        }
    }
    puts("YES");
    return;
}

int main()
{
    ios::sync_with_stdio();
    int cases = read<int>();
    while (cases--) play();
}