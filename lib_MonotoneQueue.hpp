#include <bits/stdc++.h>

template <typename T, typename CompType = std::less<T>, size_t Qsize = 500007>
struct MonotoneQueue {
    typedef T         value_type;
    typedef int       size_type;
    typedef T*        iterator;
    typedef const T*  const_iterator;
    typedef T&        reference;
    typedef const T&  const_reference;
    CompType Comp;
    size_type head, tail;
    size_type qId[Qsize];
    value_type qVal[Qsize];
    void init() {
        head = 1;
        tail = 0;
    }
    void refresh(size_type LefBound) {
        while (head <= tail && qId[head] < LefBound) ++head;
    }
    void push(value_type val, size_type pos) {
        while (head <= tail && !Comp(qVal[tail], val)) --tail;
        qVal[++tail] = val;
        qId[tail] = pos;
    }
    bool empty() const { return head > tail; }
    size_type size() const { return tail - head + 1; }
    const_reference front() const { return qVal[head]; }
    const_iterator begin() const { return qVal + head; }
    const_iterator end() const { return qVal + tail + 1; }
    const_reference operator [] (size_type pos) const { return *(begin() + pos); }
};