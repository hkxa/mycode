/*
	Problem: P2678 跳石头
	Author: 航空信奥 
	Date: 2018/07/29
*/
#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <vector>
#include <map>
#define Char_Int(a) ((a) & 15)
#define Int_Char(a) ((a) + '0')
#define rg register

namespace hkxa {
	template <typename _TpInt> inline _TpInt read();
	template <typename _TpInt> inline void write(_TpInt x);
	template <typename _TpSwap> inline void swap(_TpSwap &x, _TpSwap &y);
	
#	define SizeN 50007

	int L, n, m;
	int stone[SizeN];
	
	bool judge(int mid)
	{
		int last = 0, count = 0;
		for (int i = 0; i <= n; i++) {
			if (stone[i] - last < mid) count++;
			else last = stone[i];
		}
		return count <= m;
	}
	
	int main()
	{
		/*
25 5 2 
2
11
14
17 
21
ANSWER#:  4
		*/
//		freopen("0_test"".in", "r", stdin);
//		freopen("0_test"".out", "out", stdout);
	    L = read<int>();
	    n = read<int>();
	    m = read<int>();
	    for (int i = 0; i < n; i++) {
	    	stone[i] = read<int>();
		}
		stone[n] = L;
	    int l = 0, r = L, mid = L >> 1;
	    while (l < r) {
	    	if (judge(mid)) l = mid;
	    	else r = mid - 1;
	    	mid = (l + r + 1) >> 1;
		}
		write<int>(mid);
	    return 0;
	} 
	
	template <typename _TpInt>
	inline _TpInt read()       
	{
	    register _TpInt flag = 1;
	    register char c = getchar();
	    while ((c > '9' || c < '0') && c != '-') 
			c = getchar();
	    if (c == '-') flag = -1, c = getchar();
	    register _TpInt init = Char_Int(c);
	    while ((c = getchar()) <= '9' && c >= '0') 
			init = (init << 3) + (init << 1) + Char_Int(c);
	    return init * flag;
	}
	
	template <typename _TpInt>
	inline void write(_TpInt x)
	{
	    if (x < 0) {
	    	putchar('-');
			write<_TpInt>(~x + 1);
		}
		else {
			if (x > 9) write<_TpInt>(x / 10);	
			putchar(Int_Char(x % 10));
		}
	}
	
	template <typename _TpSwap>
	inline void swap(_TpSwap &x, _TpSwap &y)
	{
	    _TpSwap t = x;
	    x = y;
	    y = t;
	}
}

int main()
{
    hkxa::main();
    return 0;
}