/*************************************
 * @problem:      聪聪和可可.
 * @user_name:    brealid/hkxadpall/zhaoyi20/j(o)moo/littleTortoise.
 * @time:         2020-08-29.
 * @language:     C++.
 * @fastio_ver:   20200827.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        // simple io flags
        ignore_int = 1 << 0,    // input
        char_enter = 1 << 1,    // output
        flush_stdout = 1 << 2,  // output
        flush_stderr = 1 << 3,  // output
        // combination io flags
        endline = char_enter | flush_stdout // output
    };
    enum number_type_flags {
        // simple io flags
        output_double_stable = 1 << 0,  // output
        output_double_faster = 1 << 1   // output
        // combination io flags
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f & ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore character set : ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (double &lf) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            lf = endch & 15;
            while (isdigit(endch = getchar())) lf = lf * 10 + (endch & 15);
            if (endch == '.') {
                double digit = 0.1;
                while (isdigit(endch = getchar())) {
                    lf += (endch & 15) * digit;
                    digit *= 0.1;
                }
            }
            if (flag) lf = -lf;
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            Int d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return d;
        }
    } read;

    struct Writer {
        Writer& operator << (io_flags f) {
            if (f & char_enter) putchar(10);
            if (f & flush_stdout) fflush(stdout);
            if (f & flush_stderr) fflush(stderr);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
        inline void operator () (double val, int eps_digit = 6, char endch = '\0', number_type_flags flg = output_double_faster) {
            if (flg & output_double_stable) {
                static char output_format[10];
                sprintf(output_format, "%%.%dlf", eps_digit);
                printf(output_format, val);
            } else if (flg & output_double_faster) {
                if (val < 0) {
                    putchar('-');
                    val = -val;
                }
                double eps_number = 0.5;
                for (int i = 1; i <= eps_digit; i++) eps_number /= 10;
                val += eps_number;
                (*this) << (int64)val;
                val -= (int64)val;
                putchar('.');
                while (eps_digit--) {
                    val *= 10;
                    putchar((int)val | '0');
                    val -= (int)val;
                }
            } else {
                (*this) << "1.#ERROR_NOT_IDENTIFIED_FLAG";
            }
            if (endch) putchar(endch);
        }
    } write;
}
using namespace Fastio;
namespace File_IO {
    void init_IO() {
        freopen("聪聪和可可.in", "r", stdin);
        freopen("聪聪和可可.out", "w", stdout);
    }
}

// #define int int64

const int maxn = 1005;

int n, m, st, ed, u, v, deg[maxn];
int head[maxn], ver[maxn << 1], nxt[maxn << 1], tot;
double f[maxn][maxn];
int g[maxn][maxn], secg[maxn][maxn];
int dis[maxn][maxn];
int q[maxn], q_head, q_tail;

double dfs(int st, int ed) {
    if (f[st][ed] != -1) return f[st][ed];
    if (st == ed) return f[st][ed] = 0;
    if (secg[st][ed] == ed) return f[st][ed] = 1;
    f[st][ed] = dfs(secg[st][ed], ed);
    for (int p = head[ed]; p; p = nxt[p])
        f[st][ed] += dfs(secg[st][ed], ver[p]);
    f[st][ed] /= (deg[ed] + 1);
    return ++f[st][ed];
}

signed main() {
    // File_IO::init_IO();
    read >> n >> m >> st >> ed;
    for (int i = 1; i <= m; ++i) {
        read >> u >> v;
        ver[++tot] = v; nxt[tot] = head[u]; head[u] = tot; ++deg[u];
        ver[++tot] = u; nxt[tot] = head[v]; head[v] = tot; ++deg[v];
    }
    
    memset(dis, -1, sizeof(dis));
    for (int i = 1; i <= n; ++i) {
        dis[i][i] = 0;
        q_head = q_tail = 0;
        q[0] = i;
        while (q_head <= q_tail) {
            u = q[q_head++];
            for (int p = head[u]; p; p = nxt[p]) {
                v = ver[p];
                if (dis[i][v] == -1) {
                    dis[i][v] = dis[i][u] + 1;
                    q[++q_tail] = v;
                }
            }
        }
    }
    
    memset(g, 0x3f, sizeof(g));
    for (int i = 1; i <= n; ++i)
        for (int p = head[i]; p; p = nxt[p])
            for (int j = 1; j <= n; ++j)
                if (dis[i][j] == dis[ver[p]][j] + 1 && ver[p] < g[i][j])
                    g[i][j] = ver[p];
    for (int i = 1; i <= n; ++i)
        for (int j = 1; j <= n; ++j)
            if (i == j) continue;
            else if (g[i][j] == j) secg[i][j] = j;
            else secg[i][j] = g[g[i][j]][j];
    for (int i = 1; i <= n; ++i)
        for (int j = 1; j <= n; ++j)
            f[i][j] = -1; 
    write(dfs(st, ed), 3, '\n');
    return 0;
}