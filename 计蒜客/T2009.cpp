/**
 * Problem: P1081 开车旅行.  
 * Author:  航空信奥.
 * Date:    2018-09-06. 
 * Upload:  Luogu. 
 */
#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <string.h>
#include <vector>
#include <queue> 
#include <map>
#include <set>
using namespace std;

namespace AuthorName { 
    template <typename _TpInt> inline _TpInt read();
    template <typename _TpRealnumber> inline double readr();
    template <typename _TpInt> inline void write(_TpInt x);

#   define Max_N 100007 // = Max_M
#   define LogN 18 // = log(Max_N)
#   define Max_LogN_SizeArray 20 // > Max_LogN

    int n;
    long long h[Max_N] = {0};
    struct Sort_h{
        long long h_num;
        int Id;
        bool operator < (const Sort_h &o) const 
        {
            return h_num < o.h_num;
        }
    } sh[Max_N] = {0};
    long long min1[Max_N] = {0}, min2[Max_N] = {0};
    int x0, m;
    int s[Max_N] = {0};
    long long x[Max_N] = {0};
    long long fA[Max_N][Max_LogN_SizeArray] = {0}, fB[Max_N][Max_LogN_SizeArray] = {0};
    int To_Point[Max_N][Max_LogN_SizeArray] = {0};

    bool min_cmp(int &jzo, int &p1, int &p2) 
    {
        if (p1 < 1) return 0;
        if (p2 > n) return 1;
        return (h[jzo] - h[p1]) <= (h[p2] - h[jzo]);
    }
    
    long long abs(long long _x)
    {
    	return _x < 0 ? (~_x + 1) : _x;
    }

    // int min_num(int jzo, int p1, int p2) 
    // {
    //     if (h[p1] == 0x3f3f3f3f) return h[p2];
    //     if (h[p2] == 0x3f3f3f3f) return h[p1];
    //     return min((h[jzo] - h[p1]), (h[p2] - h[jzo]));
    // }

    void make_f()
    {
        // What do you say? The "f" is a dp array? No! :-(
        // It's a Multiplier array! :-)
        // Look! This function can make array "f"! 
        for (int i = 1; i <= n; i++) {
            if (To_Point[i][0] == n + 1) To_Point[i][0] = 0;
        }
        for (int k = 1; k <= LogN; k++) {
            for (int i = 1; i <= n; i++) {
                fA[i][k] = fA[i][k - 1] + fA[To_Point[i][k - 1]][k - 1];
                fB[i][k] = fB[i][k - 1] + fB[To_Point[i][k - 1]][k - 1];
                To_Point[i][k] = To_Point[To_Point[i][k - 1]][k - 1];
            }
        }
        // for (int k = 0; k <= LogN; k++) {
        //     for (int i = 1; i <= n; i++) {
        //         printf("from %d move 2^%d is %d, fA = %d, fB = %d.\n", i, k, To_Point[i][k], fA[i][k], fB[i][k]);
        //     }
        // }
    }

    void get_ans(long long &aSave, long long &bSave, int from, long long h)
    {
//      printf("from = %d, h = %d\n", from, h);
        aSave = bSave = 0;
        for (int i = LogN; ~i; i--) {
//          printf("from [%d, %d] ", aSave, bSave); 
            if (To_Point[from][i] && aSave + bSave + fA[from][i] + fB[from][i] <= h) {
                aSave += fA[from][i];
                bSave += fB[from][i];
                from = To_Point[from][i];
            }
//          printf("to [%d, %d] \n", aSave, bSave); 
        }
        if (min2[from] != 0/* && min2[from] != n + 1 */&& aSave + bSave + fA[from][0] <= h) {
            aSave += fA[from][0];
        }
        /* TODO (ZY#4#): check isOK */
        return;
    }

    int main() 
    {
//        memset(h, 0x3f, sizeof(h));
        n = read<int>();
        for (int i = 1; i <= n; i++) {
            h[i] = read<long long>();
        }
        x0 = read<int>();
        m = read<int>();
        for (int i = 1; i <= m; i++) {
            s[i] = read<int>();
            x[i] = read<long long>();
        }

        h[0] = -2000000101;
        h[n + 1] = 2000000101;
        multiset<Sort_h> ms;
        ms.insert((Sort_h){-2000000101, 0});
        ms.insert((Sort_h){-2000000101, 0});
        ms.insert((Sort_h){2000000101, n + 1});
        ms.insert((Sort_h){2000000101, n + 1});

        int Left_Id, Right_Id, This_Id, llid, rrid;
        multiset<Sort_h>::iterator it;
        Sort_h taa;
        for (int i = n; i; i--) {
            taa.h_num = h[i];
            taa.Id = i;
            ms.insert(taa);
            it = ms.lower_bound(taa); 
//          for (multiset<Sort_h>:: iterator iit = ms.begin(); iit != ms.end(); iit++)
//              printf("[%d %d] ", iit->h_num, iit->Id);
//          printf("\n");
            // printf("test_log : [%d] %d pointTo %lld\n", i, it->Id, it->h_num);

            This_Id = it -> Id;
            it--;
            Left_Id = it -> Id;
            it++;
            it++;
            Right_Id = it -> Id;
            it--;
            if (min_cmp(This_Id, Left_Id, Right_Id)) {
                min1[This_Id] = Left_Id;
//                To_Point[This_Id][0] = Left_Id;
//                fB[This_Id][0] = h[This_Id] - h[Left_Id];
                it--;
                it--;
                llid = it->Id;
                if (min_cmp(This_Id, llid, Right_Id)) {
                    /* DONE (ZY#9#): check is "sh[i - 2].Id" : "Left_Id - 1" */
                    min2[This_Id] = llid; 
//                  To_Point[This_Id][0] = llid;
//                    fA[This_Id][0] = h[This_Id] - h[llid];
                } else {
                    min2[This_Id] = Right_Id;
//                  To_Point[This_Id][0] = Right_Id;
//                    fA[This_Id][0] = h[Right_Id] - h[This_Id];
                }
            } else {
                min1[This_Id] = Right_Id;
//                To_Point[This_Id][0] = Right_Id;
//                fB[This_Id][0] = h[Right_Id] - h[Left_Id];
                it++;
                it++;
                rrid = it->Id;
                if (min_cmp(This_Id, Left_Id, rrid)) {
                    min2[This_Id] = Left_Id;
//                  To_Point[This_Id][0] = Left_Id;
//                    fA[This_Id][0] = h[This_Id] - h[Left_Id];
                } else {
                    min2[This_Id] = rrid;
//                  To_Point[This_Id][0] = rrid;
//                    fA[This_Id][0] = h[rrid] - h[This_Id];
                }
            }
            // printf("prepare: min1 = %lld, min2 = %lld\n-----------------------------------------\n", min1[This_Id], min2[This_Id]);
        }
        for (int i = 1; i <= n; i++) {
            To_Point[i][0] = min1[min2[i]];
            fA[i][0] = abs(h[min2[i]] - h[i]);
            fB[i][0] = abs(h[min1[min2[i]]] - h[min2[i]]);
        }
        make_f();
        long long ansa, ansb;
        double minn = 2147483647;
        int s0 = 0;
        for(int i = 1; i <= n; i++){
            get_ans(ansa, ansb, i, x0);
            if (ansb && (1.0 * ansa / ansb < minn)){
                minn = 1.0 * ansa / ansb;
                s0 = i;
            }
        }
        printf("%d\n", s0);
        for (int i = 1; i <= m; i++) {
            get_ans(ansa, ansb, s[i], x[i]);
            printf("%lld %lld\n", ansa, ansb);
        }

        return 0;
    }

#   define Getchar() getchar()

    template <typename _TpInt>
    inline _TpInt read()       
    {
        register int flag = 1;
        register char c = Getchar();
        while ((c > '9' || c < '0') && c != '-') 
            c = Getchar();
        if (c == '-') flag = -1, c = Getchar();
        register _TpInt init = (c & 15);
        while ((c = Getchar()) <= '9' && c >= '0') 
            init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename _TpRealnumber>
    inline double readr()       
    {
        register int flag = 1;
        register char c = Getchar();
        while ((c > '9' || c < '0') && c != '-') 
            c = Getchar();
        if (c == '-') flag = -1, c = Getchar();
        register _TpRealnumber init = (c & 15);
        while ((c = Getchar()) <= '9' && c >= '0') 
            init = init * 10 + (c & 15);
        if (c != '.') return init * flag;
        register _TpRealnumber l = 0.1;
        while ((c = Getchar()) <= '9' && c >= '0') 
            init = init + (c & 15) * l, l *= 0.1;
        return init * flag;
    }

    template <typename _TpInt>
    inline void write(_TpInt x)
    {
        if (x < 0) {
            putchar('-');
            write<_TpInt>(~x + 1);
        }
        else {
            if (x > 9) write<_TpInt>(x / 10);   
            putchar(x % 10 + '0');
        }
    }
}

int main()
{
    AuthorName::main();
    return 0;
} 