#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;

const int MAXN = 5e4 + 5, MAXM = MAXN << 1;
const int INF = 0x7fffffff;

int head[MAXM], nxt[MAXM], v[MAXM], w[MAXM], cnt;

int n, m;
int root;

inline void Addline(int x, int y, int z)
{
	v[cnt] = y, w[cnt] = z;
	nxt[cnt] = head[x], head[x] = cnt++;

	return;
}

int dp[MAXN],
    tag[MAXN];
int que[MAXN], tail;
int res;

inline void DFS(int x, int from, int lim)
{
	for (int i = head[x]; ~i; i = nxt[i])
		if (v[i] != from)
			DFS(v[i], x,
			    lim);
	tail = 0;
	for (int i = head[x]; ~i; i = nxt[i])
		if (v[i] != from) {
			que[++tail] = dp[v[i]] + w[i];
		}
	sort(que + 1, que + tail + 1);
	for (int i = tail; i >= 1 && que[i] >= lim; i--) {
		tail--, res--;
	}
	for (int i = 1; i <= tail; i++)
		if (tag[i] != x) {
			int l = i + 1, r = tail,
			    pst = tail + 1;
			while (l <= r) {
				int mid = ((l + r) >> 1);
				if (que[i] + que[mid] >= lim) {
					pst = mid, r = mid - 1;
				} else {
					l = mid + 1;
				}
			}

			while (tag[pst] == x
			          && pst <= tail) {
				pst++;
			}

			if (pst <=
			          tail) {
				tag[i] = tag[pst] = x, res--;
			}
		}

	dp[x] = 0;
	for (int i = tail; i >= 1;
	          i--)
		if (tag[i] != x) {
			dp[x] = que[i];
			break;
		}

	return;
}

int main()
{
	memset(head, -1, sizeof head);

	cin >> n >> m;
	for (int i = 1, x, y, z; i < n; i++) {
		scanf("%d %d %d", &x, &y, &z);
		Addline(x, y, z), Addline(y, x, z);
	}

	root = rand() % n + 1;

	int l = 0, r = INF, ans = 0;
	while (l <= r) {
		int mid = ((l + r) >> 1);
		res = m;

		memset(tag, false, sizeof tag);

		DFS(root, 0, mid);
		if (res <= 0) {
			ans = mid, l = mid + 1;
		} else {
			r = mid - 1;
		}
	}

	cout << ans << endl;

	return 0;
}