struct interval {
    int l, r;
    interval() : l(0), r(0) {}
    interval(int L, int R) : l(L), r(R) {}
    inline int mid() const { return (l + r) >> 1; }
    inline interval get_L() const { return interval(l, mid()); }
    inline interval get_R() const { return interval(mid() + 1, r); }
    inline int size() const { return r - l + 1; }
    inline bool exist() const { return l <= r; }
    inline bool isInclude(int pos) const { return pos >= l && pos <= r; }
    inline bool isInclude(interval s) const { return s.l >= l && s.r <= r; }
    inline bool noUnion(interval s) const { return s.r < l || s.l > r; }
    inline bool only_point(int pos) const { return pos == l && pos == r; }
};

struct square {
    interval x, y;
    square() : x(), y() {}
    square(interval L, interval R) : x(L), y(R) {}
    square(int xl, int xr, int yl, int yr) : x(xl, xr), y(yl, yr) {}
    inline square get_L() const { return square(x, y.get_L()); }
    inline square get_R() const { return square(x, y.get_R()); }
    inline square get_U() const { return square(x.get_L(), y); }
    inline square get_D() const { return square(x.get_R(), y); }
    inline square get_LU() const { return square(x.get_L(), y.get_L()); }
    inline square get_RU() const { return square(x.get_L(), y.get_R()); }
    inline square get_LD() const { return square(x.get_R(), y.get_L()); }
    inline square get_RD() const { return square(x.get_R(), y.get_R()); }
    inline bool exist() const { return x.exist() && y.exist(); }
    inline bool isInclude(int X, int Y) const { return x.isInclude(X) && y.isInclude(Y); }
    inline bool isInclude(square s) const { return x.isInclude(s.x) && y.isInclude(s.y); }
    inline bool noUnion(square s) const { return x.noUnion(s.x) || y.noUnion(s.y); }
    inline bool only_point(int X, int Y) const { return x.only_point(X) && y.only_point(Y); }
};
