/*************************************
 * @problem:      tunnel.
 * @time:         2020-10-18.
 ************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef long long int64;
typedef unsigned long long uint64;

template <typename Int>
Int read() {
    Int d = 0;
    bool flag = 0;
    char ch = getchar();
    while ((ch < '0' || ch > '9') && ch != '-' && ch != EOF) ch = getchar();
    if (ch == '-') flag = 1, ch = getchar();
    d = ch & 15;
    while ((ch = getchar()) >= '0' && ch <= '9') d = (d << 3) + (d << 1) + (ch & 15);
    return flag ? -d : d;
}

template <typename Int>
void write(Int x) {
    static char buffer[33];
    static int top = 0;
    if (!x) {
        putchar('0');
        return;
    }
    if (x < 0) putchar('-'), x = -x;
    while (x) {
        buffer[++top] = '0' | (x % 10);
        x /= 10;
    }
    while (top) putchar(buffer[top--]);
}

const int N = 1e3 + 7;
int n, m, a[N][N];

namespace ST_I {
    int solve() {
        int mask = 1 << (n * m * 2);
        int direction[N][N];
        int ans = 0, now;
        for (int status = 0; status < mask; ++status) {
            for (int i = 1; i <= n; ++i)
                for (int j = 1; j <= m; ++j) {
                    int d = (status >> (((i - 1) * m + (j - 1)) * 2)) & 3;
                    if (d == 0) direction[i][j] = 3;        // 右下
                    else if (d == 1) direction[i][j] = 6;   // 左下
                    else if (d == 2) direction[i][j] = 12;  // 左上
                    else if (d == 3) direction[i][j] = 9;   // 右上
                }
            now = 0;
            for (int i = 1; i <= n; ++i)
                for (int j = 1; j < m; ++j)
                    if ((direction[i][j] & 1) && (direction[i][j + 1] & 4))
                        now += max(a[i][j] + a[i][j + 1], 0);
            for (int i = 1; i < n; ++i)
                for (int j = 1; j <= m; ++j)
                    if ((direction[i][j] & 2) && (direction[i + 1][j] & 8))
                        now += max(a[i][j] + a[i + 1][j], 0);
            ans = max(ans, now);
        }
        write(ans);
        putchar(10);
        return 0;
    }
}

namespace ST_II {
    int solve() {
        int f[N], g[N]; // f: 往左; g: 往右
        f[1] = g[1] = 0;
        for (int i = 2; i <= m; ++i) {
            f[i] = max(f[i - 1], g[i - 1] + max(a[1][i - 1] + a[1][i], 0));
            g[i] = max(f[i - 1], g[i - 1]);
            // printf("%d %d\n", f[i], g[i]);
        }
        write(max(f[m], g[m]));
        putchar(10);
        return 0;
    }
}

namespace ST_III {
    int solve() {
        int f[N][16];
        // f[u][state] : 当前格状态 state
        // state = a << 2 | b
        // a, b \in [0, 4)
        // 0[左上] 1[左下] 2[右上] 3[右下]
        memset(f[0], 0xcf, sizeof(f[0]));
        f[0][0] = f[0][1] = f[0][4] = f[0][5] = 0;
        for (int i = 1; i <= m; ++i) {
            for (int u = 0; u < 16; ++u) {
                f[i][u] = 0;
                for (int v = 0; v < 16; ++v) {
                    int tmp = 0;
                    if ((v >> 2) >= 2 && (u >> 2) <= 1) tmp += max(a[1][i - 1] + a[1][i], 0);
                    if ((v & 3) >= 2 && (u & 3) <= 1) tmp += max(a[2][i - 1] + a[2][i], 0);
                    if (((u >> 2) & 1) && !(u & 1)) tmp += max(a[1][i] + a[2][i], 0);
                    f[i][u] = max(f[i][u], f[i - 1][v] + tmp);
                }
            }
            // write(*max_element(f[i], f[i] + 16));
            // putchar(10);
        }
        write(*max_element(f[m], f[m] + 16));
        putchar(10);
        return 0;
    }
}

signed main() {
    n = read<int>();
    m = read<int>();
    for (int i = 1; i <= n; ++i)
        for (int j = 1; j <= m; ++j)
            a[i][j] = read<int>();
#ifdef Force_Test_Subtask
    return Force_Test_Subtask::solve();
#endif
    if (n <= 3 && m <= 3) return ST_I::solve();
    if (n == 1) return ST_II::solve();
    if (n == 2) return ST_III::solve();
    return 0;
}