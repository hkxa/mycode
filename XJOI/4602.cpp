#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef long long int64;
typedef unsigned long long uint64;

template <typename Int>
Int read() {
    Int d = 0;
    bool flag = 0;
    char ch = getchar();
    while ((ch < '0' || ch > '9') && ch != '-' && ch != EOF) ch = getchar();
    if (ch == '-') flag = 1, ch = getchar();
    d = ch & 15;
    while ((ch = getchar()) >= '0' && ch <= '9') d = (d << 3) + (d << 1) + (ch & 15);
    return flag ? -d : d;
}

template <typename Int>
void write(Int x) {
    static char buffer[33];
    static int top = 0;
    if (!x) {
        putchar('0');
        return;
    }
    if (x < 0) putchar('-'), x = -x;
    while (x) {
        buffer[++top] = '0' | (x % 10);
        x /= 10;
    }
    while (top) putchar(buffer[top--]);
}

const int N = 50 + 3, P = 1e9 + 7;

#define Cx2(x) ((int64)(x) * ((x) - 1) >> 1)

int n, m, w[N];

struct info {
    int64 siz, cnt;
    info(int64 S, int64 C) : siz(S), cnt(C) {}
    bool operator != (const info &b) const { return siz != b.siz || cnt != b.cnt; }
    bool operator < (const info &b) const { return siz ^ b.siz ? siz < b.siz : cnt < b.cnt; }
};

class parti : public vector<info> {
public:
    parti(info XX) { push_back(XX); }
    bool operator < (const parti &b) const {
        const parti &a = *this;
        if (a.size() ^ b.size()) return a.size() < b.size();
        for (size_t i = 0; i < size(); ++i)
            if (a[i] != b[i]) return a[i] < b[i];
        return false;
    }
    parti merge(size_t p1, size_t p2) const {
        parti a = *this;
        int64 sum = a[p1].siz + a[p2].siz;
        if (p1 != p2) {
            if (!--a[p2].cnt) a.erase(a.begin() + p2);
            if (!--a[p1].cnt) a.erase(a.begin() + p1);
        } else {
            a[p1].cnt -= 2;
            if (!a[p1].cnt) a.erase(a.begin() + p1);
        }
        iterator it = lower_bound(a.begin(), a.end(), info(sum, 1));
        if (it != a.end() && it->siz == sum) ++(it->cnt);
        else a.insert(it, info(sum, 1));
        return a;
    }
    int64 calc() const {
        int64 ret = 0;
        const parti &a = *this;
        for (size_t i = 0; i < size(); ++i)
            ret += Cx2(a[i].siz) * a[i].cnt;
        return ret % P;
    }
    int64 total_situation() const {
        int64 ret = 1;
        const parti &a = *this;
        for (size_t i = 0; i < size(); ++i)
            ret = ret * a[i].cnt % P;
        return ret;
    }
};

map<parti, int64> state[2];
typedef map<parti, int64>::iterator Dp_Iter;

void upd_sum(int64 &a, int64 b) { if ((a += b) >= P) a -= P; }
void upd_mul(int64 &a, int64 b) { a = a * b % P; }
int64 sub(int64 a, int64 b) { a -= b; return (a % P + P) % P; }

void dp_Merge(map<parti, int64> &f, map<parti, int64> &g) {
    for (Dp_Iter i = g.begin(); i != g.end(); ++i) {
        const parti &now = i->first;
        const int64 &now_val = i->second;
        for (size_t i = 0; i < now.size(); ++i) {
            if (now[i].cnt >= 2) upd_sum(f[now.merge(i, i)], now_val * (now[i].siz * now[i].siz * Cx2(now[i].cnt) % P) % P);
            for (size_t j = i + 1; j < now.size(); ++j)
                upd_sum(f[now.merge(i, j)], now_val * (now[i].siz * now[j].siz * now[i].cnt * now[j].cnt % P) % P);
        }
    }
}

void dp_NotMerge(map<parti, int64> &f, int EdgesNow) {
    for (Dp_Iter i = f.begin(); i != f.end(); ++i)
        upd_mul(i->second, sub(i->first.calc(), EdgesNow));
}

signed main() {
    n = read<int>();
    m = n * (n - 1) >> 1;
    for (int i = 1; i < n; ++i) w[i] = read<int>();
    sort(w + 1, w + n);
    state[0][parti(info(1, n))] = 1;
    for (int i = 1, j = 1; i <= m; ++i) {
        state[i & 1].clear();
        if (j < n && w[j] == i) {
            ++j;
            dp_Merge(state[i & 1], state[~i & 1]);
        } else {
            dp_NotMerge(state[i & 1] = state[~i & 1], i - 1);
        }
        state[~i & 1].clear();
    }
    int64 ans = 0;
    map<parti, int64> &result = state[m & 1];
    for (Dp_Iter it = result.begin(); it != result.end(); ++it)
        upd_sum(ans, it->first.total_situation() * it->second % P);
    write(ans);
    putchar(10);
    return 0;
}