#include <stdio.h>
#include <iostream>
using namespace std;
#define Char_Int(a) ((a) & 15)
#define MAX 100003

template <typename Int>
Int read() {
	char ch = getchar();
	while (!isdigit(ch)) ch = getchar();
	Int a = (ch & 15);
	ch = getchar();
	while (isdigit(ch)) {
		a = ((a + (a << 2)) << 1) + (ch ^ 48);
		ch = getchar();
	}
	return a;
}  
 
long long a[MAX], temp[MAX], ans = 0;
 
void gb(int l, int r)
{
    if (l == r) return;
    int mid = (l + r) >> 1;
    int i = l, j = mid + 1, k = l;
    gb(l, mid);
    gb(mid + 1, r);
    while (i <= mid && j <= r) {
        if (a[i] <= a[j])
            temp[k++] = a[i++];
        else {
            temp[k++] = a[j++];
            ans += mid - i + 1;
            // ans += j - mid;
        }
    }
    while (i <= mid)
        temp[k++] = a[i++];
    while (j <= r)
        temp[k++] = a[j++];
    for (int i = l; i <= r; i++)
        a[i] = temp[i];
}
 
int main()
{
    int n = read<int>();
    for (int i = 1; i <= n; i++)
        a[i] = read<int>();
    gb(1, n);
    printf("%lld", ans);
    return 0;
}