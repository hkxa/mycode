/*************************************
 * @problem:      生成树.
 * @user_name:    brealid.
 * @time:         2020-11-05.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

const int N = 200 + 7, M = 100000 + 7, P = 998244353;

int exgcd(int a, int b, int64 &x, int64 &y) { // ax + by = gcd(a, b)
    if (b == 0) {
        x = 1;
        y = 0;
        return a;
    }
    int res = exgcd(b, a % b, x, y);
    int64 t = y;
    y = x - a / b * y;
    x = t;
    return res;
}

int inv(int a, int p = P) {
    static int64 x, y;
    exgcd(a, p, x, y);
    return (x % p + p) % p;
}

int64 qpow(int64 a, int64 k) {
    int64 res = 1;
    while (k) {
        if (k & 1) (res *= a) %= P;
        (a *= a) %= P;
        k >>= 1;
    }
    return res;
}

int n, m;
int a[N], G[N][N], deg[N];
int64 f[N][N];

namespace HangLieShi {
    int64 resolve(int64 (*f)[N], int n) {
        int64 temp[N], ans = 1;
        for (int i = 0; i < n; i++) {
            temp[i] = (f[i][i] * (a[i] - 1) + deg[i]) % P;
            for (int j = i + 1; j < n; j++)
                temp[j] = f[i][j] * a[i] % P;
            if (!temp[i]) {
                int p;
                for (p = i + 1; p < n; p++)
                    if (f[p][i]) break;
                    else if (p == n - 1) return 0;
                for (int k = i; k < n; k++)
                    (temp[k] += f[p][k]) %= P;
            }
            int inv_aii = inv(temp[i]);
            (ans *= temp[i]) %= P;
            for (int j = i; j < n; j++)
                (temp[j] *= inv_aii) %= P;
            (ans *= qpow((deg[i] - f[i][i] + P) % P, a[i] - 1)) %= P;
            for (int j = i + 1; j < n; j++) {
                for (int k = i + 1; k < n; k++)
                    (f[j][k] += P - f[j][i] * temp[k] % P) %= P;
                (deg[j] += P - f[j][i] * temp[j] % P) %= P;
            }
        }
        return ans;
    }
}

signed main() {
    read >> n >> m;
    for (int i = 0; i < n; i++) read >> a[i];
    for (int i = 0, u, v; i < m; ++i) {
        read >> u >> v;
        --u, --v;
        f[u][v] = (f[u][v] + P - 1) % P;
        f[v][u] = (f[v][u] + P - 1) % P;
        (deg[u] += a[v]) %= P;
        (deg[v] += a[u]) %= P;
    }
    for (int i = 0; i < n; i++) (deg[i] += f[i][i]) %= P;
    if (a[n - 1] == 1) --n;
    else --a[n - 1];
    write << HangLieShi::resolve(f, n) << '\n';
    return 0;
}

/*
3 4
1 2 3
1 2
2 3
1 3
2 2
*/