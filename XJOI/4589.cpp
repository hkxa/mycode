/*************************************
 * @problem:      Holy Sequence.
 * @time:         2020-10-24.
 ************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef long long int64;
typedef unsigned long long uint64;

template <typename Int>
Int read() {
    Int d = 0;
    bool flag = 0;
    char ch = getchar();
    while ((ch < '0' || ch > '9') && ch != '-' && ch != EOF) ch = getchar();
    if (ch == '-') flag = 1, ch = getchar();
    d = ch & 15;
    while ((ch = getchar()) >= '0' && ch <= '9') d = (d << 3) + (d << 1) + (ch & 15);
    return flag ? -d : d;
}

template <typename Int>
void write(Int x) {
    static char buffer[33];
    static int top = 0;
    if (!x) {
        putchar('0');
        return;
    }
    if (x < 0) putchar('-'), x = -x;
    while (x) {
        buffer[++top] = '0' | (x % 10);
        x /= 10;
    }
    while (top) putchar(buffer[top--]);
}

const int N = 5e3 + 7;

int n, P;
int a[N];
int ans[N];
map<int, int> occur[N];

void contribute(const int &maxx) {
    static int cnt[N] = {0};
    for (int i = 1; i <= n; ++i) ++cnt[a[i]];
    for (int i = 1; i <= maxx; ++i) {
        ans[i] = (ans[i] + cnt[i] * cnt[i]) % P;
        if (cnt[i]) ++occur[i][cnt[i]];
        cnt[i] = 0;
    }
}
void solve_bf(int u, int maxx) {
    if (u > n) contribute(maxx);
    else {
        for (int i = 1; i <= maxx; ++i) {
            a[u] = i;
            solve_bf(u + 1, maxx);
        }
        a[u] = maxx + 1;
        solve_bf(u + 1, maxx + 1);
    }
}

void solve_std() {
    int64 prefix[N][N] = {0}, suffix[N][N] = {0};
    /**
     * prefix, suffix: 记录合法序列数
     * prefix[i][j]: n = i 时, p_n = j.
     * suffix[i][j]: a_x = j, 长度为 i.（序列: a_x ~ a_{x + i - 1}, x 为任意值无影响)
     */
    prefix[0][0] = 1;
    for (int i = 1; i <= n; ++i)
        for (int j = 1; j <= i; ++j) 
            prefix[i][j] = (prefix[i - 1][j] * j + prefix[i - 1][j - 1]) % P; 
            // p_{i - 1} = p_{i} (j 种), p_{i - 1} + 1 = p_i (1 种)
    
    for (int j = 1; j <= n; ++j) suffix[1][j] = 1;
    for (int i = 2; i <= n; ++i)
        for (int j = 1; j <= n - i + 1; ++j) 
            suffix[i][j] = (suffix[i - 1][j] * j + suffix[i - 1][j + 1]) % P;
            // a_{i} >= a_{i - 1} (j 种), a_{i} + 1 = a_{i + 1} (1 种)
    
    // 贡献为平方(k²)，可以看成在 k 个元素中可以重复地选 2 个的情况数
    for (int i = 1; i <= n; ++i)                    // number X 的贡献
        for (int j = i; j <= n; ++j) {              // 第一次 X 出现在 j 处
            int64 SufCnt = suffix[n - j + 1][i];    // 占用 (j, j)
            if (j < n) SufCnt += 3 * (n - j) * suffix[n - j][i] % P; // 占用 (j, x) / (x, j) / (x, x)
            if (j < n - 1) SufCnt += (n - j) * (n - j - 1) * suffix[n - j - 1][i] % P; // 占用 (x, y)
            ans[i] = (ans[i] + prefix[j - 1][i - 1] * SufCnt) % P;
        }
}

signed main() {
    n = read<int>();
    P = read<int>();
    if (n <= 0) solve_bf(1, 0);
    else solve_std();
    for (int i = 1; i <= n; ++i) {
        write(ans[i]);
        putchar(" \n"[i == n]);
    }
    return 0;
}

/*
[n=10]
Number 1:
(1:21147) (2:37260) (3:31572) (4:17052) (5:6552) (6:1890) (7:420) (8:72) (9:9) (10:1)
Number 2:
(1:26442) (2:40817) (3:29786) (4:13547) (5:4251) (6:958) (7:155) (8:17) (9:1)
Number 3:
(1:36192) (2:43913) (3:24781) (4:8416) (5:1865) (6:271) (7:24) (8:1)
Number 4:
(1:50207) (2:38314) (3:14131) (4:3050) (5:400) (6:30) (7:1)
Number 5:
(1:47266) (2:19906) (3:4295) (4:525) (5:35) (6:1)
Number 6:
(1:23496) (2:5335) (3:632) (4:39) (5:1)
Number 7:
(1:5923) (2:710) (3:42) (4:1)
Number 8:
(1:751) (2:44) (3:1)
Number 9:
(1:45) (2:1)
Number 10:
(1:1)
*/