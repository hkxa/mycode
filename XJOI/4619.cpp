/*************************************
 * @problem:      Mortal.
 * @user_name:    brealid.
 * @time:         2020-11-06.
*************************************/

#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio
{
    struct Reader
    {
        char endch;
        Reader() { endch = '\0'; }
        Reader &operator>>(char &ch)
        { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t')
                ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader &operator>>(Int &d)
        {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF)
                endch = getchar();
            if (endch == '-')
                flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar()))
                d = (d << 3) + (d << 1) + (endch & 15);
            if (flag)
                d = -d;
            return *this;
        }
        template <typename T>
        inline T get()
        {
            T Val;
            (*this) >> Val;
            return Val;
        }
    } read;

    struct Writer
    {
        Writer &operator<<(const char *ch)
        {
            while (*ch)
                putchar(*(ch++));
            return *this;
        }
        Writer &operator<<(const char ch)
        {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer &operator<<(Int x)
        {
            static char buffer[33];
            static int top = 0;
            if (!x)
            {
                putchar('0');
                return *this;
            }
            if (x < 0)
                putchar('-'), x = -x;
            while (x)
            {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top)
                putchar(buffer[top--]);
            return *this;
        }
    } write;
} // namespace Fastio
using namespace Fastio;

void File_IO_init(string file_name)
{
    freopen((file_name + ".in").c_str(), "r", stdin);
    freopen((file_name + ".out").c_str(), "w", stdout);
}

const int N = 1e6 + 7;

int l, r;
int f[N];

signed main()
{
    File_IO_init("mortal");
    read >> l >> r;
    int ans = 0, k1, k2;
    for (k1 = 0; (2 << k1) <= l; k1++);
    for (k2 = 0; (2 << k2) <= r; k2++);
    while (k1 < k2 && l <= r) {
        ans += r - (1 << k2) + 1;
        r = (2 << k2) - r - 2;
        for (k2 = 0; (2 << k2) <= r; k2++);
    }
    write << ans + max(r - l + 1, 0) << '\n';
    return 0;
}
/*
1
2
3
4
5
6
7

0100
0101
0110
0111
1001
1010
1011
1100
1101
*/