/*************************************
 * @problem:      hot.
 * @time:         2020-10-17.
 ************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 1
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef long long int64;
typedef unsigned long long uint64;

template <typename Int>
Int read() {
    Int d = 0;
    bool flag = 0;
    char ch = getchar();
    while ((ch < '0' || ch > '9') && ch != '-' && ch != EOF) ch = getchar();
    if (ch == '-') flag = 1, ch = getchar();
    d = ch & 15;
    while ((ch = getchar()) >= '0' && ch <= '9') d = (d << 3) + (d << 1) + (ch & 15);
    return flag ? -d : d;
}

template <typename Int>
void write(Int x) {
    static char buffer[33];
    static int top = 0;
    if (!x) {
        putchar('0');
        return;
    }
    if (x < 0) putchar('-'), x = -x;
    while (x) {
        buffer[++top] = '0' | (x % 10);
        x /= 10;
    }
    while (top) putchar(buffer[top--]);
}

const int N = 5e6 + 7;

int n, m;
int a[N];
int rightmost[N], leftmost[N];

signed main() {
    n = read<int>();
    m = read<int>();
    for (int i = 1; i <= n; ++i) a[i] = read<int>();
    leftmost[1] = 1;
    for (int i = 2; i <= n; ++i)
        if (a[i - 1] >= a[i]) leftmost[i] = leftmost[i - 1];
        else leftmost[i] = i;
    rightmost[n] = n;
    for (int i = n - 1; i >= 1; --i)
        if (a[i + 1] >= a[i]) rightmost[i] = rightmost[i + 1];
        else rightmost[i] = i;
    for (int i = 1, l, r; i <= m; ++i) {
        l = read<int>();
        r = read<int>();
        if (rightmost[l] >= leftmost[r]) puts("Y");
        else puts("N");
    }
    return 0;
}