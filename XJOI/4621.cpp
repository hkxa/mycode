/*************************************
 * @problem:      Stardust.
 * @user_name:    brealid.
 * @time:         2020-11-06.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

void File_IO_init(string file_name) {
    freopen((file_name + ".in" ).c_str(), "r", stdin);
    freopen((file_name + ".out").c_str(), "w", stdout);
}

const int N = 1e5 + 7;

int n, m, Q;
vector<int> a1[N], b1[N], a2[N], b2[N];
int reach_a[N], reach_b[N];
vector<pair<int, int> > circ;

inline void circ_push_back(int a, int b) {
    int g = __gcd(a, b);
    circ.push_back(make_pair(a / g, b / g));
}

int64 a, b;
inline void contribute(const pair<int, int> &x, const pair<int, int> &y) {
    int ga = __gcd(x.first, y.first);
    int64 ax = (int64)x.second * (y.first / ga);
    int64 ay = (int64)y.second * (x.first / ga);
    a = __gcd(a, abs(ax - ay));
    int gb = __gcd(x.second, y.second);
    int64 bx = (int64)x.first * (y.second / gb);
    int64 by = (int64)y.first * (x.second / gb);
    b = __gcd(b, abs(bx - by));
}

void pretreat() {
    bool reached[N];
    memset(reached, 0, sizeof(reached));
    reached[1] = true;
    queue<int> q;
    q.push(1);
    while (!q.empty()) {
        int u = q.front(); q.pop();
        for (int &v : a1[u])
            if (!reached[v]) {
                reached[v] = true;
                reach_a[v] = reach_a[u] + 1;
                reach_b[v] = reach_b[u];
                q.push(v);
            } else if (reach_a[u] + 1 != reach_a[v] || reach_b[u] != reach_b[v])
                circ_push_back(reach_a[u] + 1 - reach_a[v], reach_b[u] - reach_b[v]);
        for (int &v : a2[u])
            if (!reached[v]) {
                reached[v] = true;
                reach_a[v] = reach_a[u] - 1;
                reach_b[v] = reach_b[u];
                q.push(v);
            } else if (reach_a[u] - 1 != reach_a[v] || reach_b[u] != reach_b[v])
                circ_push_back(reach_a[u] - 1 - reach_a[v], reach_b[u] - reach_b[v]);
        for (int &v : b1[u])
            if (!reached[v]) {
                reached[v] = true;
                reach_a[v] = reach_a[u];
                reach_b[v] = reach_b[u] + 1;
                q.push(v);
            } else if (reach_a[u] != reach_a[v] || reach_b[u] + 1 != reach_b[v])
                circ_push_back(reach_a[u] - reach_a[v], reach_b[u] + 1 - reach_b[v]);
        for (int &v : b2[u])
            if (!reached[v]) {
                reached[v] = true;
                reach_a[v] = reach_a[u];
                reach_b[v] = reach_b[u] - 1;
                q.push(v);
            } else if (reach_a[u] != reach_a[v] || reach_b[u] - 1 != reach_b[v])
                circ_push_back(reach_a[u] - reach_a[v], reach_b[u] - 1 - reach_b[v]);
    }
    sort(circ.begin(), circ.end());
    unique(circ.begin(), circ.end());
    if (circ.size() <= 1) return;
    contribute(circ[0], circ[1]);
    for (size_t i = 2; i < circ.size(); ++i)
        contribute(circ[i - 1], circ[i]);
}
bool ALL_A = true;
bool resolve(int a0, int b0) {
    if (ALL_A || circ.empty()) return false;
    if (circ.size() == 1) return (int64)a0 * circ[0].second == (int64)b0 * circ[0].first;
    set<pair<int, int> >s;
    queue<pair<int, int> > q;
    q.push(make_pair(a0, b0));
    s.insert(make_pair(a0, b0));
    while (!q.empty() && s.size() <= 1000) {
        pair<int, int> pii = q.front();
        // printf("pii = {%d, %d}\n", pii.first, pii.second);
        q.pop();
        for (size_t i = 0; i < circ.size(); ++i) {
            pair<int, int> nxt(pii.first + circ[i].first, pii.second + circ[i].second);
            if (!nxt.first && !nxt.second) return true;
            if (s.insert(nxt).second) q.push(nxt);
            nxt = make_pair(pii.first - circ[i].first, pii.second - circ[i].second);
            if (!nxt.first && !nxt.second) return true;
            if (s.insert(nxt).second) q.push(nxt);
        }
    }
    return false;
}

signed main() {
    File_IO_init("stardust");
    read >> n >> m;
    char channel_type;
    for (int i = 1, u, v; i <= m; ++i) {
        read >> u >> v >> channel_type;
        if (channel_type == 'A') {
            a1[u].push_back(v);
            a2[v].push_back(u);
        } else {
            b1[u].push_back(v);
            b2[v].push_back(u);
            ALL_A = false;
        }
    }
    pretreat();
    // for (int i = 1; i <= n; ++i)
    //     printf("reach[%u] = {%d, %d}\n", i, reach_a[i], reach_b[i]);
    // for (size_t i = 0; i < circ.size(); ++i)
    //     printf("circ[%u] = {%d, %d}\n", i, circ[i].first, circ[i].second);
    if (!a) a = INT_MAX;
    if (!b) b = INT_MAX;
    // write << a << ' ' << b << '\n';
    read >> Q;
    for (int i = 1, p, a0, b0; i <= m; ++i) {
        read >> p >> a0 >> b0;
        a0 -= reach_a[p];
        b0 -= reach_b[p];
        a0 = (a0 % a + a) % a;
        b0 = (b0 % b + b) % b;
        if ((!a0 && !b0) || resolve(a0, b0)) puts("Yes");
        else puts("No");
    }
    return 0;
}