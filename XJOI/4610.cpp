/*************************************
 * @problem:      四平方和.
 * @user_name:    brealid.
 * @time:         2020-11-04.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

const int64 N = 1e7, lim = 1e10;

// 1 = d1 < d2 < d3 < d4 < sqrt(n)
// lcm(d2, d3, d4) <= 1e18

bool check(int64 d2, int64 d3, int64 d4, int64 n) {
    if (n % d2 != 0 || n % d3 != 0 || n % d4 != 0) return false;
    for (int i = 2; i < d2; ++i) if (n % i == 0) return false;
    for (int i = d2 + 1; i < d3; ++i) if (n % i == 0) return false;
    for (int i = d3 + 1; i < d4; ++i) if (n % i == 0) return false;
    return true;
}

bool isnp[N];
int primes[N], cnt;

void get_primes() {
    for (int i = 2; i < N; ++i) {
        if (!isnp[i]) {
            primes[cnt++] = i;
            for (int j = i + i; j < N; j += i)
                isnp[j] = true;
        }
    }
}

void try_to_upd(int64 d2, int64 d3, int64 d4) {
    if (check(d2, d3, d4, 1 + d2 * d2 + d3 * d3 + d4 * d4)) printf("%lld is ok\n", 1 + d2 * d2 + d3 * d3 + d4 * d4);
}

void foreach_d4(int64 d2, int64 d3) {
    int64 up_bound = sqrt(lim);
    if (d2 * d3 * d2 * d3 <= lim) {
        try_to_upd(d2, d3, d2 * d3);
        up_bound = d2 * d3;
    }
    for (int j = upper_bound(primes, primes + cnt, d3) - primes; j < cnt && primes[j] < up_bound; ++j)
        try_to_upd(d2, d3, primes[j]);
}

void foreach_d3(int64 d2) {
    foreach_d4(d2, d2 * d2);
    for (int j = upper_bound(primes, primes + cnt, d2) - primes; j < cnt && 1 + d2 * d2 + primes[j] * primes[j] + (primes[j] + 1) * (primes[j] + 1) <= N; ++j)
        foreach_d4(d2, primes[j]);
}

void foreach_d2() {
    for (int64 d2 = 2; 1 + d2 * d2 + (d2 + 1) * (d2 + 1) + (d2 + 2) * (d2 + 2) <= lim; ++d2)
        foreach_d3(d2);
}

signed main() {
    // get_primes();
    // foreach_d2();
    if (read.get<int64>() < 130) puts("0");
    else puts("1");
    return 0;
}