/*************************************
 * @problem:      Myth.
 * @user_name:    brealid.
 * @time:         2020-11-06.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

void File_IO_init(string file_name) {
    freopen((file_name + ".in" ).c_str(), "r", stdin);
    freopen((file_name + ".out").c_str(), "w", stdout);
}

const int N = 1e5 + 7;

int T, n;
int lu[N], ru[N], ld[N], rd[N];

signed main() {
    File_IO_init("myth");
    read >> T;
    while (T--) {
        read >> n;
        for (int i = 0; i <= n; ++i) {
            lu[i] = ld[i] = -2;
            ru[i] = rd[i] = n + 2;
        }
        for (int i = 1, x, y, d; i <= n; ++i) {
            read >> x >> y >> d;
            switch (d) { 
                case 1: rd[x] = min(rd[x], y); break;
                case 2: ru[x - 1] = min(ru[x - 1], y); break;
                case 3: lu[x - 1] = max(lu[x - 1], y - 1); break;
                case 4: ld[x] = max(ld[x], y - 1); break;
            }
        }
        for (int i = 1; i <= n; ++i) {
            ld[i] = max(ld[i], ld[i - 1]);
            rd[i] = min(rd[i], rd[i - 1]);
        }
        for (int i = n - 1; i >= 0; --i) {
            lu[i] = max(lu[i], lu[i + 1]);
            ru[i] = min(ru[i], ru[i + 1]);
        }
        bool fail = false;
        for (int i = 0; i <= n && !fail; ++i)
            if (max(lu[i], ld[i]) + 1 < min(ru[i], rd[i])) fail = true;
        puts(!fail ? "Yes" : "No");
    }
    return 0;
}

/*
2
1
1 1 1
4
1 1 1
4 1 2
4 4 3
1 4 4
*/