/*************************************
 * @problem:      Red and Black.
 * @time:         2020-10-24.
 ************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef long long int64;
typedef unsigned long long uint64;

template <typename Int>
Int read() {
    Int d = 0;
    bool flag = 0;
    char ch = getchar();
    while ((ch < '0' || ch > '9') && ch != '-' && ch != EOF) ch = getchar();
    if (ch == '-') flag = 1, ch = getchar();
    d = ch & 15;
    while ((ch = getchar()) >= '0' && ch <= '9') d = (d << 3) + (d << 1) + (ch & 15);
    return flag ? -d : d;
}

template <typename Int>
void write(Int x) {
    static char buffer[33];
    static int top = 0;
    if (!x) {
        putchar('0');
        return;
    }
    if (x < 0) putchar('-'), x = -x;
    while (x) {
        buffer[++top] = '0' | (x % 10);
        x /= 10;
    }
    while (top) putchar(buffer[top--]);
}


int64 r, b;

int64 gcd(int64 a, int64 b) {
    return a ? gcd(b % a, a) : b;
}

signed main() {
    r = read<int64>();
    b = read<int64>();
    int64 fac_son = r * (r - 1) / 2;
    int64 fac_mon = (r + b) * (r + b - 1) / 2;
    int64 g = gcd(fac_son, fac_mon);
    printf("%lld/%lld\n", fac_son / g, fac_mon / g);
    return 0;
}