/**
 * Do not mind how heavily it is raining,
 * as there's always sun shinning after the rain passed.
 * 
 * NOIP.rp++;
 */
#include <bits/stdc++.h>

namespace fastio {
    struct Getchar {
        char buf[1000000], *p1, *p2;
        Getchar() : p1(buf), p2(buf) {}
        char operator() () {
            if (p1 == p2) p2 = buf + fread(p1 = buf, 1, 1000000, stdin);
            return p1 == p2 ? EOF : *p1++;
        }
    } getchar;
    struct Putchar {
        char buf[1000000], *p1, *p2;
        Putchar() : p1(buf), p2(buf + 1000000) {}
        ~Putchar() { fwrite(buf, 1, p1 - buf, stdout); }
        void operator() (char ch) {
            if (p1 == p2) fwrite(p1 = buf, 1, 1000000, stdout);
            *p1++ = ch;
        }
    } putchar;
    template<typename I> inline void get_uint(I &x) { 
        static char ch = 0;
        while (!isdigit(ch = getchar()));
        x = ch & 15;
        while (isdigit(ch = getchar())) x = (((x << 2) + x) << 1) + (ch & 15);
    }
    template<typename I> inline void attach_uint(I x) { 
        static char buf[23];
        static int top = 0;
        if (x < 0) putchar('-'), x = -x;
        do {
            buf[++top] = '0' | (x % 10);
            x /= 10;
        } while (x);
        while (top) putchar(buf[top--]);
    }
    inline void attach_str(const char *str) { 
        while (*str) putchar(*str++);
    }
}

const int N = 2e5 + 7;

int n;
std::vector<int> G[N];
int siz[N];

void dfs(int u, int fa) {
    siz[u] = 1;
    for (size_t i = 0; i < G[u].size(); ++i) {
        int v = G[u][i];
        if (v != fa) {
            dfs(v, u);
            siz[u] += siz[v];
        }
    }
}

std::multiset<int> s;
int ans = 2147483647;

inline int calc(const int &a, const int &b, const int &c) {
    if (a < b) {
        if (b < c) return c - a;
        else if (a < c) return b - a;
        else return b - c;
    } else {
        if (b > c) return a - c;
        else if (a > c) return a - b;
        else return c - b;
    }
}

void get_ans(int u, int fa) {
    std::multiset<int>::iterator it = s.lower_bound((n - siz[u]) >> 1);
    if (it != s.end()) ans = std::min(ans, calc(siz[u], *it, n - siz[u] - *it));
    if (it != s.begin()) --it, ans = std::min(ans, calc(siz[u], *it, n - siz[u] - *it));
    s.insert(n - siz[u]);
    for (size_t i = 0; i < G[u].size(); ++i) {
        int v = G[u][i];
        if (v != fa) get_ans(v, u);
    }
    s.erase(s.find(n - siz[u]));
    s.insert(siz[u]);
}

int main() {
    fastio::get_uint(n);
    for (int i = 1, u, v; i < n; ++i) {
        fastio::get_uint(u), fastio::get_uint(v);
        G[u].push_back(v), G[v].push_back(u);
    }
    dfs(1, 0);
    get_ans(1, 0);
    fastio::attach_uint(ans);
    fastio::putchar('\n');
    return 0;
}