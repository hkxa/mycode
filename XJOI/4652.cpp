/**
 * Do not mind how heavily it is raining,
 * as there's always sun shinning after the rain passed.
 * 
 * NOIP.rp++;
 */
#include <bits/stdc++.h>
typedef long long int64;

namespace io {
    struct Getchar {
        char buf[1000000], *p1, *p2;
        Getchar() : p1(buf), p2(buf) {}
        char operator() () {
            if (p1 == p2) p2 = buf + fread(p1 = buf, 1, 1000000, stdin);
            return p1 == p2 ? EOF : *p1++;
        }
    } getchar;
    template<typename I> inline void get_uint(I &x) { 
        static char ch = 0;
        while (!isdigit(ch = getchar()) && ch != EOF);
        x = ch & 15;
        while (isdigit(ch = getchar())) x = (((x << 2) + x) << 1) + (ch & 15);
    }
}

template<typename T> inline void chk_max(T &a, const T &b) { if (a < b) a = b; }

const int N = 2e5 + 7;
int n;
int64 a[N], b[N], ans = 0;

struct SegmentTree {
    struct funct {
        int64 k, b;
        funct() {}
        funct(const int64 &K, const int64 &B) : k(K), b(B) {}
        int64 operator () (const int64 x) {
            return k * x + b;
        }
    };
    int64 slp(const funct &u, const funct &v) {
        return -(u.b - v.b) / (u.k - v.k);
    }
    std::vector<funct> tr[N << 2];
    int pos[N << 2];
 
    void build(int x, int l, int r, int64 d[]) {
        static funct stk[N];
        static int len;
        len = 0;
        for (int i = l; i <= r; i++) {
            if (len && stk[len].k == d[i])
                continue;
            funct now(d[i], -d[i] * (i - 1));
            while (len >= 2 && slp(stk[len - 1], stk[len]) > slp(stk[len], now)) --len;
            stk[++len] = now;
        }
 
        tr[x] = std::vector<funct>(stk + 1, stk + len + 1);
        pos[x] = 0;
 
        if (l != r) {
            int mid = (l + r) >> 1;
            build(x << 1, l, mid, d);
            build(x << 1 | 1, mid + 1, r, d);
        }
    }
    int64 query(int x, const int &l, const int &r, const int &L, const int &R, const int64 &X) {
        if (L <= l && r <= R) {
            int &cur = pos[x];
            while (cur + 1 < (int64)tr[x].size() && tr[x][cur](X) < tr[x][cur + 1](X)) ++cur;
            return tr[x][cur](X);
        }
 
        int mid = (l + r) >> 1;
        int64 ans = -1e18;
        if (L <= mid) ans = query(x << 1, l, mid, L, R, X);
        if (R > mid) chk_max(ans, query(x << 1 | 1, mid + 1, r, L, R, X));
        return ans;
    }
} trA, trB, trAB;
 
void solve(int l, int r) {
    static int64 A[N], B[N], AB[N];
    if (l == r) {
        chk_max(ans, a[l] * b[l]);
        return;
    }
    int mid = (l + r) >> 1;
    A[mid] = a[mid];
    for (int i = mid - 1; i >= l; i--)
        A[i] = std::min(A[i + 1], a[i]);
    A[mid + 1] = a[mid + 1];
    for (int i = mid + 2; i <= r; i++)
        A[i] = std::min(A[i - 1], a[i]);
 
    B[mid] = b[mid];
    for (int i = mid - 1; i >= l; i--)
        B[i] = std::min(B[i + 1], b[i]);
    B[mid + 1] = b[mid + 1];
    for (int i = mid + 2; i <= r; i++)
        B[i] = std::min(B[i - 1], b[i]);
 
    for (int i = l; i <= r; i++)
        AB[i] = A[i] * B[i];
 
    trA.build(1, l, mid, A);
    trB.build(1, l, mid, B);
    trAB.build(1, l, mid, AB);
 
    for (int i = mid + 1, ja = mid, jb = mid; i <= r; ++i) {
        while (ja >= l && A[ja] >= A[i]) --ja;
        while (jb >= l && B[jb] >= B[i]) --jb;
        chk_max(ans, (i - std::max(ja, jb)) * A[i] * B[i]);
        if (ja >= l && jb >= l) chk_max(ans, trAB.query(1, l, mid, l, std::min(ja, jb), i));
        if (jb < ja) chk_max(ans, trA.query(1, l, mid, jb + 1, ja, i) * B[i]);
        if (jb > ja) chk_max(ans, trB.query(1, l, mid, ja + 1, jb, i) * A[i]);
    }
    solve(l, mid);
    solve(mid + 1, r);
}


int main() {
    io::get_uint(n);
    for (int i = 1; i <= n; ++i)
        io::get_uint(a[i]), io::get_uint(b[i]);
    solve(1, n);
    std::cout << ans << std::endl;
    return 0;
}