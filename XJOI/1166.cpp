/*************************************
 * problem:      id_name.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-mm-dd.
 * language:     C++.
 * upload place: Luogu.
*************************************/
 
#include <bits/stdc++.h>
using namespace std;
 
typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;
 
template <typename Int>
inline Int read()      
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}
 
template <typename Int>
inline Int read(char &c)      
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}
 
template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
} 
 
template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}
 
int n, m;
int p = 1, q = 1;
int a[1007], b[1007];
 
int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        a[i] = read<int>();
    }
    m = read<int>();
    for (int i = 1; i <= m; i++) {
        b[i] = read<int>();
    }
    for (int i = 1; i <= n + m && p <= n && q <= m; i++) {
        if (a[p] < b[q]) write(a[p++], 32);
        else write(b[q++], 32);
    }
    while (p <= n) write(a[p++], 32);
    while (q <= m) write(b[q++], 32);
    return 0;
}