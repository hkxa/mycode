/*************************************
 * @problem:      ring.
 * @time:         2020-10-17.
 ************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef long long int64;
typedef unsigned long long uint64;

template <typename Int>
Int read() {
    Int d = 0;
    bool flag = 0;
    char ch = getchar();
    while ((ch < '0' || ch > '9') && ch != '-' && ch != EOF) ch = getchar();
    if (ch == '-') flag = 1, ch = getchar();
    d = ch & 15;
    while ((ch = getchar()) >= '0' && ch <= '9') d = (d << 3) + (d << 1) + (ch & 15);
    return flag ? -d : d;
}

template <typename Int>
void write(Int x) {
    static char buffer[33];
    static int top = 0;
    if (!x) {
        putchar('0');
        return;
    }
    if (x < 0) putchar('-'), x = -x;
    while (x) {
        buffer[++top] = '0' | (x % 10);
        x /= 10;
    }
    while (top) putchar(buffer[top--]);
}

const int N = 5e4 + 7;

int n, k;
char str[N];
bitset<N> s, mask, f, ans;

inline bitset<N> CircleRight_kth(const bitset<N> &a, int step) {
    return ((a << step) & mask) | (a >> (n - step));
}

void sol_odd() {
    for (int i = 0; i % n != 1 || ((i / n) & 1); i += k) f.flip(i % n);
    for (int i = s._Find_first(); i < n; i = s._Find_next(i))
        ans ^= CircleRight_kth(f, i);
}

void sol_even() {
    for (int i = 0; i % n != 1 || !((i / n) & 1); i += k) f.flip(i % n);
    f ^= CircleRight_kth(f, 1);
    if ((s.count() & 1) == (n & 1)) s = ~s & mask;
    for (int i = s._Find_first(); i < n; i = s._Find_next(i))
        for (int j = s._Find_next(i); i < j && j < n; ++i)
            ans ^= CircleRight_kth(f, i);
}

signed main() {
    n = read<int>();
    k = read<int>();
    scanf("%s", str);
    for (int i = 0; i < n; ++i) {
        if (str[i] == '1') s.set(i);
        mask.set(i);
    }
    if (k & 1) sol_odd();
    else sol_even();
    int m = ans.count();
    if (n - m < m || (n - m == m && !ans.test(0))) ans = ~ans & mask;
    write(ans.count());
    putchar(10);
    for (int i = ans._Find_first(); i < n; i = ans._Find_next(i)) {
        write(i);
        putchar(32);
    }
    putchar(10);
    return 0;
}