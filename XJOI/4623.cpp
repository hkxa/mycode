/*************************************
 * @problem:      纸条.
 * @user_name:    brealid.
 * @time:         2020-11-05.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

template<const unsigned maxSize, const unsigned mK, const unsigned mP>
class HashEngine {
private:
    int length;
    int64 power_mK[maxSize], hash_value[maxSize];
public:
    template <typename T>
    void init(T *arr, int n) {
        length = n;
        power_mK[0] = hash_value[0] = 1;
        for (int i = 1; i <= n; ++i) {
            power_mK[i] = power_mK[i - 1] * mK % mP;
            hash_value[i] = (hash_value[i - 1] * mK + arr[i]) % mP;
        }
    }
    unsigned qHash(int l, int r) {
        return (hash_value[r] - hash_value[l - 1] * power_mK[r - l + 1] % mP + mP) % mP;
    }
    unsigned query(int l, int r) {
        return (qHash(1, l) * power_mK[length - r + 1] + qHash(r, length)) % mP;
    }
};

template<const unsigned N, const unsigned bucketSize>
class HashSet {
  private:
    uint64 val[N];
    int cnt[N], nxt[N], tot;
    int head[bucketSize], headTimeX[bucketSize], TimeXnow;
  public:
    HashSet() : tot(0), TimeXnow(1) {
        memset(head, 0, sizeof(head));
        memset(headTimeX, 0, sizeof(headTimeX));
    }
    void clear() {
        tot = 0;
        ++TimeXnow;
    }
    int insert(uint64 v) {
        int bp = v % bucketSize;
        if (headTimeX[bp] != TimeXnow) {
            headTimeX[bp] = TimeXnow;
            head[bp] = 0;
        }
        for (int u = head[bp]; u; u = nxt[u])
            if (val[u] == v) return ++cnt[u];
        val[++tot] = v;
        cnt[tot] = 1;
        nxt[tot] = head[bp];
        head[bp] = tot;
        return 1;
    }
    int erase(uint64 v) {
        int bp = v % bucketSize;
        for (int u = head[bp]; u; u = nxt[u])
            if (val[u] == v) return cnt[u]--;
        return 0;
    }
};

const int N = 1e6 + 7, P = 998244353;
int n;
char s[N];
int inv[N];

HashEngine<N, 101, 1000000009> h1;
HashEngine<N, 541, 1000000007> h2;
HashSet<N, 1 << 17> hs;

void init_inv(int n) {
    inv[1] = 1;
    for (int i = 2; i <= n; ++i) inv[i] = (int64)(P - P / i) * inv[P % i] % P;
}

inline uint64 GetStrHash(int l, int r) {
    uint64 ret = (uint64)h1.query(l, r) << 31 | h2.query(l, r);
    ret ^= ret >> 15;
    ret ^= ret << 7;
    ret ^= ret >> 19;
    return ret ^ 19260817;
}

signed main() {
    scanf("%s", s + 1);
    n = strlen(s + 1);
    init_inv(n);
    h1.init(s, n);
    h2.init(s, n);
    uint64 ans = 1;
    for (int i = 1; i < n; ++i) {
        hs.clear();
        if (n % i == 0) ++ans;
        for (int l = 0, r = (n - 1) % i + 2; r <= n + 1; l += i, r += i)
            if (hs.insert(GetStrHash(l, r)) == 1) ++ans;
    }
    write << ans << '\n';
    return 0;
}