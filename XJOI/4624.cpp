/*************************************
 * @problem:      积木游戏.
 * @user_name:    brealid.
 * @time:         2020-11-05.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

const int N = 5007, P = 998244353;

int w, h;
int64 f[2][N][3];
typedef int64 DpArr[N][3];

void array_fill(DpArr f, int h, int tp, int64 value) {
    f[0][tp] = 0;
    for (int i = 1; i <= h; ++i)
        f[i][tp] = value;
}

int64 array_sum(DpArr f, int h, int tp) {
    int64 result = 0;
    for (int i = 0; i <= h; ++i)
        result = (result + f[i][tp]) % P;
    return result;
}

int64 solve(int w, int h) {
    memset(f, 0, sizeof(f));
    f[0][0][0] = 1;
    for (int i = 1; i <= w; ++i) {
        DpArr &now = f[i & 1], &lst = f[~i & 1];
        array_fill(now, h, 0, array_sum(lst, h, 0));
        array_fill(now, h, 1, array_sum(lst, h, 1));
        array_fill(now, h, 2, array_sum(lst, h, 2));
        int64 sf0 = 0, sf1 = 0, sf2 = 0;
        for (int j = 1; j <= h; ++j) {
            sf0 = (sf0 + lst[j - 1][0]) % P;
            sf1 = (sf1 + (j - 1) * lst[j - 1][0]) % P;
            now[j][1] = (now[j][1] + j * sf0 - sf1) % P;
        }
        sf0 = sf1 = sf2 = 0;
        for (int j = 1; j <= h; ++j) {
            sf0 = (sf0 + lst[j - 1][1]) % P;
            sf1 = (sf1 + (j - 1) * lst[j - 1][1]) % P;
            now[j][2] = (now[j][2] + 2 * (j * sf0 - sf1)) % P;
        }
        sf0 = sf1 = sf2 = 0;
        for (int j = 1; j <= h; ++j) {
            sf0 = (sf0 + lst[j - 1][0]) % P;
            sf1 = (sf1 + (j - 1) * lst[j - 1][0]) % P;
            sf2 = (sf2 + (j - 1) * (j - 1) * lst[j - 1][0]) % P;
            now[j][2] = (now[j][2] + j * j * sf0 - 2 * j * sf1 + sf2) % P;
        }
    }
    int64 ans = 0;
    DpArr &now = f[w & 1];
    for (int i = 1; i <= h; ++i) ans = (ans + now[i][2]) % P;
    return (ans + P) % P;
}

signed main() {
    read >> w >> h; 
    // w = 4; h = 3;
    write << (solve(w, h) - solve(w, h - 1) + P) % P << '\n';
    return 0;
}