/**
 * Do not mind how heavily it is raining,
 * as there's always sun shinning after the rain passed.
 */
#include <bits/stdc++.h>

namespace fastio {
    namespace base {
        struct Getchar {
            char buf[1000000], *p1, *p2;
            Getchar() : p1(buf), p2(buf) {}
            char predict() {
                if (p1 == p2) p2 = buf + fread(p1 = buf, 1, 1000000, stdin);
                return p1 == p2 ? EOF : *p1;
            }
            char operator() () {
                if (p1 == p2) p2 = buf + fread(p1 = buf, 1, 1000000, stdin);
                return p1 == p2 ? EOF : *p1++;
            }
        } getchar;
        struct Putchar {
            char buf[1000000], *p1, *p2;
            Putchar() : p1(buf), p2(buf + 1000000) {}
            ~Putchar() { fwrite(buf, 1, p1 - buf, stdout); }
            void operator() (char ch) {
                if (p1 == p2) fwrite(p1 = buf, 1, 1000000, stdout);
                *p1++ = ch;
            }
        } putchar;
        template<typename I> inline void get_int(I &x) { 
            static char ch = 0;
            bool negative = false;
            while (!isdigit(ch = getchar()) && ch != '-' && ch != EOF);
            if (ch == '-') {
                negative = true;
                x = getchar() & 15;
            } else x = ch & 15;
            while (isdigit(ch = getchar())) x = (((x << 2) + x) << 1) + (ch & 15);
            if (negative) x = -x;
        }
        template<typename I> inline void get_uint(I &x) { 
            static char ch = 0;
            while (!isdigit(ch = getchar()) && ch != EOF);
            x = ch & 15;
            while (isdigit(ch = getchar())) x = (((x << 2) + x) << 1) + (ch & 15);
        }
        inline void get_str(char *str) { 
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            if (*str != EOF)
                while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
        }
        inline void get_cpp_str(std::string &str) { 
            str.clear();
            char cc;
            while (((cc = getchar()) == ' ' || cc == '\n' || cc == '\r' || cc == '\t') && cc != EOF);
            while (cc != ' ' && cc != '\n' && cc != '\r' && cc != '\t' && cc != EOF) {
                str.push_back(cc);
                cc = getchar();
            }
            cc = '\0';
        }
        inline void get_ch(char &ch) { 
            while ((ch = getchar()) == ' ' || ch == '\n' || ch == '\r' || ch == '\t');
        }
        template<typename I> inline void attach_int(I x) { 
            static char buf[23];
            static int top = 0;
            if (x < 0) putchar('-'), x = -x;
            do {
                buf[++top] = '0' | (x % 10);
                x /= 10;
            } while (x);
            while (top) putchar(buf[top--]);
        }
        template<typename I> inline void attach_uint(I x) { 
            static char buf[23];
            static int top = 0;
            do {
                buf[++top] = '0' | (x % 10);
                x /= 10;
            } while (x);
            while (top) putchar(buf[top--]);
        }
        inline void attach_str(const char *str) { 
            while (*str) putchar(*str++);
        }
    }
    struct InputStream {
        InputStream& operator >> (int &x) { base::get_int(x); return *this; }
        InputStream& operator >> (long long &x) { base::get_int(x); return *this; }
        InputStream& operator >> (unsigned &x) { base::get_uint(x); return *this; }
        InputStream& operator >> (unsigned long long &x) { base::get_uint(x); return *this; }
        InputStream& operator >> (char &x) { base::get_ch(x); return *this; }
        InputStream& operator >> (char *x) { base::get_str(x); return *this; }
        InputStream& operator >> (std::string &x) { base::get_cpp_str(x); return *this; }
        bool eof() const { return base::getchar.predict() == EOF; }
        char predict() const { return base::getchar.predict(); }
        operator bool() const { return eof(); }
    };
    struct OutputStream {
        OutputStream& operator << (const int &x) { base::attach_int(x); return *this; }
        OutputStream& operator << (const long long &x) { base::attach_int(x); return *this; }
        OutputStream& operator << (const unsigned &x) { base::attach_uint(x); return *this; }
        OutputStream& operator << (const unsigned long long &x) { base::attach_uint(x); return *this; }
        OutputStream& operator << (const char &x) { base::putchar(x); return *this; }
        OutputStream& operator << (const char *x) { base::attach_str(x); return *this; }
        OutputStream& operator << (const std::string &x) { base::attach_str(x.c_str()); return *this; }
        void put(const char &c) { base::putchar(c); }
    };
}
fastio::InputStream kin;
fastio::OutputStream kout;
template<typename T> T read() { printf("Error type for template-read: Not supportive.\n"); exit(1); }
template<> int read() { int x; kin >> x; return x; }
template<> long long read() { long long x; kin >> x; return x; }
template<> unsigned read() { unsigned x; kin >> x; return x; }
template<> unsigned long long read() { unsigned long long x; kin >> x; return x; }
template<> char read() { return fastio::base::getchar(); }
template<> std::string read() { std::string x; kin >> x; return x; }

inline void chkmin(int &a, const int &b) { if (a > b) a = b; }

const int N = 5e5 + 7;

int n;
char s[N];
std::vector<int> G[N];

int siz[N], f[N][3][2], g[3][2];
void dfs(int u, int fa) {
    bool is_need_rescue = (s[u] == '0');
    siz[u] = is_need_rescue;
    f[u][0][is_need_rescue] = f[u][1][is_need_rescue] = f[u][2][is_need_rescue] = 1;
    for (unsigned i = 0; i < G[u].size(); ++i) {
        int v = G[u][i];
        if (v == fa) continue;
        dfs(v, u);
        if (!siz[v]) continue;
        memset(g, 0x3f, sizeof(g));
        siz[u] += siz[v];
        chkmin(g[0][0], f[u][0][0] + f[v][0][0] + 3);
        chkmin(g[0][1], f[u][0][0] + f[v][0][1] + 1);
        chkmin(g[1][1], f[u][0][0] + f[v][1][0] + 2);
        chkmin(g[1][0], f[u][0][0] + f[v][1][1]);
        chkmin(g[2][0], f[u][0][0] + f[v][2][0] + 1);
        chkmin(g[2][1], f[u][0][0] + f[v][2][1] + 3);
        chkmin(g[0][1], f[u][0][1] + f[v][0][0] + 3);
        chkmin(g[0][0], f[u][0][1] + f[v][0][1] + 1);
        chkmin(g[1][0], f[u][0][1] + f[v][1][0] + 2);
        chkmin(g[1][1], f[u][0][1] + f[v][1][1]);
        chkmin(g[2][1], f[u][0][1] + f[v][2][0] + 1);
        chkmin(g[2][0], f[u][0][1] + f[v][2][1] + 3);
        chkmin(g[1][0], f[u][1][0] + f[v][0][0] + 3);
        chkmin(g[1][1], f[u][1][0] + f[v][0][1] + 1);
        chkmin(g[2][1], f[u][1][0] + f[v][1][0] + 2);
        chkmin(g[2][0], f[u][1][0] + f[v][1][1]);
        chkmin(g[1][1], f[u][1][1] + f[v][0][0] + 3);
        chkmin(g[1][0], f[u][1][1] + f[v][0][1] + 1);
        chkmin(g[2][0], f[u][1][1] + f[v][1][0] + 2);
        chkmin(g[2][1], f[u][1][1] + f[v][1][1]);
        chkmin(g[2][0], f[u][2][0] + f[v][0][0] + 3);
        chkmin(g[2][1], f[u][2][0] + f[v][0][1] + 1);
        chkmin(g[2][1], f[u][2][1] + f[v][0][0] + 3);
        chkmin(g[2][0], f[u][2][1] + f[v][0][1] + 1);
        memcpy(f[u], g, sizeof(g));
    }
}

int main() {
    kin >> n >> (s + 1);
    for (int i = 1, u, v; i < n; ++i) {
        kin >> u >> v;
        G[u].push_back(v), G[v].push_back(u);
    }
    memset(f, 0x3f, sizeof(f));
    int root = strchr(s + 1, '0') - s;
    dfs(root, 0);
    kout << f[root][2][1] << '\n';
    return 0;
}