#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef long long int64;
typedef unsigned long long uint64;

template <typename Int>
Int read() {
    Int d = 0;
    bool flag = 0;
    char ch = getchar();
    while ((ch < '0' || ch > '9') && ch != '-' && ch != EOF) ch = getchar();
    if (ch == '-') flag = 1, ch = getchar();
    d = ch & 15;
    while ((ch = getchar()) >= '0' && ch <= '9') d = (d << 3) + (d << 1) + (ch & 15);
    return flag ? -d : d;
}

template <typename Int>
void write(Int x) {
    static char buffer[33];
    static int top = 0;
    if (!x) {
        putchar('0');
        return;
    }
    if (x < 0) putchar('-'), x = -x;
    while (x) {
        buffer[++top] = '0' | (x % 10);
        x /= 10;
    }
    while (top) putchar(buffer[top--]);
}

const int N = 50 + 3, P2 = 31 + 3;

int n, m, p, S, T;
int64 reachable[N][P2], reachable_in_one_step[N];

signed main() {
    n = read<int>();
    m = read<int>();
    p = read<int>();
    S = read<int>() - 1;
    T = read<int>() - 1;
    for (int i = 1, u, v; i <= m; ++i) {
        u = read<int>() - 1;
        v = read<int>() - 1;
        reachable[u][0] |= 1ll << v;
    }
    for (int i = 1; i < p; ++i)
        for (int u = 0; u < n; ++u)
            for (int v = 0; v < n; ++v)
                if (reachable[u][i - 1] & (1ll << v))
                    reachable[u][i] |= reachable[v][i - 1];
    for (int u = 0; u < n; ++u)
        for (int i = 0; i < p; ++i)
            reachable_in_one_step[u] |= reachable[u][i];
    // for (int u = 0; u < n; ++u)
    //     for (int v = 0; v < n; ++v)
    //         if (reachable_in_one_step[u] & (1ll << v))
    //             printf("in one step: %d can reach %d\n", u + 1, v + 1);
    int64 reach = 1ll << S;
    if (S == T) {
        puts("0");
        return 0;
    }
    for (int path_cnt = 1; path_cnt <= n * 2; ++path_cnt) {
        int64 last = reach;
        reach = 0;
        for (int v = 0; v < n; ++v)
            if (last & (1ll << v))
                reach |= reachable_in_one_step[v];
        if (reach & (1ll << T)) {
            write(path_cnt);
            puts("");
            return 0;
        }
    }
    puts("-1");
    return 0;
}

/*
7 7 4 1 7
1 2
2 3
3 1
2 4
4 5
5 6
6 7 
output: 1
*/