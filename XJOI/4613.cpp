/*************************************
 * @problem:      二维码.
 * @user_name:    brealid.
 * @time:         2020-11-04.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

const int N = 1e5 + 7, P = 998244353;

int64 fpow(int64 a, int n) {
    int64 ret = 1;
    while (n) {
        if (n & 1) ret = ret * a % P;
        a = a * a % P;
        n >>= 1;
    }
    return ret;
}

struct CombinationNumber {
    int64 fac[N], ifac[N];
    CombinationNumber() {
        fac[0] = ifac[0] = 1;
        for (int i = 1; i < N; ++i) fac[i] = fac[i - 1] * i % P;
        ifac[N - 1] = fpow(fac[N - 1], P - 2);
        for (int i = N - 1; i > 1; --i) ifac[i - 1] = ifac[i] * i % P;
    }
    inline int64 operator() (int n, int m) {
        return fac[n] * ifac[m] % P * ifac[n - m] % P;
    }
} comb;

int n, m;

int64 S2(int n, int m) {
    static int64 mem[2007][2007] = {0};
    int64 &ans = mem[n][m];
    if (m <= 0 || n < m) return 0;
    if (ans) return ans;
    if (n == m) return ans = 1;
    return ans = (S2(n - 1, m) * m + S2(n - 1, m - 1)) % P;
}

int64 fac(int n) {
    static int64 mem[2007] = {0};
    int64 &ans = mem[n];
    if (ans) return ans;
    if (n <= 1) return 1;
    return ans = fac(n - 1) * n % P;
}

int64 solve(int n, int m) {
    int64 ans = 0;
    for (int col = 1; col <= n; ++col) // 行的阶梯有多少级
        // fprintf(stderr, "[%d, %d] cnt_ways(n, col) * cnt_ways(m, ln) = %lld * %lld = %lld\n", col, ln, cnt_ways(n, col), cnt_ways(m, ln), cnt_ways(n, col) * cnt_ways(m, ln));
        // ans += cnt_ways(n, col) * cnt_ways(m, ln) % P;
        if ((n & 1) == (col & 1)) ans += S2(n, col) * fac(col) % P * fpow(col + 1, m) % P;
        else ans += S2(n, col) * fac(col) % P * fpow(col + 1, m) % P * (P - 1) % P;
    return ans % P;
}

signed main() {
    read >> n >> m;
    write << solve(n, m) << '\n';
    return 0;
}