/*************************************
 * user name:    Jmoo.
 * language:     C++.
 * upload place: XJOI (dev.xjoi.net).
*************************************/ 
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, sum;
struct Edge {
    int b, w;
};
vector<Edge> G[105 + 7];
int size[105 + 7];

int dfs(int u, int fa)
{
    size[u] = 1;
    int res = 0;
    for (unsigned i = 0; i < G[u].size(); i++) {
        if (G[u][i].b != fa) {
            res = max(res, dfs(G[u][i].b, u) + G[u][i].w);
            size[u] += size[G[u][i].b];
        }
    }
    return res;
}

int dfs2(int u, int fa)
{
    size[u] = 1;
    int res[105 + 7], cnt = 0;
    for (unsigned i = 0; i < G[u].size(); i++) {
        if (G[u][i].b != fa) {
            res[++cnt] = dfs(G[u][i].b, u) + G[u][i].w; 
            size[u] += size[G[u][i].b];
        }
    }
    return res;
}

int main()
{
    n = read<int>();
    int a, b, w;
    for (int i = 1; i < n; i++) {
        a = read<int>();
        b = read<int>();
        w = read<int>();
        G[a].push_back((Edge){b, w});
        G[b].push_back((Edge){a, w});
        sum += w;
    }
    // printf("sum = %d.\n", sum);
    write(dfs2(1, 0) - dfs(1, 0));
    return 0;
}