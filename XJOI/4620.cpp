/*************************************
 * @problem:      The Lost.
 * @user_name:    brealid.
 * @time:         2020-11-06.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

void File_IO_init(string file_name) {
    freopen((file_name + ".in" ).c_str(), "r", stdin);
    freopen((file_name + ".out").c_str(), "w", stdout);
}

const int N = 1e7 + 7;
int n, L, X, Y;

namespace data {
    uint64 rand_u64(uint64 &A, uint64 &B) {
        uint64 T = A, S = B;
        A = S;
        T ^= T << 23;
        T ^= T >> 17;
        T ^= S ^ (S >> 26);
        B = T;
        return T + S;
    }
    uint64 A, B;
    void gen_nxt(int &l, int &r) {
        l = rand_u64(A, B) % L + X;
        r = rand_u64(A, B) % L + Y;
        if (l > r) swap(l, r);
    }
}

int l[N], r[N], klit[N];

void bf_solve() {
    for (int i = 1; i <= n; ++i) data::gen_nxt(l[i], r[i]);
    int lst_l[N], lst_r[N];
    for (int tim = 1, used = 0; tim <= n && used < n; ++tim) {
        for (int i = 1; i <= n; ++i) lst_l[i] = l[i], lst_r[i] = r[i];
        for (int i = 1; i <= n; ++i) {
            l[i] += max(max(lst_l[i - 1] - lst_l[i], lst_l[i + 1] - lst_l[i]), 1);
            r[i] -= max(max(lst_r[i] - lst_r[i - 1], lst_r[i] - lst_r[i + 1]), 1);
            if (l[i] > r[i] && !klit[i]) (klit[i] = tim), ++used;
        }
    }
    int64 ans = 0, power3 = 1;
    for (int i = 1; i <= n; ++i) {
        // write << klit[i] << '\n';
        ans = (ans + klit[i] * power3) % 998244353;
        power3 = power3 * 3 % 998244353;
    }
    write << ans << '\n';
}

signed main() {
    File_IO_init("lost");
    read >> n >> L >> X >> Y >> data::A >> data::B;
    if (n <= 3000 || true) return bf_solve(), 0;
    for (int i = 1; i <= n; ++i) {
        data::gen_nxt(l[i], r[i]);
        klit[i] = (r[i] - l[i] + 2) >> 1;
        // write << l[i] << ' ' << r[i] << '\n';
    }
    for (int i = 1; i <= n; ++i) {
        if (r[i - 1] < l[i] || l[i - 1] > r[i]) klit[i] = 1;
        else {
            klit[i] = min(klit[i], (r[i] - l[i] + 1 - max(l[i - 1] - l[i], 1) - max(r[i] - r[i - 1], 1) + 1) / 2 + 1);
            klit[i] = min(klit[i], klit[i - 1] + 1);
        }
    }
    for (int i = n; i >= 1; --i) {
        if (r[i + 1] < l[i] || l[i + 1] > r[i]) klit[i] = 1;
        else {
            klit[i] = min(klit[i], (r[i] - l[i] + 1 - max(l[i + 1] - l[i], 1) - max(r[i] - r[i + 1], 1) + 1) / 2 + 1);
            klit[i] = min(klit[i], klit[i + 1] + 1);
        }
    }
    int64 ans = 0, power3 = 1;
    for (int i = 1; i <= n; ++i) {
        // write << klit[i] << '\n';
        ans = (ans + klit[i] * power3) % 998244353;
        power3 = power3 * 3 % 998244353;
    }
    write << ans << '\n';
    return 0;
}

/*
6 10
7 9
6 7
2 10
4 9
5 9
2 10
9 15
2 7
3 7
7 11
9 15
1 9
1
1
1
2
3
3
2
1
1
1
1
1
1
798565
*/