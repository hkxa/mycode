#include <bits/stdc++.h>
using namespace std;
#define random(x) (rand() % (x) + 1)
#define randLR(l, r) (random((r) - (l) + 1) + (l) - 1)

int main() 
{
    freopen("9138.in", "w", stdout);
    srand(time(0) * clock());
    int n = 100, cnt = 0, opt, x;
    printf("%d\n", n);
    for (int _count = 1; _count <= n; _count++) {
        if (cnt) opt = random(100) <= 87; // 87% opt = 1 
        else opt = 1;
        printf("%d ", opt);
        if (opt == 1) {
            x = random(7) <= 3 ? 1 : randLR(1, n / 23);
            printf("%d ", x);
            cnt++;
        } else cnt--;
        putchar(10);
    }
    return 0;
}