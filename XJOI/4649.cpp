/**
 * Do not mind how heavily it is raining,
 * as there's always sun shinning after the rain passed.
 * 
 * NOIP.rp++;
 */
#include <bits/stdc++.h>

namespace fastio {
    struct Getchar {
        char buf[1000000], *p1, *p2;
        Getchar() : p1(buf), p2(buf) {}
        char operator() () {
            if (p1 == p2) p2 = buf + fread(p1 = buf, 1, 1000000, stdin);
            return p1 == p2 ? EOF : *p1++;
        }
    } getchar;
    struct Putchar {
        char buf[1000000], *p1, *p2;
        Putchar() : p1(buf), p2(buf + 1000000) {}
        ~Putchar() { fwrite(buf, 1, p1 - buf, stdout); }
        void operator() (char ch) {
            if (p1 == p2) fwrite(p1 = buf, 1, 1000000, stdout);
            *p1++ = ch;
        }
    } putchar;
    template<typename I> inline void get_uint(I &x) { 
        static char ch = 0;
        while (!isdigit(ch = getchar()));
        x = ch & 15;
        while (isdigit(ch = getchar())) x = (((x << 2) + x) << 1) + (ch & 15);
    }
    template<typename I> inline void attach_uint(I x) { 
        static char buf[23];
        static int top = 0;
        if (x < 0) putchar('-'), x = -x;
        do {
            buf[++top] = '0' | (x % 10);
            x /= 10;
        } while (x);
        while (top) putchar(buf[top--]);
    }
    inline void attach_str(const char *str) { 
        while (*str) putchar(*str++);
    }
}

const int N = 1e5 + 7;

int n;
int w[N];
bool connected[N];
std::priority_queue<std::pair<int, int> > q;
std::unordered_set<int> G[N];

struct UnionFindSet {
    int fa[N];
    UnionFindSet() {
        memset(fa, -1, sizeof(fa));
    }
    int find(int u) {
        return fa[u] < 0 ? u : fa[u] = find(fa[u]);
    }
    inline bool merge(int u, int v) {
        u = find(u), v = find(v);
        if (u == v) return false;
        if (fa[u] > fa[v]) std::swap(u, v);
        fa[u] += fa[v];
        fa[v] = u;
        w[u] = std::max(w[u], w[v]);
        return true;
    }
} ufs;

int main() {
    fastio::get_uint(n);
    for (int i = 1; i <= n; ++i) fastio::get_uint(w[i]), q.push(std::make_pair(-w[i], i));
    for (int i = 1, u, v; i < n; ++i) {
        fastio::get_uint(u), fastio::get_uint(v);
        G[u].insert(v), G[v].insert(u);
    }
    long long ans = 0;
    while (!q.empty()) {
        int u = q.top().second;
        connected[u] = true;
        q.pop();
        for (const int &x : G[u])
            if (connected[x]) {
                ans += w[u] + w[ufs.find(x)];
                ufs.merge(u, ufs.find(x));
            }
    }
    fastio::attach_uint(ans);
    fastio::putchar('\n');
    return 0;
}