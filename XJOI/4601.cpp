#include <bits/stdc++.h>
using namespace std;

#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;

typedef long long int64;
typedef unsigned long long uint64;

template <typename Int>
Int read() {
    Int d = 0;
    bool flag = 0;
    char ch = getchar();
    while ((ch < '0' || ch > '9') && ch != '-' && ch != EOF) ch = getchar();
    if (ch == '-') flag = 1, ch = getchar();
    d = ch & 15;
    while ((ch = getchar()) >= '0' && ch <= '9') d = (d << 3) + (d << 1) + (ch & 15);
    return flag ? -d : d;
}

template <typename Int>
void write(Int x) {
    static char buffer[33];
    static int top = 0;
    if (!x) {
        putchar('0');
        return;
    }
    if (x < 0) putchar('-'), x = -x;
    while (x) {
        buffer[++top] = '0' | (x % 10);
        x /= 10;
    }
    while (top) putchar(buffer[top--]);
}

const int N = 2e5 + 7;

int n, k, fa[N];
vector<int> G[N];
int dep[N];
priority_queue<pair<int, int> > q;

void predfs(int u) {
    dep[u] = dep[fa[u]] + 1;
    q.push(make_pair(dep[u], u));
    for (size_t i = 0; i < G[u].size(); ++i) {
        int v = G[u][i];
        if (v != fa[u]) predfs(G[u][i]);
    }
}

int vis[N];
void remove(int u, int from, int d) {
    if (vis[u] >= d) return;
    vis[u] = d;
    if (d == 1) return;
    for (size_t i = 0; i < G[u].size(); ++i) {
        int v = G[u][i];
        if (v == from) continue;
        remove(G[u][i], u, d - 1);
    }
}

signed main() {
    n = read<int>();
    k = min(read<int>(), n);
    for (int i = 1; i < n; ++i) {
        fa[i] = read<int>();
        G[fa[i]].push_back(i);
        G[i].push_back(fa[i]);
    }
    predfs(0);
    int ans = 0;
    while (!q.empty()) {
        int u = q.top().second;
        q.pop();
        if (vis[u]) continue;
        // printf("%d is choosen\n", u);
        ++ans;
        remove(u, -1, k);
    }
    write(ans);
    putchar(10);
    return 0;
}