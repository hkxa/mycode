/*************************************
 * @problem:      bag.
 * @time:         2020-10-17.
 ************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef long long int64;
typedef unsigned long long uint64;

template <typename Int>
Int read() {
    Int d = 0;
    bool flag = 0;
    char ch = getchar();
    while ((ch < '0' || ch > '9') && ch != '-' && ch != EOF) ch = getchar();
    if (ch == '-') flag = 1, ch = getchar();
    d = ch & 15;
    while ((ch = getchar()) >= '0' && ch <= '9') d = (d << 3) + (d << 1) + (ch & 15);
    return flag ? -d : d;
}

template <typename Int>
void write(Int x) {
    static char buffer[33];
    static int top = 0;
    if (!x) {
        putchar('0');
        return;
    }
    if (x < 0) putchar('-'), x = -x;
    while (x) {
        buffer[++top] = '0' | (x % 10);
        x /= 10;
    }
    while (top) putchar(buffer[top--]);
}

void UpdMax(int &a, const int b) { if (a < b) a = b; }
#define ModAdd(i, j) ((i) + (j) >= m ? (i) + (j) - m : (i) + (j))

const int N = 1e5 + 7, M = 100;

int n, m, q;
int c[N], w[N];
vector<int> G[N];
int fa[N], dep[N], siz[N], wson[N];
int dfn[N], id[N], beg[N], dft;

struct ModStatus {
    int maxw[M];
    void init() {
        memset(maxw, -1, sizeof(maxw));
        maxw[0] = 0;
    }
    ModStatus operator + (const ModStatus &b) const {
        ModStatus ret;
        ret.init();
        for (int i = 0; i < m; ++i)
            if (~maxw[i])
                for (int j = 0; j < m; ++j)
                    if (~b.maxw[j])
                        UpdMax(ret.maxw[ModAdd(i, j)], maxw[i] + b.maxw[j]);
        return ret;
    }
} tr[N << 2], ModStatus_initialV;

void dfs1(int u, int fat) {
    fa[u] = fat;
    dep[u] = dep[fat] + 1;
    siz[u] = 1;
    for (size_t i = 0; i < G[u].size(); ++i) {
        int v = G[u][i];
        if (v == fat) continue;
        dfs1(v, u);
        siz[u] += siz[v];
        if (siz[v] > siz[wson[u]]) wson[u] = v;
    }
}

void dfs2(int u, int cbeg) {
    id[dfn[u] = ++dft] = u;
    beg[u] = cbeg;
    if (!wson[u]) return;
    dfs2(wson[u], cbeg);
    for (size_t i = 0; i < G[u].size(); ++i) {
        int v = G[u][i];
        if (v == fa[u] || v == wson[u]) continue;
        dfs2(v, v);
    }
}

void tr_build(int u, int l, int r) {
    if (l == r) {
        tr[u].init();
        tr[u].maxw[c[id[l]]] = w[id[l]];
        return;
    }
    int mid = (l + r) >> 1, ls = u << 1, rs = ls | 1;
    tr_build(ls, l, mid);
    tr_build(rs, mid + 1, r);
    tr[u] = tr[ls] + tr[rs];
}

ModStatus tr_query(int u, int l, int r, int ml, int mr) {
    if (l > mr || r < ml) return ModStatus_initialV;
    if (l >= ml && r <= mr) return tr[u];
    int mid = (l + r) >> 1;
    return tr_query(u << 1, l, mid, ml, mr) + tr_query(u << 1 | 1, mid + 1, r, ml, mr);
}

int query(int u, int v, int l, int r) {
    // printf("query: chain %d-%d, weight_range[%d, %d]\n       result = {", u, v, l, r);
    ModStatus res;
    res.init();
    while (beg[u] != beg[v]) {
        if (dep[beg[u]] < dep[beg[v]]) swap(u, v);
        res = res + tr_query(1, 1, n, dfn[beg[u]], dfn[u]);
        u = fa[beg[u]];
    }
    if (dep[u] > dep[v]) res = res + tr_query(1, 1, n, dfn[v], dfn[u]);
    else res = res + tr_query(1, 1, n, dfn[u], dfn[v]);
    int ans = -1;
    // for (int i = 0; i < m; ++i) 
    //     printf("%d, ", res.maxw[i]);
    // printf("\b\b }\n");
    for (int i = l; i <= r; ++i) ans = max(ans, res.maxw[i]);
    return ans;
}

// O(n·log^2n·m^2)
signed main() {
    ModStatus_initialV.init();
    n = read<int>();
    m = read<int>();
    q = read<int>();
    for (int i = 1; i <= n; ++i) c[i] = read<int>();
    for (int i = 1; i <= n; ++i) w[i] = read<int>();
    for (int i = 1, u, v; i < n; ++i) {
        u = read<int>();
        v = read<int>();
        G[u].push_back(v);
        G[v].push_back(u);
    }
    dfs1(1, 0);
    dfs2(1, 1);
    tr_build(1, 1, n);
    for (int i = 1, u, v, l, r; i <= q; ++i) {
        u = read<int>();
        v = read<int>();
        l = read<int>();
        r = read<int>();
        write(query(u, v, l, r));
        putchar(10);
    }
    return 0;
}