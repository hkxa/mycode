#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef long long int64;
typedef unsigned long long uint64;

template <typename Int>
Int read() {
    Int d = 0;
    static bool flag;
    static char ch;
    flag = 0;
    ch = getchar();
    while ((ch < '0' || ch > '9') && ch != '-' && ch != EOF) ch = getchar();
    if (ch == '-') flag = 1, ch = getchar();
    d = ch & 15;
    while ((ch = getchar()) >= '0' && ch <= '9') d = (d << 3) + (d << 1) + (ch & 15);
    return flag ? -d : d;
}

template <typename Int>
void write(Int x, char endch = '\0') {
    static char buffer[33];
    static int top = 0;
    if (!x) putchar('0');
    else {
        if (x < 0) putchar('-'), x = -x;
        while (x) {
            buffer[++top] = '0' | (x % 10);
            x /= 10;
        }
        while (top) putchar(buffer[top--]);
    }
    if (endch) putchar(endch);
}

const int N = 1e5 + 7, P = 998244353;


inline int64 fpow(int64 u, int n) {
    int64 ret = 1;
    while (n) {
        if (n & 1) ret = ret * u % P;
        u = u * u % P;
        n >>= 1;
    }
    return ret;
}
#define inv(value) fpow(value, P - 2)

namespace SSI {
    vector<int> operator + (vector<int> a, int b) {
        a.push_back(b);
        return a;
    }

    void divide(int rest, int up_bound, vector<vector<int> > &situation, vector<int> &now) {
        if (!rest) {
            situation.push_back(now);
            // for (size_t i = 0; i < now.size(); ++i)
            //     printf("%d%c", now[i], " \n"[i + 1 == now.size()]);
            return;
        }
        now.push_back(0);
        for (int i = 1; i <= up_bound; ++i) {
            now.back() = i;
            divide(rest - i, min(rest - i, i), situation, now);
        }
        now.pop_back();
    }

    int C[20][20];

    int64 ans[20];

    int64 solve(int n) {
        if (n == 1) return 1;
        int64 &u = ans[n];
        if (~u) return u;
        u = 0;
        vector<vector<int> > situation;
        vector<int> emp;
        divide(n, n - 1, situation, emp);
        for (size_t _i = 0; _i < situation.size(); ++_i) {
            vector<int> &v = situation[_i];
            int64 now = 1;
            int rest = n;
            for (size_t i = 0, conti = 0; i < v.size(); ++i) {
                if (i && v[i] == v[i - 1]) ++conti;
                else conti = 1;
                now = now * C[rest][v[i]] % P * solve(v[i]) % P * inv(conti) % P;
                rest -= v[i];
            }
            u = (u + now) % P;
        }
        return u;
    }

    int sll(int n) {
        memset(ans, -1, sizeof(ans));
        C[0][0] = 1;
        for (int i = 1; i <= n; ++i) {
            C[i][0] = 1;
            for (int j = 1; j <= n; ++j)
                C[i][j] = C[i - 1][j - 1] + C[i - 1][j];
        }
        if (n == 1) puts("1");
        else write(solve(n) * 2 % P, '\n');
        return 0;
    }
}

namespace pts_n2 {
    int64 fac[N], ifac[N];
    void init() {
        fac[0] = 1;
        for (int i = 1; i < N; ++i) fac[i] = fac[i - 1] * i % P;
        ifac[N - 1] = inv(fac[N - 1]);
        for (int i = N - 1; i >= 1; --i) ifac[i - 1] = ifac[i] * i % P;
    }
    int64 calc_C(int n, int m) {
        return fac[n] * ifac[m] % P * ifac[n - m] % P;
    }
    int sll(int n) {
        init();
        int64 a[N] = {0};
        a[1] = 1;
        for (int i = 2; i <= n; ++i) {
            for (int j = 1; j + j < i; ++j)
                a[i] = (a[i] + calc_C(i, j) * a[j] % P * a[i - j] % P) % P;
            a[i] = (a[i] * 2 + a[i - 1] * (P - (i - 1))) % P;
            if (i % 2 == 0) a[i] = (a[i] + calc_C(i, i >> 1) * a[i >> 1] % P * a[i >> 1] % P) % P;
            // printf("a[%d] = %lld\n", i, a[i]);
        }
        write(a[n] * 2 % P, '\n');
        return 0;
    }
}

signed main() {
    int n = read<int>();
    // pts_n2::sll(n);
    if (n <= 10) return SSI::sll(n);
    pts_n2::sll(n);
    return 0;
}