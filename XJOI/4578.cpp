/*************************************
 * @problem:      tree.
 * @time:         2020-10-18.
 ************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef long long int64;
typedef unsigned long long uint64;

template <typename Int>
Int read() {
    Int d = 0;
    bool flag = 0;
    char ch = getchar();
    while ((ch < '0' || ch > '9') && ch != '-' && ch != EOF) ch = getchar();
    if (ch == '-') flag = 1, ch = getchar();
    d = ch & 15;
    while ((ch = getchar()) >= '0' && ch <= '9') d = (d << 3) + (d << 1) + (ch & 15);
    return flag ? -d : d;
}

template <typename Int>
void write(Int x) {
    static char buffer[33];
    static int top = 0;
    if (!x) {
        putchar('0');
        return;
    }
    if (x < 0) putchar('-'), x = -x;
    while (x) {
        buffer[++top] = '0' | (x % 10);
        x /= 10;
    }
    while (top) putchar(buffer[top--]);
}

const int N = 2e5 + 7;

int n, a[N];
int ok[1007][1007];
int occured_count[N];
int64 ans = 0;

bool check(int l, int r) {
    if (l > r) return true;
    int &u = ok[l][r];
    if (~u) return u;
    if (a[l] != a[r]) return u = false;
    u = true;
    set<int> cant_re;
    for (int i = l + 1, las = l; i <= r && u; ++i) {
        if (a[i] == a[l]) {
            u &= check(las + 1, i - 1);
            for (int j = las + 1; j < i && u; ++j)
                if (cant_re.count(a[j])) u = false;
            for (int j = las + 1; j < i && u; ++j)
                cant_re.insert(a[j]);
            las = i;
        }
    }
    return u;
}

vector<int> G[N];
int siz[N];

int build_tree(int u, int p) {
    siz[u] = 1;
    while (--occured_count[u]) {
        int v = a[p + 1];
        G[u].push_back(v);
        p = build_tree(v, p + 1);
        siz[u] += siz[v];
    }
    return p + 1;
}

void solve_tree(int u) {
    ans += (int64)G[u].size() * ((int64)G[u].size() + 1) / 2;
    for (size_t i = 0; i < G[u].size(); ++i) {
        int v = G[u][i];
        solve_tree(v);
    }
}

signed main() {
    memset(ok, -1, sizeof(ok));
    n = read<int>();
    for (int i = 1; i <= n; ++i) ++occured_count[a[i] = read<int>()];
    if (n <= 100) {
        for (int i = 1; i <= n; ++i) ok[i][i] = 1;
        for (int i = 1; i <= n; ++i)
            for (int j = i; j <= n; j += 2)
                if (check(i, j)) ++ans;
    } else {
        build_tree(a[1], 1);
        // for (int i = 1; i <= n; ++i) {
        //     printf("node %d's sons: ", i);
        //     for (size_t j = 0; j < G[i].size(); ++j)
        //         printf("%d ", G[i][j]);
        //     putchar(10);
        // }
        ans = n;
        solve_tree(a[1]);
    }
    write(ans);
    putchar(10);
    return 0;
}