/*************************************
 * @problem:      sign.
 * @time:         2020-10-17.
 ************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef long long int64;
typedef unsigned long long uint64;

template <typename Int>
Int read() {
    Int d = 0;
    bool flag = 0;
    char ch = getchar();
    while ((ch < '0' || ch > '9') && ch != '-' && ch != EOF) ch = getchar();
    if (ch == '-') flag = 1, ch = getchar();
    d = ch & 15;
    while ((ch = getchar()) >= '0' && ch <= '9') d = (d << 3) + (d << 1) + (ch & 15);
    return flag ? -d : d;
}

template <typename Int>
void write(Int x) {
    static char buffer[33];
    static int top = 0;
    if (!x) {
        putchar('0');
        return;
    }
    if (x < 0) putchar('-'), x = -x;
    while (x) {
        buffer[++top] = '0' | (x % 10);
        x /= 10;
    }
    while (top) putchar(buffer[top--]);
}

// #define int int64
const int N = 3e3 + 7, P = 998244353;

int64 fpow(int64 u, int n) {
    int64 ret = 1;
    while (n) {
        if (n & 1) ret = ret * u % P;
        u = u * u % P;
        n >>= 1;
    }
    return ret;
}
#define inv(xxx) fpow(xxx, P - 2)

int n, m;
vector<pair<int, int> > G[N];
int fa[N], dep[N], siz[N], wson[N];
int64 dis[N];
int dfn[N], id[N], beg[N], dft;

void dfs1(int u, int fat) {
    fa[u] = fat;
    dep[u] = dep[fat] + 1;
    siz[u] = 1;
    for (size_t i = 0; i < G[u].size(); ++i) {
        int v = G[u][i].first;
        if (v == fat) continue;
        dis[v] = dis[u] + G[u][i].second;
        dfs1(v, u);
        siz[u] += siz[v];
        if (siz[v] > siz[wson[u]]) wson[u] = v;
    }
}

void dfs2(int u, int cbeg) {
    id[dfn[u] = ++dft] = u;
    beg[u] = cbeg;
    if (!wson[u]) return;
    dfs2(wson[u], cbeg);
    for (size_t i = 0; i < G[u].size(); ++i) {
        int v = G[u][i].first;
        if (v == fa[u] || v == wson[u]) continue;
        dfs2(v, v);
    }
}

int get_lca(int u, int v) {
    while (beg[u] != beg[v]) {
        if (dep[beg[u]] < dep[beg[v]]) v = fa[beg[v]];
        else u = fa[beg[u]];
    }
    if (dep[u] > dep[v]) return v;
    else return u;
}

int64 get_dist(int u, int v) {
    return dis[u] + dis[v] - 2 * dis[get_lca(u, v)];
}

namespace solve_bf1_base {
    int64 invN_2M;
    int64 ans = 0, current;
    int a[11], b[11];
    int total_now = 0;
    void sol(int u, int64 now = 0) {
        if (u > m) current = max(current, now);
        else for (int i = 1; i <= m; ++i)
            if (a[i]) {
                int t = a[i];
                a[i] = 0;
                sol(u + 1, now + get_dist(t, b[u]));
                a[i] = t;
            }
    }
    void dfsB(int u) {
        if (u > m) {
            current = 0;
            sol(1);
            ans = (ans + current) % P;
            // if (++total_now % 16384 == 0) printf("%.3lf%%\r", total_now / (3125 * 3125.0) * 100);
            // printf("%d %d : %lld\n", a[1], b[1], current);
        } else for (int i = 1; i <= n; ++i) {
            b[u] = i;
            dfsB(u + 1);
        }
    }
    void dfsA(int u) {
        if (u > m) dfsB(1);
        else for (int i = 1; i <= n; ++i) {
            a[u] = i;
            dfsA(u + 1);
        }
    }
}

int solve_bf1() {
    solve_bf1_base::dfsA(1);
    write(solve_bf1_base::ans * inv(fpow(n, m * 2)) % P);
    putchar(10);
    return 0;
}

int solve_m1() {
    int64 ans = 0;
    for (int i = 1; i <= n; ++i)
        for (int j = 1; j <= n; ++j)
            ans = (ans + get_dist(i, j)) % P;
    write(ans * inv(n * n) % P);
    putchar(10);
    return 0;
}

signed main() {
    n = read<int>();
    m = read<int>();
    for (int i = 1, u, v, w; i < n; ++i) {
        u = read<int>();
        v = read<int>();
        w = read<int>();
        G[u].push_back(make_pair(v, w));
        G[v].push_back(make_pair(u, w));
    }
    dfs1(1, 0);
    dfs2(1, 1);
    if (m == 1) return solve_m1();
    if (n <= 5 && m <= 5) return solve_bf1();
    return 0;
}