/*************************************
 * @problem:      Exam Results.
 * @time:         2020-10-24.
 ************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef long long int64;
typedef unsigned long long uint64;

template <typename Int>
Int read() {
    Int d = 0;
    bool flag = 0;
    char ch = getchar();
    while ((ch < '0' || ch > '9') && ch != '-' && ch != EOF) ch = getchar();
    if (ch == '-') flag = 1, ch = getchar();
    d = ch & 15;
    while ((ch = getchar()) >= '0' && ch <= '9') d = (d << 3) + (d << 1) + (ch & 15);
    return flag ? -d : d;
}

template <typename Int>
void write(Int x) {
    static char buffer[33];
    static int top = 0;
    if (!x) {
        putchar('0');
        return;
    }
    if (x < 0) putchar('-'), x = -x;
    while (x) {
        buffer[++top] = '0' | (x % 10);
        x /= 10;
    }
    while (top) putchar(buffer[top--]);
}

const int N = 2e5 + 7;

template<typename T>
class BalanceTree {
  private:  
    struct ScapegoatNode {
        T v;
        int tot, siz, l, r;
    } t[N * 2];
    int root;
    int temp[N * 2];
    int TempCnt, NodeCnt;

    bool Balance(int x) {
        return t[t[x].l].siz * 4 < t[x].siz * 3 && 
               t[t[x].r].siz * 4 < t[x].siz * 3;
    }
    void Flatten(int x) {
        if (!x) return;
        Flatten(t[x].l);
        if (t[x].tot) temp[++TempCnt] = x;
        Flatten(t[x].r);
    }
    void Do_ReBuild(int &x, int l, int r) {
        if (l > r) return void(x = 0);
        if (l == r) {
            x = temp[l];
            t[x].l = t[x].r = 0;
            t[x].siz = t[x].tot;
            return;
        }
        int mid = (l + r) >> 1;
        x = temp[mid];
        Do_ReBuild(t[x].l, l, mid - 1);
        Do_ReBuild(t[x].r, mid + 1, r);
        t[x].siz = t[t[x].l].siz + t[t[x].r].siz + t[x].tot;
    }
    void ReBuild(int &x) {
        TempCnt = 0;
        Flatten(x);
        Do_ReBuild(x, 1, TempCnt);
    }
    void Check(int &a) {
        if (!a) return;
        if (!Balance(a)) ReBuild(a);
        else if (t[t[a].l].siz > t[t[a].r].siz) Check(t[a].l);
        else Check(t[a].r);
    }
    void Insert(int &a, T v) {
        if (!a) {
            a = ++NodeCnt;
            t[a].v = v;
            t[a].tot = 1;
        } else if (v == t[a].v) {
            ++t[a].tot;
        } else if (v < t[a].v) {
            Insert(t[a].l, v);
        } else {
            Insert(t[a].r, v);
        }
        ++t[a].siz;
    }
    bool Delete(int a, T v) {
        if (!a) return false;
        bool is_succeed = false;
        if (v == t[a].v) {
            if (t[a].tot) {
                --t[a].tot;
                is_succeed = true;
            }
        } else if (v < t[a].v) {
            if (Delete(t[a].l, v)) is_succeed = true;
        } else {
            if (Delete(t[a].r, v)) is_succeed = true;
        }
        if (is_succeed) --t[a].siz;
        return is_succeed;
    }
    T GetVal(int rank) {
        int a = root;
        while (true) {
            if (t[t[a].l].siz < rank && t[t[a].l].siz + t[a].tot >= rank) return t[a].v;
            else if (t[t[a].l].siz >= rank) a = t[a].l;
            else rank -= t[t[a].l].siz + t[a].tot, a = t[a].r;
        }
    }
  public:
    void insert(T val) {
        Insert(root, val);
        Check(root);
    }
    bool remove(T val) {
        if (Delete(root, val)) {
            Check(root);
            return true;
        } else return false;
    }
    T get_kth(int k) {
        return GetVal(k);
    }
    int size() {
        return t[root].siz;
    }
};

struct student {
    int id, now;
    bool need_remove_last;
    student(int I, int V, bool T) : id(I), now(V), need_remove_last(T) {}
    bool operator < (const student &stu_b) const {
        return now > stu_b.now;
    }
};

int n, p;
int a[N], b[N];
int now_max_score;

BalanceTree<int> btr;
priority_queue<student> q;

void deal_and_updateBtr() {
    student stu = q.top();
    q.pop();
    now_max_score = max(now_max_score, stu.now);
    if (stu.need_remove_last) btr.remove(b[stu.id]);
    btr.insert(stu.now);
}

void maintain() {
    int lim = ((int64)now_max_score * p + 99) / 100;
    while (btr.get_kth(1) < lim)
        btr.remove(btr.get_kth(1));
}

signed main() {
    n = read<int>();
    p = read<int>();
    for (int i = 1; i <= n; ++i) {
        a[i] = read<int>();
        b[i] = read<int>();
        if (a[i] < b[i]) swap(a[i], b[i]);
        q.push(student(i, b[i], false));
        if (a[i] != b[i]) q.push(student(i, a[i], true));
    }
    now_max_score = *max_element(b + 1, b + n + 1);
    while (!q.empty() && q.top().now <= now_max_score) deal_and_updateBtr();
    maintain();
    int ans = btr.size();
    while (!q.empty()) {
        deal_and_updateBtr();
        maintain();
        ans = max(ans, btr.size());
    }
    write(ans);
    putchar(10);
    return 0;
}