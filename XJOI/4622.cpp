/*************************************
 * @problem:      高斯整数.
 * @user_name:    brealid.
 * @time:         2020-11-05.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

// 高斯整数 唯一地表示为若干个不同的 -1+i 的幂次之和

inline void div_unit(int &a, int &b) {
    static int ta, tb;
    ta = a, tb = b;
    a = (-ta + tb) >> 1, b = (-ta - tb) >> 1;
}

inline int count_divide(int a, int b) {
    int res(0);
    while (a || b)
        if ((a ^ b) & 1) ++res, --a;
        else div_unit(a, b);
    return res;
}

#define check(a, b) (!(count_divide((a), (b)) & 1))
#define range_cnt(l, r) ((int64)((r) - (l) + 2) >> 1)
#define len(a, b) ((int64)(b) - (a) + 1)
#define chk_eq1(a, b) (!(((a) - (b)) & 1))

int64 work(int x, int d, int u) {
    if (d > u) return 0;
    int flag = 0, res = 0;
    if (!chk_eq1(x, u)) --x, flag = true;
    if (x & 1) ++x, --d, --u, flag = !flag;
    int y = x >> 1, l = -(u >> 1), r = -(d >> 1);
    if (!chk_eq1(l, y)) res += check(l, y), ++l;
    if (chk_eq1(r, y)) res += check(r, y), --r;
    res += len(l, r) >> 1;
    return flag ? range_cnt(d, u) - res : res;
}

int64 solve(int l1, int r1, int l2, int r2) {
    int lu = r2 - chk_eq1(l1, r2),
        ru = r2 - !chk_eq1(r1, r2),
        ld = l2 + chk_eq1(l1, l2),
        rd = l2 + !chk_eq1(r1, l2);
    // printf("(%lld -%lld -%lld)/2 %lld %lld\n", len(l1, r1) * len(l2, r2), range_cnt(ld, lu), range_cnt(rd, ru), work(l1, ld, lu), work(r1, rd, ru));
    return (len(l1, r1) * len(l2, r2) - range_cnt(ld, lu) - range_cnt(rd, ru)) / 2
         + work(l1, ld, lu) + work(r1, rd, ru);
}

signed main() {
    int T, l1, r1, l2, r2;
    read >> T;
    while (T--) {
        read >> l1 >> r1 >> l2 >> r2;
        write << solve(l1, r1, l2, r2) << '\n';
    }
    return 0;
}

/*
3
-1 3 -1 4
0 0 2 2
0 0 1 10

(30 -3 -3)/2 1 2
(1 -0 -1)/2 0 1
(10 -5 -5)/2 3 2

(30 --2 --2)/2 0 0
(1 -0 -1)/2 0 1
(10 --4 --4)/2 0 0
*/