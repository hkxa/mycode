/*************************************
 * @problem:      分蛋糕.
 * @user_name:    brealid.
 * @time:         2020-11-04.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    } write;
}
using namespace Fastio;

const int N = 1e5 + 7;

int n, m;
vector<int> G[N];
int ind[N], col[N];
int q[N], l = 1, r = 0;

inline void del_node(int x) {
    for (size_t i = 0; i < G[x].size(); ++i)
        if (--ind[G[x][i]] == 3) q[++r] = G[x][i];
}

signed main() {
    read >> n >> m;
    for (int i = 1, u, v; i <= m; ++i) {
        read >> u >> v;
        G[u].push_back(v);
        G[v].push_back(u);
    }
    for (int i = 1; i <= n; ++i) {
        ind[i] = G[i].size();
        if (ind[i] <= 3) q[++r] = i;
    }
    while (l <= r) del_node(q[l++]);
    col[q[n]] = 1;
    int cnt[3];
    // for (int i = 1; i <= r; ++i)
    //     printf("q[%d] = %d\n", i, q[i]);
    for (int p = n - 1; p >= 1; --p) {
        int u = q[p];
        memset(cnt, 0, sizeof(cnt));
        for (size_t i = 0; i < G[u].size(); ++i) ++cnt[col[G[u][i]]];
        if (cnt[1] < cnt[2]) col[u] = 1;
        else col[u] = 2;
    }
    for (int i = 1; i <= n; ++i)
        write << col[i] << " \n"[i == n];
    return 0;
}

/*
5 6
1 2
2 3
3 4
4 5
1 4
1 5
*/