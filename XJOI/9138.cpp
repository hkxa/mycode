/*************************************
 * user name:    Jmoo.
 * language:     C++.
 * upload place: XJOI (dev.xjoi.net).
*************************************/ 

/**
 * @title 测试数据( @c \n 已转化为 @c whitespace )
 * @input 3 1 10 1 10 1 3
 * @output 1 1 3 
 * @input 5 1 1 1 2 1 1 2 1 2
 * @output 1 2 2 3 
 */

/**
 * @title 简化题意
 * 有 $10^5$ 次操作，每次操作是在字符串（初始为空串）末尾加上一个字符或删去一个字符。    
 * 若操作为加字符，需要输出该字符串增加该字符后的最长公共前后缀。
 * 字符集大小 $10^6$
 */
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;
int opt, x;
int k[300007], nxt[300007], len = -1;
char s[300007]; 

int GetBitNext() {
    if (len == 0) {
        nxt[0] = -1;
        k[0] = 0;
        return -1;
    }
    k[len] = k[len - 1];
    while (true) {
        // printf("k[len] = %d, nxt = %d.\n", k[len], nxt[k[len]]);
        if (k[len] == -1 || s[len] == s[k[len]]) {
            k[len]++;
            nxt[len] = k[len] - 1;
            break;
        } else k[len] = nxt[k[len]];
    }
    return nxt[len];
}

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        opt = read<int>();
        if (opt == 1) {
            x = read<int>(); 
            s[++len] = x;
            write(len - GetBitNext(), 10);
        } else {  
            len--;
        }
    }
    return 0;
}

/*
[a][b][c][a][b][c][a][b][c]
*/