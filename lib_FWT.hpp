namespace FWT {
    void FWT_OR(int *a, int n, int MUL = 1, const int P = 998244353) {
        for (int s = 2, t = 1; s <= n; s <<= 1, t <<= 1)
            for (int i = 0; i < n; i += s)
                for (int j = 0; j < t; j++)
                    a[i + j + t] = (a[i + j + t] + (int64)a[i + j] * MUL) % P;
    }
    void OR(int *a, int *b, int *c, int n, const int P = 998244353) {
        FWT_OR(a, n, 1, P);
        FWT_OR(b, n, 1, P);
        for (int i = 0; i < n; i++) c[i] = (int64)a[i] * b[i] % P;
        FWT_OR(c, n, P - 1, P);
    }
    void FWT_AND(int *a, int n, int MUL = 1, const int P = 998244353) {
        for (int s = 2, t = 1; s <= n; s <<= 1, t <<= 1)
            for (int i = 0; i < n; i += s)
                for (int j = 0; j < t; j++)
                    a[i + j] = (a[i + j] + (int64)a[i + j + t] * MUL) % P;
    }
    void AND(int *a, int *b, int *c, int n, const int P = 998244353) {
        FWT_AND(a, n, 1, P);
        FWT_AND(b, n, 1, P);
        for (int i = 0; i < n; i++) c[i] = (int64)a[i] * b[i] % P;
        FWT_AND(c, n, P - 1, P);
    }
    void FWT_XOR(int *a, int n, int MUL = 1, const int P = 998244353) {
        for (int s = 2, t = 1; s <= n; s <<= 1, t <<= 1)
            for (int i = 0; i < n; i += s)
                for (int j = 0; j < t; j++) {
                    int a0 = a[i + j], a1 = a[i + j + t];
                    a[i + j] = (a0 + a1) % P;
                    a[i + j + t] = (a0 - a1 + P) % P;
                    a[i + j] = (int64)a[i + j] * MUL % P;
                    a[i + j + t] = (int64)a[i + j + t] * MUL % P; 
                }
    }
    void XOR(int *a, int *b, int *c, int n, const int P = 998244353) {
        FWT_XOR(a, n, 1, P);
        FWT_XOR(b, n, 1, P);
        for (int i = 0; i < n; i++) c[i] = (int64)a[i] * b[i] % P;
        FWT_XOR(c, n, fpow(2, P - 2, P), P);
    }
}