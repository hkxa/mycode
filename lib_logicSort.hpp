namespace lib_logicSort {
    void sort3(int &a, int &b, int &c) {
        if (a < b) {
            if (b < c) return;
            else if (a < c) swap(b, c);
            else { int t = c; c = b; b = a; a = t; }
        } else {
            if (c < b) swap(a, c);
            else if (a < c) swap(a, b);
            else { int t = a; a = b; b = c; c = t; }
        }
    }

    void sort4(int &a, int &b, int &c, int &d) {
        sort3(a, b, c);
        if (d < a) { int t = d; d = c; c = b; b = a; a = t; }
        else if (d < b) { int t = d; d = c; c = b; b = t; }
        else if (d < c) swap(c, d);
    }
}