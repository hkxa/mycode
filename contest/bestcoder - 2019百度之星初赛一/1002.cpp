/*************************************
 * problem:      2019BaiduASter1 - P1002.
 * user name:    hkxadpall.
 * time:         2019-08-17.
 * language:     C++.
 * upload place: HDU-BestCoder.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define puts_return(x) { puts(x); return 0; }
#define write_return(x) { write(x); return 0; }

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int l[1007], r[1007];

void Qa2()
{
    int n = read<int>();
    if (n == 1) {
        read<int>();read<int>();
        puts("0");
        return;
    }
    long long ans(0);
    int lp = l[1] = read<int>(), rp = r[1] = read<int>(), pos;
    bool fTag = true, waste = false, lgo;
    for (int i = 2; i <= n; i++) {
        l[i] = read<int>();
        r[i] = read<int>();
        if (fTag) {
            if (max(lp, l[i]) <= min(rp, r[i])) {
                lp = max(lp, l[i]);
                rp = min(rp, r[i]);
            } else {
                fTag = false;
                if (rp < l[i]) {
                    ans = (l[i] - rp + 1) / 2;
                    waste = (l[i] - rp) & 1;
                    lgo = 0;
                    pos = l[i];
                } else {
                    ans = (lp - r[i] + 1) / 2;
                    waste = (l[i] - rp) & 1;
                    lgo = 1;
                    pos = r[i];
                }
            }
        } else {
            if (pos >= l[i] && pos <= r[i]) continue;
            else {
                if (pos > r[i]) {
                    ans += (pos - r[i] + 1 - (lgo && waste)) / 2;
                    waste = (l[i] == r[i]) ? 0 : ((pos - r[i] - (lgo && waste)) & 1);
                    lgo = 1;
                    pos = r[i];
                } else {
                    ans += (l[i] - pos + 1 - (!lgo && waste)) / 2;
                    waste = (l[i] == r[i]) ? 0 : ((l[i] - pos - (!lgo && waste)) & 1);
                    lgo = 0;
                    pos = l[i];
                }
            }
        }
    }
    write(ans, 10);
}

int main()
{
    int T = read<int>();
    while (T--) Qa2();
    return 0;
}