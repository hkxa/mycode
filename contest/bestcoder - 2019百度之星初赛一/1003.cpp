/*************************************
 * problem:      2019BaiduASter初赛1 - P1003 Mindis.
 * user name:    hkxadpall.
 * time:         2019-08-17.
 * language:     C++.
 * upload place: HDU-BestCoder.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define puts_return(x) { puts(x); return 0; }
#define write_return(x) { write(x); return 0; }

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

namespace Problem1003 {
    int n;
    int x1[207], y1[207], x2[207], y2[207];
    int posx[407], xcnt, posy[407], ycnt;
    int sx[407][407], sy[407][407];
    double dis[407][407];
    bool inq[407][407];
    map<int, int> idx, idy;

    struct pos {
        int x, y;
        pos() {}
        pos(int X, int Y) : x(X), y(Y) {}
    };

    int iwi()
    {
        n = read<int>();
        idx.clear();
        idy.clear();
        xcnt = ycnt = 0;
        for (int i = 1; i <= n + 1; i++) {
            x1[i] = read<int>();
            y1[i] = read<int>();
            x2[i] = read<int>();
            y2[i] = read<int>();
            if (idx[x1[i]] == 0) {
                idx[x1[i]] = 1;
                posx[++xcnt] = x1[i];
            } 
            if (idx[x2[i]] == 0) {
                idx[x2[i]] = 1;
                posx[++xcnt] = x2[i];
            } 
            if (idy[y1[i]] == 0) {
                idy[y1[i]] = 1;
                posy[++ycnt] = y1[i];
            } 
            if (idy[y2[i]] == 0) {
                idy[y2[i]] = 1;
                posy[++ycnt] = y2[i];
            } 
        }
        sort(posx + 1, posx + xcnt + 1);
        sort(posy + 1, posy + ycnt + 1);
        idx.clear();
        idy.clear();
        for (int i = 1; i <= xcnt; i++) {
            idx[posx[i]] = i;
            // printf("x %d -> %d\n", i, posx[i]);
        }
        for (int i = 1; i <= ycnt; i++) {
            idy[posy[i]] = i;
            // printf("y %d -> %d\n", i, posy[i]);
        }
        memset(sx, 0, sizeof(sx));
        memset(sy, 0, sizeof(sy));
        memset(inq, 0, sizeof(inq));
        for (int i = 1; i <= n; i++) {
            x1[i] = idx[x1[i]];
            y1[i] = idy[y1[i]];
            x2[i] = idx[x2[i]];
            y2[i] = idy[y2[i]];
            sx[x1[i]][y1[i]]++;
            sx[x1[i]][y2[i]]--;
            sx[x2[i] + 1][y1[i]]--;
            sx[x2[i] + 1][y2[i]]++;
            sy[x1[i]][y1[i]]++;
            sy[x1[i]][y2[i] + 1]--;
            sy[x2[i]][y1[i]]--;
            sy[x2[i]][y2[i] + 1]++;
            // cnt[x1[i]][y1[i]]++;
            // cnt[x1[i]][y2[i]]--;
            // cnt[x2[i]][y1[i]]--;
            // cnt[x2[i]][y2[i]]++;
        }
        x1[n + 1] = idx[x1[n + 1]];
        y1[n + 1] = idy[y1[n + 1]];
        x2[n + 1] = idx[x2[n + 1]];
        y2[n + 1] = idy[y2[n + 1]];
        // printf("-------\n");
        for (int i = 1; i <= xcnt; i++) {
            for (int j = 1; j <= ycnt; j++) {
                dis[i][j] = 1e15;
                // printf("%d%c", cnt[i][j], " \n"[j == ycnt]);
                sx[i][j] += sx[i - 1][j] + sx[i][j - 1] - sx[i - 1][j - 1];
                sy[i][j] += sy[i - 1][j] + sy[i][j - 1] - sy[i - 1][j - 1];
            }
        }
        // printf("-------\n");
        for (int i = 1; i <= xcnt; i++) {
            for (int j = 1; j <= ycnt; j++) {
                sx[i][j]++;
                sy[i][j]++;
                // printf("[%d, %d]%c", sx[i][j], sy[i][j], " \n"[j == ycnt]);
            }
        }
        queue<pos> q;
        q.push(pos(x1[n + 1], y1[n + 1]));
        dis[x1[n + 1]][y1[n + 1]] = 0;
        inq[x1[n + 1]][y1[n + 1]] = 1;
        while (!q.empty()) {
            int x = q.front().x, y = q.front().y;
            // printf("pop (%d, %d) [dis = %.5lf]\n", x, y, dis[x][y]);
            q.pop();
            inq[x][y] = false;
            if (x > 1) {
                if (dis[x - 1][y] > dis[x][y] + (double)(posx[x] - posx[x - 1]) / sx[x - 1][y]) {
                    dis[x - 1][y] = dis[x][y] + (double)(posx[x] - posx[x - 1]) / sx[x - 1][y];
                    if (!inq[x - 1][y]) {
                        q.push(pos(x - 1, y));
                        inq[x - 1][y] = true;
                    }
                }
            }
            if (x < xcnt) {
                if (dis[x + 1][y] > dis[x][y] + (double)(posx[x + 1] - posx[x]) / sx[x][y]) {
                    dis[x + 1][y] = dis[x][y] + (double)(posx[x + 1] - posx[x]) / sx[x][y];
                    if (!inq[x + 1][y]) {
                        q.push(pos(x + 1, y));
                        inq[x + 1][y] = true;
                    }
                }
            }
            if (y > 1) {
                if (dis[x][y - 1] > dis[x][y] + (double)(posy[y] - posy[y - 1]) / sy[x][y - 1]) {
                    dis[x][y - 1] = dis[x][y] + (double)(posy[y] - posy[y - 1]) / sy[x][y - 1];
                    if (!inq[x][y - 1]) {
                        q.push(pos(x, y - 1));
                        inq[x][y - 1] = true;
                    }
                }
            }
            if (y < ycnt) {
                if (dis[x][y + 1] > dis[x][y] + (double)(posy[y + 1] - posy[y]) / sy[x][y]) {
                    dis[x][y + 1] = dis[x][y] + (double)(posy[y + 1] - posy[y]) / sy[x][y];
                    if (!inq[x][y + 1]) {
                        q.push(pos(x, y + 1));
                        inq[x][y + 1] = true;
                    }
                }
            }
        }
        printf("%.5lf", dis[x2[n + 1]][y2[n + 1]]);
        return 0;
    }
}

int main()
{
    int T = read<int>();
    while (T--) Problem1003::iwi();    
    return 0;
}
/*
.
 .
  .
   .
   */