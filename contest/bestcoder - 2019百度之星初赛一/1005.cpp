/*************************************
 * problem:      2019BaiduASter初赛1 - P1005.
 * user name:    hkxadpall.
 * time:         2019-08-17.
 * language:     C++.
 * upload place: HDU-BestCoder.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define puts_return(x) { puts(x); return 0; }
#define write_return(x) { write(x); return 0; }

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int main()
{
    int T = read<int>();
    int64 n;
    while (T--) {
        n = read<int64>();
        switch (n % 6) {
            case 0 :
            case 2 :
                write(n / 2, 10);
                break;
            case 1 : 
                write(n * 2 / 3 + 1, 10);
                break;
            case 3 : 
            case 5 : 
                write(n / 6, 10);
                break;
            case 4 : 
                write(n - 1, 10);
                break;
        }
    }
    return 0;
}
/*
/2
(id-1)/1.5+1
/2
/6 [floor(*ans)]
-1
/6 [floor(*ans)]
*/