/*************************************
 * problem:      2019BaiduASter初赛1 - P1001.
 * user name:    hkxadpall.
 * time:         2019-08-17.
 * language:     C++.
 * upload place: HDU-BestCoder.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define puts_return(x) { puts(x); return 0; }
#define write_return(x) { write(x); return 0; }

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int f[1007], g[1007];

int gcd(int a, int b)
{
    return b ? gcd(b, a % b) : a;
}

struct F {
    int s, f;
    void redu() {
        if (s == 0) {
            if (f) f = 1;
            return;
        }
        if (f == 0) {
            if (s) s = 1;
            return;
        }
        int both = gcd(s, f);
        // printf("gcd(%d, %d) = %d.\n", s, f, both);
        s /= both;
        f /= both;
    }
    bool operator != (const F &e) const {
        if (s == 0 && f == 0) return false;
        if (e.s == 0 && e.f == 0) return false;
        return s != e.s || f != e.f;
    }
};

void p()
{
    int n = read<int>();
    for (int i = 1; i <= n; i++) {
        f[i] = read<int>();
    }
    for (int i = 1; i <= n; i++) {
        g[i] = read<int>();
    }
    int P = n;
    while (f[P] == 0 && g[P] == 0) {
        P--;
    }
    F ans;
    ans.s = f[P];
    ans.f = g[P];
    ans.redu();
    printf("%d/%d\n", ans.s, ans.f);
    // F o = (F){f[1], g[1]}, t;
    // o.redu();
    // for (int i = 2; i <= n; i++) {
    //     t = (F){f[i], g[i]};
    //     t.redu();
    //     if (o != t) {
    //         if (i == n) {
    //             puts("1/1");
    //             return;
    //         }
    //         if (o.s == o.f) {
    //             puts(f[i] > g[i] ? "1/0" : "0/1");
    //             return;
    //         } else {
    //             puts(f[1] > g[1] ? "1/0" : "0/1");
    //             return;
    //         }
    //     }
    // }
    // if (o.s || o.f) printf("%d/%d\n", o.s, o.f);
    // else printf("%d/%d\n", t.s, t.f);
}

int main()
{
    int T = read<int>();
    while (T--) p();
    return 0;
}