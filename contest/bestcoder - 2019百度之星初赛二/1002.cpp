/*************************************
 * problem:      2019BaiduASter1 - P1002.
 * user name:    hkxadpall.
 * time:         2019-08-17.
 * language:     C++.
 * upload place: HDU-BestCoder.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define puts_return(x) { puts(x); return 0; }
#define write_return(x) { write(x); return 0; }

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, m;
char a[20 + 3][50 + 3];
char b[20 + 3][50 + 3];
int p[50 + 3];
int buf[26];

bool chk(int strId)
{
    memset(buf, 0, sizeof(buf));
    for (int i = 1; i <= m; i++) {
        buf[a[strId][i]]++;
        buf[b[strId][i]]--;
    }
    for (int i = 0; i < 26; i++) {
        if (buf[i]) return false;
    }
    return true;
}

// void getArrP()
// {
//     memset(head, -1, sizeof(head));
//     for (int i = 1; i <= m; i++) {
//         next[i] = head[b[1][i]];
//         head[b[1][i]] = i;
//     }
//     for (int i = m; i >= 1; i--) {
//         p[i] = head[a[1][i]];
//         head[a[1][i]] = next[head[a[1][i]]];
//     }
//     for (int i = 1; i <= m; i++) {
//         write(p[i], i != m ? 32 : 10);
//     }
// }

// bool chkPermutation()
// {
//     for (int i = 2; i <= n; i++) {
//         for (int j = 1; j <= m; j++) {
//             if (a[i][j] != b[i][p[j]]) return false;
//         }
//     }
//     return true;
// }

// void nxt(int pos)
// {
//     for (int i = 1; i <= m; i++) {
//         write(p[i], i != m ? 32 : 10);
//     }
// }

bool chkPos(int pos)
{
    for (int i = 2; i <= n; i++) {
        if (a[i][pos] != b[i][p[pos]]) return false;
    }
    return true;
}

list<int> v[26];
list<int>::iterator it;
void getAnswer()
{
    // puts("passing A.");
    for (int i = 0; i < 26; i++) {
        // while (!v[i].empty()) v[i].pop_front();
        v[i].clear();
    }
    for (int i = 1; i <= m; i++) {
        v[b[1][i]].push_back(i);
    }
    for (int i = 1; i <= m; i++) {
        it = v[a[1][i]].begin();
        p[i] = *it;
        // printf("p[%d] = %d.\n", i, *it);
        it++;
        while (!chkPos(i) && it != v[a[1][i]].end()) {
            p[i] = *it;
            // printf("p[%d] = %d.\n", i, *it);
            it++;
        }
        if (!chkPos(i) && it == v[a[1][i]].end()) {
            puts("-1");
            return;
        }
        // puts("passing B.");
        it--;
        v[a[1][i]].erase(it);
        // puts("passing C.");
    }
    for (int i = 1; i <= m; i++) {
        write(p[i], i != m ? 32 : 10);
    }
}

void Qa2()
{
    n = read<int>();
    m = read<int>();
    for (int i = 1; i <= n; i++) {
        scanf("%s", a[i] + 1);
        scanf("%s", b[i] + 1);
        for (int j = 1; j <= m; j++) {
            a[i][j] -= 'a';
            b[i][j] -= 'a';
        }
    }
    for (int i = 1; i <= n; i++) {
        if (!chk(i)) {
            puts("-1");
            return;
        }
    }
    // getArrP();
    // while (!chkPermutation()) nxt();
    getAnswer();
    // for (int i = 1; i <= m; i++) {
    //     write(p[i], i != m ? 32 : 10);
    // }
}

int main()
{
    int T = read<int>();
    while (T--) Qa2();
    return 0;
}

/*
4
1 3
abc
bca
2 4
aaab
baaa
cdcc
cccd
3 3
aaa
aaa
bbb
bbb
ccc
ccc
1 1
a
z
*/