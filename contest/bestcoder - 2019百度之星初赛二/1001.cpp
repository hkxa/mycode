/*************************************
 * problem:      2019BaiduASter初赛1 - P1001.
 * user name:    hkxadpall.
 * time:         2019-08-17.
 * language:     C++.
 * upload place: HDU-BestCoder.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define puts_return(x) { puts(x); return 0; }
#define write_return(x) { write(x); return 0; }

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// bool np[100007];
// int pri[100007], top = 0;

// void pr()
// {
//     np[1] = 1;
//     for (int i = 2; i <= 100000; i++) {
//         if (!np[i]) {
//             pri[++top] = i;
//             for (int j = i + i; j <= 100000; j += i) {
//                 np[j] = 1;
//             }
//         }
//     }
// }

int countDigit(int n)
{
    int ret(0);
    while (n) {
        ret += n % 10;
        n /= 10;
    }
    return ret;
}

int gcd(int n, int m)
{
    return n ? gcd(m % n, n) : m;
}

void p()
{
    int n = read<int>();
    int m = countDigit(n);
    int g = gcd(n, m);
    vector<int> ans;
    for (int i = 1; i <= g; i++) {
        if (g % i == 0) {
            ans.push_back(i);
        }
    }
    write(ans.size(), 10);
    for (int i = 0; i < ans.size(); i++) {
        write(ans[i], i == ans.size() - 1 ? 10 : 32);
    }
}

int main()
{
    // pr();
    int T = read<int>();
    while (T--) p();
    return 0;
}