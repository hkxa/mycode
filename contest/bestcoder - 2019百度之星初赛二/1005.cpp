/*************************************
 * problem:      2019BaiduASter初赛1 - P1005.
 * user name:    hkxadpall.
 * time:         2019-08-17.
 * language:     C++.
 * upload place: HDU-BestCoder.
*************************************/ 
#include <bits/stdc++.h>
using namespace std;
#define puts_return(x) { puts(x); return 0; }
#define write_return(x) { write(x); return 0; }

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int maxn = 200000 + 7;

struct PlusMinusOneRMQ {
    const static int N = 200000 + 7;
    const static int M = 20;
    int blocklen, block, minv[N], dp[N][23],
        t[N * 23], f[1 << M][M][M], s[N];
    void init(int n) {
        blocklen = max(1, (int)(log(n * 1.0) / log(2.0)) / 2);
        block = n / blocklen + (n % blocklen > 0);
        int total = 1 << (blocklen - 1);
        for (int i = 0; i < total; i ++) {
            for (int l = 0; l < blocklen; l ++) {
                f[i][l][l] = l;
                int now = 0, minv = 0;
                for (int r = l + 1; r < blocklen; r ++) {
                    f[i][l][r] = f[i][l][r - 1];
                    if ((1 << (r - 1)) & i) now ++;
                    else {
                        now --;
                        if (now < minv) {
                            minv = now;
                            f[i][l][r] = r;
                        }
                    }
                }
            }
        }

        int tot = N * 23;
        t[1] = 0;
        for (int i = 2; i < tot; i ++) {
            t[i] = t[i - 1];
            if (!(i & (i - 1))) t[i] ++;
        }
    }
    void initmin(int a[], int n) {
        for (int i = 0; i < n; i ++) {
            if (i % blocklen == 0) {
                minv[i / blocklen] = i;
                s[i / blocklen] = 0;
            }
            else {
                if (a[i] < a[minv[i / blocklen]]) minv[i / blocklen] = i;
                if (a[i] > a[i - 1]) s[i / blocklen] |= 1 << (i % blocklen - 1);
            }
        }
        for (int i = 0; i < block; i ++) dp[i][0] = minv[i];
        for (int j = 1; (1 << j) <= block; j ++) {
            for (int i = 0; i + (1 << j) - 1 < block; i ++) {
                int b1 = dp[i][j - 1], b2 = dp[i + (1 << (j - 1))][j - 1];
                dp[i][j] = a[b1] < a[b2]? b1 : b2;
            }
        }
    }
    int querymin(int a[], int L, int R) {
        int idl = L / blocklen, idr = R / blocklen;
        if (idl == idr)
            return idl * blocklen + f[s[idl]][L % blocklen][R % blocklen];
        else {
            int b1 = idl * blocklen + f[s[idl]][L % blocklen][blocklen - 1];
            int b2 = idr * blocklen + f[s[idr]][0][R % blocklen];
            int buf = a[b1] < a[b2]? b1 : b2;
            int c = t[idr - idl - 1];
            if (idr - idl - 1) {

                int b1 = dp[idl + 1][c];
                int b2 = dp[idr - 1 - (1 << c) + 1][c];
                int b = a[b1] < a[b2]? b1 : b2;
                return a[buf] < a[b]? buf : b;
            }
            return buf;
        }
    }
};

struct CartesianTree {
private:
    struct Node {
        int key, value, l, r;
        Node(int key, int value) {
            this->key = key;
            this->value = value;
            l = r = 0;
        }
        Node() {}
    };
    Node tree[maxn];
    int sz;
    int S[maxn], top;
    /** 小根堆 区间最小值*/
public:
    void build(int a[], int n) {
        top = 0;
        tree[0] = Node(-1, 0x80000000);//将根的key和value赋为最小以保持树根不变
        S[top ++] = 0;
        sz = 0;
        for (int i = 0; i < n; i ++) {
            tree[++ sz] = Node(i, a[i]);
            int last = 0;
            while (tree[S[top - 1]].value >= tree[sz].value) {
                last = S[top - 1];
                top --;
            }
            tree[sz].l = last;
            tree[S[top - 1]].r = sz;
            S[top ++] = sz;
        }
    }
    Node &operator [] (const int x) {
        return tree[x];
    }
};/** 树根为定值0，数组从0开始编号 **/



class stdRMQ {
public:
    void work(int a[], int n) {
        ct.build(a, n);
        dfs_clock = 0;
        dfs(0, 0);
        rmq.init(dfs_clock);
        rmq.initmin(depseq, dfs_clock);
    }
    int query(int L, int R) {
        int cl = clk[L], cr = clk[R];
        if (cl > cr) swap(cl, cr);
        return value[rmq.querymin(depseq, cl, cr)];
    }
private:
    CartesianTree ct;
    PlusMinusOneRMQ rmq;
    int dfs_clock, clk[maxn], value[maxn << 1], depseq[maxn << 1];
    void dfs(int rt, int d) {
        clk[ct[rt].key] = dfs_clock;
        depseq[dfs_clock] = d;
        value[dfs_clock ++] = ct[rt].value;
        if (ct[rt].l) {
            dfs(ct[rt].l, d + 1);
            depseq[dfs_clock] = d;
            value[dfs_clock ++] = ct[rt].value;
        }
        if (ct[rt].r) {
            dfs(ct[rt].r, d + 1);
            depseq[dfs_clock] = d;
            value[dfs_clock ++] = ct[rt].value;
        }
    }
} tMem[100007], t;
int cntM = 0;

int n;
int a[100007];
bool noSol = false;
int ans[100007], tp = 0;

void deal(int l, int r, int del)
{
    if (l == r) return;
    int minn = n + 1;
    // for (int i = l; i <= r; i++) minn = min(minn, a[i]);
    minn = t.query(l, r);
    if (minn - del < 0) {
        noSol = true;
        return;
    }
    minn = minn - del + l - 1;
    ans[++tp] = minn;
    if (minn < l || minn + 1 > r) {
        noSol = true;
        return;
    }
    deal(l, minn, del + 1);
    deal(minn + 1, r, del + 1);
}

void lllll()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) a[i] = read<int>();
    t = tMem[cntM++];
    t.work(a, n);
    noSol = false;
    tp = 0;
    deal(1, n, 0);
    if (noSol) puts("Impossible");
    else {
        puts("Possible");
        for (int i = 1; i < n; i++) write(ans[i], 32);
        putchar(10);
    }
}

int main()
{
    int T = read<int>();
    while (T--) lllll();
    return 0;
}