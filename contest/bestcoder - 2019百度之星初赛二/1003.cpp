/*************************************
 * problem:      2019BaiduASter初赛1 - P1003.
 * user name:    hkxadpall.
 * time:         2019-08-17.
 * language:     C++.
 * upload place: HDU-BestCoder.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define puts_return(x) { puts(x); return 0; }
#define write_return(x) { write(x); return 0; }

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;
char e[1 << 22];

int getHighest(int n)
{
    int ret(0);
    while (n) {
        ret++;
        n >>= 1;
    }
    return ret;
}

int calc()
{
    int ans(0), cnt(1), highest = 1;
    for (int i = 1; i <= n; i++) {
        if (e[i] == '^') {
            if (getHighest(cnt) == highest) ans = (1 << highest) - 1;
            else {
                int th = getHighest(ans & cnt);
                ans |= cnt;
                ans |= (1 << th) - 1;
                highest = max(highest, getHighest(cnt));
            }
            // ans |= cnt;
            cnt = 1;
        } else {
            cnt++;
        }
    }
    if (getHighest(cnt) == highest) ans = (1 << highest) - 1;
    else {
        int th = getHighest(ans & cnt);
        ans |= cnt;
        ans |= (1 << th) - 1;
        highest = max(highest, getHighest(cnt));
    }
    ans &= ~1;
    return ans | ((n + 1) & 1);
}

int main()
{
    int T = read<int>();
    while (T--) {
        scanf("%s", e + 1);
        n = strlen(e + 1);
        // for (int i = 1; i <= n; i++) if (e[i] == '?') e[i] = '+';
        write(calc(), 10);
    }
    return 0;
}
// +++++++^+++^^+
// 8^4^1^2

// (1+1)^(1+1)