//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      delicacy.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-18.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 50 + 6, Tmax = 52501 + 7, K = 200 + 7, M = 500 + 7;

int n, m, T, k;
int c[N];

struct solve_1to8_cases {
#define nod_id(nod, tim) (n * (tim) + (nod))
    vector<pair<int, int> > G[N];
    int64 dis[N][Tmax];
    int get[N][Tmax];
    void readln() {
        for (int i = 1, u, v, w; i <= m; i++) {
            read >> u >> v >> w;
            G[u].push_back(make_pair(v, w));
        }
        for (int i = 1; i <= n; i++) 
            for (int j = 1; j <= T; j++) get[i][j] = c[i];
        for (int i = 1, t, x, y; i <= k; i++) {
            read >> t >> x >> y;
            get[x][t] += y;
        }
    }
    signed solve() {
        readln();
        memset(dis, 0xcf, sizeof(dis));
        dis[1][0] = c[1];
        for (int tim = 0; tim < T; tim++) {
            for (int u = 1; u <= n; u++) {
                // printf("dis[%d][%d] = %-15lld%c", u, tim, dis[u][tim], "\t\n"[u == n]);
                for (size_t eid = 0; eid < G[u].size(); eid++) {
                    int v = G[u][eid].first, w = G[u][eid].second;
                    if (tim + w > T) continue;
                    if (dis[v][tim + w] < dis[u][tim] + get[v][tim + w])
                        dis[v][tim + w] = dis[u][tim] + get[v][tim + w];
                }
            }
        }
        if (dis[1][T] < 0) puts("-1");
        else write << dis[1][T] << '\n';
        return 0;
    }
#undef nod_id
}; // O(T(n + m))

int u[M], v[M], w[M];

struct solve_9to10_cases {
    int64 tim[N], totalTim, ctot;
    bool check_if_ok() const {
        if (n != m) return false;
        for (int i = 1; i <= m; i++) 
            if (u[i] != i || v[i] != (i % n) + 1) return false;
        return true;
    }
    signed solve() {
        tim[1] = ctot = 0;
        for (int i = 1; i <= n; i++) ctot += c[i];
        for (int i = 1; i < n; i++) {
            tim[i + 1] = tim[i] + w[i];
        }
        totalTim = tim[n] + w[n];
        if (T % totalTim != 0) {
            puts("-1");
            return 0;
        }
        int64 ans = ctot * (T / totalTim) + c[1];
        for (int i = 1, t, x, y; i <= k; i++) {
            read >> t >> x >> y;
            if (t % totalTim == tim[x]) ans += y;
        }
        write << ans << '\n';
        return 0;
    }
};

struct solve_11to13_cases {
    struct matrix {
        int node_cnt;
        int64 val[353][353];
        void set_to_inf_min() {
            for (int i = 1; i <= node_cnt; i++)
                for (int j = 1; j <= node_cnt; j++)
                    val[i][j] = -0x3f3f3f3f3f3f3f3f;
        }
        void clear_to_0() {
            for (int i = 1; i <= node_cnt; i++)
                for (int j = 1; j <= node_cnt; j++)
                    val[i][j] = 0;
        }
        void to_unit() {
            for (int i = 1; i <= node_cnt; i++)
                for (int j = 1; j <= node_cnt; j++)
                    val[i][j] = (i == j ? 0 : -0x3f3f3f3f3f3f3f3f);
        }
        matrix operator * (const matrix &b) {
            matrix ret(node_cnt);
            ret.set_to_inf_min();
            for (int i = 1; i <= node_cnt; i++)
                for (int k = 1; k <= node_cnt; k++)
                    for (int j = 1; j <= node_cnt; j++)
                        ret.val[i][k] = max(ret.val[i][k], val[i][j] + b.val[j][k]);
            return ret;
        }
        matrix(int cnt = 0) : node_cnt(cnt) {}
    };
    signed solve() { // O(node_cnt^3 * logT) nodecnt max be 250 EXTREME_TIME_USED = 2.55s
        int node_cnt = n;
        matrix r;
        memset(r.val, 0xcf, sizeof(r.val));
        for (int i = 1; i <= m; i++) {
            if (w[i] == 1) {
                r.val[u[i]][v[i]] = c[v[i]];
            } else {
                r.val[u[i]][++node_cnt] = 0;
                for (int j = 2; j < w[i]; j++) {
                    r.val[node_cnt][node_cnt + 1] = 0;
                    node_cnt++;
                }
                r.val[node_cnt][v[i]] = c[v[i]];
            }
        }
        r.node_cnt = node_cnt;
        matrix ans(node_cnt);
        ans.set_to_inf_min();
        ans.val[1][1] = c[1];
        while (T) {
            if (T & 1) ans = ans * r;
            r = r * r;
            T >>= 1;
        }
        // printf("MATRIX r(cnt = %d)\n", r.node_cnt);
        // for (int i = 1; i <= r.node_cnt; i++) 
        //     for (int j = 1; j <= r.node_cnt; j++) 
        //         printf("%lld%c", r.val[i][j] < 0 ? -1ll : r.val[i][j], "\t\n"[j == r.node_cnt]);
        // for (int i = 1; i <= T; i++) {
        //     ans = ans * r;
        //     printf("tim %d | ", i);
        //     for (int j = 1; j <= r.node_cnt; j++) {
        //         printf("node %d val %lld%c", j, ans.val[1][j] < 0 ? -1ll : ans.val[1][j], "\t\n"[j == r.node_cnt]);
        //     }
        // }
        write << (ans.val[1][1] < 0 ? -1ll : ans.val[1][1]) << '\n';
        return 0;
    }
};

struct solve_14to15_cases {
    struct matrix {
        int node_cnt;
        int64 val[353][353];
        void set_to_inf_min() {
            for (int i = 1; i <= node_cnt; i++)
                for (int j = 1; j <= node_cnt; j++)
                    val[i][j] = -0x3f3f3f3f3f3f3f3f;
        }
        void clear_to_0() {
            for (int i = 1; i <= node_cnt; i++)
                for (int j = 1; j <= node_cnt; j++)
                    val[i][j] = 0;
        }
        void to_unit() {
            for (int i = 1; i <= node_cnt; i++)
                for (int j = 1; j <= node_cnt; j++)
                    val[i][j] = (i == j ? 0 : -0x3f3f3f3f3f3f3f3f);
        }
        matrix operator * (const matrix &b) {
            matrix ret(node_cnt);
            ret.set_to_inf_min();
            for (int i = 1; i <= node_cnt; i++)
                for (int k = 1; k <= node_cnt; k++)
                    for (int j = 1; j <= node_cnt; j++)
                        ret.val[i][k] = max(ret.val[i][k], val[i][j] + b.val[j][k]);
            return ret;
        }
        matrix(int cnt = 0) : node_cnt(cnt) {}
    };
    signed solve() { // O(node_cnt^3 * logT) nodecnt max be 250 EXTREME_TIME_USED = 2.55s
        int node_cnt = n;
        matrix r;
        memset(r.val, 0xcf, sizeof(r.val));
        for (int i = 1; i <= m; i++) {
            if (w[i] == 1) {
                r.val[u[i]][v[i]] = c[v[i]];
            } else {
                r.val[u[i]][++node_cnt] = 0;
                for (int j = 2; j < w[i]; j++) {
                    r.val[node_cnt][node_cnt + 1] = 0;
                    node_cnt++;
                }
                r.val[node_cnt][v[i]] = c[v[i]];
            }
        }
        r.node_cnt = node_cnt;
        matrix ans(node_cnt), backup = r;
        ans.set_to_inf_min();
        ans.val[1][1] = c[1];
        struct TIME {
            int t, x, y;
            bool operator < (const TIME &b) const {
                return t < b.t;
            }
        } event[K];
        for (int i = 1; i <= k; i++) read >> event[i].t >> event[i].x >> event[i].y;
        sort(event + 1, event + k + 1);
        event[0].t = 0;
        for (int i = 1; i <= k; i++) {
            r = backup;
            int YYY = event[i].t - event[i - 1].t;
            while (YYY) {
                if (YYY & 1) ans = ans * r;
                r = r * r;
                YYY >>= 1;
            }
            for (int j = 1; j <= node_cnt; j++) ans.val[j][event[i].x] += event[i].y;
        }
        r = backup;
        int YYY = T - event[k].t;
        while (YYY) {
            if (YYY & 1) ans = ans * r;
            r = r * r;
            YYY >>= 1;
        }
        write << (ans.val[1][1] < 0 ? -1ll : ans.val[1][1]) << '\n';
        return 0;
    }
};

struct solve_16to17_cases {
    struct matrix {
        int node_cnt;
        int64 val[503][503];
        void set_to_inf_min() {
            for (int i = 1; i <= node_cnt; i++)
                for (int j = 1; j <= node_cnt; j++)
                    val[i][j] = -0x3f3f3f3f3f3f3f3f;
        }
        void clear_to_0() {
            for (int i = 1; i <= node_cnt; i++)
                for (int j = 1; j <= node_cnt; j++)
                    val[i][j] = 0;
        }
        void to_unit() {
            for (int i = 1; i <= node_cnt; i++)
                for (int j = 1; j <= node_cnt; j++)
                    val[i][j] = (i == j ? 0 : -0x3f3f3f3f3f3f3f3f);
        }
        matrix operator * (const matrix &b) {
            matrix ret(node_cnt);
            ret.set_to_inf_min();
            for (int i = 1; i <= node_cnt; i++)
                for (int k = 1; k <= node_cnt; k++)
                    for (int j = 1; j <= node_cnt; j++)
                        ret.val[i][k] = max(ret.val[i][k], val[i][j] + b.val[j][k]);
            return ret;
        }
        matrix(int cnt = 0) : node_cnt(cnt) {}
    } rth[33];
    signed solve() { // O(node_cnt^3 * klogT) 
        int node_cnt = n;
        matrix r;
        memset(r.val, 0xcf, sizeof(r.val));
        for (int i = 1; i <= m; i++) {
            if (w[i] == 1) {
                r.val[u[i]][v[i]] = c[v[i]];
            } else {
                r.val[u[i]][++node_cnt] = 0;
                for (int j = 2; j < w[i]; j++) {
                    r.val[node_cnt][node_cnt + 1] = 0;
                    node_cnt++;
                }
                r.val[node_cnt][v[i]] = c[v[i]];
            }
        }
        r.node_cnt = node_cnt;
        for (int i = 0; i <= 30; i++) {
            rth[i] = r;
            r = r * r;
        }
        matrix ans(node_cnt);
        ans.set_to_inf_min();
        ans.val[1][1] = c[1];
        struct TIME {
            int t, x, y;
            bool operator < (const TIME &b) const {
                return t < b.t;
            }
        } event[K];
        for (int i = 1; i <= k; i++) read >> event[i].t >> event[i].x >> event[i].y;
        sort(event + 1, event + k + 1);
        event[0].t = 0;
        for (int i = 1; i <= k; i++) {
            int YYY = event[i].t - event[i - 1].t;
            for (int j = 0; YYY; j++)
                if (YYY & (1 << j)) {
                    ans = ans * rth[j];
                    YYY ^= (1 << j);
                }
            for (int j = 1; j <= node_cnt; j++) ans.val[j][event[i].x] += event[i].y;
        }
        int YYY = T - event[k].t;
        for (int j = 0; YYY; j++)
            if (YYY & (1 << j)) {
                ans = ans * rth[j];
                YYY ^= (1 << j);
            }
        write << (ans.val[1][1] < 0 ? -1ll : ans.val[1][1]) << '\n';
        return 0;
    }
};

signed main() {
    // freopen("delicacy.in", "r", stdin);
    // freopen("delicacy.out", "w", stdout);
    read >> n >> m >> T >> k;
    for (int i = 1; i <= n; i++) read >> c[i];
    if (T <= 52501) {
        solve_1to8_cases solve_object;
        return solve_object.solve();
    }
    for (int i = 1; i <= m; i++) read >> u[i] >> v[i] >> w[i];
    if (((solve_9to10_cases*)NULL)->check_if_ok()) {
        solve_9to10_cases solve_object;
        return solve_object.solve();
    }
    if (k == 0) {
        solve_11to13_cases solve_object;
        return solve_object.solve();
    }
    if (k <= 10) {
        solve_14to15_cases solve_object;
        return solve_object.solve();
    }
    solve_16to17_cases solve_object;
    return solve_object.solve();
    // puts("-1");
    return 0;
}