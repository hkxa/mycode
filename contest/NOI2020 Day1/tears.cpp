//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      tears.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-18.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 200000 + 7, M = 200000 + 7;

int n, m;
int p[N];
int xa[M], xb[M], ya[M], yb[M];

struct solve_1to6_cases {
    int t[N];
    solve_1to6_cases() {
        memset(t, 0, sizeof(t));
    }
    void add(int u, int dif) {
        while (u <= n) {
            t[u] += dif;
            u += u & -u;
        }
    }
    int query(int u) {
        int ret = 0;
        while (u) {
            ret += t[u];
            u -= u & -u;
        }
        return ret;
    }
    signed solve() {
        // fprintf(stderr, "case 1~6 :: solve\n");
        for (int i = 1; i <= m; i++) {
            int l = xa[i], r = xb[i];
            int64 ans = 0;
            for (int x = l; x <= r; x++) {
                if (p[x] < ya[i] || p[x] > yb[i]) continue;
                ans += query(p[x]);
                add(p[x], 1);
            }
            for (int x = l; x <= r; x++) 
                if (p[x] >= ya[i] && p[x] <= yb[i]) add(p[x], -1);
            write << ans << '\n';
        }
        return 0;
    }
};

bool check_limitA() {
    for (int i = 1; i <= m; i++)
        if (ya[i] != 1 || yb[i] != n) return false;
    return true;
}

struct solve_7to10_cases {
    int t[N];
    int64 id_ans[M];
    struct Question {
        int xl, xr, bl, id;
        bool operator < (const Question &b) const {
            return bl ^ b.bl ? (xl < b.xl) : (xr < b.xr);
        }
    } q[M];
    solve_7to10_cases() {
        memset(t, 0, sizeof(t));
    }
    inline void add(int u, int dif) {
        while (u <= n) {
            t[u] += dif;
            u += u & -u;
        }
    }
    inline int query(int u) {
        int ret = 0;
        while (u) {
            ret += t[u];
            u -= u & -u;
        }
        return ret;
    }
    signed solve() {
        // fprintf(stderr, "case 7~10 :: solve\n");
        int S = sqrt(n);
        for (int i = 1; i <= m; i++) {
            q[i].xl = xa[i];
            q[i].xr = xb[i];
            q[i].bl = xa[i] / S;
            q[i].id = i;
        }
        sort(q + 1, q + m + 1);
        int l = q[1].xl, r = q[1].xr;
        int64 ans = 0;
        for (int i = l; i <= r; i++) {
            ans += query(p[i]);
            add(p[i], 1);
        }
        id_ans[q[1].id] = ans;
        for (int i = 2; i <= m; i++) {
            // if (i % 2000 == 0) fprintf(stderr, "deal %d\n", i);
            while (r < q[i].xr) {
                // fprintf(stderr, "case_A : %d ~ %d limit[%d, %d]\n", l, r, q[i].xl, q[i].xr);
                r++;
                ans += query(p[r]);
                add(p[r], 1);
            }
            while (l > q[i].xl) {
                // fprintf(stderr, "case_B : %d ~ %d limit[%d, %d]\n", l, r, q[i].xl, q[i].xr);
                l--;
                ans += query(n) - query(p[l]);
                add(p[l], 1);
            }
            while (l < q[i].xl) {
                // fprintf(stderr, "case_C : %d ~ %d limit[%d, %d]\n", l, r, q[i].xl, q[i].xr);
                add(p[l], -1);
                ans -= query(n) - query(p[l]);
                l++;
            }
            while (r > q[i].xr) {
                // fprintf(stderr, "case_D : %d ~ %d limit[%d, %d]\n", l, r, q[i].xl, q[i].xr);
                add(p[r], -1);
                ans -= query(p[r]);
                r--;
            }
            id_ans[q[i].id] = ans;
        }
        for (int i = 1; i <= m; i++) write << id_ans[i] << '\n';
        return 0;
    }
};

signed main() {
    // freopen("tears.in", "r", stdin);
    // freopen("tears.out", "w", stdout);
    read >> n >> m;
    for (int i = 1; i <= n; i++) read >> p[i];
    for (int i = 1; i <= m; i++) read >> xa[i] >> xb[i] >> ya[i] >> yb[i];
    if (n <= 5000 && m <= 5000) {
        solve_1to6_cases object;
        return object.solve();
    }
    if (check_limitA()) {
        solve_7to10_cases object;
        return object.solve();
    }
    solve_1to6_cases object;
    return object.solve();
    return 0;
}