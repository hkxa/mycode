//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      destiny.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-18.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 500000 + 7, P = 998244353;

int64 fpow(int64 a, int n) {
    int64 ret = 1;
    while (n) {
        if (n & 1) (ret *= a) %= P;
        (a *= a) %= P;
        n >>= 1;
    }
    return ret;
}

int n, m;
vector<int> G[N];
int u[N], v[N];
int fa[N], dep[N], siz[N], wson[N];
int id[N], beg[N], dfn[N], dft;

void dfs(int u, int fat) {
    fa[u] = fat;
    dep[u] = dep[fat] + 1;
    siz[u] = 1;
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i];
        if (v != fat) {
            dfs(v, u);
            siz[u] += siz[v];
            if (siz[v] > siz[wson[u]]) wson[u] = v;
        }
    }
}

void dfs_again(int u, int begi) {
    id[dfn[u] = ++dft] = u;
    beg[u] = begi;
    if (!wson[u]) return;
    dfs_again(wson[u], begi);
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i];
        if (v != fa[u] && v != wson[u])
            dfs_again(v, v);
    }
}

namespace subtask_1to8 {
    int zy_popcount(int number) {
        int ret = 0;
        while (number) {
            ret++;
            number -= number & -number;
        }
        return ret;
    }
    int t[N << 2], tag[N << 2];
    void modify(int u, int l, int r, int ml, int mr, int dif) {
        if (l > mr || r < ml) return;
        if (l >= ml && r <= mr) {
            tag[u] += dif;
            if (tag[u]) t[u] = r - l + 1;
            else t[u] = t[u << 1] + t[u << 1 | 1];
            return;
        }
        int mid = (l + r) >> 1;
        modify(u << 1, l, mid, ml, mr, dif);
        modify(u << 1 | 1, mid + 1, r, ml, mr, dif);
        if (tag[u]) t[u] = r - l + 1;
        else t[u] = t[u << 1] + t[u << 1 | 1];
    }
    void chain_modify(int u, int v, int dif) {
        while (beg[u] != beg[v]) {
            modify(1, 1, n, dfn[beg[u]], dfn[u], dif);
            u = fa[beg[u]];
        }
        if (u != v) modify(1, 1, n, dfn[v] + 1, dfn[u], dif);
    }
    int subtask_1to8_cases_sol() {
        // fprintf(stderr, "case 5 ~ 10\n");
        dft = 0;
        dfs_again(1, 1);
        int MASK = (1 << m) - 1, on_sched = 0;
        int64 ans = 0;
        for (int bit_on = 0; bit_on <= MASK; bit_on++) {
            int lst_on = bit_on - 1;
            if (bit_on)
                for (int i = 1; i <= m; i++) 
                    if ((bit_on & (1 << (i - 1))) && !(lst_on & (1 << (i - 1)))) {
                        chain_modify(v[i], u[i], 1);
                    } else if (!(bit_on & (1 << (i - 1))) && (lst_on & (1 << (i - 1)))) {
                        chain_modify(v[i], u[i], -1);
                    }
            // printf("bit_on %d cause %d line\n", bit_on, t[1]); 
            if ((zy_popcount(bit_on) & 1) == on_sched) {
                ans = (ans + fpow(2, n - t[1] - 1)) % P;
            } else {
                ans = (ans - fpow(2, n - t[1] - 1) + P) % P;
            }
        }
        return ans;
    }
}

signed main() {
    // freopen("destiny.in", "r", stdin);
    // freopen("destiny.out", "w", stdout);
    read >> n;
    for (int i = 1, a, b; i < n; i++) {
        read >> a >> b;
        G[a].push_back(b);
        G[b].push_back(a);
    }
    read >> m;
    for (int i = 1; i <= m; i++) read >> u[i] >> v[i];
    dfs(1, 0);
    dfs_again(1, 1);
    if (m <= 22) {
        write << subtask_1to8::subtask_1to8_cases_sol() << '\n';
        return 0;
    }
    write << subtask_1to8::subtask_1to8_cases_sol() << '\n';
    return 0;
}