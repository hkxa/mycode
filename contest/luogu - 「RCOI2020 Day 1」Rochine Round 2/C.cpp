/*************************************
 * @problem:      T114736 [RC-02] XOR.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-02-05.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int root = 1;
int n, m, q;
int v[10007];
int fa[10007][23], dep[10007];
int sum[10007], son[10007], ans;
vector<int> G[10007];

void dfs(int u, int f) 
{
    dep[u] = dep[f] + 1;
    // if (f && fa[u][0] == f) goto startPos;
    fa[u][0] = f;
    for (int k = 1; k <= 20; k++) 
        fa[u][k] = fa[fa[u][k - 1]][k - 1];
    // startPos:;
    son[u] = v[u];
    sum[u] = v[u] ^ sum[f];
    for (unsigned i = 0; i < G[u].size(); i++) {
        if (G[u][i] != f) {
            dfs(G[u][i], u);
            son[u] ^= son[G[u][i]];
        }
    }
    // printf("son[%d] = %d.\n", u, son[u]);
    ans ^= son[u];
}

/*
5 4 4
0 0 2 2 1
1 2
1 3
2 4
2 5
1 1
1 1
1 1
2 3 0
4 3 3
5 1
1 2
3 1 2
*/

inline void init()
{
    ans = dep[0] = sum[0] = 0;
    dfs(root, 0);
}

void change_up(int x, const int y)
{
    son[x] ^= y;
    if (x != root) change_up(fa[x][0], y);
}

void change_down(int x, const int y)
{
    sum[x] ^= y;
    for (unsigned i = 0; i < G[x].size(); i++) 
        if (G[x][i] != fa[x][0]) 
            dfs(G[x][i], y);
}

int LCA(int x, int y)
{
    if (dep[x] < dep[y]) swap(x, y);
    int jump = dep[x] - dep[y];
    for (unsigned k = 0; k <= 20; k++) 
        if (jump & (1 << k)) x = fa[x][k];
    for (int k = 20; k >= 0; k--) 
        if (fa[x][k] != fa[y][k]) x = fa[x][k], y = fa[y][k];
    return x == y ? x : fa[x][0];
}

int main()
{
    n = read<int>();
    m = read<int>();
    q = read<int>();
    for (int i = 1; i <= n; i++) v[i] = read<int>();
    for (int i = 1, u, v; i < n; i++) {
        u = read<int>();
        v = read<int>();
        G[u].push_back(v);
        G[v].push_back(u);
    }
    init();
    for (int i = 1, op, x, y; i <= m + q; i++) {
        op = read<int>();
        switch (op) {
            case 1 :
                x = read<int>();
                if (root == x) write(ans, 10);
                else {
                    root = x;
                    init();
                    write(ans, 10);
                }
                break;
            case 2 :
                x = read<int>();
                y = read<int>();
                change_up(x, v[x] ^ y);
                change_down(x, v[x] ^ y);
                v[x] = y;
                break;
            case 3 : 
                x = read<int>();
                y = read<int>();
                write(LCA(x, y), 10);
                break;
            case 4 : 
                x = read<int>();
                y = read<int>();
                write(sum[x] ^ sum[y] ^ sum[LCA(x, y)], 10);
                break;
            case 5 : 
                x = read<int>();
                write(son[x], 10);
                break;
        }
    }
    return 0;
}