/*************************************
 * @problem:      T114048 [RC-02] yltx 数对.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-02-05.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

bool isnp[10007];
int rec[1229 + 3], rec_cnt = 0;
int T[10007];
void initPrimes()
{
    for (int i = 2; i < 10000; i++) {
        if (!isnp[i]) {
            rec[++rec_cnt] = i;
            for (int j = i * i; j < 10000; j += i) {
                isnp[j] = true;
            }
        }
    }
}

bool isPrime(int a)
{
    if (a < 2) return false;
    if (a < 10000) return !isnp[a];
    for (int i = 1; i <= rec_cnt && rec[i] * rec[i] <= a; i++) {
        if (!(a % rec[i])) return false;
    }
    // fprintf(stderr, "%d is p\n", a);
    return true;
}

int tr[10007];
void update(int p, int v)
{
    for (; p <= 10000; p += (p & (-p))) tr[p] += v;
}

int main()
{
    initPrimes();
    // fprintf(stderr, "rec_cnt = %d.\n", rec_cnt);
    for (int i = 1; i <= rec_cnt; i++) {
        for (int j = 1; j <= rec_cnt; j++) {
            if (isPrime(rec[i] * rec[j] - 3 * (rec[i] - rec[j]))) {
                // fprintf(stderr, "(%d, %d)\n", rec[i], rec[j]);
                T[max(rec[i], rec[j])]++;
            }
        }
    }
    for (int i = 1; i <= 10000; i++) {
        update(i, T[i]);
        printf("%d, ", tr[i]);
        if (i % 20 == 0) printf("\n    ");
    }
    return 0;
}