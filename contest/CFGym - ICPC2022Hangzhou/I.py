from random import randint
from sys import exit


currentPoint = 0
def dowalk(x):
    global currentPoint
    print('walk', x, flush=True)
    currentPoint = int(input())
    return currentPoint

d = 3333

mx = 0
for i in range(d):
    mx = max(mx, dowalk(randint(0, 1000000000)))
u = currentPoint
s = {u: 0}
for i in range(d):
    if dowalk(1) == u:
        print('guess', i + 1)
        exit(0)
    s[currentPoint] = i + 1
if dowalk(mx) in s:
    print('guess', mx + d - s[currentPoint])
    exit(0)
for i in range(d):
    if dowalk(d) in s:
        print('guess', mx + (i + 2) * d - s[currentPoint])
        exit(0)