n = int(input())
outputed = set()
for _ in range(n):
    cnt = int(input())
    playGenshin = True
    for _ in range(cnt):
        msg = input()
        if 'bie' in msg:
            msg_hashed = hash(msg)
            if msg_hashed not in outputed:
                outputed.add(msg_hashed)
                playGenshin = False
                print(msg)
    if playGenshin:
        print('Time to play Genshin Impact, Teacher Rice!')