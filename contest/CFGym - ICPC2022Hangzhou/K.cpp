#include <bits/stdc++.h>
using namespace std;
typedef long long int64;


const int N = 5e5 + 7, S = 1e6 + 7;

struct trie {
    int cnt, ending;
    int ch[26];
} tr[S];
int trie_cnt = 1;
int64 contribute[26][26], must_contribute = 0;

void insert(const string &s) {
    int u = 1;
    for (char c: s) {
        ++tr[u].cnt;
        int cur = c - 'a';
        if (!tr[u].ch[cur]) tr[u].ch[cur] = ++trie_cnt;
        for (int another = 0; another < 26; ++another)
            contribute[cur][another] += tr[tr[u].ch[another]].cnt;
        u = tr[u].ch[cur];
    }
    ++tr[u].cnt;
    ++tr[u].ending;
    must_contribute += tr[u].cnt - tr[u].ending;
}

signed main() {
    ios::sync_with_stdio(false);
    int n, q;
    string s;
    cin >> n >> q;
    for (int i = 1; i <= n; ++i) {
        cin >> s;
        insert(s);
    }
    for (int i = 1; i <= q; ++i) {
        cin >> s;
        int64 ans = must_contribute;
        for (int u = 0; u < 26; ++u)
            for (int v = u + 1; v < 26; ++v)
                ans += contribute[s[u] - 'a'][s[v] - 'a'];
        cout << ans << '\n';
    }
}