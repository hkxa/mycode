from math import gcd


n, m = map(int, input().split())
a = list(map(int, input().split()))

def solveEq(a, b, c):
    # solve ax = b (mod c)
    g = gcd(gcd(a, b), c)
    return b // g * pow(a // g, -1, c // g) % (c // g)
    pass

if m == 1:
    print(0)
    print(0, 0)
elif n % 2 == 1:
    current = sum(a) % m
    smallest_piece = gcd(n, m)
    minans = current % smallest_piece
    diff = minans - current
    if diff == 0:
        print(minans)
        print(0, 0)
    else:
        n_count = solveEq(n, diff, m)
        print(minans)
        print(n_count, 0)
else:
    current = sum(a) % m
    smallest_piece = gcd(n//2, m)
    minans = current % smallest_piece
    diff = minans - current
    if diff == 0:
        print(minans)
        print(0, 0)
    else:
        n_count = solveEq(n // 2, diff, m)
        print(minans)
        print(n_count * -n//2 % m, n_count)
    
    # 2 * (n/2)
    # (n+1) * (n/2)
