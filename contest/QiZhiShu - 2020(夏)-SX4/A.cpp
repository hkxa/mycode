//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      QiZhiShu - 2020(夏)-SX4.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-27.
 * @language:     C++.
*************************************/ 
#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 2e6 + 7, Q = 5e5 + 7;

int n;
char setNameBuffer[5];
bool setName[Q];
string s[Q];

struct TrieNode {
    int ch[26];
    int cnt[2];
    TrieNode() {}
} t[N];
int TrieNodeCnt = 1;
int ans;

void insert(const char *strBuffer, int set_id, int dif) {
    int u = 1, len = strlen(strBuffer);
    for (int i = 0; i < len; i++) {
        if (!t[u].ch[strBuffer[i] - 'a']) t[u].ch[strBuffer[i] - 'a'] = ++TrieNodeCnt;
        u = t[u].ch[strBuffer[i] - 'a'];
        ans -= min(t[u].cnt[0], t[u].cnt[1]);
        t[u].cnt[set_id] += dif;
        ans += min(t[u].cnt[0], t[u].cnt[1]);
    }
}

signed main() {
    memset(t, 0, sizeof(t));
    cin.tie(0);
    ios::sync_with_stdio(0);
    cin >> n;
    for (int i = 1, opt, x; i <= n; i++) {
        // if (i % 2000 == 0) fprintf(stderr, "%d-th\n", i);
        cin >> opt;
        if (opt == 1) {
            cin >> setNameBuffer >> s[i];
            insert(s[i].c_str(), setName[i] = (setNameBuffer[0] == 'S'), 1);
        } else {
            cin >> x;
            insert(s[x].c_str(), setName[x], -1);
        }
        write << ans << '\n';
    }
    return 0;
}