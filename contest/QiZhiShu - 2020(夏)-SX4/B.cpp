#include <bits/stdc++.h>
#define int64 long long
using namespace std;

template <typename tn>
void read(tn &a) {
    tn x = 0, f = 1;
    char c = ' ';
    for (; !isdigit(c); c = getchar())
        if (c == '-')
            f = -1;
    for (; isdigit(c); c = getchar())
        x = x * 10 + c - '0';
    a = x * f;
}

const int N = 2010, mod = 1e9 + 7;

int n;

int64 m, K, f[1010], g[1010], ans[1010], a[1010];

int64 fp(int64 a, int64 k) {
    int64 ans = 1;
    for (; k; k >>= 1, a = a * a % mod)
        if (k & 1)
            ans = a * ans % mod;
    return ans;
}

void solve(int64 *f, int64 *g) {
    static int64 b[510][510];
    for (int i = 1; i < n; i++) {
        b[i][i] = 1;
        for (int j = 1; j < n; j++) {
            for (int k = j + 1; k <= n; k++)
                b[i][j] = (b[i][j] - f[i - (j - k)] * a[k]) % mod, b[i][n] = (b[i][n] + (g[i - (j - k)] + f[i - (j - k)]) * a[k]) % mod;
        }
        for (int k = 1; k <= n; k++)
            b[i][n] = (b[i][n] + (g[i + k] + f[i + k]) * a[k]) % mod;
    }
    for (int i = 1; i < n; i++) {
        for (int j = i + 1; j < n; j++)
            if (b[i][i] % mod == 0 && b[j][i] % mod)
                swap(b[i], b[j]);
        if (b[i][i] % mod == 0)
            continue;
        for (int j = i + 1; j < n; j++) {
            int64 res = b[j][i] * fp(b[i][i], mod - 2) % mod;
            for (int k = 1; k <= n; k++)
                b[j][k] = (b[j][k] - b[i][k] * res) % mod;
        }
    }
    for (int i = n - 1; i; i--) {
        for (int j = i + 1; j < n; j++)
            b[i][n] = (b[i][n] - b[i][j] * ans[j]) % mod;
        ans[i] = b[i][n] * fp(b[i][i], mod - 2) % mod;
    }
}

namespace Constructer {
void Construct(int64 *a, int n, vector<int64> &ans) {
    ans.clear();
    vector<int64> lst;
    int w = 0;
    int64 delta = 0;
    for (int i = 1; i <= n; i++) {
        int64 tmp = 0;
        for (int j = 0; j < ans.size(); j++)
            tmp = (tmp + a[i - 1 - j] * ans[j]) % mod;
        if ((a[i] - tmp) % mod == 0)
            continue;
        if (!w) {
            w = i;
            delta = a[i] - tmp;
            for (int j = i; j; j--)
                ans.push_back(0);
            continue;
        }
        vector<int64> now = ans;
        int64 mul = (a[i] - tmp) * fp(delta, mod - 2) % mod;
        if (ans.size() < lst.size() + i - w)
            ans.resize(lst.size() + i - w);
        ans[i - w - 1] = (ans[i - w - 1] + mul) % mod;
        for (int j = 0; j < lst.size(); j++)
            ans[i - w + j] = (ans[i - w + j] - mul * lst[j]) % mod;
        if (now.size() - i < lst.size() - w) {
            lst = now;
            w = i;
            delta = a[i] - tmp;
        }
    }
}
void calc(int64 m, vector<int64> &coef, int64 *ans) {
    int k = coef.size();
    static int64 f[N], g[N], res[N], p[N];
    p[0] = -1;
    for (int i = 1; i <= k; i++)
        p[i] = coef[i - 1];
    for (int i = 0; i <= 2 * k; i++)
        f[i] = g[i] = 0;
    f[0] = 1;
    if (k > 1)
        g[1] = 1;
    else
        g[0] = p[0];
    auto mul = [&](int64 *a, int64 *b, int64 *c) {
        for (int i = 0; i <= 2 * k; i++)
            res[i] = 0;
        for (int i = 0; i < k; i++)
            for (int j = 0; j < k; j++)
                res[i + j] = (res[i + j] + a[i] * b[j]) % mod;
        for (int i = 2 * k; i >= k; i--)
            if (res[i] % mod)
                for (int j = k; ~j; j--)
                    res[i - j] = (res[i - j] + res[i] * p[j]) % mod;
        for (int i = 0; i < 2 * k; i++)
            c[i] = res[i];
        return 0;
    };
    for (; m; m >>= 1, mul(g, g, g))
        if (m & 1)
            mul(f, g, f);
    for (int i = 0; i < k; i++)
        ans[i] = f[i];
}

} // namespace Constructer

vector<int64> coef;

void calc(int64 l, int64 r) {
    int now = 0;
    memset(f, 0, sizeof(f));
    memset(g, 0, sizeof(g));
    while (l < 0)
        f[now] = 0, g[now] = 0, l++, now++;
    static int64 p1[1510], p2[1510], p[1010];
    p1[0] = 1;
    p2[0] = 0;
    for (int i = 1; i <= 3 * n; i++) {
        p1[i] = p2[i] = 0;
        for (int j = 1; j <= n; j++)
            if (j <= i)
                p1[i] = (p1[i] + p1[i - j] * a[j]) % mod, p2[i] = (p2[i] + (p2[i - j] + p1[i - j]) * a[j]) % mod;
    }
    Constructer::calc(l, coef, p);
    for (int i = 0; i < n && i <= r - l; i++) {
        for (int j = 0; j < coef.size(); j++)
            f[now] = (f[now] + p[j] * p1[i + j]) % mod, g[now] = (g[now] + p[j] * p2[i + j]) % mod;
        now++;
    }
    for (int64 i = l + n; i <= r; i++) {
        f[now] = g[now] = 0;
        for (int j = 1; j <= n; j++)
            f[now] = (f[now] + f[now - j] * a[j]) % mod, g[now] = (g[now] + (g[now - j] + f[now - j]) * a[j]) % mod;
        now++;
    }
    reverse(f, f + now);
    reverse(g, g + now);
}

int64 C[2010];

void get_coef() {
    static int M[1010][1010], now[1010], q[1010], res[1010];
    vector<pair<int, int> > V;
    for (int i = 0; i < n; i++) {
        M[0][i] = a[i + 1], M[n][i] = M[n][i + n] = a[i + 1];
        V.emplace_back(0, i);
        V.emplace_back(n, i);
        V.emplace_back(n, i + n);
    }
    for (int i = 1; i < n; i++) {
        M[i][i - 1] = 1, M[i + n][i + n - 1] = 1;
        V.emplace_back(i, i - 1);
        V.emplace_back(i + n, i + n - 1);
    }
    srand(20170933);
    for (int i = 0; i < 2 * n; i++)
        now[i] = rand() % mod, q[i] = rand() % mod;
    for (int i = 1; i <= 4 * n + 1; i++) {
        for (int j = 0; j < 2 * n; j++)
            C[i] = (C[i] + 1ll * now[j] * q[j]) % mod;
        memset(res, 0, sizeof(res));
        for (auto pr : V)
            res[pr.second] = (res[pr.second] + 1ll * now[pr.first] * M[pr.first][pr.second]) % mod;
        memcpy(now, res, sizeof(res));
    }
    Constructer::Construct(C, 4 * n + 1, coef);
}

int main() {
    read(n);
    read(m);
    read(K);
    int sum = 0;
    for (int i = 1; i <= n; i++)
        read(a[i]), sum += a[i];
    for (int i = 1; i <= n; i++)
        a[i] = a[i] * fp(sum, mod - 2) % mod;
    get_coef();
    calc(m - 2 * n, m);
    solve(f, g);
    calc(K - n, K);
    int64 S = 0;
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j < n; j++)
            S = (S + a[i + j] * f[i] % mod * ans[j] + a[i + j] * (g[i] + f[i])) % mod;
        S = (S + a[i] * (g[i] + f[i])) % mod;
    }
    cout << (S + mod) % mod << '\n';
    return 0;
}