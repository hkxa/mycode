//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      QiZhiShu - 2020(夏)-SX4.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-27.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64


const int N = 101000;

struct node {
    int64 x, y, tim1, tim2;
    bool operator < (const node &b) const {
        return tim1 < b.tim1;
    }
} p[N];

int64 n, l1, l2, l3, k, cnt;
int64 Alen, Blen;

pair<int64, int64> A[N], B[N];

struct segment_tree {
    int64 tag[N * 64], lc[N * 64], rc[N * 64], rt, tot;
    set<int64> S;
    int64 insert(int64 w, int64 &x, int64 l = 1, int64 r = n + l1 + l2 + l3 + k + 2) {
        if (!x)
            x = ++tot;
        if (tag[x])
            return 0;
        if (l == r) {
            tag[x] = 1;
            S.insert(l);
            return l;
        }
        int64 mid = (l + r) >> 1, ans = 0;
        if (w <= mid)
            ans = insert(w, lc[x], l, mid);
        if (!ans)
            ans = insert(w, rc[x], mid + 1, r);
        tag[x] = tag[lc[x]] && tag[rc[x]];
        return ans;
    }
    void solve(pair<int64, int64> *A, int64 &len) {
        int64 lst = 0;
        for (set<int64>::iterator it = S.begin(); it != S.end(); it++) {
            if (lst + 1 < *it)
                A[++len] = make_pair(lst + 1, *it - 1);
            lst = *it;
        }
        A[++len] = make_pair(lst + 1, n + l1 + l2 + l3 + 2 + k);
    }
} t1, t2;

bool check(int64 lim) {
    sort(p + 1, p + cnt + 1);
    vector<int64> v;
    int64 lst = 0, now = 1;
    for (int64 i = 1; i <= cnt; i++) {
        int64 w = p[i].tim1 <= lst ? lst + 1 : p[i].tim1;
        while (now <= Alen && A[now].second < w) now++;
        if (A[now].first > w) w = A[now].first;
        if (w > lim) break;
        v.push_back(w);
        lst = w;
    }
    now = 1;
    priority_queue<int64> S;
    for (size_t i = 0; i < v.size(); i++) {
        while (now <= cnt && p[now].tim1 <= v[i])
            S.push(p[now++].tim2);
        if (S.size()) S.pop();
    }
    for (int64 i = now; i <= cnt; i++)
        S.push(p[i].tim2);
    lst = 0;
    now = 1;
    vector<int64> V;
    while (!S.empty()) {
        V.push_back(S.top());
        S.pop();
    }
    sort(V.begin(), V.end());
    for (size_t i = 0; i < V.size(); i++) {
        int64 w = V[i] <= lst ? lst + 1 : V[i];
        while (now <= Blen && B[now].second < w)
            now++;
        if (B[now].first > w)
            w = B[now].first;
        if (w > lim)
            return 0;
        lst = w;
    }
    return true;
}

int main() {
    read >> k >> l1 >> l2 >> l3 >> n;
    int64 mx = 0;
    for (int64 i = 1, x, y; i <= k; i++) {
        read >> x >> y;
        if (x <= l1) {
            mx = max(mx, t1.insert(l1 - x + 1 + y, t1.rt));
        } else if (l1 + l2 + 3 <= x && x <= l1 + l2 + l3 + 2) {
            mx = max(mx, t2.insert(x - l2 - l1 - 2 + y, t2.rt));
        } else if (l1 + 2 <= x && x <= l1 + l2 + 1) {
            cnt++;
            p[cnt].y = y;
            p[cnt].x = x;
            p[cnt].tim1 = p[cnt].x - l1 - 1 + p[cnt].y;
            p[cnt].tim2 = l1 + l2 + 2 - p[cnt].x + p[cnt].y;
        }
    }
    t1.solve(A, Alen);
    t2.solve(B, Blen);
    int64 l = mx - 1, r = n + l1 + l2 + l3 + k + 3;
    while (l + 1 < r) {
        int64 mid = l + r >> 1;
        if (check(mid))
            r = mid;
        else
            l = mid;
    }
    write << r << '\n';
    return 0;
}