#include "paint.h"

#include <bits/stdc++.h>
using std::vector;
using std::lower_bound;

const int N = 1e5 + 7;

vector<int> love[N];

bool all_loved_size_are_less_than_1_strictly(const int &k) {
    for (int i = 0; i < k; i++)
        if (love[i].size() > 1) return false;
    return true;
}

int solve_subtask_1(int &n, int &m, int &k, vector<int> &c, vector<int> &a, vector<vector<int> > &b) {
    // printf(">>> ");
    int *where = new int[n];
    for (int i = 0; i < n; i++) 
        where[i] = love[c[i]][0];
    int DuanCeng = 0;
    if ((where[n - 1] + 1) % m != where[0]) DuanCeng++;
    for (int i = 1; i < n; i++)
        if ((where[i - 1] + 1) % m != where[i]) DuanCeng++;
    return DuanCeng > 1 ? -1 : (n + m - 1) / m;
}

bool exist(vector<int> &a, int val) {
    vector<int>::iterator it = lower_bound(a.begin(), a.end(), val);
    return it != a.end() && *it == val;
}

int solve_subtask_2(int &n, int &m, int &k, vector<int> &c, vector<int> &a, vector<vector<int> > &b) {
    bool *ok_use_x = new bool[n];
    bool *covered = new bool[n];
    for (int i = 0; i < n; i++) ok_use_x[i] = false;
    for (int x = 0; x < n; x++) {
        for (int y = 0; y < m && !ok_use_x[x]; y++) {
            bool ok = 1;
            for (int g = 0; g < m && ok; g++)
                if (!exist(b[(y + g) % m], c[(x + g) % n])) ok = false;
            // if (ok) printf("x = %d ok in y = %d\n", x, y);
            if (ok) ok_use_x[x] = true;
        }
        if (ok_use_x[x]) {
            // printf("ok_use_x : %d is true\n", x);
            ok_use_x[x] = true;
        }
    }
    int count = 0, ans = -1;
    for (int x = 0; x < m; x++) {
        // force use x
        if (!ok_use_x[x]) continue;
        for (int i = 0; i < n; i++) covered[i] = false;
        int ok = 1, nearest = 0;
        for (int i = 0; i < n && ok; i++) {
            int pos = (i + x) % n;
            if (ok_use_x[pos]) nearest = i;
            if (covered[pos]) continue;
            count++;
            if (i - nearest >= m) {
                ok = 0;
                continue;
            }
            for (int p = i - nearest; p < m; p++) covered[(nearest + p) % n] = true;
        }
        if (ok && !~ans || count < ans) ans = count;
    }
    return ans;
}

int minimumInstructions(int n, int m, int k, vector<int> c, vector<int> a, vector<vector<int> > b) {
    for (int i = 0; i < m; i++)
        for (int j = 0; j < a[i]; j++)
            love[b[i][j]].push_back(i);
    for (int i = 0; i < n; i++)
        if (love[c[i]].empty()) return -1;
    if (all_loved_size_are_less_than_1_strictly(k)) return solve_subtask_1(n, m, k, c, a, b);
    if (n <= 500) return solve_subtask_2(n, m, k, c, a, b);
    return solve_subtask_2(n, m, k, c, a, b);
    return (n + m - 1) / m;
}
