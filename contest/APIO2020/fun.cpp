#include "fun.h"

#include <bits/stdc++.h>
using std::priority_queue;
using std::vector;
using std::swap;
using std::sort;
using std::max;

const int N = 2e5 + 7;

vector<int> ans;
int n, q;

vector<int> G[N];

bool forbidden[N];
int dis[N], SUBTASK_1_2_MAX_DIS;
void dfs(int u, int fa) {
    dis[u] = dis[fa] + 1;
    if (!forbidden[u] && (SUBTASK_1_2_MAX_DIS == -1 || dis[u] > dis[SUBTASK_1_2_MAX_DIS])) SUBTASK_1_2_MAX_DIS = u;
    for (size_t i = 0; i < G[u].size(); i++)
        if (G[u][i] != fa) dfs(G[u][i], u);
}
int find_farthest(int u) {
    SUBTASK_1_2_MAX_DIS = -1;
    dfs(u, -1);
    return SUBTASK_1_2_MAX_DIS;
}

void solve_subtask_1_2() {
    for (int i = 0; i < n; i++)
        for (int j = i + 1; j < n; j++)
            if (G[i].size() < 3 && G[j].size() < 3 && q-- && hoursRequired(i, j) == 1) {
                G[i].push_back(j);
                G[j].push_back(i);
                // printf("find path <%d, %d>\n", i, j);
            }
    ans[0] = find_farthest(0);
    forbidden[ans[0]] = true;
    for (int i = 1; i < n; i++) {
        ans[i] = find_farthest(ans[i - 1]);
        forbidden[ans[i]] = true;
    }
    // for (int i = 0; i < n; i++) printf("ans[%d] = %d\n", i, ans[i]);
}

int dep[N], belong[N];
int dfn[N], dft;

bool subtask3_cmp(const int &a, const int &b) {
    return dfn[a] < dfn[b];
    // old below
    if (belong[a] ^ belong[b]) return belong[a] < belong[b];
    if (belong[a] == 0) {
        // left
        if (dep[a] ^ dep[b]) return dep[a] > dep[b];
        return a > b;
    } else {
        // right
        if (dep[a] ^ dep[b]) return dep[a] < dep[b];
        return a < b;
    }
}

void set_belong(int u, int kase, int depth = 1) {
    if (u >= n) return;
    dep[u] = depth;
    belong[u] = kase;
    set_belong(u * 2 + 1, kase, depth + 1);
    set_belong(u * 2 + 2, kase, depth + 1);
    dfn[u] = ++dft;
}

int order[N];
int cnt[N], deep_most[N];
void build(int u) {
    if (u >= n) return;
    build(u * 2 + 1);
    build(u * 2 + 2);
    cnt[u] = cnt[u * 2 + 1] + cnt[u * 2 + 2] + 1;
    deep_most[u] = max(deep_most[u * 2 + 1], deep_most[u * 2 + 2]) + 1;
}

int tag[N];

void make_tag(int u, int tg) {
    tag[u] = tg;
    if (u) make_tag((u - 1) >> 1, tg);
    deep_most[u] = 0;
    if (!tag[u * 2 + 1] && cnt[u * 2 + 1]) deep_most[u] = max(deep_most[u], deep_most[u * 2 + 1] + 1);
    if (!tag[u * 2 + 2] && cnt[u * 2 + 2]) deep_most[u] = max(deep_most[u], deep_most[u * 2 + 2] + 1);
}

int climb(int u) {
    int ls = u * 2 + 1, rs = u * 2 + 2;
    if (tag[ls] && tag[rs]) return u;
    if (tag[ls]) {
        if (cnt[rs]) return climb(rs);
        else if (cnt[ls]) return climb(ls);
        else return u;
    }
    if (tag[rs]) {
        if (cnt[ls]) return climb(ls);
        else if (cnt[rs]) return climb(rs);
        else return u;
    }
    if (cnt[ls] && (deep_most[ls] >= deep_most[rs] || !cnt[rs])) return climb(ls);
    if (cnt[rs] && (deep_most[ls] <= deep_most[rs] || !cnt[ls])) return climb(rs);
    return u;
}

void cnt_less(int u) {
    cnt[u]--; 
    if (u) cnt_less((u - 1) >> 1);
    deep_most[u] = max(deep_most[u * 2 + 1], deep_most[u * 2 + 2]) + 1;
}

void try_subtask3() {
    build(0);
    // belong[0] = 1;
    // set_belong(0, 0);
    // for (int i = 0; i < n; i++) order[i] = i;
    // sort(order, order + n, subtask3_cmp);
    for (int i = n; i < N; i++) tag[i] = true;
    for (int i = 0; i < n; i++) {
        // if (i % 2 == 0) ans[i] = order[i >> 1];
        // else ans[i] = order[n - (i >> 1) - 1];
        if (i) make_tag(ans[i - 1], 1);
        ans[i] = climb(0);
        if (i) make_tag(ans[i - 1], 0);
        cnt_less(ans[i]);
        // printf("ans[%d] = %d\n", i, ans[i]);
    }
}

vector<int> createFunTour(int N, int Q) {
    n = N; q = Q;
    ans.resize(n);
    try_subtask3();
    return ans;
    if (n <= 894) solve_subtask_1_2();
    else try_subtask3();
    return ans;
}
