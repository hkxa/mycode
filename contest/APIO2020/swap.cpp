#include "swap.h"

#include <bits/stdc++.h>
using std::vector;
using std::swap;
using std::max;

const int N = 1e5 + 7;

struct Edge {
    int v, w;
    Edge() {}
    Edge(int V, int W) : v(V), w(W) {}
};

int n, m;
int case_type;
vector<Edge> G[N];

namespace case1 {
    int w1, w2, w3;
}

namespace case3 {
    int maximum;
}

bool check_case_type_1(vector<int> &u) {
    for (int i = 0; i < m; i++) 
        if (u[i] != 0) return false;
    return true;
}

bool all_degree_not_more_than_2() {
    for (int i = 0; i < n; i++)
        if (G[i].size() > 2) return false;
    return true;
}

void init(int N, int M, vector<int> U, vector<int> V, vector<int> W) {
    n = N;
    m = M;
    for (int i = 0; i < m; i++) {
        G[U[i]].push_back(Edge(V[i], W[i]));
        G[V[i]].push_back(Edge(U[i], W[i]));
    }
    if (n <= 3) return;
    if (check_case_type_1(U)) {
        case_type = 1;
        case1::w1 = 0;
        case1::w2 = 1;
        case1::w3 = 2;
        if (G[1][0].w < G[2][0].w && G[2][0].w < G[3][0].w && G[1][0].w < G[3][0].w) { case1::w1 = 1; case1::w2 = 2; case1::w3 = 3; }
        if (G[1][0].w < G[2][0].w && G[2][0].w > G[3][0].w && G[1][0].w < G[3][0].w) { case1::w1 = 1; case1::w2 = 3; case1::w3 = 2; }
        if (G[1][0].w > G[2][0].w && G[2][0].w < G[3][0].w && G[1][0].w < G[3][0].w) { case1::w1 = 2; case1::w2 = 1; case1::w3 = 3; }
        if (G[1][0].w > G[2][0].w && G[2][0].w < G[3][0].w && G[1][0].w > G[3][0].w) { case1::w1 = 2; case1::w2 = 3; case1::w3 = 1; }
        if (G[1][0].w < G[2][0].w && G[2][0].w > G[3][0].w && G[1][0].w > G[3][0].w) { case1::w1 = 3; case1::w2 = 1; case1::w3 = 2; }
        if (G[1][0].w > G[2][0].w && G[2][0].w > G[3][0].w && G[1][0].w > G[3][0].w) { case1::w1 = 3; case1::w2 = 2; case1::w3 = 1; }
        for (int i = 4; i < n; i++) {
            if (G[i][0].w < G[case1::w1][0].w) {
                case1::w3 = case1::w2;
                case1::w2 = case1::w1;
                case1::w1 = i;
            } else if (G[i][0].w < G[case1::w2][0].w) {
                case1::w3 = case1::w2;
                case1::w2 = i;
            } else if (G[i][0].w < G[case1::w3][0].w) {
                case1::w3 = i;
            }
        }
    } else if (all_degree_not_more_than_2()) {
        if (m == n - 1) case_type = 2;
        else {
            case_type = 3;
            for (int i = 0; i < m; i++)
                case3::maximum = max(case3::maximum, W[i]);
        }
    } else case_type = 20;
}

int getMinimumFuelCapacity(int x, int y) {
    if (n <= 3) return -1;
    else if (case_type == 1) {
        if (x == 0) swap(x, y);
        if (y == 0) {
            if (x == case1::w1) return G[case1::w3][0].w;
            else return max(G[x][0].w, G[case1::w3][0].w);
        } else {
            if (x == case1::w1 && y == case1::w2) return G[case1::w3][0].w;
            else if (x == case1::w2 && y == case1::w1) return G[case1::w3][0].w;
            else return max(G[x][0].w, G[y][0].w);
        }
    } else if (case_type == 2) {
        return -1;
    } else if (case_type == 3) {
        return case3::maximum;
    }
    return 0;
}
