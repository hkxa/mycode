/*************************************
 * @contest:      牛客IOI周赛25-提高组.
 * @author:       brealid.
 * @time:         2021-05-02.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 1000 + 5;

int n, m, q;
char a[N][N];
int sx, sy;
int wall_x, wall_y;
int dis[N][N];
bool inq[N][N];

struct status {
    int x, y;
    int dist;
    status() {}
    status(int X, int Y) : x(X), y(Y), dist(dis[X][Y]) {}
    bool operator < (const status &b) const { return dist > b.dist; }
};
priority_queue<status> que;

inline void try_to_push(int x, int y, int fval) {
    if (x < 1 || y < 1 || x > n || y > m || a[x][y] == 'X' || fval >= dis[x][y]) return;
    dis[x][y] = fval;
    if (!inq[x][y]) {
        inq[x][y] = true;
        que.push(status(x, y));
    }
}

void dij() {
    memset(dis, 0x3f, sizeof(dis));
    try_to_push(sx, sy, 0);
    while (!que.empty()) {
        status u = que.top(); que.pop();
        inq[u.x][u.y] = false;
        int next_dis = dis[u.x][u.y] + 1;
        try_to_push(u.x + 1, u.y, next_dis);
        try_to_push(u.x - 1, u.y, next_dis);
        try_to_push(u.x, u.y + 1, next_dis);
        try_to_push(u.x, u.y - 1, next_dis);
        try_to_push(u.x + 1, u.y + 1, next_dis);
        try_to_push(u.x + 1, u.y - 1, next_dis);
        try_to_push(u.x - 1, u.y + 1, next_dis);
        try_to_push(u.x - 1, u.y - 1, next_dis);
    }
}

#define min3(a, b, c) min(min(a, b), c)

bool flood_vis[N][N];

void flood(int x, int y) {
    flood_vis[x][y] = true;
    if (a[x][y - 1] == 'X' && !flood_vis[x][y - 1]) flood(x, y - 1);
    if (a[x][y + 1] == 'X' && !flood_vis[x][y + 1]) flood(x, y + 1);
    if (a[x - 1][y] == 'X' && !flood_vis[x - 1][y]) flood(x - 1, y);
    if (a[x + 1][y] == 'X' && !flood_vis[x + 1][y]) flood(x + 1, y);
}

void solve_special() {
    for (int i = 1; i <= q; ++i) {
        kin >> sx >> sy;
        memset(flood_vis, 0, sizeof(flood_vis));
        int ans = N * N * 2;
        for (int i = 1; i <= n; ++i)
            for (int j = 1; j <= m; ++j)
                if (a[i][j] == 'X' && !flood_vis[i][j]) {
                    if (j == sy && sx < i) {
                        if (a[i + 1][j] == 'X') continue;
                        flood(i, j);
                        if (i == 1) continue;
                        int dn = i;
                        while (a[dn + 1][j] == '.') a[++dn][j] = 'X';
                        dij();
                        for (int x = dn; x > i; --x) {
                            a[x][j] = '.';
                            ans = min(ans, min3(dis[x - 1][j - 1], dis[x][j - 1], dis[x + 1][j - 1]) +
                                           min3(dis[x - 1][j + 1], dis[x][j + 1], dis[x + 1][j + 1]) + 2);
                        }
                    } else {
                        flood(i, j);
                        if (i == 1) continue;
                        int up = i;
                        while (a[up - 1][j] == '.') a[--up][j] = 'X';
                        dij();
                        for (int x = up; x < i; ++x) {
                            a[x][j] = '.';
                            ans = min(ans, min3(dis[x - 1][j - 1], dis[x][j - 1], dis[x + 1][j - 1]) +
                                        min3(dis[x - 1][j + 1], dis[x][j + 1], dis[x + 1][j + 1]) + 2);
                        }
                    }
                }
        kout << ans << '\n';
    }
}

signed main() {
    kin >> n >> m >> q;
    for (int i = 1; i <= n; ++i) {
        kin >> (a[i] + 1);
        for (int j = 1; j <= m; ++j)
            if (a[i][j] == 'X' && !wall_x && !wall_y) wall_x = i, wall_y = j;
    }
    if (n <= 8 && m <= 8) {
        solve_special();
        return 0;
    }
    for (int i = 1; i <= q; ++i) {
        kin >> sx >> sy;
        a[sx][sy] = '*';
        // for (int i = 1; i <= n; ++i)
        //     for (int j = 1; j <= m; ++j)
        //         if (a[i][j] == 'X' && !wall_x && !wall_y) wall_x = i, wall_y = j;
        for (int i = 1; i < wall_x; ++i) a[i][wall_y] = 'X';
        dij();
        int ans = N * N * 2;
        for (int i = 1; i < wall_x; ++i) {
            a[i][wall_y] = '.';
            ans = min(ans, min3(dis[i - 1][wall_y - 1], dis[i][wall_y - 1], dis[i + 1][wall_y - 1]) +
                           min3(dis[i - 1][wall_y + 1], dis[i][wall_y + 1], dis[i + 1][wall_y + 1]) + 2);
        }
        kout << ans << '\n';
        a[sx][sy] = '.';
    }
    return 0;
}