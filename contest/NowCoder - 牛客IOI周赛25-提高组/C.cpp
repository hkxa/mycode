/*************************************
 * @contest:      牛客IOI周赛25-提高组.
 * @author:       brealid.
 * @time:         2021-05-02.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 6e5 + 7, P = 998244353;

bool isnp[N];
int primes[N], pcnt;
int mu[N];

void init(int n = 6e5) {
    mu[1] = 1;
    for (int i = 2; i <= n; ++i) {
        if (!isnp[i]) primes[++pcnt] = i, mu[i] = -1;
        for (int j = 1; j <= pcnt && i * primes[j] <= n; ++j) {
            isnp[i * primes[j]] = true;
            if (i % primes[j] == 0) break;
            mu[i * primes[j]] = -mu[i];
        }
    }
}


int gcd(int x, int y) {
    return x ? gcd(y % x, x) : y;
}

int n, a[N], b[N];
int x[N], y[N];
int64 ans[N];
// vector<int> gcd_eq_1[N];
// vector<int> factor[N];
// vector<int> Mem[N];

void BruteForce() {
    for (int i = 1; i <= n; ++i)
        for (int j = 1; j <= n; ++j)
            ans[i + j + gcd(i, j)] += (int64)a[i] * b[j] % P;
    for (int k = 1; k <= 3 * n; ++k)
        kout << ans[k] % P << " \n"[k == 3 * n];
}

int64 query(int t, int step) {
    // if (t <= n) return Mem[step][t / step];
    int64 res = 0;
    for (int i = step; i < t; i += step) res = (res + (int64)a[i] * b[t - i]) % P;
    return res;
}

signed main() {
    kin >> n;
    for (int i = 1; i <= n; ++i) kin >> a[i];
    for (int i = 1; i <= n; ++i) kin >> b[i];
    // if (n <= 2000) {
    //     BruteForce();
    //     return 0;
    // }
    // for (int g = 1; g <= n; ++g)
    //     for (int k = g; k <= 3 * n; k += g) factor[k].push_back(g);
    init(n * 3);
    // for (int g = 1; g <= n; ++g) {
    //     int mx = n / g;
    //     Mem[g].resize(mx + 1);
    //     for (int i = 1; i <= mx; ++i) {
    //         // x[i] = (x[i - 1] + a[i * g]) % P;
    //         // y[i] = (y[i - 1] + b[i * g]) % P;
    //         x[i] = a[i * g], y[i] = b[i * g];
    //         int &now = Mem[g][i];
    //         for (int s = 1; s < i; ++s) now = (now + (int64)x[s] * y[i - s]) % P;
    //     }
    // }
    for (int g = 1; g <= n; ++g) {
        // int mx = n / g;
        // for (int i = 1; i <= mx; ++i) x[i] = a[i * g], y[i] = b[i * g];
        for (int d = 1; g * d <= n; ++d) {
            int gd = g * d;
            for (int k = gd + g; k <= 3 * n; k += gd) {
                ans[k] += mu[d] * query(k - g, gd);
            }
        }
    }
    for (int k = 1; k <= 3 * n; ++k)
        kout << (ans[k] % P + P) % P << " \n"[k == 3 * n];
    return 0;
}