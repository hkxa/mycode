/*************************************
 * problem:      A.
 * user name:    Jomoo.
 * time:         2019-09-21.
 * language:     C++.
 * upload place: Comet OJ.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define ForInVec(vectorType, vectorName, iteratorName) for (vector<vectorType>::iterator iteratorName = vectorName.begin(); iteratorName != vectorName.end(); iteratorName++)
#define ForInVI(vectorName, iteratorName) ForInVec(int, vectorName, iteratorName)
#define ForInVE(vectorName, iteratorName) ForInVec(Edge, vectorName, iteratorName)
#define MemWithNum(array, num) memset(array, num, sizeof(array))
#define Clear(array) MemWithNum(array, 0)
#define MemBint(array) MemWithNum(array, 0x3f)
#define MemInf(array) MemWithNum(array, 0x7f)
#define MemEof(array) MemWithNum(array, -1)
#define ensuref(condition) do { if (!(condition)) exit(0); } while(0)

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

struct FastIOer {
#   define createReadlnInt(type)            \
    FastIOer& operator >> (type &x)         \
    {                                       \
        x = read<type>();                   \
        return *this;                       \
    }
    createReadlnInt(short);
    createReadlnInt(unsigned short);
    createReadlnInt(int);
    createReadlnInt(unsigned);
    createReadlnInt(long long);
    createReadlnInt(unsigned long long);
#   undef createReadlnInt

#   define createWritelnInt(type)           \
    FastIOer& operator << (type &x)         \
    {                                       \
        write<type>(x);                     \
        return *this;                       \
    }
    createWritelnInt(short);
    createWritelnInt(unsigned short);
    createWritelnInt(int);
    createWritelnInt(unsigned);
    createWritelnInt(long long);
    createWritelnInt(unsigned long long);
#   undef createWritelnInt
    
    FastIOer& operator >> (char &x)
    {
        x = getchar();
        return *this;
    }
    
    FastIOer& operator << (char x)
    {
        putchar(x);
        return *this;
    }
    
    FastIOer& operator << (const char *x)
    {
        int __pos = 0;
        while (x[__pos]) {
            putchar(x[__pos++]);
        }
        return *this;
    }
} fast;

int n, m, k;
int w[1007];
int a[107], b[107];
int f[3007]; // 损失 i 个节点所能获得的最大收益(结束时);
// (a[i], b[i]) => (i, f[i])
int g[107][3007]; // day : i-th; start : j;
// int g[1007][1007]; // 开始有 j 个节点, 结束有 j 个节点;
// int mx[107][3007];
int ans(0);

struct ele {
    int day;
    int rest;
    int get;
    ele(int d = 0, int r = 0, int g = 0) : day(d), rest(r), get(g) {}
};

queue<ele> q;

int main()
{
    memset(f, -1, sizeof(f));
    // memset(mx, -1, sizeof(mx));
    memset(g, -1, sizeof(g));
    fast >> n >> m >> k;
    for (int i = 0; i <= k; i++) fast >> w[i];
    for (int i = 1; i <= m; i++) fast >> a[i] >> b[i];
    f[0] = 0;
    for (int i = 1; i <= 2000; i++) {
        for (int j = 1; j <= m; j++) {
            if (i >= a[j] && ~f[i - a[j]]) f[i] = max(f[i], f[i - a[j]] + b[j]);
        }
        // printf("%d ", f[i]);
    }
    // putchar('\n');
    g[0][0] = 0;
    for (int i = 0; i <= n; i++) {
        for (int j = 0; j <= 2000; j++) {
            if (g[i][j] == -1) continue;
            if (i == n) {
                ans = max(ans, g[i][j] + j);
                continue;
            }
            for (int l = 0; l <= min(j, k); l++) {
                g[i + 1][l + w[l]] = max(g[i + 1][l + w[l]], g[i][j] + f[j - l]);
            }
        }
    }
    fast << ans;
    // q.push(ele(0, 0, 0));
    // int deal = 0;
    // while (!q.empty()) {
    //     ele now = q.front(); q.pop();
    //     if (now.get < mx[now.day][now.rest]) continue;
    //     else mx[now.day][now.rest] = now.get; // up 2 lines : 16106 => 730 / 800
    //     // printf("(%d, %d, %d)\n", now.day, now.rest, now.get);
    //     deal++;
    //     if (now.day != n) {
    //         for (int i = max(0, now.rest - k); i <= now.rest; i++) {
    //             if (~f[i]) q.push(ele(now.day + 1, now.rest - i + w[now.rest - i], now.get + f[i]));
    //         }
    //     } else ans = max(ans, now.get + now.rest);
    // }
    // fast << ans;
    // printf("\ndeal times : %d.\n", deal);

    // for (int i = 1; i <= k; i++) {
    //     f[i] = max(f[i - 1], f[i]);
    //     printf("%d ", f[i]);
    // }
    // putchar('\n');

    // g[1][0] = 0;
    // for (int i = 1; i <= n; i++) {
    //     for (int j = 0; j <= k; j++) {
    //         for (int l = j; l <= k; l++) {
    //             g[i][l]
    //         }
    //     }
    // }
    return 0;
}

/*
5 5 10
30 29 29 28 28 27 26 26 25 24 24
2 1
3 2
5 3
8 5
13 8
*/