/*************************************
 * @contest:      Harbour.Space Scholarship Contest 2021-2022 (open for everyone, rated, Div. 1 + Div. 2).
 * @author:       hkxadpall.
 * @time:         2021-07-22.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

namespace kmath/* 数学资源库的名字 */ {
    typedef unsigned uint32;
    typedef long long int64;
    typedef unsigned long long uint64;

    template <typename T>
    inline T gcd_BitOptimize_ull(T a, T b) {
        if (!a || !b) return a | b;
        int t = __builtin_ctzll(a | b);
        a >>= __builtin_ctzll(a);
        do {
            b >>= __builtin_ctzll(b);
            if (a > b) swap(a, b);
            b -= a;
        } while(b);
        return a << t;
    }
    template <typename T>
    inline T gcd_BitOptimize_ui(T a, T b) {
        if (!a || !b) return a | b;
        int t = __builtin_ctz(a | b);
        a >>= __builtin_ctz(a);
        do {
            b >>= __builtin_ctz(b);
            if (a > b) swap(a, b);
            b -= a;
        } while(b);
        return a << t;
    }
    template <typename T>
    T gcd_EuclidMethod(T a, T b) {
        return a ? gcd_EuclidMethod(b % a, a) : b;
    }

    template <typename T>
    inline T fmul(T a, T b, T p) {
        T ret = 0;
        while (b) {
            if (b & 1) if ((ret += a) >= p) ret -= p;
            if ((a <<= 1) >= p) a -= p; 
            b >>= 1;
        }
        return ret;
    }
    template <typename T>
    inline T fpow_FmulMethod(T a, T b, T p) {
        T ret = 1;
        while (b) {
            if (b & 1) ret = fmul(ret, a, p);
            a = fmul(a, a, p);
            b >>= 1;
        }
        return ret;
    }
    template <typename T, typename UpTurn = int64>
    inline T fpow(T a, T b, T p) {
        T ret = 1;
        while (b) {
            if (b & 1) ret = (UpTurn)ret * a % p;
            a = (UpTurn)a * a % p;
            b >>= 1;
        }
        return ret;
    }
    template <typename T> inline T gcd(T a, T b) { return gcd_EuclidMethod(a, b); }
    template <> inline int64 gcd(int64 a, int64 b) { return gcd_BitOptimize_ull(a, b); }
    template <> inline uint64 gcd(uint64 a, uint64 b) { return gcd_BitOptimize_ull(a, b); }
    template <> inline int gcd(int a, int b) { return gcd_BitOptimize_ui(a, b); }
    template <> inline uint32 gcd(uint32 a, uint32 b) { return gcd_BitOptimize_ui(a, b); }

    template <typename T>
    inline T lcm(T n, T m) {
        return n / gcd(n, m) * m;
    }

    template<typename Ta, typename Tb, typename ...Targ>
    inline Ta gcd(Ta a, Tb b, Targ ...arg) {
        return gcd(gcd(a, b), arg...);
    }

    template<typename Ta, typename Tb, typename ...Targ>
    inline Ta lcm(Ta a, Tb b, Targ ...arg) {
        return lcm(lcm(a, b), arg...);
    }

    template <typename T>
    void exgcd(T n, T m, T &x, T &y) {
        if (n == 1) x = 1, y = 0;
        else exgcd(m, n % m, y, x), y -= n / m * x;
    }

    template <typename T>
    T inv(T a, T m) {
        T x, y;
        exgcd(a, m, x, y);
        return (x % m + m) % m;
    }

    template <typename T>
    inline T inv_FimaMethod(T a, T m) {
        return fpow(a, m - 2, m);
    }

    template<int N, int64 ModP>
    struct CombinationNumber {
        int64 fac[N + 2], ifac[N + 2];
        CombinationNumber() {
            fac[0] = 1;
            for (int i = 1; i <= N; ++i) fac[i] = fac[i - 1] * i % ModP;
            ifac[N] = inv(fac[N], ModP);
            for (int i = N; i >= 1; --i) ifac[i - 1] = ifac[i] * i % ModP;
        }
        inline int64 C(int n, int m) {
            return fac[n] * ifac[n - m] % ModP * ifac[m] % ModP;
        }
        inline int64 P(int n, int m) {
            return fac[n] * ifac[n - m] % ModP;
        }
    };
};

const int N = 1.5e5 + 7, V = 1e6 + 7;

int n, q;
struct UnionFindSet {
    int fa[V + 5];
    UnionFindSet() {
        memset(fa, -1, sizeof(fa));
    }
    // 重置清零（下标从 1 开始）
    inline void reset(int n = sizeof(fa) / sizeof(int) - 1) {
        memset(fa, -1, (n + 1) * sizeof(int));
    }
    // 寻找 u 的祖先
    int find(int u) {
        return fa[u] < 0 ? u : fa[u] = find(fa[u]);
    }
    // 判断 u 是否为所在树的跟
    inline int is_root(int u) {
        return fa[u] < 0;
    }
    // 获取 u 所属的连通块的大小
    inline int get_size(int u) {
        return -fa[find(u)];
    }
    // 判断两个节点是否处于一棵树中
    inline bool is_family(int u, int v) {
        return find(u) == find(v);
    }
    // 合并两棵树，成功返回新的树的根节点编号，已经在同一棵树中了返回 0
    inline int merge(int u, int v) {
        u = find(u), v = find(v);
        if (u == v) return 0;
        if (fa[u] > fa[v]) swap(u, v);
        fa[u] += fa[v];
        return fa[v] = u;
    }
    // 合并两棵树，成功返回新的树的根节点编号，已经在同一棵树中了返回 0
    // f: 一个函数，若合并成功则会调用
    // 两个参数为新连边的两个端点。第一个参数是新的父亲节点，第二个参数是新的孩子节点
    template <typename FunctionType>
    inline int merge(int u, int v, FunctionType f) {
        u = find(u), v = find(v);
        if (u == v) return 0;
        if (fa[u] > fa[v]) swap(u, v);
        fa[u] += fa[v];
        f(u, v);
        return fa[v] = u;
    }
} ufs;
unordered_set<int> factor[N], relative_factor[N];
int a[N], wh[V];
bool factorGet_able[V];

bool is_reachable(int x, int y) {
    for (int xx: relative_factor[x])
        if (ufs.is_family(xx, y)) return true;
    return false;
}

bool is_inGG(int x) {
    if (x == 1) return false;
    for (int num = sqrt(x); num > 1; --num)
        if (x % num == 0)
            if (factorGet_able[num] || factorGet_able[x / num]) return true;
    return false;
}

signed main() {
    kin >> n >> q;
    // for (int i = 2; i <= 1000000; ++i)
    //     for (int j = i + i; j <= 1000000; ++j)
    //         ufs.merge(i, j);
    for (int i = 1; i <= n; ++i) {
        kin >> a[i];
        wh[a[i]] = i;
        ufs.merge(a[i], a[i]);
        for (int num = sqrt(a[i]); num > 1; --num)
            if (a[i] % num == 0) {
                ufs.merge(num, a[i]);
                factor[i].insert(num);
                if (a[i] != num * num) {
                    ufs.merge(a[i] / num, a[i]);
                    factor[i].insert(a[i] / num);
                }
            }
    }
    for (int i = 1; i <= n; ++i) {
        unordered_set<int> ffactor;
        int VAL = a[i] + 1;
        ffactor.insert(VAL);
        factorGet_able[VAL] = true;
        for (int num = sqrt(VAL); num > 1; --num)
            if (VAL % num == 0) {
                ffactor.insert(num);
                factorGet_able[num] = true;
                if (VAL != num * num) {
                    ffactor.insert(VAL / num);
                    factorGet_able[VAL / num] = true;
                }
            }
        relative_factor[i].insert(ffactor.begin(), ffactor.end());
        for (int f: factor[i])
            if (wh[f]) relative_factor[wh[f]].insert(ffactor.begin(), ffactor.end());
    }
    for (int i = 1, x, y; i <= q; ++i) {
        kin >> x >> y;
        if (ufs.is_family(a[x], a[y])) kout << "0\n";
        else if (is_reachable(x, a[y]) || is_reachable(y, a[x]) || is_inGG(kmath::gcd(a[x], a[y]))) kout << "1\n";
        else kout << "2\n";
    }
    return 0;
}