/*************************************
 * @contest:      Harbour.Space Scholarship Contest 2021-2022 (open for everyone, rated, Div. 1 + Div. 2).
 * @author:       hkxadpall.
 * @time:         2021-07-22.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 3e5 + 7, B = 550;

struct binary_index_tree {
    int64 tr[N];
    binary_index_tree() { memset(tr, 0, sizeof(tr)); }
    void inc(int p, int v = 1) {
        for (; p < N; p += (p & -p)) tr[p] += v;
    }
    int64 qry(int p) {
        int64 ret(0);
        for (; p; p -= (p & -p)) ret += tr[p];
        return ret;
    }
    int64 qry(int l, int r) {
        int64 ret(0);
        for (; r; r -= (r & -r)) ret += tr[r];
        for (--l; l; l -= (l & -l)) ret -= tr[l];
        return ret;
    }
} cnt, sum;

bool occur550[B + 1];
int modN[B + 1][B + 1];

signed main() {
    int n;
    int64 a, ans = 0;
    kin >> n;
    for (int i = 1; i <= n; ++i) {
        kin >> a;
        for (int num = 1; num <= B; ++num)
            if (occur550[num]) ans += a % num + num % a;
        if (a > B) {
            // a % other
            ans += cnt.qry(a + 1, N - 1) * a;
            for (int bs = 1; ; ++bs) {
                int l = a / (bs + 1) + 1, r = a / bs;
                if (r < B) break;
                // if (l < B) l = B;
                // if (a == 2 && bs == 1) printf("l = %d, r = %d\n", l, r);
                ans += a * cnt.qry(l, r) - bs * sum.qry(l, r);
                // if (a * cnt.qry(l, r) - bs * sum.qry(l, r)) printf("[a %% other] a = %lld, other = [%d, %d], ans += %lld\n", a, l, r, a * cnt.qry(l, r) - bs * sum.qry(l, r));
            }
            // other % a
            ans += sum.qry(1, a - 1);
            for (int bs = 1; ; ++bs) {
                int l = a * bs, r = a * (bs + 1) - 1;
                if (l >= N) break;
                if (r >= N) r = N - 1;
                ans += sum.qry(l, r) - bs * a * cnt.qry(l, r);
                // if (sum.qry(l, r) - bs * a * cnt.qry(l, r)) printf("[other %% a] a = %lld, other = [%d, %d], ans += %lld\n", a, l, r, sum.qry(l, r) - bs * a * cnt.qry(l, r));
            }
            cnt.inc(a), sum.inc(a, a);
            for (int mod_number = 1; mod_number <= B; ++mod_number)
                ++modN[mod_number][a % mod_number];
        } else {
            // printf("a = %lld, ans = %lld\n", a, ans);
            // a % other
            ans += a * cnt.qry(B + 1, N - 1);
            // other % a
            // printf("a = %lld, ans = %lld\n", a, ans);
            for (int mod_result = 0; mod_result < a; ++mod_result)
                ans += modN[a][mod_result] * mod_result;
            // printf("a = %lld, ans = %lld\n", a, ans);
            occur550[a] = true;
        }
        kout << ans << " \n"[i == n];
    }
    return 0;
}