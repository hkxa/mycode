//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      2020 年百度之星·程序设计大赛 - 初赛一.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-19.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

int n, x, y;
int g[5][12][12][12];
int ans;

int calc(int a, int b, int c, int d) {
    int gain[9];
    gain[0] = a;
    for (int i = 1; i <= b; i++) gain[i] = 3;
    for (int i = b + 1; i <= b + c; i++) gain[i] = 2;
    for (int i = b + c + 1; i <= b + c + d; i++) gain[i] = 1;
    for (int i = b + c + d + 1; i < 9; i++) gain[i] = 0;
    int now = gain[0], food = 0;
    int pos = 1, cnt = 0;
    while (pos < 9) {
        food += now;
        if (food >= 8 * pos * pos) {
            now += gain[pos];
            pos++;
        }
        cnt++;
    }
    // printf("g[%d][%d][%d][%d] = %d\n", a, b, c, d, cnt);
    return cnt;
}

void pretreat_G() {
    for (int i = 1; i <= 3; i++)
        for (int j = 0; j <= 8; j++)
            for (int k = 0; k <= 8 - j; k++)
                for (int l = 0; l <= 8 - j - k; l++)
                    g[i][j][k][l] = calc(i, j, k, l);

}

#define JuSum(k, i, j) (sum[k][i + 3][j + 3] - sum[k][i - 4][j + 3] - sum[k][i + 3][j - 4] + sum[k][i - 4][j - 4] - (a[i][j] == k))

signed main() {
    pretreat_G();
    int T = read.get_int<int>();
    while (T--) {
        int a[601][601];
        int sum[4][601][601];
        read >> n >> x >> y;
        x += 4; y += 4; n += 4;
        for (int i = 5; i <= n; i++)
            for (int j = 5; j <= n; j++) {
                read >> a[i][j];
            }
        for (int i = 5; i <= n + 4; i++)
            for (int j = 5; j <= n + 4; j++) {
                if (i > n || j > n) a[i][j] = 0;
                sum[1][i][j] = sum[1][i - 1][j] + sum[1][i][j - 1] - sum[1][i - 1][j - 1] + (a[i][j] == 1);
                sum[2][i][j] = sum[2][i - 1][j] + sum[2][i][j - 1] - sum[2][i - 1][j - 1] + (a[i][j] == 2);
                sum[3][i][j] = sum[3][i - 1][j] + sum[3][i][j - 1] - sum[3][i - 1][j - 1] + (a[i][j] == 3);
            }
        ans = 1e9 + 9;
        for (int i = 5; i <= n; i++)
            for (int j = 5; j <= n; j++) {
                int ans1 = (abs(i - x) + abs(j - y) + 1) / 2;
                int q1 = JuSum(1, i, j), q2 = JuSum(2, i, j), q3 = JuSum(3, i, j);
                q3 = min(q3, 8);
                q2 = min(q2, 8 - q3);
                q1 = min(q1, 8 - q2 - q3);
                // printf("pos(%d, %d) : ", i - 4, j - 4);
                // printf("query g[%d][%d][%d][%d] : %d\n", a[i][j], q3, q2, q1, ans1 + g[a[i][j]][q3][q2][q1]);
                ans = min(ans, ans1 + g[a[i][j]][q3][q2][q1]);
            }
        write << ans << '\n';
    }
    return 0;
}