//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      2020 年百度之星·程序设计大赛 - 初赛一.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-19.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

int f[4][407];

signed main() {
    for (int i = 95; i <= 100; i++) f[0][i] = 43;
    for (int i = 90; i <= 94; i++) f[0][i] = 40;
    for (int i = 85; i <= 89; i++) f[0][i] = 37;
    for (int i = 80; i <= 84; i++) f[0][i] = 33;
    for (int i = 75; i <= 79; i++) f[0][i] = 30;
    for (int i = 70; i <= 74; i++) f[0][i] = 27;
    for (int i = 67; i <= 69; i++) f[0][i] = 23;
    for (int i = 65; i <= 66; i++) f[0][i] = 20;
    for (int i = 62; i <= 64; i++) f[0][i] = 17;
    for (int i = 60; i <= 61; i++) f[0][i] = 10;
    for (int i = 0; i <= 59; i++) f[0][i] = 0;
    for (int k = 1; k < 4; k++) {
        for (int score = (k + 1) * 100; score >= 0; score--) {
            for (int las = max(0, score - 100); las <= min(score, k * 100); las++) {
                f[k][score] = max(f[k][score], f[k - 1][las] + f[0][score - las]);
            }
        }
    }
    int T = read.get_int<int>(), x;
    while (T--) {
        read >> x;
        write << f[3][x] / 10 << '.' << f[3][x] % 10 << '\n';
    }
    return 0;
}