//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      QiZhiShu - 2020(夏)-SX1.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-24.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64
#define NotNeg(expression) max(expression, 0LL)
#define ChooseSubtree(u, v) (mxR[u] - NotNeg(sum[v]) + NotNeg(sum[1] - sum[u]))

const int N = 1e5 + 7;
// const int64 LL_min = LONG_LONG_MIN / 3;
const int64 LL_min = -0x3f3f3f3f3f3f3f3f;

int n, m;
int64 val[N], sum[N], mx1[N], mx2[N], mxR[N], mxU[N], res[N];
vector<int> G[N];
bool vis[N], cir[N];
int64 FinalAns;

int FindCircle(int u, int fa) {
    vis[u] = 1;
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i];
        if (v != fa && !cir[v]) {
            if (vis[v]) {
                cir[u] = true;
                return v;
            } else {
                int ret = FindCircle(v, u);
                if (ret) {
                    cir[u] = true;
                    return u == ret ? 0 : ret;
                }
            }
        }
    }
    return 0;
}

void dfs1(int u, int fa) {
    // printf("%s : %d, %d\n", __FUNCTION__, u, fa);
    res[u] = mx1[u] = mx2[u] = LL_min;
    mxR[u] = sum[u] = val[u];
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i];
        if (v != fa && (!cir[u] || !cir[v])) {
            dfs1(v, u);
            sum[u] += sum[v];
            mxR[u] += NotNeg(sum[v]);
            if (res[v] > mx1[u]) {
                mx2[u] = mx1[u];
                mx1[u] = res[v];
            } else if (mx1[v] > mx2[u]) {
                mx2[u] = res[v];
            }
        }
    }
    if (u == n + 1) mxR[u] = LL_min;
    res[u] = max(mx1[u], mxR[u]);
}

void dfs2(int u, int fa) {
    // printf("%s : %d, %d\n", __FUNCTION__, u, fa);
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i];
        if (v != fa && (!cir[u] || !cir[v])) {
            if (u != n + 1) mxU[v] = ChooseSubtree(u, v);
            else mxU[v] = LL_min;
            if (u != 1) mxU[v] = max(mxU[v], mxU[u]);
            if (mx1[u] == res[v]) mxU[v] = max(mxU[v], mx2[u]);
            else mxU[v] = max(mxU[v], mx1[u]);
            dfs2(v, u);
            if (u != n + 1) FinalAns = max(FinalAns, ChooseSubtree(u, v) + res[v]);
        }
    }
    if (u != 1) FinalAns = max(FinalAns, mxU[u] + mxR[u]);
}

void circleBasedTree_PreSolve() {
    FindCircle(1, 0);
    // for (int i = 1; i <= n; i++)
    //     if (cir[i]) printf("cir %d\n", i);
    for (int i = 1; i <= n; i++)
        if (cir[i]) {
            G[n + 1].push_back(i);
            G[i].push_back(n + 1);
        }
}

void solve() {
    // printf("Enter solve\n");
    dfs1(1, 0);
    FinalAns = LL_min;
    dfs2(1, 0);
    // for (int u = 1; u <= n; u++)
    //     printf("%-3d : %-20lld %-20lld %-20lld %-20lld %-20lld %-20lld\n", u, sum[u], mx1[u], mx2[u], mxR[u], res[u], mxU[u]);
    write << FinalAns << '\n';
}

signed main() {
    int CasesCount = baseFastio::read<int>();
    while (CasesCount--) {
        read >> n >> m;
        for (int i = 1; i <= n; i++) {
            read >> val[i];
            G[i].clear();
            cir[i] = vis[i] = 0;
        }
        val[n + 1] = cir[n + 1] = vis[n + 1] = 0;
        G[n + 1].clear();
        for (int i = 1, u, v; i <= m; i++) {
            read >> u >> v;
            G[u].push_back(v);
            G[v].push_back(u);
        }
        if (m == n) circleBasedTree_PreSolve();
        solve();
    }
    return 0;
}