/*************************************
 * user name:    Jmoo.
 * language:     C++.
 * upload place: XJOI (dev.xjoi.net).
*************************************/ 
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define N 200007

struct Edge {
    int b; int64 l;
};

int n;
int c[N];
vector<Edge> G[N];
long double f[N][3] = {0}; // $f[u][0] = size\\f[u][1] = ∑dist(u\rightarrow x)*c[x](u∈{son_u})\\f[u][2]= ∑dist(u\rightarrow x)^2*c[x](u∈{son_u})$
long double g[N][3] = {0}; // $g[u][1],g[u][2]:以 u 为顶点的树, 含义同 f$
#define sum f[1][0]
#define child(u) f[u][0]
#define rest(u) (sum - child(u))

void dfs1(int u, int fa)
{
    f[u][0] = c[u];
    for (unsigned i = 0; i < G[u].size(); i++) {
        int v = G[u][i].b; int64 w = G[u][i].l;
        if (v != fa) {
            dfs1(v, u);
            f[u][0] += f[v][0];
            f[u][1] += f[v][1] + w * f[v][0];
            f[u][2] += f[v][2] + 2 * w * f[v][1] + f[v][0] * w * w;
        }
    }
}

void dfs2(int u, int fa)
{
    // printf("g[%d] = {%.2Lf, %.2Lf}\n", u, g[u][1], g[u][2]);
    for (unsigned i = 0; i < G[u].size(); i++) {
        int v = G[u][i].b; int64 w = G[u][i].l;
        if (v != fa) {
            g[v][1] = g[u][1] + w * rest(v) - w * child(v);
            g[v][2] = g[u][2] + 2 * w * g[u][1] - f[v][1] * w * 4 + sum * w * w - f[v][0] * 4 * w * w;
            dfs2(v, u);
        }
    }
}

int main()
{
    freopen("problem3.in", "r", stdin);
    freopen("problem3.out", "w", stdout);
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        c[i] = read<int>();
    }
    for (int i = 1, a, b, l; i < n; i++) {
        a = read<int>();
        b = read<int>();
        l = read<int>();
        G[a].push_back((Edge){b, l});
        G[b].push_back((Edge){a, l});
    }
    dfs1(1, 0);
    g[1][1] = f[1][1];
    g[1][2] = f[1][2];
    dfs2(1, 0);
    long double ans = g[1][2];
    for (int i = 2; i <= n; i++) ans = min(ans, g[i][2]);
    printf("%.10Lf", ans);
    return 0;
}

/*
10
100000 999700 979008 900797 970096 799005 909704 970930 979020 909071  
1 2 998221
1 3 996231
1 4 932933
1 5 994124
2 6 923523
3 7 345642
4 8 997322
5 9 998311
7 10 992911
20759004481412996240.0000000000
20759004481412996240.0000000000
*/