/*************************************
 * user name:    Jmoo.
 * language:     C++.
 * upload place: XJOI (dev.xjoi.net).
*************************************/ 
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, m;
struct Element {
    int adder;
    int v, pre, nxt;
    Element() : adder(0) {}
    void pushNum(int val) { v = val; } 
    void pushAdder(int val) { adder = 1; v = val; } 
    void pushPre(int n) { pre = n; } 
    void pushNxt(int n) { nxt = n; } 
} ele[2000007]; int top = 0;
int l[1000007] = {0}, r[1000007] = {0};
int opt, idx, x;

void PushLeft(int idx, int v)
{
    ele[++top].pushNum(v);
    ele[top].pushPre(0);
    ele[top].pushNxt(l[idx]);
    if (l[idx]) ele[l[idx]].pushPre(top);
    l[idx] = top;
    if (!r[idx]) r[idx] = top;
}

int PopLeft(int idx)
{
    int addTag = 0, res;
    while (ele[l[idx]].adder) {
        addTag = (addTag + ele[l[idx]].v) % 1000000007;
        l[idx] = ele[l[idx]].nxt;
    }
    res = ele[l[idx]].v;
    l[idx] = ele[l[idx]].nxt;
    if (!l[idx]) r[idx] = 0;
    ele[++top].pushAdder(addTag);
    ele[top].pushPre(0);
    ele[top].pushNxt(l[idx]);
    if (l[idx]) ele[l[idx]].pushPre(top);
    l[idx] = top;
    if (!r[idx]) r[idx] = top;
    return ((addTag + res) % 1000000007 + 1000000007) % 1000000007;
}

void PushRight(int idx, int v)
{
    ele[++top].pushNum(v);
    ele[top].pushNxt(0);
    ele[top].pushPre(r[idx]);
    if (r[idx]) ele[r[idx]].pushNxt(top);
    r[idx] = top;
    if (!l[idx]) l[idx] = top;
}

int PopRight(int idx)
{
    int addTag = 0, res;
    while (ele[r[idx]].adder) {
        addTag = (addTag - ele[r[idx]].v) % 1000000007;
        r[idx] = ele[r[idx]].pre;
    }
    res = ele[r[idx]].v;
    r[idx] = ele[r[idx]].pre;
    if (!r[idx]) l[idx] = 0;
    ele[++top].pushAdder(-addTag);
    ele[top].pushNxt(0);
    ele[top].pushPre(r[idx]);
    if (r[idx]) ele[r[idx]].pushNxt(top);
    r[idx] = top;
    if (!l[idx]) l[idx] = top;
    return ((addTag + res) % 1000000007 + 1000000007) % 1000000007;
}

void AddSequence(int idx, int dif)
{
    if (!l[idx]) return;
    // Left
    ele[++top].pushAdder(dif);
    ele[top].pushPre(0);
    ele[top].pushNxt(l[idx]);
    ele[l[idx]].pushPre(top);
    l[idx] = top;
    // Right
    ele[++top].pushAdder(-dif);
    ele[top].pushNxt(0);
    ele[top].pushPre(r[idx]);
    ele[r[idx]].pushNxt(top);
    r[idx] = top;
}

void Join(int i, int j)
{
    if (!l[j]) return;
    if (!l[i]) {
        l[i] = l[j];
        r[i] = r[j];
    } else {
        ele[r[i]].pushNxt(l[j]);
        ele[l[j]].pushPre(r[i]);
        r[i] = r[j];
    }
    l[j] = r[j] = 0;
}

int main()
{
    // freopen("problem2.in", "r", stdin);
    // freopen("problem2.out", "w", stdout);
    n = read<int>();
    m = read<int>();
    for (int i = 1; i <= m; i++) {
        opt = read<int>();
        idx = read<int>();
        switch (opt) {
            case 1 : PushLeft(idx, read<int>()); break;
            case 2 : write(PopLeft(idx), 10); break;
            case 3 : PushRight(idx, read<int>()); break;
            case 4 : write(PopRight(idx), 10); break;
            case 5 : AddSequence(idx, read<int>()); break;
            case 6 : Join(idx, read<int>()); break;
        }
    }
    return 0;
}

/*
2 10
1 1 100
3 1 200
3 2 10
1 2 20
5 2 40
6 1 2
2 1
2 1
2 1
2 1
*/