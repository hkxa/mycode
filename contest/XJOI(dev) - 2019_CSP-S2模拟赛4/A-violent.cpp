/*************************************
 * user name:    Jmoo.
 * language:     C++.
 * upload place: XJOI (dev.xjoi.net).
*************************************/ 
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;
int a[1003];
int ans = 0, cnt;

char s[4][3] = {"th", "st", "nd", "rd"};
char* get(int n)
{
    n = n % 100;
    if (n / 10 == 1) return s[0];
    if (n % 10 == 1) return s[1];
    if (n % 10 == 2) return s[2]; 
    if (n % 10 == 3) return s[3]; 
    return s[0];
}

// 时间复杂度 $O(n!n^2)$
// 找规律得 answer = n! / 2
int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) a[i] = i;
    do {
        cnt = 0;
        for (int i = 1; i <= n; i++) {
            for (int j = i + 1; j <= n; j++) {
                if (a[i] > a[j]) cnt++;
            }
        }
        if (cnt % 2 == 0) ans++;
        if (ans == 1000000007) ans = 0;
    } while (next_permutation(a + 1, a + n + 1));
    printf("The %d%s answer is %d.\n", n, get(n), ans);
    return 0;
}