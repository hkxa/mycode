#include <bits/stdc++.h>
using namespace std;
#define random(x) (rand() % (x) + 1)
#define randLR(l, r) (random((r) - (l) + 1) + (l) - 1)

int cnt[107];

int specailRand()
{
    int x = 0, i = 0;
    while (i++ < 5 || abs(x) < 19260817) x = x * RAND_MAX + rand();
    if (random(2) == 1) x = -x;
    return (x % 1000000007 + 1000000007) % 1000000007;
}

int main() 
{
    freopen("B.in", "w", stdout);
    srand(time(0) * clock());
    int n = random(3), m = random(200);
    printf("%d %d\n", n, m);
    for (int i = 1, op, x, y; i <= m; i++) {
        op = random(9);
        x = random(n);
        if (!cnt[x] && op == 2) op = 1;
        if (!cnt[x] && op == 4) op = 3;
        if (op == 7) op = 1;
        if (op == 8) op = 3;
        if (op == 9) op = 6;
        if (op == 6) y = random(n);
        if (op == 6 && x == y) op = random(2) * 2 - 1;
        //
        if (op == 1 || op == 3) cnt[x]++;
        if (op == 2 || op == 4) cnt[x]--;
        if (op == 6) cnt[x] += cnt[y], cnt[y] = 0;
        // 
        if (op == 1 || op == 3 || op == 5) printf("%d %d %d\n", op, x, specailRand());
        if (op == 2 || op == 4) printf("%d %d\n", op, x);
        if (op == 6) printf("%d %d %d\n", op, x, y);
    }
    return 0;
}