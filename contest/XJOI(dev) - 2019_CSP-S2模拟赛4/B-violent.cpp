/*************************************
 * user name:    Jmoo.
 * language:     C++.
 * upload place: XJOI (dev.xjoi.net).
*************************************/ 
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define P 1000000007
int n, m;
deque<int> q[1000007];
int opt, idx;

void Join(int idx, int idy)
{
    while (!q[idy].empty()) {
        q[idx].push_back(q[idy].front());
        q[idy].pop_front();
    }
}

void AddSequence(int idx, int diff)
{
    deque<int> t(q[idx]);
    q[idx].clear();
    while (!t.empty()) {
        q[idx].push_back((t.front() + diff) % P);
        t.pop_front();
    }
}

int main()
{
    n = read<int>();
    m = read<int>();
    for (int i = 1; i <= m; i++) {
        opt = read<int>();
        idx = read<int>();
        switch (opt) {
            case 1 : q[idx].push_front(read<int>()) ; break;
            case 2 : write(q[idx].front(), 10); q[idx].pop_front(); break;
            case 3 : q[idx].push_back(read<int>()) ; break;
            case 4 : write(q[idx].back(), 10); q[idx].pop_back(); break;
            case 5 : AddSequence(idx, read<int>()); break;
            case 6 : Join(idx, read<int>()); break;
        }
    }
    return 0;
}

/*
2 10
1 1 100
3 1 200
3 2 10
1 2 20
5 2 40
6 1 2
2 1
2 1
2 1
2 1
*/