#include <bits/stdc++.h>
using namespace std;
#define random(x) (rand() % (x) + 1)
#define randLR(l, r) (random((r) - (l) + 1) + (l) - 1)

int main() 
{
    freopen("A.in", "w", stdout);
    srand(time(0) * clock());
    printf("%d\n", randLR(1, 20));
    return 0;
}