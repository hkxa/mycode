/*************************************
 * user name:    Jmoo.
 * language:     C++.说明：模拟退火核心部分温度变量我用的是 $int$，因为 $endT$ 取 $1$ 会比较合适，所以我就用了 $int$ 而不是 $double$ ；来加速
 * upload place: XJOI (dev.xjoi.net).
*************************************/ 
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;

char s[4][3] = {"th", "st", "nd", "rd"};
char* get(int n)
{
    n = n % 100;
    if (n / 10 == 1) return s[0];
    if (n % 10 == 1) return s[1];
    if (n % 10 == 2) return s[2]; 
    if (n % 10 == 3) return s[3]; 
    return s[0];
}

int ans(int n)
{
    if (n == 1) return 1;
    int res = 1;
    for (int i = 3; i <= n; i++) res = (int64)res * i % 1000000007;
    return res;
}

int main()
{
    freopen("problem1.in", "r", stdin);
    freopen("problem1.out", "w", stdout);
    n = read<int>();
    printf("The %d%s answer is %d.\n", n, get(n), ans(n));
    return 0;
}