### $f[u][0] = size\\f[u][1] = ∑dist(u\rightarrow x)*c[x](u∈{son_u})\\f[u][2]= ∑dist(u\rightarrow x)^2*c[x](u∈{son_u})$

### $G[u][1],G[u][2]:以 u 为顶点的树, 含义同 f$

---

$$\because \texttt{对于一个点 }u, \texttt{与根的距离增加 } k,$$
$$ \Delta ans=c_u * (dis_u+k)^2-c_u * dis_u^2 \ \ \ \\ =c_u*(2*k*dis_u +k^2)\\ =c_u*dis_u*2k+c_uk^2\ \ \ \ $$
$$\therefore \sum\Delta ans=\sum c_u*dis_u*2k+\sum c_uk^2\\=2k*\sum c_u*dis_u+k^2*size\_tot$$