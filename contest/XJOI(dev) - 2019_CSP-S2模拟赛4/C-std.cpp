// Code From Siyuan
#include <bits/stdc++.h>
 
typedef long double LD;
 
const int N = 2e5;
 
int n, a[N + 5];
LD sum, f[N + 5][3], g[N + 5][3];
std::vector<std::pair<int, int> > e[N + 5];
 
void addEdge(int u, int v, int w) {
    e[u].push_back(std::make_pair(v, w));
}
void dfs1(int u, int p) {
    g[u][0] = a[u], sum += a[u];
    for (int i = 0; i < (int)e[u].size(); i++) {
        int v = e[u][i].first, w = e[u][i].second;
        if (v != p) {
            dfs1(v, u);
            g[u][0] += g[v][0];
            g[u][1] += g[v][1] + g[v][0] * w;
            g[u][2] += g[v][2] + g[v][1] * 2 * w + g[v][0] * w * w;
        }
    }
}
void dfs2(int u, int p) {
    for (int i = 0; i < (int)e[u].size(); i++) {
        int v = e[u][i].first, w = e[u][i].second;
        if (v != p) {
            f[v][2] = f[u][2] + f[u][1] * 2 * w + sum * w * w - g[v][1] * 4 * w - g[v][0] * 4 * w * w;
            f[v][1] = f[u][1] + sum * w - g[v][0] * 2 * w;
            dfs2(v, u);
        }
    }
}
int main() {
    // freopen("problem3.in", "r", stdin);
    // freopen("problem3.out", "w", stdout);
    scanf("%d", &n);
    for (int i = 1; i <= n; i++) scanf("%d", &a[i]);
    for (int u, v, w, i = 1; i < n; i++) {
        scanf("%d%d%d", &u, &v, &w);
        addEdge(u, v, w);
        addEdge(v, u, w);
    }
    dfs1(1, 0);
    f[1][2] = g[1][2], f[1][1] = g[1][1];
    dfs2(1, 0);
    LD ans = f[1][2];
    for (int i = 2; i <= n; i++) ans = std::min(ans, f[i][2]);
    printf("%.10Lf\n", ans);
    return 0;
}