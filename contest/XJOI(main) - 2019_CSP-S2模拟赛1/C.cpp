#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, lim;
int v[3007];
int size[3007];
vector<int> G[3007];
int f[3007][3007] = {0};

void DP(int u, int fa)
{
    f[u][1] = v[u];
    size[u] = 1;
    for (unsigned i = 0; i < G[u].size(); i++) {
        if (G[u][i] != fa) {
            DP(G[u][i], u);
            size[u] += size[G[u][i]];
            for (int j = size[u]; j >= 1; j--) {
                for (int k = 1; k < j && k <= size[G[u][i]]; k++) {
                    f[u][j] = max(f[u][j], f[u][j - k] + f[G[u][i]][k]);
                }
            }
        }
    }
}

int main()
{
    memset(f, 0xcf, sizeof(f));
    n = read<int>();
    lim = read<int>();
    for (int i = 1; i <= n; i++) v[i] = read<int>();
    for (int i = 1, u, v; i < n; i++) {
        u = read<int>();
        v = read<int>();
        G[u].push_back(v);
        G[v].push_back(u);
    }
    DP(1, 0);
    int ans = 0xcfcfcfcf;
    for (int i = 1; i <= lim; i++) {
        ans = max(ans, f[1][i]);
    }
    write(ans);
    return 0;
}

/*
6 4
-5
4
-6
6
9
6
3 2
3 1
2 4
2 5
1 6
*/