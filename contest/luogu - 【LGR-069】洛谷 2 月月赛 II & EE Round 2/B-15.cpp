/*************************************
 * @contest:      【LGR-069】洛谷 2 月月赛 II & EE Round 2 Div 2.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-02-15.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;
unsigned a[500007];
unsigned ans;

#define EE(a, b, c, d) (((a) | (b)) ^ ((c) & (d)))

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) a[i] = read<int>();
    for (int i = 1; i <= n; i++) 
        for (int j = 1; j <= n; j++)
            for (int k = 1; k <= n; k++)
                for (int l = 1; l <= n; l++)
                    ans += EE(a[i], a[j], a[k], a[l]);
    write(ans, 10);
    return 0;
}

/*
n = 2;
a b : (A ^ B) * 10

 (44)
1 1 1 : 0   0
1 1 2 : 132 3
1 1 3 : 88  2
1 1 4 : 220 5
1 1 5 : 176 4
1 1 6 : 308 7

1 2 3 : 3 001 010 011
1 2 4 : 7 001 010 100
1 2 5 : 7 001 010 101
1 3 5 : 6 001 011 101
10  -6  = 4
44  -12 = 32
114 -6  = 108
232 -24 = 208


x : 
2 * (n - x) ^ 3
+ 6 * (n - x) ^ 2
+ 2 * (n - x)

704
5 * (n - 1) ^ 3
+ 3 * (n - 1) ^ 2
+ 4 * (n - 1)
*/