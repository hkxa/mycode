/*************************************
 * @contest:      【LGR-069】洛谷 2 月月赛 II & EE Round 2 Div 2.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-02-15.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

char S[1000006]; int n;
uint32 appear[62];
__int128 x, maxx;
int cnt = 0;
inline int turn(char x)
{
    if (isdigit(x)) return x & 15;
    else if (isupper(x)) return (x - 'A') + 10;
    else return (x - 'a') + 36;
}

int main()
{
    scanf("%s", S);
    n = strlen(S);
    x = read<int64>() - n;
    if (x <= 0) {
        puts("0");
        return 0;
    }
    for (int i = 0; i < n; i++) {
        appear[turn(S[i])]++;
    }
    for (int i = 0; i < 62; i++) {
        // if (appear[i]) printf("%d : %d\n", i, appear[i]);
        if (appear[i] > maxx) maxx = appear[i];
    }
    while (true) {
        // printf("x = %lld, maxx = %lld;\n", x, maxx);
        cnt++;
        if (x > maxx) x -= maxx;
        else break;
        maxx <<= 1;
    }
    printf("%d\n", cnt);
    return 0;
}