/*************************************
 * @contest:      【LGR-069】洛谷 2 月月赛 II & EE Round 2 Div 2.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-02-15.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

uint64 a;

/*
给定一个整数 nn，问有多少个长度为 nn 的字符串，满足这个字符串是一个“程序片段”。
具体定义如下：
单个分号 ; 是一个“语句”。
空串 是一个“程序片段”。
如果字符串 A 是“程序片段”，字符串 B 是“语句”，则 AB 是“程序片段”。
如果字符串 A 是“程序片段”，则 {A} 是“语句块”。
如果字符串 A 是“语句块”，则 A 是“语句”，[]A 和 []()A 都是“函数”。
如果字符串 A 是“函数”，则 (A) 是“函数”，A 和 A() 都是“值”。
如果字符串 A 是“值”，则 (A) 是“值”，A; 是“语句”。
注意：A 是 B 并不代表 B 是 A。
*/
int n;
uint64 yj(int n);
uint64 yjk(int n);
uint64 cxpd(int n);
uint64 val(int n);
uint64 fun(int n);

uint64 yj(int n)
{
    if (n <= 0) return 0;
    if (n == 1) return 1;
    return yjk(n) + val(n - 1);
}

uint64 yjk(int n)
{
    if (n < 2) return 0;
    return cxpd(n - 2);
}

uint64 cxpd(int n)
{
    if (n < 0) return 0;
    if (n == 0) return 1;
    uint64 res = 0;
    for (int i = 1; i <= n; i++) res += yj(i) * cxpd(n - i);
    return res;
}

uint64 val(int n)
{
    if (n < 0) return 0;
    return fun(n) + fun(n - 2) + val(n - 2);
}

uint64 fun(int n)
{
    if (n < 0) return 0;
    return yjk(n - 2) + yjk(n - 4) + fun(n - 2);
}

int main()
{
    n = read<int>();
    for (int i = 0; i <= n; i++) {
        printf("%d : yj : %llu; cxpd : %llu; yjk : %llu; val : %llu; fun : %llu.\n", i, yj(i), cxpd(i), yjk(i), val(i), fun(i));
    }
    write(cxpd(n), 10);
    return 0;
}