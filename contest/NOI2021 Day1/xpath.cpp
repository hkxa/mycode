/*************************************
 * @problem:      xpath.
 * @author:       brealid.
 * @time:         2021-07-26.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int P = 998244353;

int T, k, n[107], m[107];
vector<int> G[107][207];

namespace TEST_10A {
    bool check() {
        if (n[1] > 10) return false;
        for (int i = 2; i < k; ++i)
            if (n[i] != n[1]) return false;
        return true;
    }
    int g[13][13];
    int used[13];
    pair<int64, int64> dfs(int u, bool tot) {
        if (u > n[1]) return make_pair(tot, !tot);
        int64 odd = 0, even = 0;
        for (int v = 1, cnt = 0; v <= n[1]; ++v) {
            if (used[v]) continue;
            if (g[u][v]) {
                used[v] = true;
                pair<int, int> p = dfs(u + 1, (tot + cnt) & 1);
                odd += p.first;
                even += p.second;
                used[v] = false;
            }
            ++cnt;
        }
        return make_pair(odd % P, even % P);
    }
    void solve() {
        int64 odd = 0, even = 1;
        for (int i = 1; i < k; ++i) {
            memset(g, 0, sizeof(g));
            for (int u = 1; u <= n[1]; ++u)
                for (int v: G[i][u])
                    g[u][v] = true;
            pair<int64, int64> ans = dfs(1, 0);
            int64 new_odd = odd * ans.second + even * ans.first;
            int64 new_even = odd * ans.first + even * ans.second;
            // printf("%d: (%lld, %lld) => (%lld, %lld) (through (%lld, %lld))\n", i, odd, even, new_odd % P, new_even % P, ans.first, ans.second);
            odd = new_odd % P;
            even = new_even % P;
        }
        kout << (even - odd + P) % P << '\n';
    }
}

namespace TEST_AB {
    namespace mf {
        typedef int value_type;
        const int Net_Node = 40000, Net_Edge = 5e6;
        const value_type inf = (value_type)0x3f3f3f3f3f3f3f3f; // 当 value_type 为 int 的时候，自动类型强转为 0x3f3f3f3f
        struct edge {
            int to, nxt_edge;
            value_type flow;
        } e[Net_Edge * 2 + 5];
        int depth[Net_Node + 5], head[Net_Node + 5], cur[Net_Node + 5], ecnt = 1;
        int node_total, st, ed;
        // 清零，此函数适用于多组数据
        void clear() {
            memset(head, 0, sizeof(int) * (node_total + 1));
            ecnt = 1;
            st = ed = 0;
        }
        // 添加边（正向边和反向边均会自动添加）
        inline void add_edge(const int &from, const int &to, const value_type &flow = (value_type)1) {
            // Add "positive going edge"
            e[++ecnt].to = to;
            e[ecnt].flow = flow;
            e[ecnt].nxt_edge = head[from];
            head[from] = ecnt;
            // Add "reversed going edge"
            e[++ecnt].to = from;
            e[ecnt].flow = 0;
            e[ecnt].nxt_edge = head[to];
            head[to] = ecnt;
        }
        // 将所有流还原(即: 还原残量网络至原图)
        inline void restore_flow() {
            for (int i = 2; i <= ecnt; i += 2) {
                e[i].flow += e[i | 1].flow;
                e[i | 1].flow = 0;
            }
        }
        // Dinic 算法 bfs 函数
        inline bool dinic_bfs() {
            memset(depth, 0x3f, sizeof(int) * (node_total + 1));
            memcpy(cur, head, sizeof(int) * (node_total + 1));
            std::queue<int> q;
            q.push(st);
            depth[st] = 0;
            while (!q.empty()) {
                int u = q.front(); q.pop();
                for (int i = head[u]; i; i = e[i].nxt_edge)
                    if (depth[e[i].to] > depth[u] + 1 && e[i].flow) {
                        depth[e[i].to] = depth[u] + 1;
                        if (e[i].to == ed) return ed; // 后续 bfs 到的节点, depth 一定大于 ed, 没有丝毫用处
                        q.push(e[i].to);
                    }
            }
            return false;
        }
        // Dinic 算法 dfs 函数
        value_type dinic_dfs(int u, value_type now) {
            if (u == ed) return now;
            value_type max_flow = 0, nRet;
            for (int &i = cur[u]; i && now; i = e[i].nxt_edge)
                if (depth[e[i].to] == depth[u] + 1 && (nRet = dinic_dfs(e[i].to, std::min(now, e[i].flow)))) {
                    now -= nRet;
                    max_flow += nRet;
                    e[i].flow -= nRet;
                    e[i ^ 1].flow += nRet;
                }
            return max_flow;
        }
        // Dinic 算法总工作函数，需要提供节点数（偏大影响时间），起始点（默认 1），结束点（默认 node_count）
        value_type dinic_work(int node_count, int start_node = 1, int finish_node = -1) {
            node_total = node_count;
            st = start_node;
            ed = ~finish_node ? finish_node : node_count;
            value_type max_flow = 0;
            while (dinic_bfs())
                max_flow += dinic_dfs(st, inf);
            return max_flow;
        }
    }
    int tot[107], to_node[107];
    void solve() {
        mf::clear();
        for (int i = 1; i <= k; ++i) tot[i] = tot[i - 1] + n[i];
        for (int i = 1; i < k; ++i) {
            for (int u = 1; u <= n[i]; ++u) {
                mf::add_edge(tot[i - 1] + u, tot[k] + tot[i - 1] + u);
                for (int v : G[i][u])
                    mf::add_edge(tot[k] + tot[i - 1] + u, tot[i] + v);
            }
        }
        int s = (tot[k] + tot[k - 1]) + 1, t = (tot[k] + tot[k - 1]) + 2;
        for (int u = 1; u <= n[1]; ++u) {
            mf::add_edge(s, u);
            mf::add_edge(tot[k - 1] + u, t);
        }
        int res = mf::dinic_work(t, s, t);
        // kout << "{DEBUG: " << mf::ecnt << ", " << res << "} "; Fastio::flush_output();
        if (res != n[1]) {
            kout << "0\n";
            return;
        }
        int through = 0;
        for (int i = 1, ecnt = 0; i < k; ++i) {
            for (int u = 1, tn_cnt = 0; u <= n[i]; ++u) {
                ++ecnt;
                for (int v : G[i][u])
                    if (!mf::e[++ecnt << 1].flow)
                        to_node[++tn_cnt] = v;
            }
            for (int x = 1; x < n[1]; ++x)
                for (int y = x + 1; y <= n[1]; ++y)
                    if (to_node[x] > to_node[y]) ++through;
        }
        // kout << "{DEBUG: " << through << "} "; Fastio::flush_output();
        if (through & 1) kout << P - 1 <<"\n";
        else kout << "1\n";
    }
}

signed main() {
    file_io::set_to_file("xpath");
    for (kin >> T; T--;) {
        kin >> k;
        for (int i = 1; i <= k; ++i) {
            kin >> n[i];
            for (int j = 1; j <= n[j]; ++j)
                G[i][j].clear();
        }
        for (int i = 1; i < k; ++i) kin >> m[i];
        for (int i = 1; i < k; ++i) {
            for (int j = 1, u, v; j <= m[i]; ++j) {
                kin >> u >> v;
                G[i][u].push_back(v);
            }
        }
        if (TEST_10A::check()) {
            TEST_10A::solve();
            continue;
        }
        TEST_AB::solve();
    }
    return 0;
}