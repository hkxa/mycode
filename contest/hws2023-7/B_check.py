import os, random


os.system("g++ B.cpp -o B")
# os.system("g++ B_baoli.cpp -o B_baoli")

def gen_in():
    with open("B_in.txt", "w") as f:
        n = 10
        m, k, q1, q2 = [random.randint(0, 10) for _ in range(4)]
        print(n, m, k, file = f)
        for i in range(n):
            print(random.randint(1, 100), end = ' ', file = f)
        print(file = f)
        print(q1, file = f)
        print(*random.sample(range(1, n + 1), k = q1), file = f)
        print(q2, file = f)
        print(*random.sample(range(1, n + 1), k = q2), file = f)

def run_program():
    os.system('B <B_in.txt >B_out.txt')
    os.system('B_baoli <B_in.txt >B_ans.txt')

def check():
    with open('B_out.txt', "r") as f:
        B_out = f.read()
    with open('B_ans.txt', "r") as f:
        B_ans = f.read()
    return B_out == B_ans, B_out, B_ans


round = 0
while True:
    round += 1
    print(f'[+] Round {round}:')
    gen_in()
    run_program()
    res, out, ans = check()
    if not res:
        print(f'[-] expected {ans}, receive {out}')
        break
    else:
        print(f'    pass')