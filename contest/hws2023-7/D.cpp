/*************************************
 * @problem:      hws-A.
 * @author:       brealid.
 * @time:         2023-07-13.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

// #define USE_FREAD  // 使用 fread  读入，去注释符号
// #define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;


int count1(int x, int index) {
    int p2 = 1 << index;
    int full_part = x / (p2 << 1);
    int remain_part = x % (p2 << 1);
    if (remain_part >= p2) return full_part * p2 + remain_part - p2 + 1;
    else return full_part * p2;
}

const int N = 2e5 + 7;

int n, m, k, v[N];
int q1, q2, isset[N];
int64 alike[N], blike[N], bothlike[N], nolike[N];
int cnta, cntb, cntboth, cntnone;

signed main() {
    kin >> n >> m >> k;
    if (m < k) {
        kout << "-1\n";
        return 0;
    }
    for (int i = 1; i <= n; ++i) kin >> v[i];
    kin >> q1;
    for (int i = 1; i <= q1; ++i) isset[kin.get<int>()] = 1;
    kin >> q2;
    for (int i = 1; i <= q2; ++i) isset[kin.get<int>()] |= 2;
    for (int i = 1; i <= n; ++i)
        if (isset[i] == 1) alike[++cnta] = v[i];
        else if (isset[i] == 2) blike[++cntb] = v[i];
        else if (isset[i] == 3) bothlike[++cntboth] = v[i];
        else nolike[++cntnone] = v[i];
    sort(alike + 1, alike + cnta + 1);
    sort(blike + 1, blike + cntb + 1);
    sort(bothlike + 1, bothlike + cntboth + 1);
    sort(nolike + 1, nolike + cntnone + 1);
    // for (int i = 2; i <= cnta; ++i) alike[i] += alike[i - 1];
    // for (int i = 2; i <= cntb; ++i) blike[i] += blike[i - 1];
    // for (int i = 2; i <= cntboth; ++i) bothlike[i] += bothlike[i - 1];
    // for (int i = 2; i <= cntnone; ++i) nolike[i] += nolike[i - 1];
    /*
    如果选择了 t 个共同喜欢的 (exact t, t <= min(m, cntboth))
    那么还需要 max(k-t, 0) 个分别喜欢的, 占据 m-t 个位置
    多余了 m-t - 2*max(k-t, 0) 个位置
    注意到, t >= max(k - cnta, k - cntb, 0)
    此时 a, b 单独选中了 k - max(k - cnta, k - cntb, 0) 个 = min(cnta, cntb, k)
    */
    priority_queue<int> fill_dish;
    priority_queue<int, vector<int>, greater<int> > ready_for_dish;
    int64 dish_total = 0, ans = 0x3f3f3f3f3f3f3f3f, fill_total = 0;
    for (int i = 1; i <= min(min(cnta, cntb), k); ++i)
        dish_total += alike[i] + blike[i];

    for (int i = min(min(cnta, cntb), k) + 1; i <= cnta; ++i) {
        fill_dish.push(alike[i]);
        // printf("push: %d (alike)\n", alike[i]);
        fill_total += alike[i];
    }
    for (int i = min(min(cnta, cntb), k) + 1; i <= cntb; ++i) {
        fill_dish.push(blike[i]);
        // printf("push: %d (blike)\n", blike[i]);
        fill_total += blike[i];
    }
    for (int i = 1; i <= cntnone; ++i) {
        fill_dish.push(nolike[i]);
        // printf("push: %d (nolike)\n", nolike[i]);
        fill_total += nolike[i];
    }
    for (int t = 1; t < max(k - min(cnta, cntb), 0); ++t) dish_total += bothlike[t];
    for (int t = max(k - min(cnta, cntb), 0); t <= min(m, cntboth); ++t) {
        dish_total += bothlike[t];
        int need_fenbie = max(k - t, 0);
        int need_extra = m - t - 2 * max(k - t, 0);
        // printf("bothlike %d: need_fenbie = %d, need_extra = %d\n", t, need_fenbie, need_extra);
        if (0 <= need_extra) {
            while (!ready_for_dish.empty() && fill_dish.size() < need_extra) {
                // printf("  -- push %d\n", ready_for_dish.top());
                fill_total += ready_for_dish.top();
                fill_dish.push(ready_for_dish.top());
                ready_for_dish.pop();
            }
            while (fill_dish.size() > need_extra) {
                // printf("  -- pop %d\n", fill_dish.top());
                fill_total -= fill_dish.top();
                ready_for_dish.push(fill_dish.top());
                fill_dish.pop();
            }
            // printf("  -- fill_dish.size() %d\n", fill_dish.size());
            // assert(fill_dish.top() < ready_for_dish.top());
            if (fill_dish.size() == need_extra) {
                while (!fill_dish.empty() && !ready_for_dish.empty() && fill_dish.top() > ready_for_dish.top()) {
                    fill_total -= fill_dish.top() - ready_for_dish.top();
                    int fill_dish_top = fill_dish.top(), ready_for_dish_top = ready_for_dish.top();
                    fill_dish.pop(), ready_for_dish.pop();
                    fill_dish.push(ready_for_dish_top), ready_for_dish.push(fill_dish_top);
                }
                ans = min(ans, dish_total + fill_total);
            }
            // printf("  -- dish_total = %lld\n", dish_total);
            // printf("  -- fill_total = %lld\n", fill_total);
            // printf("  -- ans = %lld\n", dish_total + fill_total);
            // printf("  -- fill_dish.top() = %d\n", fill_dish.top());
        }
        // push alike[-1], blike[-1] to bothlike
        if (need_fenbie >= 1) {
            ready_for_dish.push(alike[need_fenbie]);
            ready_for_dish.push(blike[need_fenbie]);
            // fill_total += alike[need_fenbie] + blike[need_fenbie];
            dish_total -= alike[need_fenbie] + blike[need_fenbie];
            // printf("  -- [final] dish_total = %lld\n", dish_total);
        }
    }
    if (ans == 0x3f3f3f3f3f3f3f3f) ans = -1;
    kout << ans << '\n';
    return 0;
}

/*
4 3 2
3 2 2 1
2
1 2
2
1 3

5 3 1
4 2 2 1 3
2
1 2
3
1 3 4
*/