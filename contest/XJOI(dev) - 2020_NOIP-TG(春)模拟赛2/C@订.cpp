#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define MaxN 1200
 
struct mS_Edge {
	int u, v;
    bool used;
	mS_Edge() : used(0) {}
} edges[MaxN];
 
struct QueueNode { 
	int v, p;
	QueueNode(int V = 0, int P = 0) : v(V), p(P) {}
};
 
int n, fa[2 * MaxN], num[2 * MaxN], cur = 0;
int From[MaxN], visp[2 * MaxN];
bool ok[MaxN], vis[MaxN], r[MaxN];
 
vector <QueueNode> vqn[2 * MaxN];
vector <int> vi[MaxN];
queue <int> q;
 
inline int father (int k) { return k == fa[k] ? k : fa[k] = father(fa[k]); }
 
inline bool Insert(int x) 
{
    // check is u and v has connected & connect them if they haven't be connected yet
	register int fu = father(edges[x].u);
	register int fv = father(edges[x].v);
	if (fu == fv) return false;
	fa[fu] = fv;
	edges[x].used = 1;
	return true;
}
 
bool dfs(int now, int GoalNode, int sy) // 增广
{
	if (now == GoalNode) return true;
	visp[now] = sy;
	for (register unsigned l = 0; l < vqn[now].size(); l++) {
		int &EdgeToNode = vqn[now][l].v;
		if (visp[EdgeToNode] == sy) continue;
		int &Num = vqn[now][l].p;
		r[Num] = true;
		if (dfs(EdgeToNode, GoalNode, sy)) return true;
		r[Num] = false;
	}
	return false;
}
 
inline void InitData(int x)
{
	memset(ok, 0, sizeof(ok));
	memset(r, -1, sizeof(r));
	memset(visp, -1, sizeof(visp));
	for (register int l = 0; l <= x; l++) vi[l].push_back(l ^ 1);
	for (register int l = 0; l <= x; l++)
        if (!edges[l].used) {
            ok[l] = true;
            memset(r, false, sizeof(r));
            if (!dfs(edges[l].u, edges[l].v, l)) continue;
            ok[l] = false;
            for (int j = 0; j < x; j++) 
                if (r[j]) vi[l].push_back(j);
        }
}
 
bool bfs(int x) // solve
{
	memset(vis, false, sizeof(vis));
	memset(From, -1, sizeof(From));
	register int end = -1;
	q.push(x);
	vis[x] = true;
	while (!q.empty()) {
		register int k = q.front();
		q.pop();
		for (register unsigned l = 0; l < vi[k].size(); l++) {
			int &EdgeToNode = vi[k][l];
			if (vis[EdgeToNode]) continue;
			vis[EdgeToNode] = true;
			if ((edges[EdgeToNode].used ^ edges[k].used)) {
				From[EdgeToNode] = k;
				if (ok[EdgeToNode]) {
					end = EdgeToNode;
					while (!q.empty()) q.pop();
					break;
				}
				q.push(EdgeToNode);
			}
		}
	}
	if (end == -1) return false;
	for (; end != -1; end = From[end]) edges[end].used ^= 1;
	return true;
}
 
inline void Clear(int I)
{
	for (register int j = 0; j <= cur; j++) fa[j] = j;
	for (register int j = 0; j < 4 * n; j++) vqn[j].clear(); 
	for (register int j = 0; j < 2 * n; j++) vi[j].clear();
	for (register int j = 0; j < 2 * I; j++)
		if (edges[j].used)
		{
			fa[father(edges[j].u)] = father(edges[j].v);
			vqn[edges[j].u].push_back((QueueNode){edges[j].v,j});
			vqn[edges[j].v].push_back((QueueNode){edges[j].u,j});
		}
}
 
int main()
{
    n = read<int>();
	for (register int i = 0; i < 2 * n; i++) {
		num[i * 2] = edges[i].u = read<int>();
		num[i * 2 + 1] = edges[i].v = read<int>();
	} 
	sort(num, num + 4 * n);
	for (register int i = 1; i < 4 * n; i++)
        if (num[i] != num[i - 1])
	        num[++cur] = num[i];
	for (int i = 0; i < 2 * n; i++) {
		edges[i].u = lower_bound(num, num + cur + 1, edges[i].u) - num;
		edges[i].v = lower_bound(num, num + cur + 1, edges[i].v) - num;
	}
	int ans = 0, now = 0;
	for (register int i = 0; i < n; i++) {
		Clear(i);
		if (Insert(2 * i)) continue;
		if (Insert(2 * i + 1)) continue;
		InitData(2 * i + 1);
		if (!bfs(2 * i)) bfs(2 * i + 1);
        // now = 0;
        // for (register int i = 0; i < 2 * n; i++)
        //     if (edges[i].used) 
        //         ++now;
        // ans = max(ans, now);
        // TJ
	}
    for (register int i = 0; i < 2 * n; i++)
        if (edges[i].used) 
            ++now;
    ans = max(ans, now);
    write(ans, 10);
	return 0;
}

/*
样例输入一
4
0 1 0 5
5 1 0 5
1 2 0 1
1 5 2 0
样例输入二
6
1 4 1 4
2 4 2 4
0 3 0 3
0 4 0 4
4 3 4 3
1 3 1 3
*/