/*************************************
 * @contest:      NOIP2020模拟训练题2-信友队哈佛读博郑舒冉出题.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-02-29.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    register Int flag = 1;
    register char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    register Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

inline int getM(int a)
{
    register int res = 1;
    while (a) {
        a /= 10;
        res *= 10;
    }
    return res / 10;
}

int main()
{
    register int T = read<int>(), L, R, M;
    register int64 cnt;
    while (T--) {
        L = read<int>();
        R = read<int>();
        M = getM(L);
        cnt = 0;
        for (register int i = L, j; i <= R; i++) {
            j = (i % M) * 10 + i / M;
            while (j != i) {
                if (j >= L && j <= R) cnt++;
                j = (j % M) * 10 + j / M;
            }
        }
        write(cnt / 2, 10);
    }
    return 0;
}

/*
20
1000000 1999990
1000001 1999991
1000002 1999992
1000003 1999993
1000004 1999994
1000005 1999995
1000006 1999996
1000007 1999997
1000008 1999998
1000009 1999999
100000 999990
100001 999991
100002 999992
100003 999993
100004 999994
100005 999995
100006 999996
100007 999997
100008 999998
100009 999999
---
20
1000000 1999990
1000001 1999991
1000002 1999992
1000003 1999993
1000004 1999994
1000005 1999995
1000006 1999996
1000007 1999997
1000008 1999998
1000009 1999999
1000000 1999990
1000001 1999991
1000002 1999992
1000003 1999993
1000004 1999994
1000005 1999995
1000006 1999996
1000007 1999997
1000008 1999998
1000009 1999999
*/