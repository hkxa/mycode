/*************************************
 * @contest:      QiZhiShu - 2020(秋)-TG3.
 * @author:       brealid.
 * @time:         2020-11-28.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

const int N = 500 + 7;
int n, m;
char mp[N][N];

void flood(int x, int y, char origin, char newer) {
    mp[x][y] = newer;
    if (mp[x - 1][y] == origin) flood(x - 1, y, origin, newer);
    if (mp[x + 1][y] == origin) flood(x + 1, y, origin, newer);
    if (mp[x][y - 1] == origin) flood(x, y - 1, origin, newer);
    if (mp[x][y + 1] == origin) flood(x, y + 1, origin, newer);
}

signed main() {
    scanf("%d%d", &m, &n);
    for (int i = 1; i <= n; ++i) scanf("%s", mp[i] + 1);
    flood(1, 1, '-', '*');
    int cnt_0(0), cnt_num(0);
    for (int i = 1; i <= n; ++i)
        for (int j = 1; j <= m; ++j)
            if (mp[i][j] == '-') {
                flood(i, j, '-', '#');
                ++cnt_0;
            }
    for (int i = 1; i <= n; ++i)
        for (int j = 1; j <= m; ++j)
            if (mp[i][j] == '#') {
                flood(i, j, '#', '*');
                ++cnt_num;
            }
    // for (int i = 1; i <= n; ++i)
    //     printf("%s\n", mp[i] + 1);
    printf("%d %d\n", cnt_0, cnt_num - cnt_0);
    return 0;
}