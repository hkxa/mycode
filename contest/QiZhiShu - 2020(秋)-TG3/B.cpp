/*************************************
 * @contest:      QiZhiShu - 2020(秋)-TG3.
 * @author:       brealid.
 * @time:         2020-11-28.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

const int N = 5e3, K = 1024;

int n, m, k, mask;
int have_key[N + 5];
int dis[N * K + 5];
vector<pair<int, int> > G[N + 5];
#define id(room, key_status) ((key_status) * n + (room) - 1)

void bfs(int u) {
    memset(dis, 0x3f, sizeof(dis));
    dis[u] = 0;
    queue<int> q;
    q.push(u);
    while (!q.empty()) {
        u = q.front();
        int state = u / n, room = u % n + 1;
        q.pop();
        for (uint32 i = 0; i < G[room].size(); ++i) {
            if (G[room][i].second & ~state) continue;
            int v = id(G[room][i].first, state | have_key[G[room][i].first]);
            if (dis[v] > dis[u] + 1) {
                dis[v] = dis[u] + 1;
                q.push(v);
            }
        }
    }
}

signed main() {
    scanf("%d%d%d", &n, &m, &k);
    mask = 1 << k;
    for (int i = 1; i <= n; ++i)
        for (int j = 0, x; j < k; ++j) {
            scanf("%d", &x);
            if (x) have_key[i] |= (1 << j);
        }
    for (int i = 1, u, v, w; i <= m; ++i) {
        scanf("%d%d", &u, &v);
        w = 0;
        for (int j = 0, x; j < k; ++j) {
            scanf("%d", &x);
            if (x) w |= (1 << j);
        }
        G[u].push_back(make_pair(v, w));
    }
    bfs(id(1, have_key[1]));
    int ans = 0x3f3f3f3f;
    for (int j = 0; j < mask; ++j)
        ans = min(ans, dis[id(n, j)]);
    if (ans == 0x3f3f3f3f) cout << "No Solution" << endl;
    else cout << ans << endl;
    return 0;
}