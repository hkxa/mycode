/*************************************
 * @contest:      QiZhiShu - 2020(秋)-TG3.
 * @author:       brealid.
 * @time:         2020-11-28.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

const int N = 1e5 + 7;

int n, m, k;
struct Edge {
    int v, w;
    Edge(int V, int W) : v(V), w(W) {}
};
vector<Edge> G[N];
bool is_important[N];

void get_nearest(int &md, int &mx, int &my) {
    md = 1e9;
    int dis[N], from[N];
    memset(dis, 0x3f, sizeof(dis));
    priority_queue<pair<int, int> > q;
    for (int i = 1; i <= n; ++i)
        if (is_important[i]) {
            q.push(make_pair(dis[i] = 0, i));
            from[i] = i;
        }
    while (!q.empty()) {
        int u = q.top().second;
        if (q.top().first + dis[u] != 0) {
            q.pop();
            continue;
        }
        q.pop();
        for (const Edge &e : G[u]) {
            if (dis[e.v] > dis[u] + e.w) {
                dis[e.v] = dis[u] + e.w;
                from[e.v] = from[u];
                q.push(make_pair(-dis[e.v], e.v));
            } else if (from[e.v] != from[u] && md > dis[u] + dis[e.v] + e.w) {
                md = dis[u] + dis[e.v] + e.w;
                mx = from[u];
                my = from[e.v];
            }
        }
    }
}

void dijkstra(int st, int (&dis)[N]) {
    memset(dis, 0x3f, sizeof(dis));
    priority_queue<pair<int, int> > q;
    q.push(make_pair(dis[st] = 0, st));
    while (!q.empty()) {
        int u = q.top().second;
        if (q.top().first + dis[u] != 0) {
            q.pop();
            continue;
        }
        q.pop();
        for (const Edge &e : G[u])
            if (dis[e.v] > dis[u] + e.w) {
                dis[e.v] = dis[u] + e.w;
                q.push(make_pair(-dis[e.v], e.v));
            }
    }
}

signed main() {
    scanf("%d%d%d", &n, &m, &k);
    for (int i = 1, u, v, w; i <= m; ++i) {
        scanf("%d%d%d", &u, &v, &w);
        G[u].push_back(Edge(v, w));
        G[v].push_back(Edge(u, w));
    }
    for (int i = 1, important_node; i <= k; ++i) {
        scanf("%d", &important_node);
        is_important[important_node] = true;
    }
    int d1, x1, y1, d2, x2, y2;
    get_nearest(d1, x1, y1);
    is_important[x1] = is_important[y1] = false;
    get_nearest(d2, x2, y2);
    int ans = d1 + d2;
    int dis_x1[N], dis_y1[N];
    dijkstra(x1, dis_x1);
    dijkstra(y1, dis_y1);
    int x1_fir(0), x1_sec(0), y1_fir(0), y1_sec(0);
    for (int i = 1; i <= n; ++i)
        if (is_important[i]) {
            if (dis_x1[i] < dis_x1[x1_fir]) {
                x1_sec = x1_fir;
                x1_fir = i;
            } else if (dis_x1[i] < dis_x1[x1_sec])
                x1_sec = i;
            if (dis_y1[i] < dis_y1[y1_fir]) {
                y1_sec = y1_fir;
                y1_fir = i;
            } else if (dis_y1[i] < dis_y1[y1_sec])
                y1_sec = i;
        }
    if (x1_fir != y1_fir) ans = min(ans, dis_x1[x1_fir] + dis_y1[y1_fir]);
    else ans = min(ans, min(dis_x1[x1_fir] + dis_y1[y1_sec], dis_x1[x1_sec] + dis_y1[y1_fir]));
    printf("%d\n", ans);
    return 0;
}