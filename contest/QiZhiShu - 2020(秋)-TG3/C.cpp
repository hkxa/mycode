/*************************************
 * @contest:      QiZhiShu - 2020(秋)-TG3.
 * @author:       brealid.
 * @time:         2020-11-28.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

const int N = 2e5 + 7, P = 1e9 + 7;

int64 fpow(int64 a, int n) {
    int64 ret(1);
    while (n) {
        if (n & 1) ret = ret * a % P;
        a = a * a % P;
        n >>= 1;
    }
    return ret;
}
#define inv(number) fpow(number, P - 2)

int n;
int64 fac[N], ifac[N];

void init_C() {
    fac[0] = 1;
    for (int i = 1; i < N; ++i) fac[i] = fac[i - 1] * i % P;
    ifac[N - 1] = inv(fac[N - 1]);
    for (int i = N - 1; i >= 1; --i) ifac[i - 1] = ifac[i] * i % P;
}

int64 C(int n, int m) {
    return fac[n] * ifac[m] % P * ifac[n - m] % P;
}

signed main() {
    init_C();
    scanf("%d", &n);
    for (int c0 = 0, c1 = 0, a; c0 < n && c1 < n; ) {
        printf("%lld ", C(2 * n - c0 - c1 - 2, n - c0 - 1) * fpow(2, c0 + c1 + 1) % P);
        scanf("%d", &a);
        if (a) ++c1;
        else ++c0;
    }
    printf("\n");
    return 0;
}