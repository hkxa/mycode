/*************************************
 * @contest:      Codeforces Round #622 (Div. 2).
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-02-23.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define int int64

int T;
int n, x, y;
int ans, res1, res2;


#define limit_it(x) min(n, max(1LL, (x)))
/*
1 3

3 2
4 1

4
*/

signed main()
{
    T = read<int>();
    while (T--) {
        n = read<int>();
        x = read<int>();
        y = read<int>();
        // {{{Best
        /*
            ans = n;
            res1 = res2 = 0;
            if (n - x - 1 <= y - 1) {
                ans -= n - x - 1;
            } else {
                ans -= y - 1;
                res1 = (n - x - 1) - (y - 1);
            }
            if (n - y - 1 <= x - 1) {
                ans -= n - y - 1;
            } else {
                ans -= x - 1;
                res2 = (n - y - 1) - (x - 1);
            }
            if (x < n && y < n) ans--;
            if (res1 != res2) printf("%%%%%%\n");
            write(ans - min(res1, res2), 32);
        */
            write(limit_it(n - ((n - x) + (n - y)) + 1), 32);
        // }}} 
        // {{{Worst
        /*
            ans = n;
            res1 = res2 = 0;
            x = n - x + 1;
            y = n - y + 1;
            if (n - x <= y - 1) {
                ans -= n - x;
            } else {
                ans -= y - 1;
                res1 = (n - x) - (y - 1);
            }
            // printf("\nD : %d ", ans);
            if (n - y <= x - 1) {
                ans -= n - y;
            } else {
                ans -= x - 1;
                res2 = (n - y) - (x - 1);
            }
            // printf("%d ", ans);
            if (res1 != res2) printf("%%%%%%\n");
            ans -= min(res1, res2);
            // printf("%d\n", ans);
            write(n - ans + 1, 10);
        */
            write(limit_it((x + y) - 1), 10);
        // }}}
    }
    return 0;
}

/*
5 1 3
6 3 4
5 4 4 
5 2 2

1 3
2 6
4 5
1 3
*/