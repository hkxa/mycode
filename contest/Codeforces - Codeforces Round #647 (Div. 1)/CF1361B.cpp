//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      Codeforces Round #647 (Div. 1).
 * @user_name:    hkxadpall.
 * @time:         2020-06-04.
 * @language:     C++.
 * @upload_place: Codeforces.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 1e9 + 7, N=1e6+7;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

int fpow(int a, int n){
    int ret(1);
    while (n) {
        if (n & 1) ret = mul(ret, a);
        a = mul(a, a);
        n >>=1;
    }return ret;
}

bool fpow(int a, int n, int &ret){
    ret = 1;
    while (n) {
        if (n & 1) {
            ret = ret * a;
            if ((int64)ret * a > N) return 0;
        }
        if (n != 1) {
            if ((int64)a * a > N) return 0;
        }
        a = a * a;
        n >>=1;
    }return 1;
}

int T, n, p;
int k[N];
int dy[N], yd[N], cnt = 0;
int ct[N], sp[N];

signed main() {
    T = read<int>();
    k[0] = -1;
    while (T--) {
        n = read<int>();
        p = read<int>();
        for (int i = 1; i <= n; i++) {
            k[i] = read<int>();
        }
        if (p == 1) {
            if (n & 1) puts("1");
            else puts("0");
            continue;
        }
        sort(k + 1, k + n + 1);
        cnt = 0;
        for (int i = 1; i <= n; i++) {
            if (k[i] != k[i - 1]) {
                yd[dy[k[i]] = ++cnt] = k[i];
                ct[cnt] = 0;
            }
            ct[dy[k[i]]]++;
        }
        for (int i = 1; i < cnt; i++) {
            int ps;
            if (fpow(p, yd[i + 1] - yd[i], ps)) {
                int ex = ct[i] / ps;
                if (!ex) {
                    sp[i + 1] = 0;
                    continue;
                }
                // if ((ct[i + 1] + ex) & 1) {
                //     ct[i + 1] += ex - 1;
                //     ct[i] -= (ex - 1) * ps;
                // } else {
                //     ct[i + 1] += ex;
                //     ct[i] -= ex * ps;
                // }
                ct[i + 1] += ex;
                sp[i + 1] = ex;
                ct[i] -= ex * ps;
            } else sp[i + 1] = 0;
        }
        for (int i = cnt; i >= 1; i--) {
            if (ct[i] & 1) {
                if (sp[i]) {
                    ct[i - 1] += p;
                    continue;
                }
                int ans = fpow(p, yd[i]);
                // printf("temproray = %d\n", ans);
                while (--i) {
                    ans = add(ans, P, -mul(fpow(p, yd[i]), ct[i]));
                    // ans = min(ans, Module(P - ans));
                }
                write(ans, 10);
                break;
            } else if (i == 1) {
                puts("0");
                break;
            } 
        }
    }
    return 0;
}