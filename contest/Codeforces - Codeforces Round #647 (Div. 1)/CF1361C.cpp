//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      Codeforces Round #647 (Div. 1).
 * @user_name:    hkxadpall.
 * @time:         2020-06-04.
 * @language:     C++.
 * @upload_place: Codeforces.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 988244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64
const int N = 5e5 + 7, Fill = 1 << 20;

int n;
int a[N], b[N];
int appear[Fill * 3], both[Fill * 3];

bool AllEqual() {
    for (int i = 1; i < n; i++) {
        if (a[i] != b[i]) return false;
        if (a[i] != a[i + 1]) return false;
    }
    return a[n] == b[n];
}

bool check(int bit) {
    int Y = (1 << bit) - 1;
    memarr(Y, 0, appear, both);
    for (int i = 1; i <= n; i++) {
        appear[a[i] & Y]++; appear[b[i] & Y]++;
        if ((a[i] & Y) == (b[i] & Y)) both[b[i] & Y]++;
    }
    for (int i = 1; i <= n; i++) {
        if (appear[a[i] & Y]) {
            if (appear[a[i] & Y] & 1) return false;
            int &A = appear[a[i] & Y], B = both[a[i] & Y];
            if (B && A - 2 * B < 2) return false;
            A = 0;
        }
        if (appear[b[i] & Y]) {
            if (appear[b[i] & Y] & 1) return false;
            int &A = appear[b[i] & Y], B = both[b[i] & Y];
            if (B && A - 2 * B < 2) return false;
            A = 0;
        }
    }
    return true;
    // return Mx <= n;
}
map<int, vector<int> >ind, de;
int ans[N * 2], vis[N * 2];

void GetPerm(int bit) {
    int Y = (1 << bit) - 1;
    // printf("Y = %d\n", Y);
    // memarr(Y, 0, appear, both);
    for (int i = 2; i <= n; i++) {
        if ((a[i] & Y) == (b[i] & Y)) de[b[i] & Y].push_back(i);
        else {
            ind[a[i] & Y].push_back(i * 2 - 1);
            ind[b[i] & Y].push_back(i * 2);
        }
    }
    printf("1 2 "); 
    int u = b[1] & Y;
    for (int _ = 3; _ <= (n << 1); _ += 2) {
        // printf("ENTER : nuum %d\n", _);
        while (!de[u].empty()) {
            printf("%d %d ", de[u][de[u].size() - 1] * 2 - 1, de[u][de[u].size() - 1] * 2);
            de[u].pop_back();
            _ += 2;
        }
        if (_ > (n << 1)) break;
        // printf(">");
        while (vis[ind[u][ind[u].size() - 1]]) ind[u].pop_back();
        int v = ind[u][ind[u].size() - 1];
        if (v & 1) {
            printf("%d %d ", v, v + 1);
            vis[v + 1] = true;
            u = b[(v + 1) >> 1] & Y;
        } else {
            printf("%d %d ", v, v - 1);
            vis[v - 1] = true;
            u = a[v >> 1] & Y;
        }
        ind[u].pop_back();
    }
    putchar(10);
    // return true;
    // return Mx <= n;
}

int TABLE_G_LOEBIT[37];
inline int lowbit(int num) {
    return num & (-num);
}

inline int G(int power) {
    return TABLE_G_LOEBIT[power % 37];
}

signed main() {
    for (int i = 0; i <= 20; i++) TABLE_G_LOEBIT[(1 << i) % 37] = i;
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        a[i] = read<int>();
        b[i] = read<int>();
        // appear[a[i]]++; appear[b[i]]++;
    }
    if (n == 1) {
        if (a[1] == b[1]) puts("20");
        else write(G(lowbit(a[1] ^ b[1])), 10);
        puts("1 2");
        return 0;
    }
    if (AllEqual()) {
        puts("20");
        for (int i = 1; i <= (n << 1); i++) write(i, 32);
        putchar(10);
        return 0;
    }
    int l = 1, r = 20, mid, ans = 0;
    while (l <= r) {
        mid = (l + r) >> 1;
        if (check(mid)) ans = mid, l = mid + 1;
        else r = mid - 1;
    }
    write(ans, 10);
    GetPerm(ans);
    return 0;
}