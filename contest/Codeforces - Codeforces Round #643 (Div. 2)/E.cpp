/*************************************
 * @contest:      Codeforces Round #643 (Div. 2).
 * @user_name:    hkxadpall.
 * @time:         2020-05-16.
 * @language:     C++.
 * @upload_place: Codeforces.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, A, R, M;
int a[100007];
int64 sum, ans;
int mx = 0;

int64 get(int h)
{
    if (h < 0) return R * sum;
    int64 add = 0, rem = 0, mov;
    for (int i = 1; i <= n; i++) {
        if (a[i] > h) rem += a[i] - h;
        else if (a[i] < h) add += h - a[i];
    }
    mov = min(add, rem);
    add -= mov;
    rem -= mov;
    // printf("A %d R %d M %d\n", add, rem, mov);
    return A * add + R * rem + M * mov;
}

int main()
{
    cin >> n >> A >> R >> M;
    M = min(M, A + R);
    for (int i = 1; i <= n; i++) {
        a[i] = read<int>();
        sum += a[i];
        mx = max(mx, a[i]);
    }
    int l = 0, r = mx + 1, m1, m2; 
    ans = R * sum; 
    while (l < r) {
        // printf("%d, %d\n", l, r);
        m1 = l + (r - l) / 3;
        m2 = l + (r - l) / 3 * 2;
        if (get(m1) <= get(m2)) {
            r = m2;
            ans = min(ans, get(m1));
        } else {
            l = m1;
            ans = min(ans, get(m2));
        }
        // write(ans, 10);
    }
    write(ans, 10);
    return 0;
}