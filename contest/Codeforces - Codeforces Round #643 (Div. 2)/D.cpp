/*************************************
 * @contest:      Codeforces Round #643 (Div. 2).
 * @user_name:    hkxadpall.
 * @time:         2020-05-16.
 * @language:     C++.
 * @upload_place: Codeforces.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, s;

int main()
{
    n = read<int>();
    s = read<int>();
    if (s >= (n << 1)) {
        puts("YES");
        for (int i = 1; i <= (n >> 1); i++) printf("1 ");
        write(s - n + 1, 32);
        for (int i = (n >> 1) + 2; i <= n; i++) printf("1 ");
        putchar(10);
        write(n, 10);
    } else puts("NO");
    return 0;
}