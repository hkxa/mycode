/*************************************
 * @contest:      Codeforces Round #643 (Div. 2).
 * @user_name:    hkxadpall.
 * @time:         2020-05-16.
 * @language:     C++.
 * @upload_place: Codeforces.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}
#define int int64

int p[] = {2, 3, 5, 7, 13, 17, 19, 23, 29, 37, 41, 43, 47, 53, 59, 67, 71, 73, 79, 83, 89, 97, 103, 109, 113, 127, 137, 149, 151, 163, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271};
// int p[] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271};
int64 q[30];
int64 ans = 1;

int64 kpow(int64 a, int n) {
    int64 res = 1;
    while (n) {
        if (n & 1) res = res * a;
        a = a * a;
        n >>= 1;
    }
    // printf("%lld\n", res);
    return res;
}

signed main()
{
    double logMax = log(999999999);
    int lg1, lg2;
    for (int i = 0; i < 22; i++) {
        // printf("%d\n", i);
        lg1 = logMax / log(p[i << 1]);
        lg2 = logMax / log(p[i << 1 | 1]);
        // printf("%d, %d => %d, %d\n", p[i << 1], p[i << 1 | 1], lg1, lg2);
        q[i] = kpow(p[i << 1], lg1) * kpow(p[i << 1 | 1], lg2);
        // printf("q[%d] = %lld\n", i, q[i]);
    }
    int T = read<int>(), res, cnt;
    while (T--) {
        ans = 1;
        for (int i = 0; i < 22; i++) {
            printf("? %lld\n\n", q[i]);
            fflush(stdout);
            scanf("%lld", &res);
            cnt = 1;
            while (res % p[i << 1] == 0) {
                res /= p[i << 1];
                cnt++; 
            }
            ans *= cnt;
            cnt = 1;
            while (res % p[i << 1 | 1] == 0) {
                res /= p[i << 1 | 1];
                cnt++; 
            }
            ans *= cnt;
        }
        printf("! %lld\n", max((int64)(ans * 2), ans + 7));
        fflush(stdout);
    }
    return 0;
}

/*
207994791256915968
68963683837890625
174859124550883201
366790143213462347
88055199122167369
61543017092605717
17031839720905043
95911048269453451
603822418430417999
512073858644401
1106113787207521
2977641413637361
9212392481101681
14753124573678481
23015465634663121
76612576223073361
131504758979950801
256243503037410001
428893068254316961
696707197443794161
34009074817199
50092421787647
*/