/*************************************
 * @user_id:      ZJ-00071.
 * @time:         2020-04-25.
 * @language:     C++.
 * @upload_place: NOI Online.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 998244353;

int n, m;
char belong[5007];
vector<int> G[5007];
int dfn[5007], end[5007], dfnCnt = 0;
int fa[5007], siz[2][5007];
int lis[2][2507], lisCnt[2], vis[2507];
int ans[2507];

int fac(int n)
{
    int res = 1;
    for (int i = 2; i <= n; i++) res = (int64)res * i % P;
    return res;
}

bool checkLine()
{
    if (G[1].size() != 1) return false;
    for (int i = 2, las = 1, u = G[1][0]; i < n; i++) {
        if (G[u].size() != 2) return false;
        if (G[u][0] == las) {
            las = u;
            u = G[las][1];
        } else {
            las = u;
            u = G[las][0];
        }
    }
    return true;
}

void preDfs(int u, int f)
{
    dfn[u] = ++dfnCnt;
    fa[u] = f;
    siz[belong[u]][u] = 1;
    for (uint32 i = 0; i < G[u].size(); i++) {
        if (G[u][i] != f) {
            preDfs(G[u][i], u);
            siz[0][u] += siz[0][G[u][i]];
            siz[1][u] += siz[1][G[u][i]];
        }
    }
    end[u] = dfnCnt;
}

#define isSubtree(u, f) (dfn[u] >= dfn[f] && dfn[u] <= end[f])
#define isNotFair(u, v) (isSubtree(u, v) || isSubtree(v, u))

void dfs(int u, int cnt)
{
    if (u > m) {
        ans[cnt]++;
        if (ans[cnt] >= P) ans[cnt] -= P;
        return;
    }
    for (int i = 1; i <= m; i++) {
        if (!vis[i]) {
            vis[i] = true;
            // printf("isNotFair(%d, %d) = %d\n", lis[0][u], lis[1][i], isNotFair(lis[0][u], lis[1][i]));
            dfs(u + 1, cnt + isNotFair(lis[0][u], lis[1][i]));
            vis[i] = false;
        }
    }
}

// try n = 50
int ans_[507][507];

void CorrectDfs(int u)
{
    for (uint32 i = 0; i < G[u].size(); i++) {
        if (G[u][i] != fa[u]) {
            CorrectDfs(G[u][i]);
        }
    }
}

int main()
{
    freopen("match.in", "r", stdin);
    freopen("match.out", "w", stdout);
    n = read<int>();
    m = n / 2;
    scanf("%s", belong + 1);
    for (int i = 1; i <= n; i++) {
        belong[i] &= 1;
        lis[belong[i]][++lisCnt[belong[i]]] = i;
    }
    for (int i = 1, u, v; i < n; i++) {
        u = read<int>();
        v = read<int>();
        G[u].push_back(v);
        G[v].push_back(u);
    }
    if (checkLine()) {
        for (int i = 1; i <= m; i++) printf("0 ");
        write(fac(m), 32);
        putchar(10);
        return 0;
    }
    if (n == 20) {
        preDfs(1, 0);
        dfs(1, 0);
        for (int i = 0; i <= m; i++) write(ans[i], 32);
        putchar(10);
        return 0;
    }
    preDfs(1, 0);
        dfs(1, 0);
    for (int i = 0; i <= m; i++) write(ans[i], 32);
    putchar(10);
    return 0;
}

/*
8
10010011
1 2
1 3
2 4
2 5
5 6
3 7
3 8

8 
10010011
1 2
2 6
6 8
8 3
3 5
5 7
7 4
*/