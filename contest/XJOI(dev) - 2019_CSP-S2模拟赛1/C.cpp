#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;
int card[307][2][2];
map<int, int> ntos;
int num[1207], number_cnt = 0;

// 离散化 
void discretization(int number) {
	if (!ntos[number]) {
		num[++number_cnt] = number;
		ntos[number] = number_cnt;
	}
}

#define randint(x) ((rand() * RAND_MAX + rand()) % x + 1)

struct node {
    int ch[2], fa;
    bool rev;
	node() : fa(0), rev(0) { ch[0] = ch[1] = 0; }
} t[1000007];

#define ls t[u].ch[0]
#define rs t[u].ch[1]

#define Ident(u) (t[t[u].fa].ch[1] == u)
#define IsRoot(u) (t[t[u].fa].ch[0] != u && t[t[u].fa].ch[1] != u)

inline void Pushrev(int u) {
    swap(t[u].ch[0], t[u].ch[1]); //*
    t[u].rev ^= 1;
}

inline void Pushdown(int u) {
    if (!t[u].rev) return;
    if (t[u].ch[0]) Pushrev(t[u].ch[0]);
    if (t[u].ch[1]) Pushrev(t[u].ch[1]);
    t[u].rev = 0;
}

int sta[1000007], top;

inline void Rotate(int x) {
    int y = t[x].fa, z = t[y].fa, k = Ident(x);
    t[x].fa = z; 
	if(!IsRoot(y)) t[z].ch[Ident(y)] = x;
    t[y].ch[k] = t[x].ch[k ^ 1];
	t[t[x].ch[k ^ 1]].fa = y;
    t[x].ch[k ^ 1] = y;
	t[y].fa = x;
}

inline void Splay(int u) {
    int x = u;
    top = 0;
	// printf("  - Step1...\n");
    while (!IsRoot(u)) {
        sta[++top] = u;
        u = t[u].fa;
    }
	// printf("  - Step2...\n");
    sta[++top] = u;
    for (int i = top; i >= 1; i--)  Pushdown(sta[i]);
	// printf("  - Step3...\n");
    while (!IsRoot(x)) {
        int y = t[x].fa;
        if (!IsRoot(y)) Ident(x) == Ident(y) ? Rotate(y) : Rotate(x);  
        Rotate(x);
    }
}

inline void Access(int u) {
    for (int v = 0; u; v = u, u = t[u].fa) {
        Splay(u);
        t[u].ch[1] = v;
    }
}

inline void MakeRoot(int u) {
	// printf("- Accessing...\n");
    Access(u);
	// printf("- Splaying...\n");
    Splay(u);
	// printf("- Pushreving...\n");
    Pushrev(u);
}

inline int FindRoot(int u) {
	// printf("- Accessing...\n");
    Access(u);
	// printf("- Splaying...\n");
    Splay(u);
	// printf("- Rotating...\n");
    while (ls) u = ls;
	// printf("- Splaying...\n");
    Splay(u);
    return u;
}

inline void Link(int u, int v) {
    MakeRoot(u);
    Access(v);
    Splay(v);
	// up 3 lines : option "Split"
    t[u].fa = v;
}

inline void Cut(int u, int v) {
    MakeRoot(u);
    if (FindRoot(v) == u && t[v].fa == u && !t[v].ch[0]) {
        t[v].fa = t[u].ch[1] = 0;
    }
}

inline bool TestConnection(int u, int v) {
	// printf("TestConnection %d, %d...\n", u, v);
	// printf("MakeRoot...\n");
    MakeRoot(u);
	// printf("FindRoot...\n");
	return FindRoot(v) == u;
}

bool vis_edge[307][2] = {0};

int main()
{
	srand((time(0) + (time(0) + (time(0) * 20170933)) * 201709) * 2017);
    n = read<int>();
    for (int i = 1; i <= n; i++) {
    	discretization(card[i][0][0] = read<int>());
    	discretization(card[i][0][1] = read<int>());
    	discretization(card[i][1][0] = read<int>());
    	discretization(card[i][1][1] = read<int>());
	}
	int ans = 0, now = 0;
	for (double t = 1000; t > 1e-14; t *= 0.99) {
		// printf("temperature = %.6lf.\n", t);
		int i = randint(n), type = rand() & 1;
		// printf("random %d, %d\n", i, type);
		if (!vis_edge[i][0] && !vis_edge[i][1] && !TestConnection(ntos[card[i][type][0]], ntos[card[i][type][1]])) {
			now++;
			ans = max(ans, now);
			vis_edge[i][type] = true;
			Link(ntos[card[i][type][0]], ntos[card[i][type][1]]);
		} else if (vis_edge[i][type] && exp((ans - now) / t) < (rand() * RAND_MAX + rand()) / 10737418244.0) {
			now--;
			vis_edge[i][type] = false;
			Cut(ntos[card[i][type][0]], ntos[card[i][type][1]]);
		}
	}
	/*
	for (double t = 1000; t > 1e-14; t *= 0.99) {
		// printf("temperature = %.6lf.\n", t);
		int i = randint(n), type = rand() & 1;
		// printf("random %d, %d\n", i, type);
		if (!vis_edge[i] && !TestConnection(ntos[card[i][type][0]], ntos[card[i][type][1]])) {
			now++;
			ans = max(ans, now);
			vis_edge[i] = true;
			Link(ntos[card[i][type][0]], ntos[card[i][type][1]]);
		} else if (exp(ans - now) / t < randint(1073741824) / 10737418244.0) {
			now--;
			vis_edge[i] = false;
			Cut(ntos[card[i][type][0]], ntos[card[i][type][1]]);
		}
	}
	*/
	write(ans);
    return 0;
}