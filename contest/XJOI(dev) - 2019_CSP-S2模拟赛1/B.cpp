#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;
int mp[107][107];
int minn[200007], ans[200007];
int cnt, to[600007], head[600007], nxt[600007];
bool inq[200007] = { false };
const int dx[4] = {0, 0, 1, -1}, dy[4] = {1, -1, 0, 0};
queue<int> q;

void add(int u, int v)
{
    to[++cnt] = v;
    nxt[cnt] = head[u];
    head[u] = cnt;
}

// void debugger(int id)
// {
//     if (id == 200000) printf("[finish point] ");
//     else {
//         int v = id / 18;
//         int y = (v - 1) % n + 1;
//         int x = (v - 1) / n;
//         printf("(%d, %d) [val = %d] ", x, y, id % 18);
//     }
//     printf(":\tminn = %d, ans = %d\n", minn[id], ans[id]);
// }

int main()
{
  mainStart:
    n = read<int>();
    cnt = 0;
    memset(head, -1, sizeof(head));
    memset(minn, 0x3f, sizeof(minn));
    memset(ans, 0, sizeof(ans));
    if (!n) return 0;
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= n; j++) {
            mp[i][j] = read<int>();
        }
    }
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= n - i; j++) {
            mp[i][j] += mp[n - j + 1][n - i + 1];
        }
    }
    // for (int i = 1; i <= n; i++) {
    //     for (int j = 1; j <= n - i + 1; j++) {
    //         printf("%d ", mp[i][j]);
    //     }
    //     putchar(10);
    // }
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= n - i + 1; j++) {
            for (int k = 1; k < mp[i][j]; k++) {
                add((i * n + j) * 18 + k, (i * n + j) * 18 + k - 1);
            }
            for (int k = 0, mx, my; k < 4; k++) {
                mx = i + dx[k];
                my = j + dy[k];
                if (mx >= 1 && my >= 1 && (mx + my) <= n + 1) {
                    add((i * n + j) * 18, (mx * n + my) * 18 + mp[mx][my] - 1);
                }
            }
        }
    }
    for (int i = 1, j = n; i <= n; i++, j--) {
        add((i * n + j) * 18, 200000);
    }
    ans[(n + 1) * 18 + mp[1][1] - 1] = 1;
    minn[(n + 1) * 18 + mp[1][1] - 1] = 0;
    q.push((n + 1) * 18 + mp[1][1] - 1);
    inq[(n + 1) * 18 + mp[1][1] - 1] = 1;
    while (!q.empty()) {
        int f = q.front();
        q.pop();
        inq[f] = 0;
        // debugger(f);
        for (int x = head[f]; x != -1; x = nxt[x]) {
            if (minn[f] + 1 < minn[to[x]]) {
                minn[to[x]] = minn[f] + 1;
                ans[to[x]] = ans[f];
                if (!inq[to[x]]) {
                    q.push(to[x]);
                    inq[to[x]] = 1;
                }
            } else if (minn[f] + 1 == minn[to[x]]) {
                ans[to[x]] = (ans[to[x]] + ans[f]) % 1000000009;
            }
        }
    }
    // printf("minn distance = %d\n", minn[200000]);
    write(ans[200000], 10);
    // puts("------------------------------");
    goto mainStart;
    return 0;
}

/*
1
1 1
1 2 1
1 3 3 1
1 4 6 4 1
*/ 
