#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int T, L, R, t, cnt, cntP10;
long long ans;
bool g[2000007];

int bit[10], c;
int TJ(int n)
{
    c = 0;
    for (int i = 1; i <= cnt; i++) {
        n = n / 10 + n % 10 * cntP10;
        if (n >= L && n <= R && !g[n]) {
            g[n] = true;
            c++;
        }
    }
    return c * (c - 1) / 2;
}

int main()
{
    T = read<int>();
    while (T--) {
        L = read<int>();
        R = read<int>();
        memset(g, 0, sizeof(g));
        t = L;
        cnt = 0;
        while (t) {
            t /= 10;
            cnt++;
        }
        t = cnt;
        cntP10 = 1;
        while (--t) {
            cntP10 *= 10;
        }
        ans = 0;
        for (int i = L; i <= R; i++) {
            if (!g[i]) ans += TJ(i);
        }
        write(ans, 10);
    }
    return 0;
}
