/*************************************
 * @contest:      QiZhiShu - 2020(秋)-TG2.
 * @author:       brealid.
 * @time:         2020-11-21.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

template<typename ele> 
ele gcd(ele m, ele n) {
    while (n != 0) {
        ele t = m % n;
        m = n;
        n = t;
    }
    return m;
}

template<typename ele> 
ele lcm(ele m, ele n) {
    return m / gcd(m, n) * n;
}

int a1, a2, b1, b2, cir;
int64 l, r;

#define in_seq(x, ele1, ele2) (((x) - (ele1)) % ((ele2) - (ele1)) == 0)
#define in_both_seq(x) (in_seq(x, a1, a2) && in_seq(x, b1, b2))

int64 solve(int64 n) {
    int64 ans = 0;
    if (n < cir) {
        for (int64 i = 1; i <= n; ++i)
            if (in_both_seq(i)) ++ans;
        return ans;
    }
    int64 repeat = n / cir;
    for (int64 i = n - cir + 1; i <= n; ++i) 
        if (in_both_seq(i)) ++ans;
    ans *= repeat;
    for (int64 i = n - repeat * cir; i >= 1; --i) 
        if (in_both_seq(i)) ++ans;
    return ans;
}

signed main() {
    kin >> a1 >> a2 >> b1 >> b2 >> l >> r;
    cir = lcm(a2 - a1, b2 - b1);
    kout << solve(r) - solve(l - 1) << '\n';
    return 0;
}