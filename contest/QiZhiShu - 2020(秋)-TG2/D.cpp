/*************************************
 * @contest:      QiZhiShu - 2020(Çï)-TG2.
 * @author:       brealid.
 * @time:         2020-11-21.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 5e5 + 7;

int n, m, option[N], op_x[N], op_y[N];

struct SetA {
    int fa[N << 1], direct_fa[N << 1], cnt;
    int64 w[N << 1];
    void init(int n) {
        memset(fa, -1, sizeof(fa));
        cnt = n;
    }
    int find(int u) {
        return fa[u] < 0 ? u : fa[u] = find(fa[u]);
    }
    inline void merge(int u, int v) {
        u = find(u), v = find(v);
        direct_fa[u] = direct_fa[v] = fa[u] = fa[v] = ++cnt;
    }
    // w[all nodes in tree which includes u] += val
    inline void increase_tree(int u, int val) {
        u = find(u);
        direct_fa[u] = fa[u] = ++cnt;
        w[cnt] = val;
    }
    inline void reset() {
        for (int i = cnt; i >= 1; --i)
            w[i] += w[direct_fa[i]];
        memset(fa, -1, sizeof(fa));
    }
    inline void connect_root_up(int u) {
        u = find(u);
        fa[u] = direct_fa[u];
    }
    inline int64 getw(int u) {
        return w[direct_fa[find(u)]];
    }
} A;

struct SetB {
    int fa[N << 1], direct_ch[N << 1][2], last_cover[N << 1], w[N << 1], cnt;
    int cfa[N << 1]; // 被 cover 后的 father
    void init(int n) {
        memset(fa, -1, sizeof(fa));
        memset(cfa, -1, sizeof(cfa));
        cnt = n;
    }
    int find(int u) {
        return fa[u] < 0 ? u : fa[u] = find(fa[u]);
    }
    int find_cfa(int u) {
        return cfa[u] < 0 ? u : cfa[u] = find_cfa(cfa[u]);
    }
    inline void merge(int u, int v) {
        ++cnt;
        direct_ch[cnt][0] = u = find(u);
        direct_ch[cnt][1] = v = find(v);
        fa[u] = fa[v] = cnt;
    }
    void cover(int u) {
        if (u <= n || cfa[direct_ch[u][0]] >= 0) return; // 无子树/子树中的结点已经被连接了
        cfa[direct_ch[u][0]] = u;
        cfa[direct_ch[u][1]] = u;
        cover(direct_ch[u][0]);
        cover(direct_ch[u][1]);
    }
    inline void cover(int u, int val, int tim) {
        u = find(u);
        cover(u);
        last_cover[u] = tim;
        w[u] = val;
    }
    inline int now_value(int u) {
        return w[find_cfa(u)];
    }
} B;

int q;
int64 ans[N];
struct QueryPair {
    int x, qid, type;
    QueryPair(int X, int Id, int Typ) : x(X), qid(Id), type(Typ) {}
};
queue<QueryPair> qry[N];

signed main() {
    kin >> n >> m;
    A.init(n);
    B.init(n);
    for (int i = 1; i <= m; ++i) {
        int &opt = option[i], &x = op_x[i], &y = op_y[i];
        kin >> opt >> x;
        if (opt != 5) kin >> y;
        if (opt == 1) {
            x = A.find(x);
            y = A.find(y);
            A.merge(x, y);
        } else if (opt == 2) {
            B.merge(x, y);
        } else if (opt == 3) {
            A.increase_tree(x, y);
        } else if (opt == 4) {
            B.cover(x, y, i);
        } else {
            ans[++q] = B.now_value(x);
            qry[B.last_cover[B.find_cfa(x)]].push(QueryPair(x, q, 1));
            qry[i].push(QueryPair(x, q, -1));
        }
    }
    A.reset();
    for (int i = 0; i <= m; ++i) {
        if (option[i] == 1 || option[i] == 3) A.connect_root_up(op_x[i]);
        if (option[i] == 1) A.connect_root_up(op_y[i]);
        while (!qry[i].empty()) {
            const QueryPair &qnow = qry[i].front();
            ans[qnow.qid] += A.getw(qnow.x) * qnow.type;
            qry[i].pop();
        }
    }
    for (int i = 1; i <= q; ++i)
        kout << ans[i] << '\n';
    return 0;
}