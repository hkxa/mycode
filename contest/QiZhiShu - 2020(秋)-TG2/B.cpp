/*************************************
 * @contest:      QiZhiShu - 2020(秋)-TG2.
 * @author:       brealid.
 * @time:         2020-11-21.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 3e6 + 7;

int L, b, n;
char wood[N];
int sum[N];

void assert_POSSIBLE(bool cond) {
    if (!cond) {
        kout << "IMPOSSIBLE\n";
        exit(0);
    }
}

signed main() {
    kin >> L >> b >> n >> (wood + 1);
    assert_POSSIBLE(L <= b && b <= n);
    for (int i = 1; i <= n; ++i)
        sum[i] = sum[i - 1] + (wood[i] == '1');
    if (L == b && b == n) {
        kout << "0\n";
        return 0;
    }
    int pos = 1;
    int64 ans = n - b;
    while (pos + b - 1 < n) {
        // printf("%d ", pos);
        assert_POSSIBLE(sum[pos + b - 1] - sum[pos - 1] > L);
        int move_step = sum[pos + b - 1] - sum[pos - 1] - L;
        while (pos + b - 1 < n && (move_step || wood[pos] == '0'))
            if (wood[pos++] == '1') --move_step;
        ans += L;
    }
    ans += L;
    kout << ans << '\n';
    return 0;
}