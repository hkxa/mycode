/*************************************
 * @problem:      Codeforces - Educational #76.
 * @user_name:    hkxadpall.
 * @time:         2019-11-13.
 * @language:     C++.
 * @upload_place: Codeforces.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int count(int x)
{
    // int y = x;
    int res = 0;
    while (x) {
        if (x & 1) res++;
        x >>= 1;
    }
    // printf("count(%d) = %d.\n", y, res);
    return res;
}

int n;
int a[107], cnt[107];

int x, op;

// #define pe2(x) ((x) * (x) * (x))
double check(int k)
{
    int tot = 0;
    for (int i = 1; i <= n; i++) tot += (cnt[i] = count(a[i] ^ x));
    double ave = tot / n, ans = 0;
    for (int i = 1; i <= n; i++) ans += pow(abs(cnt[i] - ave), k);
    return ans;
}

// void dfs(int bit, int tot)
// {
//     if (bit > 30) return;
//     if (check()) {
//         write(tot);
//         exit(0);
//     }
//     for (int i = 1; i < n; i++) {
//         ;
//     }
// }

#define randR(x) (rand() % (x))

int main()
{
    srand(time(0));
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        a[i] = read<int>();
        cnt[i] = count(a[i]);
    }
    for (int i = 1; i < n; i++) {
        if ((cnt[i] & 1) != (cnt[i + 1] & 1)) {
            puts("-1");
            return 0;
        }
    }
    double las = check(1);
    for (int i = 0, k = 1; las >= 0.01; i++) {
        if (i == 30) i = 0, k += 0.25;
        op = 1 << i;
        x ^= op;
        if (check(k) < las) las = check(k);
        else x ^= op;
    }
    // 0 0011
    // 1 0001
    // 0 0110
    // 0 0000
    write(x);
    return 0;
}