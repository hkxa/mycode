/*************************************
 * @problem:      Codeforces - Educational #76.
 * @user_name:    hkxadpall.
 * @time:         2019-11-13.
 * @language:     C++.
 * @upload_place: Codeforces.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int k1, k2, k3, n;
int where[200007];
int pre[200007], mid[200007], nxt[200007];
// int gpr[200007], gnx[200007];
int f[200007];
int ans;
#define fi(i, nk) for (int i = 1; i <= nk; i++)
#define fj(i, nk) for (int i = nk; i >= 1; i--)

int main()
{
    k1 = read<int>();
    k2 = read<int>();
    k3 = read<int>();
    n = k1 + k2 + k3;
    ans = min(k1 + k2, min(k1 + k3, k2 + k3));
    fi(i, k1) where[read<int>()] = 1;
    fi(i, k2) where[read<int>()] = 2;
    fi(i, k3) where[read<int>()] = 3;
    fi(i, n) pre[i] = pre[i - 1] + (where[i] != 1);
    fi(i, n) mid[i] = mid[i - 1] + (where[i] != 2);
    fi(i, n) f[i] = min(f[i - 1], pre[i] - mid[i]);
    // fi(i, n) printf("f[%d] = %d.\n", i, f[i]);
    fj(i, n) nxt[i] = nxt[i + 1] + (where[i] != 3);
    fi(i, n) ans = min(ans, mid[i - 1] + f[i - 1] + nxt[i]);
    write(ans);
    return 0;
}