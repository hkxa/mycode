/*************************************
 * @problem:      Codeforces Round #687 (Div. 1, based on Technocup 2021 Elimination Round 2).
 * @author:       hkxadpall.
 * @time:         2020-11-29.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;
namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}
#define getchar() (i_P1 == i_P2 && (i_P2 = (i_P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), i_P1 == i_P2) ? EOF : *i_P1++)
char BUF[1 << 21], *i_P1 = BUF, *i_P2 = BUF;
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 1e5 + 7;

int n;
int a[N], s[N];

// #define rXOR(l, r) (s[(l) - 1] ^ s[r])

// struct CharTrie {
//     int root[N];
//     struct node {
//         int s[2];
//     } tr[N * 31];
//     int cnt, dep_max;
//     CharTrie() : cnt(0), dep_max(30) {}
//     void insert(int &u, int v, int now, int val) {
//         if (now < 0) return;
//         if ((val >> now) & 1) {
//             tr[u].s[0] = tr[v].s[0];
//             insert(tr[u].s[1] = ++cnt, tr[v].s[1], now - 1, val);
//         } else {
//             tr[u].s[1] = tr[v].s[1];
//             insert(tr[u].s[0] = ++cnt, tr[v].s[0], now - 1, val);
//         }
//     }
//     void insert(int now, int from, int val) {
//         insert(root[now] = ++cnt, root[from], dep_max, val);
//     }
//     int find_near(int u, int now, int val) {
//         if (now < 0) return 0;
//         int p = (val >> now) & 1;
//         if (tr[u].s[p]) return find_near(tr[u].s[p], now - 1, val);
//         else return find_near(tr[u].s[p ^ 1], now - 1, val) | (1 << now);
//     }
//     int find_far(int u, int now, int val) {
//         if (now < 0) return 0;
//         int p = (val >> now) & 1;
//         if (tr[u].s[p ^ 1]) return find_near(tr[u].s[p ^ 1], now - 1, val) | (1 << now);
//         else return find_near(tr[u].s[p], now - 1, val);
//     }
// } l, r;

int b;
namespace AND {
    int oc0[N][23], oc1[N][23], lg2[N];
    void init() {
        for (int i = 0; i <= n; ++i) {
            oc0[i][0] = ~s[i];
            oc1[i][0] = s[i];
        }
        lg2[0] = -1;
        for (int i = 1; i <= n + 1; ++i) lg2[i] = lg2[i >> 1] + 1;
        for (int k = 1; k < 23; ++k)
            for (int i = 0; i + (1 << k) - 1 <= n; ++i) {
                oc0[i][k] = oc0[i][k - 1] | oc0[i + (1 << (k - 1))][k - 1];
                oc1[i][k] = oc1[i][k - 1] | oc1[i + (1 << (k - 1))][k - 1];
            }
    }
    int qry(int l, int r, int typ) {
        int k = lg2[r - l + 1];
        if (typ) return (oc1[l][k] | oc1[r - (1 << k) + 1][k]) >> b & 1;
        return (oc0[l][k] | oc0[r - (1 << k) + 1][k]) >> b & 1;
    }
}

unordered_map<int, vector<int> > c[2];
int l[N], r[N];

int qry_l(int id, int i, int d) {
    unordered_map<int, vector<int>>::iterator it = c[d].find(i);
    if (it != c[d].end()) {
        vector<int> &res = it->second;
        int l = 0, r = res.size() - 1, mid, ans = -1e9;
        // if (b == 28 && id == 3) printf("%d\n", r);
        while (l <= r) {
            mid = (l + r) >> 1;
            if (AND::qry(res[mid] + 1, id - 1, !d)) ans = res[mid], l = mid + 1;
            else r = mid - 1;
        }
        return ans;
    }
    return -1e9;
}

// int qry_r(int i, int d) {
//     unordered_map<int, val>::iterator it = c.find(i);
//     if (it != c.end()) {
//         int res = it->second[d];
//         return res == -1 ? 1e9 : res;
//     }
//     return 1e9;
// }

signed main() {
    kin >> n;
    // l.insert(0, 1e5, 0);
    for (int i = 1; i <= n; ++i) {
        kin >> a[i];
        s[i] = s[i - 1] ^ a[i];
        // l.insert(i, i - 1, s[i]);
    }
    AND::init();
    // for (int i = n; i >= 0; --i) r.insert(i, i + 1, s[i]);
    int ans = 2e9;
    for (b = 29; b >= 0; --b) {
        c[0].clear();
        c[1].clear();
        int mask = ~((1 << (b + 1)) - 1);
        for (int i = 0; i <= n; ++i) {
            int d = (s[i] >> b) & 1;
            ans = min(ans, i - qry_l(i, s[i] & mask, !d) - 2);
            // printf("(%d, %d) Possible: %d~%d\n", b, i, qry_l(i, s[i] & mask, !d), i);
            c[d][s[i] & mask].push_back(i);
        }
    }
    if (ans > n) kout << "-1\n";
    else kout << ans << '\n';
    // for (int i = 1; i < n; ++i)
        // if (l.find_far())
    // choose 0 <= l < x < r <= n ====> s[l] ^ s[x] > s[x] ^ s[r];
    return 0;
}