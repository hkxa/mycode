/*************************************
 * @problem:      Codeforces Round #687 (Div. 1, based on Technocup 2021 Elimination Round 2).
 * @author:       hkxadpall.
 * @time:         2020-11-29.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;
namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}
#define getchar() (i_P1 == i_P2 && (i_P2 = (i_P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), i_P1 == i_P2) ? EOF : *i_P1++)
char BUF[1 << 21], *i_P1 = BUF, *i_P2 = BUF;
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 1e5 + 7;
int T, n, p, k, x, y;
char s[N];

// struct BinaryIndexTree {
//     int64 tr[N];
//     void reset() {
//         memset(tr, 0, sizeof(int64) * (n + 1));
//     }
//     void add(int u, int64 d) {
//         for (; u <= n; u += u & -u) tr[u] += d;
//     }
//     int64 qry(int u) {
//         int64 ret(0);
//         for (; u; u ^= u & -u) ret += tr[u];
//         return ret;
//     }
// } tr;

int64 f[N];

signed main() {
    kin >> T;
    while (T--) {
        kin >> n >> p >> k >> (s + 1) >> x >> y;
        // tr.reset();
        memset(f, 0, sizeof(int64) * (n + 1));
        int64 best(1e14), ans(1e14);
        for (int i = p; i <= n; ++i) {
            f[i] = (i - p) * y;
            if (i - k >= p) {
                f[i] = min(f[i], f[i - k]);
            }
            if (s[i] == '0') f[i] += x;
            if (n - i - k + 1 <= 0) ans = min(ans, f[i]);
            // printf("f[%d] = %I64d\n", i, f[i]);
        }
        kout << ans << '\n';
    }
    return 0;
}