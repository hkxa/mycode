//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      hdu - 2020 Multi-University Training Contest 2.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-28.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

int T, n;
char s[100007], ans[100007];
// int status[100007];
int st[100007], head, tail;

signed main() {
    read >> T;
    while (T--) {
        scanf("%s", s + 1);
        n = strlen(s + 1);
        for (int i = 1; i <= n; i++) ans[i] = s[i];
        int l = 0, r = 0, cntl = 0, cntr = 0, cntx = 0;
        head = 1; tail = 0;
        for (int i = 1; i <= n; i++) {
            if (s[i] == '(') {
                l++;
                r++;
                cntl++;
            } else if (s[i] == ')') {
                l = max(l - 1, 0);
                r--;
                cntr++;
                if (r < 0) {
                    puts("No solution!");
                    goto no_solution;
                }
            } else {
                l = max(l - 1, 0);
                r++;
                cntx++;
            }
            // printf("%d~%d\n", l, r);
        }
        if (l > 0) {
            puts("No solution!");
            goto no_solution;
        }
        for (int i = 1, status = 0; i <= n; i++) {
            if (s[i] == '(') {
                status++;
            } else if (s[i] == ')') {
                status--;
                if (status < 0) {
                    if (head > tail) {
                        puts("No solution!");
                        goto no_solution;
                    }
                    status++;
                    s[st[head++]] = '(';
                }
            } else {
                st[++tail] = i;
            }
            if (i == n) {
                while (status && head <= tail) {
                    s[st[tail--]] = ')';
                    status--;
                }
                if (status) {
                    puts("No solution!");
                    goto no_solution;
                }
            }
        }
        // for (int i = 1; i <= n; i++)
        //     putchar(s[i]);
        // putchar(10);
        // head = 1; tail = 0;
        // for (int i = n, status = 0; i >= 1; i--) {
        //     if (s[i] == ')') {
        //         status++;
        //     } else if (s[i] == '(') {
        //         status--;
        //         if (status < 0) {
        //             if (head > tail) {
        //                 puts("No solution!");
        //                 goto no_solution;
        //             }
        //             s[st[head++]] = ')';
        //         }
        //     } else {
        //         st[++tail] = i;
        //     }
        // }
        // for (int i = 1; i <= n; i++)
        //     putchar(s[i]);
        // putchar(32);
        for (int i = 1; i <= n; i++)
            if (s[i] != '*') putchar(s[i]);
        putchar(10);
        no_solution:;
    }
    return 0;
}