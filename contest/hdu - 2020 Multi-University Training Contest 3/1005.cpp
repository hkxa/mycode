//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      hdu - 2020 Multi-University Training Contest 2.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-28.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 1e5 + 7, P = 1e9 + 7;

int T, n;
int power[N];
int fa[N], sizR[N], sizC[N];
int cntR = 0, cntC = 0;

int StartAnswer() {
    cntR = cntC = 0;
    for (int i = 1; i <= n; i++) {
        if (power[i] == 1) cntR++;
        else cntC++;
    }
    return ((int64)cntR * cntC * (cntC - 1) / 2 + (int64)cntC * (cntC - 1) * (cntC - 2) / 6) % P;
}

int find(int u) {
    return u == fa[u] ? u : fa[u] = find(fa[u]);
}

int unify(int u, int v) {
    u = find(fa[u]);
    v = find(fa[v]);
    int64 r1 = sizR[u], r2 = sizR[v], c1 = sizC[u], c2 = sizC[v];
    fa[v] = u;
    sizC[u] += sizC[v];
    sizR[u] += sizR[v];
    int64 ans1 = r2 * c1 * (cntC - c1 - c2) % P;
    int64 ans2 = r1 * c2 * (cntC - c1 - c2) % P;
    int64 ans3 = c1 * c2 * (cntC - c1 - c2) % P;
    int64 ans4 = c1 * c2 * (cntR - r1 - r2) % P;
    return (ans1 + ans2 + ans3 + ans4) % P;
}

signed main() {
    read >> T;
    while (T--) {
        read >> n;
        for (int i = 1; i <= n; i++) {
            fa[i] = i;
            read >> power[i];
            if (power[i] == 1) {
                sizC[i] = 0;
                sizR[i] = 1;
            } else {
                sizC[i] = 1;
                sizR[i] = 0;
            }
        }
        int ans = StartAnswer();
        write << ans << '\n';
        for (int i = 1, u, v; i < n; i++) {
            read >> u >> v;
            ans = (ans + P - unify(u, v)) % P;
            write << ans << '\n';
        }
    }
}