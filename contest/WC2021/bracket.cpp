/*************************************
 * @problem:      bracket.
 * @author:       brealid(赵奕).
 * @time:         2021-02-05.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

template <typename N>
N zy_gcd(N a, N b) {
    if (!a || !b) return a | b;
    return zy_gcd(b, a % b);
}

template<typename N> 
N zy_abs(N a) {
    return a > 0 ? a : -a;
}

int n, m, k;

namespace n8 {
    vector<pair<int, int> > G[20];
    bool vis[20][20][20];
    int ans = 0, rt;
    void dfs(int u, int A, int B) {
        if (!A && !B && u > rt) ++ans;
        if (A > n * 2 || B > n * 2) return;
        vis[u][A][B] = true;
        for (size_t i = 0; i < G[u].size(); ++i) {
            int v = G[u][i].first, w = G[u][i].second;
            int nA = A, nB = B;
            if (w == 1 || w == -1) nA += w;
            else nB += w / 2;
            if (nA < 0 || nB < 0 || vis[v][nA][nB]) continue;
            dfs(v, nA, nB);
        }
    }
    void solve() {
        for (int i = 1, u, v, w; i <= m; ++i) {
            kin >> u >> v >> w;
            G[u].push_back(make_pair(v, w));
            G[v].push_back(make_pair(u, -w));
        }
        for (rt = 1; rt <= n; ++rt) {
            memset(vis, 0, sizeof(vis));
            dfs(rt, 0, 0);
        }
        kout << ans << '\n';
    }
}

namespace tree {
    const int sN = 3e5 + 7;
    vector<pair<int, int> > G[sN];
    stack<int> s;
    int ans = 0;
    void get_ans(int u, int fa, int from) {
        // printf("get_ans(%d, %d, %d): total = %d\n", u, fa, from, total);
        if (u > from && s.empty()) ++ans;
        // if (u > from && !total) printf("(%d, %d)\n", from, u);
        for (size_t i = 0; i < G[u].size(); ++i) {
            int v = G[u][i].first, w = G[u][i].second;
            if (v == fa || (w < 0 && (s.empty() || s.top() + w))) continue;
            if (w > 0) s.push(w);
            else s.pop();
            get_ans(v, u, from);
            if (w > 0) s.pop();
            else s.push(-w);
        }
    }
    void solve() {
        for (int i = 1, u, v, w; i <= m; ++i) {
            kin >> u >> v >> w;
            G[u].push_back(make_pair(v, w));
            G[v].push_back(make_pair(u, -w));
        }
        for (int i = 1; i <= n; ++i)
            get_ans(i, 0, i);
        kout << ans << '\n';
    }
}

signed main() {
    file_io::set_to_file("bracket");
    kin >> n >> m >> k;
    if (n <= 8) {
        n8::solve();
        return 0;
    }
    if (m == n - 1) {
        tree::solve();
        return 0;
    }
    return 0;
}