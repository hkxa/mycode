/*************************************
 * @problem:      expr.
 * @author:       brealid(赵奕).
 * @time:         2021-02-05.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

const int M = 11, N = 5e4 + 7, P = 1e9 + 7;

int n, m;
int a[M][N];
char qs[N];

// struct Arr10 {
//     int v[10];
//     Arr10() { memset(v, 0, sizeof(v)); }
// };

struct value {
    vector<int64> cnt[10];
    value(){}
    void resize(int S) { for (int i = 0; i < m; ++i) cnt[i].resize(S); }
    value operator + (const value &b) const {
        value ret = *this;
        for (int i = 0; i < m; ++i)
            for (int j = 0; j < n; ++j)
                ret.cnt[i][j] += b.cnt[i][j];
        return ret;
    }
} st[10];

value max(const value &x, const value &y) {
    value ret;
    ret.resize(n);
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < m; ++j)
            for (int k = 0; k < n; ++k)
                if (a[i][k] > a[j][k])
                    ret.cnt[i][k] = (ret.cnt[i][k] + x.cnt[i][k] * y.cnt[j][k]) % P;
                else
                    ret.cnt[j][k] = (ret.cnt[j][k] + x.cnt[i][k] * y.cnt[j][k]) % P;
    return ret;
}

value min(const value &x, const value &y) {
    value ret;
    ret.resize(n);
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < m; ++j)
            for (int k = 0; k < n; ++k)
                if (a[i][k] < a[j][k])
                    ret.cnt[i][k] = (ret.cnt[i][k] + x.cnt[i][k] * y.cnt[j][k]) % P;
                else
                    ret.cnt[j][k] = (ret.cnt[j][k] + x.cnt[i][k] * y.cnt[j][k]) % P;
    return ret;
}

value solve(int l, int r) {
    // printf("solve(%d, %d)\n", l, r);
    if (l == r) return st[qs[l] - '0'];
    for (int i = r, top = 0; i >= l; --i)
        if (qs[i] == ')') ++top;
        else if (qs[i] == '(') --top;
        else if (!top) {
            if (qs[i] == '<') return min(solve(l, i - 1), solve(i + 1, r));
            if (qs[i] == '>') return max(solve(l, i - 1), solve(i + 1, r));
            if (qs[i] == '?') return min(solve(l, i - 1), solve(i + 1, r)) + max(solve(l, i - 1), solve(i + 1, r));
            // if (qs[i] == '?') return solve(l, i - 1) + solve(i + 1, r);
        }
    return solve(l + 1, r - 1);
}

// value solve2(int l, int r) {
//     // printf("solve(%d, %d)\n", l, r);
//     if (l == r) return st[qs[l] - '0'];
//     for (int i = r, top = 0; i >= l; --i)
//         if (qs[i] == ')') ++top;
//         else if (qs[i] == '(') --top;
//         else if (!top) {
//             if (qs[i] == '<') return min(solve(l, i - 1), solve(i + 1, r));
//             if (qs[i] == '>') return max(solve(l, i - 1), solve(i + 1, r));
//             if (qs[i] == '?') return min(solve(l, i - 1), solve(i + 1, r)) + max(solve(l, i - 1), solve(i + 1, r));
//             // if (qs[i] == '?') return solve(l, i - 1) + solve(i + 1, r);
//         }
//     return solve(l + 1, r - 1);
// }

void solve_small() {
    for (int i = 0; i < m; ++i) {
        st[i].resize(n);
        for (int j = 0; j < n; ++j)
            st[i].cnt[i][j] = 1;
    }
    value ans = solve(1, strlen(qs + 1));
    // value ans = solve(1, 3);
    int64 total_ans = 0;
    // for (int i = 0; i < n; ++i)
    //     for (int j = 0; j < m; ++j)
    //         printf("ans[%d] by array[%d]: %d counts\n", i, j, ans.cnt[j][i]);
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            total_ans = (total_ans + ans.cnt[i][j] * a[i][j]) % P;
    kout << total_ans << '\n';
}

#define min(a, b) (a < b ? a : b)
#define max(a, b) (a > b ? a : b)
void solve_noW() {
    // printf("ll");
    int p[N], SIZE = n * sizeof(int);
    memcpy(p, a[qs[1] - '0'], SIZE);
    for (int i = 1; qs[i]; i += 2) {
        int *d = a[qs[i + 1] - '0'];
        if (qs[i] == '<')
            for (int i = 0; i < n; i += 8) {
                p[i] = min(p[i], d[i]);
                p[i + 1] = min(p[i + 1], d[i + 1]);
                p[i + 2] = min(p[i + 2], d[i + 2]);
                p[i + 3] = min(p[i + 3], d[i + 3]);
                p[i + 4] = min(p[i + 4], d[i + 4]);
                p[i + 5] = min(p[i + 5], d[i + 5]);
                p[i + 6] = min(p[i + 6], d[i + 6]);
                p[i + 7] = min(p[i + 7], d[i + 7]);
            }
        else
            for (int i = 0; i < n; i += 8) {
                p[i] = max(p[i], d[i]);
                p[i + 1] = max(p[i + 1], d[i + 1]);
                p[i + 2] = max(p[i + 2], d[i + 2]);
                p[i + 3] = max(p[i + 3], d[i + 3]);
                p[i + 4] = max(p[i + 4], d[i + 4]);
                p[i + 5] = max(p[i + 5], d[i + 5]);
                p[i + 6] = max(p[i + 6], d[i + 6]);
                p[i + 7] = max(p[i + 7], d[i + 7]);
            }
    }
    int64 ans = 0;
    for (int i = 0; i < n; ++i)
        ans += p[i];
    kout << ans % P << '\n';
}
#undef min
#undef max

signed main() {
    file_io::set_to_file("expr");
    kin >> n >> m;
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            kin >> a[i][j];
    kin >> (qs + 1);
    if (n <= 10) {
        solve_small();
        return 0;
    }
    if (string(qs + 1).find('?') == string::npos) {
        solve_noW();
        return 0;
    }
    solve_small();
    return 0;
}