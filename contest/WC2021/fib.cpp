/*************************************
 * @problem:      fib.
 * @author:       brealid(赵奕).
 * @time:         2021-02-05.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

int64 gcd(int64 a, int64 b) {
    if (!a || !b) return a | b;
    return gcd(b, a % b);
}

int64 lcm(int64 n, int64 m) {
    return n / gcd(n, m) * m;
}

int64 Get_ModPeriod(int64 p) {
    int64 pi[10007], k[10007];
    switch (p) {
        case 2 : return 3;
        case 3 : return 8;
        case 5 : return 20;
        default : break; // use regular way
    }
    int64 s = sqrt(p), tot = 0;
    for (int64 i = 2; i <= s; ++i)
        if (p % i == 0) {
            pi[++tot] = i;
            k[tot] = 1;
            while (p % i == 0) {
                p /= i, k[tot] *= i;
            }
        }
    for (int64 i = 1; i <= tot; ++i) k[i] /= pi[i];
    if (p ^ 1) k[++tot] = 1, pi[tot] = p;
    for (int64 i = 1; i <= tot; ++i) {
        if (pi[i] == 2) k[i] *= 3;
        else if (pi[i] == 3) k[i] *= 5;
        else if (pi[i] == 5) k[i] *= 20;
        else if (pi[i] % 5 == 1|| pi[i] % 5 == 4) k[i] *= pi[i] - 1;
        else k[i] *= (pi[i] + 1) << 1;
    }
    int64 ans = k[1];
    for (int64 i = 2; i <= tot; ++i) ans = lcm(ans, k[i]);
    return ans;
}

const int N = 1e5 + 7;

int n, m;
int64 period;
map<pair<int, int>, int> ans[N];
set<int> where0[N];

void solve(map<pair<int, int>, int> &s, int p) {
    period = Get_ModPeriod(p);
    int a = 0, b = 1;
    for (int i = 0; i < period; ++i) {
        s[make_pair(a, b)] = i;
        if (a == 58466 && b == 43134) printf("orch!\n");
        if (a == 0 && b == 29233) printf("orch 2!\n");
        if (!a) where0[p].insert(i);
        int c = (a + b) % p;
        a = b;
        b = c;
    }
}

bitset<1007> visiting[1007];
int sd_ans[1007][1007];
// map<int, map<int, int> > sd_ans;
int solve_small_dfs(int u, int v) {
    if (!u) return 0;
    if (sd_ans[u][v]) return sd_ans[u][v];
    if (visiting[u].test(v)) return sd_ans[u][v] = -1;
    visiting[u].set(v);
    int nRet = solve_small_dfs(v, (u + v) % m);
    visiting[u].reset(v);
    return sd_ans[u][v] = (~nRet ? nRet + 1 : -1);
}

void solve_small() {
    for (int i = 1, u, v; i <= n; ++i) {
        kin >> u >> v;
        kout << solve_small_dfs(u, v) << '\n';
    }
}

signed main() {
    file_io::set_to_file("fib");
    kin >> n >> m;
    if (n <= 1000 && m <= 1000) {
        solve_small();
        return 0;
    }
    for (int i = 2; i <= m; ++i)
        if (m % i == 0) solve(ans[i], i);
    for (int i = 1, u, v; i <= n; ++i) {
        kin >> u >> v;
        if (!u) {
            kout << "0\n";
            continue;
        }
        if (!v) {
            kout << "1\n";
            continue;
        }
        int g = gcd(gcd(u, v), m), m_g = m / g;
        map<pair<int, int>, int>::iterator it = ans[m_g].find(make_pair(u / g, v / g));
        if (it != ans[m_g].end()) {
            set<int>::iterator it2 = where0[m_g].lower_bound(it->second);
            if (it2 != where0[m_g].end()) kout << *it2 - it->second << '\n';
            else kout << ans[m_g].size() - it->second << '\n';
        } else kout << "-1\n";
    }
    return 0;
}