/*************************************
 * problem:      Contest #14 C. 
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-11-08.
 * language:     C++.
 * upload place: Comet OJ.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, q;
int a[2007] = {0};
int l[2007], r[2007];
int f[2007] = {0};
int ans = 0;

int main()
{
    n = read<int>();
    q = read<int>();
    f[n] = 1;
    for (int i = 1, pow2 = 1; i <= q; i++, pow2 = (pow2 << 1) % 20050321) {
        l[i] = read<int>();
        r[i] = read<int>();
        for (int j = 1; j < l[i] - 1; j++) {
            f[j] = (f[j] << 1) % 20050321;
        }
        f[l[i] - 1] = (f[l[i] - 1] + pow2) % 20050321;
        f[r[i]] = (f[r[i]] + pow2) % 20050321;
        for (int j = r[i] + 1; j <= n; j++) {
            f[j] = (f[j] << 1) % 20050321;
        }
        ans = 0;
        for (int j = 1; j <= n; j++) {
            // write(f[j], j == n ? 10 : 32);
            ans = (ans + f[j]) % 20050321;
        }
        write(ans, 10);
    }
    return 0;
}