/*************************************
 * problem:      A.
 * user name:    Jomoo.
 * time:         2019-09-21.
 * language:     C++.
 * upload place: Comet OJ.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
#define ForInVec(vectorType, vectorName, iteratorName) for (vector<vectorType>::iterator iteratorName = vectorName.begin(); iteratorName != vectorName.end(); iteratorName++)
#define ForInVI(vectorName, iteratorName) ForInVec(int, vectorName, iteratorName)
#define ForInVE(vectorName, iteratorName) ForInVec(Edge, vectorName, iteratorName)
#define MemWithNum(array, num) memset(array, num, sizeof(array))
#define Clear(array) MemWithNum(array, 0)
#define MemBint(array) MemWithNum(array, 0x3f)
#define MemInf(array) MemWithNum(array, 0x7f)
#define MemEof(array) MemWithNum(array, -1)
#define ensuref(condition) do { if (!(condition)) exit(0); } while(0)

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

struct FastIOer {
#   define createReadlnInt(type)            \
    FastIOer& operator >> (type &x)         \
    {                                       \
        x = read<type>();                   \
        return *this;                       \
    }
    createReadlnInt(short);
    createReadlnInt(unsigned short);
    createReadlnInt(int);
    createReadlnInt(unsigned);
    createReadlnInt(long long);
    createReadlnInt(unsigned long long);
#   undef createReadlnInt

#   define createWritelnInt(type)           \
    FastIOer& operator << (type &x)         \
    {                                       \
        write<type>(x);                     \
        return *this;                       \
    }
    createWritelnInt(short);
    createWritelnInt(unsigned short);
    createWritelnInt(int);
    createWritelnInt(unsigned);
    createWritelnInt(long long);
    createWritelnInt(unsigned long long);
#   undef createWritelnInt
    
    FastIOer& operator >> (char &x)
    {
        x = getchar();
        return *this;
    }
    
    FastIOer& operator << (char x)
    {
        putchar(x);
        return *this;
    }
    
    FastIOer& operator << (const char *x)
    {
        int __pos = 0;
        while (x[__pos]) {
            putchar(x[__pos++]);
        }
        return *this;
    }
} fast;

const long long p = 998244353;

template <typename Int>
Int lowbit(Int x) 
{
	return x & (-x);
}

template <typename Int>
struct BIT {
    Int *C;
    int arrSize;
    BIT(int size = 100007) // 10W
    {
        arrSize = size;
		C = new Int[size + 1];
        memset(C, 0, (size + 1) * sizeof(Int));
        for (int i = 0; i <= size; i++) C[i] = 1; 
    }
    void add(Int x, Int delta)
    {
        while (x <= arrSize)
        {
            C[x] = C[x] * delta % p;
            x += lowbit(x);
        }
    }
    Int sum(Int x)
    {
        Int res(1);
        while (x)
        {
            res = res * C[x] % p;
            x -= lowbit(x);
        }
        return res;
    }
};

template <typename Int>
Int ksm(Int a, Int b) 
{
    Int res = 1;
    while (b) {
        if (b & 1) res = res * a % p;
        a = a * a % p;
        b >>= 1;
    }
    return res;
} 

#define inv(a) ksm(a, p - 2)
#define div(a, b) (a * inv(b) % p)

int n, m, q;
long long a[100007];
BIT<long long> t;
vector<int> G[100007];

int main()
{
    fast >> n >> m >> q;
    for (int i = 1; i <= n; i++) {
        fast >> a[i];
        t.add(i, a[i]);
    }
    int u, v;
    for (int i = 1; i <= m; i++) {
        fast >> u >> v;
        G[u].push_back(v);
        G[v].push_back(u);
    }    
    int op, x, y;
    while (q--) {
        fast >> op >> x >> y;
        if (op == 1) {
            if (x > y) fast << "0\n";
            else {
                // fast << (long long)t.sum(y) << '\n';
                write(t.sum(y), '\n');
            }
        } else {
            y %= p;
            t.add(x, div(y, a[x]));
            a[x] = y;
        }
    }
    return 0;
}