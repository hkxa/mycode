/*************************************
 * problem:      Contest #14 B. 
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-11-08.
 * language:     C++.
 * upload place: Comet OJ.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define aS [1000007]

int T, len;
char s aS;
int I = -1, N = -1, K = -1;
int p = -1, i = -1, n = -1;
int ans;

int main()
{
    T = read<int>();
    while (T--) {
        len = read<int>();
        scanf("%s", s + 1);
        p = i = n = I = N = K = EOF;
        for (int j = len; j >= 1; j--) {
            // printf("%c", s[j]);
            switch (s[j]) {
                case 'k': if (K == EOF) K = j; break;
                case 'n': if (~K && N == EOF) N = j; break;
                case 'i': if (~K && ~N && I == EOF) I = j; break;
                default: break;
            }
        }
        for (int j = 1; j <= len; j++) {
            switch (s[j]) {
                case 'p': if (p == EOF) p = j; break;
                case 'i': if (~p && i == EOF) i = j; break;
                case 'n': if (~p && ~i && n == EOF) n = j; break;
                default: break;
            }
        }
        ans = -1;
        // printf("p = %d, i = %d, n = %d.\n", p, i, n);
        // printf("I = %d, N = %d, K = %d.\n", I, N, K);
        if (~p && ~I) ans = max(ans, I - p - 1);
        if (~i && ~N) ans = max(ans, N - i - 1);
        if (~n && ~K) ans = max(ans, K - n - 1);
        write(ans, 10);
    }
    return 0;
}