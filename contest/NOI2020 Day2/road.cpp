//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      road.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-19.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 5e5 + 7;

int n, m, s, t;
vector<pair<int, int> > G[N];
int64 dis[N];
bool inq[N];

int64 dij(int s, int t) {
    memset(dis, 0x3f, sizeof(dis));
    dis[s] = 0;
    inq[s] = 1;
    priority_queue<pair<int64, int>, vector<pair<int64, int> >, greater<pair<int64, int> > > q;
    q.push(make_pair(0LL, s));
    while (!q.empty()) {
        int u = q.top().second; q.pop();
        inq[u] = 0;
        for (size_t i = 0; i < G[u].size(); i++) {
            int v = G[u][i].first, w = G[u][i].second;
            if (dis[v] > dis[u] + w) {
                dis[v] = dis[u] + w;
                if (!inq[v]) {
                    q.push(make_pair(dis[v], v));
                    inq[v] = true;
                }
            }
        }
    }
    return dis[t];
}

set<pair<int, int> > erased;
int deg[N];

int64 minn_dis;

void dfs(int u, int64 now, int destory_rest) {
    if (now >= minn_dis) return;
    if (u == t) return void(minn_dis = now);
    if (destory_rest <= 0) return;
    deg[u]--; 
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i].first, w = G[u][i].second;
        if (erased.count(make_pair(min(u, v), max(u, v)))) continue;
        if (deg[v] <= 1 || (deg[v] <= 2 && v != t)) continue;
        deg[v]--; erased.insert(make_pair(min(u, v), max(u, v)));
        dfs(v, now + w, destory_rest - 1);
        deg[v]++; erased.erase(make_pair(min(u, v), max(u, v)));
    }
    deg[u]++; 
}

int64 solve_small() {
    minn_dis = 0x3f3f3f3f3f3f3f3f;
    for (int i = 1; i <= n; i++) deg[i] = G[i].size();
    dfs(s, 0, m - n + 1);
    return (minn_dis == 0x3f3f3f3f3f3f3f3f ? -1 : minn_dis);
}

signed main() {
    // freopen("road.in", "r", stdin);
    // freopen("road.out", "w", stdout);
    read >> n >> m;
    for (int i = 1, u, v, w; i <= m; i++) {
        read >> u >> v >> w;
        G[u].push_back(make_pair(v, w));
        G[v].push_back(make_pair(u, w));
    }
    read >> s >> t;
    if (G[s].size() == 1) {
        puts("-1");
        return 0;
    }
    int64 ret = dij(s, t);
    if (n <= 20)
        ret = max(ret, solve_small());
    write << ret << '\n';
    return 0;
}