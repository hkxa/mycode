//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      dish.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-19.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 500 + 7, M = 5000 + 7, K = 5000 + 7;

int T, n, m, k;
int d[N];

struct case_1to5 { // n, m <= 10
    int x[N], cx[N], y[N], cy[N];
    bool dfs(int u) {
        if (u > m) {
            for (int i = 1; i <= m; i++) {
                if (cx[i] == k) printf("%d %d\n", x[i], cx[i]);
                else printf("%d %d %d %d\n", x[i], cx[i], y[i], cy[i]);
            }
            return true;
        }
        int id = 0;
        for (int i = 1; i <= n; i++)
            if (d[i] && (!id || d[i] < d[id])) id = i;
        // fprintf(stderr, "dfs(%d, %d) : \n", u, id);
        // for (int i = 1; i < u; i++) {
        //     if (cx[i] == k) fprintf(stderr, "  %d %d\n", x[i], cx[i]);
        //     else fprintf(stderr, "  %d %d %d %d\n", x[i], cx[i], y[i], cy[i]);
        // }
        if (d[id] >= k) {
            d[id] -= k;
            x[u] = id; cx[u] = k;
            if (dfs(u + 1)) return true;
            d[id] += k;
        } else {
            x[u] = id; cx[u] = d[id]; d[id] = 0;
            for (int i = 1; i <= n; i++) {
                if (i == id || cx[u] + d[i] < k) continue;
                y[u] = i; cy[u] = k - cx[u];
                d[i] -= cy[u];
                if (dfs(u + 1)) return true;
                d[i] += cy[u];
            }
            d[id] = cx[u];
        }
        return false;
    }
    void solve() {
        if (!dfs(1)) puts("-1");
    }
};

struct case_6to7 { // n, m <= 10
    int x[N], cx[N], y[N], cy[N];
    bool dfs(int u) {
        if (u > m) {
            for (int i = 1; i <= m; i++) {
                if (cx[i] == k) printf("%d %d\n", x[i], cx[i]);
                else printf("%d %d %d %d\n", x[i], cx[i], y[i], cy[i]);
            }
            return true;
        }
        int id = 0;
        for (int i = 1; i <= n; i++)
            if (d[i] && (!id || d[i] < d[id])) id = i;
        // fprintf(stderr, "dfs(%d, %d) : \n", u, id);
        // for (int i = 1; i < u; i++) {
        //     if (cx[i] == k) fprintf(stderr, "  %d %d\n", x[i], cx[i]);
        //     else fprintf(stderr, "  %d %d %d %d\n", x[i], cx[i], y[i], cy[i]);
        // }
        if (d[id] >= k) {
            d[id] -= k;
            x[u] = id; cx[u] = k;
            if (dfs(u + 1)) return true;
            d[id] += k;
        } else {
            x[u] = id; cx[u] = d[id]; d[id] = 0;
            for (int i = 1; i <= n; i++) {
                if (i == id || cx[u] + d[i] < k) continue;
                y[u] = i; cy[u] = k - cx[u];
                d[i] -= cy[u];
                if (dfs(u + 1)) return true;
                d[i] += cy[u];
            }
            d[id] = cx[u];
        }
        return false;
    }
    void solve() {
        if (!dfs(1)) puts("-1");
    }
};

signed main() {
    // freopen("dish.in", "r", stdin);
    // freopen("dish.out", "w", stdout);
    read >> T;
    while (T--) {
        // if (clock() > 1.9 * CLOCKS_PER_SEC) {
        //     for (int i = 0; i <= T; i++) {
        //         puts("-1");
        //         return 0;
        //     }
        // }
        read >> n >> m >> k;
        for (int i = 1; i <= n; i++) read >> d[i];
        if (n > m * 2) {
            puts("-1");
            continue;
        }
        if (n <= 10 && m <= 10) {
            case_1to5 object;
            object.solve();
            continue;
        }
        if (m == n - 1) {
            case_6to7 object;
            object.solve();
            continue;
        }
        case_1to5 object;
        object.solve();
    }
    return 0;
}