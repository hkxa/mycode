//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      surreal.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-19.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 2e6 + 7, P = 998244353;

int T, n, m;
vector<int>  tl[N], tr[N];

namespace sub_chain {
    int root, id;
    int t[N], l[N], r[N];
    double complete_percent[N];
    void build(int tid, int &u, int now) {
        if (!u) {
            u = ++id;
            t[u] = l[u] = r[u] = 0;
            complete_percent[u] = 0;
        }
        if (t[u]) return;
        if (tl[tid][now]) build(tid, l[u], tl[tid][now]);
        if (tr[tid][now]) build(tid, r[u], tr[tid][now]);
        if (!tl[tid][now] && !tr[tid][now]) t[u] = true;
        if (t[l[u]] && t[r[u]]) t[u] = true;
        if (t[u] == true) complete_percent[u] = 1;
        else complete_percent[u] = (complete_percent[l[u]] + complete_percent[r[u]]) / 2;
        // fprintf(stderr, "node %d : l = %d, r = %d, t[l] = %d, t[r] = %d\n", u, l[u], r[u], t[l[u]], t[r[u]]);
    }
}

bool is_chain(int tid, int u) {
    if (tl[tid][u] && tr[tid][u]) return false;
    if (!tl[tid][u] && !tr[tid][u]) return true;
    return is_chain(tid, tl[tid][u] | tr[tid][u]);
}

const uint64 my_ull_MAX = 2ULL * 9223372036854775807LL + 1;

double rand_double_0to1() {
    uint64 s = (uint64)rand() * (uint64)rand() * (uint64)rand() * (uint64)rand() * (uint64)rand();
    // fprintf(stderr, "random %.6lf\n", pow((double)s / my_ull_MAX, (double)s / my_ull_MAX));
    return pow((double)s / my_ull_MAX, (double)s / my_ull_MAX);
}

int get_maxh(int tid, int u) {
    int ret = 1;
    if (tl[tid][u]) ret = max(ret, get_maxh(tid, tl[tid][u]) + 1);
    if (tr[tid][u]) ret = max(ret, get_maxh(tid, tr[tid][u]) + 1);
    return ret;
}

signed main() {
    srand(time(0) ^ clock() ^ 20170933);
    // freopen("surreal.in", "r", stdin);
    // freopen("surreal.out", "w", stdout);
    read >> T;
    while (T--) {
        read >> m;
        int special_pd = 0, cnt_not_chain = 0;
        sub_chain::root = sub_chain::id = sub_chain::complete_percent[1] = sub_chain::t[1] = 0;
        int maxh = 0;
        for (int i = 1; i <= m; i++) {
            read >> n;
            tl[i].resize(n + 1);
            tr[i].resize(n + 1);
            for (int j = 1; j <= n; j++) read >> tl[i][j] >> tr[i][j];
            // if (is_chain(i, 1)) printf("chain #%d-%d\n", T, i);
            if (is_chain(i, 1)) sub_chain::build(i, sub_chain::root, 1);
            else cnt_not_chain++;
            maxh = max(maxh, get_maxh(i, 1));
            if (n == 1) special_pd = 1;
        }
        if (special_pd || (sub_chain::t[1] && !cnt_not_chain)) {
            puts("Almost Complete");
            continue;
        }
        if (maxh <= 2) {
            int emm = 0;
            for (int i = 1; i <= m; i++) {
                if (tl[i].size() == 2) emm |= 3;
                else if (tl[i].size() == 2) {
                    if (tl[i][1]) emm |= 1;
                    else if (tr[i][1]) emm |= 2;
                }
            }
            if (emm == 3) puts("Almost Complete");
            else puts("No");
            continue;
        }
        // fprintf(stderr, "expect %.6lf\n", sub_chain::complete_percent[1] * pow((double)cnt_not_chain / m, 2.7182818284590452354));
        if (rand_double_0to1() <= sub_chain::complete_percent[1] * pow((double)cnt_not_chain / m, 2.7182818284590452354)) {
            puts("Almost Complete");
            continue;
        }
        puts("No");
    }
    return 0;
}

/*
#define M_E		    2.7182818284590452354
#define M_LOG2E		1.4426950408889634074
#define M_LOG10E	0.43429448190325182765
#define M_LN2		0.69314718055994530942
#define M_LN10		2.30258509299404568402
#define M_PI		3.14159265358979323846
#define M_PI_2		1.57079632679489661923
#define M_PI_4		0.78539816339744830962
#define M_1_PI		0.31830988618379067154
#define M_2_PI		0.63661977236758134308
#define M_2_SQRTPI	1.12837916709551257390
#define M_SQRT2		1.41421356237309504880
#define M_SQRT1_2	0.70710678118654752440
*/


