/*************************************
 * @contest:      AtCoder - AtCoder Beginner Contest 214.
 * @author:       brealid.
 * @time:         2021-08-14.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

// #define USE_FREAD  // 使用 fread  读入，去注释符号
// #define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 2e5 + 7;

int T, n;
struct distric {
    mutable int l, r;
    distric() {}
    distric(int L, int R) : l(L), r(R) {}
    bool operator < (const distric &d) const {
        return l < d.l;
    }
} st[N];
bool comp(const distric &a, const distric &b) {
    return a.r != b.r ? a.r < b.r : a.l < b.l;
    // return a.l != b.l ? a.l < b.l : a.r < b.r;
}

#define check_limit(exp) if (!(exp)) { kout << "No\n"; goto NoSol; }

void solve_way1() {
    sort(st + 1, st + n + 1, comp);
    set<distric> available;
    available.insert(distric(1, 1000000000));
    for (int i = 1; i <= n; ++i) {
        set<distric>::iterator it = available.lower_bound(distric(st[i].l, st[i].l));
        // if (it != available.end()) printf("[# 1] it = [%d, %d]\n", it->l, it->r);
        if (it != available.begin()) {
            --it;
            // printf("[# 2] it = [%d, %d]\n", it->l, it->r);
            if (it->r >= st[i].l) {
                if (it->r > st[i].l) available.insert(distric(st[i].l + 1, it->r));
                // printf("[1] range[%d, %d] use %d\n", st[i].l, st[i].r, st[i].l);
                if (it->l < st[i].l) it->r = st[i].l - 1;
                else available.erase(it);
                continue;
            }
            ++it;
        }
        check_limit(it != available.end());
        check_limit(it->l <= st[i].r);
        // printf("[2] range[%d, %d] use %d\n", st[i].l, st[i].r, it->l);
        if (it->l < it->r) ++(it->l);
        else available.erase(it);
    }
    kout << "Yes\n";
    NoSol:;
}

signed main() {
    for (kin >> T; T--;) {
        kin >> n;
        for (int i = 1; i <= n; ++i) kin >> st[i].l >> st[i].r;
        solve_way1();

    }
    return 0;
}