/*************************************
 * @contest:      AtCoder - AtCoder Beginner Contest 214.
 * @author:       brealid.
 * @time:         2021-08-14.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

// #define USE_FREAD  // 使用 fread  读入，去注释符号
// #define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 1e5 + 7;

struct UnionFindSet {
    int fa[N + 5];
    UnionFindSet() {
        memset(fa, -1, sizeof(fa));
    }
    // 重置清零（下标从 1 开始）
    inline void reset(int n = sizeof(fa) / sizeof(int) - 1) {
        memset(fa, -1, (n + 1) * sizeof(int));
    }
    // 寻找 u 的祖先
    int find(int u) {
        return fa[u] < 0 ? u : fa[u] = find(fa[u]);
    }
    // 判断 u 是否为所在树的跟
    inline int is_root(int u) {
        return fa[u] < 0;
    }
    // 获取 u 所属的连通块的大小
    inline int get_size(int u) {
        return -fa[find(u)];
    }
    // 判断两个节点是否处于一棵树中
    inline bool is_family(int u, int v) {
        return find(u) == find(v);
    }
    // 合并两棵树，成功返回新的树的根节点编号，已经在同一棵树中了返回 0
    inline int merge(int u, int v) {
        u = find(u), v = find(v);
        if (u == v) return 0;
        if (fa[u] > fa[v]) swap(u, v);
        fa[u] += fa[v];
        return fa[v] = u;
    }
    // 合并两棵树，成功返回新的树的根节点编号，已经在同一棵树中了返回 0
    // f: 一个函数，若合并成功则会调用
    // 两个参数为新连边的两个端点。第一个参数是新的父亲节点，第二个参数是新的孩子节点
    template <typename FunctionType>
    inline int merge(int u, int v, FunctionType f) {
        u = find(u), v = find(v);
        if (u == v) return 0;
        if (fa[u] > fa[v]) swap(u, v);
        fa[u] += fa[v];
        f(u, v);
        return fa[v] = u;
    }
} ufs;

int n;
struct edge {
    int u, v, d;
    bool operator < (const edge &b) const { return d < b.d; }
} e[N];

signed main() {
    kin >> n;
    for (int i = 1; i < n; ++i)
        kin >> e[i].u >> e[i].v >> e[i].d;
    sort(e + 1, e + n);
    int64 ans = 0;
    for (int i = 1; i < n; ++i) {
        ans += (int64)e[i].d * ufs.get_size(e[i].u) * ufs.get_size(e[i].v);
        ufs.merge(e[i].u, e[i].v);
    }
    kout << ans << '\n';
    return 0;
}