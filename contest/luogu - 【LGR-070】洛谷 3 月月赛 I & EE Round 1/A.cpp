/*************************************
 * @contest:      luogu - 【LGR-070】洛谷 3 月月赛 I & EE Round 1 Div.2.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-03-08.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

char a[10][10];
bool could[10][10];

void explor(int x, int y, int dx, int dy)
{
    x += dx;
    y += dy;
    while (x >= 1 && x <= 8 && y >= 1 && y <= 8 && a[x][y] == '.') {
        could[x][y] = 0;
        x += dx;
        y += dy;
    }
}

int main()
{
    memset(could, 1, sizeof(could));
    for (int i = 1; i <= 8; i++) scanf("%s", a[i] + 1);
    for (int i = 1; i <= 8; i++) {
        for (int j = 1; j <= 8; j++) {
            if (a[i][j] == 'R') {
                could[i][j] = false; 
                explor(i, j, 0, 1); 
                explor(i, j, 0, -1); 
                explor(i, j, 1, 0); 
                explor(i, j, -1, 0);
            } else if (a[i][j] == 'B') {
                could[i][j] = false; 
                explor(i, j, -1, 1); 
                explor(i, j, -1, -1); 
                explor(i, j, 1, 1); 
                explor(i, j, 1, -1);
            } 
        }
    }
    for (int i = 1; i <= 8; i++) {
        for (int j = 1; j <= 8; j++) {
            if (could[i][j]) putchar('1');
            else putchar('0');
        }
        putchar(10);
    }
    return 0;
}