/*************************************
 * @contest:      luogu - 【LGR-070】洛谷 3 月月赛 I & EE Round 1 Div.2.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-03-08.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;
int a[1000007];
int64 f[5007][5007];
int64 ans = 0;
#define cost(i, j, k) ((int64)a[i] * a[j] * a[k])

int main()
{
    n = read<int>();
    a[0] = a[n + 1] = 1;
    for (int i = 1; i <= n; i++) {
        a[i] = read<int>();
    }
    if (n <= 500) {
        for (int l = 1; l <= n; l++) {
            for (int i = 1, j = l; j <= n; i++, j++) {
                f[i][j] = 0x3f3f3f3f3f3f;
                for (int k = i; k <= j; k++) {
                    f[i][j] = min(f[i][j], f[i][k - 1] + f[k + 1][j] + cost(i - 1, k, j + 1));
                }
            }
        }
        write(f[1][n], 10);
    } else {
        int x = 0, i = 1;
        for (; i <= n; i++) {
            if (a[i] == 1) {
                x = i;
                break;
            }
        }
        if (x >= 2) ans = 4 * (x - 2) + 2;
        else ans = 0;
        for (i++; i <= n; i++) ans += a[i];
        ans++;
        write(ans, 10);
    }
    return 0;
}