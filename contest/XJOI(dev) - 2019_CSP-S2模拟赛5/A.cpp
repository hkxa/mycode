/*************************************
 * user name:    Jmoo.
 * language:     C++.
 * upload place: XJOI (dev.xjoi.net).
*************************************/ 
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

double l, r, h, vv, x;
 
double solvet(double vx, double vy) {
    if (vx == 0 && x == 0) return sqrt(2 * h / 9.8);
    return sqrt(2 * vy * x / 9.8 / vx + 2 * h / 9.8);
}
 
double solve(double vx, double vy) {
    double t = solvet(vx, vy);
    if (9.8 * vx * vx * t * t - 2 * 9.8 * t * x * vx + 9.8 * x * x - 2 * h * vx * vx > 0) return 1e18;
    return sqrt(vx * vx + (vy - 9.8 * t) * (vy - 9.8 * t));
}
 
int main() 
{
#ifndef _WIN32
    freopen("a.in", "r", stdin);
    freopen("a.out", "w", stdout);
#endif
    l = read<int>();
    r = read<int>();
    h = read<int>();
    vv = read<int>();
    if (l * r <= 0) x = 0;
    else x = min(abs(l), abs(r));
    double ans = 1e18;
    ans = min(ans, solve(0, 0));
    ans = min(ans, solve(vv, 0));
    ans = min(ans, solve(0, vv));
    ans = min(ans, solve(M_SQRT1_2 * vv, M_SQRT1_2 * vv));
    if (ans == 1e18) puts("-1");
    else printf("%.2lf", ans);
}