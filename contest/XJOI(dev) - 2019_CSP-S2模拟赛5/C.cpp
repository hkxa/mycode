typedef long long int64;
int n, m;
int64 num[18 + 7];
bool chase[200005 + 7];
int64 lim[200005 + 7];
int chaser_cnt = 0;
list<int> ctOS[18 + 7];
list<int>::iterator it;
int cnt = 0;

void init_base(int x, int y)
{
    n = x;
    m = y;
}

void init(int n, int m)
{
    init_base(n, m);
}

int doit(int opttype, int input1, int input2)
{
    if (opttype == 1) {
        num[input1] += input2;
        for (it = ctOS[input1].begin(); it != ctOS[input1].end(); ) {
            if (!chase[*it]) it = ctOS[input1].erase(it);
            else {
                lim[*it] -= input2;
                if (lim[*it] <= 0) {
                    chase[*it] = 0;
                    it = ctOS[input1].erase(it);
                    cnt--;
                } else it++;
            }
        }
    } else {
        chase[++chaser_cnt] = 1;
        lim[chaser_cnt] = input2;
        cnt++;
        for (int i = 0; i < n; i++) {
            if (input1 & (1 << i)) {
                lim[chaser_cnt] -= num[i];
            }
        }
        if (lim[chaser_cnt] <= 0) {
            cnt--;
            chase[++chaser_cnt] = 0;
        } else {
            for (int i = 0; i < n; i++) {
                if (input1 & (1 << i)) {
                    ctOS[i].push_back(chaser_cnt);
                }
            }
        }
    }
    return cnt;
}