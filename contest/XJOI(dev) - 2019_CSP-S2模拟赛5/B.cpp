/*************************************
 * user name:    Jmoo.
 * language:     C++.
 * upload place: XJOI (dev.xjoi.net).
*************************************/ 
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}
#define int int64
int x[6], y[6];

int gcd(int m, int n)
{
    return n ? gcd(n, m % n) : m;
}

int dis(int i, int j)
{
    return (x[i] - x[j]) * (x[i] - x[j]) + (y[i] - y[j]) * (y[i] - y[j]);
}

signed main()
{
    // freopen("b.in", "r", stdin);
    // freopen("b.out", "w", stdout);
    bool cse1 = 1;
    for (int i = 0; i < 6; i++) {
        x[i] = read<int>();
        y[i] = read<int>();
        if (y[i]) cse1 = 0;
    }
    if (cse1) {
        int g1 = gcd(x[0] - x[1], x[0] - x[2]);
        int g2 = gcd(x[3] - x[4], x[3] - x[5]);
        if (g1 == 0 && g2 == 0) {
            if (x[0] == x[3]) puts("Yes"); 
            else puts("No");
        } else {
            if (gcd(x[0] - x[3], g2) % g1 == 0) puts("No");
            else puts("Yes");
        }
        return 0;
    } else {
        int k1, k2;
        for (int i = 0; i < 3; i++) {
            if (dis(i, (i + 1) % 3) == dis(i, (i + 2) % 3)) k1 = i;
        }
        for (int i = 3; i < 6; i++) {
            if (dis(i, (i + 1) % 3 + 3) == dis(i, (i + 2) % 3 + 3)) k2 = i;
        }
        if ((x[k1] - x[k2] % ((int)sqrt(dis(k1, (k1 + 1) % 3) + 0.1) * 2) == 0)&&
         (y[k1] - y[k2] % ((int)sqrt(dis(k1, (k1 + 1) % 3) + 0.1) * 2) == 0)) puts("Yes");
        else puts("No");
        return 0;
    }
    puts("No");
    return 0;
}