//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      QiZhiShu - 2020(夏)-SX5.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-28.
 * @language:     C++.
*************************************/ 
// #pragma GCC optimize(1)
// #pragma GCC optimize(2)
#pragma GCC optimize(3)
#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 100 + 7, P = 1e9 + 7;

int n, m, a[N];
int f[N][N][N];
int fac[N], ifac[N];

void update_sum(int &dest, const int src) { dest += src; if (dest >= P) dest -= P; }

int64 fpow(int64 a, int n) {
    int64 ret = 1;
    while (n) {
        if (n & 1) ret = ret * a % P;
        a = a * a % P;
        n >>= 1;
    }
    return ret;
}
#define inv(x) fpow(x, P - 2)

void init_factor() {
    fac[0] = ifac[0] = 1;
    for (int i = 1; i < N; i++) fac[i] = (int64)fac[i - 1] * i % P;
    ifac[N - 1] = inv(fac[N - 1]);
    for (int i = N - 1; i >= 1; i--) ifac[i - 1] = (int64)ifac[i] * i % P;
}

signed main() {
    init_factor();
    read >> n;
    for (int i = 0; i < n; i++) read >> a[i];
    f[0][0][0] = 1;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            for (int k = 0; k < n; k++) {
                // if (f[i][j][k]) printf("f[%d][%d][%d] = %d\n", i, j, k, f[i][j][k]);
                update_sum(f[i + 1][j][k], f[i][j][k]);
                for (int c = 0; c < min(a[i], n - 1 - k); c++)
                    update_sum(f[i + 1][j + 1][k + c], (int64)f[i][j][k] * ifac[c] % P);
            }
        }
    }
    write << n << ' ';
    for (int i = 2; i <= n; i++)
        write << ((int64)f[n][i][i - 2] * fac[i - 2]) % P << " \n"[i == n];
    return 0;
}