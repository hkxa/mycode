//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      QiZhiShu - 2020(夏)-SX5.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-28.
 * @language:     C++.
*************************************/ 
// #pragma GCC optimize(2)
#pragma GCC optimize(3)
#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *CharBuffer) {
            // char *p = CharBuffer;
            while (*CharBuffer) putchar(*(CharBuffer++));
            return *this;
        }
        Writer& operator << (const char CharBuffer) {
            putchar(CharBuffer);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

namespace against_cpp11 {
    const int N = 12 + 1, M = (1 << 24), P = 998244353;

    int n, h[N][N], a[N][N], f[M], count_bits[M], g[30];
    char CharBuffer[N];

    void ChkMin(int &x, int y) { if (y < x) x = y; }
    int x[N], y[N];

    void trans_and_dp(int c) {
        int tx = 0, ty = -1, cnt = 0;
        y[0] = -1;
        for (int k = 0; k < n * 2; k++)
            if (c & (1 << k)) ty++;
            else {
                if (y[cnt] < ty) {
                    cnt++;
                    x[cnt] = tx;
                    y[cnt] = ty;
                }
                tx++;
            }
        for (int i = 1; i <= cnt; i++)
            ChkMin(f[c ^ (3 << (x[i] + y[i]))], f[c] + a[x[i]][y[i]]);
        for (int i = 1; i < cnt; i++)
            for (int j = i + 1; j <= cnt; j++)
                if ((h[x[i]][y[i]] + h[x[j]][y[j]]) == 1)
                    ChkMin(f[c ^ (3 << (x[i] + y[i])) ^ (3 << (x[j] + y[j]))], f[c] + abs(a[x[i]][y[i]] - a[x[j]][y[j]]));
    }

    signed main() {
        read >> n;
        for (int i = 0; i < n; i++) {
            scanf("%s", CharBuffer);
            for (int j = 0; j < n; j++)
                h[i][j] = (CharBuffer[j] == 'B') - (CharBuffer[j] == '.');
        }
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                read >> a[i][j];
        memset(f, 0x3f, sizeof(f));
        f[(1 << n) - 1] = 0;
        for (int i = 1, Mask = (1 << (n * 2)); i < Mask; i++) {
            count_bits[i] = count_bits[i >> 1] + (i & 1);
            if (count_bits[i] == n) trans_and_dp(i);
        }
        write << f[((1 << n) - 1) << n] << '\n';
        return 0;
    }
}

signed main() { return against_cpp11::main(); }