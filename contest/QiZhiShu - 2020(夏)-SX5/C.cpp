//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      QiZhiShu - 2020(夏)-SX5.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-28.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 5e5 + 7;

int n, a[N], b[N], c[N];
vector<int> G[N];

int w[N];
int dep[N], dft;
int64 val[N];

void UpdMin(int &pos, const int &toUpd) {
    if (toUpd < pos) pos = toUpd;
}

int t[N];

void add(int u, int dif) {
    while (u <= 5e5) {
        t[u] += dif;
        u += (u & -u);
    }
}
int query(int u) {
    int ret = 0;
    while (u) {
        ret += t[u];
        u -= (u & -u);
    }
    return ret;
}

void pretreat_dfs1(int u, int fa) {
    // printf("dfs(%d, %d) :: w = %d\n", u, fa, w[u]);
    a[u] = query(w[u] - 1);
    c[u] = query(w[u]) - a[u];
    add(w[u], 1);
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i];
        if (v != fa) {
            dep[v] = dep[u] + 1;
            pretreat_dfs1(v, u);
        }
    }
    add(w[u], -1);
}


void pretreat_dfs2(int u, int fa) {
    add(w[u], 1);
    b[u] -= ++dft - query(w[u]);
    for (size_t i = 0; i < G[u].size(); i++) {
        int v = G[u][i];
        if (v != fa) pretreat_dfs2(v, u);
    }
    b[u] += dft - query(w[u]);
}

signed main() {
    read >> n;
    for (int i = 1; i <= n; i++) read >> w[i];
    for (int i = 1, u, v; i < n; i++) {
        read >> u >> v;
        G[u].push_back(v);
        G[v].push_back(u);
    }
    pretreat_dfs1(1, 0);
    pretreat_dfs2(1, 0);
    int64 sum_of_val = 0;
    for (int i = 1; i <= n; i++) {
        // printf("%d %d %d\n", a[i], b[i], c[i]);
        sum_of_val += a[i] + b[i];
        // printf("*%d = %d\n", i, a[i] + b[i]);
        val[i] = dep[i] - a[i] - b[i] - c[i];
    }
    sum_of_val /= 2;
    sort(val + 1, val + n + 1);
    // printf("sum_of_val = %d\n", sum_of_val);
    for (int i = 1; i <= n; i++)
        val[i] += val[i - 1];
    for (int i = n; i >= 0; i--)
        write << (int64)i * (i - 1) / 2 + val[i] + sum_of_val << '\n';
    return 0;
}