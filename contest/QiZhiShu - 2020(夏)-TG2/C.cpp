//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      QiZhiShu - 2020(夏)-TG2.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-20.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int debugP = 19260817, RealP = 998244353;
const int P = RealP, Unknown = 0;

int n;

namespace fpow {
    template <typename T>
    T fpow(T a, int64 n) {
        T ret;
        ret.setI();
        while (n) {
            if (n & 1) ret = ret * a;
            a = a * a;
            n >>= 1;
        }
        return ret;
    }
}

namespace solve6 {
    struct Matrix {
        int64 a[4][4];
        void reset() {
            for (int i = 0; i < 4; i++)
                for (int j = 0; j < 4; j++)
                    a[i][j] = 0;
        }
        void setI() {
            for (int i = 0; i < 4; i++)
                for (int j = 0; j < 4; j++)
                    a[i][j] = (i == j);
        }
        Matrix operator * (const Matrix &b) {
            Matrix ret;
            ret.reset();
            for (int i = 0; i < 4; i++)
                for (int j = 0; j < 4; j++)
                    for (int k = 0; k < 4; k++)
                        ret.a[i][j] = (ret.a[i][j] + a[i][k] * b.a[k][j]) % P;
            return ret;
        }
    };
    int64 run() {
        Matrix unit, ret;
        unit.a[0][0] = 1;
        unit.a[0][1] = 1;
        unit.a[0][2] = 0;
        unit.a[0][3] = 0;
        unit.a[1][0] = 0;
        unit.a[1][1] = 1;
        unit.a[1][2] = 2;
        unit.a[1][3] = 1;
        unit.a[2][0] = 0;
        unit.a[2][1] = 0;
        unit.a[2][2] = 1;
        unit.a[2][3] = 1;
        unit.a[3][0] = 0;
        unit.a[3][1] = 0;
        unit.a[3][2] = 0;
        unit.a[3][3] = 1;
        ret = fpow::fpow(unit, 5000000000000000LL);
        // ret = fpow::fpow(unit, 5);
        // for (int i = 0; i < 4; i++)
        //     for (int j = 0; j < 4; j++)
        //         printf("%lld%c", ret.a[i][j], " \n"[j == 3]);
        return (ret.a[0][1] + ret.a[0][2] + ret.a[0][3]) % P;
    }
}

namespace solve7 {
    struct Matrix {
        int64 a[5][5];
        void reset() {
            for (int i = 0; i < 5; i++)
                for (int j = 0; j < 5; j++)
                    a[i][j] = 0;
        }
        void setI() {
            for (int i = 0; i < 5; i++)
                for (int j = 0; j < 5; j++)
                    a[i][j] = (i == j);
        }
        Matrix operator * (const Matrix &b) {
            Matrix ret;
            ret.reset();
            for (int i = 0; i < 5; i++)
                for (int j = 0; j < 5; j++)
                    for (int k = 0; k < 5; k++)
                        ret.a[i][j] = (ret.a[i][j] + a[i][k] * b.a[k][j]) % P;
            return ret;
        }
    };
    int64 run() {
        Matrix unit, ret;
        unit.a[0][0] = 1;
        unit.a[0][1] = 1;
        unit.a[0][2] = 0;
        unit.a[0][3] = 0;
        unit.a[0][4] = 0;
        unit.a[1][0] = 0;
        unit.a[1][1] = 1;
        unit.a[1][2] = 3;
        unit.a[1][3] = 3;
        unit.a[1][4] = 1;
        unit.a[2][0] = 0;
        unit.a[2][1] = 0;
        unit.a[2][2] = 1;
        unit.a[2][3] = 2;
        unit.a[2][4] = 1;
        unit.a[3][0] = 0;
        unit.a[3][1] = 0;
        unit.a[3][2] = 0;
        unit.a[3][3] = 1;
        unit.a[3][4] = 1;
        unit.a[4][0] = 0;
        unit.a[4][1] = 0;
        unit.a[4][2] = 0;
        unit.a[4][3] = 0;
        unit.a[4][4] = 1;
        ret = fpow::fpow(unit, 5000000000000000LL);
        // ret = fpow::fpow(unit, 5);
        // for (int i = 0; i < 4; i++)
        //     for (int j = 0; j < 4; j++)
        //         printf("%lld%c", ret.a[i][j], " \n"[j == 3]);
        return (ret.a[0][1] + ret.a[0][2] + ret.a[0][3] + ret.a[0][4]) % P;
    }
}

namespace solve8 {
    struct Matrix {
        int64 a[6][6];
        void reset() {
            for (int i = 0; i < 6; i++)
                for (int j = 0; j < 6; j++)
                    a[i][j] = 0;
        }
        void setI() {
            for (int i = 0; i < 6; i++)
                for (int j = 0; j < 6; j++)
                    a[i][j] = (i == j);
        }
        Matrix operator * (const Matrix &b) {
            Matrix ret;
            ret.reset();
            for (int i = 0; i < 6; i++)
                for (int j = 0; j < 6; j++)
                    for (int k = 0; k < 6; k++)
                        ret.a[i][j] = (ret.a[i][j] + a[i][k] * b.a[k][j]) % P;
            return ret;
        }
    };
    int64 run() {
        Matrix unit, ret;
        unit.a[0][0] = 1;
        unit.a[0][1] = 1;
        unit.a[0][2] = 0;
        unit.a[0][3] = 0;
        unit.a[0][4] = 0;
        unit.a[0][5] = 0;
        unit.a[1][0] = 0;
        unit.a[1][1] = 1;
        unit.a[1][2] = 4;
        unit.a[1][3] = 6;
        unit.a[1][4] = 4;
        unit.a[1][5] = 1;
        unit.a[2][0] = 0;
        unit.a[2][1] = 0;
        unit.a[2][2] = 1;
        unit.a[2][3] = 3;
        unit.a[2][4] = 3;
        unit.a[2][5] = 1;
        unit.a[3][0] = 0;
        unit.a[3][1] = 0;
        unit.a[3][2] = 0;
        unit.a[3][3] = 1;
        unit.a[3][4] = 2;
        unit.a[3][5] = 1;
        unit.a[4][0] = 0;
        unit.a[4][1] = 0;
        unit.a[4][2] = 0;
        unit.a[4][3] = 0;
        unit.a[4][4] = 1;
        unit.a[4][5] = 1;
        unit.a[5][0] = 0;
        unit.a[5][1] = 0;
        unit.a[5][2] = 0;
        unit.a[5][3] = 0;
        unit.a[5][4] = 0;
        unit.a[5][5] = 1;
        ret = fpow::fpow(unit, 5000000000000000LL);
        // ret = fpow::fpow(unit, 5);
        // for (int i = 0; i < 4; i++)
        //     for (int j = 0; j < 4; j++)
        //         printf("%lld%c", ret.a[i][j], " \n"[j == 3]);
        return (ret.a[0][1] + ret.a[0][2] + ret.a[0][3] + ret.a[0][4] + ret.a[0][5]) % P;
    }
}

int main() {
    read >> n;
    switch (n) {
        case 1 :
            write << 447405405 << '\n';
            break;
        case 2 :
            write << Unknown << '\n';
            break;
        case 3 :
            write << Unknown << '\n';
            break;
        case 4 :
            write << 1000000000000000LL % P << '\n';
            break;
        case 5 :
            write << (2500000000000000LL % P) * (5000000000000001LL % P) % P << '\n';
            break;
        case 6 :
            write << solve6::run() << '\n';
            break;
        case 7 :
            write << solve7::run() << '\n';
            break;
        case 8 :
            write << solve8::run() << '\n';
            break;
        case 9 :
            write << Unknown << '\n';
            break;
        case 10 :
            write << Unknown << '\n';
            break;
    }
    return 0;
}

