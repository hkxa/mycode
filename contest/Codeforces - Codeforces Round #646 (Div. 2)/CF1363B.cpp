//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      Codeforces - Codeforces Round #646 (Div. 2).
 * @user_name:    hkxadpall.
 * @time:         2020-05-31.
 * @language:     C++.
 * @upload_place: Codeforces.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// #define int int64

const int N = 200007;
int n, l;
char s[N];
int cnt, c2, ans;

signed main() {
    n = read<int>();
    while (n--) {
        scanf("%s", s + 1);
        l = strlen(s + 1);
        cnt = c2 = 0;
        for (int i = 1; i <= l; i++) {
            cnt += s[i] & 1;
        }
        ans = min(cnt, l - cnt);
        for (int i = 1; i <= l; i++) {
            c2 += s[i] & 1;
            ans = min(ans, i - c2 + (cnt - c2));
        }
        c2 = 0;
        for (int i = 1; i <= l; i++) {
            c2 += s[i] & 1;
            ans = min(ans, c2 + (l - i) - (cnt - c2));
        }
        write(ans, 10);
    }
    return 0;
}