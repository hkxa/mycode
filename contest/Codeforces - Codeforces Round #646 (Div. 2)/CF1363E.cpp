//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      Codeforces - Codeforces Round #646 (Div. 2).
 * @user_name:    hkxadpall.
 * @time:         2020-05-31.
 * @language:     C++.
 * @upload_place: Codeforces.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// #define int int64
const int N = 200007;
int n;
int a[N], b[N], c[N];
int up[N], low[N];
vector<int> G[N];
int64 ans;

void dfs(int u, int fa) {
    if (fa)a[u] = min(a[u], a[fa]);
    if (b[u] != c[u]) {
        if (c[u] > b[u]) up[u]= 1;
        else low[u] = 1;
    }
    for (unsigned i = 0; i < G[u].size() ; i++) {
        int v = G[u][i];
        if (v == fa) continue;
        dfs(v, u);
        ;up[u] += up[v];
        low[u] += low[v];
    }
    // printf("up[%d] = %d, low[%d] = %d\n", u, up[u], u, low[u]);
    int k = min(up[u], low[u]);
    // printf("k = %d, a[%d] = %d\n", k, u, a[u]);
    ans += (int64) 2 * k * a[u];
    up[u] -= k;
    low[u] -= k;
}

signed main() {
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        a[i] = read<int>();
        b[i] = read<int>();
        c[i] = read<int>();
    }
    for (int i = 1, u, v; i < n; i++) {
        u = read<int>();
        v = read<int>();
        G[u].push_back(v);
        G[v].push_back(u);
    }
    dfs(1, 0);
    if (low[1] || up[1]) puts("-1");
    else write(ans, 10);
    return 0;
}