//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      id_name.
 * @user_id:      63720.
 * @user_name:    brealid.
 * @time:         2020-mm-dd.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int N = 1000000 + 7, P = 1000000000 + 7, MaxAi = 262144 + 7;

int n, a[N];
int fpow(int x, int p) {
    int rwt = 1;
    while (p) {
        if (p & 1) rwt = (int64)rwt * x % P;
        x = (int64)x * x % P;
        p >>= 1;
    }
    return rwt;
}

namespace Sub1 {
    int zero = 0;
    bool check() {
        for (int i = 1; i <= n; i++) {
            if (a[i] > 1) return false;
            if (!a[i]) zero++;
        }
        return true;
    }
    void solve() {
        write((int64)fpow(2, zero) * (n - zero + 1) % P, 10);
    }
}

namespace Sub4 {
    bool check() {
        return n <= 20;
    }
    bool m[MaxAi];
    int phi[MaxAi], p[MaxAi], nump;
    void getPhi() {
        phi[1] = 1;
        for (int i = 2; i <= MaxAi; i++) {
            if (!m[i]) {
                p[++nump] = i;
                phi[i] = i - 1;
            }
            for (int j = 1; j <= nump && p[j] * i <= MaxAi; j++) {
                m[p[j] * i] = 1;
                if (i % p[j] == 0) {
                    phi[p[j] * i] = phi[i] * p[j];
                    break;
                } else phi[p[j] * i] = phi[i] * (p[j] - 1);
            }
        }
    }
    map<int, int64> M[N];
    int64 dfs(int u, int A) {
        if (u >= n + 1) {
            // printf("A = %d => phi[A + 1] = %d\n", A, phi[A + 1]);
            return phi[A + 1];
        }
        if (M[u].find(A) != M[u].end()) return M[u][A];
        int64 Res = dfs(u + 1, A);
        if (!(A & a[u])) Res = (Res + dfs(u + 1, A | a[u])) % P;
        return M[u][A] = Res;
    }
    void solve() {
        getPhi();
        write(dfs(1, 0), 10);
    }
}

namespace Sub2 {
    int64 cnt[1024 + 7], k[1024 + 7], ans = 1;
    bool check() {
        for (int i = 1; i <= n; i++) {
            if (a[i] > 1024) return false;
            cnt[a[i]]++;
        }
        return true;
    }
    void solve() {
        Sub4::getPhi();
        for (int i = 1; i <= 1000; i++) {
            k[i] = cnt[i];
            // printf("k[%d] = cnt[%d] = %lld\n", i, i, cnt[i]);
        }
        for (int i = 1; i <= 1023; i++) {
            // k[i] = cnt[i];
            for (int j = i + 1; j <= 1023; j++) {
                if (i & j) continue;
                k[i | j] = (k[i | j] + k[i] * cnt[j]) % P;
            }
        }
        // for (int i = 0; i < 32; i++) printf("k[%d] = %lld\n", i, k[i]);
        for (int i = 1; i <= 1023; i++) ans = (ans + k[i] * Sub4::phi[i + 1]) % P;
        // printf("test Unknown\n");
        // printf("cnt[0] = %lld\n", cnt[0]);
        write(ans * fpow(2, cnt[0]) % P, 10);
    }
}

int main()
{
    // Sub4::getPhi();
    // for (int i = 1; i <= 100; i++) printf("phi[%d] = %d\n", i, Sub4::phi[i]);
    freopen("sequence.in", "r", stdin);
    freopen("sequence.out", "w", stdout);
    n = read<int>();
    for (int i = 1; i <= n; i++) a[i] = read<int>();
    // Sub2::check();
    // Sub2::solve();
    // return 0;
    if (Sub1::check()) Sub1::solve();
    else if (Sub4::check()) Sub4::solve();
    else if (Sub2::check()) Sub2::solve();
    else Sub4::solve();
    return 0;
}

/*
20
0 0 1 2 3 5 5 6 8 9 11 13 13 13 15 16 18 21 24 30
answer = 4088

20
0 1 4 9 16 25 36 49 64 81 100 121 144
*/