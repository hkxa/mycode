//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      id_name.
 * @user_id:      63720.
 * @user_name:    brealid.
 * @time:         2020-mm-dd.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const uint32 N = 102;

#define getMat(m, j, k) (m[j][!!((k) & 64)] & (1LL << ((k) & 63)))
#define setMat(m, j, k) (m[j][!!((k) & 64)] |= (1LL << ((k) & 63)))

uint32 n, m, q;
uint32 f[N];
uint32 ans;

struct MT {
    uint64 m[N][2];
    MT() { memset(m, 0, sizeof(m)); }
    MT operator* (const MT &b) {
        // printf("------------------------------------------------------------------\n");
        MT ret;
        for (uint32 i = 1; i <= n; i++) {
            for (uint32 j = 1; j <= n; j++) {
                if (getMat(m, i, j)) {
                    // printf("for CHARGE (%d -> %d) ret from %d to %d\n", i, j, ret.m[i][0] >> 1, (ret.m[i][0] ^ b.m[j][0]) >> 1);
                    ret.m[i][0] ^= b.m[j][0];
                    ret.m[i][1] ^= b.m[j][1];
                } 
                // if (getMat(m, i, j)) {
                //     ret.m[i][0] ^= ~b.m[j][0];
                //     ret.m[i][1] ^= ~b.m[j][1];
                // } else {
                //     ret.m[i][0] ^= b.m[j][0];
                //     ret.m[i][1] ^= b.m[j][1];
                // } 
            }
        }
        // printf("-----Times element 1-----\n");
        // for (uint32 i = 1; i <= n; i++) {
        //     for (uint32 j = 1; j <= n; j++) {
        //         write(!!getMat(m, i, j), j == n ? 10 : 32);
        //     }
        // }
        // printf("-----Times element 1-----\n");
        // for (uint32 i = 1; i <= n; i++) {
        //     for (uint32 j = 1; j <= n; j++) {
        //         write(!!getMat(b.m, i, j), j == n ? 10 : 32);
        //     }
        // }
        // printf("-----Times equals to-----\n");
        // for (uint32 i = 1; i <= n; i++) {
        //     for (uint32 j = 1; j <= n; j++) {
        //         write(!!getMat(ret.m, i, j), j == n ? 10 : 32);
        //     }
        // }
        return ret;
    }
} Z, R;

MT fpow(MT x, uint32 p) {
    MT ret;
    for (uint32 i = 1; i <= n; i++) setMat(ret.m, i, i);
    while (p) {
        if (p & 1) ret = ret * x;
        x = x * x;
        p >>= 1;
    }
    // printf("------------------------------------------------------------------\n");
    // printf("-----fPow equals to-----\n");
    // for (uint32 i = 1; i <= n; i++) {
    //     for (uint32 j = 1; j <= n; j++) {
    //         write(!!getMat(ret.m, i, j), j == n ? 10 : 32);
    //     }
    // }
    return ret;
}

int main()
{
    freopen("magic.in", "r", stdin);
    freopen("magic.out", "w", stdout);
    cin >> n >> m >> q;
    for (uint32 i = 1; i <= n; i++) f[i] = read<uint32>();
    for (uint32 i = 1, u, v; i <= m; i++) {
        u = read<uint32>();
        v = read<uint32>();
        setMat(Z.m, u, v);
        setMat(Z.m, v, u);
    }
    for (uint32 i = 1, ai; i <= q; i++) {
        ai = read<uint32>();
        // fprintf(stderr, "Read %u\n", ai);
        R = fpow(Z, ai);
        ans = 0;
        for (uint32 j = 1; j <= n; j++) {
            if (getMat(R.m, 1, j)) ans ^= f[j];
        }
        write(ans, 10);
    }
    return 0;
}