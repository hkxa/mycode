//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      QiZhiShu - 2020(夏)-SX2.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-25.
 * @language:     C++.
*************************************/ 
// #pragma GCC optimize(2)
// #pragma GCC optimize(3)

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int P = 1e9 + 7;

int64 n;
int64 f[70][2][70][2];

int64 dfs(int u, bool bit, int cnt, bool isBound) {
    // printf("enter_dfs(%d, %d, %d, %d)\n", u, bit, cnt, isBound);
    if (cnt > u + 1 || cnt < 0) return 0;
    if (u == 0) {
        if (isBound && !(n & 1)) {
            if (bit) return cnt == 0;
            else return cnt == 1;
        } else return cnt <= 1;
    }
    int64 &ans = f[u][bit][cnt][isBound];
    if (ans) return ans;
    if (n & (1ll << u)) {
        ans = (dfs(u - 1, 1, cnt - bit, isBound) + dfs(u - 1, 0, cnt - !bit, 0)) % P;
    } else if (isBound) {
        ans = dfs(u - 1, 0, cnt - !bit, 1);
    } else {
        ans = (dfs(u - 1, 1, cnt - bit, 0) + dfs(u - 1, 0, cnt - !bit, 0)) % P;
    }
    // printf("dfs(%d, %d, %d, %d) = %lld\n", u, bit, cnt, isBound, ans);
    return ans;
}

signed solve() {
    read >> n;
    if (n == 1) {
        puts("0");
        return 0;
    }
    for (int i = 62; i >= 0; i--) {
        if (n & (1ll << i)) {
            for (int a = 0; a < 64; a++)
                for (int b = 0; b < 2; b++)
                    for (int c = 0; c < 70; c++)
                        f[a][b][c][1] = 0;
            int64 ans = 0;
            for (int j = 0; j <= i; j++)
                ans = (ans + (dfs(i - 1, 1, j, 1) * abs(i - 2 * j))) % P;
            for (int g = i - 1; g >= 1; g--)
                for (int j = 0; j <= g; j++)
                    ans = (ans + (dfs(g - 1, 1, j, 0) * abs(g - 2 * j))) % P;
            write << ans << '\n';
            break;
        }
    }
    return 0;
}

signed main() {
    int T;
    read >> T;
    while (T--) {
        solve();
    }
    return 0;
}