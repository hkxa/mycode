//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      QiZhiShu - 2020(夏)-SX2.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-25.
 * @language:     C++.
*************************************/ 
#pragma GCC optimize(2)
#pragma GCC optimize(3)
#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 4000000 + 7;

int n, m, T;
int a[N], d1[N], d2[N];
struct Operation {
    int x1, y1, x2, y2, k;
} op[N];
bool subtask2 = 1;

#define pos(x, y) ((x) * m + (y))

signed main() {
    read >> n >> m >> T;
    for (int i = 1; i <= n; i++) 
        for (int j = 1; j <= m; j++)
            read >> a[pos(i, j)];
    for (int i = 1; i <= T; i++)
        read >> op[i].x1 >> op[i].y1 >> op[i].x2 >> op[i].y2 >> op[i].k;
    for (int i = 0; i < 20; i++) {
        for (int x = 0; x <= n + 1; x++)
            for (int y = 0; y <= m + 1; y++)
                d1[pos(x, y)] = d2[pos(x, y)] = 0;
        for (int j = 1; j <= T; j++)
            if (op[j].k & (1 << i)) {
                d1[pos(op[j].x1, op[j].y1)]++;
                d1[pos(op[j].x1, op[j].y2 + 1)]--;
                d1[pos(op[j].x2 + 1, op[j].y1)]--;
                d1[pos(op[j].x2 + 1, op[j].y2 + 1)]++;
            } else {
                d2[pos(op[j].x1, op[j].y1)]++;
                d2[pos(op[j].x1, op[j].y2 + 1)]--;
                d2[pos(op[j].x2 + 1, op[j].y1)]--;
                d2[pos(op[j].x2 + 1, op[j].y2 + 1)]++;
            }
        for (int x = 1; x <= n; x++)
            for (int y = 1; y <= m; y++) {
                d1[pos(x, y)] += d1[pos(x - 1, y)] + d1[pos(x, y - 1)] - d1[pos(x - 1, y - 1)];
                d2[pos(x, y)] += d2[pos(x - 1, y)] + d2[pos(x, y - 1)] - d2[pos(x - 1, y - 1)];
                if (a[pos(x, y)] & (1 << i)) {
                    if (d2[pos(x, y)]) a[pos(x, y)] = 0;
                } else {
                    if (d1[pos(x, y)]) a[pos(x, y)] = 0;
                }
            }
    }
    int dead = 0;
    for (int x = 1; x <= n; x++)
        for (int y = 1; y <= m; y++)
            if (!a[pos(x, y)]) dead++;
    write << dead << '\n';
    return 0;
}