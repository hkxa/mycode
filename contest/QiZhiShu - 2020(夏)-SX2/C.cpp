//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      QiZhiShu - 2020(夏)-SX2.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-25.
 * @language:     C++.
*************************************/ 
// #pragma GCC optimize(2)
// #pragma GCC optimize(3)

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 1e5 + 7, P = 1e9 + 7;

int fpow(int a, int n) {
    int ret = 1;
    while (n) {
        if (n & 1) ret = (int64)ret * a % P;
        a = (int64)a * a % P;
        n >>= 1;
    }
    return ret;
}

int n, m;
vector<int> G[N];
int val[N], fa[N];
int opt, x, c;

struct STATUS {
    int s[12];
    STATUS() {
        memset(s, 0, sizeof(s));
    }
    STATUS operator + (const STATUS &b) const {
        STATUS Result = *this;
        for (int i = 10; i >= 1; i--)
            for (int j = 1; i + j <= 10; j++) 
                Result.s[i + j] = (Result.s[i + j] + (int64)Result.s[i] * b.s[j]) % P;
        return Result;
    }
    STATUS operator - (const STATUS &b) const {
        STATUS Result = *this;
        for (int i = 1; i <= 10; i++)
            for (int j = 1; i + j <= 10; j++) 
                Result.s[i + j] = (Result.s[i + j] - (int64)Result.s[i] * b.s[j]) % P;
        for (int i = 0; i <= 10; i++)
            Result.s[i] = (Result.s[i] + P) % P;
        return Result;
    }
    STATUS& operator += (const STATUS &b) {
        return *this = *this + b;
    }
    STATUS& operator -= (const STATUS &b) {
        return *this = *this - b;
    }
} f[N];

void IncFaDpVal(int u, int floor) {
    if (u == 1 || !floor) return;
    f[fa[u]] += f[u];
    IncFaDpVal(fa[u], floor - 1);
}

void DecFaDpVal(int u, int floor) {
    if (u == 1 || !floor) return;
    DecFaDpVal(fa[u], floor - 1);
    f[fa[u]] -= f[u];
}

STATUS query(int u, int floor)
{
    if (u == 1 || !floor) return f[u];
    return f[u] + (query(fa[u], floor - 1) - f[u]);
}

signed main() {
    read >> n >> m;
    for (int i = 1; i <= n; i++) {
        read >> val[i];
        f[i].s[1] = val[i];
    } 
    for (int i = 2; i <= n; i++) read >> fa[i];
    for (int i = n; i >= 2; i--) f[fa[i]] += f[i];
    for (int i = 1; i <= m; i++) {
        read >> opt >> x >> c;
        if (opt == 0) {
            DecFaDpVal(x, 10);
            int CorrectValue = (int64)c * fpow(val[x], P - 2) % P;
            for (int i = 1; i <= 10; i++)
                f[x].s[i] = (int64)f[x].s[i] * CorrectValue % P;
            val[x] = c;
            IncFaDpVal(x, 10);
        } else if (opt == 1) {
            DecFaDpVal(x, 10);
            IncFaDpVal(fa[x], 9);
            fa[x] = c;
            DecFaDpVal(fa[x], 9);
            IncFaDpVal(x, 10);
        } else {
            write << query(x, c).s[c] << '\n';
        }
    }
    return 0;
}