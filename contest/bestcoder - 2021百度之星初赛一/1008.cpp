/*************************************
 * @contest:      bestcoder - 2021百度之星初赛一.
 * @author:       hkxadpall (brealid).
 * @time:         2021-07-31.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

/*
本题有多组测试数据。

第一行，一个正整数 TT (1 \le T \le 3000)(1≤T≤3000) 表示数据组数。

对于每组数据，第一行一个整数 nn (3 \le n \le 50)(3≤n≤50)。

之后一行 nn 个整数表示每个人的身份，第 ii 个整数为 11 则表示 ii 是狼人，为 00 则表示 ii 是猎人。

之后 nn 行，一行 nn 个数，从这里开始的第 ii 行表示编号 ii 的人想杀死的人的排名表，保证是一个 11 到 nn 的排列，如果 ii 是猎人，则这个是其死亡时想打死的人的排名表，如果 ii 是狼人，则这个排名表中第一个人是其第一天晚上会杀死的人。
*/

const int N = 54;

int T, n;
int fighter, wolf;
bool die[N];
int wanna[N][N];

signed main() {
    for (kin >> T; T--;) {
        memset(die, 0, sizeof(die));
        kin >> n;
        for (int i = 1; i <= n; ++i)
            if (kin.get<int>()) wolf = fighter = i;
        for (int i = 1; i <= n; ++i)
            for (int j = 1; j <= n; ++j)
                kin >> wanna[i][j];
        for (int turn = 1; turn <= n; ++turn) {
            int death_id = 1;
            while (die[wanna[fighter][death_id]]) ++death_id;
            fighter = wanna[fighter][death_id];
            if (fighter == wolf) {
                kout << "lieren\n";
                break;
            }
            die[fighter] = true;
            if (turn >= n - 2) {
                kout << "langren\n";
                break;
            }
        }
        // end:;
    }
    return 0;
}