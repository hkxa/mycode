/*************************************
 * problem:      【LGR-064】洛谷11月月赛 II & CSG Round 2 D.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-11-09.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, k;
int64 a[100006];
int64 sum[100006];
int64 ps[2][100006];
int f[101][101][101];

int PianFenFunction1()
{
    for (int i = 1, t = 0; i <= n; i++) {
        t = (t + a[i]) % 998244353;
        write(t, 32);
    }
    return 0;
}

int PianFenFunction2()
{
    int64 r = 0, s = 0, t = 0;
    for (int j = 1; j <= n; j++) {
        r = (r + a[j]) % 998244353;
        t = (r * j - s) % 998244353;
        s = (s + r) % 998244353;
        sum[j] = (sum[j - 1] + t + 998244353) % 998244353;
        write(sum[j], 32);
    }
    
    // putchar(10);
    return 0;
}


int PianFenFunction3()
{
    int64 r = 0;
    for (int j = 1; j <= n; j++) {
        a[j] = (a[j] + a[j - 1]) % 998244353;
    }
    for (int j = 1; j <= n; j++) {
        // t = (r * j - s) % 998244353;
        // s = (s + r) % 998244353;
        ps[1][j] = (ps[1][j - 1] - r + a[j] * j % 998244353 + 998244353) % 998244353;
        r = (r + a[j]) % 998244353;
        // write(ps[1][j], 32);
    }
    // putchar(10);
    r = 0;
    for (int j = 1; j <= n; j++) {
        ps[0][j] = (ps[0][j - 1] - r + ps[1][j] * j + 998244353) % 998244353;
        r = (r + ps[1][j]) % 998244353;
        write(ps[0][j], 32);
    }
    return 0;
}


int PianFenFunction4()
{
    int64 r = 0;
    for (int j = 1; j <= n; j++) {
        ps[1][j] = (a[j] + ps[1][j - 1]) % 998244353;
    }
    for (int i = 2; i <= k; i++) {
        r = 0;
        for (int j = 1; j <= n; j++) {
            ps[i & 1][j] = (ps[i & 1][j - 1] - r + ps[!(i & 1)][j] * j + 998244353) % 998244353;
            r = (r + ps[!(i & 1)][j]) % 998244353;
        }
    }
    for (int j = 1; j <= n; j++) {
        write(ps[k & 1][j], 32);
    }
    return 0;
}

int main()
{
    n = read<int>();
    k = read<int>();
    for (int i = 1; i <= n; i++) {
        a[i] = read<int>();
    }
    if (k == 1) return PianFenFunction1();
    if (k == 2) return PianFenFunction2();
    if (k == 3) return PianFenFunction3();
    if (k <= 1000) return PianFenFunction4();
    for (int l = 1; l <= n; l++) {
        for (int r = l; r <= n; r++) {
            for (int x = l; x <= r; x++) {
                f[1][l][r] = (f[1][l][r] + a[x]) % 998244353;
            }
        }
    }
    for (int i = 2; i <= k; i++) {
        for (int l = 1; l <= n; l++) {
            for (int r = l; r <= n; r++) {
                for (int x = l; x <= r; x++) {
                    for (int y = x; y <= r; y++) {
                        f[i][l][r] = (f[i][l][r] + f[i - 1][x][y]) % 998244353;
                    }
                }
            }
        }
    }
    for (int r = 1; r <= n; r++) {
        write(f[k][1][r], 32);
    }
    return 0;
}