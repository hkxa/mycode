//-std=c++11
/*************************************
 * problem:      【LGR-064】洛谷11月月赛 II & CSG Round 2 C.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-11-09.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, m;
int s[1000007];
int op, t, cnt = 0;
int _a[33333 * 2 + 9], base = 33333 + 3;
#define a(x) _a[base + (x)]
#define push_front(x) (_a[base--] = (x))
#define push_back(x) (_a[base + i] = (x))

bool _isok[33333 * 2 + 9]; int isok_base = 33333 + 3;
#define v(x) _isok[isok_base + (x)]
#define pf(x) (_isok[isok_base--] = (x))
#define pb(x) (_isok[isok_base + i] = (x))
int tryGetScore()
{
    for (int i = 1; i <= m; i++) {
        op = read<int>();
        t = read<int>();
        if (op == 1) {
            push_front(t);
            cnt++;
            pf(1);
            for (int j = 1; j <= m; j++) {
                if (s[j] == a(j)) {
                    cnt--;
                    v(1) = 0;
                    break;
                }
            }
        } else {
            push_back(t);
            cnt++;
            pb(1);
            // if (i == 9) fprintf(stderr, "%d. v(9) = %d\n", i, v(9));
            // if (i == 9) fprintf(stderr, "t = %d, s[1] = %d\n", t, s[1]);
            for (int j = 1; j <= i; j++) {
                // if (i == 9 && j == i) fprintf(stderr, "entered %d %d\n", v(j), s[i - j + 1] == t);
                if (v(j) && s[i - j + 1] == t) {
                    // if (i == 9) fprintf(stderr, "cnt-- in pos %d.\n", j);
                    cnt--;
                    v(j) = 0;
                    // break;
                }
            }
        }
        // if (i >= 7 && i <= 10) fprintf(stderr, "%d. v(1) = %d\n", i, v(1));
        write(cnt, 10);
    }
    return 0;
}

int main()
{
    n = read<int>();
    m = read<int>();
    for (int i = 1; i <= n; i++) s[i] = read<int>();
#ifdef Tes
    return tryGetScore();
#endif
    if (m > 333) return tryGetScore();
    for (int i = 1; i <= m; i++) {
        op = read<int>();
        t = read<int>();
        if (op == 1) {
            push_front(t);
            cnt++;
            for (int j = 1; j <= m; j++) {
                if (s[j] == a(j)) {
                    cnt--;
                    break;
                }
            }
        } else {
            push_back(t);
            cnt = i;
            for (int j = 1; j <= i; j++) {
                // fprintf(stderr, "%c", a(j) | 48);
                for (int k = 1; k <= j; k++) {
                    if (s[k] == a(i - j + k)) {
                        cnt--;
                        break;
                    }
                }
            }
            // fprintf(stderr, "\n");
        }
        write(cnt, 10);
    }
    return 0;
}