//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      牛客练习赛65.
 * @user_name:    HZ-ZY.
 * @time:         2020-06-12.
 * @language:     C++.
*************************************/ 
#pragma GCC optimize(2)
#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 1000000007;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

#define int int64

const int N = 200007;

int n;
bool notp[N];
int prime[N], siz;

pair<double, int> f[N], g[N];

void getPrime() {
    for (int i = 2; i <= n; i++) {
        if (!notp[i])
            prime[++siz] = i;
        for (int j = i + i; j <= n; j += i)
            notp[j] = true;
    }
}

signed main() {
    n = read<int>();
    getPrime();
    f[0] = make_pair(0, 1);
    for (int i = 1; i <= min(siz, 190LL); i++) {
        for (int T = prime[i], gain = 2; T <= n; gain++, T = (T + 1) * prime[i]) 
            for (int j = n; j >= T; j--)
                g[j] = max(g[j], make_pair(f[j - T].first + log(gain), f[j - T].second * gain % P));
        for (int j = n; j >= prime[i]; j--)
            f[j] = g[j];
    }
    pair<double, int> ans;
    for (int i = 0; i <= n; i++) ans = max(ans, f[i]);
    write(ans.second, 10);
    return 0;
}