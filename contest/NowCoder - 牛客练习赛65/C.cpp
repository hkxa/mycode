//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      牛客练习赛65.
 * @user_name:    HZ-ZY.
 * @time:         2020-06-12.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 988244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? w - P : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

#define int int64
const int N = 1e5 + 7;

int n, q;
int a[N], b[N], x, y;
set<long long> oc;

int gcd(int a, int b) {
    return a ? b : gcd(b % a, a);
}

int64 fold(int a, int b) {
    if (!a) return -1;
    if (!b) return -2;
    int g = gcd(a, b);
    a /= g; b /= g;
    return a * 1000000007LL + b;
}

bool NoAnswer(int x, int y) {
    if (n == 1 || oc.size() == 1) return true;
    return false;
}

bool SpecialSituation(int x, int y) {
    if (n != 2) return false;
    if (a[1] + a[2] == x && b[1] + b[2] == y) return true;
    // if (a[1] + x == a[2] && b[1] + y == b[2]) return true;
    // if (a[2] + x == a[1] && b[2] + y == b[1]) return true;
    return false;
}

signed main() {
    n = read<int>();
    q = read<int>();
    for (int i = 1; i <= n; i++) {
        a[i] = read<int>();
        b[i] = read<int>();
        if (!a[i] && !b[i]) n--, i--;
        else oc.insert(fold(a[i], b[i]));
    }
    for (int i = 1; i <= q; i++) {
        x = read<int>();
        y = read<int>();
        int64 u = fold(x, y);
        if (!x && !y) puts("0");
        else if (oc.find(u) != oc.end()) puts("1");
        else if (NoAnswer(x, y)) puts("-1");
        else if (SpecialSituation(x, y)) puts("3");
        else puts("2");
    }
    return 0;
}