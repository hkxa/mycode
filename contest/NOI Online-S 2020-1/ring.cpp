/*************************************
 * @problem:      ring.
 * @user_id:      ZJ-00625.
 * @time:         2020-03-07.
 * @language:     C++.
 * @upload_place: CCF - NOI Online.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, m, k;
int a[200007];
int64 ans = 0, now = 0;

int main()
{
    freopen("ring.in", "r", stdin);
    freopen("ring.out", "w", stdout);
    n = read<int>();
    m = read<int>();
    for (int i = 0; i < n; i++) a[i] = read<int>();
    while (m--) {
        k = read<int>();
        if (k == 0) {
            now = 0;
            for (int i = 0; i < n; i++) now += (int64)a[i] * a[i];
            write(now, 10);
            continue;
        } 
        // if (k == 1) {
        //     sort(a, a + n);
        //     for (int i = 0; i < n; i++) now += (int64)a[i] * a[(i + 1) % n];
        //     write(now, 10);
        //     continue;
        // }
        sort(a, a + n);
        ans = 0;
        do {
            now = 0;
            for (int i = 0; i < n; i++) now += (int64)a[i] * a[(i + k) % n];
            if (now > ans) ans = now;
        } while (next_permutation(a, a + n));
        write(ans, 10);
    }
    return 0;
}