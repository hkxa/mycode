/*************************************
 * @problem:      sequence.
 * @user_id:      ZJ-00625.
 * @time:         2020-03-07.
 * @language:     C++.
 * @upload_place: CCF - NOI Online.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int T, n, m;
int a[100007];
bool chk[100007];
struct Operation {
    int t, u, v;
    bool operator < (const Operation &other) const { return u > other.u; }
} op[100007];
priority_queue<Operation> q;
bool Could;

bool check()
{
    unsigned now = 0;
    for (int i = 1; i <= n; i++) {
        now += (a[i] & 1);
    }
    // printf("now = %d.\n", now);
    if (now & 1) return true;
    memset(chk, 0, sizeof(chk));
    for (int i = 1; i <= m; i++) {
        chk[op[i].u] = true;
        chk[op[i].v] = true;
    }
    for (int i = 1; i <= n; i++) {
        if (a[i] && !chk[i]) return true;
    }
    return false;
}

int main()
{
    freopen("sequence.in", "r", stdin);
    freopen("sequence.out", "w", stdout);
    T = read<int>();
    while (T--) {
        n = read<int>();
        m = read<int>();
        for (int i = 1; i <= n; i++) a[i] = read<int>();
        for (int i = 1; i <= n; i++) a[i] -= read<int>();
        for (int i = 1; i <= m; i++) {
            op[i].t = read<int>();
            op[i].u = read<int>();
            op[i].v = read<int>();
            if (op[i].t == 2) op[i].t = -1;
            if (op[i].u > op[i].v) swap(op[i].u, op[i].v);
            q.push(op[i]);
        }
        if (check()) {
            puts("NO");
            continue;
        }
        Operation f, x = q.top();
        while (!q.empty()) {
            f = x; q.pop(); if (!q.empty()) x = q.top();
            if (f.u == f.v) {
                a[f.u] = (a[f.u] & 1); 
            } else {
                a[f.v] -= a[f.u] * f.t;
                a[f.u] = 0;
            }
            if (!q.empty() && x.u == f.u && (x.v != f.v || x.t != f.t)) 
                q.push((Operation){x.t == f.t ? -1 : 1, min(x.v, f.v), max(x.v, f.v)});
        }
        Could = true;
        for (int i = 1; i <= n; i++) 
            if (a[i]) {
                Could = false;
                break;
            }
        puts(Could ? "YES" : "NO");
    }
    return 0;
}