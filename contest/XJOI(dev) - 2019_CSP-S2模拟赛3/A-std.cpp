//废话不多说，直接上爆搜
//爆搜顺子，每次爆搜顺子长度，爆搜完统计三个牌和四个牌和单牌的数量
//如果三个牌 + 四个牌 * 2 - 单牌 >= 1 表示可行，标记flag 返回
#include <cstdio>
#include <iostream>
#include <string>
using namespace std;
const int M = 15;
int card[M + 5];
int n;
string s;
bool flag = false;
void outcard(){
    if(flag) return;
    int sincard = 0;
    int thrcard = 0;
    int foucard = 0;
    /*for(int i = 2;i <= 14;i ++) printf("%d ",card[i]);
    printf("\n");*/
    for(int i = 2;i <= 14;i ++){
        if(card[i] == 1) sincard ++;
        if(card[i] == 3) thrcard ++;
        if(card[i] == 4) foucard ++;
    }
    if(thrcard + foucard * 2 - sincard >= -1){
        flag = true;
        return;
    }
    for(int i = 3;i <= 14;i ++){
        int l = 0,now = i;
        while(card[now]){
            l ++;
            card[now] --;
            now ++;
            if(l >= 5){
                outcard();
            }
        }
        for(int j = i;j <= i + l - 1;j ++)
            card[j] ++;
    }
}
bool check(){
    outcard();
    if(flag) return true;
    return false;
}
int main(){
    scanf("%d",&n);
    for(int i = 1;i <= n;i ++){
        cin >> s;
        if(s[0] == '1' && s[1] == '0'){
            card[10] ++;
            continue;
        }
        if(s[0] >= '0' && s[0] <= '9'){
            card[s[0] - '0'] ++;
        }
        if(s[0] == 'A') card[14] ++;
        if(s[0] == 'J') card[11] ++;
        if(s[0] == 'Q') card[12] ++;
        if(s[0] == 'K') card[13] ++;
    }
    if(check())
        printf("Yes");
    else
        printf("No");
    return 0;
}