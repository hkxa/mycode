/*************************************
 * problem:      B.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-11-10.
 * language:     C++.
 * upload place: http://dev.xjoi.net/.
*************************************/ 
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    // Int flag = 1;
    c = getchar();
    while ((!isdigit(c))) c = getchar();
    // while ((!isdigit(c)) && c != '-') c = getchar();
    // if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (((init << 2) + init) << 1) + (c & 15);
	return init;
	// return init * flag;
}

#define ouf_size______ 1000007
char _zy_out_buf[ouf_size______], *_zy_out_pt = _zy_out_buf, *_zy_out_pe = _zy_out_buf + ouf_size______;
void _zy_putchar(char c)
{
    *_zy_out_pt++ = c;
    if (_zy_out_pt == _zy_out_pe) fwrite(_zy_out_buf, 1, ouf_size______, stdout), _zy_out_pt = _zy_out_buf;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) _zy_putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    _zy_putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    _zy_putchar(nextch);
}

int l[5000007], r[5000007], v[5000007], cnt = 0;

void insert(int u, int x)
{
    if (!cnt) {
        v[++cnt] = x;
        return;
    }
    if (x < v[u]) {
        if (l[u]) insert(l[u], x);
        else {
            v[++cnt] = x;
            l[u] = cnt;
        }
    } else {
        if (r[u]) insert(r[u], x);
        else {
            v[++cnt] = x;
            r[u] = cnt;
        }
    }
}

int pre(int x)
{
    int u = 1, ans = 0;
    while (true) {
        if (v[u] >= x) {
            if (l[u]) u = l[u];
            else return ans;
        } else {
            ans = max(v[u], ans);
            if (r[u]) u = r[u];
            else return ans;
        }
    }
}

int nxt(int x)
{
    int u = 1, ans = 100000001;
    while (true) {
        if (v[u] <= x) {
            if (r[u]) u = r[u];
            else return ans;
        } else {
            ans = min(v[u], ans);
            if (l[u]) u = l[u];
            else return ans;
        }
    }
}

int main()
{
    int q = read<int>(), t, x;
    while (q--) {
        t = read<int>();
        x = read<int>();
        if (t == 1) {
            insert(1, x);
        } else if (t == 2) {
            write(pre(x), 10);
        } else {
            write(nxt(x), 10);
        }
    }
    return fwrite(_zy_out_buf, 1, _zy_out_pt - _zy_out_buf, stdout), 0;
}