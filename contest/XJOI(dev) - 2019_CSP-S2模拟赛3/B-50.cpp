/*************************************
 * problem:      B.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-11-10.
 * language:     C++.
 * upload place: http://dev.xjoi.net/.
*************************************/ 
#pragma GCC optimize(2)
#pragma GCC optimize(3)
#pragma GCC optimize("Ofast")
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

vector<int> nums;

int main()
{
    int q = read<int>(), t, x;
    nums.push_back(0);
    nums.push_back(100000001);
    while (q--) {
        t = read<int>();
        x = read<int>();
        if (t == 1) {
            nums.insert(upper_bound(nums.begin(), nums.end(), x), x);
        } else if (t == 2) {
            write(*--lower_bound(nums.begin(), nums.end(), x), 10);
        } else {
            write(*upper_bound(nums.begin(), nums.end(), x), 10);
        }
    }
    return 0;
}