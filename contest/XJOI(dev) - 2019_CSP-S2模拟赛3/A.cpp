/*************************************
 * problem:      A.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-11-10.
 * language:     C++.
 * upload place: http://dev.xjoi.net/.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

map<string, int> num;
int prepare_ArrNum() {
    num["3"] = 1;   num["4"] = 2;   num["5"] = 3;   num["6"] = 4;
    num["7"] = 5;   num["8"] = 6;   num["9"] = 7;   num["10"] = 8;
    num["J"] = 9;   num["Q"] = 10;  num["K"] = 11;  num["A"] = 12;
    num["2"] = 13;  
    return 0;
}
int preMain_Do = prepare_ArrNum();
/*
单牌：即单张手牌。但是由于卢老爷手上的单牌是大王，所以陈老爷出单牌时必须保证出完了所有手牌。
对子：即两张数码相同的牌，比如两个3，两个J。
三张牌：即三张数码相同的牌，比如三个3，三个J。
三带一：三张牌加一个不同花色的单牌，比如 3334，AAA2 。
顺子：五张或更多连续数码的牌。这里，连续数码的定义为：3 4 5 6 7 8 9 10 J Q K A 依次连续，但是不和2连续，2也不和3连续。比如你可以打出以下顺子：56789, 345678910JQKA 等。
*/

int n;
int cnt[16];
string s;

/*
Sample Data
17 K K K K Q 9 9 9 10 10 10 J J J 8 8 5
Yes
5 1
8 2
9 3
10 3
J 3
Q 1
K 4
*/
bool test()
{
    int cnt1 = 0, cnt3 = 0, cnt4 = 0;
    for (int i = 1; i <= 13; i++) {
        switch (cnt[i]) {
            case 1: cnt1++; break;
            case 3: cnt3++; break;
            case 4: cnt4++; break;
            default: break;
        }
    }
    // printf("cnt1 = %d, cnt3 = %d, cnt4 = %d.\n", cnt1, cnt3, cnt4);
    return cnt3 + cnt4 * 2 + 1 >= cnt1;
}

bool ans = false;
void dfs()
{
    // if (clock() > 0.9 * CLOCKS_PER_SEC) return;
    if (test()) ans = true;
    if (ans) return;
    // 顺子
    for (int i = 1, j; i <= 8; i++) {
        for (j = i; j <= 12; j++) {
            if (!cnt[j]) break;
            cnt[j]--;
            if (j - i + 1 >= 5) dfs();
        }
        for (int k = i; k < j; k++) cnt[k]++;   
    }
}

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        cin >> s;
        cnt[num[s]]++;
        // printf("%d %d\n", num[s], cnt[num[s]]);
    }
    dfs();
    puts(ans ? "Yes" : "No");
    return 0;
}