# Date 日志

## Explanation 说明

Message|Meaning|
:-:|:-:|
``[info] = 0``  | 信息记录 |
``[warn] = 8``  | 警告     |
``[-err] = 16`` | 错误     |  

## Record 记录

``[info] 10:03`` 预估分数   
$(100 + 50 + 50) * 70\% = 140$   
``[info] 10:58`` 预估分数    
$(100 + 70 + 50) * 72\% = 158.4$    
``[-err] 11:06`` vscode 崩溃   
从 [lib_fastHeader.hpp](../../lib_fastHeader.hpp) 创建后    
``[info] 11:09`` vscode 恢复   
已重启 vscode    
``[info] 11:22`` 预估分数    
$(100 + 70 + 50) * 75\% = 165$  
``[info] 11:35`` 预估分数    
$(100 + 70 + 50) * 80\% = 176$  
``[info] 11:50`` 实际分数    
$0 + 100 + 50 = 150$  