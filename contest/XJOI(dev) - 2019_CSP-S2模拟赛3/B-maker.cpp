#include <bits/stdc++.h>
using namespace std;
#define bigRand() (rand() * RAND_MAX + rand())
#define random(x) (bigRand() % (x) + 1)

int main(int argc, char **argv) 
{
    freopen("B.in", "w", stdout);
    srand(time(0) * clock());
    if (argc != 2) {
        fprintf(stderr, "fail to revieve ARGs\n");
        // puts("fail to revieve ARGs\n");
        return 0;
    }
    int n = atoi(argv[1]);
    printf("%d\n", n);
    for (int i = 1; i <= n; i++) {
        printf("%d %d\n", random(3), random(100000000));
    }
    return 0;
}