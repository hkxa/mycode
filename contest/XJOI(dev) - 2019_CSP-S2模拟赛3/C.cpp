/*************************************
 * problem:      C.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-11-10.
 * language:     C++.
 * upload place: http://dev.xjoi.net/.
*************************************/ 
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, L, c[1007], v[1007];
int m;
// int s[1007];
bool could[1007];
bool ans[1007];
struct id {
    int i, k, v;
    // int val;
    id() {}
    id(int I, int K, int V) : i(I), k(K), v(V) {}
    // id(int I, int K, int V) : i(I), k(K), v(V), val(i * 2000000 + k * 1100 + v) {}
    // void pushVal() { val = i * 2000000 + k * 1100 + v; }
    // bool operator < (const id &b) const { 
    //     return val < b.val;
    // }
} t;
// set<id> f;
queue<id> q;
int mn[1007][1007];

int main()
{
    memset(mn, -1, sizeof(mn));
    n = read<int>();
    L = read<int>();
    for (int i = 1; i <= n; i++) {
        c[i] = read<int>();
        v[i] = read<int>();
    }
    m = read<int>();
    for (int i = 1; i <= m; i++) {
        // s[i] = read<int>();
        could[read<int>()] = true;
    }
    q.push(id(1, 0, 0));
    while (!q.empty()) {
        t = q.front(); q.pop();
        if (t.v > L || t.k <= mn[t.i][t.v]) continue;
        if (could[t.k]) ans[t.v] = true;
        mn[t.i][t.v] = t.k;
        if (t.i > n) continue;
        q.push(id(t.i + 1, t.k, t.v));
        for (int i = 1; i <= c[t.i]; i++) {
            q.push(id(t.i + 1, t.k + 1, t.v + i * v[t.i]));
        }
    }
    for (int i = 1; i <= L; i++) if (ans[i]) write(i, 32);
    putchar(10);
    return 0;
}