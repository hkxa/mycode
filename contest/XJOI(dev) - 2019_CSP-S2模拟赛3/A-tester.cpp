#include <bits/stdc++.h>
using namespace std;
#define random(x) (rand() % (x) + 1)
#define randLR(l, r) (random((r) - (l) + 1) + (l) - 1)

vector<int> WAlist;

int main() 
{
    int n;
    printf("Input Data Count : ");
    scanf("%d", &n);
    system("g++ A.cpp -o A");
    system("g++ A-std.cpp -o A-std");
    system("g++ A-maker.cpp -o A-maker");
    for (int i = 1; i <= n; i++) {
        printf("testing data #%d... ", i);
        system("A-maker");
        printf("1 ... ");
        system("A <A.in >A.out");
        printf("2 ... ");
        system("A-std <A.in >A.ans");
        printf("3 ... \n");
        if (system("fc A.out A.ans")) {
            char buf[104];
            sprintf(buf, "copy A.in A%03d.in", i);
            system(buf);
            WAlist.push_back(i);
            printf("WA\n");
        } else {
            printf("AC\n");
        }
    }
    if (WAlist.size()) {
        printf("Wrong Answer Points %.1lf\n", (double)(n - WAlist.size()) * 100 / n);
        printf("WA in %d", WAlist[0]);
        for (unsigned i = 1; i < WAlist.size(); i++) printf(", %d", WAlist[i]);
        printf("\n");
    } else {
        printf("ALL Accepted\n");
    }
    system("DEL A.exe A-std.exe A-maker.exe");
    system("DEL A.in A.ans A.out");
    return 0;
}