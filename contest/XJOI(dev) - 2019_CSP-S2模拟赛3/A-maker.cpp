#include <bits/stdc++.h>
using namespace std;
#define random(x) (rand() % (x) + 1)
#define randLR(l, r) (random((r) - (l) + 1) + (l) - 1)

const string s[] = {"3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A", "2"};

int main() 
{
    freopen("A.in", "w", stdout);
    srand(time(0) * clock());
    int n = randLR(2, 20);
    printf("%d\n", n);
    for (int i = 1; i <= n; i++) {
        printf("%s ", s[randLR(0, 12)].c_str());
    }
    return 0;
}