#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

struct IO_stdin_stdout {
    char endch;
    IO_stdin_stdout() : endch('\0') {}
    IO_stdin_stdout& operator >> (char *str) {
        while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
        while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
        *str = '\0';
        return *this;
    }
    IO_stdin_stdout& operator >> (char &ch) {
        while (((ch = getchar()) == ' ' || ch == '\n' || ch == '\r' || ch == '\t') && ch != EOF);
        return *this;
    }
    template <typename It>
    IO_stdin_stdout& operator >> (It &x) {
        static bool negative;
        negative = false;
        while (((endch = getchar()) < '0' || endch > '9') && endch != EOF)
            if (endch == '-') negative = true;
        x = endch & 15;
        while ((endch = getchar()) >= '0' && endch <= '9')
            x = (((x << 2) + x) << 1) + (endch & 15);
        if (negative) x = ~x + 1;
        return *this;
    }
    template <typename It>
    IO_stdin_stdout& operator << (It x) {
        static char bufs[25];
        static int top;
        if (!x) { putchar('0'); return *this; }
        if (x < 0) { putchar('-'); x = -x; }
        top = 0;
        while (x) { bufs[++top] = x % 10; x /= 10; }
        while (top) putchar(bufs[top--] | '0');
        return *this;
    }
    IO_stdin_stdout& operator << (const char ch) {
        putchar(ch); 
        return *this;
    }
    IO_stdin_stdout& operator << (const char* ss) {
        while (*ss) putchar(*(ss++)); 
        return *this;
    }
    IO_stdin_stdout& operator << (char* ss) { return *this << (const char *)ss; }
} io;

const int N = 1e6 + 7;

int n, m;
vector<int> GraphAdj[N];
int survivor_cnt[N];
int cnt_disaster[N], *disaster[N];
int fa[N];

int cntSonAlive[N];
bool already_dead[N];

struct MaybeBlackToWhite {
    bool in_list[N];
    queue<int> q;
    MaybeBlackToWhite() {
        memset(in_list, 0, sizeof(in_list));
    }
    bool push(int v) {
        if (in_list[v]) return false;
        in_list[v] = true;
        q.push(v);
        return true;
    }
    int get_and_pop() {
        if (q.empty()) return 0;
        int val = q.front();
        in_list[val] = false;
        q.pop();
        return val;
    }
} bySon, toSon, couldSurvive;

vector<int> dead_child[N];

void dfs(int u, int fat) {
    fa[u] = fat;
    for (size_t i = 0; i < GraphAdj[u].size(); i++)
        if (GraphAdj[u][i] != fat) dfs(GraphAdj[u][i], u);
}

void set_dead(int u) {
    if (already_dead[u]) return;
    already_dead[u] = true;
    if (cntSonAlive[u]) bySon.push(u);
    if (fa[u]) {
        dead_child[fa[u]].push_back(u);
        --cntSonAlive[fa[u]];
        if (!already_dead[fa[u]]) toSon.push(fa[u]);
    }
}

void set_alive(int u) {
    if (!already_dead[u]) return;
    already_dead[u] = false;
    if (dead_child[u].size()) toSon.push(u);
    if (fa[u] && !(cntSonAlive[fa[u]]++)) bySon.push(fa[u]);
}

void deal_day() {
    for (int u = bySon.get_and_pop(); u; u = bySon.get_and_pop())
        if (cntSonAlive[u]) couldSurvive.push(u);
    for (int u = toSon.get_and_pop(); u; u = toSon.get_and_pop())
        if (!already_dead[u]) {
            vector<int> &v = dead_child[u];
            for (size_t i = 0; i < v.size(); ++i)
                couldSurvive.push(v[i]);
            v.clear();
        }
    // 此处需要延迟对 couldSurvive 中的元素 set_alive，防止在一轮中被迭代更新数次
    for (int u = couldSurvive.get_and_pop(); u; u = couldSurvive.get_and_pop())
        set_alive(u);
}

signed main() {
    io >> n >> m;
    for (int i = 1; i <= n; i++) io >> survivor_cnt[i];
    for (int i = 1, u, v; i < n; i++) {
        io >> u >> v;
        GraphAdj[u].push_back(v);
        GraphAdj[v].push_back(u);
    }
    dfs(1, 0);
    for (int i = 1; i <= n; i++) cntSonAlive[i] = GraphAdj[i].size() - (i != 1);
    for (int i = 1; i <= m; ++i) {
        io >> cnt_disaster[i];
        disaster[i] = new int[cnt_disaster[i]];
        for (int j = 0; j < cnt_disaster[i]; ++j)
            io >> disaster[i][j];
    }
    for (int i = m; i >= 1; --i) {
        for (int j = 0; j < cnt_disaster[i]; ++j)
            set_dead(disaster[i][j]);
        deal_day();
    }
    int64 ans = 0;
    for (int i = 1; i <= n; i++)
        if (!already_dead[i]) ans += survivor_cnt[i];
    io << ans << '\n';
    return 0;
}