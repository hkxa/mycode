#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

struct IO_stdin_stdout {
    char endch;
    IO_stdin_stdout() : endch('\0') {}
    IO_stdin_stdout& operator >> (char *str) {
        while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
        while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
        *str = '\0';
        return *this;
    }
    template <typename It>
    IO_stdin_stdout& operator >> (It &x) {
        static bool negative;
        negative = false;
        while (((endch = getchar()) < '0' || endch > '9') && endch != EOF)
            if (endch == '-') negative = true;
        x = endch & 15;
        while ((endch = getchar()) >= '0' && endch <= '9')
            x = (((x << 2) + x) << 1) + (endch & 15);
        if (negative) x = ~x + 1;
        return *this;
    }
    template <typename It>
    IO_stdin_stdout& operator << (It x) {
        static char bufs[25];
        static int top;
        if (!x) { putchar('0'); return *this; }
        if (x < 0) { putchar('-'); x = -x; }
        top = 0;
        while (x) { bufs[++top] = x % 10; x /= 10; }
        while (top) putchar(bufs[top--] | '0');
        return *this;
    }
    IO_stdin_stdout& operator << (const char ch) {
        putchar(ch); 
        return *this;
    }
    IO_stdin_stdout& operator << (const char* ss) {
        while (*ss) putchar(*(ss++)); 
        return *this;
    }
    IO_stdin_stdout& operator << (char* ss) { return *this << (const char *)ss; }
} io;

const int W = 50000 + 6, H = 100 + 6;

int w, h;
char snow[H][W];

int expand_ru(int x, int y) {
    int ret = 0;
    while (x >= 1 && y <= w) {
        if (snow[x][y] == '*') ++ret;
        --x, ++y;
    }
    return ret;
}

int expand_lu(int x, int y) {
    int ret = 0;
    while (x >= 1 && y >= 1) {
        if (snow[x][y] == '*') ++ret;
        --x, --y;
    }
    return ret;
}

signed main() {
    io >> w >> h;
    for (int i = 1; i <= h; ++i) io >> (snow[i] + 1);
    int ans = 0, now = 0;
    for (int i = 1; i <= h; ++i)
        for (int j = 1; j <= min(h - i + 1, w); ++j)
            if (snow[i][j] == '*') ++now;
    ans = now;
    // io << now << '\n';
    for (int pos = 2; pos <= w; ++pos) {
        now = now + expand_ru(h, pos) - expand_lu(h, pos - 1);
        ans = max(ans, now);
        // io << now << '\n';
    }
    io << ans << '\n';
    return 0;
}