#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

struct IO_stdin_stdout {
    char endch;
    IO_stdin_stdout() : endch('\0') {}
    IO_stdin_stdout& operator >> (char *str) {
        while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
        while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
        *str = '\0';
        return *this;
    }
    IO_stdin_stdout& operator >> (char &ch) {
        while (((ch = getchar()) == ' ' || ch == '\n' || ch == '\r' || ch == '\t') && ch != EOF);
        return *this;
    }
    template <typename It>
    IO_stdin_stdout& operator >> (It &x) {
        static bool negative;
        negative = false;
        while (((endch = getchar()) < '0' || endch > '9') && endch != EOF)
            if (endch == '-') negative = true;
        x = endch & 15;
        while ((endch = getchar()) >= '0' && endch <= '9')
            x = (((x << 2) + x) << 1) + (endch & 15);
        if (negative) x = ~x + 1;
        return *this;
    }
    template <typename It>
    IO_stdin_stdout& operator << (It x) {
        static char bufs[25];
        static int top;
        if (!x) { putchar('0'); return *this; }
        if (x < 0) { putchar('-'); x = -x; }
        top = 0;
        while (x) { bufs[++top] = x % 10; x /= 10; }
        while (top) putchar(bufs[top--] | '0');
        return *this;
    }
    IO_stdin_stdout& operator << (const char ch) {
        putchar(ch); 
        return *this;
    }
    IO_stdin_stdout& operator << (const char* ss) {
        while (*ss) putchar(*(ss++)); 
        return *this;
    }
    IO_stdin_stdout& operator << (char* ss) { return *this << (const char *)ss; }
} io;

const int N = 2e5 + 7;
#define test_bit(x, j) (((x) >> (j)) & 1)

int T, n, a[207];
int64 m, s;

bitset <N> cur, trn;

int main() {
    for (io >> T; T--;) {
        io >> m >> s >> n;
        for (int i = 1; i <= n; i++) io >> a[i];
        cur.reset();
        cur.set(0);
        for (int i = 61; i >= 0; i--) {
            trn.reset();
            for (int j = 0; j <= 1e5; j++)
                if (cur.test(j)) trn.set(j << 1);
            if (test_bit(s, i)) trn <<= 1;
            cur.reset();
            if (test_bit(m, i))
                for (int j = 1; j <= n; j++)
                    cur ^= trn >> a[j];
            else cur = trn;
        }
        io << (int)cur.test(0) << '\n';
    }
    return 0;
}