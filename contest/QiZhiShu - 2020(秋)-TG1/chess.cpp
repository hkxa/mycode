#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

struct IO_stdin_stdout {
    char endch;
    IO_stdin_stdout() : endch('\0') {}
    IO_stdin_stdout& operator >> (char *str) {
        while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
        while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
        *str = '\0';
        return *this;
    }
    IO_stdin_stdout& operator >> (char &ch) {
        while (((ch = getchar()) == ' ' || ch == '\n' || ch == '\r' || ch == '\t') && ch != EOF);
        return *this;
    }
    template <typename It>
    IO_stdin_stdout& operator >> (It &x) {
        static bool negative;
        negative = false;
        while (((endch = getchar()) < '0' || endch > '9') && endch != EOF)
            if (endch == '-') negative = true;
        x = endch & 15;
        while ((endch = getchar()) >= '0' && endch <= '9')
            x = (((x << 2) + x) << 1) + (endch & 15);
        if (negative) x = ~x + 1;
        return *this;
    }
    template <typename It>
    IO_stdin_stdout& operator << (It x) {
        static char bufs[25];
        static int top;
        if (!x) { putchar('0'); return *this; }
        if (x < 0) { putchar('-'); x = -x; }
        top = 0;
        while (x) { bufs[++top] = x % 10; x /= 10; }
        while (top) putchar(bufs[top--] | '0');
        return *this;
    }
    IO_stdin_stdout& operator << (const char ch) {
        putchar(ch); 
        return *this;
    }
    IO_stdin_stdout& operator << (const char* ss) {
        while (*ss) putchar(*(ss++)); 
        return *this;
    }
    IO_stdin_stdout& operator << (char* ss) { return *this << (const char *)ss; }
} io;

namespace RandGen {
    int xx, AA, BB, CC;
    void init_from_stdin() { io >> xx >> AA >> BB >> CC; }
    int get() { return xx = (xx * AA + BB) % CC; }
}

int n;

struct chequer {
    int attack, life;
    char race, attr;
    void read_from_stdin() { io >> attack >> life >> race >> attr; }
};

struct player {
    chequer c[51];
    int cnt_alive;
    int cntL, cntC;
    void calc_cnt_LandC() {
        cntL = cntC = 0;
        for (int i = 0; i < n; ++i)
            if (c[i].attr == 'L') ++cntL;
            else if (c[i].attr == 'C') ++cntC;
    }
    chequer& get_nth(int nth) {
        chequer *now = c;
        while (true) {
            if (now->life > 0) --nth;
            if (nth < 0) return *now;
            ++now;
        }
    }
    void deal_died(chequer &x) {
        if (x.life > 0) return;
        --cnt_alive;
        if (x.attr == 'C') --cntC;
        else if (x.attr == 'L') {
            --cntL;
            for (int i = 0; i < n; ++i)
                if (c[i].life > 0 && c[i].race == 'M')
                    c[i].attack -= 20;
        } else if (x.attr == 'K') {
            for (int i = 0; i < n; ++i)
                if (c[i].life > 0 && c[i].race == 'M') {
                    c[i].attack += 20;
                    c[i].life += 20;
                }
        }
        if (x.race == 'P') {
            for (int i = 0; i < n; ++i)
                if (c[i].life > 0 && c[i].attr == 'G')
                    c[i].attack += 50;
        }
    }
} S, R;

pair<int, int> operator + (pair<int, int> a, pair<int, int> b) {
    return make_pair(a.first + b.first, a.second + b.second);
}

pair<int, int> play(player s, player r) {
    while (s.cnt_alive && r.cnt_alive) {
        int id1 = RandGen::get() % s.cnt_alive;
        int id2 = RandGen::get() % r.cnt_alive;
        chequer &a = s.get_nth(id1), &b = r.get_nth(id2);
        if (a.race == 'P') {
            a.attack += (s.cntC - (a.attr == 'C')) * 20;
            a.life += (s.cntC - (a.attr == 'C')) * 20;
        }
        if (b.race == 'P') {
            b.attack += (r.cntC - (b.attr == 'C')) * 20;
            b.life += (r.cntC - (b.attr == 'C')) * 20;
        }
        a.life -= b.attack;
        b.life -= a.attack;
        s.deal_died(a);
        r.deal_died(b);
    }
    return make_pair(s.cnt_alive, r.cnt_alive);
}

signed main() {
    io >> n;
    S.cnt_alive = R.cnt_alive = n;
    RandGen::init_from_stdin();
    for (int i = 0; i < n; ++i) S.c[i].read_from_stdin();
    S.calc_cnt_LandC();
    for (int i = 0; i < n; ++i)
        if (S.c[i].race == 'M')
            S.c[i].attack += 20 * (S.cntL - (S.c[i].attr == 'L'));
    for (int i = 0; i < n; ++i) R.c[i].read_from_stdin();
    R.calc_cnt_LandC();
    for (int i = 0; i < n; ++i)
        if (R.c[i].race == 'M')
            R.c[i].attack += 20 * (R.cntL - (R.c[i].attr == 'L'));
    pair<int, int> ans(0, 0);
    for (int i = 0; i < 500; ++i)
        ans = ans + play(S, R);
    io << ans.first << ' ' << ans.second << '\n';
    return 0;
}