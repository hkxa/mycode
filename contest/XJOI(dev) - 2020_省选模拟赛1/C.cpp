/*************************************
 * @user_name:    Jomoo.
 * @time:         2020-03-01.
 * @language:     C++.
*************************************/ 

#pragma GCC optimize("-O1")
#pragma GCC optimize("-O2")
#pragma GCC optimize("-O3")
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}
#define int int64
int n, m;
int ForSort[250000];
list<pair<int, int> > a[250000];
list<pair<int, int> >::iterator it;
#define A first
#define B second
int opt, x, y, val, k, l, r, Al, Ar;
// inline int RV(int ele) // DEBUG 专用
// {
//     printf("read : {%d} \n", ele);
//     return ele;
// }
#define readValue() (read<int>() ^ ans)
#define read2(a, b) { a = readValue(); b = readValue(); }
#define read3(a, b, c) { a = readValue(); read2(b, c); }
#define read4(a, b, c, d) { a = readValue(); read3(b, c, d); }
#define read5(a, b, c, d, e) { a = readValue(); read4(b, c, d, e); }
#define max(a, b) (((a) > (b)) ? (a) : (b))

signed main()
{
    n = read<int>();
    m = read<int>();
    for (register int i = 1, ele1, ele2; i <= n; i++) {
        ele1 = read<int>();
        ele2 = read<int>();
        a[i].push_back(make_pair(ele1, ele2));
    }
    register int ans = 0;
    for (register int i = 1; i <= m; i++) {
        opt = read<int>();
        switch (opt) {
            case 1:
                read3(x, y, val);
                it = next(a[x].begin(), y - 1);
                it->A = val;
                break;
            case 2:
                read3(x, y, val);
                it = next(a[x].begin(), y - 1);
                it->B = val;
                break;
            case 3:
                read2(x, y);
                a[x].splice(a[x].end(), a[y]);
                a[y].clear();
                // printf("front = %d.\n", a[x].begin()->A);
                break;
            case 4:
                read4(x, l, r, val);
                ans = 0;
                it = next(a[x].begin(), l - 1);
                while (l <= r) {
                    if (it->A > val) ans++;
                    it++;
                    l++;
                }
                write(ans, 10);
                break;
            case 5:
                read4(x, l, r, k);
                ans = 0;
                it = next(a[x].begin(), l - 1);
                for (int j = l; j <= r; j++) {
                    ForSort[j] = it->A;
                    // printf("ForSort[%d] = %d.\n", j, ForSort[j]);
                    it++;
                }
                sort(ForSort + l, ForSort + r + 1);
                // for (int j = l; j <= r; j++) {
                //     printf("ForSort[%d] = %d.\n", j, ForSort[j]);
                // }
                ans = ForSort[r - k + 1];
                write(ans, 10);
                break;
            case 6:
                read5(x, l, r, Al, Ar);
                ans = 0;
                it = next(a[x].begin(), l - 1);
                // 3 5 4 7 
                // 3 4 5 7
                while (l <= r) {
                    // printf("find {%d, %d} in [%d, %d]\n", it->A, it->B, l, r);
                    if (it->A >= Al && it->A <= Ar) ans = max(ans, it->B);
                    it++;
                    l++;
                }
                write(ans, 10);
                break;
        }
        // printf("list[%d] : ", x);
        // it = a[x].begin();
        // while (it != a[x].end()) {
        //     printf("[%d, %d] ", it->A, it->B);
        //     it++;
        // }
        // puts("");
    }
    return 0; 
}

/*
10 10
1 1
2 2
3 3
4 4 
5 5
6 6
7 7
8 8
9 9
10 10
3 3 5
3 3 4
4 3 1 2 3
5 2 0 2 0
3 2 6
6 2 7 6 6 6
1 4 0 0
6 4 1 0 0 0
2 2 4 3
6 2 4 1 1 2
*/