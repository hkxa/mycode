/*************************************
 * @user_name:    Jomoo.
 * @time:         2020-03-01.
 * @language:     C++.
*************************************/ 

#pragma GCC optimize("-O1")
#pragma GCC optimize("-O2")
#pragma GCC optimize("-O3")
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, k;
int a[100007][2];
int64 ans[100007];

inline int64 getAns(int i, int j)
{
    int64 A = ans[j];
    for (register int b = 0; b < k; b++)
        A += (a[i][b] - a[j][b]) * (a[i][b] - a[j][b]);
    return A;
}

int main()
{
    n = read<int>();
    k = read<int>();
    for (register int i = 1; i <= n; i++)
        for (register int j = 0; j < k; j++) a[i][j] = read<int>();
    if (k == 1)
        for (register int i = 1; i <= n; i++) {
            ans[i] = a[i][0] * a[i][0];
            for (register int j = 1; j < i; j++) {
                ans[i] = min(ans[i], ans[j] + (a[i][0] - a[j][0]) * (a[i][0] - a[j][0]));
            }
            printf("%.4lf\n", sqrt(ans[i]));
        }
    else 
        for (register int i = 1; i <= n; i++) {
            ans[i] = getAns(i, 0);
            for (register int j = 1; j < i; j++) {
                ans[i] = min(ans[i], getAns(i, j));
            }
            printf("%.4lf\n", sqrt(ans[i]));
        }
    return 0;
}