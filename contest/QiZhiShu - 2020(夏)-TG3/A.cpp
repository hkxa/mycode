//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      QiZhiShu - 2020(夏)-TG3.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-21.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 5e5 + 7;

int n, k;
bool ban[N];
int fa[N], dep[N];
int ans[N], cable_ans = 0;
vector<pair<int, int> > son[N];
vector<int> nodes[N];

void preDfs(int u) {
    nodes[dep[u]].push_back(u);
    for (size_t i = 0; i < son[u].size(); i++) {
        int v = son[u][i].first, w = son[u][i].second;
        dep[v] = dep[u] + 1;
        preDfs(v);
    }
}

signed main() {
    read >> n >> k;
    for (int i = 2, l; i <= n; i++) {
        read >> fa[i] >> l >> ban[i];
        son[fa[i]].push_back(make_pair(i, l));
    }
    dep[1] = 1;
    preDfs(1);
    for (int i = 1; !nodes[i].empty(); i++) {
        for (size_t j = 0; j < nodes[i].size(); j++) {
            cable_ans = min(cable_ans, ans[nodes[i][j]]);
        }
        for (size_t j = 0; j < nodes[i].size(); j++) {
            int &u = nodes[i][j];
            if (!ban[u]) ans[u] = min(ans[u], cable_ans);
            for (size_t g = 0; g < son[u].size(); g++) {
                ans[son[u][g].first] = ans[u] + son[u][g].second;
            }
        }
        cable_ans += k;
    }
    for (int i = 1; i <= n; i++)
        write << ans[i] << '\n';
    return 0;
}