//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      QiZhiShu - 2020(夏)-TG3.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-21.
 * @language:     C++.
*************************************/ 
#pragma GCC optimize(2)
#pragma GCC optimize(3)

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

#define int int64

const int N = 300007;

int n, q, k;
int a[N], cut[N];
int64 ans;

int64 value(int A, int cut_cnt) {
    cut_cnt++;
    int res1 = A / cut_cnt;
    int cnt2 = A - res1 * cut_cnt;
    // printf("value(%d, %d) = %lld\n", A, cut_cnt - 1, (int64)(cut_cnt - cnt2) * res1 * res1 + (int64)cnt2 * (res1 + 1) * (res1 + 1));
    return (int64)(cut_cnt - cnt2) * res1 * res1 + (int64)cnt2 * (res1 + 1) * (res1 + 1);
}

struct PrioNode {
    int pos;
    int orig_cut;
    int64 diff_val;
    PrioNode() {}
    PrioNode(int Id, int Oc, int64 Dv) : pos(Id), orig_cut(Oc), diff_val(Dv) {}
    bool operator < (const PrioNode &b) const {
        return diff_val ^ b.diff_val ? diff_val < b.diff_val : orig_cut > b.orig_cut;
    }
};
priority_queue<PrioNode> pq;
stack<PrioNode> done_operation;

signed main() {
    read >> n >> q >> k;
    for (int i = 1; i <= n; i++) {
        read >> a[i];
        ans += (int64)a[i] * a[i];
        pq.push(PrioNode(i, 0, value(a[i], 0) - value(a[i], 1)));
    }
    for (int i = 1; i <= k; i++) {
        PrioNode u = pq.top(); pq.pop();
        cut[u.pos]++;
        ans -= u.diff_val;
        if (cut[u.pos] + 1 < a[u.pos])
            pq.push(PrioNode(u.pos, cut[u.pos], value(a[u.pos], cut[u.pos]) - value(a[u.pos], cut[u.pos] + 1)));
        done_operation.push(u);
    }
    write << ans << '\n';
    for (int i = 1, opt, x; i <= q; i++) {
        read >> opt;
        if (opt == 1) {
            while (pq.top().orig_cut != cut[pq.top().pos]) pq.pop();
            PrioNode u = pq.top(); pq.pop();
            cut[u.pos]++;
            ans -= u.diff_val;
            if (cut[u.pos] + 1 < a[u.pos])
                pq.push(PrioNode(u.pos, cut[u.pos], value(a[u.pos], cut[u.pos]) - value(a[u.pos], cut[u.pos] + 1)));
            done_operation.push(u);
        } else if (opt == 2) {
            int u = done_operation.top().pos;
            cut[u]--;
            ans += done_operation.top().diff_val;
            pq.push(PrioNode(u, cut[u], value(a[u], cut[u]) - value(a[u], cut[u] + 1)));
            done_operation.pop();
        } else if (opt == 3) {
            read >> x;
            ans += (int64)x * x;
            a[++n] = x;
            PrioNode u(n, 0, value(a[n], 0) - value(a[n], 1));
            pq.push(u);
            int cnt_pop = 0;
            while (!done_operation.empty() && done_operation.top() < u) {
                int wh = done_operation.top().pos;
                cut[wh]--;
                ans += done_operation.top().diff_val;
                pq.push(PrioNode(wh, cut[wh], value(a[wh], cut[wh]) - value(a[wh], cut[wh] + 1)));
                done_operation.pop();
                cnt_pop++;
            }
            while (cnt_pop--) {
                while (pq.top().orig_cut != cut[pq.top().pos]) pq.pop();
                PrioNode u = pq.top(); pq.pop();
                cut[u.pos]++;
                ans -= u.diff_val;
                if (cut[u.pos] + 1 < a[u.pos])
                    pq.push(PrioNode(u.pos, cut[u.pos], value(a[u.pos], cut[u.pos]) - value(a[u.pos], cut[u.pos] + 1)));
                done_operation.push(u);
            }
        }
        write << ans << '\n';
    }
    return 0;
}