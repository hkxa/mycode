//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      QiZhiShu - 2020(夏)-TG3.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-21.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 50000 + 7;

struct point {
    int p, v;
    double calc_pos(double tim) {
        return p + tim * v;
    }
    bool operator < (const point &b) const {
        return p < b.p;
    }
} a[N];
int n, k;
struct LIS {
    double pos;
    int id;
    bool operator < (const LIS &b) const {
        return pos < b.pos;
    }
} g[N];

int f[N];
int BIT_t[N];

void BIT_clear() { memset(BIT_t, 0, sizeof(BIT_t)); }
void BIT_update(int pos, int mx) { 
    while (pos <= n) {
        BIT_t[pos] = max(BIT_t[pos], mx);
        pos += pos & -pos;
    }
}
int BIT_query(int pos) { 
    int mx = 0;
    while (pos) {
        mx = max(mx, BIT_t[pos]);
        pos -= pos & -pos;
    }
    return mx;
}

bool check(double tim) {
    for (int i = 1; i <= n; i++) {
        g[i].pos = a[i].calc_pos(tim);
        g[i].id = i;
    }
    sort(g + 1, g + n + 1);
    BIT_clear();
    for (int i = 1; i <= n; i++) {
        f[i] = BIT_query(g[i].id) + 1;
        BIT_update(g[i].id, f[i]);
    }
    return f[n] >= n - k;
}

signed main() {
    read >> n >> k;
    for (int i = 1; i <= n; i++) {
        read >> a[i].p >> a[i].v;
    }
    sort(a + 1, a + n + 1);
    double l = 0, r = 3e6, mid;
    if (check(3e6)) {
        puts("Forever");
        return 0;
    }
    while (r - l >= 1e-6) {
        mid = (l + r) / 2;
        if (check(mid)) l = mid;
        else r = mid;
    }
    printf("%.4lf\n", (l + r) / 2);
    return 0;
}