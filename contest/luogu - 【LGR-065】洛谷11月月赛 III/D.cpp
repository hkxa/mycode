/*************************************
 * @problem:      Monthly Contest III.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2019-11-13.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, k;
int64 a[1000007], w[1000007];

#define min4(a, b, c, d) min(min(a, b), min(c, d))

int Sub5()
{
    int64 x = 0, ans = 0;
    a[0] = a[n + 1] = 100000000;
    for (int i = 1; i <= n; i++) {
        x = min4(x + k, a[i], a[i - 1] + k, a[i + 1] + k);
        ans += x * w[i];
    }
    write(ans);
    return 0;
}

int TanXin()
{
    int64 x = 0, ans = 0, wtot = 0;
    a[0] = a[n + 1] = 100000000;
    for (int i = 1; i <= n; i++) wtot += w[i];
    for (int i = 1; i <= n; i++) {
        // wtot -= w[i];
        x = min4(x + k * (wtot >= 0 ? 1 : -1), a[i], a[i - 1] + k, a[i + 1] + k);
        ans += x * w[i];
        wtot -= w[i];
    }
    write(ans);
    return 0;
}

int64 TanXinBase(double alpha)
{
    int64 x = 0, ans = 0;
    double alpha_ = 1 / alpha;
    double wtot = 0;
    a[0] = a[n + 1] = 100000000;
    for (int i = n; i >= 1; i--) wtot = wtot * alpha + w[i];
    for (int i = 1; i <= n; i++) {
        // wtot -= w[i];
        x = min4(x + k * (wtot >= 0 ? 1 : -1), a[i], a[i - 1] + k, a[i + 1] + k);
        ans += x * w[i];
        wtot = (wtot - w[i]) * alpha_;
    }
    return ans;
}

int TanXin3()
{
    int64 ans = 0;
    for (double al = 0.2; al <= 1.8; al += 0.01) ans = max(ans, TanXinBase(al));
    write(ans);
    return 0;
}

int64 dfs(int i, int64 x)
{
    if (i > n) return 0;
    int64 ans = -0x7fffffffffffffff;
    for (int j = x - k; j <= min(x + k, a[i]); j++) {
        ans = max(ans, j * w[i] + dfs(i + 1, j));
    }
    return ans;
}

int BaoLi()
{
    write(dfs(1, 0));
    return 0;
}

int main()
{
    n = read<int>();
    k = read<int>();
    bool CouldSub5 = 1;
    for (int i = 1; i <= n; i++) a[i] = read<int>();
    for (int i = 1; i <= n; i++) {
        w[i] = read<int>();
        if (w[i] < 0) CouldSub5 = 0;
    }
    for (int i = n - 1; i >= 1; i--) a[i] = min(a[i], a[i + 1] + k);
    if (n <= 100 && pow(k, n) < 1e7) return BaoLi();
    if (CouldSub5) return Sub5();
    if (n > 100 && n <= 1000) return TanXin3();
    TanXin();
    return 0;
}