/*************************************
 * @problem:      Monthly Contest III.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2019-11-13.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

uint32 n, m, q, type;
bool couldVis[1000007];
int win[1000007];
// int nearbyWin[1000007];
typedef uint32 FuncGetInt(void);
FuncGetInt *geti;

uint32 A, B, C, P;
inline uint32 rnd() { return A = (A * B + C) % P; }
inline uint32 randm() { return rnd() % n + 1; }

uint32 ans = 0;

void get55pts()
{
    queue<uint32> lose;
    for (uint32 i = 1, l, r; i <= q; i++) {
        l = geti();
        r = geti();
        if (l > r) swap(l, r);
        if (!couldVis[l]) {
            ans += i * i;
            continue;
        }
        while (!lose.empty()) lose.pop();
        for (uint32 j = r; j >= l; j--) {
            if (!lose.empty() && lose.front() > j + m) lose.pop();
            if (!lose.empty()) win[j] = 1;
            else win[j] = !couldVis[j];
            if (!win[j]) lose.push(j);
        }
        // write(win[l]);
        if (win[l]) ans += i * i;
    }
    write(ans);
}
struct Ques {
    uint32 id;
    uint32 l, r;
    bool operator < (const Ques &b) const { return r == b.r ? l < b.l : r < b.r; }
} qa[1000007];
void try100pts()
{
    queue<uint32> lose;
    for (uint32 i = 1; i <= q; i++) {
        qa[i].id = i;
        qa[i].l = geti();
        qa[i].r = geti();
        if (qa[i].l > qa[i].r) swap(qa[i].l, qa[i].r);
    }
    sort(qa + 1, qa + q + 1);
    for (uint32 r = 1, i = 1, needUpd = 0; r <= n; r++) {
        needUpd |= couldVis[r];
        if (qa[i].r != r) continue;
        if (needUpd) {
            while (!lose.empty()) lose.pop();
            for (uint32 j = r; j >= 1; j--) {
                if (!lose.empty() && lose.front() > j + m) lose.pop();
                if (!lose.empty()) win[j] = 1;
                else win[j] = !couldVis[j];
                if (!win[j]) lose.push(j);
            }
        }
        while (qa[i].r == r) {
            if (win[qa[i].l]) ans += qa[i].id * qa[i].id;
            if (++i > q) goto outputTag_func100;
        }
    }
    outputTag_func100 : write(ans);
}

int main()
{
    n = read<uint32>();
    m = read<uint32>();
    q = read<uint32>();
    type = read<uint32>();
    for (uint32 i = 1; i <= n; i++) couldVis[i] = (read<int>() & 1) == 1;
    if (type == 0) geti = read<uint32>;
    else {
        A = read<uint32>();
        B = read<uint32>();
        C = read<uint32>();
        P = read<uint32>();
        geti = randm;
    }
    // try100pts(); return 0;
    if (n <= 5000 && m <= 5000 && q <= 5000) get55pts();
    // else if (m <= 5) try15pts();
    else try100pts();
    return 0;
}