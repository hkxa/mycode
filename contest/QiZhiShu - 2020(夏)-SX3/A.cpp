//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      QiZhiShu - 2020(夏)-SX3.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-26.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 5e5 + 7, Sn = 4007, P = 998244353;
char s[N];
int fac[N], ifac[N];

int power(int x, int y) {
    int ret = 1;
    while (y) {
        if (y & 1) ret = (int64)ret * x % P;
        x = (int64)x * x % P;
        y >>= 1;
    }
    return ret;
}

#define inv(x) power(x, P - 2)

int C(int x, int y) {
    if (y > x || y < 0) return 0;
    return (int64)fac[x] * ifac[y] % P * ifac[x - y] % P;
}

void upd_sum(int &x, int y) {
    x += y;
    if (x >= P) x -= P;
}

void pretreat(int n) {
    fac[0] = 1;
    for (int i = 1; i <= n; i++)
        fac[i] = (int64)fac[i - 1] * i % P;
    ifac[n] = inv(fac[n]);
    for (int i = n; i > 0; i--)
        ifac[i - 1] = (int64)ifac[i] * i % P;
}

int main() {
    scanf("%s", s + 1);
    int n = strlen(s + 1); 
    pretreat(n);
    int ans = power(2, n) - 1;
    for (int i = 1, last = 1; i <= n; i++) {
        if (s[i] != s[i + 1]) {
            int len = i - last + 1;
            int cur = C(n - len, last - 1);
            upd_sum(ans, (int64)cur * (len - 1) % P);
            for (int l = len - 1, j = last - 2, k = last; l >= 1; l--, k++) {
                cur = 2 * cur % P;
                upd_sum(cur, (C(n - l - 1, j) + C(n - l - 1, k)) % P);
                upd_sum(ans, P - cur);
                if (last != 1) upd_sum(ans, (int64)l * C(n - l - 1, last - 2) % P);
                if (i != n) upd_sum(ans, (int64)l * C(n - l - 1, n - i - 1) % P);
            }
            // printf("contribution = %d\n", cur);
            last = i + 1;
        }
    }
    write << ans << '\n';
    return 0;
}