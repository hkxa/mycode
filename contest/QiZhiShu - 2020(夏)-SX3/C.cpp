//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      QiZhiShu - 2020(夏)-SX3.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-26.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 2e5 + 7, Subtask_N = 1e4 + 7, time_limit = 3.8 * CLOCKS_PER_SEC;

int n, k, a[N];
int64 sum[N];
#define query(l, r) (sum[r] - sum[(l) - 1])
int mx[Subtask_N][Subtask_N];

// powershell $N = 200007; $SubN = 10007; $MemN = 507; ($N * 4 + $N * 8 + $SubN * $SubN * 4 + $MemN * $MemN * $MemN * 4) / (1024 * 1024)

int b[N];
int can_not[468][468][468];

#ifdef YouHua_dfs
int dfs_cnt = 0;
#endif

bool dfs(int u, int predi) {
#ifdef YouHua_dfs
    dfs_cnt++;
#endif
    if (u < 468 && predi < 468 && predi > 1 && can_not[b[predi - 2]][u][predi]) return false;
    if (clock() > time_limit) return false;
    if (predi == k) {
        if (abs(query(b[predi - 2] + 1, u - 1) - query(u, n)) > mx[b[predi - 2] + 1][n]) return false;
        for (int i = 1; i < k; i++) 
            write << b[i] << " \n"[i == k - 1];
        return true;
    }
    for (int v = u; v <= n - k + predi; v++) {
        b[predi] = v;
        if (predi != 1 && abs(query(b[predi - 2] + 1, u - 1) - query(u, v)) > mx[b[predi - 2] + 1][v]) continue;
        if (dfs(v + 1, predi + 1)) return true;
    }
    if (u < 468 && predi < 468 && predi > 1) can_not[b[predi - 2]][u][predi] = true;
    return false;
}

signed main() {
    read >> n >> k;
    if (n >= Subtask_N) {
        puts("No Solution");
        return 0;
    }
    for (int i = 1; i <= n; i++) {
        read >> a[i];
        sum[i] = sum[i - 1] + a[i];
        for (int j = 1; j <= i; j++)
            mx[j][i] = max(mx[j][i - 1], a[i]);
    }
    if (!dfs(1, 1)) puts("No Solution");
#ifdef YouHua_dfs
    printf("dfs %d time(s)\n", dfs_cnt);
#endif
    return 0;
}