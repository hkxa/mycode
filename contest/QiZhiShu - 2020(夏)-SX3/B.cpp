//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      QiZhiShu - 2020(夏)-SX3.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-26.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 2e5 + 7;

int n;
int64 m, k;
int64 a[N];

void Work(int64 inc, int len, int64 rounds) {
    int g = __gcd(len, n), cnt = n / g;
    for (int i = 1; i <= n; i++)
        a[i] = rounds + 1;
    int64 PerRound = rounds / cnt, ExtraRound = rounds % cnt;
    int64 ExtraGet = PerRound * (len * k - n);
    for (int i = 1; i <= n; i++)
        if (i % g == !!(g - 1)) a[i] += ExtraGet;
    for (int i = 1, j = 1; i <= ExtraRound; i++, j = (j + len - 1) % n + 1)
        a[j] += len * k - n;
    a[(len * rounds) % n + 1] += inc;
}

void ShiftArray(int RightMove) {
    int64 Tmp[N];
    for (int i = 1; i <= n; i++)    
        Tmp[(i + RightMove - 1) % n + 1] = a[i];
    for (int i = 1; i <= n; i++)    
        a[i] = Tmp[i];
}

void DebugArray() {
    for (int i = 1; i <= n; i++)
        printf("%lld%c", a[i], " \n"[i == n]);
}

signed main() {
    read >> n >> m >> k;
    if (m < n) {
        for (int i = 1; i <= n; i++)
            write << '0' << " \n"[i == n];
        return 0;
    }

    int64 cur = m % k, len = (n - 1) / k + 1; 
    while (cur < n) cur += k;
    int64 tot = (m - cur) / k;

    // printf("%lld %lld %lld\n", cur, len, tot);
    
    Work(cur - n, len, tot / len);
    // DebugArray();
    ShiftArray(tot % len);

    for (int i = 1; i <= n; i++)
        write << a[i] << " \n"[i == n];
    return 0;
}

