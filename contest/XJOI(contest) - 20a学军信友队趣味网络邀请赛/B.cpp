/*************************************
 * @contest:      学军信友队趣味网络邀请赛.
 * @user_id:      20a-056.
 * @user_name:    赵奕.
 * @time:         2020-04-05.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;
int a[50007];
vector<int> G[50007];
int mx[50007], mxSon[50007], mxTo[50007], mx2[50007], mx2Son[50007], mx2To[50007];
int f[50007], fTo[50007];

void dfs(int u, int fa)
{
    for (unsigned i = 0; i < G[u].size(); i++)
        if (G[u][i] != fa) { 
            dfs(G[u][i], u);
            // Add comp(a)?
            if (mx[G[u][i]] + 1 > mx[u]) {
                mx2[u] = mx[u];
                mx2To[u] = mxTo[u];
                mx2Son[u] = mxSon[u];
                mx[u] = mx[G[u][i]] + 1;
                mxTo[u] = mxTo[G[u][i]];
                mxSon[u] = G[u][i];
            } else if (mx[G[u][i]] + 1 > mx2[u]) {
                mx2[u] = mx[G[u][i]] + 1;
                mx2To[u] = mxTo[G[u][i]];
                mx2Son[u] = G[u][i];
            }
        }
    if (mx[u] == 0) {
        mxSon[u] = u;
        mxTo[u] = u;
    } 
    if (mx2[u] == 0) {
        mx2Son[u] = u;
        mx2To[u] = u;
    } 
}

int ans = 0;
#define UpdAnswer(x, y, dist) do { if (x != y) ans = max(ans, max(a[x], a[y]) * (dist)); } while(0)
// #define UpdAnswer(x, y, dist) do { if (x != y) ans = max(ans, max(a[x], a[y]) * (dist)); printf("dist = %d, {%d(%d), %d(%d)}\n", dist, x, a[x], y, a[y]); } while(0)

void dfs2(int u, int fa)
{
    if (fa == 1 && u == mxSon[fa]) {
        f[u] = mx2[1] + 1;
        fTo[u] = mx2To[1];
        // printf("A - Updated %d %d\n", f[u], fTo[u]);
    } else {
        f[u] = f[fa] + 1;
        fTo[u] = fTo[fa];
        // printf("B - Updated %d %d\n", f[u], fTo[u]);
    }
    if (u == mxSon[fa]) {
        if (f[u] < mx2[u] + 1) {
            f[u] = mx2[u] + 1;
            fTo[u] = mx2To[u];
            // printf("C - Updated %d %d\n", f[u], fTo[u]);
        }
    } else {
        if (f[u] < mx[fa] + 1) {
            f[u] = mx[fa] + 1;
            fTo[u] = mxTo[fa];
            // printf("D - Updated %d %d\n", f[u], fTo[u]);
        }
    }
    // printf("$");
    UpdAnswer(u, mxTo[u], mx[u]); 
    UpdAnswer(u, fTo[u], f[u]);
    for (unsigned i = 0; i < G[u].size(); i++)
        if (G[u][i] != fa) { 
            dfs2(G[u][i], u);
            mx[u] = max(mx[u], mx[G[u][i]]);
        }
    mx[u]++;
}

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        a[i] = read<int>();
    }
    for (int i = 1, u, v; i < n; i++) {
        u = read<int>();
        v = read<int>();
        G[u].push_back(v);
        G[v].push_back(u);
    }
    dfs(1, 0);
    f[1] = mx[1];
    fTo[1] = mxTo[1];
    printf("mxSon[rt] = %d.\n", mxSon[1]);
    printf("mxTo[rt] = %d.\n", mxTo[1]);
    dfs2(1, 0);
    write(ans, 10);
    return 0;
}