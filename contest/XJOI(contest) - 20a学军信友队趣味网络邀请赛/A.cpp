/*************************************
 * @contest:      学军信友队趣味网络邀请赛.
 * @user_id:      20a-056.
 * @user_name:    赵奕.
 * @time:         2020-04-05.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;
int x = 1, y = 1;
int dx = 1, dy = 1;
int sx, sy;

int main()
{
    n = read<int>();
    if (n == 2) {
        printf("5\n1 1\n1 2\n1 3\n2 3\n2 2\n2 1\n");
        return 0;
    }
    // if (n == 3) {
    //     printf("11\n2 2\n1 3\n1 4\n2 4\n3 4\n3 3\n3 2\n3 1\n2 1\n1 1\n1 2\n2 3\n");
    //     return 0;
    // }
    write(n * (n + 1) - 1, 10);
    x = 1;
    y = n + 1;
    dx = 1;
    dy = -1;
    for (int Cnt = n * (n + 1) - 1; Cnt > 0; Cnt--) {
        printf("%d %d\n", x, y);
        x += dx;
        y += dy;
        if (x == 1 || x == n) {
            printf("%d %d\n", x, y); Cnt--;
            y += dy;
            dx = -dx;
            if (y == 1 || y == n + 1) {
                printf("%d %d\n", x, y); Cnt--;
                x += dx;
                dy = -dy;
            }
        } else if (y == 1 || y == n + 1) {
            printf("%d %d\n", x, y); Cnt--;
            x += dx;
            dy = -dy;
            if (x == 1 || x == n) {
                printf("%d %d\n", x, y); Cnt--;
                y += dy;
                dx = -dx;
            }
        }
    }
    return 0;
}