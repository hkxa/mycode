/*************************************
 * @contest:      【LGR-086】洛谷 5 月月赛 II & EZEC Round 8.
 * @author:       brealid.
 * @time:         2021-05-09.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 1e6 + 7;
int n, a[N];

signed main() {
    kin >> n;
    for (int i = 1; i <= n; ++i) {
        kin >> a[i];
    }
    if (n <= 2) {
        kout << max(a[1], a[2]) + (a[1] == a[2] && a[1]) << '\n';
        return 0;
    }
    int cost = 0, p = -1e6, h = 0;
    if (*min_element(a + 1, a + n + 1) != 0) p = 0;
    stack<pair<int, int> > s;
    for (int i = 1; i <= n; ++i) {
        a[i] -= max(h - (i - p), 0);
        if (a[i] <= 0) continue;
        int need = 0;
        if (h + (i - p) >= a[i] + max(h - (i - p), 0)) {
            // int attach = (i - (p + h - 1) + 1) / 2;
            // int ai_ForCalc = a[i] + max(h - (i - p), 0) - ((h + attach) - (i - (p + attach)));
            // need = attach + ((ai_ForCalc + 1) >> 1);
            need = ((i - p) + (a[i] + max(h - (i - p), 0) - h) + 1) / 2;
            // fprintf(stderr, "%d: need = %d\n", i, need);
        } else need = a[i] + max(h - (i - p), 0) - h;
        // } else need = (i - p) + (a[i] - 2 * (i - p));
        if (h - (i - p) < 0) {
            if (a[i] < need) {
                // fprintf(stderr, "%d: Cases_New (%d, %d)\n", i, i, a[i]);
                s.push(make_pair(p, h));
                p = i;
                h = a[i];
                cost += a[i];
                continue;
            }
        }
        cost += need;
        h += need;
        if (2 * (i - p) >= a[i]) p = min(p + need, i);
        else {
            p = i;
            if (!s.empty() && s.top().first + s.top().second > p - h) {
                int p1 = s.top().first, h1 = s.top().second;
                s.pop();
                int mov = (h + 1) / 2;
                p = p1 + mov;
                h = h1 + mov;
            }
        }
        // fprintf(stderr, "%d: p = %d, h = %d, cost = %d\n", i, p, h, cost);
    }
    kout << cost << '\n';
    return 0;
}

/*
const int N = 1e6 + 7;
int n, a[N];
set<pair<int, int> > s;

void sub(int u, int x) {
    s.erase(make_pair(a[u], u));
    if ((a[u] -= x) > 0) s.insert(make_pair(a[u], u));
}

signed main() {
    kin >> n;
    for (int i = 1; i <= n; ++i) {
        kin >> a[i];
        s.insert(make_pair(a[i], i));
    }
    int cost = 0;
    while (!s.empty()) {
        int u = s.rbegin()->second;
        cost += a[u];
        int pos = u - 1, val = a[u] - 1;
        while (pos >= 1 && val > 0) sub(pos--, val--);
        pos = u + 1, val = a[u] - 1;
        while (pos <= n && val > 0) sub(pos++, val++);
        sub(u, a[u]);
    }
    kout << cost << '\n';
    return 0;
}
*/