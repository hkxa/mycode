//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      牛客挑战赛41.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-06-19.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

const int P = 998244353;

#define MEM_ARR(arr, siz, v) { for (unsigned i = 0; i < siz + 5; i++) arr[i] = v; }

inline int Module(int w) { return w >= P ? Module(w - P) : w; }

int mul(int x) { return x; }
template<typename... Type>
int mul(int x, Type... Args) { return (int64)x * mul(Args...) % P; }

int add(int x) { return x; }
template<typename... Type>
int add(int x, Type... Args) { return Module(x + add(Args...)); }

template<typename T>
void memarr(size_t siz, int val, T Arg) { MEM_ARR(Arg, siz, val); }
template<typename T, typename... T_>
void memarr(size_t siz, int val, T FirstArg, T_... Args) {
    MEM_ARR(FirstArg, siz, val);
    memarr(siz, val, Args...);
}

// #define int int64

int a, b;

int n;
vector<pair<int, int> > ans;

signed main() {
    a = read<int>();
    b = read<int>();
    if (!a) {
        if (!b) printf("3 3\n1 2\n2 3\n1 3\n");
        else if (b == 1) printf("2 1\n1 2\n");
        else puts("-1");
        return 0;
    }
    for (int i = 0; i < a; i++) {
        ans.push_back(make_pair(i * 5 + 1, i * 5 + 2));
        ans.push_back(make_pair(i * 5 + 1, i * 5 + 3));
        ans.push_back(make_pair(i * 5 + 2, i * 5 + 3));
        ans.push_back(make_pair(i * 5 + 3, i * 5 + 4));
        ans.push_back(make_pair(i * 5 + 3, i * 5 + 5));
        ans.push_back(make_pair(i * 5 + 4, i * 5 + 5));
        if (i) {
            ans.push_back(make_pair((i - 1) * 5 + 4, i * 5 + 1));
            ans.push_back(make_pair((i - 1) * 5 + 5, i * 5 + 2));
        }
    }
    n = 5 * a;
    int Cut = n - 2;
    while (b--) {
        ans.push_back(make_pair(Cut, ++n));
    }
    printf("%d %d\n", n, ans.size());
    for (unsigned i = 0; i < ans.size(); i++) {
        printf("%d %d\n", ans[i].first, ans[i].second);
    }
    return 0;
}

// Create File Date : 2020-06-19