/*************************************
 * problem:      T99101 【SWTR-01】Ethan and Sets.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-10-03.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 
#pragma GCC optimize(2)
#pragma GCC optimize(3)
#pragma GCC optimize("Ofast")
#pragma GCC optimize("inline")
#pragma GCC optimize("-fgcse")
#pragma GCC optimize("-fgcse-lm")
#pragma GCC optimize("-fipa-sra")
#pragma GCC optimize("-ftree-pre")
#pragma GCC optimize("-ftree-vrp")
#pragma GCC optimize("-fpeephole2")
#pragma GCC optimize("-ffast-math")
#pragma GCC optimize("-fsched-spec")
#pragma GCC optimize("unroll-loops")
#pragma GCC optimize("-falign-jumps")
#pragma GCC optimize("-falign-loops")
#pragma GCC optimize("-falign-labels")
#pragma GCC optimize("-fdevirtualize")
#pragma GCC optimize("-fcaller-saves")
#pragma GCC optimize("-fcrossjumping")
#pragma GCC optimize("-fthread-jumps")
#pragma GCC optimize("-funroll-loops")
#pragma GCC optimize("-fwhole-program")
#pragma GCC optimize("-freorder-blocks")
#pragma GCC optimize("-fschedule-insns")
#pragma GCC optimize("inline-functions")
#pragma GCC optimize("-ftree-tail-merge")
#pragma GCC optimize("-fschedule-insns2")
#pragma GCC optimize("-fstrict-aliasing")
#pragma GCC optimize("-fstrict-overflow")
#pragma GCC optimize("-falign-functions")
#pragma GCC optimize("-fcse-skip-blocks")
#pragma GCC optimize("-fcse-follow-jumps")
#pragma GCC optimize("-fsched-interblock")
#pragma GCC optimize("-fpartial-inlining")
#pragma GCC optimize("no-stack-protector")
#pragma GCC optimize("-freorder-functions")
#pragma GCC optimize("-findirect-inlining")
#pragma GCC optimize("-fhoist-adjacent-loads")
#pragma GCC optimize("-frerun-cse-after-loop")
#pragma GCC optimize("inline-small-functions")
#pragma GCC optimize("-finline-small-functions")
#pragma GCC optimize("-ftree-switch-conversion")
#pragma GCC optimize("-foptimize-sibling-calls")
#pragma GCC optimize("-fexpensive-optimizations")
#pragma GCC optimize("-funsafe-loop-optimizations")
#pragma GCC optimize("inline-functions-called-once")
#pragma GCC optimize("-fdelete-null-pointer-checks")

#include <bits/stdc++.h>
#include <time.h>
using namespace std;
#define ForInVec(vectorType, vectorName, iteratorName) for (vector<vectorType>::iterator iteratorName = vectorName.begin(); iteratorName != vectorName.end(); iteratorName++)
#define ForInVI(vectorName, iteratorName) ForInVec(int, vectorName, iteratorName)
#define ForInVE(vectorName, iteratorName) ForInVec(Edge, vectorName, iteratorName)
#define MemWithNum(array, num) memset(array, num, sizeof(array))
#define Clear(array) MemWithNum(array, 0)
#define MemBint(array) MemWithNum(array, 0x3f)
#define MemInf(array) MemWithNum(array, 0x7f)
#define MemEof(array) MemWithNum(array, -1)
#define ensuref(condition) do { if (!(condition)) exit(0); } while(0)

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

struct FastIOer {
#   define createReadlnInt(type)            \
    FastIOer& operator >> (type &x)         \
    {                                       \
        x = read<type>();                   \
        return *this;                       \
    }
    createReadlnInt(int);
    createReadlnInt(long long);
#   undef createReadlnInt
} fast;

int n, m, d;
int p[1007];
bool love[1007] = {0};
int t[3007];
int num[3007];
int c[3007][1007];

struct s_counter {
    bool appeared[1007];
    long long ok, fail, magic;

    void init()
    {
        memset(appeared, 0, sizeof(bool) * (m + 3));
        ok = fail = magic = 0;
    }

    void add(int p) 
    {
        for (int i = 1; i <= num[p]; i++) {
            if (love[c[p][i]]) {
                if (!appeared[c[p][i]]) ok++;
            } else fail++;
            appeared[c[p][i]] = true;
        }
        magic += t[p];
    }
} counter;

int main()
{
    fast >> n >> m >> d;
    for (int i = 1; i <= d; i++) {
        fast >> p[i];
        love[p[i]] = true;
    }
    for (int i = 1; i <= n; i++) {
        fast >> t[i] >> num[i];
        for (int j = 1; j <= num[i]; j++) fast >> c[i][j];
    }
    long long fail = 0x3f3f3f3f, magic = -1, al, ar;
    for (int l = 1; l <= n; l++) {
        // if (clock() >= 990) break;
        counter.init();
        for (int r = l; r <= n; r++) {
            counter.add(r);
            if (counter.fail > fail) break;
            if (counter.ok == d && (counter.fail < fail || (counter.fail == fail && counter.magic > magic))) {
                fail = counter.fail;
                magic = counter.magic;
                al = l;
                ar = r;
            }
        }
    }
    if (fail != 0x3f3f3f3f) {
        write(al, 32);
        write(ar, 10);
    } else puts("-1");
    return 0;
}