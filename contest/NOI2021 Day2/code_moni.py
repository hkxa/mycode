from math import gcd

def operate(op):
    f = [0, 1]
    for p in op:
        if p == 'W':
            f[-1] += 1
        elif f[-1] == 1:
            f[-2] += 1
        else:
            f[-1] -= 1
            f.append(1)
            f.append(1)
    return f

def calc(f):
    s, g = f[-1], 1
    for i in range(len(f) - 2, -1, -1):
        s, g = g + s * f[i], s
        fac = gcd(s, g)
        s, g = s // fac, g // fac
    return s, g

def moni():
    lis = ''
    while True:
        op = input()
        if op == 'exit':
            return
        elif op[:3] == 'del':
            if len(op) < 5:
                lis = lis[:-1]
            else:
                lis = lis[:-int(op[4:])]
        else:
            lis += op
        print('%20s'%lis, '%5d/%-5d'%calc(operate(lis)), operate(lis))

moni()