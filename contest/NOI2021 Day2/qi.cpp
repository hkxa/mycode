/*************************************
 * @problem:      qi.
 * @author:       brealid.
 * @time:         2021-07-28.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 400007;
int n, m;
uint64 a1, a2;
int s[N][64], c[64];
char c_input[64];
int popcnt[16] = {0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4};
int toInt[256];

namespace ProvidedLibrary {
    uint64 myRand(uint64 &k1, uint64 &k2) {
        uint64 k3 = k1, k4 = k2;
        k1 = k4;
        k3 ^= (k3 << 23);
        k2 = k3 ^ k4 ^ (k3 >> 17) ^ (k4 >> 26);
        return k2 + k4;
    }

    void gen(int n, uint64 a1, uint64 a2) {
        for (int i = 1; i <= n; ++i)
            for (int j = 0; j < 64; ++j) {
                int s3 = (myRand(a1, a2) & (1ull << 32)) ? 8 : 0;
                int s2 = (myRand(a1, a2) & (1ull << 32)) ? 4 : 0;
                int s1 = (myRand(a1, a2) & (1ull << 32)) ? 2 : 0;
                int s0 = (myRand(a1, a2) & (1ull << 32)) ? 1 : 0;
                s[i][j] = s3 | s2 | s1 | s0;
            }
    }
}

namespace trie {
    const int SIZ = N * 32;
    int s[SIZ][16], cnt, root;
    void build(int &u, int *num, int fl) {
        if (!u) u = ++cnt;
        if (fl >= 64) return;
        build(s[u][num[fl]], num, fl + 1);
    }
    bool query(int u, int fl, int rem) {
        if (rem < 0) return false;
        if (fl >= 64) return true;
        // for (int i = 15; i >= 0; --i)
        for (int i = 0; i < 16; ++i)
            if (s[u][i] && query(s[u][i], fl + 1, rem - popcnt[c[fl] ^ i]))
                return true;
        return false;
    }
}

signed main() {
    for (int i = 0; i < 10; ++i) toInt['0' + i] = i;
    for (int i = 0; i < 6; ++i) toInt['A' + i] = i + 10;
    file_io::set_to_file("qi");
    kin >> n >> m >> a1 >> a2;
    ProvidedLibrary::gen(n, a1, a2);
    for (int i = 1; i <= n; ++i) {
        trie::build(trie::root, s[i], 0);
        // for (int j = 0; j < 64; ++j)
        //     kout << char(s[i][j] < 10 ? s[i][j] + '0' : s[i][j] - 10 + 'A');
        // kout << '\n';
    }
    for (int i = 1, k, lastans = 0; i <= m; ++i) {
        // if (i % 100 == 0) fprintf(stderr, "oh! %d\n", i);
        kin >> c_input >> k;
        for (int i = 0; i < 64; ++i) c[i] = lastans ? 15 - toInt[c_input[i]] : toInt[c_input[i]];
        lastans = trie::query(trie::root, 0, k);
        kout << char('0' | lastans) << '\n';
    }
    return 0;
}