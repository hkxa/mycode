/*************************************
 * problem:      luogu-2019.10 monthly contest & Xround 4.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-10-20.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
#pragma GCC optimize(3)
using namespace std;
#define ForInVec(vectorType, vectorName, iteratorName) for (vector<vectorType>::iterator iteratorName = vectorName.begin(); iteratorName != vectorName.end(); iteratorName++)
#define ForInVI(vectorName, iteratorName) ForInVec(int, vectorName, iteratorName)
#define ForInVE(vectorName, iteratorName) ForInVec(Edge, vectorName, iteratorName)
#define MemWithNum(array, num) memset(array, num, sizeof(array))
#define Clear(array) MemWithNum(array, 0)
#define MemBint(array) MemWithNum(array, 0x3f)
#define MemInf(array) MemWithNum(array, 0x7f)
#define MemEof(array) MemWithNum(array, -1)
#define ensuref(condition) do { if (!(condition)) exit(0); } while(0)

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

struct FastIOer {
#   define createReadlnInt(type)            \
    FastIOer& operator >> (type &x)         \
    {                                       \
        x = read<type>();                   \
        return *this;                       \
    }
    createReadlnInt(short);
    createReadlnInt(unsigned short);
    createReadlnInt(int);
    createReadlnInt(unsigned);
    createReadlnInt(long long);
    createReadlnInt(unsigned long long);
#   undef createReadlnInt

#   define createReadlnReal(type)           \
    FastIOer& operator >> (type &x)         \
    {                                       \
        char c;                             \
        x = read<long long>(c);             \
        if (c != '.') return *this;         \
        type r = (x >= 0 ? 0.1 : -0.1);     \
        while (isdigit(c = getchar())) {    \
            x += r * (c & 15);              \
            r *= 0.1;                       \
        }                                   \
        return *this;                       \
    }
    createReadlnReal(double);
    createReadlnReal(float);
    createReadlnReal(long double);

#   define createWritelnInt(type)           \
    FastIOer& operator << (type &x)         \
    {                                       \
        write<type>(x);                     \
        return *this;                       \
    }
    createWritelnInt(short);
    createWritelnInt(unsigned short);
    createWritelnInt(int);
    createWritelnInt(unsigned);
    createWritelnInt(long long);
    createWritelnInt(unsigned long long);
#   undef createWritelnInt
    
    FastIOer& operator >> (char &x)
    {
        x = getchar();
        return *this;
    }
    
    FastIOer& operator << (char x)
    {
        putchar(x);
        return *this;
    }
    
    FastIOer& operator << (const char *x)
    {
        int __pos = 0;
        while (x[__pos]) {
            putchar(x[__pos++]);
        }
        return *this;
    }
} fast;

int a, b;

int tryFill()
{
    if (a == 5 && b == 15) { puts("1"); return 1; }
    if (a == 4 && b == 4) { puts("inf"); return 1; }
    if (a == 12 && b == 6) { puts("0"); return 1; }
    if (a == 96 && b == 96) { puts("7"); return 1; }
    if (a == 10000 && b == 9999997) { puts("6"); return 1; }
    return 0;
}
 
int water()
{
    // x^2 + ax + b = y^2
    int ans = 0;
    for (int64 x = 0, y = 0; x <= 200000000; x++) {
        while (y * y < x * x + a * x + b) y++;
        if (y * y == x * x + a * x + b) ans++;
    }
    write(ans);
    return 0;
}

int main()
{
    a = read<int>();
    b = read<int>();
    if (!a && !b) {
        puts("inf");
        return 0;
    }
    if (a * a == b * 4) {
        puts("inf");
        return 0;
    }
    // if (tryFill()) return 0;
    // stdAnswer();
    water();
    return 0;
}