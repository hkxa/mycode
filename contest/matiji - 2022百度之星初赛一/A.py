fa = []

def find(u):
    global fa
    if u == fa[u]:
        return u
    else:
        fa[u] = find(fa[u])
        return fa[u]

def main():
    n = int(input())
    edges = []
    for i in range(n):
        for j, c in enumerate(map(int, input().split()[i+1:])):
            edges.append([c, i, i + j + 1])
    global fa
    fa = [i for i in range(n)]
    near = []
    for e in sorted(edges, key=lambda x:x[0]):
        u, v = find(e[1]), find(e[2])
        if u != v:
            fa[u] = fa[v]
            near.append((e[1], e[2]))
    print(n - 1)
    for e in sorted(near, key=lambda x:x[0] * n + x[1]):
        print(e[0] + 1, e[1] + 1)

for i in range(int(input())):
    main()