/*************************************
 * @problem:      QiZhiShu - 2020(夏)-TG5.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-23.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

#ifdef DEBUG
# define passing() cerr << "passing line [" << __LINE__ << "] in No." << clock() << "ms" << endl
# define debug(...) fprintf(stderr, __VA_ARGS__)
# define show(x) cerr << #x << " = " << (x) << endl
#else
# define passing() (0)
# define debug(...) (0)
# define show(x) (0)
#endif

// #define int int64

const int N = 4e5 + 17, M = 1e7 + 17;

struct Edge {
    int to, occur;
    Edge() {}
    Edge(int node, int tim) : to(node), occur(tim) {}
};

int n, m, x, q;
int64 k;
vector<Edge> G[N];
int head[N], nxt[M], to[M], edge_cnt;
#define AddE(u, v) to[++edge_cnt] = v; nxt[edge_cnt] = head[u]; head[u] = edge_cnt;
int head_rev[N], nxt_rev[M], to_rev[M], edge_cnt_rev;
#define AddE_rev(u, v) to_rev[++edge_cnt_rev] = v; nxt_rev[edge_cnt_rev] = head_rev[u]; head_rev[u] = edge_cnt_rev;

void PrepareEdge(int u, int l0, int r0) {
    // if (l0 == r0) debug("link %d -> %d\n", u + n, l0);
    if (l0 == r0) return void(G[u + n].push_back(Edge(l0, 0)));
    int mid = (l0 + r0) >> 1;
    // debug("link %d -> %d\n", u + n, (u << 1) + n);
    // debug("link %d -> %d\n", u + n, (u << 1 | 1) + n);
    G[u + n].push_back(Edge((u << 1) + n, 0));
    G[u + n].push_back(Edge((u << 1 | 1) + n, 0));
    PrepareEdge(u << 1, l0, mid);
    PrepareEdge(u << 1 | 1, mid + 1, r0);
}
void LinkEdge(int u, int from, int l, int r, int l0, int r0, int Tim) {
    if (l0 > r || r0 < l) return;
    // if (l0 >= l && r0 <= r) debug("link %d -> [%d, %d](%d)\n", from, l0, r0, u + n);
    if (l0 >= l && r0 <= r) return void(G[from].push_back(Edge(u + n, Tim)));
    int mid = (l0 + r0) >> 1;
    LinkEdge(u << 1, from, l, r, l0, mid, Tim);
    LinkEdge(u << 1 | 1, from, l, r, mid + 1, r0, Tim);
}

int ind[N], ind_rev[N];
int cirReach[N], cirReach_rev[N];
int VisableTime, dfn[N], low[N], dft;
int fam[N], siz[N], ans_siz[N], fam_cnt;
int st[N], top = 0; bool ins[N];

void Trajan(int u) {
    low[u] = dfn[u] = ++dft;
    st[++top] = u;
    ins[u] = 1;
    for (size_t i = 0; i < G[u].size(); i++) {
        if (G[u][i].occur > VisableTime) continue;
        int v = G[u][i].to;
        if (!dfn[v]) {
            Trajan(v);
            low[u] = min(low[u], low[v]);
        } else if (ins[v]) {
            low[u] = min(low[u], dfn[v]);
        }
    }
    if (low[u] == dfn[u]) {
        fam[u] = ++fam_cnt;
        ins[u] = 0;
        siz[fam_cnt] = 1;
        ans_siz[fam_cnt] = (u <= n);
        while (st[top] != u) {
            ins[st[top]] = 0;
            siz[fam_cnt]++;
            ans_siz[fam_cnt] += (st[top] <= n);
            fam[st[top--]] = fam_cnt;
        }
        top--; // pop u
    }
}

void get_couldReach(int u) {
    // debug("%s : vis %d\n", __FUNCTION__, u);
    cirReach[u] = 1;
    for (int e = head[u]; ~e; e = nxt[e]) {
        int v = to[e];
        if (!cirReach[v]) get_couldReach(v);
    }
}

void get_couldReach_rev(int u) {
    // debug("%s : vis %d\n", __FUNCTION__, u);
    cirReach_rev[u] = 1;
    for (int e = head_rev[u]; ~e; e = nxt_rev[e]) {
        int v = to_rev[e];
        if (!cirReach_rev[v]) get_couldReach_rev(v);
    }
}

void dfs(int u) {
    // debug("dfs(%d)\n", u);
    cirReach[u] = max((siz[u] > 1) ? 2 : 1, cirReach[u]);
    for (int e = head[u]; ~e; e = nxt[e]) {
        int v = to[e];
        cirReach[v] = max(cirReach[v], cirReach[u]);
        if (!--ind[v]) dfs(v);
    }
}

void dfs_rev(int u) {
    // debug("dfs_rev(%d)\n", u);
    cirReach_rev[u] = (siz[u] > 1) ? 2 : 1;
    for (int e = head_rev[u]; ~e; e = nxt_rev[e]) {
        int v = to_rev[e];
        if (!--ind_rev[v]) dfs_rev(v);
        cirReach_rev[u] = max(cirReach_rev[u], cirReach_rev[v]);
    }
}

bool check() {
    // passing(); show(VisableTime);
    memset(cirReach, 0, sizeof(cirReach));
    memset(cirReach_rev, 0, sizeof(cirReach_rev));
    memset(dfn, 0, sizeof(dfn));
    memset(low, 0, sizeof(low));
    memset(ind, 0, sizeof(ind));
    memset(ind_rev, 0, sizeof(ind_rev));
    memset(head, -1, sizeof(head));
    memset(head_rev, -1, sizeof(head_rev));
    edge_cnt = edge_cnt_rev = fam_cnt = 0;
    for (int i = 1; i <= n * 5; i++) 
        if (!dfn[i]) Trajan(i);
    // passing(); show(dft); show(fam_cnt); show(edge_cnt); show(edge_cnt_rev);
    for (int i = 1; i <= n * 5; i++) 
        for (size_t j = 0; j < G[i].size(); j++)
            if (G[i][j].occur <= VisableTime && fam[i] != fam[G[i][j].to]) {
                AddE(fam[i], fam[G[i][j].to]);
                AddE_rev(fam[G[i][j].to], fam[i]);
                // debug("%d -> %d\n", fam[i], fam[G[i][j].to]);
            }
    // passing();
    get_couldReach(fam[x]);
    // passing();
    // for (int i = 1; i <= fam_cnt; i++) 
    //     if (cirReach[i])
    //         debug("%d : exist\n", i);
    for (int i = 1; i <= fam_cnt; i++) 
        if (cirReach[i]) 
            for (int e = head[i]; ~e; e = nxt[e]) 
                ind[to[e]]++;
    get_couldReach_rev(fam[x]);
    // passing();
    // for (int i = 1; i <= fam_cnt; i++) 
    //     if (cirReach_rev[i])
    //         debug("%d : (rev) exist\n", i);
    for (int i = 1; i <= fam_cnt; i++) 
        if (cirReach_rev[i]) 
            for (int e = head_rev[i]; ~e; e = nxt_rev[e]) 
                ind_rev[to_rev[e]]++;
    // for (int i = 1; i <= fam_cnt; i++) 
    //     debug("family %d : siz = %d, ind = %d, ind_rev = %d\n", i, siz[i], ind[i], ind_rev[i]);
    memset(cirReach, 0, sizeof(cirReach));
    memset(cirReach_rev, 0, sizeof(cirReach_rev));
    cirReach[fam[x]] = cirReach_rev[fam[x]] = (siz[fam[x]] > 1) ? 2 : 1;
    dfs(fam[x]);
    // passing();
    dfs_rev(fam[x]);
    // passing();
    int aa(0), bb(0), cc(0), dd(0), ee(0), ff(0);
    for (int i = 1; i <= fam_cnt; i++) {
        // show(i); show(cirReach[i]); show(cirReach_rev[i]);
        switch (cirReach[i]) {
            case 0 : aa += ans_siz[i]; break;
            case 1 : bb += ans_siz[i]; break;
            case 2 : cc += ans_siz[i]; break;
        }
        switch (cirReach_rev[i]) {
            case 0 : dd += ans_siz[i]; break;
            case 1 : ee += ans_siz[i]; break;
            case 2 : ff += ans_siz[i]; break;
        }
    }
    // passing(); show(aa); show(bb); show(cc); show(dd); show(ee); show(ff);
    int64 ok = (int64)n * n - (int64)bb * ff - (int64)cc * ee - (int64)cc * ff;
    // passing(); show(ok); show(k); show((ok >= k));
    // assert(edge_cnt <= 3e7);
    // assert(edge_cnt_rev <= 3e7);
    return ok >= k;
}

signed main() {
    read >> n >> m >> x >> q >> k;
    PrepareEdge(1, 1, n);
    for (int i = 1, u, v; i <= m; i++) {
        read >> u >> v;
        G[u].push_back(Edge(v, 0));
    }
    // passing();
    for (int i = 1, u, l, r; i <= q; i++) {
        read >> u >> l >> r;
        LinkEdge(1, u, l, r, 1, n, i);
    }
    int l = 0, r = q, ans = -1;
    while (l <= r) {
        VisableTime = (l + r) >> 1;
        if (check()) l = VisableTime + 1, ans = VisableTime;
        else r = VisableTime - 1;
    }
    write << ans + 1 << '\n';
    return 0;
}