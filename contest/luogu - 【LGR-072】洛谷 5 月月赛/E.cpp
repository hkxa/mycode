//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      T132641 中子衰变.
 * @user_id:      63720.
 * @user_name:    brealid.
 * @time:         2020-05-30.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
       return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
       return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

// #define int int64

int n, task;
int status[2048 + 7];

inline void Read() {
    int pl = read<int>();
    status[pl] = read<int>();
}

inline void order(int cmd) {
    write(cmd, 10);
    fflush(stdout);
    if (cmd == 1) Read();
}

inline void output(int pl, int type) {
    write(pl, 32);
    write(type, 10);
    status[pl] = type;
    fflush(stdout);
}

inline bool end() {
    for (int i = 1; i <= n; i++) {
        if (status[i] || (status[i - 1] && status[i + 1] && status[i - 1] != status[i + 1])) continue;
        return false;
    }
    return true;
}

signed main() {
    n = read<int>();
    task = read<int>();
    if (task == 3 || (task == 4 && n % 2 == 0)) {
        order(1);
        // while (!end()) {
        //     for (int i = 1; i <= n; i++) {
        //         if (status[i] || (status[i - 1] && status[i + 1] && status[i - 1] != status[i + 1])) continue;
        //         output(i, status[i - 1]);
        //         break;
        //     }
        //     if (!end()) Read();
        // }
        for (int i = 2; i <= n; i += 2)
            output(i, 1);
    }
    if (task == 4 && n % 2) {
        if (n == 1) {
            order(1);
            // output(1, 1);
        } else if (n == 3) {
            order(1);
            output(3, -1);
        } else {
            order(1);
            // output(3, -1);
            for (int i = 3; i <= n; i += 2)
                output(i, -1);
        }
    }
    if (task == 1) {
        if (n == 4) {
            order(1);
            if (status[1]) {
                output(3, status[1]);
                Read();
                if (status[2] == status[1]) output(4, status[1]);
                else output(2, status[1]);
            } else if (status[2]) {
                output(3, status[2]);
                Read();
                if (status[1] == status[2]) output(4, status[2]);
                else output(1, status[2]);
            } else if (status[3]) {
                output(2, status[3]);
                Read();
                if (status[1] == status[3]) output(4, status[3]);
                else output(1, status[3]);
            } else if (status[4]) {
                output(2, status[4]);
                Read();
                if (status[1] == status[4]) output(3, status[4]);
                else output(1, status[4]);
            }
        } else if (n == 3) {
            order(1);
            // output(1, 1);
            if (status[1]) {
                output(3, -status[1]);
            } else if (status[2]) {
                output(1, status[2]);
            } else if (status[3]) {
                output(1, -status[3]);
            } 
        } else if (n == 2) {
            order(1);
            // output(1, 1);
            if (status[1]) {
                output(2, status[1]);
            } else if (status[2]) {
                output(1, status[2]);
            }
        }
    }
    return 0;
}

// Create File Date : 2020-05-30