/*************************************
 * @problem:      island.
 * @author:       brealid.
 * @time:         2021-03-27.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 1e5 + 7, K = 500 + 7, P = 1e9 + 7, inv2 = (P + 1) >> 1;

int n, q, a[N], b[N];
int l[N], r[N], c[N], d[N];

int ch[N * 24][2], tot[N * 24], root[N], cnt;

void insert(int u, int v, int dep, int num) {
    if (dep < 0) return;
    tot[u] = tot[v] + 1;
    if (num >> dep & 1) {
        ch[u][0] = ch[v][0];
        insert(ch[u][1] = ++cnt, ch[v][1], dep - 1, num);
    } else {
        ch[u][1] = ch[v][1];
        insert(ch[u][0] = ++cnt, ch[v][0], dep - 1, num);
    }
}

int get(int u, int v, int dep, int c, int d) {
    if (tot[u] == tot[v]) return 0;
    if ((c | ((1 << (dep + 1)) - 1)) <= d) return tot[u] - tot[v];
    if ((c & ~((1 << (dep + 1)) - 1)) > d) return 0;
    return get(ch[u][0], ch[v][0], dep - 1, c, d) + get(ch[u][1], ch[v][1], dep - 1, c ^ (1 << dep), d);
}

int Brute() {
    for (int i = 1, l, r, c, d, cnt; i <= q; ++i) {
        kin >> l >> r >> c >> d;
        cnt = 0;
        for (int p = l; p <= r; ++p)
            if ((a[p] ^ c) <= min(b[p], d)) ++cnt;
        kout << cnt << '\n';
    }
    return 0;
}

signed main() {
    file_io::set_to_file("island");
    kin >> n >> q;
    for (int i = 1; i <= n; ++i) kin >> a[i] >> b[i];
    if (n <= 5000 && q <= 5000) return Brute();
    for (int i = 1; i <= q; ++i) kin >> l[i] >> r[i] >> c[i] >> d[i];
    if (*max_element(d + 1, d + q + 1) < *min_element(b + 1, b + n + 1)) {
        for (int i = 1; i <= n; ++i)
            insert(root[i] = ++cnt, root[i - 1], 23, a[i]);
        for (int i = 1; i <= q; ++i)
            kout << get(root[r[i]], root[l[i] - 1], 23, c[i], d[i]) << '\n';
    }
    return 0;
}