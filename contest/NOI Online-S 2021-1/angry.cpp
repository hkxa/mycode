/*************************************
 * @problem:      angry.
 * @author:       brealid.
 * @time:         2021-03-27.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 5e5 + 7, K = 500 + 7, P = 1e9 + 7, inv2 = (P + 1) >> 1;

int64 fpow(int64 x, int n) {
    int64 r(1);
    while (n) {
        if (n & 1) r = r * x % P;
        x = x * x % P;
        n >>= 1;
    }
    return r;
}

char n[N];
int len, a1[K], a2[K], k;
int64 C[K][K];
// int64 w1[K], w0[K];
int64 w[K];
int64 ans = 0;

#define AddToAns(x_expression) (ans = (ans + (x_expression)) % P)

void get_C() {
    for (int i = 0; i <= k; ++i) {
        C[i][0] = 1;
        for (int j = 1; j <= i; ++j) C[i][j] = (C[i - 1][j - 1] + C[i - 1][j]) % P;
    }
}

// void swap_w1_w0() {
//     for (int i = 0; i < k; ++i) swap(w1[i], w0[i]);
// }

int64 f(int64 x) {
    int64 x_now = 1, ret(0);
    for (int i = 0; i < k; ++i) {
        ret = (ret + x_now * a1[i]) % P;
        x_now = x_now * x % P;
    }
    return ret;
}

int Brute() {
    int num = 0;
    for (int i = 1; i <= len; ++i) if (n[i] == '1') num |= 1 << (len - i);
    bool test[1050000] = {0};
    // test[1] = test[2] = 1;
    int64 ans = 0;
    for (int b = 1; b <= len; ++b) {
        int pow2b = 1 << b, tp = 1 << (b - 1);
        test[tp] = 1;
        for (int i = tp; i < min(pow2b, num); ++i)
            if (test[i]) ans = (ans + f(i)) % P;
            else test[i + pow2b] = true;
    }
    kout << ans << '\n';
    return 0;
}

signed main() {
    file_io::set_to_file("angry");
    kin >> (n + 1) >> k;
    len = strlen(n + 1);
    get_C();
    for (int i = 0; i < k; ++i) kin >> a1[i];
    if (len <= 20) return Brute();
    // int64 total_n = 0;
    for (int i = 1; i <= len; ++i) {
        int64 pow2x = fpow(2, len - i), pow2x1 = pow2x * inv2 % P;
        for (int t = 0; t < k; ++t) {
            AddToAns(w[t] * pow2x % P);
            for (int s = 0; s < t; ++s) w[s] = (w[s] + w[t] * C[t][s]) % P;
        }
        // printf("fpow(pow2x, k - 1) = %lld\n", fpow(pow2x, k - 1));
        if (n[i] == '1') {
            // total_n = (total_n + pow2x) % P;
            AddToAns(pow2x1 * fpow(pow2x1, k - 1) * a1[k - 1]);
            for (int s = 0; s < k - 1; ++s) w[s] = (w[s] + C[k - 1][s] * fpow(pow2x1, s)) % P;
        }
        // printf("ans = %lld\n", ans);
    }
    kout << ans << '\n';
    return 0;
}