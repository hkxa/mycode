/*************************************
 * @contest:      【LGR-067】洛谷 1 月月赛 II & CSGRound 3.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-01-28.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define P 998244353

int n, T;
struct Matrix {
    bitset<507> v[507];
    Matrix() {}
} base, ans;

inline void mul(Matrix &m, const Matrix &l)  
{
    const Matrix tmp;
    memset(tmp.v, 0, sizeof(tmp.v));
    for (int i = 1; i <= n; i++) 
        for (int k = 1; k <= n; k++) 
            if (m.v[i][k]) 
                tmp.v[i] |= l.v[k];
    m = tmp;
}

inline void fpow(int T)
{
    for (int i = 1; i <= n; i++) ans.v[i].set(i);
    while (T) {
        if (T & 1) mul(ans, base);
        mul(base, base);
        T >>= 1;
    }
}

int64 formal[507];

int main()
{
    n = read<int>();
    T = read<int>();
    for (register int i = 1, p, a; i <= n; i++) {
        formal[i] = (1 - read<int>() + P) % P;
        p = read<int>();
        while (p--) {
            a = read<int>();
            base.v[i][a] = 1;
        }
    } 
    if (!T) {
        int64 sp_Ans = 0;
        for (int i = 1; i <= n; i++) {
            sp_Ans = (sp_Ans + 1 - formal[i] + P) % P;
        } 
        write(sp_Ans, 10);
        return 0;
    }
    fpow(T);
    int64 result = 0, tmp;
    for (register int i = 1; i <= n; i++) {
        tmp = 1;
        for (register int j = 1; j <= n; j++) {
            if (ans.v[i][j]) tmp = (tmp * formal[j]) % P;
        }
        result = (result + 1 - tmp + P) % P;
    }
    write(result, 10);
    return 0;
}