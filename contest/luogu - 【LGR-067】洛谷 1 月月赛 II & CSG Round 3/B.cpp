/*************************************
 * @contest:      【LGR-067】洛谷 1 月月赛 II & CSGRound 3.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-01-28.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;
int a[1000007], sum;
int has[10];

bool CHK(int toGet)
{
    for (int i = 1; i <= n; i++) has[a[i] % 10]++;
    for (int i = 0; i < 10; i++) {
        if (has[i] >= 2 && (i * 2) % 10 == toGet) return true;
        for (int j = i + 1; j < 10; j++) {
            if (has[i] && has[j] && (i + j) % 10 == toGet) return true;
        }
    }
    return false;
}

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) sum += (a[i] = read<int>());
    if (!CHK(sum % 10)) puts("0");
    else write(sum % 10 == 0 ? 10 : sum % 10, 10);
    return 0;
}