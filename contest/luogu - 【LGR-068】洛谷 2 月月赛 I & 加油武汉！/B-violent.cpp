/*************************************
 * @contest:      【LGR-068】洛谷 2 月月赛 I & 加油武汉！.
 * @user_id:      63720.
 * @user_name:    Jomoo.
 * @time:         2020-02-07.
 * @language:     C++.
 * @upload_place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline Int read(char &c)       
{
    Int flag = 1;
    c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
    return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

#define P 998244353 

int n, a[1007];

void solve(int k)
{
    int maxx, ans, tot = 0, test = 0;
    sort(a + 1, a + n + 1);
    do {
        maxx = ans = 0;
        for (int i = 1; i <= k; i++) maxx = max(maxx, a[i]);
        for (int i = k + 1; i <= n; i++) 
            if (a[i] > maxx || i == n) {
                ans = a[i];
                break;
            }
        tot += ans;
        test += maxx;
    } while (next_permutation(a + 1, a + n + 1));
    printf("k = %d : %d. (test = %d)\n", k, tot, test);
}

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) a[i] = read<int>();
    for (int i = 1; i <= n; i++) {
        solve(i);
    }

    /*
    9 7 2
    0 15 3
    1 14 3.5
    2 12 4
    3 9  4.5
    4 5  5
    5 0  0
    */

    /*
    4 12 16 (n - 1)
    (n - 1) (n - 2)
    15 * 24(4 * 3 * 2 * 1)
    54
    42
    12
    27 23 18 12
    37 32 25 16 (diff +10 +9 +7 +4)
    47 41 32 20
    ...
    54 46 36 24 (diff +7 +5 +4 +4)
    61 51 40 28
    *n!    average_max = (1*0 2*1 3*2 4*3 5*4) * 3!;
    360 480 540 576 600
    240 120 60  24  0
    3   4   4.5 4.8 5

    2520 3360  3780 4032 4200  4320
    1800 960   540  288  120   0
    3.5  4+2/3 5.25 5.6  5+5/6 6
    5/2  4/3   3/4  2/5  1/6   0

    468 456 414 360
    132 144 186 240 (600 - *this)
       12  42  54
    48 18 6
    8 3 1

    3360 3360 3150 2856 2520 0
        0   210  294  336
         210   84   42
            126  42
               84

               
    3360 3360 3150 2856 2520 0
    2520 3360 3780 4032 4200 4320
    */
    return 0;
}