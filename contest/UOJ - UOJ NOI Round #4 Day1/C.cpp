// UnAc 10pts

//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      UOJ NOI Round #4 Day1.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-12.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

namespace against_cpp11 {
    const int N = 8 + 3, Max_V = 65000 + 7, P = 998244353;
    int n, m, Q, mxv;
    struct edge {
        int v, w, cnt;
        edge() {}
        edge(int V, int W) : v(V), w(W) {}
        edge(int V, int W, int CNT) : v(V), w(W), cnt(CNT) {}
        bool operator < (const edge &b) const {
            return (v ^ b.v) ? (v < b.v) : (w < b.w);
        }
    };
    vector<edge> G[N];
    map<edge, int> adj[N];
    map<edge, int>::iterator it;
    int w[N][N][Max_V];
    int dfs(int u, int z, int lim) {
        int &ans = w[u][z][lim];
        if (~ans) return ans;
        if (!lim) return 0;
        ans = 0;
        for (size_t i = 0; i < G[u].size(); i++) {
            int &w = G[u][i].w;
            if (w <= lim) ans = (ans + (int64)dfs(G[u][i].v, z, lim - w) * G[u][i].cnt) % P;
        }
        return ans;
    }
    // O(n * m * maxV)
    signed main() {
        memset(w, -1, sizeof(w));
        read >> n >> m >> Q >> mxv;
        for (int i = 1, u, v, w; i <= m; i++) {
            read >> u >> v >> w;
            adj[u][edge(v, w)]++;
        }
        for (int i = 1; i <= n; i++) {
            w[i][i][0] = 1;
            for (it = adj[i].begin(); it != adj[i].end(); it++)
                G[i].push_back(edge(it->first.v, it->first.w, it->second));
        }
        for (int i = 1, x, y, lim; i <= Q; i++) {
            read >> x >> y >> lim;
            write << dfs(x, y, lim) << '\n';
        }
        return 0;
    }
}

signed main() { return against_cpp11::main(); }