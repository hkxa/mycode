//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      UOJ NOI Round #4 Day1.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-08-12.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

namespace against_cpp11 {
    const int N = 60000 + 7, K = 8 + 3;
    int n, k;
    int xor_sum[N];
    int f[N][K];
    namespace hello_brute_force {
        void do_function() {
            for (int i = 1; i <= n; i++) {
                for (int j = 1; j < i; j++) {
                    int *to = f[i], *from = f[j], value = xor_sum[i] ^ xor_sum[j];
                    to[2] = min(to[2], from[1] + value);
                    to[3] = min(to[3], from[2] + value);
                    to[4] = min(to[4], from[3] + value);
                    to[5] = min(to[5], from[4] + value);
                    to[6] = min(to[6], from[5] + value);
                    to[7] = min(to[7], from[6] + value);
                    to[8] = min(to[8], from[7] + value);
                }
            }
            for (int i = k; i <= n; i++)
                write << f[i][k] << " \n"[i == n];
        }
    }
    namespace trie {
        int root;
        int mi[N * 21], ch[N * 21][2], cnt;
        int max_log;
        void init() {
            memset(mi, 0x3f, sizeof(mi));
            root = cnt = 0;
        }
        void clear() {
            while (cnt) {
                ch[cnt][0] = ch[cnt][1] = 0;
                mi[cnt--] = 0x3f3f3f3f;
            }
            root = 0;
        }
        void insert(int &u, int num, int value, int k = max_log) {
            if (!u) u = ++cnt;
            mi[u] = min(mi[u], value);
            if (k < 0) return;
            int p = (num >> k) & 1;
            insert(ch[u][p], num, value, k - 1);
        }
        void insert(int number, int value) {
            insert(root, number, value);
        }
        int ans;
        void search(int u, int num, int extra, int k = max_log) {
            if (!u || mi[u] + extra >= ans) return;
            if (k < 0) return void(ans = mi[u] + extra);
            int p = (num >> k) & 1;
            if (mi[ch[u][p]] < mi[ch[u][p ^ 1]] + (1 << k)) {
                search(ch[u][p], num, extra, k - 1);
                search(ch[u][p ^ 1], num, extra | (1 << k), k - 1);
            } else {
                search(ch[u][p ^ 1], num, extra | (1 << k), k - 1);
                search(ch[u][p], num, extra, k - 1);
            }
        }
        int search(int number) {
            ans = 1 << 19;
            search(root, number, 0);
            return ans;
        }
    }
    signed main() {
        memset(f, 0x3f, sizeof(f));
        read >> n >> k;
        for (int i = 1, x; i <= n; i++) {
            read >> x;
            xor_sum[i] = xor_sum[i - 1] ^ x;
            f[i][1] = xor_sum[i];
            trie::max_log = max(trie::max_log, xor_sum[i]);
        }
        trie::max_log = log2(trie::max_log) + 1;
        if (n <= 5000) {
            hello_brute_force::do_function();
            return 0;
        }
        trie::init();
        for (int i = 2; i <= k; i++) {
            trie::clear();
            for (int j = i; j <= n; j++) {
                trie::insert(xor_sum[j - 1], f[j - 1][i - 1]);
                f[j][i] = trie::search(xor_sum[j]);
            }
        }
        for (int i = k; i <= n; i++)
            write << f[i][k] << " \n"[i == n];
        return 0;
    }
}

signed main() { return against_cpp11::main(); }