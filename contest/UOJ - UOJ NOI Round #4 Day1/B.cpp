#include "explore.h"
#include <bits/stdc++.h>
typedef unsigned long long uint64;
using std::vector;

inline int min(int a, int b) { return a < b ? a : b; }
inline int max(int a, int b) { return a > b ? a : b; }

void Solve(int n, int m) {
    // printf("Awwawa! Dis plablem yis ratten buy tEMMIE!\nCorrect\n");
    // exit(0);
    int reported = 0;
    vector<uint64> val, res;
    vector<int> S;
    val.resize(n);
    res.resize(n);
    S.resize(m);
    for (int i = 1; i <= m; i++) S[i - 1] = i;
    for (int l = 1, r; l <= n; l = r + 1) {
        r = min(l + 63, n);
        for (int i = l; i <= r; i++)
            val[i - 1] = (1ull << (i - l));
        res = Query(val, S);
        for (int i = l; i <= r; i++)
            val[i - 1] = 0;
        for (int i = 1; i < r; i++) {
            uint64 &t = res[i - 1];
            if (t) for (int j = max(l, i + 1); j <= r; j++)
                if (t & (1ull << (j - l))) {
                    // fprintf(stderr, "Report Edge <%d, %d>\n", i, j);
                    Report(i, j);
                    reported++;
                }
        }
        if (reported >= m) break;
    }
}
