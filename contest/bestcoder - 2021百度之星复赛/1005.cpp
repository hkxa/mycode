/*************************************
 * @contest:      bestcoder - 2021百度之星复赛.
 * @author:       brealid | hkxadpall.
 * @time:         2021-08-21.
*************************************/
#pragma GCC optimize(2)
#pragma GCC optimize(3)
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int P = 1e9 + 7;

int T, n, x;
int p[10], cp, yp[1024];
int t[10], ct, yt[1024];
int lowest[1024];

int ans;
void solve(int sp, int st, int dir, int x) {
    if (!sp && !st) {
        ans = min(ans, x);
        return;
    }
    if (dir == 0) {
        if (!st) {
            // for (int u = sp; u; u ^= u & -u) if ((x += p[lowest[u]]) >= P) x -= P;
            ans = min(ans, (x + yp[sp]) % P);
            return;
        }
        for (int use = sp; use; use = (use - 1) & sp) {
            // int nxt = x;
            // // for (int i = 0; i < cp; ++i)
            // //     if (use >> i & 1) if ((nxt += p[i]) >= P) nxt -= P;
            // for (int u = use; u; u ^= u & -u) if ((nxt += p[lowest[u]]) >= P) nxt -= P;
            solve(sp ^ use, st, 1, (x + yp[use]) % P);
            // if (!ans) return 0;
        }
    } else {
        if (!sp) {
            // for (int u = st; u; u ^= u & -u) x = (int64)x * t[lowest[u]] % P;
            ans = min(ans, int((int64)x * yt[st] % P));
            return;
        }
        for (int use = st; use; use = (use - 1) & st) {
            // int nxt = x;
            // // for (int i = 0; i < ct; ++i)
            // //     if (use >> i & 1) nxt = (int64)nxt * t[i] % P;
            // for (int u = use; u; u ^= u & -u) nxt = (int64)nxt * t[lowest[u]] % P;
            solve(sp, st ^ use, 0, (int64)x * yt[use] % P);
            // if (!ans) return 0;
        }
    }
}

// int solve(int sp, int st, int dir, int x) {
//     if (!sp && !st) return x;
//     int ans = P;
//     if (dir == 0) {
//         if (!st) {
//             for (int u = sp, bit; u;) {
//                 bit = lowest[u];
//                 if ((x += p[bit]) >= P) x -= P;
//                 u ^= (1 << bit);
//             }
//             return x;
//         }
//         for (int use = sp; use; use = (use - 1) & sp) {
//             int nxt = x;
//             // for (int i = 0; i < cp; ++i)
//             //     if (use >> i & 1) if ((nxt += p[i]) >= P) nxt -= P;
//             for (int u = use, bit; u;) {
//                 bit = lowest[u];
//                 if ((nxt += p[bit]) >= P) nxt -= P;
//                 u ^= (1 << bit);
//             }
//             ans = min(ans, solve(sp ^ use, st, 1, nxt));
//             // if (!ans) return 0;
//         }
//     } else {
//         if (!st) {
//             for (int u = sp, bit; u;) {
//                 bit = lowest[u];
//                 x = (int64)x * t[bit] % P;
//                 u ^= (1 << bit);
//             }
//             return x;
//         }
//         for (int use = st; use; use = (use - 1) & st) {
//             int nxt = x;
//             // for (int i = 0; i < ct; ++i)
//             //     if (use >> i & 1) nxt = (int64)nxt * t[i] % P;
//             for (int u = use, bit; u;) {
//                 bit = lowest[u];
//                 nxt = (int64)nxt * t[bit] % P;
//                 u ^= (1 << bit);
//             }
//             ans = min(ans, solve(sp, st ^ use, 0, nxt));
//             // if (!ans) return 0;
//         }
//     }
//     return ans;
// }

/*
5880
40458
6743
4360
*/

// int f[1024][1024][2];
// int64 solve_NotDfs() {
//     int maskp = (1 << cp) - 1, maskt = (1 << ct) - 1;
//     for (int i = 1; i < maskp; ++i)
// }

signed main() {
    for (int p = 0, step = 2; p < 10; ++p, step <<= 1)
        for (int i = step >> 1; i < 1024; i += step)
            lowest[i] = p;
    yt[0] = 1;
    for (kin >> T; T--; ) {
        kin >> n >> x;
        cp = ct = 0;
        for (int i = 0; i < 10; ++i)
            if (kin.get<char>() == '+') kin >> p[cp++];
            else kin >> t[ct++];
        int maskp = (1 << cp) - 1, maskt = (1 << ct) - 1;
        for (int i = 1; i <= maskp; ++i)
            yp[i] = (yp[i ^ (i & -i)] + p[lowest[i]]) % P;
        for (int i = 1; i <= maskt; ++i)
            yt[i] = ((int64)yt[i ^ (i & -i)] * t[lowest[i]]) % P;
        // kout << min(solve(maskp, maskt, 0, x), solve(maskp, maskt, 1, x)) << '\n';
        ans = P;
        solve(maskp, maskt, 0, x);
        solve(maskp, maskt, 1, x);
        kout << ans << '\n';
    }
    // fprintf(stderr, "*** Time Used ***\n"
    //                 "*    %d ms    *\n"
    //                 "*****************\n", clock());
    return 0;
}