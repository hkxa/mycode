/*************************************
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-02.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

double get(int a, int b)
{
    if (a == 0) return b;
    else if (b == 0) return a;
    else if (a == b) return get(a, b - 1);
    else return (get(a - 1, b) * a + get(a, b - 1) * b + max(a, b)) / (a + b);
}

int main()
{
    int ssr = read<int>(), normal = read<int>();
    // printf("%.2lf", get(ssr, normal));
    printf("%d.00", max(ssr, normal));
    return 0;
}