// luogu-judger-enable-o2
/*************************************
 * problem:      赛题 #B: T30204 偷上网 | 满分: 1000分.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2018-05-19.
 * mode:         C++.
 * upload place: Luogu.
*************************************/
#include <stdio.h>
#include <algorithm>
#include <string.h>
#include <iostream>
#include <math.h>
#define ll long long
#define ull unsinged long long
#define rull register unsigned long long
#define ri register int
#define rc register char
#define max(a, b) ((a) > (b) ? (a) : (b)) 
#define min(a, b) ((a) < (b) ? (a) : (b)) 
#define Char_Int(a) ((a) - '0')
#define Int_Char(a) ((a) + '0')
#define NeverZero(a) ((a) < 0 ? 0 : (a))
#define abs(a) ((a) < 0 ? -(a) : (a))
//#define print_end(a) {printf("%d", a); return 0;}
//#define For(i, a, b) for (int i = (a); i < (b); i++)
//#define IfUn(a, b) if (a != b)
//#define Day 365
//#define MOD 
#define MAXN 15000
#define MAXM 40001
//#define _HALF_INT_MAX 1073741823
//#define eof
//#define accept
//#define DEBUG
using namespace std;

int readint();
void writeint(int); 

int main()
{
	readint();
    int n = readint();
    int r = rand() % 10000000;
    double ra = r / 10000000.0 * n;
    printf("%.3lf ", ra);
    r = rand() % 10000000;
     ra = r / 10000000.0 * n;
    printf("%.3lf ", ra);
    return 0;
} 

inline int readint()       
{
    char c = getchar();
    while (c > '9' || c < '0') c = getchar();
    int init = Char_Int(c);
    while ((c = getchar()) <= '9' && c >= '0') init = (init << 3) + (init << 1) + Char_Int(c);
    return init;
}

inline void writeint(int x)
{
    if (x < 0) putchar('-'), x = -x;
    if (x > 9) writeint(x / 10);
    putchar(Int_Char(x % 10));
}
