// luogu-judger-enable-o2
/*************************************
 * problem:      ���� #C: T28881 ճ���� | ����: 1500��.
 * user ID:      63720.
 * user name:    �����Ű�.
 * time:         2018-05-19.
 * mode:         C++.
 * upload place: Luogu.
*************************************/
#include <stdio.h>
#include <algorithm>
#include <string.h>
#include <iostream>
#include <math.h>
#define ll long long
#define ull unsinged long long
#define rull register unsigned long long
#define ri register int
#define rc register char
#define max(a, b) ((a) > (b) ? (a) : (b)) 
#define min(a, b) ((a) < (b) ? (a) : (b)) 
#define Char_Int(a) ((a) - '0')
#define Int_Char(a) ((a) + '0')
#define NeverZero(a) ((a) < 0 ? 0 : (a))
#define abs(a) ((a) < 0 ? -(a) : (a))
//#define print_end(a) {printf("%d", a); return 0;}
//#define For(i, a, b) for (int i = (a); i < (b); i++)
//#define IfUn(a, b) if (a != b)
//#define Day 365
//#define MOD 
#define MAXN 15000
#define MAXM 40001
//#define _HALF_INT_MAX 1073741823
//#define eof
//#define accept
//#define DEBUG
using namespace std;

int readint();
void writeint(int); 

int main()
{
    int n = readint(), m = readint(), k = readint();
	int p[1001] = {0}, s[1000001][2] = {0}, map[1001][1001] = {0};
	bool st/*stick*/[1001] = {0}, vis[1001][1001] = {0};
	long long sum = 0;
	for (int i = 0; i < (n * m >> 1); i++)
		p[i] = readint();
	for (int i = 0; i < k; i++)
		s[i][0] = readint() - 1, s[i][1] = readint() - 1;
	for (int i = 0; i < n; i++)
		for (int j = 0; j < m; j++)
			map[i][j] = readint();
	for (int i = 0; i < k; i++) {
		int x = s[i][0], y = s[i][1];
		if (vis[x][y]) continue;
		if (!map[x][y]) {
		    //printf("[*3* | {x = %d ~ y = %d}]", x, y);
			printf("GG");
			return 0;
		}
		//printf("[*1*]");printf("[*2* | $%d$]", map[x][y]);
		int num = map[x][y] - 1;
		if (st[num]) continue;
		if (x && map[x - 1][y] == map[x][y]) {
			int once = 0;
			if (x - 1 && !st[map[x - 2][y] - 1] && map[x - 2][y])  
				once += p[map[x - 2][y] - 1];
			if (x + 1 != n && !st[map[x + 1][y] - 1] && map[x + 1][y])  
				once += p[map[x + 1][y] - 1];
			int twice = p[num];
			if (once > twice) {
				sum += twice;
				st[num] = 1;
			}
			else {
				sum += once;
				if (x - 1 && map[x - 2][y]) st[map[x - 2][y] - 1] = 1;
				if (x + 1 != n && map[x + 1][y]) st[map[x + 1][y] - 1] = 1;
			}
		}
	} 
	printf("%lld", sum);
    return 0;
} 

inline int readint()       
{
    char c = getchar();
    while (c > '9' || c < '0') c = getchar();
    int init = Char_Int(c);
    while ((c = getchar()) <= '9' && c >= '0') init = (init << 3) + (init << 1) + Char_Int(c);
    return init;
}

inline void writeint(int x)
{
    if (x < 0) putchar('-'), x = -x;
    if (x > 9) writeint(x / 10);
    putchar(Int_Char(x % 10));
}
