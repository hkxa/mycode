/*************************************
 * problem:      P1077 �ڻ�.
 * user ID:      63720.
 * user name:    �����Ű�.
 * time:         2018-06-09.
 * mode:         C++.
 * upload place: Luogu.
*************************************/
#include <stdio.h>
#include <algorithm>
#include <string.h>
#include <iostream>
#include <math.h>
#include <queue>
#include <vector>
#define p_int_2 pair<int, int>
#define ll long long
#define max(a, b) ((a) > (b) ? (a) : (b)) 
#define min(a, b) ((a) < (b) ? (a) : (b)) 
#define MAX 100001
#define MAX4 400010
#define Char_Int(a) ((a) - '0')
#define Int_Char(a) ((a) + '0')
using namespace std;

ll readint();
void writeint(int); 

//#define DUIPAI
//#define BIGOUT
//#define DEBUG

void init_bool_1(bool *&a, int n)
{
    a = new bool[n + 1];
    for (int i = 0; i <= n; i++) 
        a[i] = false;
}

void mem_bool_1(bool *&a, int n)
{
    for (int i = 0; i <= n; i++) 
        a[i] = false;
}

void init_int_1(int *&a, int n)
{
    a = new int[n + 1];
    for (int i = 0; i <= n; i++) 
        a[i] = 0;
}

void init_bool_2(bool **&a, int n)
{
    a = new bool*[n + 1];
    for (int i = 0; i <= n; i++) {
        a[i] = new bool[n + 1];
        for (int j = 0; j <= n; j++) {
            a[i][j] = 0;
        }
    }
}

struct STATION {
    int si;
    int *s;
    void init()
    {
        s = new int[si + 2];
    }
    void read(bool *&vis, int n)
    {
        si = readint();
        init();
        mem_bool_1(vis, n + 1);
        for (int i = 1; i <= si; i++) {
            s[i] = readint();
            vis[s[i]] = true;
        }
    }
};

bool *vis, **map;
int *inr;
long long ans;
STATION *station;
vector <int> G[1010];
queue <p_int_2> q;

/*
9 3 
4 1 3 5 6 
3 3 5 6 
3 1 5 9 
*/

int Do()
{
	const int mod = 1000007;
    int n = readint(), m = readint();
    int dp[101][101] = {0};
    memset(dp, 0, sizeof(dp));
    
    for (int i = 0; i <= n; i++)
        dp[i][0] = 1;
        
    for (int i = 1; i <= n; i++)
    {
        int t = readint();
        for (int j = 0; j <= t; j++) {
        	for (int k = 0; k <= m - j; k++)
            {
                if (!j && !k)
                    continue;
                dp[i][j + k] = (dp[i][j + k] + dp[i - 1][k]) % mod;
            }
		}  
    }
    printf("%d",dp[n][m] % mod);
    return 0;
} 

int main()
{
#ifdef DUIPAI
    freopen("bigger.in", "r", stdin);
#endif
#ifdef BIGOUT
    freopen("bigger.out", "w", stdout);
#endif
    return Do();
}

inline ll readint()       
{
    int flag = 1;
    char c = getchar();
    while (c > '9' || c < '0' && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    ll init = Char_Int(c);
    while ((c = getchar()) <= '9' && c >= '0') init = (init << 3) + (init << 1) + Char_Int(c);
    return init * flag;
}

inline void writeint(int x)
{
    if (x < 0) putchar('-'), x = -x;
    if (x > 9) writeint(x / 10);
    putchar(Int_Char(x % 10));
}
