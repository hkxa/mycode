// luogu-judger-enable-o2
// luogu-judger-enable-o2
/*************************************
 * problem:      赛题 #A: T29693 取石子 | 满分: 500分.
 * user ID:      63720.
 * user name:    航空信奥.
 * time:         2018-05-19.
 * mode:         C++.
 * upload place: Luogu.
*************************************/
#include <stdio.h>
#include <algorithm>
#include <string.h>
#include <iostream>
#include <math.h>
#define ll long long
#define ull unsinged long long
#define rull register unsigned long long
#define ri register int
#define rc register char
#define max(a, b) ((a) > (b) ? (a) : (b)) 
#define min(a, b) ((a) < (b) ? (a) : (b)) 
#define Char_Int(a) ((a) - '0')
#define Int_Char(a) ((a) + '0')
#define NeverZero(a) ((a) < 0 ? 0 : (a))
#define abs(a) ((a) < 0 ? -(a) : (a))
//#define print_end(a) {printf("%d", a); return 0;}
//#define For(i, a, b) for (int i = (a); i < (b); i++)
//#define IfUn(a, b) if (a != b)
//#define Day 365
//#define MOD 
#define MAXN 15000
#define MAXM 40001
//#define _HALF_INT_MAX 1073741823
//#define eof
//#define accept
//#define DEBUG
using namespace std;

long long read();

int main()
{
    long long n = read();
    bool win = false;
    while (n--) if (read() % 2) win = !win;
    printf(win ? "Alice" : "Bob");
    return 0;
} 

inline long long read()       
{
    char c = getchar();
    while (c > '9' || c < '0') c = getchar();
    long long init = Char_Int(c);
    while ((c = getchar()) <= '9' && c >= '0') init = (init << 3) + (init << 1) + Char_Int(c);
    return init;
}

