// luogu-judger-enable-o2
#include <stdio.h>
#include <algorithm>
#include <string.h>
#include <iostream>
#include <math.h>
#define ll long long
#define ull unsinged long long
#define rull register unsigned long long
#define ri register int
#define rc register char
#define max(a, b) ((a) > (b) ? (a) : (b)) 
#define min(a, b) ((a) < (b) ? (a) : (b)) 
#define Char_Int(a) ((a) - '0')
#define Int_Char(a) ((a) + '0')
using namespace std;

short read();

int main()
{
    short n = read();
    int sum = 0;
    while (n--) sum += read();
    printf(sum % 2 ? "Alice" : "Bob");
    return 0;
} 

inline short read()       
{
    char c = getchar();
    while (c > '9' || c < '0') c = getchar();
    short init = Char_Int(c);
    while ((c = getchar()) <= '9' && c >= '0') init = (init << 3) + (init << 1) + Char_Int(c);
    return init;
}

