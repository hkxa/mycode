/*************************************
 * problem:      contest 18784 #C T84893 【tg3】路径.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-07-26.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

int n, m;
int v[200007];

struct Edge {
    int to, v;
    Edge(int To = 0, int Val = 1) : to(To), v(Val) {}
};

#define VaSize (1048576 + 7)
#define PointSize (200000 + 7)
#define TotalSize (VaSize + PointSize)
vector<Edge> G[TotalSize];
int dis[TotalSize] = {0};
bool iq[TotalSize] = {0};

int main()
{
    n = read<int>();
    m = read<int>();
    for (register int i = 1; i <= n; ++i) {
        v[i] = read<int>();
        G[i].push_back(Edge(v[i] + PointSize, 1));
        G[v[i] + PointSize].push_back(Edge(i, 0));
    }
    memset(dis, 0x3f, sizeof(dis));
    int _u, _v;
    for (register int i = 1; i <= m; ++i) {
        _u = read<int>();
        _v = read<int>();
        G[_u].push_back(Edge(_v, 1));
    }
    for (int i = 0; i < 1048576; i++) {
        for (int j = 0; j < 20; j++) {
            if (i & (1 << j)) {
                G[i + PointSize].push_back(Edge((i ^ (1 << j)) + PointSize, 0));
                // if (i <= 3) printf("Value %d->%d.\n", i, i ^ (1 << j));
            }
        }
    }
    queue<int> q;
    q.push(1);
    iq[1] = 1;
    dis[1] = 0;
    while (!q.empty())
    {
        int fr = q.front();
        q.pop();
        iq[fr] = 1;
        for (vector<Edge>::iterator it = G[fr].begin(); it != G[fr].end(); it++) {
            if (dis[fr] + it->v < dis[it->to]) {
                dis[it->to] = dis[fr] + it->v;
                q.push(it->to);
                iq[it->to] = 1;
            }
        }
    }
    for (int i = 1; i <= n; i++) {
        write(dis[i] != 0x3f3f3f3f ? dis[i] : -1, 10);
    }
    return 0;
}