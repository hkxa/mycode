/*************************************
 * problem:      contest 18784 #B T84894 【tg2】流星.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-07-26.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

#define double long double

int n;
struct pos {
    double x, y;
} s[100007], star;

double calc2PointDist(pos a, pos b)
{
    return sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
}

double calcDis(double X)
{
    star.x = X;
    double sum = 0;
    for (int i = 1; i <= n; i++) {
        sum += calc2PointDist(star, s[i]);
    }
    return sum;
}

int main()
{
    // printf("%.0lf %.0lf %.0lf %.0lf", 3.4, 3.49, 3.5, 3.8);
    n = read<int>();
    double l = 1e9 + 7, r = -l, m1, m2;
    for (int i = 1; i <= n; i++) {
        s[i].x = read<int>();
        s[i].y = read<int>();
        l = min(l, s[i].x);
        r = max(r, s[i].x);
    }
    star.y = read<int>();
    // for (double dTmp = 32; dTmp <= 41; dTmp += 0.1) {
    //     printf("(%.1lf, %.1lf) : sum[dist] = %.3lf.\n", dTmp, star.y, calcDis(dTmp));
    // }
    while ((r - l) > 1e-5 || abs(calcDis(l) - calcDis(r)) > 1e-5) {
        m1 = l + (r - l) / 3;
        m2 = r - (r - l) / 3;
        if (calcDis(m1) < calcDis(m2)) r = m2;
        else l = m1;
    }
    printf("%.0llf\n", calcDis((l + r) / 2));
    // printf("final pos : (%.3lf, %.3lf)\n", (l + r) / 2, star.y);
    // printf("exact answer : %.3lf\n", calcDis((l + r) / 2));
    return 0;
}
