/*************************************
 * problem:      contest 18784 #A T84891 【tg1】零件.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-07-26.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

int n, k;
int tmp;
int ans = 0;
bool tag[100007] = {0};

int main()
{
    n = read<int>();
    k = read<int>();
    for (int i = 1; i <= n; i++) {
        tmp = read<int>();
        if (!tag[tmp % k]) {
            tag[tmp % k] = true;
            ans++;
        }
    }
    write(ans, 10);
    return 0;
}