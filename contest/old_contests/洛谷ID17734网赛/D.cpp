/*************************************
 * problem:      contest 17734 D.
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-07-14.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}  

int n;

#define P 1000000007

struct SAN {
    // int id;
    int l, r;

    bool isEdge(const SAN &other) const
    {
        if (l < other.l) return r >= other.l;
        else return l <= other.r;
    } 

    void readIn()
    {
        l = read<int>();
        r = read<int>();
    }

    bool operator < (const SAN &other) const
    {
        return r != other.r ? r < other.r : l < other.l;
    }
} a[4096];

struct union_set {
    int fa[4096];
    int cnt;
    union_set(int sCnt) 
    {
        for (int i = 1; i <= n; i++) fa[i] = i;
        cnt = sCnt;
    }

    int find(int a)
    {
        return a == fa[a] ? a : fa[a] = find(fa[a]);
    }

    bool link(int x, int y)
    {
        int fx = find(x), fy = find(y);
        if (fx == fy) return 0;
        else fa[fx] = fy, cnt--;
        return 1;
    }

    bool isForest()
    {
        return cnt != 1;
    }
};

struct tester_Pts20 {
    int ans;
    bool s[4096];

    tester_Pts20() : ans(0) {}

    bool test_ok(int sCnt) 
    {
        union_set uns(sCnt);
        for (int i = 1; i <= n; i++) {
            if (!s[i]) continue;
            for (int j = i + 1; j <= n; j++) {
                if (!s[j]) continue;
                if (a[i].isEdge(a[j])) {
                    if (!uns.link(i, j)) {
                        return 0;
                    }
                }
            }
        }
        return !uns.isForest();
    }

    void dfs(int p, int sCnt)
    {
        if (p >= n + 1) {
            ans = (ans + test_ok(sCnt)) % P;
            return;
        }
        s[p] = 1; dfs(p + 1, sCnt + 1);
        s[p] = 0; dfs(p + 1, sCnt);
    }

    void do_test()
    {
        dfs(1, 0);
        write(ans, 0);
    }
} test;

#define pts_20() test.do_test()

struct common_BIT {
    int C[4096];
    common_BIT()
    {
        memset(C, 0, sizeof(C));
    }
#   define lowbit(x) ((x) & (-(x)))
    void add(int x, int delta)
    {
        x++;
        while (x <= 4095)
        {
            C[x] = (C[x] + delta) % P;
            x += lowbit(x);
        }
    }
    int sum(int x)
    {
        x++;
        int res(0);
        while (x)
        {
            res = (res + C[x]) % P;
            x -= lowbit(x);
        }
        return res;
    }
#   undef lowbit
} t[4096];

int f[4096][4096] = {0};

void pts_100()
{
    sort(a + 1, a + n + 1);
    for (int i = 1; i <= n; i++) {
        f[i][0] = 1;
        // printf("[ pre  ] in list[%d] : add 1 in pos 0.\n", i);
        t[i].add(0, 1);
    }
    for (int i = 2; i <= n; i++) {
        for (int j = 1; j < i; j++) {
            if (a[j].r < a[i].l) continue;
            if (a[i].l <= a[j].l){
                f[i][j] = t[i].sum(a[j].l - 1);
                // printf("[[%d][%d]] in list[%d] : query %d.\n", i, j, i, a[j].l - 1);
            } else {
                f[i][j] = t[j].sum(a[i].l - 1);
                // printf("[[%d][%d]] in list[%d] : query %d.\n", i, j, j, a[i].l - 1);
            }
            t[i].add(a[j].r, f[i][j]);
            // printf("[[%d][%d]] in list[%d] : add %d in pos %d.\n", i, j, i, f[i][j], a[j].r);
        }
    }
    long long ans(0);
    for (int i = 1; i <= n; i++) {
        for (int j = 0; j < i; j++) {
            // printf("%d ", f[i][j]);
            ans = (ans + f[i][j]) % P;
        }
        // puts("");
    }
    write(ans, 10);
}

int main()
{
    n = read<int>();
    for(int i = 1; i <= n; i++) a[i].readIn();
    // if (n <= 18) pts_20();
    // else pts_100();
    pts_100();
    return 0;
}