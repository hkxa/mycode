#include<bits/stdc++.h>
#define ll long long
#define ljc 1000000007
using namespace std;
#ifdef Fading
#define gc getchar
#endif
#ifndef Fading
inline char gc(){
    static char now[1<<16],*S,*T;
    if (T==S){T=(S=now)+fread(now,1,1<<16,stdin);if (T==S) return EOF;}
    return *S++;
}
#endif
inline ll read(){
    register ll x=0,f=1;char ch=gc();
    while (!isdigit(ch)){if(ch=='-')f=-1;ch=gc();}
    while (isdigit(ch)){x=(x<<3)+(x<<1)+ch-'0';ch=gc();}
    return (f==1)?x:-x;
}
int f[4011][4011],n,m,L[4021];
struct que{
    int x,y;
}x[4011];
inline bool cmp(que a,que b){
    if (a.y==b.y) return a.x<b.x;
    return a.y<b.y;
}
struct tree{
    int tr[4021];
    inline void add(int a,int b){
        a++;
        for (;a<=4001;a+=a&-a) tr[a]=(tr[a]+b)%ljc;
    }
    inline int query(int a){
        int ans=0;
        a++;
        for (;a;a-=a&-a) ans=(ans+tr[a])%ljc;
        return ans;
    }
}tr[4021];
signed main(){ 
    n=read();
    for (int i=1;i<=n;i++){
        x[i].x=read(),x[i].y=read();
    }
    sort(x+1,x+1+n,cmp);
    for (int i=2;i<=n;i++){
        int lb=1,rb=i,ans=-1;
        while (lb<=rb){
            int mid=lb+rb>>1;
            if (x[mid].y<x[i].x) lb=mid+1;
            else ans=mid,rb=mid-1;
        }
        L[i]=ans;//卡常用的，预处理第一个满足条件的点
    }
    for (int i=1;i<=n;i++) f[i][0]=1,tr[i].add(0,1);
    for (int i=2;i<=n;i++){
        for (int j=L[i];j<i;j++){
            if (x[i].x<=x[j].x){
                f[i][j]=tr[i].query(x[j].x-1);
            }else{
                f[i][j]=tr[j].query(x[i].x-1);
            }
            tr[i].add(x[j].y,f[i][j]);
        }
    }
    int ans=0;
    for (int i=1;i<=n;i++){
        for (int j=0;j<i;j++){
            printf("%d ", f[i][j]);
            ans=(ans+f[i][j])%ljc;
        }
        puts("");
    }
    printf("%d\n",ans);
    return 0;
}