#include <math.h>
#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <string.h>
#include <vector>
#include <queue> 
#include <map>
#include <set>
#define openfile(a) {freopen(a".in", "r", stdin); freopen(a".out", "w", stdout); }
#define closefile() {fclose(stdin); fclose(stdout); }
using namespace std;

/* For problem domain */
#define Max_N 1003
int n, a[Max_N];

struct domain {
	string f, s, t;
	void set(string a)
	{
		int len = a.size();
		int i = len - 1;
		for (; i >= 0; i--) {
			if (a[i] == '.') break;
		}
		int p1 = i;
		for (i--; i >= 0; i--) {
			if (a[i] == '.') break;
		}
		int p2 = i;
		bool yes = 0;
		if (a[p1 + 1] == 'c' && a[p1 + 2] == 'n') {
			if (a[p2 + 1] == 'c' && a[p2 + 2] == 'o' && a[p2 + 3] == 'm') 
				yes = 1;
			if (a[p2 + 1] == 'n' && a[p2 + 2] == 'e' && a[p2 + 3] == 't') 
				yes = 1;
			if (a[p2 + 1] == 'o' && a[p2 + 2] == 'r' && a[p2 + 3] == 'g') 
				yes = 1;
			if (a[p2 + 1] == 'g' && a[p2 + 2] == 'o' && a[p2 + 3] == 'v') 
				yes = 1;
		} 
		if (yes) {
			p1 = p2;
			for (i--; i >= 0; i--) {
				if (a[i] == '.') break;
			}
			p2 = i;
		}
		f = a.substr(0, p2);
		s = a.substr(p2 + 1, p1 - (p2 + 1));
		t = a.substr(p1 + 1, len - 1 - (p2 + 1));
//		cout << "f:" << f << " s:" << s << " t:" << t << endl;
	}
} t, pq;



#define fileio
int main()
{
    #ifdef fileio
    openfile("domain");
    #endif
//	t.set("oj.23444.aha.com.cn");
	string a;
	cin >> a;
	t.set(a);
	int n;
	cin >> n;
	int T = n;
	string p ;
	int ans = 0;
	while (n--) {
		cin >> p;
		pq.set(p);
		if (t.s == pq.s && t.t == pq.t) ans++;
		if (t.f == pq.f && t.s == pq.s && t.t == pq.t) ans--;
	}
	if (ans == 0) {
		printf("NO");
	} else if (ans == T) {
		printf("ALL");
	} else {
		printf("%d", ans);
	}
    #ifdef fileio
    closefile();
    #endif
}
