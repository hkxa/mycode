#include <stdio.h>
#define openfile(a) {freopen(a".in", "r", stdin); freopen(a".out", "w", stdout); }
#define closefile() {fclose(stdin); fclose(stdout); }
using namespace std;

/* For problem surround */
#define Max_N 1003
int n, a[Max_N];

#define fileio
int main()
{
    #ifdef fileio
    openfile("surround");
    #endif
	int n;
	scanf("%d", &n);
	printf("%d", n * 2);
    #ifdef fileio
    closefile();
    #endif
}
