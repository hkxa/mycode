#include <math.h>
#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <string.h>
#include <vector>
#include <queue> 
#include <map>
#include <set>
#define openfile(a) {freopen(a".in", "r", stdin); freopen(a".out", "w", stdout); }
#define closefile() {fclose(stdin); fclose(stdout); }
using namespace std;

/* For problem serial */
#define Max_N 10003

char A[Max_N], B[Max_N];
long long topa = 0, topb = 0;
long long n;

void turn(char *start, char *end)
{
	int size = (end - start) / 2;
	for (int i = 0; i < size; i++) {
		start[i] ^= end[- i - 1] ^= start[i] ^= end[- i - 1];
	}
}

void turnToString(long long n, char *a, long long &top)
{
	long long start = top;
	if (!n) {
		a[top++] = '0';
		return;
	}
	while (n) {
		a[top++] = (n % 10) | 48;
		n /= 10;
	}
	turn(a + start, a + top);
}

#define fileio
int main()
{
    #ifdef fileio
    openfile("serial");
    #endif
	scanf("%lld", &n);
	while (n) {
//		printf("now = %d;", n); getchar();
		turnToString((n << 2) / 5, A, topa);
		turnToString((n *  3) / 7, B, topb);
		n = (n << 2) / 5;
	} 
	printf("%lld %lld\n", topa, topb);
	for (int i = 0; i < topa; i++) {
		putchar(A[i]);
	}
	for (int i = topb - 1; i >= 0; i--) {
		putchar(B[i]);
	}
    #ifdef fileio
    closefile();
    #endif
}
