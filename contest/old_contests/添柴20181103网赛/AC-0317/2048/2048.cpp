#include <math.h>
#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <string.h>
#include <vector>
#include <queue> 
#include <map>
#include <set>
#define openfile(a) {freopen(a".in", "r", stdin); freopen(a".out", "w", stdout); }
#define closefile() {fclose(stdin); fclose(stdout); }
using namespace std;

/* For problem 2048 */
#define Max_N 10
int n, block[Max_N][Max_N];

/*
  W
A S D
*/
bool move;

#define a(p) block[p][pRow]

void moveW1(int pRow)
{
	if (a(1) == a(2)) { a(1) = a(1) * 2; a(2) = 0; if (a(1)) move = 1; }
	if (a(2) == a(3)) { a(2) = a(2) * 2; a(3) = 0; if (a(1)) move = 1; }
	if (a(3) == a(4)) { a(3) = a(3) * 2; a(4) = 0; if (a(1)) move = 1; }
	if (a(3) == 0) { a(3) = a(4); a(4) = 0; if (a(1)) move = 1; }
	if (a(2) == 0) { a(2) = a(3); a(3) = 0; if (a(1)) move = 1; }
	if (a(3) == 0) { a(3) = a(4); a(4) = 0; if (a(1)) move = 1; }
	if (a(1) == 0) { a(1) = a(2); a(2) = 0; if (a(1)) move = 1; }
	if (a(2) == 0) { a(2) = a(3); a(3) = 0; if (a(1)) move = 1; }
	if (a(3) == 0) { a(3) = a(4); a(4) = 0; if (a(1)) move = 1; }
}

void moveS1(int pRow)
{
	if (a(3) == a(4)) { a(4) = a(4) * 2; a(3) = 0; if (a(4)) move = 1; }
	if (a(2) == a(3)) { a(3) = a(3) * 2; a(2) = 0; if (a(3)) move = 1; }
	if (a(1) == a(2)) { a(2) = a(2) * 2; a(1) = 0; if (a(2)) move = 1; }
	if (a(2) == 0) { a(2) = a(1); a(1) = 0; if (a(2)) move = 1; }
	if (a(3) == 0) { a(3) = a(2); a(2) = 0; if (a(3)) move = 1; }
	if (a(2) == 0) { a(2) = a(1); a(1) = 0; if (a(2)) move = 1; }
	if (a(4) == 0) { a(4) = a(3); a(3) = 0; if (a(4)) move = 1; }
	if (a(3) == 0) { a(3) = a(2); a(2) = 0; if (a(3)) move = 1; }
	if (a(2) == 0) { a(2) = a(1); a(1) = 0; if (a(2)) move = 1; }
}

#undef a
#define a(p) block[pRow][p]

void moveA1(int pRow)
{
	if (a(1) == a(2)) { a(1) = a(1) * 2; a(2) = 0; if (a(1)) move = 1; }
	if (a(2) == a(3)) { a(2) = a(2) * 2; a(3) = 0; if (a(1)) move = 1; }
	if (a(3) == a(4)) { a(3) = a(3) * 2; a(4) = 0; if (a(1)) move = 1; }
	if (a(3) == 0) { a(3) = a(4); a(4) = 0; if (a(1)) move = 1; }
	if (a(2) == 0) { a(2) = a(3); a(3) = 0; if (a(1)) move = 1; }
	if (a(3) == 0) { a(3) = a(4); a(4) = 0; if (a(1)) move = 1; }
	if (a(1) == 0) { a(1) = a(2); a(2) = 0; if (a(1)) move = 1; }
	if (a(2) == 0) { a(2) = a(3); a(3) = 0; if (a(1)) move = 1; }
	if (a(3) == 0) { a(3) = a(4); a(4) = 0; if (a(1)) move = 1; }
}

void moveD1(int pRow)
{
	if (a(3) == a(4)) { a(4) = a(4) * 2; a(3) = 0; if (a(4)) move = 1; }
	if (a(2) == a(3)) { a(3) = a(3) * 2; a(2) = 0; if (a(3)) move = 1; }
	if (a(1) == a(2)) { a(2) = a(2) * 2; a(1) = 0; if (a(2)) move = 1; }
	if (a(2) == 0) { a(2) = a(1); a(1) = 0; if (a(2)) move = 1; }
	if (a(3) == 0) { a(3) = a(2); a(2) = 0; if (a(3)) move = 1; }
	if (a(2) == 0) { a(2) = a(1); a(1) = 0; if (a(2)) move = 1; }
	if (a(4) == 0) { a(4) = a(3); a(3) = 0; if (a(4)) move = 1; }
	if (a(3) == 0) { a(3) = a(2); a(2) = 0; if (a(3)) move = 1; }
	if (a(2) == 0) { a(2) = a(1); a(1) = 0; if (a(2)) move = 1; }
}

#undef a

void moveW()
{
	move = 0;
	moveW1(1);
	moveW1(2);
	moveW1(3);
	moveW1(4);
}

void moveS()
{
	move = 0;
	moveS1(1);
	moveS1(2);
	moveS1(3);
	moveS1(4);
}

void moveA()
{
	move = 0;
	moveA1(1);
	moveA1(2);
	moveA1(3);
	moveA1(4);
}

void moveD()
{
	move = 0;
	moveD1(1);
	moveD1(2);
	moveD1(3);
	moveD1(4);
}

bool checkOver()
{
	for (int i = 1; i <= 4; i++) {
		for (int j = 1; j <= 4; j++) {
			if (block[i][j] == 0) return 0;
		}
	}
	for (int i = 1; i <= 4; i++) {
		for (int j = 1; j <= 3; j++) {
			if (block[i][j] == block[i][j + 1]) return 0;
		}
	}
	for (int i = 1; i <= 3; i++) {
		for (int j = 1; j <= 4; j++) {
			if (block[i][j] == block[i + 1][j]) return 0;
		}
	}
	return 1;
}

#define fileio
int main()
{
    #ifdef fileio
    openfile("2048");
    #endif
	for (int i = 1; i <= 4; i++) {
		for (int j = 1; j <= 4; j++) {
			scanf("%d", block[i] + j);
		}
	}
	int T;
	scanf("%d", &T);
	char a[100006];
	scanf("%s", a + 1);
	int p = 0;
	for (int i = 1; i <= T; i++) {
		if (checkOver()) {
			for (int i = 1; i <= 4; i++) {
				for (int j = 1; j <= 4; j++) {
					printf("%d ", block[i][j]);
				}
				putchar(10);
			}
			printf("Game over\n");
			return 0;
		}
		switch (a[i]) {
			case 'w' : moveW(); break;
			case 's' : moveS(); break;
			case 'a' : moveA(); break;
			case 'd' : moveD(); break;
		}
		if (!move) {
			p++;
		}
//		for (int i = 1; i <= 4; i++) {
//			for (int j = 1; j <= 4; j++) {
//				printf("%d ", block[i][j]);
//			}
//			putchar(10);
//		}
//		printf("-----------------\n");
	}
	for (int i = 1; i <= 4; i++) {
		for (int j = 1; j <= 4; j++) {
			printf("%d ", block[i][j]);
		}
		putchar(10);
	}
	while (p--) printf("Can not move!\n");
    #ifdef fileio
    closefile();
    #endif
}
