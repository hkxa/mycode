/*************************************
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-02.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n;
int a[10007];
long long sum[10007];
long long mn[10007];
long long mx[10007];
long long ans[10007];
int size[10007];
vector<int> son[10007];

long long getBaoli(int u, int lim)
{
    long long res(min(a[u], lim));
    for (vector<int>::iterator it = son[u].begin(); it != son[u].end(); it++) {
        // mn[u] = min(mn[u], mn[*it]);
        // mx[u] = max(mx[u], mx[*it]);
        if (lim <= mn[*it]) res += size[*it] * lim;
        else if (lim >= mx[*it]) res += sum[*it];
        else res += getBaoli(*it, lim);
        // size[u] += size[*it];
    }
    return res;
}

void getSum(int u)
{
    size[u] = 1;
    sum[u] = mn[u] = mx[u] = a[u];
    for (vector<int>::iterator it = son[u].begin(); it != son[u].end(); it++) {
        getSum(*it);
        mn[u] = min(mn[u], mn[*it]);
        mx[u] = max(mx[u], mx[*it]);
        if (a[u] <= mn[*it]) sum[u] += size[*it] * a[u];
        else if (a[u] >= mx[*it]) sum[u] += sum[*it];
        else sum[u] += getBaoli(*it, a[u]);
        size[u] += size[*it];
    }
}

// void getAns(int u)
// {
//     ans[u] = sum[u];
//     for (vector<int>::iterator it = son[u].begin(); it != son[u].end(); it++) {
//         getAns(*it);
//         ans[u] += ans[*it];
//     }
// }

int main()
{
    n = read<int>();
    for (int i = 1; i <= n; i++) {
        a[i] = read<int>();
        son[read<int>()].push_back(i);
    }
    getSum(1);
    for (int i = 1; i <= n; i++) {
        write(sum[i], 10);
    }
    // getAns(1);
    // for (int i = 1; i <= n; i++) {
    //     write(ans[i], 10);
    // }
    return 0;
}