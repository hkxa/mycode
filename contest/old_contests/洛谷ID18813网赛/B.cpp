/*************************************
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-02.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, m;
int t[1000007], p[1000007];

int main()
{
    n = read<int>();
    m = read<int>();
    t[0] = 0x7fffffff;
    for (int i = 1; i <= n; i++) {
        t[i] = read<int>();
    }
    for (int i = 1; i <= m; i++) {
        p[i] = read<int>();
    }
    int bear(233), i = n, j = n, ans = 0;
    while (p[j] > t[i] && j >= 1) {
        printf("%% *%d, *%d. [%d]\n", i, j, bear);
        j--;
        ans++;
    }
    bear = t[i] - i;
    while (j >= 1 && bear > 0) {
        while (p[j] < t[i] && i >= 1) {
            i--;
            bear = min(bear, t[i] - i);
            printf("^ *%d, *%d. [%d]\n", i, j, bear);
        }
        if (bear > 0) ans++, bear--;
        j--;
    }
    write(ans, 10);
    return 0;
}