/*************************************
 * user ID:      63720.
 * user name:    Jomoo.
 * time:         2019-08-02.
 * language:     C++.
 * upload place: Luogu.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

template <typename Int>
inline Int read()       
{
    Int flag = 1;
    char c = getchar();
    while ((!isdigit(c)) && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    Int init = c & 15;
    while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
	return init * flag;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int n, m;
long long a[100007];
struct way {
    long long k, v;
};
vector<way> w[100007];

int main()
{
    n = read<int>();
    m = read<int>();
    for (int i = 1; i <= n; i++) {
        a[i] = read<long long>();
    }
    way tmp;
    int u;
    for (int i = 1; i <= m; i++) {
        tmp.k = read<long long>();
        u = read<long long>();
        tmp.v = read<long long>();
        w[u].push_back(tmp);
    }
    for (int i = 1; i < n; i++) {
        for (vector<way>::iterator it = w[i].begin(); it != w[i].end(); it++) {
            if (a[it->v] > a[i] * it->k) {
                a[it->v] = a[i] * it->k;
            }
        }
    }
    write(a[n], 10);
    return 0;
}