// luogu-judger-enable-o2
/*
	Problem: C++ 代码模板 
	Author: 航空信奥 
	Date: 2018/08/02
*/
#pragma GCC optimize("O1")
#pragma GCC optimize("O2")
#pragma GCC optimize("O3")
#include <stdio.h>
#include <iostream>
#include <string.h>
#include <vector>
#include <queue>
#include <map>
#include <set>
#define lowbit(a) ((a) & (~a + 1)) // define 快 
using namespace std;
namespace AuthorName { // 防重名 
	inline char Getchar();
	template <typename _TpInt> inline _TpInt read();
	template <typename _TpRealnumber> inline double readr();
	template <typename _TpInt> inline void write(_TpInt x);
	template <typename _TpSwap> inline void swap(_TpSwap &x, _TpSwap &y);
	
//#	define OJ

	int main()
	{
		int n;
		n = read<int>();
		int *a = new int[n + 1];
		for (int i = 1; i <= n; i++)
			a[i] = read<int>();
		int Q;
		Q = read<int>();
		typedef pair<int, int> pii;
		priority_queue <pii, vector<pii>, greater<pii> > q; 
		pii t;
		for (int i = 0; i < Q; i++) {
			t.first  = read<int>();
			t.second = read<int>();
			q.push(t);
		}
		int now = 0;
		for (int i = 0; i < Q; i++) {
			t = q.top();
			q.pop();
			while (now < t.first) {
				int fst = a[1];
				for (int i = 1; i < n; i++)
					a[i] += a[i + 1];
				a[n] += fst;
				now++;
			}
			write<int>(a[t.second]);
			putchar(10);
		}
	    return 0;
	}
	
	
#ifdef OJ
	char BufferRead[1 << 15];
	int rLen = 0, rPos = 0;
	inline char Getchar()
	{
		if (rPos == rLen) rPos = 0, rLen = fread(BufferRead, 1, 1 << 15, stdin);
		if (rPos == rLen) return EOF;
		return BufferRead[rPos++];
	} 
#else
#	define Getchar() getchar()
#endif
	
	template <typename _TpInt>
	inline _TpInt read()       
	{
	    register int flag = 1;
	    register char c = Getchar();
	    while ((c > '9' || c < '0') && c != '-') 
			c = Getchar();
	    if (c == '-') flag = -1, c = Getchar();
	    register _TpInt init = (c & 15);
	    while ((c = Getchar()) <= '9' && c >= '0') 
			init = (init << 3) + (init << 1) + (c & 15);
	    return init * flag;
	}
	
	template <typename _TpRealnumber>
	inline double readr()       
	{
	    register int flag = 1;
	    register char c = Getchar();
	    while ((c > '9' || c < '0') && c != '-') 
	        c = Getchar();
	    if (c == '-') flag = -1, c = Getchar();
	    register _TpRealnumber init = (c & 15);
	    while ((c = Getchar()) <= '9' && c >= '0') 
	        init = init * 10 + (c & 15);
	    if (c != '.') return init * flag;
	    register _TpRealnumber l = 0.1;
	    while ((c = Getchar()) <= '9' && c >= '0') 
	        init = init + (c & 15) * l, l *= 0.1;
	    return init * flag;
	}
	
	template <typename _TpInt>
	inline void write(_TpInt x)
	{
	    if (x < 0) {
	    	putchar('-');
			write<_TpInt>(~x + 1);
		}
		else {
			if (x > 9) write<_TpInt>(x / 10);	
			putchar(x % 10 + '0');
		}
	}
	
	template <typename _TpSwap>
	inline void swap(_TpSwap &x, _TpSwap &y)
	{
	    _TpSwap t = x;
	    x = y;
	    y = t;
	}
}

int main()
{
    AuthorName::main();
    return 0;
}
