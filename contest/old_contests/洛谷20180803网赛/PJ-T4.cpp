// luogu-judger-enable-o2
/*
	Problem: C++ 代码模板 
	Author: 航空信奥 
	Date: 2018/08/02
*/
#pragma GCC optimize("O1")
#pragma GCC optimize("O2")
#pragma GCC optimize("O3")
#include <stdio.h>
#include <iostream>
#include <string.h>
#include <vector>
#include <queue>
#include <map>
#include <set>
#define lowbit(a) ((a) & (~a + 1)) // define 快 
using namespace std;
namespace AuthorName { // 防重名 
	inline char Getchar();
	template <typename _TpInt> inline _TpInt read();
	template <typename _TpRealnumber> inline double readr();
	template <typename _TpInt> inline void write(_TpInt x);
	template <typename _TpSwap> inline void swap(_TpSwap &x, _TpSwap &y);
	
//#	define OJ
	
	void generate_array(int tp[], int n, int m, int seed) {
	    unsigned x = seed;
	    for (int i = 0; i < n; ++i) {
	        x ^= x << 13;
	        x ^= x >> 17;
	        x ^= x << 5;
	    	tp[x % m + 1]++;
	    }
	}
	
	int main()
	{
		int n, m, seed;
		n = read<int>();
		m = read<int>();
		seed = read<long long>();
		int *tp = new int[10000001];
		generate_array(tp, n, m, seed);
		queue <long long> q1, q2; 
		
		for (int i = 1; i <= m; i++) {
			while (tp[i]--) {
				q1.push(i);
			}
		}
		
		long long t1, t2, t3;
		for (int i = 1; i < n; i++) {
			t1 = 0x3f3f3f3f3f3f3f3f;
			t2 = 0x3f3f3f3f3f3f3f3f;
			t3 = 0x3f3f3f3f3f3f3f3f;
			if (!q1.empty()) t1 = q1.front();
			if (!q2.empty()) t2 = q2.front();
			if (t1 < t2) {
				q1.pop();
				if (!q1.empty()) t3 = q1.front();
				if (t2 < t3) {
					if (t1 < t2)
						q2.push(max(t1 << 1, t2));
					else
						q2.push(max(t2 << 1, t1));
					q2.pop();
				}
				else {
					if (t1 < t3)
						q2.push(max(t1 << 1, t3));
					else
						q2.push(max(t3 << 1, t1));
					q1.pop();
				}
			}
			else {
				q2.pop();
				if (!q2.empty()) t3 = q2.front();
				if (t1 < t3) {
					if (t1 < t2)
						q2.push(max(t1 << 1, t2));
					else
						q2.push(max(t2 << 1, t1));
					q1.pop();
				}
				else {
					if (t2 < t3)
						q2.push(max(t2 << 1, t3));
					else
						q2.push(max(t3 << 1, t2));
					q2.pop();
				}
			}
		}
		write<long long>(q2.front());
//		q2.pop();
//		write<long long>(q2.front());
//		q2.pop();
	    return 0;
	}
	
	
#ifdef OJ
	char BufferRead[1 << 15];
	int rLen = 0, rPos = 0;
	inline char Getchar()
	{
		if (rPos == rLen) rPos = 0, rLen = fread(BufferRead, 1, 1 << 15, stdin);
		if (rPos == rLen) return EOF;
		return BufferRead[rPos++];
	} 
#else
#	define Getchar() getchar()
#endif
	
	template <typename _TpInt>
	inline _TpInt read()       
	{
	    register int flag = 1;
	    register char c = Getchar();
	    while ((c > '9' || c < '0') && c != '-') 
			c = Getchar();
	    if (c == '-') flag = -1, c = Getchar();
	    register _TpInt init = (c & 15);
	    while ((c = Getchar()) <= '9' && c >= '0') 
			init = (init << 3) + (init << 1) + (c & 15);
	    return init * flag;
	}
	
	template <typename _TpRealnumber>
	inline double readr()       
	{
	    register int flag = 1;
	    register char c = Getchar();
	    while ((c > '9' || c < '0') && c != '-') 
	        c = Getchar();
	    if (c == '-') flag = -1, c = Getchar();
	    register _TpRealnumber init = (c & 15);
	    while ((c = Getchar()) <= '9' && c >= '0') 
	        init = init * 10 + (c & 15);
	    if (c != '.') return init * flag;
	    register _TpRealnumber l = 0.1;
	    while ((c = Getchar()) <= '9' && c >= '0') 
	        init = init + (c & 15) * l, l *= 0.1;
	    return init * flag;
	}
	
	template <typename _TpInt>
	inline void write(_TpInt x)
	{
	    if (x < 0) {
	    	putchar('-');
			write<_TpInt>(~x + 1);
		}
		else {
			if (x > 9) write<_TpInt>(x / 10);	
			putchar(x % 10 + '0');
		}
	}
	
	template <typename _TpSwap>
	inline void swap(_TpSwap &x, _TpSwap &y)
	{
	    _TpSwap t = x;
	    x = y;
	    y = t;
	}
}

int main()
{
    AuthorName::main();
    return 0;
}
