/*************************************
 * problem:      B.
 * user ID:      63720.
 * user name:    �����Ű�.
 * time:         2018-07-14.
 * mode:         C++.
 * upload place: Luogu.
*************************************/
#include <stdio.h>
#include <algorithm>
#include <string.h>
#include <iostream>
#include <math.h>
#include <queue>
#include <vector>
#include <time.h>
#define i64Inf 0x3f3f3f3f3f3f3f3f
#define i32Inf 0x3f3f3f3f
#define pow2(a) (a) * (a)
#define Char_Int(a) ((a) & 15)
#define Int_Char(a) ((a) + '0')
#define Pi 3.141592653589793
//using namespace std;

template<typename _Tp>
inline const _Tp &min(const _Tp &__a, const _Tp &__b)
{
	//return __comp(__b, __a) ? __b : __a;
	if (__b < __a)
		return __b;
	return __a;
}

template<typename _Tp>
inline const _Tp &max(const _Tp &__a, const _Tp &__b)
{
	//return __comp(__b, __a) ? __b : __a;
	if (__b > __a)
		return __b;
	return __a;
}

typedef int int32;
typedef char char8;
typedef bool bool1;
typedef long long int64;
typedef unsigned uint32;
typedef unsigned long long uint64;

#define ZY_DEBUG
//#define OJ
//#define BIGIN
//#define BIGOUT

#ifdef OJ 
#define getcharR() rT = BufferRead + fread(BufferRead, 1, 1 << 15, stdin)
#define getchar() (rS == rT ? EOF : *rS++)
char BufferRead[1 << 15], *rS = BufferRead, *rT;
#else
#define getcharR()
#endif

#ifdef ZY_DEBUG
#define DeBugPrintf(...) fprintf(stderr, __VA_ARGS__)
#else
#define DeBugPrintf(...) 
#endif

namespace Bits {
    int64 Readint();
    void writeui64(uint64); 
    void writei64(int64); 
}
using namespace Bits;

int Do()
{
    int64 n = Readint(), m = Readint() + 1, s = Readint();
    int64 a[100010] = {0};
    int64 rd[100010] = {0}, top = 0;
    bool1 vis[100010] = {0};
    a[m] = n;
    for (int64 i = 1; i < m; i++) {
    	a[i] = Readint();
	}
	int64 las = s;
	for (int64 i = 1; i <= m; i++) {
		if (a[i] >= las) {
			vis[i] = 1;
			las = a[i] + s;
			rd[top++] = i;
		}
	}
	las = n - s;
	for (int64 i = m - 1; i >= 0; i--) {
		if (vis[i]) continue;
		if (a[i] <= las) {
			vis[i] = 1;
			las = a[i] - s;
			rd[top++] = i;
//			printf("a[%d] = %d & las = %d\n", i, a[i], las);
		}
		if (!vis[i]) {
//			printf("a[%d] = %d & las = %d\n", i, a[i], las);
			printf("NO");
//			for (int i = 0; i <= m; i++) {
//				writei64(rd[i]); putchar(32);
//			}
			return 0;
		}
	}
	printf("YES\n");
	for (int64 i = 0; i < m; i++) {
		writei64(rd[i]); putchar(32);
	}
	putchar(48);
    return 0;
} 

int main()
{
#ifdef BIGIN
    freopen("bigger.in", "r", stdin);
#endif
#ifdef BIGOUT
    freopen("bigger.out", "w", stdout);
#endif
#ifdef ZY_DEBUG
    freopen("bigger.err", "w", stderr);
#endif
	getcharR();
    Do();
    return 0;
}

inline int64 Bits::Readint()       
{
    register int flag = 1;
    register char c = getchar();
    while (c > '9' || c < '0' && c != '-') c = getchar();
    if (c == '-') flag = -1, c = getchar();
    register int64 init = Char_Int(c);
    while ((c = getchar()) <= '9' && c >= '0') init = (init << 3) + (init << 1) + Char_Int(c);
    return init * flag;
}

inline void Bits::writeui64(uint64 x)
{
    //if (x < 0) putchar('-'), x = -x;
    if (x > 9) writeui64(x / 10);
    putchar(Int_Char(x % 10));
}

inline void Bits::writei64(int64 x)
{
    if (x < 0) putchar('-'), x = -x;
    writeui64(x);
}
