/*************************************
 * problem:      D.
 * user ID:      63720.
 * user name:    �����Ű�.
 * time:         2018-07-14.
 * mode:         C++.
 * upload place: Luogu.
*************************************/
#include <stdio.h>
//#include <algorithm>
#include <string.h>
#include <iostream>
#include <math.h>
#define ll long long
#define ull unsinged long long
#define rull register unsigned long long
#define ri register int
#define rc register char
#define max(a, b) ((a) > (b) ? (a) : (b)) 
#define min(a, b) ((a) < (b) ? (a) : (b)) 
#define Char_Int(a) ((a) - '0')
#define Int_Char(a) ((a) + '0')
#define NeverZero(a) ((a) < 0 ? 0 : (a))
#define abs(a) ((a) < 0 ? -(a) : (a))
#define print_end(a) {printf("%d", a); return 0;}
#define For(i, a, b) for (int i = (a); i < (b); i++)
#define IfUn(a, b) if (a != b)
#define NewLongTake()
#define Day 365
#define MOD 
#define MAXN 500001
#define MAXN4 2000001
#define _HALF_INT_MAX 1073741823
#define eof
#define accept
//#define DEBUG
using namespace std;

ll readint();

struct LineTree {
	ll l, r;
	ll tot;
	ll add;
} tree[MAXN4] = {0};
ll a[MAXN] = {0};

ll change(ll test)
{
	ll ans=0;
	while (test) {
		test>>=1;
		ans++;
	}
	test=1;
	while(ans--)test*=2;
	return test;
}

void build(ll lef, ll rig, ll root)
{
	tree[root].l   = lef;
	tree[root].r   = rig;
	tree[root].add = 0;
	
	if (lef == rig) {
		tree[root].tot = a[lef];
		return;
	}
	
	ll tmid = (lef + rig) >> 1;
	build(lef, tmid, root << 1); 
	build(tmid + 1, rig, root << 1 | 1);
	tree[root].tot = tree[root << 1].tot + tree[root << 1 | 1].tot;
	
	return;
}

ll find(ll lef, ll rig, ll root)
{
	if (tree[root].l > rig || tree[root].r < lef) return 0;
	if (tree[root].l >= lef && tree[root].r <= rig) return tree[root].tot;
	
	//pushdown(root);
	return max(find(lef, rig, root << 1), find(lef, rig, root << 1 | 1));
}

int main()
{
	ll n = readint(), t, x, y, k;
	for (ll i = 1; i <= n; i++) a[i] = readint();
	build(1, change(n), 1);
	ll c = 0;
	for (ll i = 1; i <= n; i++) {
		for (ll j = i; j <= n; j++) {
			if (a[i] * a[j] <= find(i, j, 1)) c++;
		}
	}
	printf("%lld", c);
    return 0;
} 

inline ll readint()       
{
    char c = getchar();
    while (c > '9' || c < '0') c = getchar();
    ll init = Char_Int(c);
    while ((c = getchar()) <= '9' && c >= '0') init = (init << 3) + (init << 1) + Char_Int(c);
    return init;
}
