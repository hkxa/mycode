// luogu-judger-enable-o2
/*
	Problem: C++ 代码模板 
	Author: 航空信奥 
	Date: 2018/08/02
*/
#pragma GCC optimize("O1")
#pragma GCC optimize("O2")
#pragma GCC optimize("O3")
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <map>
#include <set>
#define lowbit(a) ((a) & (~a + 1)) // define 快 
using namespace std;
namespace AuthorName { // 防重名 
	inline char Getchar();
	template <typename _TpInt> inline _TpInt read();
	template <typename _TpRealnumber> inline double readr();
	template <typename _TpInt> inline void write(_TpInt x);
	template <typename _TpSwap> inline void swap(_TpSwap &x, _TpSwap &y);
	
//#	define OJ
	
	int n, a[2001];
	int A[2001][2001];
	int B[2001][2001];
	#define min_4(a, b, c, d) min(min(a, b), min(c, d))
	#define max_4(a, b, c, d) max(max(a, b), max(c, d))

	int main()
	{
		//freopen("maker2.txt", "r", stdin);
		srand(20170933);
		int MAX_mnth = 34567;
		n = read<int>();
		for (int i = 1; i <= n; i++) {
			a[i] = read<int>();
		}
		
		for (int i = 1; i <= n; i++) {
			A[i][i] = B[i][i] = a[i];
		}
		
		for (int l = 1; l < n; l++) {
			for (int i = 1, j = l + 1; j <= n; i++, j++) {
				//A[i][j] = min_4(B[i][j - 1], a[j], B[i + 1][j], a[i]);
				//B[i][j] = max_4(A[i][j - 1], a[j], A[i + 1][j], a[i]);
				A[i][j] = min(B[i][j - 1], B[i + 1][j]);
				B[i][j] = max(A[i][j - 1], A[i + 1][j]);
			}
		}
		write(A[1][n]);
		
	    return 0;
	}
	
#ifdef OJ
	char BufferRead[1 << 15];
	int rLen = 0, rPos = 0;
	inline char Getchar()
	{
		if (rPos == rLen) rPos = 0, rLen = fread(BufferRead, 1, 1 << 15, stdin);
		if (rPos == rLen) return EOF;
		return BufferRead[rPos++];
	} 
#else
#	define Getchar() getchar()
#endif
	
	template <typename _TpInt>
	inline _TpInt read()       
	{
	    register int flag = 1;
	    register char c = Getchar();
	    while ((c > '9' || c < '0') && c != '-') 
			c = Getchar();
	    if (c == '-') flag = -1, c = Getchar();
	    register _TpInt init = (c & 15);
	    while ((c = Getchar()) <= '9' && c >= '0') 
			init = (init << 3) + (init << 1) + (c & 15);
	    return init * flag;
	}
	
	template <typename _TpRealnumber>
	inline double readr()       
	{
	    register int flag = 1;
	    register char c = Getchar();
	    while ((c > '9' || c < '0') && c != '-') 
	        c = Getchar();
	    if (c == '-') flag = -1, c = Getchar();
	    register _TpRealnumber init = (c & 15);
	    while ((c = Getchar()) <= '9' && c >= '0') 
	        init = init * 10 + (c & 15);
	    if (c != '.') return init * flag;
	    register _TpRealnumber l = 0.1;
	    while ((c = Getchar()) <= '9' && c >= '0') 
	        init = init + (c & 15) * l, l *= 0.1;
	    return init * flag;
	}
	
	template <typename _TpInt>
	inline void write(_TpInt x)
	{
	    if (x < 0) {
	    	putchar('-');
			write<_TpInt>(~x + 1);
		}
		else {
			if (x > 9) write<_TpInt>(x / 10);	
			putchar(x % 10 + '0');
		}
	}
	
	template <typename _TpSwap>
	inline void swap(_TpSwap &x, _TpSwap &y)
	{
	    _TpSwap t = x;
	    x = y;
	    y = t;
	}
}

int main()
{
    AuthorName::main();
    return 0;
}
