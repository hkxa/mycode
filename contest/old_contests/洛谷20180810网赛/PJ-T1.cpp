// luogu-judger-enable-o2
/*
	Problem: C++ 代码模板 
	Author: 航空信奥 
	Date: 2018/08/02
*/
#pragma GCC optimize("O1")
#pragma GCC optimize("O2")
#pragma GCC optimize("O3")
#include <stdio.h>
#include <iostream>
#include <string.h>
#include <algorithm>
#include <string>
#include <vector>
#include <map>
#include <set>
#define lowbit(a) ((a) & (~a + 1)) // define 快 
using namespace std;
namespace AuthorName { // 防重名 
	inline char Getchar();
	template <typename _TpInt> inline _TpInt read();
	template <typename _TpRealnumber> inline double readr();
	template <typename _TpInt> inline void write(_TpInt x);
	template <typename _TpSwap> inline void swap(_TpSwap &x, _TpSwap &y);
	
#	define SizeN 1010
//#	define OJ

	string Trans10to2(int x)
	{
	    string s;
	    if (x == 0)
	        s = "0";
	    else
	        while (x)
	        {
	            s += char(x % 2 + '0');
	            x /= 2;
	        }
	    reverse(s.begin(), s.end());
	    return s;
	}
	
	int main()
	{
		int a(read<int>());
		int b(read<int>());
		int c(a + b);
		cout << Trans10to2(a) << '+' << Trans10to2(b) << '=' << Trans10to2(c);
	    return 0;
	}
	
#ifdef OJ
	char BufferRead[1 << 15];
	int rLen = 0, rPos = 0;
	inline char Getchar()
	{
		if (rPos == rLen) rPos = 0, rLen = fread(BufferRead, 1, 1 << 15, stdin);
		if (rPos == rLen) return EOF;
		return BufferRead[rPos++];
	} 
#else
#	define Getchar() getchar()
#endif
	
	template <typename _TpInt>
	inline _TpInt read()       
	{
	    register int flag = 1;
	    register char c = Getchar();
	    while ((c > '9' || c < '0') && c != '-') 
			c = Getchar();
	    if (c == '-') flag = -1, c = Getchar();
	    register _TpInt init = (c & 15);
	    while ((c = Getchar()) <= '9' && c >= '0') 
			init = (init << 3) + (init << 1) + (c & 15);
	    return init * flag;
	}
	
	template <typename _TpRealnumber>
	inline double readr()       
	{
	    register int flag = 1;
	    register char c = Getchar();
	    while ((c > '9' || c < '0') && c != '-') 
	        c = Getchar();
	    if (c == '-') flag = -1, c = Getchar();
	    register _TpRealnumber init = (c & 15);
	    while ((c = Getchar()) <= '9' && c >= '0') 
	        init = init * 10 + (c & 15);
	    if (c != '.') return init * flag;
	    register _TpRealnumber l = 0.1;
	    while ((c = Getchar()) <= '9' && c >= '0') 
	        init = init + (c & 15) * l, l *= 0.1;
	    return init * flag;
	}
	
	template <typename _TpInt>
	inline void write(_TpInt x)
	{
	    if (x < 0) {
	    	putchar('-');
			write<_TpInt>(~x + 1);
		}
		else {
			if (x > 9) write<_TpInt>(x / 10);	
			putchar(x % 10 + '0');
		}
	}
	
	template <typename _TpSwap>
	inline void swap(_TpSwap &x, _TpSwap &y)
	{
	    _TpSwap t = x;
	    x = y;
	    y = t;
	}
}

int main()
{
    AuthorName::main();
    return 0;
}
