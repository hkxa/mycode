// luogu-judger-enable-o2
/*
	Problem: C++ 代码模板 
	Author: 航空信奥 
	Date: 2018/08/02
*/
#pragma GCC optimize("O1")
#pragma GCC optimize("O2")
#pragma GCC optimize("O3")
#include <stdio.h>
#include <iostream>
#include <string.h>
#include <vector>
#include <queue>
#include <map>
#include <set>
#define lowbit(a) ((a) & (~a + 1)) // define 快 
using namespace std;
namespace AuthorName { // 防重名 
	inline char Getchar();
	template <typename _TpInt> inline _TpInt read();
	template <typename _TpRealnumber> inline double readr();
	template <typename _TpInt> inline void write(_TpInt x);
	template <typename _TpSwap> inline void swap(_TpSwap &x, _TpSwap &y);
	
//#	define OJ

	int xx[4] = {0, -1, 1, 0}, yy[4] = {1, 0, 0, -1};
	char put[5] = "RUDL";
	int n, m, ans;
	int way[1000007][2], top = 0;
	bool wall[1007][1007] = {0};
	bool vis[1007][1007] = {0};

	bool dfs(int step, int x, int y)
	{
		if (step >= ans) return false;
		if (vis[x][y]) return false;
			vis[x][y] = 1;
		if (x == 1 && y == n) {
			ans = step;
			return true;
		}
		int mx, my;
		for (int o = 0; o < 4; o++) {
			mx = x + xx[o];
			my = y + yy[o];
			if (yy[o] != 0) {
				if (wall[x][min(y, my)]) continue;
			}
			if (mx < 1 || mx > n || my < 1 || my > n) continue;
			way[top][0] = o;
			way[top][1] = 1;
			top++;
			if (dfs(step + 1, mx, my)) return true; 
			vis[mx][my] = 0;
			top--;
		} 
		return false;
	}
	
	int main()
	{
		n = read<int>();
		m = read<int>();
		ans = n * n + 1;
		int x, d, u;
		for (int i = 0; i < m; i++) {
			x = read<int>();
			d = read<int>();
			u = read<int>();
			if (d > u) swap(d, u);
			while (d < u) {
				d++;
				wall[n-d+1][x] = 1;
			}
		}
//		for (int i = 1; i <= n; i++) {
//		for (int j = 1; j <= n; j++)
//			printf("%d ", wall[i][j]); 
//				printf("\n");
//		}
vis[n][1] = 1;
		dfs(0, n, 1);
		int zl = top;
		for (int i = 0; i < top; i++) {
			int j;
			for (j = i + 1; way[j][0] == way[i][0] && j < top; j++) {
				way[j][1] = -1;
				way[i][1]++;
				zl--;
			}
			i = j;
		} 
		printf("%d\n%d\n", ans, zl);
		for (int i = 0; i < top; i++) {
			if (way[i][1] != -1) {
				printf("%c %d\n", put[way[i][0]], way[i][1]);
			}
		} 
	    return 0;
	}
	
	
#ifdef OJ
	char BufferRead[1 << 15];
	int rLen = 0, rPos = 0;
	inline char Getchar()
	{
		if (rPos == rLen) rPos = 0, rLen = fread(BufferRead, 1, 1 << 15, stdin);
		if (rPos == rLen) return EOF;
		return BufferRead[rPos++];
	} 
#else
#	define Getchar() getchar()
#endif
	
	template <typename _TpInt>
	inline _TpInt read()       
	{
	    register int flag = 1;
	    register char c = Getchar();
	    while ((c > '9' || c < '0') && c != '-') 
			c = Getchar();
	    if (c == '-') flag = -1, c = Getchar();
	    register _TpInt init = (c & 15);
	    while ((c = Getchar()) <= '9' && c >= '0') 
			init = (init << 3) + (init << 1) + (c & 15);
	    return init * flag;
	}
	
	template <typename _TpRealnumber>
	inline double readr()       
	{
	    register int flag = 1;
	    register char c = Getchar();
	    while ((c > '9' || c < '0') && c != '-') 
	        c = Getchar();
	    if (c == '-') flag = -1, c = Getchar();
	    register _TpRealnumber init = (c & 15);
	    while ((c = Getchar()) <= '9' && c >= '0') 
	        init = init * 10 + (c & 15);
	    if (c != '.') return init * flag;
	    register _TpRealnumber l = 0.1;
	    while ((c = Getchar()) <= '9' && c >= '0') 
	        init = init + (c & 15) * l, l *= 0.1;
	    return init * flag;
	}
	
	template <typename _TpInt>
	inline void write(_TpInt x)
	{
	    if (x < 0) {
	    	putchar('-');
			write<_TpInt>(~x + 1);
		}
		else {
			if (x > 9) write<_TpInt>(x / 10);	
			putchar(x % 10 + '0');
		}
	}
	
	template <typename _TpSwap>
	inline void swap(_TpSwap &x, _TpSwap &y)
	{
	    _TpSwap t = x;
	    x = y;
	    y = t;
	}
}

int main()
{
    AuthorName::main();
    return 0;
}
