// luogu-judger-enable-o2
/*
    Problem: C++ 代码模板 
    Author: 航空信奥 
    Date: 2018/08/02
*/
#pragma GCC optimize("O1")
#pragma GCC optimize("O2")
#pragma GCC optimize("O3")
#include <stdio.h>
#include <iostream>
#include <string.h>
#include <vector>
#include <queue>
#include <map>
#include <set>
#define lowbit(a) ((a) & (~a + 1)) // define 快 
using namespace std;
namespace AuthorName { // 防重名 
    inline char Getchar();
    template <typename _TpInt> inline _TpInt read();
    template <typename _TpRealnumber> inline double readr();
    template <typename _TpInt> inline void write(_TpInt x);
    template <typename _TpSwap> inline void swap(_TpSwap &x, _TpSwap &y);
    
//#	define OJ
    const bool jmp = 1;
    const bool run = 0;

    struct list_Node{
        int value;
        bool mode;
        list_Node *next_Node;
        list_Node(int v = 0, bool m = jmp) : value(v), mode(m), next_Node(NULL) {}
    };
    
    struct my_list{
        list_Node *head;
        list_Node *tail;
        my_list()
        {
            head = new list_Node;
            tail = head;
        }
        
        list_Node *push_back(int v)
        {
            tail->next_Node = new list_Node(v, !(tail->mode));
            //printf("New:%d, %d\n", tail->next_Node->value, tail->next_Node->mode);
            tail = tail->next_Node;
            return tail;
        }
        
        void put()
        {
            list_Node *p = head->next_Node;
            int las = -1;
            int t = 0;
            while (p) {
                if (p->mode == jmp && (!p->next_Node)) t = -1;
                
                printf("%s %d\n", p->mode ? "JUMP" : "RUN", p->value - las + (p->mode ? 1 : -1) + t);
                //las = p->value + (p->mode ? 1 : -1);
                las = p->value;
                p = p->next_Node;
            }
            //putchar(10);
        }
        /*
        while (p) {
                printf("%s %d\n", p->mode ? "JUMP" : "RUN", p->value - las + (p->mode ? -1 : 1));
                las = p->value + (p->mode ? -1 : 1);
                p = p->next_Node;
            }
        */
    };
    
    int main()
    {
        int n, m, s, d;
        n = read<int>();
        m = read<int>();
        s = read<int>();
        d = read<int>();
        int a[200007];	
        a[0] = 0;
        my_list ml;
        for (int i = 1; i <= n; i++) {
            a[i] = read<int>();
            if (a[i] <= s) {
                printf("Impossible");
                //goto endMain;	
                return 0;
            }
            if (a[i - 1] + 1 != a[i]) {
                if (a[i - 1]) ml.push_back(a[i - 1]);
                ml.push_back(a[i] - 1);
            }
        }
		if (!n) {
		    printf("RUN %d\n", m);
		    return 0;
		}
		if (d == 1) {
		    printf("Impossible\n");
		    return 0;
		}
		if (a[1] - 1 < s) {
		    printf("Impossible\n");
		    return 0;
		}
		if (n == 1 && a[1] - 1) {
		    printf("RUN %d\n", a[1] - 1);
		    printf("JUMP %d\n", 2);
		    if (m - a[1] - 1) printf("RUN %d\n", m - a[1] - 1);
		    return 0;
		}
		if (n == 1 && !(a[1] - 1)) {
		    printf("Impossible\n");
		    return 0;
		}
        ml.push_back(a[n]);
        //if (a[n] != m) 
		ml.push_back(m);
        //readIn():OK.
//ml.put();
        list_Node *p = ml.head->next_Node;
        while (p) {
            if (p->mode == jmp && p->next_Node && p->next_Node->value - p->value <= s) {
                p->value = p->next_Node->value;
                if (p->next_Node->next_Node) {
                    p->value = p->next_Node->next_Node->value;
                    list_Node *t = p->next_Node->next_Node->next_Node;
                    delete p->next_Node->next_Node;
                    delete p->next_Node;
                    p->next_Node = t;
                }
                else {
                    delete p->next_Node;
                    p->next_Node = NULL;
                }
            }
            else {
                p = p->next_Node;
            }
        }
//ml.put();
        
        p = ml.head->next_Node;
        int las = 0;
        while (p) {
            //printf("%d [las %d]\n", p->value, las);
            if (p->mode == jmp) {
            	if (p->value - las + 1 > d) {
	                printf("Impossible");
	                //goto endMain;
	                return 0;
				}
				else {
					p = p->next_Node;
				}
            }
            else {
                las = p->value;
                p = p->next_Node;
            }
        }
        //putchar(10);
		ml.put();
        return 0;
    }
    /*
10 123 6 14
12 34 35 39 42 49 61 80 81 99

    */
    
    
#ifdef OJ
    char BufferRead[1 << 15];
    int rLen = 0, rPos = 0;
    inline char Getchar()
    {
        if (rPos == rLen) rPos = 0, rLen = fread(BufferRead, 1, 1 << 15, stdin);
        if (rPos == rLen) return EOF;
        return BufferRead[rPos++];
    } 
#else
#	define Getchar() getchar()
#endif
    
    template <typename _TpInt>
    inline _TpInt read()       
    {
        register int flag = 1;
        register char c = Getchar();
        while ((c > '9' || c < '0') && c != '-') 
            c = Getchar();
        if (c == '-') flag = -1, c = Getchar();
        register _TpInt init = (c & 15);
        while ((c = Getchar()) <= '9' && c >= '0') 
            init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }
    
    template <typename _TpRealnumber>
    inline double readr()       
    {
        register int flag = 1;
        register char c = Getchar();
        while ((c > '9' || c < '0') && c != '-') 
            c = Getchar();
        if (c == '-') flag = -1, c = Getchar();
        register _TpRealnumber init = (c & 15);
        while ((c = Getchar()) <= '9' && c >= '0') 
            init = init * 10 + (c & 15);
        if (c != '.') return init * flag;
        register _TpRealnumber l = 0.1;
        while ((c = Getchar()) <= '9' && c >= '0') 
            init = init + (c & 15) * l, l *= 0.1;
        return init * flag;
    }
    
    template <typename _TpInt>
    inline void write(_TpInt x)
    {
        if (x < 0) {
        	putchar('-');
            write<_TpInt>(~x + 1);
        }
        else {
            if (x > 9) write<_TpInt>(x / 10);	
            putchar(x % 10 + '0');
        }
    }
    
    template <typename _TpSwap>
    inline void swap(_TpSwap &x, _TpSwap &y)
    {
        _TpSwap t = x;
        x = y;
        y = t;
    }
}

int main()
{
    AuthorName::main();
    return 0;
}
