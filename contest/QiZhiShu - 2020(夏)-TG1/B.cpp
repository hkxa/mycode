//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      QiZhiShu - 2020(夏)-TG1.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-19.
 * @language:     C++.
*************************************/ 
#pragma GCC optimize(2)

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

int n, m;
char ch[50007], tmp;
uint32 l0[50007], r0[50007], ans;

char GetOpt() {
    static char bufferRead[7];
    scanf("%s", bufferRead);
    return *bufferRead;
}

signed main() {
    read >> n >> m;
    scanf("%s", ch + 1);
    for (int i = 1, a, b; i <= m; i++) {
        switch (GetOpt()) {
            case 'A' :
                read >> a;
                ch[a] = GetOpt();
                break;
            case 'B' :
                read >> a >> b;
                ch[a] = tmp = GetOpt();
                while (++a <= b) ch[a] = tmp;
                break;
            case 'C' :
                read >> a >> b;
                ans = l0[a - 1] = r0[b + 1] = 0;
                for (int i = b, cntr = 0; i >= a; i--) {
                    r0[i] = r0[i + 1];
                    if (ch[i] == ')') cntr++;
                    else if (ch[i] == '0') r0[i] += cntr;
                }
                for (int i = a, cntl = 0; i <= b; i++) {
                    l0[i] = l0[i - 1];
                    switch (ch[i]) {
                        case '(' : cntl++; break;
                        case '0' : l0[i] += cntl; break;
                        case 'w' : ans += l0[i - 1] * r0[i + 1]; break;
                    }
                }
                write << ans << '\n';
                break;
        }
    }
    return 0;
}