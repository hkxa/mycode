//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      QiZhiShu - 2020(夏)-TG1.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-19.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

int n, m;
char ch[50007];
uint32 l0[50007], r0[50007], ans;

char GetOpt() {
    static char bufferRead[7];
    scanf("%s", bufferRead);
    return *bufferRead;
}

namespace violence_solution {
    signed solve() {
        scanf("%s", ch + 1);
        uint32 cntl = 0, cntr = 0;
        for (int i = 1, a, b; i <= m; i++) {
            switch (GetOpt()) {
                case 'A' :
                    read >> a;
                    ch[a] = GetOpt();
                    break;
                case 'B' :
                    read >> a >> b;
                    ch[a] = GetOpt();
                    while (++a <= b) ch[a] = ch[a - 1];
                    break;
                case 'C' :
                    read >> a >> b;
                    l0[a - 1] = r0[b + 1] = 0;
                    cntl = cntr = 0;
                    for (int i = a; i <= b; i++) {
                        l0[i] = l0[i - 1];
                        if (ch[i] == '(') cntl++;
                        else if (ch[i] == '0') l0[i] += cntl;
                    }
                    for (int i = b; i >= a; i--) {
                        r0[i] = r0[i + 1];
                        if (ch[i] == ')') cntr++;
                        else if (ch[i] == '0') r0[i] += cntr;
                    }
                    ans = 0;
                    for (int i = a; i <= b; i++) {
                        if (ch[i] == 'w') ans += l0[i - 1] * r0[i + 1];
                    }
                    write << ans << '\n';
                    break;
            }
        }
    }
}

namespace ODT_solution {
    struct node {
        int l, r;
        char ch;
        node() {}
        node(int L) : l(L) {}
        node(int L, int R, int Char) : l(L), r(R), ch(Char) {}
        bool operator < (const node &b) const { return l < b.l; }
        bool operator == (const node &b) const { return l == b.l; }
    };
    uint32 len[50007], cnt;
    char tch[50007];

    set<node> odt;
    typedef set<node>::iterator odt_it;
    odt_it itx, ity;

    void modify_point(int pos, char ne) {
        itx = --odt.upper_bound(node(pos));
        node orig = *itx;
        odt.erase(itx);
        if (orig.ch == ne) return;
        if (orig.l < pos) odt.insert(node(orig.l, pos - 1, orig.ch));
        if (pos < orig.r) odt.insert(node(pos + 1, orig.r, orig.ch));
        odt.insert(node(pos, pos, ne));
    }

    void modify_range(int l, int r, char ne) {
        node x, y;

        ity = --odt.upper_bound(node(r));
        y = *ity;
        // printf("erase <%d~%d, '%c'>\n", ity->l, ity->r, ity->ch);

        itx = --odt.upper_bound(node(l));
        x = *itx;
        // printf("erase <%d~%d, '%c'>\n", itx->l, itx->r, itx->ch);

        if (x == y) {
            odt.erase(itx);
            if (x.l < l) odt.insert(node(x.l, l - 1, x.ch));
            odt.insert(node(l, r, ne));
            if (y.r > r) odt.insert(node(r + 1, y.r, y.ch));
            return;
        }

        odt.erase(ity++);
        odt.erase(itx++);

        // while (itx != ity) odt.erase(itx++);
        odt.erase(itx, ity);
        // if (x.r + 1 <= y.l - 1) printf("(1)insert <%d~%d, '%c'>\n", x.r + 1, y.l - 1, ne);
        if (x.r + 1 <= y.l - 1) odt.insert(node(x.r + 1, y.l - 1, ne));

        // if (x.l < l) printf("(2)insert <%d~%d, '%c'>\n", x.l, l - 1, x.ch);
        if (x.l < l) odt.insert(node(x.l, l - 1, x.ch));
        // printf("(3)insert <%d~%d, '%c'>\n", l, x.r, ne);
        odt.insert(node(l, x.r, ne));

        // printf("(4)insert <%d~%d, '%c'>\n", y.l, r, ne);
        odt.insert(node(y.l, r, ne));
        // if (y.r > r) printf("(5)insert <%d~%d, '%c'>\n", r + 1, y.r, y.ch);
        if (y.r > r) odt.insert(node(r + 1, y.r, y.ch));
    }

    void debug() {
        printf("element(s) of set : \n");
        for (itx = odt.begin(); itx != odt.end(); itx++) {
            printf("<%d~%d, '%c'> ", itx->l, itx->r, itx->ch);
        }
        putchar(10);
    }

    signed solve() {
        scanf("%s", ch + 1);
        for (int i = 1, j = 1; i <= n; i = j + 1) {
            while (j <= n && ch[i] == ch[j + 1]) j++;
            odt.insert(node(i, j, ch[i]));
        }
        // debug();
        uint32 cntl = 0, cntr = 0;
        for (int i = 1, a, b; i <= m; i++) {
            switch (GetOpt()) {
                case 'A' :
                    read >> a;
                    modify_point(a, GetOpt());
                    break;
                case 'B' :
                    read >> a >> b;
                    modify_range(a, b, GetOpt());
                    break;
                case 'C' :
                    read >> a >> b;
                    ity = --odt.upper_bound(node(b));
                    itx = --odt.upper_bound(node(a));
                    if (itx == ity) {
                        len[cnt = 1] = b - a + 1;
                        tch[cnt] = itx->ch;
                    } else {
                        len[cnt = 1] = itx->r - a + 1;
                        tch[cnt] = itx->ch;
                        while (++itx != ity) {
                            len[++cnt] = itx->r - itx->l + 1;
                            tch[cnt] = itx->ch;
                        }
                        len[++cnt] = b - ity->l + 1;
                        tch[cnt] = ity->ch;
                    }
                    l0[0] = r0[cnt + 1] = 0;
                    cntl = cntr = 0;
                    for (int i = 1; i <= cnt; i++) {
                        // printf("[%d, %c] ", len[i], tch[i]);
                        l0[i] = l0[i - 1];
                        if (tch[i] == '(') cntl += len[i];
                        else if (tch[i] == '0') l0[i] += cntl * len[i];
                    }
                    // putchar(10);
                    for (int i = cnt; i >= 1; i--) {
                        r0[i] = r0[i + 1];
                        if (tch[i] == ')') cntr += len[i];
                        else if (tch[i] == '0') r0[i] += cntr * len[i];
                    }
                    ans = 0;
                    for (int i = 1; i <= cnt; i++) {
                        if (tch[i] == 'w') ans += l0[i - 1] * r0[i + 1] * len[i];
                    }
                    write << ans << '\n';
                    break;
            }
            // debug();
        }
        return 0;
    }
}

signed main() {
    read >> n >> m;
    // return ODT_solution::solve();
    if (n <= 1000) violence_solution::solve();
    else ODT_solution::solve();
    return 0;
}