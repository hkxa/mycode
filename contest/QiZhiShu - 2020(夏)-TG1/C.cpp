//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      QiZhiShu - 2020(夏)-TG1.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-19.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 2e6 + 10;

int64 xor_rand() {
    static int64 x = 20170933;
    x ^= x << 13; x ^= x >> 17; x ^= x << 5;
    return x;
}

int n, k, q, i, l, r, ans;
char s[N];
int64 z[N], Z[N];
int a[N], b[N], c[N], d[N];

inline int64 ran(){
    static int64 x = 23333;
    return x ^= x << 13, x ^= x >> 17, x ^= x << 5;
}

int main(){
    read >> n >> k >> q;
    scanf("%s", s + 1);
    for (i = 0; i < k; i++)
        z[i] = xor_rand();
    for (i = 1; i <= n; i++){
        a[i] = a[i - 1];
        Z[i] = Z[i - 1];
        if ((s[i] -= '0') ^ s[i - 1]){
            Z[i] ^= z[i % k];
            a[i] -= b[i % k];
            a[i] += b[i % k] = i / k - b[i % k];
        }
        c[i] = b[(i + 1) % k];
        d[i] = b[i % k];
    }
    while (q--) {
        read >> l >> r;
        if (Z[r] ^ Z[l] ^ (s[r] * z[(r + 1) % k]) ^ (s[l] * z[l % k])){
            puts("-1");
            continue;
        }
        ans = a[r] - a[l];
        if (s[r])
            ans += (r + 1) / k - 2 * c[r];
        if (s[l])
            ans += 2 * d[l] - l / k;
        write << ans << '\n';
    }
    return 0;
}

