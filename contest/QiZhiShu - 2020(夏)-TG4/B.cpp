//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      QiZhiShu - 2020(夏)-TG4.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-22.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 1e5 + 7;

#define ll int64
#define extra_value(u) max(0, dep[0][a[u]] + dep[0][b[u]] - d[u])

int n, m;
vector<int> G[N];
int dep[4][N];
int a[N], b[N], d[N];
int mx, pos;

void no_solution() {
    puts("NO");
    exit(0);
}

void dfs(int u, int fa, int *depth) {
    depth[u] = depth[fa] + 1;
    for (size_t i = 0; i < G[u].size(); i++) 
        if (G[u][i] != fa) dfs(G[u][i], u, depth);
}

int main() {
    dep[0][0] = dep[1][0] = dep[2][0] = dep[3][0] = -1; 
    read >> n >> m;
    for (int i = 1, u, v; i < n; i++) {
        read >> u >> v;
        G[u].push_back(v);
        G[v].push_back(u);
    }
    dfs(1, 0, dep[0]);
    for (int i = 1; i <= m; i++) {
        read >> a[i] >> b[i] >> d[i];
        mx = max(mx, extra_value(i));
    }
    // printf("mx = %d\n", mx);
    for (int i = 1; i <= m; i++)
        if (extra_value(i) == mx) {
            dfs(a[i], 0, dep[1]);
            dfs(b[i], 0, dep[2]);
            mx = d[i];
            break;
        }
    // printf("mx = %d\n", mx);
    dep[0][pos = 0] = 1e9 + 7;
    // for (int i = 1; i <= n; i++)
    //     printf("%d : dis to = %d+%d\n", i, dep[1][i], dep[2][i]);
    for (int i = 1; i <= n; i++)
        if (dep[1][i] + dep[2][i] <= mx && dep[0][i] < dep[0][pos])
            pos = i;
    // printf("pos = %d\n", pos);
    dfs(pos, 0, dep[3]);
    for (int i = 1; i <= m; ++i)
        if (dep[3][a[i]] + dep[3][b[i]] > d[i])
            no_solution();
    printf("%d\n", pos);
    dfs(1, 0, dep[0]);
    return 0;
}