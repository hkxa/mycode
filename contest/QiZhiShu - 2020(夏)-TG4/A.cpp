//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      QiZhiShu - 2020(夏)-TG4.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-22.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 5e5 + 7;

int n, Q, a[N];
int64 floor_sum[N], ceil_sum[N];

inline int floor(int a) {
    return a % 1000 ? a / 1000 * 1000 : a;
}

inline int ceil(int a) {
    return a % 1000 ? (a / 1000 + 1) * 1000 : a;
}

signed main() {
    read >> n >> Q;
    for (int i = 1, integer, decimal; i <= n; i++) {
        read >> integer >> decimal;
        a[i] = integer * 1000 + decimal;
        ceil_sum[i] = ceil_sum[i - 1] + ceil(a[i]) - a[i];
        floor_sum[i] = floor_sum[i - 1] + a[i] - floor(a[i]);
        // printf("floor_diff = %d, ceil_diff = %d\n", a[i] - floor(a[i]), ceil(a[i]) - a[i]);
    }
    int l, r, op;
    int64 floor_max, ceil_max, ans;
    while (Q--) {
        read >> l >> r;
        op = (r - l + 1) * 500;
        floor_max = floor_sum[r] - floor_sum[l - 1];
        ceil_max = ceil_sum[r] - ceil_sum[l - 1];
        // printf("floor_sum[%d...%d] = %lld, ceil_sum[%d...%d] = %lld\n", l, r, floor_max, l, r, ceil_max);
        if (ceil_max > op) ans = ceil_max - op;
        else if (floor_max > op) ans = floor_max - op;
        else ans = min(ceil_max % 1000, floor_max % 1000);
        printf("%lld.%03lld\n", ans / 1000, ans % 1000);
    }
    return 0;
}
//  0.000 0.500 0.750 1.000 2.000 3.000 0.300 0.025 1.000 2.000

/*
4 2
0.999 0.999 0.001 0.001
1 2
3 4
*/
