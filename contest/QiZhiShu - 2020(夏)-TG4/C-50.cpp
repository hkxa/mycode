//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @problem:      QiZhiShu - 2020(夏)-TG4.
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-22.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int P = 1e9 + 7;

int T; int64 n;
int64 f[64][64][64], g[2][64][64];

void pretreat() {
    f[0][0][0] = 1;
    for (int i = 1; i < 63; i++) {
        f[i][i][i] = 1;
        for (int j = 0; j < i; j++) 
            for (int k = 0; k <= j; k++)
                for (int l = k; l <= j; l++)
                    for (int m = k; m <= l; m++)
                        f[i][j][k] = (f[i][j][k] + f[i - 1][j][l] * f[i - 1][m][k]) % P;
    }
    // for (int i = 0; i < 5; i++) 
    //     for (int j = 0; j <= i; j++) 
    //         for (int k = 0; k <= j; k++)    
    //             if (f[i][j][k])
    //                 printf("f[%d][%d][%d] = %lld\n", i, j, k, f[i][j][k]);
}

#define now g[i & 1]
#define lst g[!(i & 1)]

signed main() {
    pretreat();
    read >> T;
    int64 ans;
    int i;
    while (T--) {
        read >> n;
        memset(g, 0, sizeof(g));
        g[0][0][0] = 1;
        for (i = 1; (1ll << i) <= n; i++) {
            if (n & (1ll << i))
                for (int j = 0; j <= i; j++) 
                    for (int k = 0; k <= j; k++) {
                        now[j][k] = 0;
                        for (int l = k; l <= j; l++)
                            for (int m = k; m <= l; m++)
                                now[j][k] = (now[j][k] + f[i][j][l] * lst[m][k]) % P;
                    }
            else 
                for (int j = 0; j <= i; j++) 
                    for (int k = 0; k <= j; k++)
                        now[j][k] = lst[j][k];
        }
        ans = 0;
        for (int j = 0; j < 63; j++) 
            for (int k = 0; k <= j; k++)
                ans = (ans + lst[j][k]) % P;
        write << ans << '\n';
    }
    return 0;
}