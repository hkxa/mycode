//     自己选择的路，跪着也要走完。朋友们，虽然这个世界日益浮躁起来，只
// 要能够为了当时纯粹的梦想和感动坚持努力下去，不管其它人怎么样，我们也
// 能够保持自己的本色走下去。                               ——陈立杰
/*************************************
 * @contest:      Codeforces - Codeforces Round #659 (Div. 1).
 * @user_name:    brealid/hkxadpall/zhaoyi20/jmoo/jomoo/航空信奥/littleTortoise.
 * @time:         2020-07-24.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace baseFastio {
    template <typename Int>
    inline Int read()       
    {
        Int flag = 1;
        char c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline Int read(char &c)       
    {
        Int flag = 1;
        c = getchar();
        while ((!isdigit(c)) && c != '-' && c != EOF) c = getchar();
        if (c == '-') flag = -1, c = getchar();
        Int init = c & 15;
        while (isdigit(c = getchar())) init = (init << 3) + (init << 1) + (c & 15);
        return init * flag;
    }

    template <typename Int>
    inline void write(Int x)
    {
        if (x < 0) putchar('-'), x = ~x + 1;
        if (x > 9) write(x / 10);
        putchar((x % 10) | 48);
    }  

    template <typename Int>
    inline void write(Int x, char nextch)
    {
        write(x);
        putchar(nextch);
    }
}

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        template <typename Int>
        Int operator () () {
            return baseFastio::read<Int>(endch);;
        }
        Reader& operator >> (io_flags f) {
            if (f == ignore_int) baseFastio::read<int>();
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &i) {
            i = baseFastio::read<Int>(endch);
            return *this;
        }
        template <typename Int>
        inline Int get_int() {
            return baseFastio::read<Int>();
        }
        inline char get_nxt() {
            return endch;
        }
    } read;

    struct Writer {
        Writer& operator << (const char *ch) {
            // char *p = ch;
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (const Int i) {
            baseFastio::write(i);
            return *this;
        }
    } write;
}
using namespace Fastio;

// #define int int64

const int N = 1e5 + 7, _2N = 2.5e6;

int T, n;
char a[N], b[N];
int ans;
int road[23][23], mark[23][23];
bool Marked[23];

int dfn[23], low[23], dft;
bool ins[23];
int sta[23], top;
int fam[23], siz[23], famCnt;

void tarjan(int u) {
    low[u] = dfn[u] = ++dft;
    sta[++top] = u;
    ins[u] = true;
    for (int v = 1; v <= 20; v++)
        if (road[u][v]) {
            if (!dfn[v]) {
                tarjan(v);
                low[u] = min(low[u], low[v]);
            } else if (ins[v]) {
                low[u] = min(low[u], dfn[v]);
            }
        }
    if (dfn[u] == low[u]) {
        ins[u] = 0;
        fam[u] = ++famCnt;
        siz[famCnt] = 1;
        while (sta[top] != u) {
            ins[sta[top]] = 0;
            fam[sta[top--]] = famCnt;
            siz[famCnt]++;
        }
        top--;
    }
}

int Try_mark(int u) {
    Marked[u] = true;
    int nRet = 1;
    for (int v = 1; v <= famCnt; v++)
        if (mark[u][v] && !Marked[v]) nRet += Try_mark(v);
    // printf("fam %d : nRet = %d\n", u, nRet);
    return nRet;
}

int EmmAns;
int vis[23];

bool chk(int famId) {
    for (int u = 1; u <= 20; u++)
        if (fam[u] == famId && !vis[u]) return false;
    return true;
}

void dfs(int u, int with, int cnt, int FamId) {
    vis[u]++;
    // printf("dfs(%d, %d, %d)\n", u, with >> 1, cnt);
    if (cnt >= EmmAns) return;
    int Road = 0;
    if (vis[u] == 1)
        for (int i = 1; i <= 20; i++)
            if (road[u][i])
                Road |= (1 << i);
    with |= Road;
    with &= ~(1 << u);
    if (!with && chk(FamId)) {
        EmmAns = cnt;
        return;
    }
    for (int v = 1; v <= 20; v++)
        if ((Road & (1 << v)) && (!vis[v] || (with & (1 << v)))) dfs(v, with, cnt + 1, FamId);
    vis[u]--;
}

int Sol_inBlock(int u) {
    EmmAns = 1e7;
    for (int v = 1; v <= 20; v++)
        if (fam[v] == u) dfs(v, 0, 0, u);
    return EmmAns;
}

signed main() {
    read >> T;
    while (T--) {
        read >> n;
        scanf("%s", a + 1);
        scanf("%s", b + 1);
        memset(road, 0, sizeof(road));
        memset(mark, 0, sizeof(mark));
        memset(dfn, 0, sizeof(dfn));
        for (int i = 0; i < 23; i++) 
            dfn[i] = low[i] = sta[i] = fam[i] = siz[i] = vis[i] = Marked[i] = ins[i] = 0;
        ans = dft = top = famCnt = 0;
        // solve...
        for (int i = 1; i <= n; i++)
            if (a[i] != b[i]) {
                road[a[i] - 'a' + 1][b[i] - 'a' + 1] = true;
            }
        for (int i = 1; i <= 20; i++) 
            if (!dfn[i]) tarjan(i);
        // for (int i = 1; i <= famCnt; i++)
        //     printf("siz[%d] = %d\n", i, siz[i]);
        // for (int i = 0; i < 20; i++) 
        //     printf("fam[%d] = %d\n", i, fam[i]);
        for (int i = 1; i <= famCnt; i++)
            if (siz[i] > 1) ans += Sol_inBlock(i);
        for (int u = 1; u <= 20; u++)
            for (int v = 1; v <= 20; v++)
                if (road[u][v] && fam[u] != fam[v]) {
                    // printf("con %d & %d\n", fam[u], fam[v]);
                    mark[fam[u]][fam[v]] = mark[fam[v]][fam[u]] = true;
                }
        // printf("[bef %d] ", ans);
        for (int i = 1; i <= famCnt; i++)
            if (!Marked[i]) ans += Try_mark(i) - 1;
        write << ans << '\n';
    }
    return 0;
}