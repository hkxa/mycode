#include <bits/stdc++.h>
using namespace std;

template <typename I>
inline I read() {
    char READ_CH = getchar();
    while (!isdigit(READ_CH)) READ_CH = getchar();
    I n = READ_CH & 15;
    while (isdigit(READ_CH = getchar())) n = (((n << 2) + n) << 1) + (READ_CH & 15);
    return n;
}

template <typename Int>
inline void write(Int x)
{
    if (x < 0) putchar('-'), x = ~x + 1;
    if (x > 9) write(x / 10);
    putchar((x % 10) | 48);
}  

template <typename Int>
inline void write(Int x, char nextch)
{
    write(x);
    putchar(nextch);
}

int main()
{
    int a, b;
    a = read<int>(); b = read<int>();
    printf("%d+%d=%d\n", a, b, a + b);
    printf("%d-%d=%d\n", a, b, a - b);
    printf("%d*%d=%d\n", a, b, a * b);
    return 0;
}