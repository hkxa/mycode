int GetRand() {
    static unsigned long long x = 20170933;
    static bool inited = 0;
    if (!inited) {
        inited = true;
        srand(time(0) ^ clock() ^ 2017);
    }
    x ^= x >> 13;
    x ^= x << 17;
    x ^= x >> 5;
    return (x ^ rand()) & INT_MAX;
}

int GetRand(int l, int r) {
    return GetRand() % (r - l + 1) + l;
}