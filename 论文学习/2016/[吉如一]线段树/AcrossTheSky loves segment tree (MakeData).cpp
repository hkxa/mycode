#include <bits/stdc++.h>
using namespace std;

namespace RandomVal {
    unsigned long long x = 20170933;
    bool intend_use_srand_to_init = 0;
}

int GetRand() {
    if (RandomVal::intend_use_srand_to_init) {
        RandomVal::intend_use_srand_to_init = false;
        srand(time(0) ^ clock() ^ RandomVal::x);
        for (int i = 1; i <= 5; i++)
            RandomVal::x = (RandomVal::x << 15) | rand();
    }
    RandomVal::x ^= RandomVal::x >> 13;
    RandomVal::x ^= RandomVal::x << 17;
    RandomVal::x ^= RandomVal::x >> 5;
    return RandomVal::x & INT_MAX;
}

int GetRand(int l, int r) {
    return GetRand() % (r - l + 1) + l;
}

string stdName = "AcrossTheSky loves segment tree";
// string stdName = "AcrossTheSky loves segment tree (BruteForce)";

int system(string command) {
    return system(command.c_str());
}

void Generate(string inf, string ouf, int n, int m, int A, int x) {
    printf("Generate { %s, %s }\n", inf.c_str(), ouf.c_str());
    FILE *in = fopen(inf.c_str(), "w");
    fprintf(in, "%d %d\n", n, m);
    for (int i = 1; i <= n; i++) fprintf(in, "%d ", GetRand(1, A));
    fprintf(in, "\n");
    for (int i = 1; i <= m; i++) {
        int opt = GetRand(1, 3), l = GetRand(1, n), r = GetRand(1, n);
        if (l > r) swap(l, r);
        if (opt == 1 || opt == 2) fprintf(in, "%d %d %d %d\n", opt, l, r, GetRand(1, x));
        else fprintf(in, "%d %d %d\n", opt, l, r);
    }
    fclose(in);
    system("copy " + inf + " AcrossTheSkyLovesSegmentTree.in");
    system("\"" + stdName + "\"");
    system("copy AcrossTheSkyLovesSegmentTree.out " + ouf);
    system("del AcrossTheSkyLovesSegmentTree.in AcrossTheSkyLovesSegmentTree.out");
}

int main() {
    system("g++ \"" + stdName + ".cpp\" -o \"" + stdName + ".exe\"");
    string name = "AcrossTheSkyLovesSegmentTree\\AcrossTheSkyLovesSegmentTree";
    Generate(name + "1.in", name + "1.out", 200, 200, 200, 200);
    Generate(name + "2.in", name + "2.out", 200, 200, 200, 200);
    Generate(name + "3.in", name + "3.out", 1000, 5000, 1000, 1000);
    Generate(name + "4.in", name + "4.out", 1000, 5000, 1000, 1000);
    Generate(name + "5.in", name + "5.out", 5000, 5000, 40000, 40000);
    Generate(name + "6.in", name + "6.out", 5000, 5000, 40000, 40000);
    Generate(name + "7.in", name + "7.out", 20000, 50000, 40000, 40000);
    Generate(name + "8.in", name + "8.out", 20000, 50000, 40000, 40000);
    Generate(name + "9.in", name + "9.out", 20000, 50000, 40000, 40000);
    Generate(name + "10.in", name + "10.out", 50000, 50000, 40000, 40000);
    Generate(name + "11.in", name + "11.out", 50000, 50000, 40000, 40000);
    Generate(name + "12.in", name + "12.out", 50000, 50000, 40000, 40000);
    Generate(name + "13.in", name + "13.out", 300000, 300000, 1e8, 1e8);
    Generate(name + "14.in", name + "14.out", 300000, 300000, 1e8, 1e8);
    Generate(name + "15.in", name + "15.out", 300000, 300000, 1e8, 1e8);
    Generate(name + "16.in", name + "16.out", 300000, 300000, 1e8, 1e8);
    Generate(name + "17.in", name + "17.out", 300000, 300000, 1e8, 1e8);
    Generate(name + "18.in", name + "18.out", 300000, 300000, 1e8, 1e8);
    Generate(name + "19.in", name + "19.out", 300000, 300000, 1e8, 1e8);
    Generate(name + "20.in", name + "20.out", 300000, 300000, 1e8, 1e8);
    system("del \"" + stdName + ".exe\"");
    return 0;
}