/*************************************
 * @problem:      历史最小值&历史最大值&历史版本和.
 * @username:     brealid(zy).
 * @time:         2020-10-09.
 * @language:     C++.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

const int N = 3e5 + 7;

int n, m;
int64 A[N], X[N], Y[N], Z[N];

signed main() {
    scanf("%d%d", &n, &m);
    for (int i = 1; i <= n; ++i) {
        scanf("%lld", A + i);
        X[i] = Y[i] = Z[i] = A[i];
    }
    for (int i = 1, opt, l, r, x; i <= m; ++i) {
        scanf("%d%d%d", &opt, &l, &r);
        if (opt == 1) {
            scanf("%d", &x);
            for (int j = l; j <= r; ++j) A[j] += x;
        } else if (opt == 2) {
            int64 ans = 0;
            for (int j = l; j <= r; ++j) ans += X[j];
            printf("%lld\n", ans);
        } else if (opt == 3) {
            int64 ans = 0;
            for (int j = l; j <= r; ++j) ans += Y[j];
            printf("%lld\n", ans);
        } else if (opt == 4) {
            int64 ans = 0;
            for (int j = l; j <= r; ++j) ans += Z[j];
            printf("%lld\n", ans);
        }
        for (int j = 1; j <= n; ++j) {
            X[j] = min(X[j], A[j]);
            Y[j] = max(Y[j], A[j]);
            Z[j] += A[j];
        }
    }
    return 0;
}