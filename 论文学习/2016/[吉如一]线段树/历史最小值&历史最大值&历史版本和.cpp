/*************************************
 * @problem:      历史最小值&历史最大值&历史版本和.
 * @username:     brealid(zy).
 * @time:         2020-10-09.
 * @language:     C++.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;
typedef long long int64;

const int N = 3e5 + 7;

int n, m, a[N];

class Segtree_beats {
private:
    const int64 inf = 2e18;
    struct SegNode {
        int64 fir_min, sec_min, sum;
        int64 lazy_add;
        int lRange, rRange, cnt_min;
    } tr[N << 2];
    void pushdown(int u) {
        SegNode &now = tr[u], &ls = tr[u << 1], &rs = tr[u << 1 | 1];
        if (now.lazy_add) {
            ls.fir_min += now.lazy_add;
            rs.fir_min += now.lazy_add;
            ls.lazy_add += now.lazy_add;
            rs.lazy_add += now.lazy_add;
            if (ls.sec_min != inf) ls.sec_min += now.lazy_add;
            if (rs.sec_min != inf) rs.sec_min += now.lazy_add;
            ls.sum += now.lazy_add * (ls.rRange - ls.lRange + 1);
            rs.sum += now.lazy_add * (rs.rRange - rs.lRange + 1);
            now.lazy_add = 0;
        }
        if (now.fir_min > ls.fir_min) {
            ls.sum += (now.fir_min - ls.fir_min) * ls.cnt_min;
            ls.fir_min = now.fir_min;
        }
        if (now.fir_min > rs.fir_min) {
            rs.sum += (now.fir_min - rs.fir_min) * rs.cnt_min;
            rs.fir_min = now.fir_min;
        }
    }
    void pushup(int u) {
        SegNode &now = tr[u], &ls = tr[u << 1], &rs = tr[u << 1 | 1];
        now.sum = ls.sum + rs.sum;
        if (ls.fir_min == rs.fir_min) {
            now.fir_min = ls.fir_min;
            now.sec_min = min(ls.sec_min, rs.sec_min);
            now.cnt_min = ls.cnt_min + rs.cnt_min;
        } else if (ls.fir_min < rs.fir_min) {
            now.fir_min = ls.fir_min;
            now.sec_min = min(ls.sec_min, rs.fir_min);
            now.cnt_min = ls.cnt_min;
        } else {
            now.fir_min = rs.fir_min;
            now.sec_min = min(ls.fir_min, rs.sec_min);
            now.cnt_min = rs.cnt_min;
        }
    }
    void build(int u, int l, int r, int *val) {
        tr[u].lRange = l;
        tr[u].rRange = r;
        if (l == r) {
            if (val == NULL) tr[u].sum = tr[u].fir_min = 0;
            else tr[u].sum = tr[u].fir_min = a[l];
            tr[u].sec_min = inf;
            tr[u].cnt_min = 1;
            return;
        }
        int mid = (l + r) >> 1;
        build(u << 1, l, mid, val);
        build(u << 1 | 1, mid + 1, r, val);
        pushup(u);
    }
    void range_add(int u, int ml, int mr, int64 add_value) {
        SegNode &now = tr[u];
        if (now.lRange > mr || now.rRange < ml) return;
        if (now.lRange >= ml && now.rRange <= mr) {
            now.fir_min += add_value;
            now.lazy_add += add_value;
            now.sum += add_value * (now.rRange - now.lRange + 1);
            if (now.sec_min != inf) now.sec_min += add_value;
            return;
        }
        pushdown(u);
        range_add(u << 1, ml, mr, add_value);
        range_add(u << 1 | 1, ml, mr, add_value);
        pushup(u);
    }
    void replace_max(int u, int ml, int mr, int replace_value) {
        SegNode &now = tr[u];
        if (now.lRange > mr || now.rRange < ml) return;
        if (now.lRange >= ml && now.rRange <= mr) {
            if (replace_value <= now.fir_min) return;
            else if (replace_value < now.sec_min) {
                tr[u].sum += (replace_value - tr[u].fir_min) * tr[u].cnt_min;
                tr[u].fir_min = replace_value;
                return;
            }
        }
        pushdown(u);
        replace_max(u << 1, ml, mr, replace_value);
        replace_max(u << 1 | 1, ml, mr, replace_value);
        pushup(u);
    }
    int64 query_sum(int u, int ml, int mr) {
        SegNode &now = tr[u];
        if (now.lRange > mr || now.rRange < ml) return 0;
        if (now.lRange >= ml && now.rRange <= mr) return now.sum;
        pushdown(u);
        return query_sum(u << 1, ml, mr) + query_sum(u << 1 | 1, ml, mr);
    }
public:
    void build(int *val) {
        build(1, 1, n, val);
    }
    void range_add(int LeftRange, int RightRange, int64 DifferValue) {
        range_add(1, LeftRange, RightRange, DifferValue);
    }
    void range_add_and_make_greater_than_0(int LeftRange, int RightRange, int DifferValue) {
        range_add(1, LeftRange, RightRange, DifferValue);
        replace_max(1, LeftRange, RightRange, 0);
    }
    int64 query(int LeftRange, int RightRange) {
        return query_sum(1, LeftRange, RightRange);
    }
} ta, tx, ty, tz;

// tx : a[i] - x[i]
// ty : y[i] - a[i]
// tz : z[i] - t * a[i]

signed main() {
    scanf("%d%d", &n, &m);
    for (int i = 1; i <= n; ++i) scanf("%d", a + i);
    ta.build(a);
    tx.build(NULL);
    ty.build(NULL);
    tz.build(NULL);
    for (int tim = 1, opt, l, r, x; tim <= m; ++tim) {
        scanf("%d%d%d", &opt, &l, &r);
        if (opt == 1) {
            scanf("%d", &x);
            ta.range_add(l, r, x);
            tx.range_add_and_make_greater_than_0(l, r, x);
            ty.range_add_and_make_greater_than_0(l, r, -x);
            tz.range_add(l, r, -x * (int64)tim);
        } else if (opt == 2) {
            printf("%lld\n", ta.query(l, r) - tx.query(l, r));
        } else if (opt == 3) {
            printf("%lld\n", ta.query(l, r) + ty.query(l, r));
        } else if (opt == 4) {
            printf("%lld\n", tz.query(l, r) + tim * ta.query(l, r));
        }
    }
    return 0;
}