/*************************************
 * @problem:      AcrossTheSky loves segment tree (BruteForce).
 * @user_name:    brealid/hkxadpall/zhaoyi20/j(o)moo/littleTortoise.
 * @time:         2020-10-03.
 * @language:     C++.
 * @fastio_ver:   20200913.
*************************************/ 

#include <bits/stdc++.h>
using namespace std;

const int N = 3e5 + 7;

int n, m;
int a[N];

int main() {
    freopen("AcrossTheSkyLovesSegmentTree.in", "r", stdin);
    freopen("AcrossTheSkyLovesSegmentTree.out", "w", stdout);
    scanf("%d%d", &n, &m);
    for (int i = 1; i <= n; i++) scanf("%d", a + i);
    for (int i = 1, opt, l, r, x; i <= m; i++) {
        scanf("%d%d%d", &opt, &l, &r);
        if (opt != 3) scanf("%d", &x);
        if (opt == 1) {
            for (int i = l; i <= r; i++)
                a[i] = min(a[i], x);
        } else if (opt == 2) {
            for (int i = l; i <= r; i++)
                a[i] = max(a[i], x);
        } else {
            long long ans = 0;
            for (int i = l; i <= r; i++)
                ans += a[i];
            printf("%lld\n", ans);
        }
    }
    return 0;
}