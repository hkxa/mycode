/*************************************
 * @problem:      AcrossTheSky loves segment tree.
 * @user_name:    brealid/hkxadpall/zhaoyi20/j(o)moo/littleTortoise.
 * @time:         2020-09-29.
 * @language:     C++.
 * @fastio_ver:   20200913.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

#if 0
#define getchar() (P1 == P2 && (P2 = (P1 = BUF) + fread(BUF, 1, 1 << 21, stdin), P1 == P2) ? EOF : *P1++)
char BUF[1 << 21], *P1 = BUF, *P2 = BUF;
#endif

typedef signed char          int8;
typedef unsigned char       uint8;
typedef short                int16;
typedef unsigned short      uint16;
typedef int                  int32;
typedef unsigned            uint32;
typedef long long            int64;
typedef unsigned long long  uint64;

namespace Fastio {
    enum io_flags {
        ignore_int = 1 << 0,    // input
        flush_stdout = 1 << 1,  // output
        flush_stderr = 1 << 2,  // output
    };
    enum number_type_flags {
        output_double_stable = 1 << 0,  // output
        output_double_faster = 1 << 1   // output
    };

    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (io_flags f) {
            if (f & ignore_int) {
                endch = getchar();
                while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
                while (isdigit(endch = getchar()));
            }
            return *this;
        }
        Reader& operator >> (char &ch) {
            // ignore character set = {' ', '\r', '\n', '\t'}
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (double &lf) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            lf = endch & 15;
            while (isdigit(endch = getchar())) lf = lf * 10 + (endch & 15);
            if (endch == '.') {
                double digit = 0.1;
                while (isdigit(endch = getchar())) {
                    lf += (endch & 15) * digit;
                    digit *= 0.1;
                }
            }
            if (flag) lf = -lf;
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename Int>
        inline Int get() {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            Int d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return d;
        }
    } read;

    struct Writer {
        Writer& operator << (io_flags f) {
            if (f & flush_stdout) fflush(stdout);
            if (f & flush_stderr) fflush(stderr);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = ~x + 1;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
        inline void operator () (double val, int eps_digit = 6, char endch = '\0', number_type_flags flg = output_double_faster) {
            if (flg & output_double_stable) {
                static char output_format[10];
                sprintf(output_format, "%%.%dlf", eps_digit);
                printf(output_format, val);
            } else if (flg & output_double_faster) {
                if (val < 0) {
                    putchar('-');
                    val = -val;
                }
                double eps_number = 0.5;
                for (int i = 1; i <= eps_digit; i++) eps_number /= 10;
                val += eps_number;
                (*this) << (int64)val;
                val -= (int64)val;
                if (eps_digit) putchar('.');
                while (eps_digit--) {
                    val *= 10;
                    putchar((int)val | '0');
                    val -= (int)val;
                }
            } else {
               (*this) << '1';
                if (eps_digit) {
                    (*this) << ".E";
                    for (int i = 2; i <= eps_digit; i++) (*this) << '0';
                }
            }
            if (endch) putchar(endch);
        }
    } write;
}
using namespace Fastio;

namespace File_IO {
    void init_IO(const char *file_name) {
        char buff[107];
        sprintf(buff, "%s.in", file_name);
        freopen(buff, "r", stdin);
        sprintf(buff, "%s.out", file_name);
        freopen(buff, "w", stdout);
    }
}

// #define int int64

const int N = 1000000 + 7, inf = 1e9 + 7;

int n, m, a[N];

struct segtree_node_info {
    int fir_max, sec_max, cnt_max;
    int fir_min, sec_min, cnt_min;
    int lef, rig;
    int64 sum;
} tr[N << 2];

inline void pushdown_min(int u, int val) {
    segtree_node_info &now = tr[u];
    if (val >= now.fir_max) return;     // 无影响
    now.sum -= (int64)(now.fir_max - val) * now.cnt_max;
    now.fir_max = val;
    if (now.sec_min == inf) now.fir_min = min(now.fir_min, val);
    else now.sec_min = min(now.sec_min, val);
}

inline void pushdown_max(int u, int val) {
    segtree_node_info &now = tr[u];
    if (val <= now.fir_min) return;     // 无影响
    now.sum += (int64)(val - now.fir_min) * now.cnt_min;
    now.fir_min = val;
    if (now.sec_max == -inf) now.fir_max = max(now.fir_max, val);
    else now.sec_max = max(now.sec_max, val);
}

void pushdown(int u) {
    int ls = u << 1, rs = u << 1 | 1;
    pushdown_min(ls, tr[u].fir_max);
    pushdown_min(rs, tr[u].fir_max);
    pushdown_max(ls, tr[u].fir_min);
    pushdown_max(rs, tr[u].fir_min);
}

void pushup(int u) {
    int ls = u << 1, rs = u << 1 | 1;
    tr[u].sum = tr[ls].sum + tr[rs].sum;
    if (tr[ls].fir_max == tr[rs].fir_max) {
        tr[u].fir_max = tr[ls].fir_max;
        tr[u].cnt_max = tr[ls].cnt_max + tr[rs].cnt_max;
        tr[u].sec_max = max(tr[ls].sec_max, tr[rs].sec_max);
    } else if (tr[ls].fir_max < tr[rs].fir_max) {
        tr[u].fir_max = tr[rs].fir_max;
        tr[u].cnt_max = tr[rs].cnt_max;
        tr[u].sec_max = max(tr[ls].fir_max, tr[rs].sec_max);
    } else {
        tr[u].fir_max = tr[ls].fir_max;
        tr[u].cnt_max = tr[ls].cnt_max;
        tr[u].sec_max = max(tr[ls].sec_max, tr[rs].fir_max);
    }
    if (tr[ls].fir_min == tr[rs].fir_min) {
        tr[u].fir_min = tr[ls].fir_min;
        tr[u].cnt_min = tr[ls].cnt_min + tr[rs].cnt_min;
        tr[u].sec_min = min(tr[ls].sec_min, tr[rs].sec_min);
    } else if (tr[ls].fir_min > tr[rs].fir_min) {
        tr[u].fir_min = tr[rs].fir_min;
        tr[u].cnt_min = tr[rs].cnt_min;
        tr[u].sec_min = min(tr[ls].fir_min, tr[rs].sec_min);
    } else {
        tr[u].fir_min = tr[ls].fir_min;
        tr[u].cnt_min = tr[ls].cnt_min;
        tr[u].sec_min = min(tr[ls].sec_min, tr[rs].fir_min);
    }
}

void build(int u, int l, int r) {
    tr[u].lef = l;
    tr[u].rig = r;
    if (l == r) {
        tr[u].sum = tr[u].fir_max = tr[u].fir_min = a[l];
        tr[u].sec_max = -inf;
        tr[u].sec_min = inf;
        tr[u].cnt_max = tr[u].cnt_min = 1;
        return;
    }
    int mid = (l + r) >> 1;
    build(u << 1, l, mid);
    build(u << 1 | 1, mid + 1, r);
    pushup(u);
}

void replace_min(int u, int ml, int mr, int val) {
    segtree_node_info &now = tr[u];
    if (now.lef > mr || now.rig < ml) return;
    if (now.lef >= ml && now.rig <= mr) {
        if (val >= now.fir_max) return;
        if (val < now.fir_max && val > now.sec_max) {
            now.sum -= (int64)(now.fir_max - val) * now.cnt_max;
            now.fir_max = val;
            if (now.sec_min == inf) now.fir_min = min(now.fir_min, val);
            else now.sec_min = min(now.sec_min, val);
            return;
        }
    }
    pushdown(u); 
    replace_min(u << 1, ml, mr, val);
    replace_min(u << 1 | 1, ml, mr, val);
    pushup(u);
}

void replace_max(int u, int ml, int mr, int val) {
    segtree_node_info &now = tr[u];
    if (now.lef > mr || now.rig < ml) return;
    if (now.lef >= ml && now.rig <= mr) {
        if (val <= now.fir_min) return;
        if (val > now.fir_min && val < now.sec_min) {
            now.sum += (int64)(val - now.fir_min) * now.cnt_min;
            now.fir_min = val;
            if (now.sec_max == -inf) now.fir_max = max(now.fir_max, val);
            else now.sec_max = max(now.sec_max, val);
            return;
        }
    }
    pushdown(u); 
    replace_max(u << 1, ml, mr, val);
    replace_max(u << 1 | 1, ml, mr, val);
    pushup(u);
}

int64 query_sum(int u, int ml, int mr) {
    segtree_node_info &now = tr[u];
    if (now.lef > mr || now.rig < ml) return 0;
    if (now.lef >= ml && now.rig <= mr) return now.sum;
    pushdown(u);
    return query_sum(u << 1, ml, mr) + query_sum(u << 1 | 1, ml, mr);
}

signed main() {
    File_IO::init_IO("AcrossTheSkyLovesSegmentTree");
    read >> n >> m;
    for (int i = 1; i <= n; i++) read >> a[i];
    build(1, 1, n);
    for (int i = 1, op, x, y; i <= m; i++) {
        read >> op >> x >> y;
        if (op == 1) replace_min(1, x, y, read.get<int>());
        else if (op == 2) replace_max(1, x, y, read.get<int>());
        else write << query_sum(1, x, y) << '\n';
    }
    return 0;
}