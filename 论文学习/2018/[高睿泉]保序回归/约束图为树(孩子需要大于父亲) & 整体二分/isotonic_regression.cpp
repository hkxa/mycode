/*************************************
 * @problem:      isotonic_regression.cpp 约束图为树(孩子需要大于父亲) & 整体二分.
 * @user_name:    brealid/hkxadpall/zhaoyi20/j(o)moo/littleTortoise.
 * @time:         2020-11-01.
 * @language:     C++.
*************************************/ 

#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;

typedef long long int64;

const int N = 2e5 + 7;

int n;
int w[N], a[N];
int fa[N];
vector<int> sons[N];
int val[N], m;
int ans[N];

void set_answer(int u, int ans_v) {
    ans[u] = ans_v;
    for (size_t i = 0; i < sons[u].size(); ++i)
        set_answer(sons[u][i], ans_v);
}

int f[N][2];
void dp(int u, int v0, int v1) {
    f[u][0] = abs(v0 - a[u]) * w[u];
    f[u][1] = abs(v1 - a[u]) * w[u];
    for (size_t i = 0; i < sons[u].size(); ++i) {
        int v = sons[u][i];
        dp(v, v0, v1);
        f[u][0] += min(f[v][0], f[v][1]);
        f[u][1] += f[v][1];
    }
}
void find_divide_point(int u, vector<int> &greater_than) {
    for (size_t i = 0; i < sons[u].size(); ++i) {
        int v = sons[u][i];
        if (f[v][0] < f[v][1]) find_divide_point(v, greater_than);
        else greater_than.push_back(v);
    }
}

void solve(int l, int r, vector<int> rt_list) {
    if (rt_list.empty()) return;
    if (l == r) {
        for (size_t i = 0; i < rt_list.size(); ++i) 
            set_answer(rt_list[i], val[l]);
        return;
    }
    vector<int> less_than, greater_than;
    int mid = (l + r) >> 1;
    for (size_t i = 0; i < rt_list.size(); ++i) {
        int rt = rt_list[i];
        dp(rt, val[mid], val[mid + 1]);
        if (f[rt][0] < f[rt][1]) {
            less_than.push_back(rt);
            find_divide_point(rt, greater_than);
        } else greater_than.push_back(rt);
    }
    solve(l, mid, less_than);
    solve(mid + 1, r, greater_than);
}

signed main() {
    scanf("%d", &n);
    for (int i = 2; i <= n; ++i) {
        scanf("%d", fa + i);
        sons[fa[i]].push_back(i);
    }
    for (int i = 1; i <= n; ++i) scanf("%d", w + i);
    for (int i = 1; i <= n; ++i) {
        scanf("%d", a + i);
        val[i] = a[i];
    }
    sort(val + 1, val + n + 1);
    m = unique(val + 1, val + n + 1) - val - 1;
    solve(1, m, vector<int>(1, 1));
    int64 total_ans = 0;
    for (int i = 1; i <= n; ++i) 
        total_ans += (int64)w[i] * abs(ans[i] - a[i]);
    printf("%lld\n", total_ans);
    for (int i = 1; i <= n; ++i) 
        printf("%d ", ans[i]);
    putchar('\n');
    return 0;
}