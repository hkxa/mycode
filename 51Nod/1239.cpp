/*************************************
 * @problem:      欧拉函数之和.
 * @author:       brealid.
 * @time:         2021-03-21.
*************************************/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned uint32;
typedef long long int64;
typedef unsigned long long uint64;

namespace file_io {
    void set_to_file(string file_name, bool set_in = true, bool set_out = true) {
        if (set_in) freopen((file_name + ".in").c_str(), "r", stdin);
        if (set_out) freopen((file_name + ".out").c_str(), "w", stdout);
    }
    void set_to_stdio(bool set_in = true, bool set_out = true) {
        if (set_in) freopen("con", "r", stdin);
        if (set_out) freopen("con", "w", stdout);
    }
}

#define USE_FREAD  // 使用 fread  读入，去注释符号
#define USE_FWRITE // 使用 fwrite 输出，去注释符号

#ifdef USE_FREAD
namespace iB { char buf[1 << 21], *p1 = buf, *p2 = buf; }
#define getchar() (iB::p1 == iB::p2 && (iB::p2 = (iB::p1 = iB::buf) + fread(iB::buf, 1, 1 << 21, stdin), iB::p1 == iB::p2) ? EOF : *iB::p1++)
#endif
#ifdef USE_FWRITE
namespace oB { char buf[1 << 21], *p1 = buf, *p2 = buf + (1 << 21); }
#define putchar(ch) ((oB::p1 == oB::p2 && fwrite(oB::p1 = oB::buf, 1, 1 << 21, stdout)), *oB::p1++ = ch)
#endif
namespace Fastio {
    struct Reader {
        char endch;
        Reader() { endch = '\0'; }
        Reader& operator >> (char &ch) { // ignore character ' ', '\r', '\n', '\t'
            ch = getchar();
            while (ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t') ch = getchar();
            return *this;
        }
        Reader& operator >> (char *str) {
            while (((*str = getchar()) == ' ' || *str == '\n' || *str == '\r' || *str == '\t') && *str != EOF);
            while ((*++str = getchar()) != ' ' && *str != '\n' && *str != '\r' && *str != '\t' && *str != EOF);
            *str = '\0';
            return *this;
        }
        template <typename Int>
        Reader& operator >> (Int &d) {
            bool flag = 0;
            endch = getchar();
            while ((!isdigit(endch)) && endch != '-' && endch != EOF) endch = getchar();
            if (endch == '-') flag = 1, endch = getchar();
            d = endch & 15;
            while (isdigit(endch = getchar())) d = (d << 3) + (d << 1) + (endch & 15);
            if (flag) d = -d;
            return *this;
        }
        template <typename T>
        inline T get() {
            T Val;
            (*this) >> Val;
            return Val;
        }
    };

    struct Writer {
        ~Writer() {
            #ifdef USE_FWRITE
            fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
            #endif
        }
        Writer& operator << (const char ch) {
            putchar(ch);
            return *this;
        }
        Writer& operator << (const char *ch) {
            while (*ch) putchar(*(ch++));
            return *this;
        }
        Writer& operator << (char* ss) { return *this << (const char *)ss; }
        template <typename Int>
        Writer& operator << (Int x) {
            static char buffer[33];
            static int top = 0;
            if (!x) {
                putchar('0');
                return *this;
            }
            if (x < 0) putchar('-'), x = -x;
            while (x) {
                buffer[++top] = '0' | (x % 10);
                x /= 10;
            }
            while (top) putchar(buffer[top--]);
            return *this;
        }
    };
    void flush_output() {
#ifdef USE_FWRITE
        fwrite(oB::buf, 1, oB::p1 - oB::buf, stdout);
        oB::p1 = oB::buf;
#else
        fflush(stdout);
#endif
    }
}
Fastio::Reader kin;
Fastio::Writer kout;

const int N = 4641589 + 7, P = 1e9 + 7, inv2 = (P + 1) >> 1;

int64 a, b, bound;
bool isnp[N];
int prs[N], pcnt;
int phi[N], sum_phi[N];
map<int64, int64> M_sum_phi;

void initp(int mx) {
    sum_phi[1] = phi[1] = 1;
    for (int i = 2; i <= mx; ++i) {
        if (!isnp[i]) {
            phi[i] = i - 1;
            prs[++pcnt] = i;
        }
        for (int j = 1; j <= pcnt && i * prs[j] <= mx; ++j) {
            isnp[i * prs[j]] = true;
            if (i % prs[j] == 0) {
                phi[i * prs[j]] = phi[i] * prs[j];
                continue;
            }
            phi[i * prs[j]] = phi[i] * (prs[j] - 1);
        }
        sum_phi[i] = (sum_phi[i - 1] + phi[i]) % P;
    }
}

int64 S(int64 n) {
    if (n <= bound) return sum_phi[n];
    map<int64, int64>::iterator it = M_sum_phi.find(n);
    if (it != M_sum_phi.end()) return it->second;
    int64 &ans = M_sum_phi[n];
    ans = (n % P) * ((n + 1) % P) % P * inv2 % P;
    for (int64 l = 2, r; l <= n; l = r + 1) {
        r = n / (n / l);
        ans -= S(n / l) * (r - l + 1) % P;
    }
    ans = ((ans % P) + P) % P;
    return ans;
}

signed main() {
    kin >> a;
    bound = pow(a, 2. / 3.);
    initp(bound);
    kout << S(a) << '\n';
    return 0;
}